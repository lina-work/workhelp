﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using WorkHelp.ArasDesk.Samples.SqlClient;

namespace WorkHelp.ArasDesk.Methods.Judo.種子籤
{
    internal class in_meeting_draw_xls3 : Item
    {
        public in_meeting_draw_xls3(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 隊職員名冊
                輸入: meeting_id
                輸出: 隊職員名冊
                人員: lina 創建 (2024-07-15)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_draw_xls3";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
                FontName = "標楷體",
            };

            if (cfg.meeting_id == "")
            {
                throw new Exception("賽事 id 不得為空白");
            }

            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");

            switch (cfg.scene)
            {
                case "export-insurance":
                    ExportInsuranceXlsx(cfg, itmR);
                    break;
                default:
                    ExportStaffXlsx(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void ExportInsuranceXlsx(TConfig cfg, Item itmReturn)
        {
            var pages = MapInsuranceDays(cfg);

            var exp = ExportInfo(cfg, "weight_path");
            var workbook = new Spire.Xls.Workbook();

            for (var i = 0; i < pages.Count; i++)
            {
                var page = pages[i];
                AppendInsuranceSheetByDay(cfg, workbook, page);
            }

            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();

            string guid = System.Guid.NewGuid().ToString().Substring(0, 4).ToUpper()
                + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");

            string ext_name = ".xlsx";
            string xls_name = cfg.mt_title + "_保險名單_" + guid;
            string xls_file = exp.export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);

            itmReturn.setProperty("xls_name", xls_url);

            AppendExportLog(cfg, "保險名單");
        }

        private void AppendExportLog(TConfig cfg, string in_name)
        {
            var itmNew = cfg.inn.newItem("In_Meeting_Export_Log", "add");
            itmNew.setProperty("in_meeting", cfg.meeting_id);
            itmNew.setProperty("in_name", in_name);
            itmNew.apply();
        }

        private void AppendInsuranceSheetByDay(TConfig cfg, Spire.Xls.Workbook workbook, TDay page)
        {
            cfg.HeadColor = System.Drawing.Color.FromArgb(237, 237, 237);
            cfg.FontName = "新細明體";

            var sheet = workbook.CreateEmptySheet();
            sheet.Name = page.day;

            sheet.PageSetup.TopMargin = 0.4;
            sheet.PageSetup.LeftMargin = 0.6;
            sheet.PageSetup.RightMargin = 0.6;
            sheet.PageSetup.BottomMargin = 0.6;

            var titlePos = "A1:F1";
            var titleTxt = cfg.mt_title;
            SetStrCell(cfg, sheet, titlePos, titleTxt, 16, 20, true, true, HA.C);

            SetStrCell(cfg, sheet, "A2", "比賽日期", 12, 16, false, true, HA.R);
            SetStrCell(cfg, sheet, "B2", page.day, 12, 16, false, true, HA.L);
            SetStrCell(cfg, sheet, "E2", "人數", 12, 16, false, true, HA.R);
            SetStrCell(cfg, sheet, "F2", page.players.Count.ToString(), 12, 16, false, true, HA.R);

            var ri = 3;
            SetColorHead(cfg, sheet, "A" + ri, "姓名", 12, 0, true, false, HA.C);
            SetColorHead(cfg, sheet, "B" + ri, "身分證字號", 12, 0, true, false, HA.C);
            SetColorHead(cfg, sheet, "C" + ri, "性別", 12, 0, true, false, HA.C);
            SetColorHead(cfg, sheet, "D" + ri, "出生年月日", 12, 0, true, false, HA.C);
            SetColorHead(cfg, sheet, "E" + ri, "組名", 12, 0, true, false, HA.C);
            SetColorHead(cfg, sheet, "F" + ri, "目前單位", 12, 0, true, false, HA.C);
            ri++;

            for (var i = 0; i < page.players.Count; i++)
            {
                var x = page.players[i];
                SetStrCell(cfg, sheet, "A" + ri, x.in_name, 12, 0, false, false, HA.C);
                SetStrCell(cfg, sheet, "B" + ri, x.in_sno, 12, 0, false, false, HA.C);
                SetStrCell(cfg, sheet, "C" + ri, x.in_gender, 12, 0, false, false, HA.C);
                SetStrCell(cfg, sheet, "D" + ri, x.in_birth, 12, 0, false, false, HA.C);
                SetStrCell(cfg, sheet, "E" + ri, GetDayPlayerSects(cfg, x), 12, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "F" + ri, GetDayPlayerOrgs(cfg, x), 12, 0, false, false, HA.L);
                ri++;
            }

            for (var i = 0; i < 6; i++)
            {
                sheet.Columns[i].AutoFitColumns();
            }

            SetRangeBorder(sheet, "A3" + ":" + "F" + (ri - 1));
        }

        private string GetDayPlayerOrgs(TConfig cfg, TDPlayer x)
        {
            if (x.orgNames == null) return "";
            return string.Join("、", x.orgNames);
        }

        private string GetDayPlayerSects(TConfig cfg, TDPlayer x)
        {
            if (x.sectNames == null) return "";
            return string.Join(System.Environment.NewLine, x.sectNames);
        }

        private List<TDay> MapInsuranceDays(TConfig cfg)
        {
            var days = new List<TDay>();
            var itmDays = GetProgramFightDayItems(cfg);
            if (itmDays.isError() || itmDays.getResult() == "") throw new Exception("尚未設定組別的比賽日期");

            var dayCount = itmDays.getItemCount();
            for (var i = 0; i < dayCount; i++)
            {
                var itmDay = itmDays.getItemByIndex(i);
                var in_fight_day = itmDay.getProperty("in_fight_day", "");
                var items = GetInsuranceMUserItems(cfg, in_fight_day);
                days.Add(new TDay
                {
                    day = in_fight_day,
                    players = MapInsuranceRows(cfg, items),
                });
            }
            return days;
        }

        private List<TDPlayer> MapInsuranceRows(TConfig cfg, Item items)
        {
            var rows = new List<TDPlayer>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_sno = item.getProperty("in_sno", "");
                var in_current_org = item.getProperty("in_current_org", "");
                var key = in_sno;

                var row = rows.Find(x => x.key == key);
                if (row == null)
                {
                    row = new TDPlayer
                    {
                        key = key,
                        in_name = item.getProperty("in_name", ""),
                        in_sno = in_sno,
                        in_gender = item.getProperty("in_gender", ""),
                        in_birth = item.getProperty("in_birth", ""),
                        sectRows = new List<TDSect>(),
                        sectNames = new List<string>(),
                        orgRows = new List<TDOrg>(),
                        orgNames = new List<string>(),
                    };
                    rows.Add(row);
                }

                var sect = new TDSect
                {
                    id = item.getProperty("id", ""),
                    in_l1 = item.getProperty("in_l1", ""),
                    in_l2 = item.getProperty("in_l2", ""),
                    in_l3 = item.getProperty("in_l3", ""),
                };

                sect.in_l2 = sect.in_l2.Replace("個-", "").Replace("團-", "").Replace("格-", "");

                var sectName = sect.in_l1 + "-" + sect.in_l2 + "-" + sect.in_l3;

                row.sectRows.Add(sect);
                row.sectNames.Add(sectName);

                var org = row.orgRows.Find(x => x.in_current_org == in_current_org);
                if (org == null)
                {
                    org = new TDOrg 
                    {
                        in_current_org = in_current_org,
                        in_short_org =item.getProperty("in_short_org", ""),
                    };
                    row.orgRows.Add(org);
                    row.orgNames.Add(org.in_short_org);
                }
            }
            return rows;
        }

        private class TDay
        {
            public string day { get; set; }
            public List<TDPlayer> players { get; set; }
        }

        private class TDPlayer
        {
            public string key { get; set; }
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_gender { get; set; }
            public string in_birth { get; set; }

            public List<TDSect> sectRows { get; set; }
            public List<string> sectNames { get; set; }

            public List<TDOrg> orgRows { get; set; }
            public List<string> orgNames { get; set; }
        }

        private class TDOrg
        {
            public string in_current_org { get; set; }
            public string in_short_org { get; set; }
        }

        private class TDSect
        {
            public string id { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
        }

        private Item GetInsuranceMUserItems(TConfig cfg, string in_fight_day)
        {
            var sql = @"
                SELECT
	                t1.id
	                , t1.in_fight_day
	                , t2.in_name
	                , UPPER(t2.in_sno) AS 'in_sno'
	                , t2.in_gender
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, t2.in_birth), 111) AS 'in_birth'
	                , t2.in_l1
	                , t2.in_l2
	                , t2.in_l3
	                , t2.in_current_org
	                , t2.in_short_org
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_USER t2 WITH(NOLOCK)
	                ON t2.source_id = t1.in_meeting
	                AND t2.in_l1 = t1.in_l1
	                AND t2.in_l2 = t1.in_l2
	                AND t2.in_l3 = t1.in_l3
                LEFT JOIN
	                IN_ORG_MAP t3 WITH(NOLOCK)
	                ON t3.in_stuff_b1 = t2.in_stuff_b1
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_fight_day = '{#in_fight_day}'
                ORDER BY
	                t1.in_sort_order
	                , t2.in_city_no
	                , t2.in_stuff_b1
	                , t2.in_current_org
	                , t2.in_birth
	                , t2.in_name
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", in_fight_day);

            return cfg.inn.applySQL(sql);
        }

        private Item GetProgramFightDayItems(TConfig cfg)
        {
            var sql = @"
                SELECT DISTINCT 
	                in_fight_day 
                FROM 
	                IN_MEETING_PROGRAM WITH(NOLOCK) 
                WHERE
	                in_meeting = '{#meeting_id}'
	                AND ISNULL(in_fight_day, '') <> ''
                ORDER BY
	                in_fight_day
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private void ExportStaffXlsx(TConfig cfg, Item itmReturn)
        {
            var exp = ExportInfo(cfg, "weight_path");

            //取得與會者資料
            var itmMUsers = GetMeetingUsers(cfg);
            var musers = MapMUserList(cfg, itmMUsers);
            var orgSource = MapOrgList(cfg, musers);
            var orgs = orgSource.OrderBy(x => x.in_stuff_b1).ToList();

            //試算表
            var workbook = new Spire.Xls.Workbook();
            //選手名單
            AppendPlayersSheet(cfg, workbook, musers);
            //公告名單
            AppendBulletinSheet(cfg, workbook, musers);
            // //保險代表人
            // AppendInsuranceSheet(cfg, workbook, musers);
            //隊職員名冊
            AppendOrgPersonsSheet(cfg, workbook, orgs);
            //簽到表
            AppendOrgSignatureSheet(cfg, workbook, orgs);

            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();

            string ext_name = ".xlsx";
            string xls_name = cfg.mt_title + "_選手與隊職員_" + DateTime.Now.ToString("MMdd_HHmmss");
            string xls_file = exp.export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);

            itmReturn.setProperty("xls_name", xls_url);
        }

        private void AppendOrgSignatureSheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TOrg> orgs)
        {
            cfg.FontName = "標楷體";

            var sheet = workbook.CreateEmptySheet();
            sheet.Name = "簽到表";

            sheet.PageSetup.TopMargin = 0.2;
            sheet.PageSetup.LeftMargin = 0.2;
            sheet.PageSetup.RightMargin = 0.2;
            sheet.PageSetup.BottomMargin = 0.2;

            var titlePos = "A1:D1";
            var titleTxt = cfg.mt_title + "簽到表";
            SetStrCell(cfg, sheet, titlePos, titleTxt, 24, 30, false, true, HA.C);

            SetStrCell(cfg, sheet, "A2", "編號", 18, 0, false, false, HA.C);
            SetStrCell(cfg, sheet, "B2", "單位", 18, 0, false, false, HA.C);
            SetStrCell(cfg, sheet, "C2", "簽到", 18, 0, false, false, HA.C);
            SetStrCell(cfg, sheet, "D2", "備註" + Environment.NewLine + "(收據編號)", 12, 0, true, false, HA.C);

            var ri = 3;
            var no = 1;
            for (var i = 0; i < orgs.Count; i++)
            {
                var x = orgs[i];

                SetStrCell(cfg, sheet, "A" + ri, "【" + no + "】", 12, 36, false, false, HA.C);
                SetStrCell(cfg, sheet, "B" + ri, x.in_short_org, 18, 36, false, false, HA.L);
                ri++;
                no++;
            }
            sheet.Columns[0].ColumnWidth = 9;
            sheet.Columns[1].ColumnWidth = 42;
            sheet.Columns[2].ColumnWidth = 30;
            sheet.Columns[3].ColumnWidth = 15;

            SetRangeBorder(sheet, "A2" + ":" + "D" + (ri - 1));

            sheet.PageSetup.PrintTitleRows = "$1:$2";
            sheet.PageSetup.CenterHorizontally = true;
        }

        private void AppendBulletinSheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TPerson> musers)
        {
            cfg.FontName = "新細明體";

            var sorted = musers.FindAll(x => x.isPlayer)
                .OrderBy(x => x.in_stuff_b1).ThenBy(x => x.program_sort).ThenBy(x => x.in_name).ToList();

            var sheet = workbook.CreateEmptySheet();
            sheet.Name = "公告名單";

            sheet.PageSetup.TopMargin = 0.2;
            sheet.PageSetup.LeftMargin = 0.2;
            sheet.PageSetup.RightMargin = 0.2;
            sheet.PageSetup.BottomMargin = 0.2;

            var titlePos = "A1:C1";
            var titleTxt = cfg.mt_title + "確認名單";
            SetStrCell(cfg, sheet, titlePos, titleTxt, 18, 30, false, true, HA.C);

            SetStrCell(cfg, sheet, "A2", "姓名", 11, 0, false, false, HA.C);
            SetStrCell(cfg, sheet, "B2", "單位", 11, 0, false, false, HA.C);
            SetStrCell(cfg, sheet, "C2", "量級", 11, 0, false, false, HA.C);

            var ri = 3;
            var lastOrg = "";
            for (var i = 0; i < sorted.Count; i++)
            {
                var x = sorted[i];
                var name = x.in_name[0] + "O" + x.in_name.Substring(2, x.in_name.Length - 2);

                if (i > 0 && lastOrg != x.map_org_name)
                {
                    lastOrg = x.map_org_name;
                    ri++;
                }

                SetStrCell(cfg, sheet, "A" + ri, name, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "B" + ri, x.map_org_name, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "C" + ri, x.program_name, 11, 0, false, false, HA.L);

                ri++;
            }
            sheet.Columns[0].ColumnWidth = 12;
            sheet.Columns[1].ColumnWidth = 28;
            sheet.Columns[2].ColumnWidth = 57;

            SetRangeBorder(sheet, "A2" + ":" + "C" + (ri - 1));

            sheet.PageSetup.PrintTitleRows = "$1:$2";
            sheet.PageSetup.CenterHorizontally = true;
        }

        private void AppendPlayersSheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TPerson> musers)
        {
            cfg.FontName = "新細明體";

            var sorted = musers.FindAll(x => x.isPlayer)
                .OrderBy(x => x.reg_birth).ThenBy(x => x.in_stuff_b1).ToList();

            var sheet = workbook.CreateEmptySheet();
            sheet.Name = "選手名單";

            sheet.PageSetup.TopMargin = 0.4;
            sheet.PageSetup.LeftMargin = 0.6;
            sheet.PageSetup.RightMargin = 0.6;
            sheet.PageSetup.BottomMargin = 0.6;

            SetStrCell(cfg, sheet, "A1", "姓名", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "B1", "身分證字號", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "C1", "性別", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "D1", "生日", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "E1", "單位", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "F1", "賽事", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "G1", "組別", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "H1", "量級", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "I1", "籤號", 11, 0, true, false, HA.C);

            var ri = 2;
            for (var i = 0; i < sorted.Count; i++)
            {
                var x = sorted[i];
                if (!x.isPlayer) continue;
                var wsBirth = x.reg_birth.ToString("yyyy年MM月dd日");
                var twBirth = (x.reg_birth.Year - 1911) + "年" + x.reg_birth.ToString("MM月dd日");
                SetStrCell(cfg, sheet, "A" + ri, x.in_name, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "B" + ri, x.in_sno.ToUpper(), 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "C" + ri, x.in_gender, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "D" + ri, twBirth, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "E" + ri, x.map_org_name, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "F" + ri, cfg.mt_title, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "G" + ri, x.in_l1, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "H" + ri, x.program_name, 11, 0, false, false, HA.L);
                ri++;
            }
            for (var i = 0; i < 8; i++)
            {
                sheet.Columns[i].AutoFitColumns();
            }
        }

        private void AppendInsuranceSheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TPerson> musers)
        {
            cfg.FontName = "新細明體";

            var sorted = musers.FindAll(x => x.isInsurance)
                .OrderBy(x => x.in_stuff_b1).ToList();

            var sheet = workbook.CreateEmptySheet();
            sheet.Name = "保險代表人";

            sheet.PageSetup.TopMargin = 0.4;
            sheet.PageSetup.LeftMargin = 0.6;
            sheet.PageSetup.RightMargin = 0.6;
            sheet.PageSetup.BottomMargin = 0.6;

            SetStrCell(cfg, sheet, "A1", "姓名", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "B1", "身分證字號", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "C1", "性別", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "D1", "生日", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "E1", "單位", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "F1", "賽事", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "G1", "組別", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "H1", "選手家長", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "I1", "與選手關係", 11, 0, true, false, HA.C);

            var ri = 2;
            for (var i = 0; i < sorted.Count; i++)
            {
                var x = sorted[i];
                SetStrCell(cfg, sheet, "A" + ri, x.in_name, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "B" + ri, x.in_sno.ToUpper(), 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "C" + ri, x.in_gender, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "D" + ri, x.twBirth, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "E" + ri, x.map_org_name, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "F" + ri, cfg.mt_title, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "G" + ri, x.in_l1, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "H" + ri, x.in_emrg_contact1, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "I" + ri, x.in_emrg_relation1, 11, 0, false, false, HA.L);
                ri++;
            }
            for (var i = 0; i < 9; i++)
            {
                sheet.Columns[i].AutoFitColumns();
            }
        }

        private void AppendOrgPersonsSheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TOrg> orgs)
        {
            cfg.FontName = "標楷體";
            var sheet = workbook.CreateEmptySheet();
            sheet.Name = "隊職員名冊";

            sheet.PageSetup.TopMargin = 0.4;
            sheet.PageSetup.LeftMargin = 0.6;
            sheet.PageSetup.RightMargin = 0.6;
            sheet.PageSetup.BottomMargin = 0.6;

            var titlePos = "A1:I3";
            var titleTxt = cfg.mt_title + Environment.NewLine + "隊職員名冊";
            SetStrCell(cfg, sheet, titlePos, titleTxt, 16, 20, false, true, HA.C);

            var rpt = new TReport { ridx = 4 };
            var no = 1;
            for (var i = 0; i < orgs.Count; i++)
            {
                var org = orgs[i];
                var staffCount = org.leaders.Count + org.managers.Count + org.coaches_m.Count + org.coaches_w.Count;

                var orgPos1 = "A" + rpt.ridx;
                var orgTxt1 = "【" + no + "】";
                SetStrCell(cfg, sheet, orgPos1, orgTxt1, 14, 20, false, false, HA.C);

                var orgPos2 = "B" + rpt.ridx + ":" + "I" + rpt.ridx;
                var orgTxt2 = org.in_short_org;
                SetStrCell(cfg, sheet, orgPos2, orgTxt2, 14, 20, false, true, HA.L);
                rpt.ridx++;
                rpt.ridx++;

                if (staffCount > 0)
                {
                    AppendStaffBlock(cfg, sheet, rpt, org);
                    rpt.ridx++;
                    rpt.ridx++;
                }

                AppendPlayerBlock(cfg, sheet, rpt, org);
                rpt.ridx++;

                no++;
            }

            sheet.Columns[8].AutoFitColumns();
        }

        private void AppendPlayerBlock(TConfig cfg, Spire.Xls.Worksheet sheet, TReport rpt, TOrg org)
        {
            var titlePos = "A" + rpt.ridx + ":" + "B" + rpt.ridx;
            var titleTxt = "選手:";
            SetStrCell(cfg, sheet, titlePos, titleTxt, 14, 20, false, true, HA.C);
            rpt.ridx++;

            var groups = GetPlayerGroupMap(org.players);
            foreach (var kv in groups)
            {
                var in_l1 = kv.Key;
                var players = kv.Value;

                var groupPos = "A" + rpt.ridx + ":" + "C" + rpt.ridx;
                //var groupTxt = in_l1 + " " + "自由式";
                var groupTxt = in_l1;
                SetStrCell(cfg, sheet, groupPos, groupTxt, 14, 20, false, true, HA.L);

                var cn = 6;
                var rc = players.Count % cn == 0 ? players.Count / cn : (players.Count / cn) + 1;
                var mx = players.Count - 1;
                var arr = new string[] { "D", "E", "F", "G", "H", "I" };
                for (var i = 0; i < rc; i++)
                {
                    for (var j = 0; j < cn; j++)
                    {
                        var idx = i * 6 + j;
                        if (idx > mx) continue;

                        var player = players[idx];
                        var playerPos = arr[j] + rpt.ridx;
                        var playerTxt = player.in_name;
                        SetNameCell(cfg, sheet, playerPos, playerTxt, 14, 20, false, false, HA.L);
                    }
                    rpt.ridx++;
                }
            }
        }

        private void AppendStaffBlock(TConfig cfg, Spire.Xls.Worksheet sheet, TReport rpt, TOrg org)
        {
            var has2Rows = false;
            if (org.leaders.Count > 0)
            {
                var staffPos1 = "A" + rpt.ridx;
                var staffTxt1 = "領隊:";
                SetStrCell(cfg, sheet, staffPos1, staffTxt1, 14, 0, false, false, HA.L);

                var staffPos2 = "B" + rpt.ridx;
                var staffTxt2 = org.leaders[0].in_name;
                SetNameCell(cfg, sheet, staffPos2, staffTxt2, 14, 0, false, false, HA.L);

                if (org.leaders.Count > 1)
                {
                    var staffPos3 = "B" + (rpt.ridx + 1);
                    var staffTxt3 = org.leaders[1].in_name;
                    SetNameCell(cfg, sheet, staffPos3, staffTxt3, 14, 0, false, false, HA.L);
                    has2Rows = true;
                }
            }

            if (org.managers.Count > 0)
            {
                var staffPos1 = "H" + rpt.ridx;
                var staffTxt1 = "管理:";
                SetStrCell(cfg, sheet, staffPos1, staffTxt1, 14, 0, false, false, HA.L);

                var staffPos2 = "I" + rpt.ridx;
                var staffTxt2 = org.managers[0].in_name;
                SetNameCell(cfg, sheet, staffPos2, staffTxt2, 14, 0, false, false, HA.L);

                if (org.leaders.Count > 1)
                {
                    var staffPos3 = "I" + (rpt.ridx + 1);
                    var staffTxt3 = org.managers[1].in_name;
                    SetNameCell(cfg, sheet, staffPos3, staffTxt3, 14, 0, false, false, HA.L);
                    has2Rows = true;
                }
            }

            if (org.coaches_m.Count > 0 && org.coaches_w.Count > 0)
            {
                var staffPos1 = "D" + rpt.ridx;
                var staffTxt1 = "男子教練:";
                SetStrCell(cfg, sheet, staffPos1, staffTxt1, 14, 0, false, false, HA.R);

                var staffPos2 = "E" + rpt.ridx;
                var staffTxt2 = org.coaches_m[0].in_name;
                SetNameCell(cfg, sheet, staffPos2, staffTxt2, 14, 0, false, false, HA.L);

                if (org.coaches_m.Count > 1)
                {
                    var staffPos3 = "F" + rpt.ridx;
                    var staffTxt3 = org.coaches_m[1].in_name;
                    SetNameCell(cfg, sheet, staffPos3, staffTxt3, 14, 0, false, false, HA.L);
                }

                var staffPosW1 = "D" + (rpt.ridx + 1);
                var staffTxtW1 = "女子教練:";
                SetStrCell(cfg, sheet, staffPosW1, staffTxtW1, 14, 0, false, false, HA.R);

                var staffPosW2 = "E" + (rpt.ridx + 1);
                var staffTxtW2 = org.coaches_w[0].in_name;
                SetNameCell(cfg, sheet, staffPosW2, staffTxtW2, 14, 0, false, false, HA.L);

                if (org.coaches_w.Count > 1)
                {
                    var staffPosW3 = "F" + (rpt.ridx + 1);
                    var staffTxtW3 = org.coaches_w[1].in_name;
                    SetNameCell(cfg, sheet, staffPosW3, staffTxtW3, 14, 0, false, false, HA.L);
                }
                has2Rows = true;
            }
            else if (org.coaches_m.Count > 0)
            {
                var staffPos1 = "D" + rpt.ridx;
                var staffTxt1 = "男子教練:";
                SetStrCell(cfg, sheet, staffPos1, staffTxt1, 14, 0, false, false, HA.R);

                var staffPos2 = "E" + rpt.ridx;
                var staffTxt2 = org.coaches_m[0].in_name;
                SetNameCell(cfg, sheet, staffPos2, staffTxt2, 14, 0, false, false, HA.L);

                if (org.coaches_m.Count > 1)
                {
                    var staffPos3 = "F" + rpt.ridx;
                    var staffTxt3 = org.coaches_m[1].in_name;
                    SetNameCell(cfg, sheet, staffPos3, staffTxt3, 14, 0, false, false, HA.L);
                }
            }
            else if (org.coaches_w.Count > 0)
            {
                var staffPos1 = "D" + rpt.ridx;
                var staffTxt1 = "女子教練:";
                SetStrCell(cfg, sheet, staffPos1, staffTxt1, 14, 0, false, false, HA.R);

                var staffPos2 = "E" + rpt.ridx;
                var staffTxt2 = org.coaches_w[0].in_name;
                SetNameCell(cfg, sheet, staffPos2, staffTxt2, 14, 0, false, false, HA.L);

                if (org.coaches_w.Count > 1)
                {
                    var staffPos3 = "F" + rpt.ridx;
                    var staffTxt3 = org.coaches_w[1].in_name;
                    SetNameCell(cfg, sheet, staffPos3, staffTxt3, 14, 0, false, false, HA.L);
                }
            }
            if (has2Rows) rpt.ridx++;
        }

        private void SetNameCell(TConfig cfg, Spire.Xls.Worksheet sheet
            , string pos
            , string text
            , int size
            , int height = 0
            , bool needBold = false
            , bool needMerge = false
            , HA ha = HA.C)
        {
            var range = sheet.Range[pos];
            if (needMerge) range.Merge();
            range.Text = text;
            var newSize = size;
            if (text.Length == 4)
            {
                newSize = 12;
            }
            else if (text.Length >= 5)
            {
                newSize = 9;
            }
            SetUtlCell(cfg, range, newSize, height, needBold, ha);
        }

        private void SetColorHead(TConfig cfg, Spire.Xls.Worksheet sheet
            , string pos
            , string text
            , int size
            , int height = 0
            , bool needBold = false
            , bool needMerge = false
            , HA ha = HA.C)
        {
            var range = sheet.Range[pos];
            if (needMerge) range.Merge();
            range.Style.Color = cfg.HeadColor;
            range.Text = text;
            SetUtlCell(cfg, range, size, height, needBold, ha);
        }

        private void SetStrCell(TConfig cfg, Spire.Xls.Worksheet sheet
            , string pos
            , string text
            , int size
            , int height = 0
            , bool needBold = false
            , bool needMerge = false
            , HA ha = HA.C)
        {
            var range = sheet.Range[pos];
            if (needMerge) range.Merge();
            range.Text = text;
            SetUtlCell(cfg, range, size, height, needBold, ha);
        }

        private void SetIntCell(TConfig cfg, Spire.Xls.Worksheet sheet
            , string pos
            , int text
            , int size
            , int height = 0
            , bool needBold = false
            , bool needMerge = false
            , HA ha = HA.C)
        {
            var range = sheet.Range[pos];
            if (needMerge) range.Merge();
            range.NumberValue = text;
            range.NumberFormat = "0";
            SetUtlCell(cfg, range, size, height, needBold, ha);
        }

        private void SetUtlCell(TConfig cfg, Spire.Xls.CellRange range
            , int size
            , int height
            , bool needBold
            , HA ha = HA.C)
        {
            range.Style.Font.FontName = cfg.FontName;
            range.Style.Font.Size = size;

            if (needBold) range.Style.Font.IsBold = true;
            if (height > 0) range.RowHeight = height;

            range.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;

            switch (ha)
            {
                case HA.C:
                    range.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case HA.L:
                    range.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;
                    break;
                case HA.R:
                    range.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                    break;
            }
        }

        private enum HA
        {
            L = 100,
            C = 200,
            R = 300
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        /// <summary>
        /// 取得與會者資料
        /// </summary>
        private Item GetMeetingUsers(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.in_l1
                    , t1.in_l2
                    , t1.in_l3
                    , t1.in_index
                    , ISNULL(t1.in_l1, '') + '-' + ISNULL(t1.in_l2, '') + '-' + ISNULL(t1.in_l3, '') + '-' + ISNULL(t1.in_index, '') + '-' + ISNULL(t1.in_creator_sno, '') AS 'in_team_key'
                    , t1.in_l1 + '-' + t1.in_l3 AS 'program_name'
                    , t1.in_current_org
                    , t1.in_team
                    , t1.in_name
                    , t1.in_sno
                    , t1.in_gender
                    , t1.in_birth
                    , t1.in_creator
                    , t1.in_creator_sno
                    , t2.in_tel AS 'in_creator_tel'
                    , t1.in_expense
                    , t1.in_paynumber
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, t1.in_birth), 111) AS 'reg_birth'
                    , t1.in_emrg_contact1
                    , t1.in_emrg_relation1
					, t1.in_stuff_b1
					, t2.in_email AS 'creator_email'
					, t2.in_tel   AS 'creator_tel'
                    , t3.pay_bool
                    , t3.in_pay_amount_exp
                    , t3.in_pay_amount_real
					, t4.in_type
                    , t4.in_short_org AS 'map_org_name'
                    , t5.sort_order   AS 'program_sort'
                FROM 
                    IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_RESUME t2 WITH(NOLOCK)
                    ON t2.in_sno = t1.in_creator_sno
                LEFT OUTER JOIN
                    IN_MEETING_PAY t3 WITH(NOLOCK)
                    ON t3.in_meeting = t1.source_id 
                    AND t3.item_number = t1.in_paynumber
                LEFT OUTER JOIN
                    IN_ORG_MAP t4 WITH(NOLOCK)
                    ON t4.in_stuff_b1 = t1.in_stuff_b1 
				LEFT OUTER JOIN
					VU_MEETING_SVY_L3 t5
					ON t5.source_id = t1.source_id
					AND t5.in_grand_filter = t1.in_l1
					AND t5.in_filter = t1.in_l2
					AND t5.in_value = t1.in_l3
                WHERE 
                    t1.source_id = '{#meeting_id}'
                ORDER BY 
                    t5.sort_order
					, t1.in_stuff_b1
					, t1.in_name
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            var items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                throw new Exception("查無與會者資料");
            }

            return items;
        }

        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>" + in_name + "</in_name>");
            string Export_Path = itmXls.getProperty("export_path", "");
            string Template_Path = itmXls.getProperty("template_path", "");

            TExport result = new TExport
            {
                template_Path = Template_Path,
                export_Path = Export_Path,
            };

            return result;
        }

        private List<TOrg> MapOrgList(TConfig cfg, List<TPerson> rows)
        {
            var orgs = new List<TOrg>();
            for (int i = 0; i < rows.Count; i++)
            {
                var p = rows[i];
                var org = orgs.Find(x => x.in_stuff_b1 == p.in_stuff_b1);
                if (org == null)
                {
                    org = new TOrg
                    {
                        in_type = p.in_type,
                        in_stuff_b1 = p.in_stuff_b1,
                        in_current_org = p.in_current_org,
                        in_short_org = p.map_org_name,
                        in_name = p.creator_name,
                        in_email = p.creator_email,
                        in_tel = p.creator_tel,
                        leaders = new List<TPerson>(),
                        managers = new List<TPerson>(),
                        coaches_m = new List<TPerson>(),
                        coaches_w = new List<TPerson>(),
                        insurances = new List<TPerson>(),
                        players = new List<TPerson>(),
                    };
                    orgs.Add(org);
                }
                AnalysisMUser(cfg, org, p);
            }
            return orgs;
        }

        private void AnalysisMUser(TConfig cfg, TOrg org, TPerson p)
        {
            if (p.isPlayer)
            {
                org.players.Add(p);
            }
            else if (p.isInsurance)
            {
                org.insurances.Add(p);
            }
            else if (p.isLeader)
            {
                org.hasStaff = true;
                org.leaders.Add(p);
            }
            else if (p.isManager)
            {
                org.hasStaff = true;
                org.managers.Add(p);
            }
            else if (p.isCoachM)
            {
                org.hasStaff = true;
                org.coaches_m.Add(p);
            }
            else if (p.isCoachW)
            {
                org.hasStaff = true;
                org.coaches_w.Add(p);
            }
        }

        private List<TPerson> MapMUserList(TConfig cfg, Item items)
        {
            var list = new List<TPerson>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                list.Add(MapMUser(cfg, item));
            }
            return list;
        }

        private TPerson MapMUser(TConfig cfg, Item item)
        {
            var x = new TPerson
            {
                item = item,
                in_type = item.getProperty("in_type", ""),
                in_stuff_b1 = item.getProperty("in_stuff_b1", ""),
                in_current_org = item.getProperty("in_current_org", ""),
                map_org_name = item.getProperty("map_org_name", ""),
                in_l1 = item.getProperty("in_l1", ""),
                in_l2 = item.getProperty("in_l2", ""),
                in_l3 = item.getProperty("in_l3", ""),
                in_name = item.getProperty("in_name", ""),
                in_sno = item.getProperty("in_sno", ""),
                reg_birth = GetDtm(item.getProperty("reg_birth", "")),
                in_gender = item.getProperty("in_gender", ""),
                in_emrg_contact1 = item.getProperty("in_emrg_contact1", ""),
                in_emrg_relation1 = item.getProperty("in_emrg_relation1", ""),
                creator_name = item.getProperty("in_creator", ""),
                creator_email = item.getProperty("creator_email", ""),
                creator_tel = item.getProperty("creator_tel", ""),
                program_name = item.getProperty("program_name", "").Replace("自由式-", ""),
                program_sort = GetInt(item.getProperty("program_sort", "0")),
            };

            if (x.reg_birth != DateTime.MinValue)
            {
                x.wsBirth = x.reg_birth.ToString("yyyy年MM月dd日");
                x.twBirth = (x.reg_birth.Year - 1911) + "年" + x.reg_birth.ToString("MM月dd日");
            }

            switch (x.in_l1)
            {
                case "隊職員":
                    x.isStaff = true;
                    AnalysisStaff(cfg, x);
                    x.groupName = x.in_l1;
                    x.sheetName = x.in_l1;
                    break;
                case "保險代表人":
                    x.isInsurance = true;
                    x.groupName = x.in_l1;
                    x.sheetName = x.in_l1;
                    break;
                default:
                    x.isPlayer = true;
                    AnalysisPlayer(cfg, x);
                    break;
            }
            x.item.setProperty("groupName", x.groupName);
            return x;
        }

        private void AnalysisPlayer(TConfig cfg, TPerson x)
        {
            if (x.in_l1.Contains("大專"))
            {
                x.groupName = "大專社會組";
            }
            else if (x.in_l1.Contains("高中"))
            {
                x.groupName = "高中組";
            }
            else if (x.in_l1.Contains("國中"))
            {
                x.groupName = "國中組";
            }
            else if (x.in_l1.Contains("國小"))
            {
                x.groupName = "國小組";
            }
            else
            {
                x.groupName = x.in_l1;
            }
            x.sheetName = x.groupName + "籤表";

        }
        private void AnalysisStaff(TConfig cfg, TPerson x)
        {
            switch (x.in_l2)
            {
                case "領隊":
                    x.isLeader = true;
                    break;
                case "管理":
                    x.isManager = true;
                    break;
                case "男教練":
                case "男子隊教練":
                    x.isCoachM = true;
                    break;
                case "女教練":
                case "女子隊教練":
                    x.isCoachW = true;
                    break;
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }

            public System.Drawing.Color HeadColor { get; set; }
            public string FontName { get; set; }
        }

        private class TExport
        {
            public string template_Path { get; set; }
            public string export_Path { get; set; }
        }

        private class TOrg
        {
            public string in_type { get; set; }
            public string in_stuff_b1 { get; set; }
            public string in_current_org { get; set; }
            public string in_short_org { get; set; }
            public string in_name { get; set; }
            public string in_email { get; set; }
            public string in_tel { get; set; }
            public List<TPerson> leaders { get; set; }
            public List<TPerson> managers { get; set; }
            public List<TPerson> coaches_m { get; set; }
            public List<TPerson> coaches_w { get; set; }
            public List<TPerson> insurances { get; set; }
            public List<TPerson> players { get; set; }
            public bool hasStaff { get; set; }
        }

        private class TPerson
        {
            public bool isStaff { get; set; }
            public bool isLeader { get; set; }
            public bool isManager { get; set; }
            public bool isCoachM { get; set; }
            public bool isCoachW { get; set; }
            public bool isPlayer { get; set; }
            public bool isInsurance { get; set; }
            public string groupName { get; set; }
            public string sheetName { get; set; }

            public string in_type { get; set; }
            public string in_stuff_b1 { get; set; }
            public string in_current_org { get; set; }
            public string map_org_name { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string program_name { get; set; }
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_gender { get; set; }
            public string in_emrg_contact1 { get; set; }
            public string in_emrg_relation1 { get; set; }
            public string creator_name { get; set; }
            public string creator_email { get; set; }
            public string creator_tel { get; set; }
            public DateTime reg_birth { get; set; }
            public Item item { get; set; }

            public string wsBirth { get; set; }
            public string twBirth { get; set; }
            public int program_sort { get; set; }

        }

        private class TReport
        {
            public int ridx { get; set; }
        }

        private Dictionary<string, List<TPerson>> GetPlayerGroupMap(List<TPerson> rows)
        {
            var map = new Dictionary<string, List<TPerson>>();
            for (var i = 0; i < rows.Count; i++)
            {
                var x = rows[i];
                var group = default(List<TPerson>);
                if (map.ContainsKey(x.in_l1))
                {
                    group = map[x.in_l1];
                }
                else
                {
                    group = new List<TPerson>();
                    map.Add(x.in_l1, group);
                }
                group.Add(x);
            }
            return map;
        }

        private string FixSectName(string value)
        {
            var resule = "";
            var arr = value.Split(new char[] { '：' }, StringSplitOptions.RemoveEmptyEntries);
            var w1 = arr.Length > 0 ? arr[0] : "";
            var w2 = arr.Length > 1 ? arr[1] : "";

            if (w2.Contains("以上至"))
            {
                w2 = w2.Replace(".1公斤以上至", "~");
                w2 = w2.Replace(".0公斤以下", "");
                var arr2 = w2.Split(new char[] { '~' }, StringSplitOptions.RemoveEmptyEntries);
                var weight = arr2.Length > 1 ? arr2[1] : "";
                resule = w1.Replace("-", "-自由式 : ") + "(-" + weight + "公斤)";
            }
            else
            {
                w2 = "~" + w2.Replace(".0公斤以下", "");
                resule = w1.Replace("-", "-自由式 : ") + "(-" + w2 + "公斤)";
            }
            return resule;
        }

        private int GetInt(string value, int def = 0)
        {
            if (value == "") return 0;
            int result = def;
            int.TryParse(value, out result);
            return result;
        }

        private DateTime GetDtm(string value)
        {
            if (value == "") return DateTime.MinValue;
            DateTime dt;
            if (DateTime.TryParse(value, out dt)) return dt;
            return DateTime.MinValue;
        }
    }
}