﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using Aras.Server.Core;
using Spire.Doc;

namespace WorkHelp.ArasDesk.Methods.Judo.種子籤
{
    public class In_Meeting_Seeds : Item
    {
        public In_Meeting_Seeds(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;            /*
                目的: 建立種子籤表
                日誌: 
                    - 2024-10-21: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_Seeds";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                in_old_source = "BCA302C39E264A2B84D062AAA880B022", //中華民國113年全國中等學校運動會
                in_new_source = "95C1607DDA434EFD9BE95FDC042A49D2", //中華民國柔道總會 113 年全國中正盃柔道錦標賽
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "edit-seed":
                    EditSeed(cfg, itmR);
                    break;

                case "rank-page":
                    RankPage(cfg, itmR);
                    break;

                case "analysis":
                    AnalysisSeeds(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void EditSeed(TConfig cfg, Item itmReturn)
        {
            var sql = "";
            var id = itmReturn.getProperty("id", "");
            var in_final_seed = itmReturn.getProperty("in_final_seed", "");

            sql = "SELECT * FROM IN_MEETING_SEEDS WITH(NOLOCK) WHERE id = '" + id + "'";
            var itmSeed = cfg.inn.applySQL(sql);
            if (itmSeed.isError() || itmSeed.getResult() == "") throw new Exception("查無種子籤資料");

            var in_new_source = itmSeed.getProperty("in_new_source", "");
            var in_new_muid = itmSeed.getProperty("in_new_muid", "");
            var in_new_l1 = itmSeed.getProperty("in_new_l1", "");
            var in_new_l2 = itmSeed.getProperty("in_new_l2", "");
            var in_new_l3 = itmSeed.getProperty("in_new_l3", "");

            var in_match_yn = itmSeed.getProperty("in_match_yn", "");
            if (in_match_yn != "Y") throw new Exception("不匹配，無法更新種子籤");

            sql = "SELECT id, in_index, in_creator_sno FROM IN_MEETING_USER WITH(NOLOCK) WHERE id = '" + in_new_muid + "'";
            var itmMUser = cfg.inn.applySQL(sql);
            if (itmMUser.isError() || itmMUser.getResult() == "") throw new Exception("查無與會者資料");

            sql = "SELECT id FROM IN_MEETING_PROGRAM WITH(NOLOCK)"
                + " WHERE in_meeting = '" + in_new_source + "'"
                + " AND in_l1 = N'" + in_new_l1 + "'"
                + " AND in_l2 = N'" + in_new_l2 + "'"
                + " AND in_l3 = N'" + in_new_l3 + "'"
                ;
            Item itmProgram = cfg.inn.applySQL(sql);
            if (itmProgram.isError() || itmProgram.getResult() == "") throw new Exception("查無組別資料");

            var program_id = itmProgram.getProperty("id", "");
            var in_index = itmMUser.getProperty("in_index", "");
            var in_creator_sno = itmMUser.getProperty("in_creator_sno", "");

            sql = "SELECT id FROM IN_MEETING_PTEAM WITH(NOLOCK)"
                + " WHERE source_id = '" + program_id + "'"
                + " AND in_index = '" + in_index + "'"
                + " AND in_creator_sno = '" + in_creator_sno + "'"
                ;
            Item itmPTeam = cfg.inn.applySQL(sql);
            if (itmPTeam.isError() || itmPTeam.getResult() == "") throw new Exception("查無參賽資料");

            var team_id = itmPTeam.getProperty("id", "");

            var sql_upd = @"
                UPDATE 
                    IN_MEETING_SEEDS 
                SET 
                    in_final_seed = '{#in_final_seed}'
                WHERE
                    id = '{#id}'
            ";

            sql_upd = sql_upd.Replace("{#id}", id)
                .Replace("{#in_final_seed}", in_final_seed);

            cfg.inn.applySQL(sql_upd);

            var itmData = cfg.inn.newItem("In_Meeting_PTeam");
            itmData.setProperty("program_id", program_id);
            itmData.setProperty("team_id", team_id);
            itmData.setProperty("exe_type", "seed");
            itmData.setProperty("in_seeds", in_final_seed);
            itmData.apply("in_meeting_draw_update");
        }

        private void RankPage(TConfig cfg, Item itmReturn)
        {
            var items = GetSeedItems(cfg);

            var sectMap = MapSectList(cfg, items);
            var sects = sectMap.Values.ToList();

            var builder = new StringBuilder();
            for (var i = 0; i < sects.Count; i++)
            {
                var sect = sects[i];
                AppendSectTable(cfg, sect, sect.items, (i + 1), builder);
            }

            itmReturn.setProperty("inn_table", builder.ToString());
        }

        private void AppendSectTable(TConfig cfg, TSect sect, List<Item> items, int index, StringBuilder builder)
        {
            var cssH = "class='text-center'";
            var cssB = "class='text-center'";

            var head = new StringBuilder();
            head.Append("<tr>");
            head.Append("<th " + cssH + ">113全中運<BR>組別</th>");
            head.Append("<th " + cssH + ">113全中運<BR>量級</th>");
            head.Append("<th " + cssH + ">113全中運<BR>名次</th>");
            head.Append("<th " + cssH + ">選手<BR>中正盃</th>");
            head.Append("<th " + cssH + ">113中正盃<BR>組別</th>");
            head.Append("<th " + cssH + ">113中正盃<BR>量級</th>");
            head.Append("<th " + cssH + ">勝場數</th>");
            head.Append("<th " + cssH + ">輸金輸銀</th>");
            head.Append("<th " + cssH + ">匹配</th>");
            head.Append("<th " + cssH + ">種子籤</th>");
            head.Append("<th " + cssH + ">抽籤</th>");
            head.Append("</tr>");

            var body = new StringBuilder();
            for (var i = 0; i < items.Count; i++)
            {
                var item = items[i];
                var id = item.getProperty("id", "");
                var meeting_id = item.getProperty("meeting_id", "");
                var program_id = item.getProperty("program_id", "");
                var in_l2 = item.getProperty("in_l2", "");
                var in_l3 = item.getProperty("in_l3", "");
                var in_name = item.getProperty("in_name", "");
                var in_final_rank = item.getProperty("in_final_rank", "");
                var in_match_yn = item.getProperty("in_match_yn", "");
                var in_final_seed = item.getProperty("in_final_seed", "");
                var in_new_meeting = item.getProperty("in_new_source", "");
                var in_new_l1 = item.getProperty("in_new_l1", "");
                var in_new_l2 = item.getProperty("in_new_l2", "");
                var in_new_l3 = item.getProperty("in_new_l3", "");
                var in_new_name = item.getProperty("in_new_name", "");

                if (in_new_l2 == "") in_new_l2 = "<span class='text-danger'>查無報名資料</span>";
                if (!in_new_name.Contains(in_name)) in_name += "<BR>/" + in_new_name;

                var clear_l3 = ClearL3(in_l3);
                var in_win_count = "";
                var in_loss_rank = "";

                if (in_final_rank == "3")
                {
                    in_win_count = item.getProperty("in_win_count", "0");
                    in_loss_rank = item.getProperty("in_loss_rank", "0");
                    if (in_loss_rank == "1") in_loss_rank = "輸金牌";
                    else if (in_loss_rank == "2") in_loss_rank = "輸銀牌";
                    else in_loss_rank = "未對上";
                }

                var link1 = " <a style='display:inline;' href='javascript:;'"
                    + " data-mtid='" + meeting_id + "'"
                    + " data-pgid='" + program_id + "'"
                    + " onclick='GoFightTreePage(this)'><i class='fa fa-link'></i></a>";

                var link2 = "";
                if (in_match_yn == "Y")
                {
                    link2 = " <a style='display:inline;' href='javascript:;'"
                        + " data-mtid='" + in_new_meeting + "'"
                        + " data-l1='" + in_new_l1 + "'"
                        + " data-l2='" + in_new_l2 + "'"
                        + " data-l3='" + in_new_l3 + "'"
                        + " onclick='GoDrawPage(this)'><i class='fa fa-link'></i></a>";
                }

                var seedCss = in_final_seed != "" && in_final_seed != "0" ? "background-color: #B7D1FC" : "";
                var input = "<input type='text' class='text' style='text-align: right; width: 60px; " + seedCss + "'"
                    + " data-id='" + id + "'"
                    + " data-old='" + in_final_seed + "'"
                    + " value='" + in_final_seed + "'"
                    + " onkeyup='doUpdateSeed(this)'>";

                body.Append("<tr>");
                body.Append("<td " + cssB + ">" + in_l2.Replace("個-", "") + "</td>");
                body.Append("<td " + cssB + ">" + clear_l3 + link1 + "</td>");
                body.Append("<td " + cssB + ">" + in_final_rank + "</td>");
                body.Append("<td " + cssB + ">" + in_name + "</td>");
                body.Append("<td " + cssB + ">" + in_new_l2.Replace("個-", "") + "</td>");
                body.Append("<td " + cssB + ">" + in_new_l3 + "</td>");
                body.Append("<td " + cssB + ">" + in_win_count + "</td>");
                body.Append("<td " + cssB + ">" + in_loss_rank + "</td>");
                body.Append("<td " + cssB + ">" + in_match_yn + "</td>");
                body.Append("<td " + cssB + ">" + input + "</td>");
                body.Append("<td " + cssB + ">" + link2 + "</td>");
                body.Append("</tr>");
            }

            var table_name = "data-table-" + index;
            builder.Append("<div class='box-body tablenobg' style='position: relative'>");
            builder.AppendLine(GetTableAttribute(table_name));
            builder.AppendLine("<thead>");
            builder.Append(head);
            builder.AppendLine("</thead>");
            builder.AppendLine("<tbody>");
            builder.Append(body);
            builder.AppendLine("</tbody>");
            builder.AppendLine("</table>");
            builder.Append("</div>");
        }

        private string ClearL3(string in_l3)
        {
            var arr = in_l3.Split(new char[] { '：' }, StringSplitOptions.RemoveEmptyEntries);
            var w1 = arr != null && arr.Length > 0 ? arr[0] : "";
            var w2 = arr != null && arr.Length > 1 ? arr[1] : "";
            var v2 = "";
            if (w2.Contains(".0公斤以下"))
            {
                v2 = "-" + w2.Replace(".0公斤以下", "") + "Kg";
            }
            else if (w2.Contains(".1公斤以上"))
            {
                v2 = "+" + w2.Replace(".1公斤以上", "") + "Kg";
            }
            else
            {
                var arr2 = w2.Split(new char[] { '至' }, StringSplitOptions.RemoveEmptyEntries);
                var w21 = arr2 != null && arr2.Length > 0 ? arr2[0] : "";
                var w22 = arr2 != null && arr2.Length > 1 ? arr2[1] : "";
                v2 = "-" + w22.Replace(".0公斤", "") + "Kg";
            }
            return w1 + "：" + v2;
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "'"
                + " class='table table-hover table-striped'"
                + " data-toggle='table'"
                + " data-show-toggle='false'"
                + " data-show-columns='false'"
                + " data-search-align='left'"
                + " data-sort-stable='true'"
                + " data-search='false'"
                + ">";
        }

        private Item GetSeedItems(TConfig cfg)
        {
            var sql = @"

                SELECT
                    t1.id
                    , t1.source_id						AS 'meeting_id'
                    , t1.in_program					AS 'program_id'
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_name
	                , t1.in_final_rank
	                , t1.in_new_l3
                    , t1.in_match_yn
                    , t1.in_win_count
                    , t1.in_loss_rank
                    , t1.in_final_seed
                    , t1.in_note
					, t1.in_new_source
					, t1.in_new_l1
					, t1.in_new_l2
					, t1.in_new_l3
					, t1.in_new_name
					, t2.id AS 'in_new_program'
                FROM 
	                IN_MEETING_SEEDS t1 WITH(NOLOCK)
				LEFT OUTER JOIN
					IN_MEETING_PROGRAM t2 WITH(NOLOCK)
					ON t2.in_meeting = t1.in_new_source
					AND t2.in_l1 = t1.in_new_l1
					AND t2.in_l2 = t1.in_new_l2
					AND t2.in_l3 = t1.in_new_l3
				WHERE
					t1.source_id = '{#meeting_id}'
                ORDER BY
	                t1.in_sort_order
	                , t1.in_final_rank
	                , t1.in_win_count DESC
	                , t1.in_loss_rank
            ";

            sql = sql.Replace("{#meeting_id}", cfg.in_old_source);

            return cfg.inn.applySQL(sql);
        }

        private void AnalysisSeeds(TConfig cfg, Item itmReturn)
        {
            var items = GetRankItems(cfg);
            var sectMap = MapSectList(cfg, items);
            RemoveMeetingSeeds(cfg);
            CreateMeetingSeeds(cfg, sectMap.Values.ToList());
        }

        private void CreateMeetingSeeds(TConfig cfg, List<TSect> sects)
        {
            for (var i = 0; i < sects.Count; i++)
            {
                var sect = sects[i];
                for (var j = 0; j < sect.items.Count; j++)
                {
                    var item = sect.items[j];
                    NewMeetingSeeds(cfg, sect, item);
                }
            }
        }

        private void NewMeetingSeeds(TConfig cfg, TSect sect, Item item)
        {
            var in_name = item.getProperty("in_name", "");
            var in_sno = item.getProperty("in_sno", "");
            var in_final_rank = item.getProperty("in_final_rank", "");
            var in_sign_no = item.getProperty("in_sign_no", "");
            var in_win_count = GetWinEventCount(cfg, sect.program_id, in_sign_no);
            var in_loss_rank = "0";
            var in_note = "";
            var in_match_key = GetMatchKey(cfg, item);
            var in_match_yn = "";

            if (in_final_rank == "3")
            {
                var a = GetLossRank(cfg, sect.program_id, in_sign_no, sect.rank1_sign_no);
                var b = GetLossRank(cfg, sect.program_id, in_sign_no, sect.rank2_sign_no);
                if (a != "")
                {
                    in_loss_rank = "1";
                }
                else if (b != "")
                {
                    in_loss_rank = "2";
                }
            }

            var itmMUsers = GetMeetingUserItems(cfg, in_name, in_sno);
            var reg_count = itmMUsers.getItemCount();
            if (reg_count <= 0)
            {
                itmMUsers = GetMeetingUserItems2(cfg, in_name, in_sno);
                reg_count = itmMUsers.getItemCount();
            }

            if (reg_count <= 0)
            {
                in_note = "查無報名資料";
            }
            else if (reg_count > 1)
            {
                in_note = "報名組數異常: " + reg_count;
            }

            var itmNew = cfg.inn.newItem("In_Meeting_Seeds", "add");
            itmNew.setProperty("source_id", cfg.in_old_source);
            itmNew.setProperty("in_program", sect.program_id);
            itmNew.setProperty("in_name", in_name);
            itmNew.setProperty("in_sort_order", sect.in_sort_order);
            itmNew.setProperty("in_l1", item.getProperty("in_l1", ""));
            itmNew.setProperty("in_l2", item.getProperty("in_l2", ""));
            itmNew.setProperty("in_l3", item.getProperty("in_l3", ""));
            itmNew.setProperty("in_final_rank", in_final_rank);
            itmNew.setProperty("in_win_count", in_win_count.ToString());
            itmNew.setProperty("in_loss_rank", in_loss_rank);

            if (reg_count == 1)
            {
                var itmMUser = itmMUsers.getItemByIndex(0);
                itmNew.setProperty("in_new_source", itmMUser.getProperty("source_id", ""));
                itmNew.setProperty("in_new_muid", itmMUser.getProperty("id", ""));
                itmNew.setProperty("in_new_l1", itmMUser.getProperty("in_l1", ""));
                itmNew.setProperty("in_new_l2", itmMUser.getProperty("in_l2", ""));
                itmNew.setProperty("in_new_l3", itmMUser.getProperty("in_l3", ""));
                itmNew.setProperty("in_new_name", itmMUser.getProperty("in_name", ""));

                var new_match_key = GetMatchKey(cfg, itmMUser);
                if (in_match_key == new_match_key)
                {
                    in_match_yn = "Y";
                }
            }

            itmNew.setProperty("in_note", in_note);

            itmNew.setProperty("in_match_key", in_match_key);
            itmNew.setProperty("in_match_yn", in_match_yn);

            itmNew = itmNew.apply();
        }

        private string GetMatchKey(TConfig cfg, Item item)
        {
            var age = "";
            var gender = "";
            var lv = "";

            var in_l2 = item.getProperty("in_l2", "");
            var in_l3 = item.getProperty("in_l3", "");

            if (in_l2.Contains("高中")) age = "高";
            else if (in_l2.Contains("國中")) age = "國";

            if (in_l2.Contains("男")) gender = "男";
            else if (in_l2.Contains("女")) gender = "女";

            if (in_l3.Contains("第一級")) lv = "第一級";
            else if (in_l3.Contains("第二級")) lv = "第二級";
            else if (in_l3.Contains("第三級")) lv = "第三級";
            else if (in_l3.Contains("第四級")) lv = "第四級";
            else if (in_l3.Contains("第五級")) lv = "第五級";
            else if (in_l3.Contains("第六級")) lv = "第六級";
            else if (in_l3.Contains("第七級")) lv = "第七級";
            else if (in_l3.Contains("第八級")) lv = "第八級";
            else if (in_l3.Contains("第九級")) lv = "第九級";
            else if (in_l3.Contains("第十級")) lv = "第十級";

            return age + gender + "-" + lv;
        }

        private Dictionary<string, TSect> MapSectList(TConfig cfg, Item items)
        {
            var map = new Dictionary<string, TSect>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var program_id = item.getProperty("program_id", "");
                var in_sort_order = item.getProperty("in_sort_order", "");
                var in_sign_no = item.getProperty("in_sign_no", "");
                var in_final_rank = item.getProperty("in_final_rank", "");
                if (!map.ContainsKey(program_id))
                {
                    map.Add(program_id, new TSect
                    {
                        program_id = program_id,
                        in_sort_order = in_sort_order,
                        rank1_sign_no = "",
                        rank2_sign_no = "",
                        items = new List<Item>(),
                    });
                }

                var existed = map[program_id];
                existed.items.Add(item);

                if (in_final_rank == "1")
                {
                    existed.rank1_sign_no = in_sign_no;
                }
                else if (in_final_rank == "2")
                {
                    existed.rank2_sign_no = in_sign_no;
                }
            }

            return map;
        }

        private class TSect
        {
            public string program_id { get; set; }
            public string in_sort_order { get; set; }
            public string rank1_sign_no { get; set; }
            public string rank2_sign_no { get; set; }
            public List<Item> items { get; set; }
        }

        private void RemoveMeetingSeeds(TConfig cfg)
        {
            var sql = "DELETE FROM IN_MEETING_SEEDS WHERE source_id = '" + cfg.in_old_source + "'";
            cfg.inn.applySQL(sql);
        }

        private Item GetRankItems(TConfig cfg)
        {
            var sql = @"
                SELECT
                    t1.id AS 'program_id'
	                , t1.in_sort_order
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t2.in_name
	                , t2.in_sno
	                , t2.in_final_rank
					, t2.in_sign_no
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PTEAM t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t2.in_final_rank, 0) > 0
	                AND ISNULL(t2.in_final_rank, 0) <= 4
                ORDER BY
	                t1.in_sort_order
	                , t2.in_final_rank
            ";

            sql = sql.Replace("{#meeting_id}", cfg.in_old_source);

            return cfg.inn.applySQL(sql);
        }

        private int GetWinEventCount(TConfig cfg, string program_id, string in_sign_no)
        {
            var sql = @"
                SELECT 
	                t1.id
	                , t1.in_fight_id
	                , t1.in_tree_sno
	                , t1.in_win_status
	                , t1.in_win_sign_no
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                WHERE 
	                t1.source_id = '{#program_id}'
	                AND t1.in_win_sign_no = {#in_sign_no}
	                AND t1.in_win_status NOT IN ('cancel', 'bypass')
                ORDER BY
	                t1.in_tree_sno
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_sign_no}", in_sign_no);

            var items = cfg.inn.applySQL(sql);

            return items.getItemCount();
        }

        private string GetLossRank(TConfig cfg, string program_id, string in_sign_no, string in_target_no)
        {
            var sql = @"
                SELECT 
	                t1.id
	                , t1.in_fight_id
	                , t1.in_tree_no
	                , t1.in_win_status
	                , t1.in_win_sign_no
	                , t2.in_sign_foot
	                , t2.in_sign_no
	                , t2.in_target_no
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK) 
	                ON t2.source_id = t1.id
                WHERE 
	                t1.source_id = '{#program_id}'
	                AND t1.in_win_status NOT IN ('cancel', 'bypass')
	                AND t2.in_sign_no = {#in_sign_no}
	                AND t2.in_target_no = {#in_target_no}
                ORDER BY
	                t1.in_tree_no
	                , t2.in_sign_foot
            ";
            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_sign_no}", in_sign_no)
                .Replace("{#in_target_no}", in_target_no);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            if (count <= 0) return "";
            return items.getItemByIndex(0).getProperty("id", "");
        }

        private Item GetMeetingUserItems(TConfig cfg, string in_name, string in_sno)
        {
            var cond = "AND t1.in_sno = '" + in_sno + "'";
            if (in_sno == "J223282476") cond = "AND t1.in_name LIKE N'%" + in_name + "%'";
            if (in_sno == "S225846000") cond = "AND t1.in_name LIKE N'%" + in_name + "%'";

            var sql = @"
                SELECT
	                t1.source_id
	                , t1.id
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_name
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                VU_MEETING_SVY_L3 t13
	                ON t13.source_id = t1.source_id
	                AND t13.in_grand_filter = t1.in_l1
	                AND t13.in_filter = t1.in_l2
	                AND t13.in_value = t1.in_l3
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_l1 NOT IN (N'隊職員', N'團體組', N'常年會費', N'格式組')
	                AND (t1.in_l2 LIKE '%國中%' OR t1.in_l2 LIKE '%高中%')
	                {#cond}
                ";

            sql = sql.Replace("{#meeting_id}", cfg.in_new_source)
                .Replace("{#cond}", cond);

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingUserItems2(TConfig cfg, string in_name, string in_sno)
        {
            var cond = "AND t1.in_sno = '" + in_sno + "'";

            var sql = @"
                SELECT TOP 1
	                t1.source_id
	                , t1.id
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_name
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                VU_MEETING_SVY_L3 t13
	                ON t13.source_id = t1.source_id
	                AND t13.in_grand_filter = t1.in_l1
	                AND t13.in_filter = t1.in_l2
	                AND t13.in_value = t1.in_l3
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_l1 NOT IN (N'隊職員', N'團體組', N'常年會費', N'格式組')
	                AND (t1.in_l2 LIKE '%社會%' OR t1.in_l2 LIKE '%大專%')
	                {#cond}
                ";

            sql = sql.Replace("{#meeting_id}", cfg.in_new_source)
                .Replace("{#cond}", cond);

            return cfg.inn.applySQL(sql);
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string in_old_source { get; set; }
            public string in_new_source { get; set; }
            public string scene { get; set; }
        }
    }
}