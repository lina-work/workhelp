﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Leeway.Common
{
    public class In_CheckSurveyAns : Item
    {
        public In_CheckSurveyAns(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
1.取得會議Id、問卷類型、學員Id
2.以meetingid取得這筆會議的學員履歷
3.以學員履歷的in_user來取得每筆學員id(meetinguserid)
4.以meetingid、meetinguserid、surveytype取得符合這筆會議的每筆學員的問卷類型的問卷結果
5.對問卷結果的每題題目與對應的標準答案做比較
*/
            //System.Diagnostics.Debugger.Break();
            Innovator inn = this.getInnovator();

            /*
            這個method的this將會是 in_meeting
            所以可以抓到這個 in_meeting 的所有 meeting_user_resume, 然後逐條取得 meeting_user_resume的 user, 帶入 MeetingUserId
            */


            string meetingid = this.getProperty("meeting_id", "");
            string surveytype = this.getProperty("in_surveytype", "");
            string meetinguserid = this.getProperty("muid", "");

            //meetingid = "D0A61E2B1CF249D6BC1A4907764C2A16";
            //surveytype = "4";
            //meetinguserid = "6CA577EE40E4419DA516D4CDB17DA686";


            //CCO.Utilities.WriteDebug("In_CheckSurveyAns", "meetingid:" + meetingid);
            //CCO.Utilities.WriteDebug("In_CheckSurveyAns", "surveytype:" + surveytype);
            //CCO.Utilities.WriteDebug("In_CheckSurveyAns", "meetinguserid:" + meetinguserid);
            switch (surveytype)
            {
                case "2":
                case "3":
                case "4":
                case "5":
                    break;
                default:
                    /*
                        Item itmMeetingUser1 = inn.newItem("In_Meeting_User");
                        itmMeetingUser1.setProperty("meetinguserid", meetinguserid);
                        itmMeetingUser1 = itmMeetingUser1.apply("In_Update_Resume");
                        return inn.newResult("此問卷無算分數功能!");
                    */
                    return inn.newResult("");
            }

            /*
            string meetingid = "9C473A25FCFE4E028DCA45449DC2538D";
            string surveytype = "1";
            string meetinguserid = "A9BC0DDA582A411FA0476952CEE610F6";
            */

            string ResultAns = "";//答案 
            string SurveyAns = "";//標準答案 
            Item MeetingResumes = inn.newItem("In_Meeting_Resume", "get");
            MeetingResumes.setProperty("source_id", meetingid);
            MeetingResumes.setProperty("in_user", meetinguserid);
            MeetingResumes = MeetingResumes.apply();
            //CCO.Utilities.WriteDebug("In_CheckSurveyAns", "58_MeetingResumes.getItemCount():" + MeetingResumes.getItemCount());
            if (MeetingResumes.getItemCount() == 0)
            {
                Item MeetingUser = inn.getItemById("In_Meeting_User", meetinguserid);
                string MeetingUserName = MeetingUser.getProperty("in_name", "no_data");
                return inn.newError("參加者" + MeetingUserName + "沒有學員履歷!");
            }
            //CCO.Utilities.WriteDebug("In_CheckSurveyAns", "65");
            string surveyresultaml =
             @" <AML>
        <Item type='IN_MEETING_SURVEYS_RESULT' action='get' orderBy='KEYED_NAME'>
        <SOURCE_ID>[@meetingid]</SOURCE_ID>
        <IN_PARTICIPANT>[@meetinguserid]</IN_PARTICIPANT>
        <IN_SURVEYTYPE>[@surveytype]</IN_SURVEYTYPE>
        </Item>
    </AML>"
                .Replace("[@meetingid]", meetingid)
                .Replace("[@meetinguserid]", meetinguserid)
                .Replace("[@surveytype]", surveytype);
            Item MeetingSurveyResult = inn.applyAML(surveyresultaml);
            //CCO.Utilities.WriteDebug("In_CheckSurveyAns", "78_MeetingSurveyResult.getItemCount():" + MeetingSurveyResult.getItemCount());
            int Count = MeetingSurveyResult.getItemCount();
            double SurveyScores = 0;
            string SurveyidList = "";//用來存放問卷的id列表,在滿意度(surveytype為4)時會用到
                                     //	這裡宣告一個　總分數　SurveyScore
                                     //CCO.Utilities.WriteDebug("In_CheckSurveyAns", "83");
            for (int i = 0; i < Count; i++)
            {
                Item SurveyResult = MeetingSurveyResult.getItemByIndex(i);
                Item Survey = SurveyResult.getRelatedItem();
                double SurveyScore = Convert.ToDouble(Survey.getProperty("in_score", "0"));//每題題目的試題分數
                SurveyAns = Survey.getProperty("in_answer", "");
                ResultAns = SurveyResult.getProperty("in_answer", "");
                SurveyidList += ("'" + Survey.getID() + "',");
                SurveyAns = SurveyAns.Replace(" ", "");
                ResultAns = ResultAns.Replace(" ", "");
                /*
                    這裡要判斷SurveyAns與ResultAns是否相等，若字串相同，則更新In_Meeting_Surveys_result的正確與否欄位為＂１＂否則為０
                    這裡同時可以計算各個In_Meeting_Surveys_result的分數，然後累加到 SurveyScore

                    2018-12-07 新增滿意度調查可以算分數的邏輯，有填就算對,滿意度問卷的選項的第一個數字作為此題填答分數(如:@1.是@2.否)

                */

                if (string.Compare(SurveyAns, ResultAns, false) == 0)
                {
                    SurveyResult.setProperty("in_sysresult", "1");
                    SurveyScores += SurveyScore;
                }
                else
                {
                    SurveyResult.setProperty("in_sysresult", "0");
                }

                switch (SurveyAns)//如果標準答案為空，則代表不用計算分數,#evaluator目前只在技術考問卷的標準答案中用到
                {
                    case " ":
                    case "#evaluator":
                        SurveyResult.setProperty("in_sysresult", "");
                        break;
                }

                SurveyResult = SurveyResult.apply("edit");
            }
            SurveyidList = SurveyidList.TrimEnd(',');
            if (surveytype == "4" && Count > 0)
            {

                /*
                string sql =@"select count(B.id)results
                from [innovator].[IN_MEETING_SURVEYS] as A join  [innovator].[IN_SURVEY] as B on (A.RELATED_ID=B.ID)
                where A.SOURCE_ID='{#meetingid}' and B.ID in({#SurveyidList}) and IN_ANSWER IS NOT NULL and IN_ANSWER !=''
                ".Replace("{#meetingid}",meetingid).Replace("{#SurveyidList}",SurveyidList);

                Item SatisfactSurveyCount = inn.applySQL(sql);//計算此課程的滿意度問卷中正確答案不為NULL且不為空的筆數
                //CCO.Utilities.WriteDebug("In_CheckSurveyAns",sql+"\n"+SatisfactSurveyCount.ToString());
                double dblWeight=(100/Convert.ToDouble(SatisfactSurveyCount.getProperty("results","0"))/5); //每題滿意度選項的分數權重,例如,一題有五個選項,最滿意為5分,最不滿意為1分
                */

                for (int i = 0; i < Count; i++)
                {
                    double SatisfactScore = 0;
                    Item SurveyResult = MeetingSurveyResult.getItemByIndex(i);
                    Item Survey = SurveyResult.getRelatedItem();
                    if (Survey.getProperty("in_answer", "") != "" && SurveyResult.getProperty("in_answer") != "")
                    {
                        if (Double.TryParse(SurveyResult.getProperty("in_answer", "").Split('.')[0], out SatisfactScore))
                        {
                            SurveyScores += SatisfactScore;
                            SurveyResult.setProperty("in_sysresult", "1");
                        }
                        else
                        {
                            //填答結果的開頭不是數字
                            /*
                            string msg ="";
                            msg += "課程id:" + meetingid + "\n";
                            msg += "學員id:" + meetinguserid + "\n";
                            msg += "出錯的滿意度問卷題目:" + Survey.getProperty("in_questions","") + "\n";
                            msg += "出錯的漫意度問卷填答結果:" + SurveyResult.getProperty("in_answer", "") + "\n";
                            //CCO.Utilities.WriteDebug("In_CheckSurveyAns",msg);
                            */
                            throw new Exception(SurveyResult.getProperty("in_answer", "") + "無題號");
                        }

                    }
                    SurveyResult = SurveyResult.apply("edit");
                }
            }
            //CCO.Utilities.WriteDebug("In_CheckSurveyAns", "167");


            /*
            單一人員的所有問題都跑完之後,也獲得了 SurveyScore 總分數　SurveyScore
            將 SurveyScore 總分更新到 meeting_user_resume 的 分數欄位上
            */
            if (Count > 0)
            {
                switch (surveytype)
                {
                    case "3":
                        MeetingResumes.setProperty("in_technical", SurveyScores.ToString());//將分數回寫到學員履歷的術科分數
                        MeetingResumes.setProperty("in_stat_4", "O");
                        break;
                    case "4":

                        MeetingResumes.setProperty("in_satisfact", SurveyScores.ToString());//將分數回寫到學員履歷的滿意度分數
                        MeetingResumes.setProperty("in_stat_2", "O");
                        break;
                    case "5":
                        MeetingResumes.setProperty("in_score_o", SurveyScores.ToString());//將分數回寫到學員履歷的原始分數
                        MeetingResumes.setProperty("in_stat_3", "O");
                        break;

                }
            }

            //CCO.Utilities.WriteDebug("In_CheckSurveyAns", "195:" + MeetingResumes.getID());
            MeetingResumes = MeetingResumes.apply("edit");
            //CCO.Utilities.WriteDebug("In_CheckSurveyAns", "197");
            //這樣METHOD執行完之後,就會算出這個 meeting 的所有上課學員的surveytype=5的考卷分數更新到個人的 meeting_user_resume 分數欄位內
            /*
            Item itmMeetingUser2 = inn.newItem("In_Meeting_User");
            itmMeetingUser2.setProperty("meetinguserid", meetinguserid);
            itmMeetingUser2.setProperty("surveytype",surveytype);
            itmMeetingUser2 = itmMeetingUser2.apply("In_Update_Resume");
            */
            return inn.newResult("");

        }

    }
}