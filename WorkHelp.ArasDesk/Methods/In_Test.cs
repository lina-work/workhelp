﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_Test : Item
    {
        public In_Test(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            //System.Diagnostics.Debugger.Break();
            // string a= "1";
            // throw new Exception("z");

            var inn = this.getInnovator();
            var itmR = this;

            //清除舊資料
            var sql = "DELETE FROM IN_CATEGORY";
            inn.applySQL(sql);

            //新增資料
            var list = GetList();
            for (var i = 0; i < list.Count; i++)
            {
                var row = list[i];
                var item = inn.newItem("In_Category", "add");
                item.setProperty("in_name", row.in_name);
                item.setProperty("in_sort_order", row.in_sort_order);
                item = item.apply();
            }

            return itmR;
        }

       
        private List<TRow> GetList()
        {
            var result = new List<TRow>();
            result.Add(new TRow { in_name = "團本部", in_sort_order = "10000" });
            result.Add(new TRow { in_name = "高男組田徑", in_sort_order = "10101" });
            result.Add(new TRow { in_name = "高女組田徑", in_sort_order = "10102" });
            result.Add(new TRow { in_name = "國男組田徑", in_sort_order = "10103" });
            result.Add(new TRow { in_name = "國女組田徑", in_sort_order = "10104" });
            result.Add(new TRow { in_name = "高中組田徑", in_sort_order = "10105" });
            result.Add(new TRow { in_name = "國中組田徑", in_sort_order = "10106" });
            result.Add(new TRow { in_name = "高男組游泳", in_sort_order = "10201" });
            result.Add(new TRow { in_name = "高女組游泳", in_sort_order = "10202" });
            result.Add(new TRow { in_name = "國男組游泳", in_sort_order = "10203" });
            result.Add(new TRow { in_name = "國女組游泳", in_sort_order = "10204" });
            result.Add(new TRow { in_name = "高男組跆拳道品勢", in_sort_order = "10301" });
            result.Add(new TRow { in_name = "高女組跆拳道品勢", in_sort_order = "10302" });
            result.Add(new TRow { in_name = "國男組跆拳道品勢", in_sort_order = "10303" });
            result.Add(new TRow { in_name = "國女組跆拳道品勢", in_sort_order = "10304" });
            result.Add(new TRow { in_name = "高中組跆拳道品勢", in_sort_order = "10305" });
            result.Add(new TRow { in_name = "國中組跆拳道品勢", in_sort_order = "10306" });
            result.Add(new TRow { in_name = "高男組跆拳道對打", in_sort_order = "10401" });
            result.Add(new TRow { in_name = "高女組跆拳道對打", in_sort_order = "10402" });
            result.Add(new TRow { in_name = "國男組跆拳道對打", in_sort_order = "10403" });
            result.Add(new TRow { in_name = "國女組跆拳道對打", in_sort_order = "10404" });
            result.Add(new TRow { in_name = "高男組柔道", in_sort_order = "10501" });
            result.Add(new TRow { in_name = "高女組柔道", in_sort_order = "10502" });
            result.Add(new TRow { in_name = "國男組柔道", in_sort_order = "10503" });
            result.Add(new TRow { in_name = "國女組柔道", in_sort_order = "10504" });
            result.Add(new TRow { in_name = "高男組空手道", in_sort_order = "10601" });
            result.Add(new TRow { in_name = "高女組空手道", in_sort_order = "10602" });
            result.Add(new TRow { in_name = "國男組空手道", in_sort_order = "10603" });
            result.Add(new TRow { in_name = "國女組空手道", in_sort_order = "10604" });
            result.Add(new TRow { in_name = "高男組角力", in_sort_order = "10701" });
            result.Add(new TRow { in_name = "高女組角力", in_sort_order = "10702" });
            result.Add(new TRow { in_name = "國男組角力", in_sort_order = "10703" });
            result.Add(new TRow { in_name = "國女組角力", in_sort_order = "10704" });
            result.Add(new TRow { in_name = "高男組射箭", in_sort_order = "10801" });
            result.Add(new TRow { in_name = "高女組射箭", in_sort_order = "10802" });
            result.Add(new TRow { in_name = "國男組射箭", in_sort_order = "10803" });
            result.Add(new TRow { in_name = "國女組射箭", in_sort_order = "10804" });
            result.Add(new TRow { in_name = "高男組射擊", in_sort_order = "10901" });
            result.Add(new TRow { in_name = "高女組射擊", in_sort_order = "10902" });
            result.Add(new TRow { in_name = "國男組射擊", in_sort_order = "10903" });
            result.Add(new TRow { in_name = "國女組射擊", in_sort_order = "10904" });
            result.Add(new TRow { in_name = "高中組射擊", in_sort_order = "10905" });
            result.Add(new TRow { in_name = "國中組射擊", in_sort_order = "10906" });
            result.Add(new TRow { in_name = "高男組拳擊", in_sort_order = "11001" });
            result.Add(new TRow { in_name = "高女組拳擊", in_sort_order = "11002" });
            result.Add(new TRow { in_name = "國男組拳擊", in_sort_order = "11003" });
            result.Add(new TRow { in_name = "國女組拳擊", in_sort_order = "11004" });
            result.Add(new TRow { in_name = "高男組舉重", in_sort_order = "11201" });
            result.Add(new TRow { in_name = "高女組舉重", in_sort_order = "11202" });
            result.Add(new TRow { in_name = "國男組舉重", in_sort_order = "11203" });
            result.Add(new TRow { in_name = "國女組舉重", in_sort_order = "11204" });
            result.Add(new TRow { in_name = "高男組擊劍", in_sort_order = "11401" });
            result.Add(new TRow { in_name = "高女組擊劍", in_sort_order = "11402" });
            result.Add(new TRow { in_name = "國男組擊劍", in_sort_order = "11403" });
            result.Add(new TRow { in_name = "國女組擊劍", in_sort_order = "11404" });
            result.Add(new TRow { in_name = "高男組划船", in_sort_order = "11601" });
            result.Add(new TRow { in_name = "高女組划船", in_sort_order = "11602" });
            result.Add(new TRow { in_name = "國男組划船", in_sort_order = "11603" });
            result.Add(new TRow { in_name = "國女組划船", in_sort_order = "11604" });
            result.Add(new TRow { in_name = "高男組自由車", in_sort_order = "11701" });
            result.Add(new TRow { in_name = "高女組自由車", in_sort_order = "11702" });
            result.Add(new TRow { in_name = "國男組自由車", in_sort_order = "11703" });
            result.Add(new TRow { in_name = "國女組自由車", in_sort_order = "11704" });
            result.Add(new TRow { in_name = "高男組輕艇", in_sort_order = "11801" });
            result.Add(new TRow { in_name = "高女組輕艇", in_sort_order = "11802" });
            result.Add(new TRow { in_name = "國男組輕艇", in_sort_order = "11803" });
            result.Add(new TRow { in_name = "國女組輕艇", in_sort_order = "11804" });
            result.Add(new TRow { in_name = "高男組桌球", in_sort_order = "20101" });
            result.Add(new TRow { in_name = "高女組桌球", in_sort_order = "20102" });
            result.Add(new TRow { in_name = "國男組桌球", in_sort_order = "20103" });
            result.Add(new TRow { in_name = "國女組桌球", in_sort_order = "20104" });
            result.Add(new TRow { in_name = "高中組桌球", in_sort_order = "20105" });
            result.Add(new TRow { in_name = "國中組桌球", in_sort_order = "20106" });
            result.Add(new TRow { in_name = "高男組羽球", in_sort_order = "20201" });
            result.Add(new TRow { in_name = "高女組羽球", in_sort_order = "20202" });
            result.Add(new TRow { in_name = "國男組羽球", in_sort_order = "20203" });
            result.Add(new TRow { in_name = "國女組羽球", in_sort_order = "20204" });
            result.Add(new TRow { in_name = "高中組羽球", in_sort_order = "20205" });
            result.Add(new TRow { in_name = "國中組羽球", in_sort_order = "20206" });
            result.Add(new TRow { in_name = "高男組網球", in_sort_order = "20301" });
            result.Add(new TRow { in_name = "高女組網球", in_sort_order = "20302" });
            result.Add(new TRow { in_name = "國男組網球", in_sort_order = "20303" });
            result.Add(new TRow { in_name = "國女組網球", in_sort_order = "20304" });
            result.Add(new TRow { in_name = "高中組網球", in_sort_order = "20305" });
            result.Add(new TRow { in_name = "國中組網球", in_sort_order = "20306" });
            result.Add(new TRow { in_name = "高男組軟式網球", in_sort_order = "20401" });
            result.Add(new TRow { in_name = "高女組軟式網球", in_sort_order = "20402" });
            result.Add(new TRow { in_name = "國男組軟式網球", in_sort_order = "20403" });
            result.Add(new TRow { in_name = "國女組軟式網球", in_sort_order = "20404" });
            result.Add(new TRow { in_name = "高中組軟式網球", in_sort_order = "20405" });
            result.Add(new TRow { in_name = "國中組軟式網球", in_sort_order = "20406" });
            result.Add(new TRow { in_name = "高男組木球", in_sort_order = "20501" });
            result.Add(new TRow { in_name = "高女組木球", in_sort_order = "20502" });
            result.Add(new TRow { in_name = "國男組木球", in_sort_order = "20503" });
            result.Add(new TRow { in_name = "國女組木球", in_sort_order = "20504" });
            result.Add(new TRow { in_name = "高女組韻律體操", in_sort_order = "30102" });
            result.Add(new TRow { in_name = "國女組韻律體操", in_sort_order = "30104" });
            result.Add(new TRow { in_name = "高男組競技體操", in_sort_order = "30201" });
            result.Add(new TRow { in_name = "高女組競技體操", in_sort_order = "30202" });
            result.Add(new TRow { in_name = "國男組競技體操", in_sort_order = "30203" });
            result.Add(new TRow { in_name = "國女組競技體操", in_sort_order = "30204" });
            result.Add(new TRow { in_name = "高男組滑輪溜冰", in_sort_order = "30301" });
            result.Add(new TRow { in_name = "高女組滑輪溜冰", in_sort_order = "30302" });
            result.Add(new TRow { in_name = "國男組滑輪溜冰", in_sort_order = "30303" });
            result.Add(new TRow { in_name = "國女組滑輪溜冰", in_sort_order = "30304" });
            result.Add(new TRow { in_name = "國中組滑輪溜冰", in_sort_order = "30306" });

            return result;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TRow
        {
            public string in_name { get; set; }
            public string in_sort_order { get; set; }
        }

    }
}
