﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class Windows_Status_Control : Item
    {
        public Windows_Status_Control(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            Innovator inn = this.getInnovator();

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "Windows_Status_Control";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                scene = itmR.getProperty("scene", ""),
                value = itmR.getProperty("value", ""),
            };

            switch (cfg.scene)
            {
                case "page":
                    Page(cfg, itmR);
                    break;

                case "cmd":
                    ExecuteCmd(cfg);
                    break;
            }

            return itmR;
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            try
            {
                string file = @"C:\App\WinStatus\monitor.txt";

                //2022-02-23 15:48:23.948|CPU: 0.0%|Memory: 62%
                string contents = File.ReadAllText(file, System.Text.Encoding.UTF8);

                string[] cols = contents.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                if (cols != null)
                {
                    for (int i = 0; i < cols.Length; i++)
                    {
                        var col = cols[i];
                        if (i == 0)
                        {
                            string inn_time = GetStatusTime(col);

                            itmReturn.setProperty("inn_time", inn_time);
                        }
                        else
                        {
                            Item item = cfg.inn.newItem();
                            item.setType("inn_item");
                            item.setProperty("value", col);
                            itmReturn.addRelationship(item);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Item itmErr = cfg.inn.newItem();
                itmErr.setType("inn_item");
                itmErr.setProperty("value", ex.Message);
                itmReturn.addRelationship(itmErr);
            }
        }

        private string GetStatusTime(string value)
        {
            DateTime now = DateTime.Now;
            
            var dt = GetDtmVal(value);
            var ts = now - dt;

            string color = "blue";
            string msg = "";
            if (ts.TotalSeconds > 3)
            {
                color = "red";
                msg = " (異常)";
            }

            string inn_time = "<h5 style='color: " + color + "'>"
                + "狀態偵測時間："
                + dt.ToString("yyyy-MM-dd HH:mm:ss")
                + msg
                + "</h5>";

            return inn_time;
        }

        private void ExecuteCmd(TConfig cfg)
        {
            string file = @"C:\app\WinStatus\mlogs\cmd.log";
            string contents = "";

            //MSSQLSERVER: default 
            //MSSQL$SQL17: 85 prod 運動
            string sqlsvr = "MSSQL$SQL17";//MSSQL$SQL17

            switch (cfg.value)
            {
                case "1": //重啟 IIS
                    contents = "iisreset /start";
                    break;
                case "2": //重啟 SQL
                    contents = "net stop \"" + sqlsvr + "\" & net start \"" + sqlsvr + "\"";
                    break;
                case "3": //重啟 IIS 與 SQL
                    contents = "net stop \"" + sqlsvr + "\" & net start \"" + sqlsvr + "\" & iisreset /restart";
                    break;

                case "1001"://啟動柔道成績推送程式
                    contents = "net start \"InnSportSyncSvc\"";
                    break;

                case "1002"://停止柔道成績推送程式
                    contents = "net stop \"InnSportSyncSvc\"";
                    break;
            }

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, contents);

            System.IO.File.WriteAllText(file, contents, System.Text.Encoding.UTF8);
        }

        private DateTime GetDtmVal(string value, string format = "yyyy-MM-dd")
        {
            DateTime dt = DateTime.Now;
            DateTime.TryParse(value, out dt);
            return dt;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string scene { get; set; }
            public string value { get; set; }
        }
    }
}