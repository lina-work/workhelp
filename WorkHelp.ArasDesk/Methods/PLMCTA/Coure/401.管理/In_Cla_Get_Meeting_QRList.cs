﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using System.Net;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
	public class In_Cla_Get_Meeting_QRList : Item
	{
		public In_Cla_Get_Meeting_QRList(IServerConnection arg) : base(arg) { }

		/// <summary>
		/// 編程啟動點 (Code 在此撰寫)
		/// </summary>
		public Item Run()
		{
			Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
			Aras.Server.Core.IContextState RequestState = CCO.RequestState;
			//System.Diagnostics.Debugger.Break();

			Innovator inn = this.getInnovator();
			Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

			string strDatabaseName = inn.getConnection().GetDatabaseName();
			string strMethodName = "[" + strDatabaseName + "]" + "In_Cla_Get_Meeting_QRList";

			Item Inn_Result;

			string meetingid = this.getProperty("meeting_id", "");
			string Refresh = this.getProperty("refresh", "0");

			string strUserName = inn.getItemById("User", inn.getUserID()).getProperty("keyed_name");
			string CurrentIdentityList = Aras.Server.Security.Permissions.Current.IdentitiesList;
			string CurrentUserAlias = inn.getUserAliases();

			string aml = @"
				<AML> 
					<Item type='In_Cla_Meeting' action='get' select='id,in_title'>
						<or>
							<owned_by_id>{#CurrentUserAlias}</owned_by_id>
							<managed_by_id>{#CurrentUserAlias}</managed_by_id>
							<in_teacher>{#CurrentUserAlias}</in_teacher>
						</or>
						<in_date_s condition='lt'>{#today}</in_date_s>
						<in_date_e condition='gt'>{#today}</in_date_e >
						<in_is_main condition='ne'>1</in_is_main>
					</Item>
				</AML>
			";

			aml = aml.Replace("{#CurrentUserAlias}", CurrentUserAlias);
			aml = aml.Replace("{#today}", System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
			Item itmMeetings = inn.applyAML(aml);

			if (itmMeetings.getItemCount() == 0 && meetingid == "") //查無資料時,建立一個空的 Item 並把 in_title 塞進去
			{
				Item itmfake = inn.newItem("In_Cla_Meeting");
				itmfake.setProperty("in_title", "查無資料");
				itmfake.addRelationship(itmfake);
				return itmfake;
			}


			if (meetingid == "")
			{
				meetingid = itmMeetings.getItemByIndex(0).getProperty("id", "");//第一筆會議的id
			}

			Item itmR = inn.newItem("In_Cla_Meeting");
			//itmR.setAttribute("select","in_title,in_url");
			itmR.setID(meetingid);
			itmR = itmR.apply("get");


			bool IsSetMeetingIdInResult = false; //網址上指定的ID是否存在於查詢結果內
			for (int i = 0; i < itmMeetings.getItemCount(); i++)
			{
				Item itmMeeting = itmMeetings.getItemByIndex(i);
				if (itmMeeting.getID() == meetingid)
				{
					IsSetMeetingIdInResult = true;
					itmMeeting.setProperty("selected", "selected");

				}
				itmR.addRelationship(itmMeetings.getItemByIndex(i));
			}

			if (!IsSetMeetingIdInResult)
			{
				//代表網址上指定的ID沒有出現在查詢結果內
				aml = "<AML><Item type='In_Cla_Meeting' action='get' id='" + meetingid + "' select='id,in_title'></Item></AML>";
				Item itmMeeting = inn.applyAML(aml);
				if (itmMeeting.isError())
				{
					Item itmfake = inn.newItem("In_Cla_Meeting");
					itmfake.setProperty("in_title", "查無資料");
					itmfake.addRelationship(itmfake);
					return itmfake;
				}
				else
				{
					itmMeeting.setProperty("selected", "selected");
					itmR.addRelationship(itmMeeting);
				}
			}

			string app_url = _InnH.GetInVariable("app_url").getProperty("in_value", "In_Variable.app_url").TrimEnd('/') + "/";
			string api_url = _InnH.GetInVariable("api_url").getProperty("in_value", "In_Variable.api_url").TrimEnd('/') + "/";

			TConfig cfg = new TConfig
			{
				CCO = CCO,
				refresh = Refresh,
				api_url = api_url,
				mt_type = itmR.getProperty("in_meeting_type", "")
			};

			Item QRList = null;

			//學員登入
			string UserLogin = "";
			string UserLogin_url = "";
			string mulogin_page = "Cla_MeetingUserLogin.html";
			if (cfg.mt_type == "vote")
			{
				mulogin_page = "Cla_MeetingUserLoginV.html";//數字登入版
				UserLogin = app_url + "pages/b.aspx?page=" + mulogin_page + "&method=In_Cla_Get_MeetingUserLogin&meeting_id=" + meetingid;
				UserLogin_url = ReadShortUrl(cfg, UserLogin, force_api: false);
			}
			else
			{
				//UserLogin = app_url + "pages/b.aspx?page=" + mulogin_page + "&method=In_Cla_Get_MeetingUserLogin&meeting_id=" + meetingid;
				//UserLogin_url = ReadShortUrl(cfg, UserLogin, force_api: false);

				UserLogin = app_url + "pages/public/MeetingUser_Login.html?meeting_id=" + meetingid;
				UserLogin_url = ReadShortUrl(cfg, UserLogin, force_api: true);
			}

			QRList = inn.newItem("QRList");
			QRList.setProperty("title", "學員登入");
			QRList.setProperty("url", UserLogin_url);

			//只有付費課程(in_is_advanced)才可以看到學員登入
			if (itmR.getProperty("in_is_advanced", "0") == "1")
			{
				itmR.addRelationship(QRList);
			}


			//學員報名
			string UserReg = "";
			string OldMeetings = "D52073C17E11470799E21CE147F258E7,A6B8038A14DB4311A8CEB1D1BE25F40B,9C473A25FCFE4E028DCA45449DC2538D,9445DB6A9F9A4E4BB85DB5666D67C157,69EC2A2846DF4640A35989BF0B5BB220,DF65D77348A244039437D2E735C283EF,05C8A12B318744C994723116E74AB5D6,B05E1C85407D4E17B586DC234E7F5CF3,C2B846447F6140069D31FCDCE0E5D4FC,78A34EDFB8224C4D806C1D05F144FC98,BB96946D30564A139939E57A9E3CC631,055C5B2904A043A2A7727951A81FBDD6,29A5E844109C4904A03FB1C8DD49BBF2,A0120EB314B64556BD493EB5F8754518,FA5406E6E3B24E27AFAEA64254010F18,2944DF37EE784C46AECD1258C558C0F6";
			if (OldMeetings.Contains(meetingid))
			{
				UserReg = app_url + "pages/b.aspx?page=" + itmR.getProperty("in_register_url", "") + "&method=In_Cla_Get_Meeting_Register&agent_id=0E53E163F74843AEAABAF65DDC39DCC6&meeting_id=" + meetingid;
			}
			else
			{
				UserReg = app_url + "pages/b.aspx?page=" + itmR.getProperty("in_register_url", "") + "&method=In_Cla_Get_Meeting_Register&meeting_id=" + meetingid;
			}

			//縮址
			string UserReg_url = ReadShortUrl(cfg, UserReg);

			QRList = inn.newItem("QRList");
			QRList.setProperty("title", "學員報名");
			QRList.setProperty("url", UserReg_url);
			itmR.addRelationship(QRList);



			//管理員登入
			string admin = app_url + "pages/c.aspx?page=In_Cla_MeetingSignin.html&method=In_Cla_Get_MeetingSignIn&actiontype=2";
			//縮址
			string admin_url = ReadShortUrl(cfg, admin);

			QRList = inn.newItem("QRList");
			QRList.setProperty("title", "管理員登入");
			QRList.setProperty("url", admin_url);
			//itmR.addRelationship(QRList);



			Item itmPassengers = this.apply("In_Collect_PassengerParam");
			int intPassengerCount = itmPassengers.getItemCount();
			for (int b = 0; b < intPassengerCount; b++)
			{
				itmR.addRelationship(itmPassengers.getItemByIndex(b));
			}


			QRList = null;

			//報名說明
			string IntroInurl = itmR.getProperty("in_url", "");
			string IntroShow = app_url + "pages/b.aspx?page=" + IntroInurl + "&method=In_Cla_MeetingUserResponse&in_meetingid=" + meetingid;
			//縮址
			string IntroShow_url = ReadShortUrl(cfg, IntroShow);

			QRList = inn.newItem("QRList");
			QRList.setProperty("title", "報名說明");
			QRList.setProperty("url", IntroShow_url);
			itmR.addRelationship(QRList);


			//報名結果 
			string IntroShow2 = app_url + "pages/b.aspx?page=RegistryList.html&method=in_registry_list&meeting_id=" + meetingid + "&mode=cla";
			//縮址
			string IntroShow_url_2 = ReadShortUrl(cfg, IntroShow2);

			QRList = inn.newItem("QRList");
			QRList.setProperty("title", "報名結果");
			QRList.setProperty("url", IntroShow_url_2);
			itmR.addRelationship(QRList);

			return itmR;
		}

		private string ReadShortUrl(TConfig cfg, string url, bool force_api = false)
		{
			string result = "";
			string md5 = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(url, "md5");

			if (cfg.refresh == "1")
			{
				cfg.CCO.Application.Remove(md5);
			}

			if (cfg.CCO.Application[md5] == null)
			{
				if(force_api)
				{
					result = ForceShortUrl(cfg.mt_type, cfg.api_url, url);
				}
				else
                {
					result = GetShortUrl(cfg.mt_type, cfg.api_url, url);
                }
				cfg.CCO.Application.Add(md5, result);
			}
			else
			{
				result = cfg.CCO.Application[md5].ToString();
			}

			return result;
		}

		private string GetShortUrl(string mt_type, string api_url, string url)
		{
			if (mt_type == "vote")
			{
				return ShortUrlFromApi(api_url, url);
			}
			else
			{
				return gettinyurl(api_url, url);

			}
		}

		private string ForceShortUrl(string mt_type, string api_url, string url)
		{
			return ShortUrlFromApi(api_url, url);
		}

		private string gettinyurl(string api_url, string url)
		{
			string tinyurl = "http://tinyurl.com/api-create.php?url=";
			string new_url = tinyurl + url;
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new_url);
			HttpWebResponse response = (HttpWebResponse)request.GetResponse();
			StreamReader tReader = new StreamReader(response.GetResponseStream());
			new_url = tReader.ReadToEnd();
			return new_url;
		}

		private string ShortUrlFromApi(string api_url, string url)
		{
			string result = "";
			try
			{
				string tgturl = System.Web.HttpUtility.UrlEncode(url);
				string fullurl = api_url + "create?url=" + tgturl;

				HttpWebRequest request = (HttpWebRequest)WebRequest.Create(fullurl);
				HttpWebResponse response = (HttpWebResponse)request.GetResponse();
				StreamReader tReader = new StreamReader(response.GetResponseStream());

				var rst = tReader.ReadToEnd().Replace("\"", "");
				if (rst != null && rst != "")
				{
					result = rst;
				}
			}
			catch (Exception ex)
			{
				result = ex.Message;
			}
			return result;
		}

		private class TConfig
		{
			public Aras.Server.Core.CallContext CCO { get; set; }
			public string mt_type { get; set; }
			public string api_url { get; set; }
			public string refresh { get; set; }
		}
	}
}