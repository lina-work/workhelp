﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_Cla_MeetingRegistryResult : Item
    {
        public In_Cla_MeetingRegistryResult(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 提供報名清單
    位置: In_MeetingRegistryResult.html
    日誌:
        - 2022-08-24: 新增晉段的判斷 報名截止後 管理者能檢視和刪除、報名者僅能檢視(Willy)
*/

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Cla_MeetingRegistryResult";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string aml = "";
            string sql = "";
            string strError = "";
            string strUserId = inn.getUserID();


            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                pay_number = itmR.getProperty("pay_number", ""), //取得[繳費單號]
                org_photo = itmR.getProperty("org_photo", ""),
            };

            cfg.itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            cfg.isMeetingAdmin = cfg.itmPermit.getProperty("isMeetingAdmin", "") == "1";
            cfg.isCommittee = cfg.itmPermit.getProperty("isCommittee", "") == "1";
            cfg.isGymOwner = cfg.itmPermit.getProperty("isGymOwner", "") == "1";
            cfg.isGymAssistant = cfg.itmPermit.getProperty("isGymAssistant", "") == "1";

            //無權限
            if (!cfg.isMeetingAdmin && !cfg.isCommittee && !cfg.isGymOwner && !cfg.isGymAssistant)
            {
                return itmR;
            }

            //登入者資訊
            cfg.itmLogin = cfg.inn.newItem("In_Resume", "get");
            cfg.itmLogin.setProperty("in_user_id", cfg.strUserId);
            cfg.itmLogin = cfg.itmLogin.apply();

            //登入者錯誤
            if (cfg.itmLogin.isError())
            {
                return itmR;
            }

            cfg.login_resume_id = cfg.itmLogin.getProperty("id", "");
            cfg.login_member_type = cfg.itmLogin.getProperty("in_member_type", "");
            cfg.login_sno = cfg.itmLogin.getProperty("in_sno", "");
            cfg.login_group = cfg.itmLogin.getProperty("in_group", "");

            cfg.gym_is_closed = "";
            cfg.gym_really_closed = "";

            //活動資訊
            MeetingInfo(cfg);

            //是否為共同講師
            bool open = GetUserResumeListStatus(cfg);
            if (open) cfg.isMeetingAdmin = true;

            if (!cfg.isMeetingAdmin && cfg.in_meeting_type == "degree")
            {
                Item itmRoleResult = inn.applyMethod("In_Association_Role"
                    , "<in_sno>" + cfg.login_sno + "</in_sno>"
                    + "<idt_names>ACT_ASC_Degree</idt_names>");

                cfg.isMeetingAdmin = itmRoleResult.getProperty("inn_result", "") == "1";
            }

            cfg.itmPermit.setProperty("isCoInstructor", open ? "1" : "0");

            bool is_gym = (cfg.isGymOwner || cfg.isGymAssistant);
            if (cfg.isMeetingAdmin || open) is_gym = false;
            cfg.isGym = is_gym;

            if (cfg.isGym)
            {
                cfg.gym_is_closed = "0";
                cfg.gym_really_closed = "0";

                sql = "SELECT id FROM In_Cla_Meeting_Gymlist WITH(NOLOCK) WHERE source_id = '{#meeting_id}' AND in_creator_sno = N'{#in_creator_sno}'";
                sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                    .Replace("{#in_creator_sno}", cfg.login_sno);

                Item itmMeetingGymlist = inn.applySQL(sql);

                if (itmMeetingGymlist.getResult() != "")
                {
                    //已結束報名單位
                    cfg.gym_is_closed = "1";
                    cfg.gym_really_closed = "1";
                }
            }

            //附加條件
            cfg.group_condition = "";

            if (cfg.isMeetingAdmin)
            {
                //協會帳號可看所有報名人員

            }
            else if (cfg.isCommittee)
            {
                if (cfg.in_meeting_type == "degree")
                {
                    //委員會帳號可看該區所有報名人員
                    cfg.group_condition = "AND t5.in_manager_org = '" + cfg.login_resume_id + "'";
                }
                else
                {
                    cfg.group_condition = "AND in_creator_sno = N'" + cfg.login_sno + "'";
                }
            }
            else
            {
                cfg.group_condition = "AND in_creator_sno = N'" + cfg.login_sno + "'";
            }

            cfg.pay_condition = cfg.pay_number != ""
                ? "AND ISNULL(in_paynumber, '') = N'" + cfg.pay_number + "'"
                : "";

            DateTime dtNow = System.DateTime.Now;
            DateTime meeting_time_s = Convert.ToDateTime(cfg.in_state_time_start);//將開始時間轉型
            DateTime meeting_time_e = Convert.ToDateTime(cfg.in_state_time_end);//將結束時間轉型

            cfg.isInTime = dtNow < meeting_time_e && dtNow > meeting_time_s;
            if (cfg.isInTime) //報名中
            {
                if (cfg.in_isfull == "1")//已額滿
                {
                    cfg.gym_is_closed = "1";
                    cfg.gym_really_closed = "0";
                }
                else
                {
                    cfg.gym_is_closed = "0";
                    cfg.gym_really_closed = "0";
                }
            }
            else
            {
                cfg.gym_is_closed = "1";
                cfg.gym_really_closed = "1";
            }

            //單位編輯模式
            Item itmOrgMode = GetOrgMode(inn, cfg.org_photo);

            Item itmSurvey = this;
            itmSurvey.setProperty("meeting_id", cfg.meeting_id);
            itmSurvey.setProperty("group_condition", cfg.group_condition);
            itmSurvey.setProperty("paynumber_condition", cfg.pay_condition);
            itmSurvey.setProperty("gym_is_closed", cfg.gym_is_closed);

            //與會者
            AppendMeetingUsers(cfg, itmSurvey, itmOrgMode, itmR);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private void MeetingInfo(TConfig cfg)
        {
            DateTime CurrentTime = System.DateTime.Today;

            //賽事資訊
            string aml = "<AML><Item type='In_Cla_Meeting' action='get' id='" + cfg.meeting_id + "'></Item></AML>";
            Item itmMeeting = cfg.inn.applyAML(aml);

            cfg.itmMeeting = itmMeeting;
            //活動開始時間
            cfg.in_date_s = itmMeeting.getProperty("in_date_s", CurrentTime.ToString("yyyy-MM-dd"));
            //活動結束時間
            cfg.in_date_e = itmMeeting.getProperty("in_date_e", CurrentTime.ToString("yyyy-MM-dd"));
            //報名開始時間
            cfg.in_state_time_start = itmMeeting.getProperty("in_state_time_start", CurrentTime.ToString("yyyy-MM-dd"));
            //報名結束時間
            cfg.in_state_time_end = itmMeeting.getProperty("in_state_time_end", CurrentTime.ToString("yyyy-MM-dd"));
            //已額滿
            cfg.in_isfull = itmMeeting.getProperty("in_isfull", "");
            //活動類型
            cfg.in_meeting_type = itmMeeting.getProperty("in_meeting_type", "");
            //講習類型
            cfg.in_seminar_type = itmMeeting.getProperty("in_seminar_type", "");
            //付款方式
            cfg.in_pay_mode = itmMeeting.getProperty("in_pay_mode", "");
            //審核模式
            cfg.in_verify_mode = itmMeeting.getProperty("in_verify_mode", "");

        }

        //與會者
        private void AppendMeetingUsers(TConfig cfg, Item itmSurvey, Item itmOrgMode, Item itmReturn)
        {
            string group_condition = cfg.group_condition.Replace("AND in_creator_sno", "AND t1.in_creator_sno");
            string pay_condition = cfg.pay_condition;

            string org_upload_mode = itmOrgMode.getProperty("org_upload_mode", "");
            string org_text_mode = itmOrgMode.getProperty("org_text_mode", "");

            var end_reg_time = ToDtm(cfg.in_state_time_end);
            //是否為[晉段]活動
            bool is_degree = cfg.in_meeting_type == "degree";
            //是否已結束報名
            bool is_time_up = DateTime.Now > end_reg_time;

            string hide_section_col = "data-visible=\"false\"";
            string hide_index_col = "data-visible=\"false\"";
            string hide_degree_col = "data-visible=\"false\"";
            string hide_note_col = "data-visible=\"false\"";
            string hide_pay_col = "data-visible=\"false\"";    //繳費狀態
            string hide_refund_col = "data-visible=\"false\""; //退費申請
            string hide_cert_col = "data-visible=\"false\"";   //資料狀態
            string hide_verify_col = "data-visible=\"false\""; //審核資格狀態

            //無金流模式、土銀模式、QrCode 模式
            if (cfg.in_pay_mode == "2" || cfg.in_pay_mode == "3" || cfg.in_pay_mode == "4")
            {
                hide_pay_col = "";
            }

            switch (cfg.in_meeting_type)
            {
                case "degree":
                    hide_degree_col = "";
                    hide_cert_col = "";
                    hide_verify_col = "";
                    //晉段強制關閉【繳費狀態】欄位
                    hide_pay_col = "data-visible=\"false\"";
                    break;

                case "seminar":
                    hide_section_col = "";
                    hide_index_col = "";
                    hide_note_col = "";
                    //hide_refund_col = "";
                    hide_verify_col = "";
                    break;

                default:
                    hide_section_col = "";
                    hide_index_col = "";
                    hide_note_col = "";
                    //hide_refund_col = "";
                    break;

            }

            //申請退費僅允許在活動開始前一週之前可以
            bool can_refund = true;
            DateTime dtNow = System.DateTime.Now;

            DateTime meeting_time_s = Convert.ToDateTime(cfg.in_date_s).AddDays(-7);//活動開始時間
            if (dtNow >= meeting_time_s)
            {
                //已過可退費時間
                can_refund = false;
                hide_refund_col = "data-visible=\"false\""; //退費申請
            }


            string hide_download = " ";
            if (cfg.in_seminar_type == "other")
            {
                hide_download = "item_show_0";
            }

            string sql = @"
                SELECT
                    t1.*
                    , t3.in_pay_amount_real
                    , t3.in_pay_photo
                    , t3.in_return_mark
                    , t3.in_collection_agency
                    , t4.in_cancel_status
                    , ISNULL(t6.in_short_org, t1.in_committee) AS 'committee_short_name'
                FROM
                    IN_CLA_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_MEETING_PAY t3 WITH(NOLOCK)
                    ON t3.in_cla_meeting = t1.source_id
                    AND t3.item_number = t1.in_paynumber
                LEFT OUTER JOIN
                    IN_MEETING_NEWS t4 WITH(NOLOCK)
                    ON t4.source_id = t3.id
                    AND t4.in_muid = t1.id
                LEFT OUTER JOIN 
	                IN_RESUME t5 WITH(NOLOCK)
	                ON t5.in_sno = t1.in_creator_sno
                LEFT OUTER JOIN 
	                IN_RESUME t6 WITH(NOLOCK)
	                ON t6.in_name = t1.in_committee 
	                AND t6.in_member_type = 'area_cmt'
	                AND t6.in_member_role = 'sys_9999'
                WHERE
                    t1.source_id = '{#meeting_id}'
                    AND t1.in_note_state = N'official'
                    {#group_condition}
                    {#paynumber_condition}
                ORDER BY
                    t1.in_degree DESC
                    , t1.in_birth
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#group_condition}", group_condition)
                .Replace("{#paynumber_condition}", pay_condition);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_group = item.getProperty("in_group", "");
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                string in_l3 = item.getProperty("in_l3", "");

                string in_l1_short = in_l1.Replace("跆拳道教練講習", "")
                    .Replace("跆拳道裁判講習", "");

                if (in_l2 != "")
                {
                    in_l1_short += "-" + in_l2;
                }

                if (in_l3 != "")
                {
                    in_l1_short += "-" + in_l3;
                }

                string in_paynumber = item.getProperty("in_paynumber", "");
                string in_paytime = item.getProperty("in_paytime", "");
                string in_pay_photo = item.getProperty("in_pay_photo", "");

                string in_return_mark = item.getProperty("in_return_mark", "");
                string in_cancel_status = item.getProperty("in_cancel_status", "");

                string in_cert_valid = item.getProperty("in_cert_valid", "");
                string in_verify_result = item.getProperty("in_verify_result", "");
                string in_ass_ver_result = item.getProperty("in_ass_ver_result", "");
                bool is_reject = in_verify_result == "0" || in_ass_ver_result == "0";

                if (can_refund && in_cancel_status != "")
                {
                    //標題列要出現退款申請欄位 (預設隱藏)
                    hide_refund_col = "";
                }

                string inn_keywords = "," + in_l1 + ",";

                item.setType("inn_meeting_user");
                item.setProperty("number", in_paynumber);
                item.setProperty("group", in_group);
                item.setProperty("meeting_id", cfg.meeting_id);
                item.setProperty("inn_keywords", inn_keywords);
                item.setProperty("inn_has_stuffs", "0");
                item.setProperty("inn_gym_is_closed", cfg.gym_is_closed);
                item.setProperty("in_l1_short", in_l1_short);
                item.setProperty("beaten", "備註");//是badminton丟出備註
                item.setProperty("org_upload_mode", org_upload_mode);
                item.setProperty("org_text_mode", org_text_mode);
                item.setProperty("in_verify_mode", cfg.in_verify_mode);

                //繳費狀態
                if (hide_pay_col == "")
                {
                    item.setProperty("pay_number", GetPayStatusIcon(cfg, item));
                }
                else
                {
                    item.setProperty("pay_number", "");
                }

                //資料狀態
                if (hide_cert_col == "")
                {
                    item.setProperty("inn_cert_valid", GetCertStatus(in_cert_valid));
                }
                else
                {
                    item.setProperty("inn_cert_valid", "");
                }

                //審核資料狀態
                if (hide_verify_col == "")
                {
                    item.setProperty("inn_verify_display", GetVerifyDisplay(cfg, item));
                }

                //預設全關閉
                string hide_edit = "item_show_0";
                string hide_delete = "item_show_0";
                string hide_view = "item_show_0";
                string hide_refund = "item_show_0";
                string hide_exchange = "item_show_0";

                if (cfg.isInTime)//可報名
                {
                    if (in_paytime != "")//已繳費
                    {
                        hide_view = "";
                        if (in_cancel_status == "變更申請通過")
                        {
                            hide_exchange = "";//可申請，不可退費
                        }
                        else
                        {
                            hide_refund = "";//可退費
                        }

                        if (is_reject)
                        {
                            hide_edit = "";//可修改(審核退件)
                        }
                        else
                        {
                            hide_view = "";//可檢視
                        }
                    }
                    else if (in_paynumber != "")//已產生繳費單
                    {
                        if (is_reject)
                        {
                            hide_edit = "";//可修改(審核退件)
                        }
                        else
                        {
                            hide_view = "";//可檢視
                        }
                    }
                    else
                    {
                        hide_edit = "";
                        hide_delete = "";
                    }
                }
                else//不可報名
                {
                    hide_view = "";//可檢視

                    if (in_paytime != "")//已繳費
                    {
                        if (in_cancel_status == "變更申請通過")
                        {
                            hide_exchange = "";//可申請，不可退費
                        }
                        else
                        {
                            hide_refund = "";//可退費
                        }
                    }
                }
                

                if (!can_refund)
                {
                    //不可退費申請
                    hide_refund = "item_show_0";
                }

                if (is_degree)
                {
                    //如為晉段，暫不開放退費功能
                    hide_refund = "item_show_0";
                    if (is_time_up)
                    {
                        //時間到，不開放修改與刪除
                        hide_edit = "item_show_0";
                        hide_delete = "item_show_0";
                        hide_view = "";
                    }
                }

                //管理者最後判斷
                if (cfg.isMeetingAdmin)
                {
                    hide_delete = "";//只要是管理者就能刪
                    if (hide_edit != "" && hide_view != "")
                    {
                        hide_view = "";//管理者不能改一定要能看
                    }
                }

                item.setProperty("hide_edit", hide_edit);
                item.setProperty("hide_delete", hide_delete);
                item.setProperty("hide_view", hide_view);
                item.setProperty("hide_refund", hide_refund);
                item.setProperty("hide_exchange", hide_exchange);
                item.setProperty("hide_download", hide_download);

                itmReturn.addRelationship(item);
            }

            itmReturn.setProperty("muser_count", count > 0 ? count.ToString() : "0");

            itmReturn.setProperty("hide_section_col", hide_section_col);
            itmReturn.setProperty("hide_index_col", hide_index_col);
            itmReturn.setProperty("hide_degree_col", hide_degree_col);
            itmReturn.setProperty("hide_note_col", hide_note_col);
            itmReturn.setProperty("hide_pay_col", hide_pay_col);

            itmReturn.setProperty("hide_refund_col", hide_refund_col);
            itmReturn.setProperty("hide_cert_col", hide_cert_col);
            itmReturn.setProperty("hide_verify_col", hide_verify_col);
        }

        //取得共同講師 (判斷現在的登入者是否為此課程的共同講師)
        private bool GetUserResumeListStatus(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t2.id
                FROM 
                    In_Cla_Meeting_Resumelist t1 WITH(NOLOCK) 
                INNER JOIN 
                    IN_RESUME t2 WITH(NOLOCK) 
                    ON t2.id = t1.related_id    
                WHERE 
                    t1.source_id = '{#meeting_id}' 
                    AND t2.in_user_id = '{#in_user_id}';    
                ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_user_id}", cfg.strUserId);

            Item item = cfg.inn.applySQL(sql);

            return item.getResult() != "";
        }

        //取得單位編輯模式
        private Item GetOrgMode(Innovator inn, string org_photo)
        {
            Item item = inn.newItem();
            if (org_photo == "1")
            {
                item.setProperty("org_upload_mode", "item_show_1");
                item.setProperty("org_text_mode", "item_show_0");
            }
            else
            {
                item.setProperty("org_upload_mode", "item_show_0");
                item.setProperty("org_text_mode", "item_show_1");
            }
            return item;
        }

        private string GetPayStatusIcon(TConfig cfg, Item item)
        {
            StringBuilder builder = new StringBuilder();

            string[] properties = new string[]
            {
                "in_paynumber", //繳費單號
                "in_pay_amount_real", //實際收款金額
                "in_pay_photo", //上傳繳費照片(繳費收據)
                "in_return_mark", //審核退回說明
                "in_collection_agency", //代收機構
            };

            foreach (string property in properties)
            {
                builder.Append("<" + property + ">" + item.getProperty(property, "") + "</" + property + ">");
            }

            Item itmResult = cfg.inn.applyMethod("In_PayType_Icon", builder.ToString());

            return itmResult.getProperty("in_pay_type", "");
        }

        //資料狀態
        private string GetCertStatus(string value)
        {
            if (value == "1")
            {
                return "<span style='color: green'>已上傳</span>";
            }
            else if (value == "0")
            {
                return "<span style='color: red'>資料不齊</span>";
            }
            else
            {
                return "<span style='color: #2C4198'>未檢查</span>";
            }
        }

        private string GetVerifyDisplay(TConfig cfg, Item item)
        {
            StringBuilder builder = new StringBuilder();

            string[] properties = new string[]
            {
                "in_verify_mode", //活動審核模式
                "in_verify_result", //委員會審核結果
                "in_verify_memo", //委員會審核說明
                "in_ass_ver_result", //協會審核結果
                "in_ass_ver_memo", //協會審核說明
                "in_ass_ver_memo", //協會審核說明
                "in_registration_status", //與會者報名狀態
            };

            foreach (string property in properties)
            {
                builder.Append("<" + property + ">" + item.getProperty(property, "") + "</" + property + ">");
            }

            Item itmResult = cfg.inn.applyMethod("In_Verify_Message", builder.ToString());

            return itmResult.getProperty("in_verify_display", "");
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string pay_number { get; set; }
            public string org_photo { get; set; }

            public string login_resume_id { get; set; }
            public string login_sno { get; set; }
            public string login_group { get; set; }
            public string login_member_type { get; set; }

            public string gym_is_closed { get; set; }
            public string gym_really_closed { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmLogin { get; set; }
            public Item itmPermit { get; set; }


            public string in_date_s { get; set; }
            public string in_date_e { get; set; }
            public string in_state_time_start { get; set; }
            public string in_state_time_end { get; set; }
            public string in_isfull { get; set; }
            public string in_meeting_type { get; set; }
            public string in_seminar_type { get; set; }
            public string in_pay_mode { get; set; }
            public string in_verify_mode { get; set; }


            public bool isMeetingAdmin { get; set; }
            public bool isCommittee { get; set; }
            public bool isGymOwner { get; set; }
            public bool isGymAssistant { get; set; }

            public bool isGym { get; set; }
            public bool isInTime { get; set; }

            public string group_condition { get; set; }
            public string pay_condition { get; set; }

        }

        private DateTime ToDtm(string value, int hours = 0)
        {
            var dt = DateTime.MinValue;
            if (value == "") return dt;

            if (DateTime.TryParse(value, out dt))
            {
                return dt.AddHours(hours);
            }
            else
            {
                return DateTime.MinValue;
            }
        }
    }
}