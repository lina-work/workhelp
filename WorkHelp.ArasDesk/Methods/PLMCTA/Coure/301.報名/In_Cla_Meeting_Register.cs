﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aras.IOM;
using Aras.IOME.ConflictDetection;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Coure
{
    public class In_Cla_Meeting_Register : Item
    {
        public In_Cla_Meeting_Register(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Cla_Meeting_Register";

            string strMethodDay = "[" + strDatabaseName + "]" + "In_Cla_Meeting_Register_"
                + System.DateTime.Now.ToString("yyyyMMdd");

            CCO.Utilities.WriteDebug(strMethodDay, "this dom: " + this.dom.InnerXml);

            //處理傳入的各項資料  
            string meeting_id = this.getProperty("meeting_id", "no_data");//對應的會議物件編號  
            string in_refuser = this.getProperty("agent_id", "no_data");//使用者的業務窗口。
            string param = this.getProperty("parameter", "no_data"); //取得傳入的param，是escape過的form-data
            string surveytype = this.getProperty("surveytype", "1");//surveytype，因為是報名專用，所以未傳入預設為1(1報名問卷)。修改使用者資料時會傳入1a
            string strMuid = this.getProperty("muid", "");  //要update的使用者id
            string isUserId = this.getProperty("isUserId", "");//偷藏的User ID
            string isIndId = this.getProperty("isIndId", "");//偷藏的Identity ID

            //CCO.Utilities.WriteDebug(strMethodName, "strMuid: " + strMuid);

            string login_resume_id = this.getProperty("login_resume_id", "");
            string login_resume_current_org = this.getProperty("login_resume_current_org", "");
            string login_resume_group = this.getProperty("login_resume_group", "");

            string mode = this.getProperty("mode", "");

            string checkResultMsg = "";

            string[] pms = param.Split('&');//處理傳入的parameter
            string muid = "";//會議使用者id，判斷是否為口袋名單時用到。
            Item applicant = inn.newItem("In_Cla_Meeting_User"); // 用來add使用者的資料
            Item itmMeeting;
            Item itmError;
            Item mUser;
            bool boolIsModify = false; //是否為修改資料模式

            //是否為人員異動
            string in_exchange = this.getProperty("in_exchange", "");
            if (in_exchange == "1")
            {
                return this.apply("in_exchange_cla_meeting_register");
            }


            Dictionary<string, string> dicWriteBacks = new Dictionary<string, string>(); //需要回寫回使用者的回答欄位清單  
            StringBuilder stbAddAnswer = new StringBuilder("");//用來處理答案及更新 In_Cla_Meeting 的AML。
            applicant.setProperty("source_id", meeting_id);

            if (surveytype == "1a")
            {
                string strRemoveSurveyAML = @"<AML>
    									<Item type='In_Cla_Meeting_Surveys_Result' action='delete' where=""in_participant='{#muid}' and in_surveytype='1' ""/>
    								</AML>"
                                            .Replace("{#muid}", strMuid);
                inn.applyAML(strRemoveSurveyAML);
                surveytype = "1";
                boolIsModify = true;
            }
            else if (strMuid != "")
            {
                string strRemoveSurveyAML = @"<AML>
    									<Item type='In_Cla_Meeting_Surveys_Result' action='delete' where=""in_participant='{#muid}' and in_surveytype='1' ""/>
    								</AML>"
                                            .Replace("{#muid}", strMuid);
                inn.applyAML(strRemoveSurveyAML);
                surveytype = "1";
                boolIsModify = true;
            }

            string strWriteBackAML = string.Format(@"
    		<AML>
    			<Item type=""In_Cla_Survey"" action=""get"" select=""in_property,id"">
    				<source_id>
    					<Item type=""In_Cla_Meeting_Surveys"" action=""get"">
    						<source_id>{0}</source_id>
    					</Item>
    				</source_id>
    				<in_write_back>1</in_write_back>
    			</Item>
    		</AML>", meeting_id);

            string strResultAML = @"
    		<Item type=""In_Cla_Meeting_Surveys_Result"" action=""add"">
    			<source_id>{0}</source_id>
    			<in_answer>{1}</in_answer>
    			<related_id>{2}</related_id>
    			<in_participant>muid</in_participant>
    			<in_surveytype>{3}</in_surveytype>
    		</Item>
    		";

            string strGetMeetingAML = "<AML><Item type='In_Cla_Meeting' action='get' id='{#meeting_id}' select='*'/></AML>"
                                        .Replace("{#meeting_id}", meeting_id);
            string strMuFilterName;
            bool boolRegOnScene = false;

            itmMeeting = inn.applyAML(strGetMeetingAML);
            strMuFilterName = itmMeeting.getProperty("in_mu_filter");

            //檢查是否有設定in_mu_filter
            if (strMuFilterName == null)
            {
                itmError = inn.newError("");
                itmError.setErrorDetail("要求的會議未設定會議使用者規則，請聯絡管理人員。");
                return itmError;
            }
            //檢查是否仍可報名
            if (strMuid == "" && itmMeeting.getProperty("in_isfull", "0") == "1")
            {
                itmError = inn.newError("");
                itmError.setErrorDetail(itmMeeting.getProperty("in_close_msg", "無法報名，原因未設定"));
                return itmError;
            }

            Item itmWriteBacks = inn.applyAML(strWriteBackAML);
            int intWriteBackCount = itmWriteBacks.getItemCount();
            //將要回寫的欄位寫入dictionary方便處理。
            for (int q = 0; q < intWriteBackCount; q++)
            {
                Item itmTemp = itmWriteBacks.getItemByIndex(q);
                dicWriteBacks.Add(itmTemp.getID(), itmTemp.getProperty("in_property", ""));
            }

            //處理 In_Cla_Meeting_User 的特殊欄位
            applicant.setProperty("in_mail", this.getProperty("email"));
            if (this.getProperty("email").Contains("@"))
            {
                applicant.setProperty("in_email", this.getProperty("email"));
            }


            //建立答案卷並將答案填入


            //針對某些舊型會議做特殊處理

            List<string> staRequiredFields = new List<string>(new string[] { "in_mail" });
            string[] staStuffings = new string[] { meeting_id, null, null, surveytype };
            //順序 meeting_id,in_answer,In_Cla_Survey(id),in_surveytype
            //是否正取
            bool isAdmission = true;
            //處理傳過來的答案卷，將要回寫的部分加入到applican的內容中
            for (int p = 0; p < pms.Length; p++)
            {
                string[] kvs = pms[p].Split('=');

                string surveyID = kvs[0];
                string[] answers = kvs[1].Split(',');

                for (int e = 0; e < answers.Length; e++)
                {
                    staStuffings[1] = answers[e];
                    staStuffings[2] = kvs[0];
                    stbAddAnswer.Append(string.Format(strResultAML, staStuffings));
                }

                //將要回寫的部分加入到applican的內容中
                if (dicWriteBacks.ContainsKey(kvs[0]))
                {
                    if (kvs[1].Trim() == "Invalid date" || kvs[1].Trim() == "") { continue; }
                    applicant.setProperty(dicWriteBacks[kvs[0]], kvs[1]);

                    string checkResult = checkLimit(CCO, strMethodName, inn, dicWriteBacks[kvs[0]], kvs[1], meeting_id, strMuid, login_resume_current_org);
                    if (checkResult != "")
                    {
                        checkResultMsg += checkResult;
                        isAdmission = false;
                    }

                }

                this.setProperty("x" + surveyID, kvs[1]);
            }

            //lina 2022.02.11: 避免帶空白
            applicant.setProperty("in_sno", applicant.getProperty("in_sno", "").Trim());
            applicant.setProperty("in_mail", applicant.getProperty("in_mail", "").Trim());

            //處理[身分證字號(外顯)]
            applicant.setProperty("in_sno_display", GetSidDisplay(applicant.getProperty("in_sno", "")));

            if (isAdmission)
            {
                //正取
                applicant.setProperty("in_note_state", "official");
            }
            else
            {
                //備取
                if (checkResultMsg.Contains("失敗"))
                {
                    itmError = inn.newError("");
                    itmError.setErrorDetail("<div style='position: relative;'>[" + applicant.getProperty("in_name", "") + "]" + checkResultMsg + "</div>");
                    return itmError;
                }
                else
                {
                    applicant.setProperty("in_note_state", "waiting");
                }
            }

            //判斷輸入的時間是否在區間內
            string check_birth = applicant.getProperty("in_birth", "no_data");
            if (check_birth != "no_data")
            {
                DateTime check_date = Convert.ToDateTime(check_birth);
                DateTime date_range_s = Convert.ToDateTime("1/1/1753 12:00:00 AM");
                DateTime date_range_e = Convert.ToDateTime("12/31/9999 11:59:59 PM");
                if (check_date.CompareTo(date_range_s) < 0 || check_date.CompareTo(date_range_e) > 0)
                {
                    throw new Exception("日期格式錯誤!請重新輸入!");
                }
            }

            if (isUserId != "")
            {
                Item CurrentUser = inn.getItemById("User", isUserId);
                //用偷藏的ID取得當前登入者
                applicant.setProperty("in_creator", CurrentUser.getProperty("last_name", "")); ;//協助報名者
                applicant.setProperty("in_creator_sno", CurrentUser.getProperty("login_name", ""));//協助報名者帳號
            }

            //補被報名者修改不改協助報名者
            if (this.getProperty("isSingleUser", "") == "1" || isUserId == "")
            {
                applicant.setProperty("in_creator", this.getProperty("su_creator", "")); ;//協助報名者
                applicant.setProperty("in_creator_sno", this.getProperty("su_creator_sno", ""));//協助報名者帳號
            }

            if (this.getProperty("in_from_import", "") == "1")
            {
                applicant.setProperty("in_creator", this.getProperty("su_creator", "")); ;//協助報名者
                applicant.setProperty("in_creator_sno", this.getProperty("su_creator_sno", ""));//協助報名者帳號
            }

            //所屬群組
            string in_group = applicant.getProperty("in_group", "");
            if (in_group == "")
            {
                in_group = login_resume_group;
                applicant.setProperty("in_group", login_resume_group);
            }

            //所屬單位
            string in_current_org = applicant.getProperty("in_current_org", "");
            if (in_current_org == "")
            {
                in_current_org = login_resume_current_org;
                applicant.setProperty("in_current_org", login_resume_current_org);
            }

            //▼檢查是否為現場報名
            string strCheckMeetingFunctionAML = @"<AML>
    										<Item type='In_Cla_Meeting_FunctionTime' action='get' select='id,in_type,in_date_s,in_date_e'>
    										    <in_action>2</in_action>
    											<source_id>{#meeting_id}</source_id>
    											<in_date_s condition='lt'>{#today}</in_date_s>
    											<in_date_e condition='gt'>{#today}</in_date_e>
    										</Item>
    									</AML>"
                                            .Replace("{#meeting_id}", meeting_id)
                                            .Replace("{#today}", System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));

            Item itmSignOnScene = inn.applyAML(strCheckMeetingFunctionAML);
            if (!itmSignOnScene.isError() && !itmSignOnScene.isEmpty())
            {
                boolRegOnScene = true;
            }

            //報名費用
            applicant.setProperty("in_expense", GetExpense(CCO, strMethodName, inn, itmMeeting, applicant));

            //組名
            string sectionName = GetSectionName(applicant, this.getProperty("register_section_name", ""));
            applicant.setProperty("in_section_name", sectionName);

            //如果有 in_index 則沿用,否則就給號
            string _in_index = applicant.getProperty("in_index", "");
            if (_in_index == "")
            {
                Item itmIndex = GetLevelMaxIndex(CCO, strMethodName, inn, meeting_id, applicant);

                if (itmIndex.getResult() == "")
                {
                    applicant.setProperty("in_index", "00001");
                }
                else
                {
                    int intIndex = int.Parse(itmIndex.getProperty("c1", "1")) + 1;
                    applicant.setProperty("in_index", intIndex.ToString("00000"));
                }
            }

            string in_meeting_type = itmMeeting.getProperty("in_meeting_type", "").ToLower();
            string in_degree_label = applicant.getProperty("in_degree_label", "");
            if (in_degree_label == "" && in_meeting_type == "degree")
            {
                in_degree_label = applicant.getProperty("in_l1", "");
            }

            //lina 2021.03.05-03.16
            Item itmMethodData = inn.newItem("In_Meeting_User");
            itmMethodData.setProperty("in_committee", applicant.getProperty("in_committee", ""));
            itmMethodData.setProperty("in_degree", applicant.getProperty("in_degree", ""));
            itmMethodData.setProperty("in_degree_label", in_degree_label);

            //段位數值與標籤處理
            Item itmDegreeRslt = itmMethodData.apply("In_Degree_Value");
            applicant.setProperty("in_degree", itmDegreeRslt.getProperty("in_degree", ""));

            // 		//委員會簡稱處理
            // 		Item itmCmtRslt = itmMethodData.apply("In_Committee_Value");
            // 		applicant.setProperty("in_committee_2", itmCmtRslt.getProperty("in_committee_2", ""));

            //特殊報名規則
            //--站台
            Item itmSite = inn.applyAML("<AML><Item type='In_Site' action='get' select='*'/></AML>");
            string st_reg_validate_id = itmSite.getProperty("in_reg_validate_2", "");
            if (st_reg_validate_id != "")
            {
                string st_reg_validate_name = itmSite.getPropertyAttribute("in_reg_validate_2", "keyed_name", "");
                applicant.apply(st_reg_validate_name);
            }
            //--課程
            string mt_reg_validate_id = itmMeeting.getProperty("in_reg_validate_method", "");
            if (mt_reg_validate_id != "")
            {
                string mt_reg_validate_name = itmMeeting.getPropertyAttribute("in_reg_validate_method", "keyed_name", "");
                applicant.apply(mt_reg_validate_name);
            }
            //特殊報名規則

            //只有在非修改的狀況下設定報名時間
            if (boolIsModify)
            {
                //lina 2021.02.18 不加會被清為空字串
                applicant.setProperty("in_regdate", System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
                //lina 2021.02.18 將審核狀態清空
                applicant.setProperty("in_verify_result", "");
                applicant.setProperty("in_ass_ver_result", "");

                applicant.setAttribute("where", "[In_Cla_Meeting_User].id='" + strMuid + "'");
                mUser = applicant.apply("merge");
            }
            else
            {
                applicant.setProperty("in_regdate", System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
                applicant = applicant.apply(strMuFilterName);
                mUser = applicant.apply("add");
            }

            muid = mUser.getID();
            stbAddAnswer.Replace("muid", muid);
            inn.applyAML("<AML>" + stbAddAnswer.ToString() + "</AML>");//寫入答案卷答案
            itmMeeting.apply("In_Cla_Update_MeetingOnMUEdit");
            mUser.apply("In_Cla_Merge_ThisResume");
            mUser.apply("In_Cla_Send_MeetingConfirm");


            //非盲報新增至 in_resume
            if (mode == "cla")
            {
                mUser.apply("In_Cla_Meeting_ToFormal1User");

                //驗證大頭照，resume 已有大頭照更新至與會者
                string muser_sno = applicant.getProperty("in_sno", "");
                string sql = "SELECT TOP 1 * FROM IN_Resume WITH(NOLOCK) WHERE in_sno = '" + muser_sno + "'";
                Item mUserResume = inn.applySQL(sql);
                if (!mUserResume.isError() && mUserResume.getResult() != "")
                {
                    string resume_photo = mUserResume.getProperty("in_photo", "");
                    string resume_user_id = mUserResume.getProperty("in_user_id", "");

                    sql = "UPDATE IN_CLA_MEETING_USER SET"
                        + " in_photo1 = '" + resume_photo + "'"
                        + ", related_id = '" + resume_user_id + "'"
                        + " WHERE id = '" + muid + "'";

                    inn.applySQL(sql);//補大頭照和 related_id
                }

                //lina 2021.01.06 建立成員關係
                if (login_resume_group == "中華跆協")
                {
                    if (this.getProperty("su_creator_sno", "") != "M001" && this.getProperty("su_creator_sno", "") != "")
                    {
                        sql = "SELECT * FROM in_resume WHERE in_sno ='" + this.getProperty("su_creator_sno", "") + "'";
                        Item itmRe = inn.applySQL(sql);
                        if (!itmRe.isError())
                        {
                            login_resume_id = itmRe.getProperty("id", "");
                            BindResumeRelation(CCO, strMethodName, inn, login_resume_id, mUser);
                        }
                    }
                }
                else
                {
                    BindResumeRelation(CCO, strMethodName, inn, login_resume_id, mUser);
                }
            }

            if (!boolIsModify && boolRegOnScene && mUser.getProperty("in_note_state", "") != "official" && itmMeeting.getProperty("in_register_url", "") == "MeetingRegistryContinous.html")
            {
                mUser.setProperty("inn_message", "使用者報名狀態為備取，請洽服務台。&" + muid);
                mUser.setProperty("inn_reg_on_scene", "true");
            }

            if (this.getProperty("in_note_state", "") == "")
            {
                this.getProperty("in_note_state", applicant.getProperty("in_note_state", ""));  //因為有時候是從手工報名過來的,所以要做切換
            }

            this.setProperty("in_note_state", this.getProperty("in_note_state"));
            this.setProperty("in_sno", mUser.getProperty("in_sno"));
            this.setProperty("in_email", mUser.getProperty("in_email"));
            this.setProperty("in_tel", mUser.getProperty("in_tel"));

            Item itmMResume = this.apply("In_Cla_mt_UpdateResumeByApplyForm");

            //[盲報]產生一張繳費單                                                   
            AppendPayment(CCO, strMethodName, inn, itmMeeting, mUser, isIndId);

            if (checkResultMsg != "")
            {
                mUser.setProperty("in_info_5", "<div style='position: relative;'>[" + mUser.getProperty("in_name", "") + "]" + checkResultMsg + "</div>");
            }

            return mUser;
        }

        //取得該組當前最大序號
        public static Item GetLevelMaxIndex(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id, Item applicant)
        {
            string sql = "";

            string in_l1 = applicant.getProperty("in_l1", "");

            sql = "SELECT max(in_index) as c1 FROM IN_CLA_MEETING_USER WITH(NOLOCK) WHERE source_id = '" + meeting_id + "'";
            sql += " AND in_l1 = N'" + in_l1 + "'";
            //CCO.Utilities.WriteDebug(strMethodName, "sql_index: " + sql);

            return inn.applySQL(sql);
        }

        //組合總名稱(in_section_name)
        public static string GetSectionName(Item applicant, string register_section_name)
        {
            if (register_section_name != "")
            {
                return register_section_name;
            }
            else
            {
                return applicant.getProperty("in_l1", "");
            }
        }

        //建立成員關係
        private void BindResumeRelation(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string login_resume_id, Item mUser)
        {
            if (login_resume_id == "")
            {
                return;
            }

            string sql = "SELECT TOP 1 * FROM In_Resume WITH(NOLOCK) where in_sno = N'" + mUser.getProperty("in_sno", "") + "'";
            Item mUserResume = inn.applySQL(sql);

            if (mUserResume.isError() || mUserResume.getItemCount() != 1 || mUserResume.getResult() == "")
            {
                //log
                return;
            }

            string in_sno = mUser.getProperty("in_sno", "");
            string in_l1 = mUser.getProperty("in_l1", "");
            string in_l2 = mUser.getProperty("in_l2", "");

            //CCO.Utilities.WriteDebug(strMethodName, "建立成員關係 _# start: " + in_sno);

            if (login_resume_id == "")
            {
                CCO.Utilities.WriteDebug(strMethodName, "建立成員關係 _# 登入者 resume id 無資料");
                return;
            }

            string in_user_resume = mUserResume.getProperty("id", "");

            string in_resume_role = "reg_110"; //選手
            string in_resume_remark = "選手";

            if (in_sno == "")
            {
                CCO.Utilities.WriteDebug(strMethodName, "建立成員關係 _# Player 身分證號 無資料");
                return;
            }

            if (in_l1 == "隊職員" && in_l2 != "")
            {
                in_resume_remark = in_l2;
                switch (in_l2)
                {
                    case "領隊":
                        in_resume_role = "reg_210";
                        break;
                    case "管理":
                        in_resume_role = "reg_220";
                        break;
                    case "教練":
                        in_resume_role = "reg_230";
                        break;
                }
            }

            sql = "SELECT id FROM In_Resume_Resume WITH(NOLOCK) WHERE source_id = '" + login_resume_id + "' AND related_id = '" + in_user_resume + "'";
            //CCO.Utilities.WriteDebug(strMethodName, "建立成員關係 _# sql: " + sql);
            Item itmQuery = inn.applySQL(sql);

            if (itmQuery.isError())
            {
                CCO.Utilities.WriteDebug(strMethodName, "建立成員關係 _# sql 發生錯誤");
                return;
            }

            if (itmQuery.getResult() != "")
            {
                //CCO.Utilities.WriteDebug(strMethodName, "建立成員關係 _# 已存在，不處理");
                return;
            }

            Item itmResumeRole = inn.newItem("In_Resume_Resume", "add");
            itmResumeRole.setProperty("source_id", login_resume_id);
            itmResumeRole.setProperty("related_id", in_user_resume);
            itmResumeRole.setProperty("in_resume_role", in_resume_role);
            itmResumeRole.setProperty("in_resume_remark", in_resume_remark);
            itmResumeRole = itmResumeRole.apply();

            //CCO.Utilities.WriteDebug(strMethodName, "建立成員關係 _# end: 建立成功");
        }

        //[盲報]產生一張繳費單
        private void AppendPayment(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMeeting, Item mUser, string isIndId)
        {
            string in_url = itmMeeting.getProperty("in_url", "").ToLower();

            if (in_url.Contains("in_cla_meeting_list_b"))
            {
                Item itmMeetingPay = inn.newItem();
                itmMeetingPay.setType("In_Meeting_Pay");
                itmMeetingPay.setProperty("cla_meeting_id", mUser.getProperty("source_id", ""));//課程報名 放課程ID
                itmMeetingPay.setProperty("in_name", mUser.getProperty("in_name", ""));//姓名(單位簡稱)
                itmMeetingPay.setProperty("in_sno", mUser.getProperty("in_sno", ""));//身分證字號
                itmMeetingPay.setProperty("isIndId", isIndId);//把ID丟過去
                Item itmPayResult = itmMeetingPay.apply("In_Payment_List_Add");//產生繳費單method

            }
        }

        //身分證字號(外顯)
        private string GetSidDisplay(string in_sno)
        {
            if (in_sno.Length == 10)
            {
                //10碼(身分證處理)
                string in_sno_1 = in_sno.Substring(0, 2);
                string in_sno_2 = in_sno.Substring(6, 4);
                return in_sno_1 + "****" + in_sno_2;//A1****6789
            }
            else
            {
                return "";
            }
        }

        //取得報名費用
        private string GetExpense(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMeeting, Item applicant)
        {
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + applicant.dom.InnerXml);

            string sql = "";
            string result = itmMeeting.getProperty("in_course_fees", "0");
            string meeting_id = itmMeeting.getProperty("id", "");

            sql = @"
                SELECT 
	                TOP 1 t2.*
                FROM 
	                IN_CLA_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN
	                IN_CLA_SURVEY t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t2.in_expense = '1'
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmSurvey = inn.applySQL(sql);

            if (itmSurvey.isError() || itmSurvey.getItemCount() != 1)
            {
                return result;
            }

            string survey_id = itmSurvey.getProperty("id", "");
            string in_property = itmSurvey.getProperty("in_property", "");
            if (in_property == "")
            {
                return result;
            }

            string in_value = applicant.getProperty(in_property, "");
            if (in_value == "")
            {
                return result;
            }

            sql = @"
                SELECT 
	                TOP 1 t2.*
                FROM 
	                IN_CLA_SURVEY t1 WITH(NOLOCK)
                INNER JOIN
	                IN_CLA_SURVEY_OPTION t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t2.source_id = '{#survey_id}'
	                AND t2.in_value = N'{#in_value}'    
            ";

            sql = sql.Replace("{#survey_id}", survey_id)
                .Replace("{#in_value}", in_value);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmOption = inn.applySQL(sql);

            if (itmOption.isError() || itmOption.getItemCount() != 1)
            {
                return result;
            }

            string in_expense_value = itmOption.getProperty("in_expense_value", "0");
            if (in_expense_value != "" && in_expense_value != "0")
            {
                result = in_expense_value;
            }

            //CCO.Utilities.WriteDebug(strMethodName, "in_expense: " + result);

            return result;
        }
        private string checkLimit(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string in_property, string in_label, string meeting_id, string strMuid, string in_current_org)
        {
            string question = "";
            string sql = @" SELECT * FROM in_cla_meeting_user  WITH(NOLOCK) 
                            WHERE SOURCE_ID='{#meeting_id}' AND id = '{#strMuid}' AND {#in_property} ='{#in_label}' ";
            sql = sql.Replace("{#in_property}", in_property)
                    .Replace("{#meeting_id}", meeting_id)
                    .Replace("{#in_label}", in_label)
                    .Replace("{#strMuid}", strMuid);

            Item itmUser = inn.applySQL(sql);


            //修改時判斷是否值無改變，沒改變則不需驗證人數
            if (itmUser.getItemCount() > 0 && !itmUser.isError())
            {
                return "";
            }

            //判斷單位限制人數
            sql = @" SELECT t2.in_property, IN_LABEL, IN_QUESTIONS, reg_curr_qty, in_limit_current
                    FROM innovator.IN_Cla_MEETING_SURVEYS AS T1 WITH(NOLOCK)
                    JOIN  innovator.IN_CLA_SURVEY AS T2 WITH(NOLOCK)
                    ON T1.RELATED_ID = T2.id
                    JOIN innovator.IN_Cla_SURVEY_OPTION AS T3 WITH(NOLOCK)
                    ON T2.id = T3.SOURCE_ID
                    LEFT JOIN
                    (
                    	SELECT '{#in_property}' as in_property, count(*) AS reg_curr_qty 
                    	FROM innovator.in_cla_meeting_user　WITH(NOLOCK)
                        WHERE SOURCE_ID='{#meeting_id}' 
                        AND IN_CURRENT_ORG ='{#in_current_org}'
                        AND {#in_property} ='{#in_label}' 
                        AND  IN_NOTE_STATE in('official','waiting')
                    ) AS T4
                    ON t2.in_property = T4.in_property
                    WHERE  T1.SOURCE_ID ='{#meeting_id}' 
                    AND t2.in_property='{#in_property}' 
                    AND IN_LABEL='{#in_label}'  
                    AND  ISNULL(in_limit_current,-1) >= 0　
                    ORDER BY t2.in_property";

            sql = sql.Replace("{#in_property}", in_property)
                    .Replace("{#meeting_id}", meeting_id)
                    .Replace("{#in_label}", in_label)
                    .Replace("{#in_current_org}", in_current_org);

            Item itmCurr = inn.applySQL(sql);

            if (itmCurr.getItemCount() > 0 && !itmCurr.isError())
            {
                string in_limit_current = itmCurr.getProperty("in_limit_current", ""); //限制單位人數
                int reg_curr_qty = GetIntVal(itmCurr.getProperty("reg_curr_qty", ""));//已報單位人數
                question = itmCurr.getProperty("in_questions", "");
                int limit_curr_qty = GetIntVal(in_limit_current);
                if (reg_curr_qty >= limit_curr_qty)
                {
                    return "<span style='color:red;'>報名失敗</span>，<br>" + question + "：" + in_label + "，單位可報人數已滿。<br>";
                }
            }


            //判斷項目正取與備取人數
            sql = @" SELECT t2.in_property, IN_LABEL, in_admission, IN_QUESTIONS, reg_adm_qty , reg_pre_qty,in_preparation
                    FROM innovator.IN_Cla_MEETING_SURVEYS AS T1 WITH(NOLOCK)
                    JOIN  innovator.IN_CLA_SURVEY AS T2 WITH(NOLOCK)
                    ON T1.RELATED_ID = T2.id
                    JOIN innovator.IN_Cla_SURVEY_OPTION AS T3 WITH(NOLOCK)
                    ON T2.id = T3.SOURCE_ID
                    LEFT JOIN
                    (
                    	SELECT '{#in_property}' as in_property, count(*) AS reg_adm_qty 
                    	FROM innovator.in_cla_meeting_user　WITH(NOLOCK)
                        WHERE SOURCE_ID='{#meeting_id}' 
                        AND {#in_property} ='{#in_label}' 
                        AND  IN_NOTE_STATE ='official'
                    ) AS T4
                    ON t2.in_property = T4.in_property
                    LEFT JOIN
                    (
                    	SELECT '{#in_property}' as in_property, count(*) AS reg_pre_qty 
                    	FROM innovator.in_cla_meeting_user　WITH(NOLOCK)
                        WHERE SOURCE_ID='{#meeting_id}' 
                        AND {#in_property} ='{#in_label}' 
                        AND  IN_NOTE_STATE ='waiting'
                    ) AS T5
                    ON t2.in_property = T5.in_property
                    WHERE  T1.SOURCE_ID ='{#meeting_id}' 
                    AND t2.in_property='{#in_property}' 
                    AND IN_LABEL='{#in_label}'  
                    AND  ISNULL(in_admission,-1) >= 0　
                    ORDER BY t2.in_property";

            sql = sql.Replace("{#in_property}", in_property)
                    .Replace("{#meeting_id}", meeting_id)
                    .Replace("{#in_label}", in_label);


            Item itmSurvery = inn.applySQL(sql);

            if (itmSurvery.getItemCount() > 0 && !itmSurvery.isError())
            {

                string in_admission = itmSurvery.getProperty("in_admission", ""); //限制正取人數
                int reg_adm_qty = GetIntVal(itmSurvery.getProperty("reg_adm_qty", ""));//正取人數
                string in_preparation = itmSurvery.getProperty("in_preparation", ""); //限制備取人數
                int reg_pre_qty = GetIntVal(itmSurvery.getProperty("reg_pre_qty", ""));//備取人數
                question = itmSurvery.getProperty("in_questions", "");

                int limit_adm_qty = GetIntVal(in_admission);
                int limit_pre_qty = GetIntVal(in_preparation);
                if (reg_adm_qty >= limit_adm_qty)
                {
                    if (reg_pre_qty >= limit_pre_qty)
                    {
                        return "<span style='color:red;'>報名失敗</span>，<br>" + question + "：" + in_label + "，人數已滿。<br>";
                    }
                    else
                    {
                        return "報名結果為<span style='color:red;'>備取</span>，<br>" + question + "：" + in_label + "，正取人數已滿。<br>";
                    }
                }

            }
            return "";
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}