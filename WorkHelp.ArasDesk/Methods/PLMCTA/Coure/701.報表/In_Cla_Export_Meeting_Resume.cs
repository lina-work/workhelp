﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_Cla_Export_Meeting_Resume : Item
    {
        public In_Cla_Export_Meeting_Resume(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 學員履歷匯出功能
                人員: lina
                版本:
                    - 2022-03-21: 審核名冊修正學科成績、術科成績 (lina)
                    - 2021-10-05: 修改 (lina)
                    - 2021-09-23: 修改 by David
                    - 2020-03-26: 創建
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Cla_Export_Meeting_Resume";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string meetingid = itmR.getProperty("meeting_id", "");
            string id = itmR.getProperty("id", "");
            string type = itmR.getProperty("type", "");
            string range = itmR.getProperty("range", "0");

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                strMethodName = strMethodName,
                inn = inn,
                meeting_id = meetingid,
                type = type,
                id = id
            };

            string aml = "<AML>"
                + "<Item type='in_cla_meeting' action='get' id='" + cfg.meeting_id + "'"
                + " select='in_title, in_url, in_register_url, in_need_receipt, in_meeting_type, in_echelon, in_annual, in_certificate_no, in_date_s, in_date_e, in_coursh_hours, in_address, in_seminar_type'"
                + "></Item></AML>";
            cfg.itmMeeting = inn.applyAML(aml);

            cfg.in_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.in_url = cfg.itmMeeting.getProperty("in_url", "");
            cfg.in_register_url = cfg.itmMeeting.getProperty("in_register_url", "");
            cfg.in_meeting_type = cfg.itmMeeting.getProperty("in_meeting_type", "");
            cfg.in_echelon = cfg.itmMeeting.getProperty("in_echelon", "");
            cfg.in_annual = cfg.itmMeeting.getProperty("in_annual", "");
            cfg.certificate_no = cfg.itmMeeting.getProperty("in_certificate_no", "");
            cfg.in_date_s = cfg.itmMeeting.getProperty("in_date_s", "");
            cfg.in_date_e = cfg.itmMeeting.getProperty("in_date_e", "");
            cfg.in_coursh_hours = cfg.itmMeeting.getProperty("in_coursh_hours", "");
            cfg.in_address = cfg.itmMeeting.getProperty("in_address", "");
            cfg.seminar_type = cfg.itmMeeting.getProperty("in_seminar_type", "");
            cfg.range = GetInt(range);

            switch (type)
            {
                case "seminar":
                    exportCertificate(cfg, itmR);
                    break;

                case "degreeB":
                    exportDegreeB(cfg, itmR);
                    break;

                case "degreeS":
                    exportDegreeS(cfg, itmR);
                    break;

                case "importXls":
                    string users = itmR.getProperty("json", "");
                    if (users != null && users != "")
                    {
                        importScore(cfg, itmR, users);
                    }
                    else
                    {
                        throw new Exception("匯入成績異常!");
                    }
                    break;

                case "sign": //現場簽到表
                    exportSign(cfg, itmR);
                    break;

                case "ass": //考核名冊
                    exportAss(cfg, itmR);
                    break;

                case "verify": //審核名冊
                    exportVerify(cfg, itmR);
                    break;

                case "prove": //學員證
                    exportProve(cfg, itmR);
                    break;

                default: //學習履歷
                    Item itmMeetingResume = inn.newItem("In_Cla_Meeting_Resume", "get");
                    itmMeetingResume.setProperty("source_id", meetingid);
                    itmR = itmMeetingResume.apply();
                    break;
            }

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        //匯出學員證
        private void exportProve(TConfig cfg, Item itmReturn)
        {
            var exp = ExportInfo(cfg, "prove_path");

            string pdfName = cfg.in_title + "_學員證";
            string pdfFile = exp.exportPath + pdfName + ".xlsx";

            Item itmUsers = GetMUsers(cfg);

            if (itmUsers.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "匯出學員證發生錯誤");
                throw new Exception("匯出學員證發生錯誤");
            }

            if (itmUsers.getItemCount() < 1)
            {
                throw new Exception("目前暫無學員證供下載!");
            }


            Spire.Xls.Workbook book = new Spire.Xls.Workbook();
            book.LoadFromFile(exp.templatePath);
            Spire.Xls.Worksheet sheetTemplate = book.Worksheets[0];

            AppendProve(book, sheetTemplate, cfg, itmUsers);

            sheetTemplate.Remove();
            book.SaveToFile(pdfFile, Spire.Xls.ExcelVersion.Version2010);

            itmReturn.setProperty("pdf_name", pdfName + ".xlsx");
        }

        //匯出審核名冊
        private void exportVerify(TConfig cfg, Item itmReturn)
        {
            var exp = ExportInfo(cfg, "verify_path");

            string pdfName = cfg.in_title + "_審核名冊";
            string pdfFile = exp.exportPath + pdfName + ".xlsx";

            Item itmUsers = GetMUsers(cfg);

            if (itmUsers.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "匯出審核名冊發生錯誤");
                throw new Exception("匯出審核名冊發生錯誤");
            }

            if (itmUsers.getItemCount() < 1)
            {
                throw new Exception("目前暫無審核名冊供下載!");
            }


            Spire.Xls.Workbook book = new Spire.Xls.Workbook();
            book.LoadFromFile(exp.templatePath);
            Spire.Xls.Worksheet sheetTemplate = book.Worksheets[0];

            AppendVerify(book, sheetTemplate, cfg, itmUsers);

            sheetTemplate.Remove();
            book.SaveToFile(pdfFile, Spire.Xls.ExcelVersion.Version2010);

            itmReturn.setProperty("pdf_name", pdfName + ".xlsx");
        }

        //匯出考核名冊
        private void exportAss(TConfig cfg, Item itmReturn)
        {
            var exp = ExportInfo(cfg, "ass_path");

            string pdfName = cfg.in_title + "_考核名冊";
            string pdfFile = exp.exportPath + pdfName + ".xlsx";

            Item itmUsers = GetMUsers(cfg);

            if (itmUsers.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "匯出考核名冊發生錯誤");
                throw new Exception("匯出考核名冊發生錯誤");
            }

            if (itmUsers.getItemCount() < 1)
            {
                throw new Exception("目前暫無考核名冊供下載!");
            }


            Spire.Xls.Workbook book = new Spire.Xls.Workbook();
            book.LoadFromFile(exp.templatePath);
            Spire.Xls.Worksheet sheetTemplate = book.Worksheets[0];

            AppendAss(book, sheetTemplate, cfg, itmUsers);

            sheetTemplate.Remove();
            book.SaveToFile(pdfFile, Spire.Xls.ExcelVersion.Version2010);
            itmReturn.setProperty("pdf_name", pdfName + ".xlsx");
        }

        //匯出簽到表
        private void exportSign(TConfig cfg, Item itmReturn)
        {
            var exp = ExportInfo(cfg, "sign_path");

            string pdfName = cfg.in_title + "_簽到表";
            string pdfFile = exp.exportPath + pdfName + ".xlsx";

            Item itmUsers = GetMUsers(cfg);
            if (itmUsers.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "匯出簽到表發生錯誤");
                throw new Exception("匯出簽到表發生錯誤");
            }

            if (itmUsers.getItemCount() < 1)
            {
                throw new Exception("目前暫無學員供下載!");
            }

            Spire.Xls.Workbook book = new Spire.Xls.Workbook();
            book.LoadFromFile(exp.templatePath);
            Spire.Xls.Worksheet sheetTemplate = book.Worksheets[0];

            AppendSign(book, sheetTemplate, cfg, itmUsers);

            sheetTemplate.Remove();

            book.SaveToFile(pdfFile, Spire.Xls.ExcelVersion.Version2010);

            itmReturn.setProperty("pdf_name", pdfName + ".xlsx");
        }

        //匯出晉段小段證
        private void exportDegreeS(TConfig cfg, Item itmReturn)
        {
            var exp = ExportInfo(cfg, "DegreeS_path");

            string pdfName = cfg.in_title + "_晉段小段證";
            string pdfFile = exp.exportPath + pdfName + ".pdf";

            Item itmUsers = GetUsers(cfg);
            if (itmUsers.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "匯出晉段小段證資料發生錯誤");
                throw new Exception("匯出晉段小段證發生錯誤");
            }

            if (itmUsers.getItemCount() < 1)
            {
                throw new Exception("目前暫無合格學員供下載!");
            }

            Spire.Xls.Workbook book = new Spire.Xls.Workbook();
            book.LoadFromFile(exp.templatePath);
            Spire.Xls.Worksheet sheetTemplate = book.Worksheets[0];

            AppendCertificateDegreeS(book, sheetTemplate, cfg, itmUsers);

            sheetTemplate.Remove();
            book.SaveToFile(pdfFile, Spire.Xls.FileFormat.PDF);

            itmReturn.setProperty("pdf_name", pdfName + ".pdf");
        }

        //匯出晉段大段證
        private void exportDegreeB(TConfig cfg, Item itmReturn)
        {
            var exp = ExportInfo(cfg, "DegreeB_path");

            string pdfName = cfg.in_title + "_晉段大段證";
            string pdfFile = exp.exportPath + pdfName + ".pdf";

            Item itmUsers = GetUsers(cfg);

            if (itmUsers.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "匯出晉段大段證資料發生錯誤");
                throw new Exception("匯出晉段大段證發生錯誤");
            }

            if (itmUsers.getItemCount() < 1)
            {
                throw new Exception("目前暫無合格學員供下載!");
            }


            Spire.Xls.Workbook book = new Spire.Xls.Workbook();
            book.LoadFromFile(exp.templatePath);
            Spire.Xls.Worksheet sheetTemplate = book.Worksheets[0];

            AppendCertificateDegreeB(book, sheetTemplate, cfg, itmUsers);

            sheetTemplate.Remove();
            book.SaveToFile(pdfFile, Spire.Xls.FileFormat.PDF);

            itmReturn.setProperty("pdf_name", pdfName + ".pdf");
        }

        //匯出結業證書
        private void exportCertificate(TConfig cfg, Item itmReturn)
        {
            var exp = ExportInfo(cfg, "certificate_path");

            string pdfName = cfg.in_title + "_結業證書";
            string pdfFile = exp.exportPath + pdfName + ".pdf";

            Item itmUsers = GetUsers(cfg);

            if (itmUsers.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "匯出結業證書資料發生錯誤");
                throw new Exception("匯出結業證書發生錯誤");
            }

            if (itmUsers.getItemCount() < 1)
            {
                throw new Exception("目前暫無合格學員供下載!");
            }


            Spire.Xls.Workbook book = new Spire.Xls.Workbook();
            book.LoadFromFile(exp.templatePath);
            Spire.Xls.Worksheet sheetTemplate = book.Worksheets[0];

            AppendCertificate(book, sheetTemplate, cfg, itmUsers);

            sheetTemplate.Remove();
            book.SaveToFile(pdfFile, Spire.Xls.FileFormat.PDF);

            itmReturn.setProperty("pdf_name", pdfName + ".pdf");
        }

        private Item GetMUsers(TConfig cfg)
        {
            string sql = @"
                SELECT 
                    t1.*
                    , t2.in_note AS 'mt_resume_note'
					, t2.in_score_O_5 AS 'know_score'
					, t2.in_score_O_3 AS 'tech_score'
                    , t3.id AS 'rid'
					, t3.in_instructor_level
					, t3.in_instructor_id
					, t3.in_referee_level
					, t3.in_referee_id
					, t3.in_poomsae_level
					, t3.in_poomsae_id
                FROM 
                    IN_CLA_MEETING_USER AS t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_CLA_MEETING_RESUME AS t2 WITH(NOLOCK)
                    ON t2.in_user = t1.id
                LEFT OUTER JOIN
                    IN_RESUME AS t3 WITH(NOLOCK)
                    ON t3.in_sno = t1.in_sno
                WHERE 
                    t1.source_id = '{#meeting_id}' 
                    AND t1.in_note_state ='official'
                ORDER BY 
                    t1.in_name_num
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        //取得測驗人員清單
        private Item GetUsers(TConfig cfg)
        {
            string sqlFilter = "";
            string degreeStr = "";
            string rangeStr = "";

            if (cfg.type == "seminar" && cfg.id != "")
            {
                sqlFilter = " AND id IN (" + cfg.id + ")";
            }
            else if (cfg.type == "degreeB" || cfg.type == "degreeS")
            {
                degreeStr = @" LEFT JOIN (
                    SELECT T1.id as user_id,in_l1,T1.in_birth,T1.in_sno ,T1.in_en_name , T1.in_area , T2.in_date_e, T2.in_echelon
                    FROM in_cla_meeting_user AS T1  WITH (NOLOCK)
                    JOIN in_cla_meeting AS T2  WITH (NOLOCK)
                    ON T1.SOURCE_ID = T2.id
                    WHERE source_id = '{#meeting_id}' 
                ) AS L1
                ON LIST.IN_USER = L1.user_id  
                ORDER BY in_moe_no";

                // sqlFilter =" AND in_moe_no IS NOT NULL";

            }
            else
            {
                rangeStr = " WHERE rowno > {#page_s} and rowno <= {#page_e}";
                // sqlFilter =" AND in_stat_0 = 'O'";
            }

            // string sql = @"SELECT id, in_name_s FROM In_Cla_Meeting_Resume
            //               WHERE source_id = '{#meeting_id}' {#sqlFilter}";

            string sql = @"
                SELECT * FROM 
                (
                    SELECT rowno=ROW_NUMBER() OVER (ORDER BY id ),id, in_name_s, IN_USER , in_moe_no
                    FROM In_Cla_Meeting_Resume WITH (NOLOCK)
                    WHERE source_id = '{#meeting_id}' {#sqlFilter}
                ) AS LIST
                " + degreeStr + rangeStr;

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                        .Replace("{#sqlFilter}", sqlFilter)
                        .Replace("{#page_s}", ((cfg.range - 1) * 80).ToString())
                        .Replace("{#page_e}", (cfg.range * 80).ToString());

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private void AppendProve(Spire.Xls.Workbook book, Spire.Xls.Worksheet sheetTemplate, TConfig cfg, Item itmUsers)
        {
            int user_count = itmUsers.getItemCount();
            if (user_count != 0)
            {
                Spire.Xls.Worksheet sheet = book.CreateEmptySheet();
                sheet.CopyFrom(sheetTemplate);

                int j = 0;

                for (int i = 0; i < user_count; i++)
                {
                    Item itmUser = itmUsers.getItemByIndex(i);

                    string in_name = itmUser.getProperty("in_name", "");
                    string in_name_num = itmUser.getProperty("in_name_num", "");//學員編號


                    //   "110年2月27日~2月27日"

                    string dateStr = (Convert.ToInt32(Convert.ToDateTime(cfg.in_date_s).ToString("yyyy")) - 1911).ToString() + "年"
                                    + Convert.ToDateTime(cfg.in_date_s).ToString("M月d日") + "~"
                                    + Convert.ToDateTime(cfg.in_date_e).ToString("M月d日");

                    if (i % 8 == 0 && i != 0)
                    {

                        sheet.PageSetup.FitToPagesWide = 1;
                        sheet.PageSetup.FitToPagesTall = 1;
                        sheet.PageSetup.TopMargin = 0.3;
                        sheet.PageSetup.LeftMargin = 0.4;
                        sheet.PageSetup.RightMargin = 0.1;
                        sheet.PageSetup.BottomMargin = 0.3;

                        //適合頁面
                        // book.ConverterSetting.SheetFitToPage = true;
                        sheet = book.CreateEmptySheet();
                        sheet.CopyFrom(sheetTemplate);

                        j = 0;

                    }
                    sheet.Range["A1"].Text = cfg.in_title.Replace("中華民國跆拳道協會", "").Trim();
                    sheet.Range["B1"].Text = "學員證";
                    sheet.Range["C1"].Text = dateStr;
                    sheet.Range["D1"].Text = cfg.in_address;

                    switch (j)
                    {
                        case 0:
                            // sheet.Range["A2"].Text = (i + 1).ToString() ;
                            sheet.Range["A2"].Text = in_name_num;
                            sheet.Range["A3"].Text = in_name;
                            break;
                        case 1:
                            // sheet.Range["B2"].Text = (i + 1).ToString() ;
                            sheet.Range["B2"].Text = in_name_num;
                            sheet.Range["B3"].Text = in_name;
                            break;
                        case 2:
                            // sheet.Range["C2"].Text = (i + 1).ToString() ;
                            sheet.Range["C2"].Text = in_name_num;
                            sheet.Range["C3"].Text = in_name;
                            break;
                        case 3:
                            // sheet.Range["D2"].Text = (i + 1).ToString() ;
                            sheet.Range["D2"].Text = in_name_num;
                            sheet.Range["D3"].Text = in_name;
                            break;
                        case 4:
                            // sheet.Range["E2"].Text = (i + 1).ToString() ;
                            sheet.Range["E2"].Text = in_name_num;
                            sheet.Range["E3"].Text = in_name;
                            break;
                        case 5:
                            // sheet.Range["F2"].Text = (i + 1).ToString() ;
                            sheet.Range["F2"].Text = in_name_num;
                            sheet.Range["F3"].Text = in_name;
                            break;
                        case 6:
                            // sheet.Range["G2"].Text = (i + 1).ToString() ;
                            sheet.Range["G2"].Text = in_name_num;
                            sheet.Range["G3"].Text = in_name;
                            break;
                        case 7:
                            // sheet.Range["H2"].Text = (i + 1).ToString() ;
                            sheet.Range["H2"].Text = in_name_num;
                            sheet.Range["H3"].Text = in_name;
                            break;

                    }

                    j++;

                }

                for (int i = j; i < 8; i++)
                {


                    switch (i)
                    {
                        case 0:
                            sheet.Range["A2"].Text = "";
                            sheet.Range["A3"].Text = "";
                            break;
                        case 1:
                            sheet.Range["B2"].Text = "";
                            sheet.Range["B3"].Text = "";
                            break;
                        case 2:
                            sheet.Range["C2"].Text = "";
                            sheet.Range["C3"].Text = "";
                            break;
                        case 3:
                            sheet.Range["D2"].Text = "";
                            sheet.Range["D3"].Text = "";
                            break;
                        case 4:
                            sheet.Range["E2"].Text = "";
                            sheet.Range["E3"].Text = "";
                            break;
                        case 5:
                            sheet.Range["F2"].Text = "";
                            sheet.Range["F3"].Text = "";
                            break;
                        case 6:
                            sheet.Range["G2"].Text = "";
                            sheet.Range["G3"].Text = "";
                            break;
                        case 7:
                            sheet.Range["H2"].Text = "";
                            sheet.Range["H3"].Text = "";
                            break;

                    }
                }


                sheet.PageSetup.FitToPagesWide = 1;
                sheet.PageSetup.FitToPagesTall = 1;
                sheet.PageSetup.TopMargin = 0.3;
                sheet.PageSetup.LeftMargin = 0.4;
                sheet.PageSetup.RightMargin = 0.1;
                sheet.PageSetup.BottomMargin = 0.3;

                //適合頁面
                // book.ConverterSetting.SheetFitToPage = true;

            }
        }

        private void AppendVerify(Spire.Xls.Workbook book, Spire.Xls.Worksheet sheetTemplate, TConfig cfg, Item itmUsers)
        {
            int user_count = itmUsers.getItemCount();
            if (user_count <= 0) return;
            
            Spire.Xls.Worksheet sheet = book.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Range["A1"].Text = cfg.in_title + "審核名冊";
            sheet.Range["A25"].Text = DateTime.Now.ToString("yyyy年MM月dd日");

            string cert_prop = "";
            if (cfg.seminar_type.Contains("cocah"))
            {
                sheet.Range["E2"].Text = "教練級數 證號";
                cert_prop = "in_instructor_id";
            }
            else if (cfg.seminar_type.Contains("referee"))
            {
                sheet.Range["E2"].Text = "裁判級數 證號";
                cert_prop = "in_referee_id";
            }
            else if (cfg.seminar_type.Contains("poomsae"))
            {
                sheet.Range["E2"].Text = "裁判級數 證號";
                cert_prop = "in_poomsae_id";
            }
            else
            {
                throw new Exception("講習類型未設定");
            }

            int j = 0;


            for (int i = 0; i < user_count; i++)
            {
                Item itmUser = itmUsers.getItemByIndex(i);

                string in_name = itmUser.getProperty("in_name", "");
                string in_name_num = itmUser.getProperty("in_name_num", ""); // 學員編號
                string in_degree = itmUser.getProperty("in_degree_label", "");
                string in_note = itmUser.getProperty("mt_resume_note", ""); // 學習履歷-備註
                if (in_degree == "")
                {
                    in_degree = itmUser.getProperty("degree_lab", "");
                }

                string in_birth = "";
                if (itmUser.getProperty("in_birth", "") != "")
                {
                    in_birth = Convert.ToDateTime(itmUser.getProperty("in_birth", "")).AddHours(8).ToString("yyyy/MM/dd");
                }
                string instructor_level = itmUser.getProperty("in_instructor_level", "");
                string referee_level = itmUser.getProperty("in_referee_level", "");
                string poomsae_level = itmUser.getProperty("in_poomsae_level", "");
                string cert_id = itmUser.getProperty(cert_prop, "");

                if (instructor_level != "" && !instructor_level.Contains("級"))
                {
                    instructor_level += "級";
                }

                if (referee_level != "" && !referee_level.Contains("級"))
                {
                    referee_level += "級";
                }

                if (poomsae_level != "" && !poomsae_level.Contains("級"))
                {
                    poomsae_level += "級";
                }

                if (i % 10 == 0 && i != 0)
                {
                    sheet.PageSetup.FitToPagesWide = 1;
                    sheet.PageSetup.FitToPagesTall = 1;
                    sheet.PageSetup.TopMargin = 0.3;
                    sheet.PageSetup.LeftMargin = 0.5;
                    sheet.PageSetup.RightMargin = 0.5;
                    sheet.PageSetup.BottomMargin = 0.5;

                    //適合頁面
                    book.ConverterSetting.SheetFitToPage = true;

                    sheet = book.CreateEmptySheet();
                    sheet.CopyFrom(sheetTemplate);
                    sheet.Range["A1"].Text = cfg.in_title + "審核名冊";
                    sheet.Range["A25"].Text = DateTime.Now.ToString("yyyy年MM月dd日");

                    j = 0;

                }

                // sheet.Range["A"+(j * 2 + 4).ToString()].Text = (i+1).ToString();
                sheet.Range["A" + (j * 2 + 4).ToString()].Text = in_name_num;
                sheet.Range["B" + (j * 2 + 4).ToString()].Text = in_name;
                sheet.Range["C" + (j * 2 + 4).ToString()].Text = in_birth;
                sheet.Range["D" + (j * 2 + 4).ToString()].Text = in_degree;
                // sheet.Range["E"+(j * 2 + 4).ToString()].Text = instructor_level  ;
                // sheet.Range["E"+(j * 2 + 5).ToString()].Text = instructor_id ;

                //講習紀錄
                string rid = itmUser.getProperty("rid", "");
                string sql = "";

                if (cfg.seminar_type.Contains("coach"))
                {
                    sql = @" SELECT * FROM In_Resume_Coach WITH (NOLOCK)
                        WHERE source_id = '{#rid}' AND in_year IS NOT NULL
                        ORDER BY in_year DESC";
                    sheet.Range["E" + (j * 2 + 4).ToString()].Text = instructor_level;
                    sheet.Range["E" + (j * 2 + 5).ToString()].Text = cert_id;
                }
                else if (cfg.seminar_type == "referee")
                {
                    sql = @" SELECT * FROM In_Resume_Referee WITH (NOLOCK)
                        WHERE source_id = '{#rid}' AND in_year IS NOT NULL
                        ORDER BY in_year DESC";
                    sheet.Range["E" + (j * 2 + 4).ToString()].Text = referee_level;
                    sheet.Range["E" + (j * 2 + 5).ToString()].Text = cert_id;
                }
                else if (cfg.seminar_type == "poomsae")
                {
                    sql = @" SELECT * FROM In_Resume_Poomsae WITH (NOLOCK)
                        WHERE source_id = '{#rid}' AND in_year IS NOT NULL
                        ORDER BY in_year DESC";
                    sheet.Range["E" + (j * 2 + 4).ToString()].Text = poomsae_level;
                    sheet.Range["E" + (j * 2 + 5).ToString()].Text = cert_id;
                }
                else
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "講習類型未定義");
                    throw new Exception("講習類型未定義");
                }

                sql = sql.Replace("{#rid}", rid);

                Item itmInsts = cfg.inn.applySQL(sql);

                int itmInstsCount = itmInsts.getItemCount();

                if (!itmInsts.isError() && itmInstsCount > 0)
                {
                    for (int k = 0; k < itmInstsCount; k++)
                    {
                        Item itmInst = itmInsts.getItemByIndex(k);

                        string in_year = itmInst.getProperty("in_year", "");
                        if (in_year != "")
                        {
                            in_year = (Convert.ToInt32(in_year) - 1911).ToString();
                        }

                        switch (k)
                        {
                            case 0:
                                sheet.Range["F" + (j * 2 + 4).ToString()].Text = in_year;
                                break;
                            case 1:
                                sheet.Range["G" + (j * 2 + 4).ToString()].Text = in_year;
                                break;
                            case 2:
                                sheet.Range["H" + (j * 2 + 4).ToString()].Text = in_year;
                                break;
                            case 3:
                                sheet.Range["I" + (j * 2 + 4).ToString()].Text = in_year;
                                break;
                            case 4:
                                sheet.Range["J" + (j * 2 + 4).ToString()].Text = in_year;
                                break;
                            case 5:
                                sheet.Range["K" + (j * 2 + 4).ToString()].Text = in_year;
                                break;
                            case 6:
                                sheet.Range["F" + (j * 2 + 5).ToString()].Text = in_year;
                                break;
                            case 7:
                                sheet.Range["G" + (j * 2 + 5).ToString()].Text = in_year;
                                break;
                            case 8:
                                sheet.Range["H" + (j * 2 + 5).ToString()].Text = in_year;
                                break;
                            case 9:
                                sheet.Range["I" + (j * 2 + 5).ToString()].Text = in_year;
                                break;
                            case 10:
                                sheet.Range["J" + (j * 2 + 5).ToString()].Text = in_year;
                                break;
                            case 11:
                                sheet.Range["K" + (j * 2 + 5).ToString()].Text = in_year;
                                break;
                        }
                    }
                }

                //學科
                sheet.Range["L" + (j * 2 + 4).ToString()].Text = itmUser.getProperty("know_score", "");
                //術科
                sheet.Range["M" + (j * 2 + 4).ToString()].Text = itmUser.getProperty("tech_score", "");
                //備註
                sheet.Range["O" + (j * 2 + 4).ToString()].Text = in_note;

                j++;

            }
            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 1;
            sheet.PageSetup.TopMargin = 0.3;
            sheet.PageSetup.LeftMargin = 0.5;
            sheet.PageSetup.RightMargin = 0.5;
            sheet.PageSetup.BottomMargin = 0.5;

            //適合頁面
            book.ConverterSetting.SheetFitToPage = true;

        }

        private void AppendAss(Spire.Xls.Workbook book, Spire.Xls.Worksheet sheetTemplate, TConfig cfg, Item itmUsers)
        {
            int user_count = itmUsers.getItemCount();
            if (user_count != 0)
            {
                Spire.Xls.Worksheet sheet = book.CreateEmptySheet();
                sheet.CopyFrom(sheetTemplate);
                sheet.Range["A1"].Text = cfg.in_title + "考核名冊";
                sheet.Range["A25"].Text = DateTime.Now.ToString("yyyy年MM月dd日");

                int j = 0;

                for (int i = 0; i < user_count; i++)
                {
                    Item itmUser = itmUsers.getItemByIndex(i);

                    string in_name = itmUser.getProperty("in_name", "");
                    string in_name_num = itmUser.getProperty("in_name_num", ""); // 學員編號
                    string in_degree = itmUser.getProperty("in_degree_label", "");
                    if (in_degree == "")
                    {
                        in_degree = itmUser.getProperty("degree_lab", "");
                    }
                    if (i % 10 == 0 && i != 0)
                    {

                        sheet.PageSetup.FitToPagesWide = 1;
                        sheet.PageSetup.FitToPagesTall = 1;
                        sheet.PageSetup.TopMargin = 0.3;
                        sheet.PageSetup.LeftMargin = 0.5;
                        sheet.PageSetup.RightMargin = 0.5;
                        sheet.PageSetup.BottomMargin = 0.5;

                        //適合頁面
                        book.ConverterSetting.SheetFitToPage = true;

                        sheet = book.CreateEmptySheet();
                        sheet.CopyFrom(sheetTemplate);
                        sheet.Range["A1"].Text = cfg.in_title + "考核名冊";
                        sheet.Range["A25"].Text = DateTime.Now.ToString("yyyy年MM月dd日");

                        j = 0;

                    }

                    // sheet.Range["A"+(j*2 + 4).ToString()].Text = (i+1).ToString();
                    sheet.Range["A" + (j * 2 + 4).ToString()].Text = in_name_num;
                    sheet.Range["B" + (j * 2 + 4).ToString()].Text = in_name;
                    sheet.Range["C" + (j * 2 + 4).ToString()].Text = in_degree;
                    j++;

                }
                sheet.PageSetup.FitToPagesWide = 1;
                sheet.PageSetup.FitToPagesTall = 1;
                sheet.PageSetup.TopMargin = 0.3;
                sheet.PageSetup.LeftMargin = 0.5;
                sheet.PageSetup.RightMargin = 0.5;
                sheet.PageSetup.BottomMargin = 0.5;

                //適合頁面
                book.ConverterSetting.SheetFitToPage = true;

            }
        }

        private void AppendSign(Spire.Xls.Workbook book, Spire.Xls.Worksheet sheetTemplate, TConfig cfg, Item itmUsers)
        {
            int user_count = itmUsers.getItemCount();
            if (user_count != 0)
            {
                Spire.Xls.Worksheet sheet = book.CreateEmptySheet();
                sheet.CopyFrom(sheetTemplate);
                sheet.Range["A1"].Text = cfg.in_title + "簽到表";
                sheet.Range["A13"].Text = DateTime.Now.ToString("yyyy年MM月dd日");
                DateTime s_date = Convert.ToDateTime(cfg.in_date_s);
                DateTime e_date = Convert.ToDateTime(cfg.in_date_e);
                int days = new TimeSpan(e_date.Ticks - s_date.Ticks).Days;

                for (int d = 0; d <= days; d++)
                {

                    int j = 0;
                    if (d % 4 != 0)
                    {
                        continue;
                    }
                    else if (d == 0)
                    {
                        sheet.Range["E2"].Text = checkEDate(s_date.AddDays(0), e_date);
                        sheet.Range["H2"].Text = checkEDate(s_date.AddDays(1), e_date);
                        sheet.Range["K2"].Text = checkEDate(s_date.AddDays(2), e_date);
                        sheet.Range["N2"].Text = checkEDate(s_date.AddDays(3), e_date);
                    }

                    if (d % 4 == 0 && d != 0)
                    {
                        s_date = s_date.AddDays(4);
                        sheet.PageSetup.FitToPagesWide = 1;
                        sheet.PageSetup.FitToPagesTall = 1;
                        sheet.PageSetup.TopMargin = 0.3;
                        sheet.PageSetup.LeftMargin = 0.5;
                        sheet.PageSetup.RightMargin = 0.5;
                        sheet.PageSetup.BottomMargin = 0.5;

                        //適合頁面
                        book.ConverterSetting.SheetFitToPage = true;

                        sheet = book.CreateEmptySheet();
                        sheet.CopyFrom(sheetTemplate);
                        sheet.Range["A1"].Text = cfg.in_title + "簽到表";
                        sheet.Range["A13"].Text = DateTime.Now.ToString("yyyy年MM月dd日");
                        sheet.Range["E2"].Text = checkEDate(s_date.AddDays(0), e_date);
                        sheet.Range["H2"].Text = checkEDate(s_date.AddDays(1), e_date);
                        sheet.Range["K2"].Text = checkEDate(s_date.AddDays(2), e_date);
                        sheet.Range["N2"].Text = checkEDate(s_date.AddDays(3), e_date);

                        j = 0;
                    }


                    for (int i = 0; i < user_count; i++)
                    {
                        Item itmUser = itmUsers.getItemByIndex(i);

                        string in_name = itmUser.getProperty("in_name", "");
                        string in_name_num = itmUser.getProperty("in_name_num", "");//學員編號

                        if (i % 10 == 0 && i != 0)
                        {

                            sheet.PageSetup.FitToPagesWide = 1;
                            sheet.PageSetup.FitToPagesTall = 1;
                            sheet.PageSetup.TopMargin = 0.3;
                            sheet.PageSetup.LeftMargin = 0.5;
                            sheet.PageSetup.RightMargin = 0.5;
                            sheet.PageSetup.BottomMargin = 0.5;

                            //適合頁面
                            book.ConverterSetting.SheetFitToPage = true;

                            sheet = book.CreateEmptySheet();
                            sheet.CopyFrom(sheetTemplate);
                            sheet.Range["A1"].Text = cfg.in_title + "簽到表";
                            sheet.Range["A13"].Text = DateTime.Now.ToString("yyyy年MM月dd日");
                            sheet.Range["E2"].Text = checkEDate(s_date.AddDays(0), e_date);
                            sheet.Range["H2"].Text = checkEDate(s_date.AddDays(1), e_date);
                            sheet.Range["K2"].Text = checkEDate(s_date.AddDays(2), e_date);
                            sheet.Range["N2"].Text = checkEDate(s_date.AddDays(3), e_date);

                            j = 0;

                        }

                        // sheet.Range["A"+(j+3).ToString()].Text = (i+1).ToString();
                        sheet.Range["A" + (j + 3).ToString()].Text = in_name_num;
                        sheet.Range["B" + (j + 3).ToString()].Text = in_name;
                        j++;

                    }
                    sheet.PageSetup.FitToPagesWide = 1;
                    sheet.PageSetup.FitToPagesTall = 1;
                    sheet.PageSetup.TopMargin = 0.3;
                    sheet.PageSetup.LeftMargin = 0.5;
                    sheet.PageSetup.RightMargin = 0.5;
                    sheet.PageSetup.BottomMargin = 0.5;

                    //適合頁面
                    book.ConverterSetting.SheetFitToPage = true;

                }
            }
        }

        private void AppendCertificateDegreeS(Spire.Xls.Workbook book, Spire.Xls.Worksheet sheetTemplate, TConfig cfg, Item itmUsers)
        {
            int user_count = itmUsers.getItemCount();
            string[] col = { "P", "Q", "R", "S", "T", "U", "V", "W" };
            if (user_count != 0)
            {
                Spire.Xls.Worksheet sheet = book.CreateEmptySheet();
                sheet.CopyFrom(sheetTemplate);
                sheet.Name = "page1";
                sheet.PageSetup.FitToPagesWide = 1;
                sheet.PageSetup.FitToPagesTall = 1;
                sheet.PageSetup.TopMargin = 0;
                sheet.PageSetup.LeftMargin = 0;
                sheet.PageSetup.RightMargin = 0;
                sheet.PageSetup.BottomMargin = 0;
                //適合頁面
                book.ConverterSetting.SheetFitToPage = true;

                for (int i = 0; i < user_count; i++)
                {
                    Item itmUser = itmUsers.getItemByIndex(i);

                    string in_name = itmUser.getProperty("in_name_s", "");

                    string in_birth = itmUser.getProperty("in_birth", "");
                    string in_moe_no = itmUser.getProperty("in_moe_no", "");
                    string birthCY = CDayFunc(in_birth, 8, "CY");
                    string birthCM = CDayFunc(in_birth, 8, "CM");
                    string birthCD = CDayFunc(in_birth, 8, "CD");
                    string in_area = itmUser.getProperty("in_area", "");
                    string in_l1 = itmUser.getProperty("in_l1", "");
                    string in_date_e = itmUser.getProperty("in_date_e", "");
                    string eCY = CDayFunc(in_date_e, 8, "CY");
                    string eCM = CDayFunc(in_date_e, 8, "CM");
                    string eCD = CDayFunc(in_date_e, 8, "CD");
                    string in_echelon = itmUser.getProperty("in_echelon", "");
                    string in_en_name = itmUser.getProperty("in_en_name", "");
                    int j = i % 8;


                    if (j == 0 && i > 0)
                    {
                        sheet = book.CreateEmptySheet();
                        sheet.CopyFrom(sheetTemplate);
                        sheet.Name = itmUser.getProperty("id", ""); ;
                        sheet.PageSetup.FitToPagesWide = 1;
                        sheet.PageSetup.FitToPagesTall = 1;
                        sheet.PageSetup.TopMargin = 0;
                        sheet.PageSetup.LeftMargin = 0;
                        sheet.PageSetup.RightMargin = 0;
                        sheet.PageSetup.BottomMargin = 0;
                        //適合頁面
                        book.ConverterSetting.SheetFitToPage = true;
                    }

                    //編號
                    sheet.Range[col[j] + "1"].Text = "編號： " + in_moe_no;
                    //段位(數字)
                    sheet.Range[col[j] + "2"].Text = "段位： " + CDegreeFunc(in_l1, "C") + " 段";

                    sheet.Range[col[j] + "3"].Text = "姓名：";
                    //姓名
                    sheet.Range[col[j] + "4"].Text = in_name;

                    sheet.Range[col[j] + "5"].Text = "出生日期：";
                    //生日年月日
                    sheet.Range[col[j] + "6"].Text = " " + CDayFunc(in_birth, 8, "CDay");

                    sheet.Range[col[j] + "7"].Text = "上列會員資格經本會於";
                    //中華民國年月日
                    sheet.Range[col[j] + "8"].Text = CDayFunc(in_date_e, 8, "CountryDay");
                    sheet.Range[col[j] + "9"].Text = "審查合格，特此證明。";
                }
            }
        }

        private void AppendCertificateDegreeB(Spire.Xls.Workbook book, Spire.Xls.Worksheet sheetTemplate, TConfig cfg, Item itmUsers)
        {
            int user_count = itmUsers.getItemCount();

            if (user_count != 0)
            {
                for (int i = 0; i < user_count; i++)
                {
                    Item itmUser = itmUsers.getItemByIndex(i);

                    string in_name = itmUser.getProperty("in_name_s", "");

                    string in_birth = itmUser.getProperty("in_birth", "");
                    string in_moe_no = itmUser.getProperty("in_moe_no", "");
                    string birthCY = CDayFunc(in_birth, 8, "CY");
                    string birthCM = CDayFunc(in_birth, 8, "CM");
                    string birthCD = CDayFunc(in_birth, 8, "CD");
                    string in_area = itmUser.getProperty("in_area", "");
                    string in_l1 = itmUser.getProperty("in_l1", "");
                    string in_date_e = itmUser.getProperty("in_date_e", "");
                    string eCY = CDayFunc(in_date_e, 8, "CY");
                    string eCM = CDayFunc(in_date_e, 8, "CM");
                    string eCD = CDayFunc(in_date_e, 8, "CD");
                    string in_echelon = itmUser.getProperty("in_echelon", "");
                    string in_en_name = itmUser.getProperty("in_en_name", "");

                    Spire.Xls.Worksheet sheet = book.CreateEmptySheet();

                    sheet.CopyFrom(sheetTemplate);
                    sheet.Name = itmUser.getProperty("id", "");
                    //姓名
                    sheet.Range["P1"].Text = " " + in_name;
                    //證號(體總證號)
                    sheet.Range["P2"].Text = " " + in_moe_no;
                    //生日_民國年
                    sheet.Range["P3"].Text = " " + birthCY;
                    //生日_月
                    sheet.Range["P4"].Text = " " + birthCM;
                    //生日_日
                    sheet.Range["P5"].Text = " " + birthCD;
                    //出生地
                    sheet.Range["P6"].Text = " " + in_area;
                    //段位(數字)
                    sheet.Range["P7"].Text = " " + CDegreeFunc(in_l1, "C");
                    //課程結束_民國年
                    sheet.Range["P8"].Text = " " + eCY;
                    //課程結束_月
                    sheet.Range["P9"].Text = " " + eCM;
                    //課程結束_日
                    sheet.Range["P10"].Text = " " + eCD;
                    //梯次
                    sheet.Range["P11"].Text = " " + in_echelon;
                    //英文名
                    sheet.Range["P13"].Text = " " + in_en_name;
                    //生日_英文格式
                    sheet.Range["P14"].Text = " " + CDayFunc(in_birth, 8, "EDay");
                    //段位(數字含英文)
                    sheet.Range["P15"].Text = " " + CDegreeFunc(in_l1, "E");
                    //課程結束_英文格式
                    sheet.Range["P16"].Text = " " + CDayFunc(in_date_e, 8, "EDay");


                    sheet.PageSetup.FitToPagesWide = 1;
                    sheet.PageSetup.FitToPagesTall = 1;
                    sheet.PageSetup.TopMargin = 0;
                    sheet.PageSetup.LeftMargin = 0;
                    sheet.PageSetup.RightMargin = 0;
                    sheet.PageSetup.BottomMargin = 0;
                    //  sheet.PageSetup.TopMargin = 0.3;
                    // sheet.PageSetup.LeftMargin = 0.5;
                    // sheet.PageSetup.RightMargin = 0.5;
                    // sheet.PageSetup.BottomMargin = 0.5;
                    //適合頁面
                    book.ConverterSetting.SheetFitToPage = true;
                }
            }
        }

        private void AppendCertificate(Spire.Xls.Workbook book, Spire.Xls.Worksheet sheetTemplate, TConfig cfg, Item itmUsers)
        {
            int user_count = itmUsers.getItemCount();
            DateTime currentTime = System.DateTime.Now;
            DateTime sTime = Convert.ToDateTime(cfg.in_date_s);
            DateTime eTime = Convert.ToDateTime(cfg.in_date_e);
            if (user_count != 0)
            {
                for (int i = 0; i < user_count; i++)
                {
                    Item itmUser = itmUsers.getItemByIndex(i);
                    string in_name = itmUser.getProperty("in_name_s", "");
                    if (in_name == "")
                    {
                        in_name = "　　　";
                    }

                    string chinese_day = convertCDay(eTime, "footer");

                    Spire.Xls.Worksheet sheet = book.CreateEmptySheet();
                    // string content = "{#in_name}　君參加本會{#in_annual}年度{#in_title}修業期滿特發此證以茲證明" ;  
                    string content = "{#in_name}　君參加本會{#in_title}修業期滿特發此證以茲證明";
                    content = content.Replace("{#in_name}", in_name)
                                    .Replace("{#in_annual}", cfg.in_annual)
                                    .Replace("{#in_title}", cfg.in_title)
                                    .Replace("中華民國跆拳道協會", "");

                    sheet.CopyFrom(sheetTemplate);
                    sheet.Name = itmUser.getProperty("id", "");
                    sheet.Range["B16"].Text = "中跆爐字第" + cfg.certificate_no + "號";                  //文號
                    sheet.Range["B18"].Text = content;                                              //內文
                    sheet.Range["A30"].Text = "修業期間：" + convertCDay(sTime, "content") + "至" + convertCDay(eTime, "content") + "共計" + cfg.in_coursh_hours + "小時";   //修業時間
                    sheet.Range["B43"].Text = chinese_day;                                          //日期

                    sheet.PageSetup.FitToPagesWide = 1;
                    sheet.PageSetup.FitToPagesTall = 1;
                    sheet.PageSetup.TopMargin = 0.3;
                    sheet.PageSetup.LeftMargin = 0.5;
                    sheet.PageSetup.RightMargin = 0.5;
                    sheet.PageSetup.BottomMargin = 0.5;

                    //適合頁面
                    book.ConverterSetting.SheetFitToPage = true;
                }
            }

        }

        private string convertCDay(DateTime CDay, string type)
        {
            string y = (CDay.Year - 1911).ToString();
            string m = CDay.Month.ToString("00");
            string d = CDay.Day.ToString("00");
            string returnStr = "";
            if (type == "footer")
            {
                returnStr = " 中華民國　" + y + "　年　" + m + "　月　" + d + "　日";
            }
            else
            {
                returnStr = y + "年" + m + "月" + d + "日";
            }

            return returnStr;
        }

        //匯入成績
        private void importScore(TConfig cfg, Item itmReturn, string users)
        {
            //這行會避免json轉換為XML十日期被轉換格式。
            Newtonsoft.Json.JsonConvert.DefaultSettings = () => new Newtonsoft.Json.JsonSerializerSettings
            {
                DateParseHandling = Newtonsoft.Json.DateParseHandling.None
            };

            List<Newtonsoft.Json.Linq.JObject> list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Newtonsoft.Json.Linq.JObject>>(users);
            int cnt = list.Count;
            for (int i = 0; i < cnt; i++)
            {
                Newtonsoft.Json.Linq.JObject obj = list[i];
                string userId = getJsonValue(obj, "userId");
                string score_3 = getJsonValue(obj, "score_3");
                string score_5 = getJsonValue(obj, "score_5");
                string certificate_no = getJsonValue(obj, "certificate_no");
                string stat_0 = getJsonValue(obj, "stat_0");
                string note = getJsonValue(obj, "note");
                if (score_3 == "") score_3 = "NULL";
                if (score_5 == "") score_5 = "NULL";
                if (certificate_no == "") certificate_no = "";
                if (stat_0 == "") stat_0 = "X";
                if (note == "") note = "";

                string sql = @"UPDATE IN_CLA_MEETING_RESUME SET in_score_o_3 = {#score_3}, in_score_o_5 = {#score_5}, in_certificate_no = '{#certificate_no}', in_stat_0 = '{#stat_0}', in_note = '{#note}'
                       WHERE id ='{#userId}' AND source_id = '{#meeting_id}' ";
                sql = sql.Replace("{#userId}", userId)
                        .Replace("{#meeting_id}", cfg.meeting_id)
                        .Replace("{#score_3}", score_3)
                        .Replace("{#score_5}", score_5)
                        .Replace("{#certificate_no}", certificate_no)
                        .Replace("{#stat_0}", stat_0)
                        .Replace("{#note}", note);
                cfg.inn.applySQL(sql);
            }

        }

        private string getJsonValue(Newtonsoft.Json.Linq.JObject obj, string key)
        {
            Newtonsoft.Json.Linq.JToken value;
            if (obj.TryGetValue(key, out value))
            {
                return value.ToString();
            }
            return "";

        }

        private string CDayFunc(string CDay, int hours, string format)
        {
            switch (format)
            {
                case "CY":
                    CDay = (Convert.ToDateTime(CDay).AddHours(hours).Year - 1911).ToString();
                    break;
                case "CM":
                    CDay = Convert.ToDateTime(CDay).AddHours(hours).Month.ToString("00");
                    break;
                case "CD":
                    CDay = Convert.ToDateTime(CDay).AddHours(hours).Day.ToString("00");
                    break;
                case "EDay":
                    CDay = Convert.ToDateTime(CDay).AddHours(hours).ToString("dd.MMM.yyyy", new System.Globalization.CultureInfo("EN-US"));
                    break;
                case "CDay":
                    CDay = CDayFunc(CDay, 8, "CY") + "年" + CDayFunc(CDay, 8, "CM") + "月" + CDayFunc(CDay, 8, "CD") + "日";
                    break;
                case "CountryDay":
                    CDay = "中華民國" + CDayFunc(CDay, 8, "CDay");
                    break;
            }
            return CDay;
        }

        private string CDegreeFunc(string in_l1, string format)
        {
            string cStr = "";
            string eStr = "";
            switch (in_l1)
            {
                case "壹段":
                    cStr = "1";
                    eStr = "1st";
                    break;
                case "貳段":
                    cStr = "2";
                    eStr = "2nd";
                    break;
                case "參段":
                    cStr = "3";
                    eStr = "3rd";
                    break;
                case "肆段":
                    cStr = "4";
                    eStr = "4th";
                    break;
                case "伍段":
                    cStr = "5";
                    eStr = "5th";
                    break;
                case "陸段":
                    cStr = "6";
                    eStr = "6th";
                    break;
                case "柒段":
                    cStr = "7";
                    eStr = "7th";
                    break;
                case "捌段":
                    cStr = "8";
                    eStr = "8th";
                    break;
                case "玖段":
                    cStr = "9";
                    eStr = "9th";
                    break;
            }

            if (format == "C")
            {
                return cStr;
            }
            else
            {
                return eStr;
            }
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public string strMethodName { get; set; }
            public Innovator inn { get; set; }

            //外部輸入區

            /// <summary>
            /// 活動 id
            /// </summary>
            public string meeting_id { get; set; }

            // Meeting 區

            /// <summary>
            /// Meeting 資料
            /// </summary>
            public Item itmMeeting { get; set; }

            /// <summary>
            /// 活動名稱
            /// </summary>
            public string in_title { get; set; }

            /// <summary>
            /// 專屬展示網址
            /// </summary>
            public string in_url { get; set; }

            /// <summary>
            /// 專屬報名網址
            /// </summary>
            public string in_register_url { get; set; }

            /// <summary>
            /// 活動類型
            /// </summary>
            public string in_meeting_type { get; set; }

            /// <summary>
            /// 梯次
            /// </summary>
            public string in_echelon { get; set; }

            /// <summary>
            /// 年度
            /// </summary>
            public string in_annual { get; set; }

            /// <summary>
            /// 文號
            /// </summary>
            public string in_decree { get; set; }

            /// <summary>
            /// 證書號碼
            /// </summary>
            public string certificate_no { get; set; }

            /// <summary>
            /// 課程起
            /// </summary>
            public string in_date_s { get; set; }

            /// <summary>
            /// 課程迄
            /// </summary>
            public string in_date_e { get; set; }

            /// <summary>
            /// 修課時數
            /// </summary>
            public string in_coursh_hours { get; set; }

            /// <summary>
            /// 地址
            /// </summary>
            public string in_address { get; set; }
            public string type { get; set; }
            public string id { get; set; }

            public int range { get; set; }
            public string seminar_type { get; set; }
        }

        private class TExport
        {
            public string templatePath { get; set; }
            public string exportPath { get; set; }
        }

        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            TExport result = new TExport { templatePath = "", exportPath = "" };

            //Word Template 所在路徑與匯出路徑 (from 原創.設定參數)
            string amlVariable = "<AML>"
                 + "<Item type='In_Variable' action='get'>"
                 + "  <in_name>meeting_excel</in_name>"
                 + "  <Relationships>"
                 + "    <Item type='In_Variable_Detail' action='get'/>"
                 + "  </Relationships>"
                 + "</Item></AML>";

            Item itmVairable = cfg.inn.applyAML(amlVariable);
            Item itmVairableDetail = itmVairable.getRelationships("In_Variable_Detail");
            int count = itmVairableDetail.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmDetail = itmVairableDetail.getItemByIndex(i);
                string detail_name = itmDetail.getProperty("in_name", "");

                if (detail_name == in_name)
                {
                    result.templatePath = itmDetail.getProperty("in_value", "");
                }
                if (detail_name == "export_path")
                {
                    result.exportPath = itmDetail.getProperty("in_value", "");
                }
                if (!result.exportPath.EndsWith(@"\"))
                {
                    result.exportPath = result.exportPath + @"\";
                }
            }

            return result;
        }

        private string checkEDate(DateTime sDate, DateTime eDate)
        {
            return (sDate > eDate)
                ? ""
                : sDate.ToString("MM月dd日");
        }

        private int GetInt(string value, int defV = 0)
        {
            int result = defV;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return defV;
        }
    }
}