﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_Cla_Meeting_NameNum : Item
    {
        public In_Cla_Meeting_NameNum(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 學員編號 Page
                日期: 2021-10-04 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Cla_Meeting_NameNum";

            Item itmR = inn.newItem("Inn_Result");
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = this.getProperty("meeting_id", ""),
                scene = this.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "edit":
                    UpdNameNum(cfg, this);
                    break;

                default:
                    Item itmMeeting = cfg.inn.getItemById("In_Cla_Meeting", cfg.meeting_id);
                    itmR = itmMeeting;
                    itmR.setType("Inn_Result");
                    itmR.setProperty("inn_item_number", itmMeeting.getProperty("item_number", ""));
                    itmR.setProperty("inn_title", itmMeeting.getProperty("in_title", ""));
                    Page(cfg, itmR);
                    break;
            }

            return itmR;
        }

        /// <summary>
        /// 更新學員編號
        /// </summary>
        private void UpdNameNum(TConfig cfg, Item itmReturn)
        {
            string muid = itmReturn.getProperty("muid", "");
            string in_name_num = itmReturn.getProperty("in_name_num", "");

            string sql_upd1 = "UPDATE IN_CLA_MEETING_USER SET in_name_num = '" + in_name_num + "' WHERE id = '" + muid + "'";
            cfg.inn.applySQL(sql_upd1);
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT 
	                t1.in_verify_mode
	                , t2.id AS 'muid'
	                , t2.in_name
	                , t2.in_current_org
	                , t2.in_sno_display
	                , t2.in_birth
	                , t2.in_degree
	                , t2.in_degree_id
	                , t2.in_name_num
	                , t2.in_note_state
	                , t2.in_creator
	                , t2.in_creator_sno
	                , t2.in_regdate
	                , t2.in_verify_result
	                , t2.in_verify_memo
	                , t2.in_ass_ver_result
	                , t2.in_ass_ver_memo
	                , t2.in_retraining
                FROM 
	                IN_CLA_MEETING t1 WITH(NOLOCK)
	            INNER JOIN
	                IN_CLA_MEETING_USER t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                LEFT OUTER JOIN 
	                IN_CLA_MEETING_RESUME t3 WITH(NOLOCK)
	                ON t3.in_user = t2.id
                WHERE
	                t1.id = '{#meeting_id}'
                ORDER BY
                    t2.in_name_num
	                , t2.in_degree DESC
	                , t2.in_birth
	                , t2.in_retraining
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmMUsers = cfg.inn.applySQL(sql);

            int count = itmMUsers.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmMUser = itmMUsers.getItemByIndex(i);
                string in_birth = itmMUser.getProperty("in_birth", "");
                string in_degree = itmMUser.getProperty("in_degree", "");
                string in_note_state = itmMUser.getProperty("in_note_state", "");
                string in_regdate = itmMUser.getProperty("in_regdate", "");
                string in_ass_ver_result = itmMUser.getProperty("in_regdate", ""); 

                int degree_code = GetInt(in_degree);

                string typeName = in_ass_ver_result == "1"
                    ? "inn_muser"
                    : "inn_muser_reject";

                itmMUser.setType(typeName);
                itmMUser.setProperty("inn_birth", GetBirth(in_birth));
                itmMUser.setProperty("inn_degree", GetDegreeLabel(degree_code));
                itmMUser.setProperty("inn_note", GetNoteLabel(in_note_state));
                itmMUser.setProperty("inn_regdate", GetDateTimeValue(in_regdate, "yyyy/MM/dd HH:mm", 8));
                itmMUser.setProperty("inn_verify", GetVerifyDisplay(cfg, itmMUser));
                itmReturn.addRelationship(itmMUser);
            }

            //學員人數
            itmReturn.setProperty("inn_muser_count", count >= 0 ? count.ToString() : "0");

        }

        private string GetVerifyDisplay(TConfig cfg, Item item)
        {
            StringBuilder builder = new StringBuilder();

            string[] properties = new string[]
            {
                "in_verify_mode", //活動審核模式
                "in_verify_result", //委員會審核結果
                "in_verify_memo", //委員會審核說明
                "in_ass_ver_result", //協會審核結果
                "in_ass_ver_memo", //協會審核說明
            };

            foreach (string property in properties)
            {
                builder.Append("<" + property + ">" + item.getProperty(property, "") + "</" + property + ">");
            }

            Item itmResult = cfg.inn.applyMethod("In_Verify_Message", builder.ToString());

            return itmResult.getProperty("in_verify_display", "");
        }

        //生日(外顯)
        private string GetBirth(string value)
        {
            if (value.Contains("1900") || value.Contains("1899"))
            {
                return "";
            }

            return GetDateTimeValue(value, "yyyy年MM月dd日", 8);
        }

        private string GetDegreeLabel(int val)
        {
            switch (val)
            {
                case 1000: return "壹段";
                case 2000: return "貳段";
                case 3000: return "參段";
                case 4000: return "肆段";
                case 5000: return "伍段";
                case 6000: return "陸段";
                case 7000: return "柒段";
                case 8000: return "捌段";
                case 9000: return "玖段";
                default: return "";
            }
        }

        private string GetNoteLabel(string val)
        {
            switch (val)
            {
                case "official": return "正取";
                case "waiting": return "備取";
                default: return val;
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }
        }

        private string GetDateTimeValue(string value, string format, int hours = 0)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }

        private int GetInt(string value, int defV = 0)
        {
            int result = defV;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return defV;
        }
    }
}