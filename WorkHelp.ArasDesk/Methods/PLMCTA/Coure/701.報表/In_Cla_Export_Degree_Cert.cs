﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using System.Drawing;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_Cla_Export_Degree_Cert : Item
    {
        public In_Cla_Export_Degree_Cert(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 大段證、小段證
                人員: lina
                版本:
                    - 2022-08-31: 大段證改合併列印 (lina)
                    - 2022-03-14: 小段證(製卡)、會員證 (lina)
                    - 2022-02-18: 大小段證改寫法(數量過多) (lina)
                    - 2021-10-05: 修改 by lina
                    - 2021-09-23: 修改 by David
                    - 2020-03-26: 創建 by Allen
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Cla_Export_Degree_Cert";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                aras_vault_url = @"C:\Aras\Vault\" + strDatabaseName,

                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
                type = itmR.getProperty("type", ""),
                extend = itmR.getProperty("extend", ""),
                folder = itmR.getProperty("folder", ""),
                step = itmR.getProperty("step", ""),
                cmt_sno = itmR.getProperty("cmt_sno", ""),
                rsm_sno = itmR.getProperty("rsm_sno", ""),
                is_part = itmR.getProperty("is_part", ""),
                need_headshot = itmR.getProperty("need_headshot", ""),
                url_mode = itmR.getProperty("url_mode", ""),
            };

            //設定活動資訊
            SetMeetingInfo(cfg);

            if (cfg.extend == "")
            {
                cfg.extend = "pdf";
            }
            if (cfg.extend.ToLower() == "pdf")
            {
                cfg.is_pdf = true;
            }

            cfg.type = cfg.type.ToLower();
            switch (cfg.type)
            {
                case "degreeb_xls":
                    cfg.report_name = "大段證清單";
                    cfg.report_variable = "";
                    cfg.report_type = ReportEnum.big_degree;
                    break;

                case "degreeb_new":
                    cfg.report_name = "大段證";
                    cfg.report_variable = "big_degree";
                    cfg.report_type = ReportEnum.big_degree;
                    break;

                case "degreeb":
                    cfg.report_name = "大段證";
                    cfg.report_variable = "DegreeB_path";
                    cfg.report_type = ReportEnum.big_degree;
                    break;

                case "degrees":
                    cfg.report_name = "小段證";
                    cfg.report_variable = "DegreeS_path";
                    cfg.report_type = ReportEnum.small_degree;
                    break;

                case "degreesn":
                    cfg.report_name = "小段證";
                    cfg.report_variable = "";
                    cfg.report_type = ReportEnum.degree_card;
                    cfg.is_pdf = false;
                    cfg.extend = "xlsx";
                    break;

                case "membern":
                    cfg.report_name = "會員證";
                    cfg.report_variable = "";
                    cfg.report_type = ReportEnum.member_card;
                    cfg.is_pdf = false;
                    cfg.extend = "xlsx";
                    break;
            }

            switch (cfg.scene)
            {
                case "export_big_word":
                    ExportBigDegreeCertWord(cfg, itmR);
                    break;

                case "export_big_xls":
                    ExportBigDegreeCertXls(cfg, itmR);
                    break;

                case "export_committee":
                    Export(cfg, itmR);
                    break;

                case "export_city"://一單位
                    ExportCity(cfg, itmR);
                    break;

                case "export_resume"://一人
                    ExportResume(cfg, itmR);
                    break;

                case "page":
                    Page(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void ExportBigDegreeCertWord(TConfig cfg, Item itmReturn)
        {
            var exp = ExportInfo(cfg, cfg.report_variable);
            exp.exportPath = exp.exportPath.Replace("meeting_excel", "meeting_degree");

            var items = GetItmDegrees(cfg, cfg.cmt_sno, cfg.rsm_sno);
            if (items.isError() || items.getResult() == "")
            {
                throw new Exception("查無資料");
            }

            var cmts = MapCommittees(cfg, items);
            if (cmts.Count == 0)
            {
                return;
            }

            string xls_name = cfg.in_title
                + "_" + cfg.report_name
                + "_" + System.Guid.NewGuid().ToString().Substring(0, 6).ToUpper()
                + "." + "docx";

            string xls_path = exp.exportPath.Trim('\\') + "\\" + xls_name;


            //載入模板
            using (var doc = Xceed.Words.NET.DocX.Load(exp.templatePath))
            {
                var sign_file = @"C:\site\CTA_git\pages\Images\chairman_signature2.png";
                Xceed.Document.NET.Image sign_img = doc.AddImage(sign_file);//取得路徑圖片
                Xceed.Document.NET.Picture sign_pic = sign_img.CreatePicture(84, 457);

                foreach (var cmt in cmts)
                {
                    foreach (var degree in cmt.DegreeList)
                    {
                        foreach (var row in degree.Rows)
                        {
                            Item item = MapItem(cfg, cmt, degree, row);
                            MatchWordTable(cfg, doc, item, sign_pic);

                            doc.InsertParagraph();
                        }
                    }
                }

                doc.Tables[0].Remove();
                doc.RemoveParagraphAt(0);
                doc.SaveAs(xls_path);
            }

            //輸出
            itmReturn.setProperty("pdf_name", xls_name);
        }

        private void MatchWordTable(TConfig cfg, Xceed.Words.NET.DocX doc, Item item, Xceed.Document.NET.Picture sign_pic)
        {
            try
            {
                string head_key = "[HeadShot]";
                string signature_key = "[Signature]";

                var dic = new Dictionary<string, TPic>();
                dic.Add(head_key, new TPic { key = head_key, exists = false });
                dic.Add(signature_key, new TPic { key = signature_key, exists = false });

                //取得模板表格
                var template_table = doc.Tables[0];
                //複製樣板表格
                var docTable = doc.InsertTable(template_table);

                MatchWordCell(cfg, docTable, item, dic);

                //大頭照
                var head_shot = dic[head_key];
                if (head_shot.exists)
                {
                    AppendHeadShot(cfg, doc, docTable, item, 175, 131, head_shot.ridx, head_shot.cidx);
                }

                //理事長簽名
                var sign_cfg = dic[signature_key];
                if (sign_cfg.exists)
                {
                    docTable.Rows[sign_cfg.ridx].Cells[sign_cfg.cidx].Paragraphs.First().AppendPicture(sign_pic);
                    // AppendWordImage(cfg, doc, docTable, sign_file, 84, 457, sign_cfg.ridx, sign_cfg.cidx);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void MatchWordCell(TConfig cfg, Xceed.Document.NET.Table table, Item item, Dictionary<string, TPic> pics)
        {
            int row_count = table.Rows.Count;
            for (int i = 0; i < row_count; i++)
            {
                var wrow = table.Rows[i];
                int cell_count = wrow.Cells.Count;
                for (int j = 0; j < cell_count; j++)
                {
                    var cell = wrow.Cells[j];
                    string text = cell.Paragraphs.First().Text;
                    if (!text.Contains("[")) continue;

                    string property = GetCellProperty(text);
                    string key = "[" + property + "]";

                    if (pics.ContainsKey(key))
                    {
                        var pic = pics[key];
                        pic.ridx = i;
                        pic.cidx = j;
                        pic.exists = true;
                    }

                    if (property == "") continue;

                    string value = item.getProperty(property, "");
                    cell.ReplaceText(key, value);
                }
            }
        }

        private string GetCellProperty(string value)
        {
            int posS = value.IndexOf("[");
            int posE = value.IndexOf("]");

            if (posS == -1 || posE == -1)
            {
                return "";
            }
            else
            {
                return value.Substring(posS + 1, posE - posS - 1);
            }
        }

        private Item MapItem(TConfig cfg, TCommittee cmt, TDegree degree, TRow row)
        {
            var result = cfg.inn.newItem();
            result.setProperty("ec", cfg.in_echelon);//梯次
            result.setProperty("did", row.in_degree_id);//國內段證號
            result.setProperty("name", row.in_name);//姓名
            result.setProperty("by", row.birth.cy);//生日-年
            result.setProperty("bm", row.birth.cm);//生日-月
            result.setProperty("bd", row.birth.cd);//生日-日
            result.setProperty("area", row.in_area);//出生地
            result.setProperty("dc", degree.code);//段位

            result.setProperty("cy", cfg.EndDayInfo.cy);//發證日-年
            result.setProperty("cm", cfg.EndDayInfo.cm);//發證日-月
            result.setProperty("cd", cfg.EndDayInfo.cd);//發證日-日

            result.setProperty("enm", row.in_en_name);//英文姓名
            result.setProperty("enb", row.birth.en_day);//英文生日
            result.setProperty("end", degree.en_display);//英文段位
            result.setProperty("enc", cfg.EndDayInfo.en_day);//英文發證日

            result.setProperty("fi", row.headshot_fileid);
            result.setProperty("fn", row.headshot_filename);
            result.setProperty("fe", row.headshot_extname);

            return result;
        }

        private void AppendHeadShot(TConfig cfg, Xceed.Words.NET.DocX doc, Xceed.Document.NET.Table docTable, Item item, int h, int w, int l_x, int l_y)
        {
            try
            {
                string file_id = item.getProperty("fi", "");
                string file_name = item.getProperty("fn", "");
                string file_ext = item.getProperty("fe", "");

                if (file_id == "") return;

                string id_1 = "";
                string id_2 = "";
                string id_3 = "";
                string url = @"C:\Aras\Vault\" + cfg.strDatabaseName + @"\";//讀取大頭照的路徑

                //路徑切出來
                id_1 = file_id.Substring(0, 1);
                id_2 = file_id.Substring(1, 2);
                id_3 = file_id.Substring(3, 29);

                string photo = url + id_1 + @"\" + id_2 + @"\" + id_3 + @"\" + file_name;
                if (System.IO.File.Exists(photo))
                {
                    Xceed.Document.NET.Image img = doc.AddImage(photo);//取得路徑圖片
                    Xceed.Document.NET.Picture p = img.CreatePicture(h, w);

                    docTable.Rows[l_x].Cells[l_y].Paragraphs.First().AppendPicture(p);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private class TPic
        {
            public string key { get; set; }
            public bool exists { get; set; }
            public int ridx { get; set; }
            public int cidx { get; set; }
        }

        private void ExportBigDegreeCertXls(TConfig cfg, Item itmReturn)
        {
            var exp = ExportInfo(cfg, cfg.report_variable);
            exp.exportPath = exp.exportPath.Replace("meeting_excel", "meeting_degree");

            var items = GetItmDegrees(cfg, cfg.cmt_sno, "");
            if (items.isError() || items.getResult() == "")
            {
                throw new Exception("查無資料");
            }

            var cmts = MapCommittees(cfg, items);
            if (cmts.Count == 0)
            {
                return;
            }

            Spire.Xls.Workbook book = new Spire.Xls.Workbook();

            var is_single = cmts.Count == 1;
            foreach (var cmt in cmts)
            {
                //附加大段證 List Sheet
                AppendBigDegreeCertXls(cfg, book, cmt, is_single);
            }

            //移除空 sheet
            book.Worksheets[0].Remove();
            book.Worksheets[0].Remove();
            book.Worksheets[0].Remove();

            //適合頁面
            book.ConverterSetting.SheetFitToPage = true;

            // string xls_name = cfg.in_title
            //     + "_" + cfg.report_name
            //     + "_" + "ALL"
            //     + "." + "xlsx";

            string xls_name = "中華跆協_大段證_合併列印_資料來源.xlsx";
            string xls_path = exp.exportPath.Trim('\\') + "\\" + xls_name;

            book.SaveToFile(xls_path, Spire.Xls.ExcelVersion.Version2013);

            //輸出
            itmReturn.setProperty("pdf_name", xls_name);
        }

        private void AppendBigDegreeCertXls(TConfig cfg, Spire.Xls.Workbook book, TCommittee cmt, bool is_single)
        {
            //工作表
            var sheet = book.CreateEmptySheet();
            sheet.Name = is_single ? "print" : cmt.short_name;

            SetHeadCell(sheet, "A1", "梯次");
            SetHeadCell(sheet, "B1", "縣市");
            SetHeadCell(sheet, "C1", "姓名");
            SetHeadCell(sheet, "D1", "英文姓名");
            SetHeadCell(sheet, "E1", "生日年");
            SetHeadCell(sheet, "F1", "生日月");
            SetHeadCell(sheet, "G1", "生日日");
            SetHeadCell(sheet, "H1", "出生地");
            SetHeadCell(sheet, "I1", "段位");
            SetHeadCell(sheet, "J1", "國內段證號");
            SetHeadCell(sheet, "K1", "發證年");
            SetHeadCell(sheet, "L1", "發證月");
            SetHeadCell(sheet, "M1", "發證日");
            SetHeadCell(sheet, "N1", "英文生日");
            SetHeadCell(sheet, "O1", "英文段位");
            SetHeadCell(sheet, "P1", "英文發證日");
            SetHeadCell(sheet, "Q1", "照片路徑");

            var ridx = 2;

            Func<TConfig, TCommittee, TRow, string> getHeadShotUrl = GetHeadShotUrlFromUrl;
            if (cfg.url_mode == "dest") getHeadShotUrl = GetHeadShotUrlFromDestPath;

            for (int i = 0; i < cmt.DegreeList.Count; i++)
            {
                var degree = cmt.DegreeList[i];
                for (int j = 0; j < degree.Rows.Count; j++)
                {
                    var row = degree.Rows[j];
                    var photo_url = getHeadShotUrl(cfg, cmt, row);

                    SetBodyCell(sheet, "A" + ridx, cfg.in_echelon);
                    SetBodyCell(sheet, "B" + ridx, cmt.short_name);
                    SetBodyCell(sheet, "C" + ridx, row.in_name);
                    SetBodyCell(sheet, "D" + ridx, row.in_en_name);
                    SetBodyCell(sheet, "E" + ridx, row.birth.cy);
                    SetBodyCell(sheet, "F" + ridx, row.birth.cm);
                    SetBodyCell(sheet, "G" + ridx, row.birth.cd);
                    SetBodyCell(sheet, "H" + ridx, row.in_area);
                    SetBodyCell(sheet, "I" + ridx, degree.code);
                    SetBodyCell(sheet, "J" + ridx, row.in_degree_id);
                    SetBodyCell(sheet, "K" + ridx, cfg.EndDayInfo.cy);//課程結束_民國年
                    SetBodyCell(sheet, "L" + ridx, cfg.EndDayInfo.cm);
                    SetBodyCell(sheet, "M" + ridx, cfg.EndDayInfo.cd);
                    SetBodyCell(sheet, "N" + ridx, row.birth.en_day);
                    SetBodyCell(sheet, "O" + ridx, degree.en_display);
                    SetBodyCell(sheet, "P" + ridx, cfg.EndDayInfo.en_day);
                    SetBodyCell(sheet, "Q" + ridx, "\"" + photo_url + "\"");
                    ridx++;
                }
            }
        }

        private string GetHeadShotUrlFromDestPath(TConfig cfg, TCommittee cmt, TRow row)
        {
            var result = @"C:\CTA_PHOTO"
                + @"\" + cfg.in_title
                + @"\" + cmt.sno + cmt.short_name
                + @"\" + row.in_name + row.in_sno + "." + row.headshot_extname
                ;

            return result.Replace(@"\", @"\\");
        }

        private string GetHeadShotUrlFromUrl(TConfig cfg, TCommittee cmt, TRow row)
        {
            return "https://act.innosoft.com.tw/cta/DownloadMeetingFile.aspx?fileid=" + row.headshot_fileid;
        }

        //設定標題格
        private void SetHeadCell(Spire.Xls.Worksheet sheet, string cr, string value)
        {
            var range = sheet.Range[cr];
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Style.Font.FontName = "微軟正黑體";
            range.Style.Font.Size = 14;
            range.Style.Font.IsBold = true;
            range.Text = value;
        }

        //設定資料格
        private void SetBodyCell(Spire.Xls.Worksheet sheet, string cr, string value)
        {
            var range = sheet.Range[cr];
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Style.Font.FontName = "微軟正黑體";
            range.Style.Font.Size = 14;
            range.Style.Font.IsBold = false;
            range.Text = value;
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            Item items = GetItmDegrees(cfg, "", "");

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setType("In_Cla_Meeting_Promotion");
                item.setProperty("no", (i + 1).ToString());
                itmReturn.addRelationship(item);
            }
            itmReturn.setProperty("in_title", cfg.in_title);
            itmReturn.setProperty("inn_count", count.ToString());
        }

        //設定活動資訊
        private void SetMeetingInfo(TConfig cfg)
        {
            List<string> cols = new List<string>
            {
                "in_title",
                "in_url",
                "in_register_url",
                "in_need_receipt",
                "in_meeting_type",
                "in_echelon",
                "in_annual",
                "in_certificate_no",
                "in_date_s",
                "in_date_e",
                "in_coursh_hours",
                "in_address",
                "in_seminar_type",
            };

            string aml = "<AML>"
                + "<Item type='in_cla_meeting' action='get' id='" + cfg.meeting_id + "'"
                + " select='" + string.Join(",", cols) + "'"
                + "></Item></AML>";

            cfg.itmMeeting = cfg.inn.applyAML(aml);

            cfg.in_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.in_url = cfg.itmMeeting.getProperty("in_url", "");
            cfg.in_register_url = cfg.itmMeeting.getProperty("in_register_url", "");
            cfg.in_meeting_type = cfg.itmMeeting.getProperty("in_meeting_type", "");
            cfg.in_echelon = cfg.itmMeeting.getProperty("in_echelon", "");
            cfg.in_annual = cfg.itmMeeting.getProperty("in_annual", "");
            cfg.certificate_no = cfg.itmMeeting.getProperty("in_certificate_no", "");
            cfg.in_date_s = cfg.itmMeeting.getProperty("in_date_s", "");
            cfg.in_date_e = cfg.itmMeeting.getProperty("in_date_e", "");
            cfg.in_coursh_hours = cfg.itmMeeting.getProperty("in_coursh_hours", "");
            cfg.in_address = cfg.itmMeeting.getProperty("in_address", "");
            cfg.seminar_type = cfg.itmMeeting.getProperty("in_seminar_type", "");

            cfg.EndDayInfo = MapDay(cfg.in_date_e, 0);
        }
        #region 製卡

        #endregion 製卡
        //匯出
        private void ExportCity(TConfig cfg, Item itmReturn)
        {
            var exp = ExportInfo(cfg, cfg.report_variable);
            exp.exportPath = exp.exportPath.Replace("meeting_excel", "meeting_degree");

            var items = GetItmDegrees(cfg, cfg.cmt_sno, "");
            if (items.isError() || items.getResult() == "")
            {
                throw new Exception("查無資料");
            }

            var cmts = MapCommittees(cfg, items);
            var list = new List<TRow>();
            foreach (var cmt in cmts)
            {
                foreach (var degree in cmt.DegreeList)
                {
                    list.AddRange(degree.Rows);
                }
            }

            string xls_name = cfg.in_title
                + "_" + cfg.report_name
                + "_" + cfg.cmt_sno
                + "." + cfg.extend;

            string xls_path = exp.exportPath.Trim('\\') + "\\" + xls_name;

            switch (cfg.report_type)
            {
                case ReportEnum.big_degree://大段證
                    CreateBigCertFile(cfg, exp, list, xls_path);
                    break;

                case ReportEnum.small_degree://小段證(old)
                    CreateSmallCertFile(cfg, exp, list, xls_path);
                    break;

                case ReportEnum.degree_card://製卡-段證
                    CreateDegreeCardFile(cfg, exp, list, xls_path);
                    break;

                case ReportEnum.member_card://製卡-會員證
                    CreateMemberCardFile(cfg, exp, list, xls_path);
                    break;
            }

            //輸出
            itmReturn.setProperty("pdf_name", xls_name);
        }

        //匯出
        private void ExportResume(TConfig cfg, Item itmReturn)
        {
            var exp = ExportInfo(cfg, cfg.report_variable);
            exp.exportPath = exp.exportPath.Replace("meeting_excel", "meeting_degree");

            var item = GetItmDegrees(cfg, "", cfg.rsm_sno);
            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("查無資料");
            }

            var muser = MapRow(cfg, item);
            muser.cmt = NewCommittee(cfg, muser);
            muser.degree = NewDegree(cfg, muser);

            var list = new List<TRow> { muser };

            string xls_name = cfg.in_title
                + "_" + cfg.report_name
                + "_" + muser.promotion_id.Substring(0, 6).ToUpper()
                + "." + cfg.extend;

            string xls_path = exp.exportPath.Trim('\\') + "\\" + xls_name;

            switch (cfg.report_type)
            {
                case ReportEnum.big_degree://大段證
                    CreateBigCertFile(cfg, exp, list, xls_path);
                    break;
                case ReportEnum.small_degree://小段證
                    CreateSmallCertFile(cfg, exp, list, xls_path);
                    break;
                case ReportEnum.degree_card://製卡-晉段卡
                    CreateDegreeCardFile(cfg, exp, list, xls_path);
                    break;
                case ReportEnum.member_card://製卡-會員卡
                    CreateMemberCardFile(cfg, exp, list, xls_path);
                    break;
            }

            //輸出
            itmReturn.setProperty("pdf_name", xls_name);
        }

        //匯出
        private void Export(TConfig cfg, Item itmReturn)
        {
            var exp = ExportInfo(cfg, cfg.report_variable);
            exp.exportPath = exp.exportPath.Replace("meeting_excel", "meeting_degree");

            var znfo = ZipInfo(cfg, exp);

            if (cfg.step == "all" || cfg.is_part == "1")
            {
                ExportOneStep(cfg, exp, znfo, itmReturn);
            }
            else
            {
                ExportStepByStep(cfg, exp, znfo, itmReturn);
            }
        }

        //匯出晉段大小段證(步驟不拆解)
        private void ExportOneStep(TConfig cfg, TExport exp, TZip znfo, Item itmReturn)
        {
            //清除檔案
            FixDirAndClearFile(znfo.zip_fldr);

            //匯出
            RunExport(cfg, exp, znfo, cfg.cmt_sno);

            //打包與輸出
            ThinkZip(cfg, exp, znfo, itmReturn);
        }

        //匯出晉段大小段證(步驟拆解)
        private void ExportStepByStep(TConfig cfg, TExport exp, TZip znfo, Item itmReturn)
        {
            if (cfg.step == "start")
            {
                //清除檔案
                FixDirAndClearFile(znfo.zip_fldr);
            }

            if (cfg.step == "sno")
            {
                //匯出
                RunExport(cfg, exp, znfo, cfg.cmt_sno);
            }

            if (cfg.step == "end")
            {
                //打包與輸出
                ThinkZip(cfg, exp, znfo, itmReturn);
            }
        }

        private void ThinkZip(TConfig cfg, TExport exp, TZip znfo, Item itmReturn)
        {
            var dir = new System.IO.DirectoryInfo(znfo.zip_fldr);
            var files = dir.GetFiles();
            var subs = dir.GetDirectories();

            if (files.Length == 0 && subs.Length == 0)
            {
                throw new Exception("無檔案匯出");
            }
            else if (files.Length == 1)
            {
                string fpath = znfo.zip_fldr + "/" + files[0].Name;
                string spath = fpath.Replace(exp.exportPath.Trim('\\'), "").Trim('\\');
                itmReturn.setProperty("pdf_name", spath);
            }
            else
            {
                //打包
                Zip(znfo.zip_file, znfo.zip_fldr);

                //輸出
                itmReturn.setProperty("pdf_name", znfo.zip_name);
            }
        }

        private void RunExport(TConfig cfg, TExport exp, TZip znfo, string cmt_sno)
        {
            //取得資料
            Item items = GetItmDegrees(cfg, cmt_sno, "");
            var cmts = MapCommittees(cfg, items);
            if (cmts.Count == 0)
            {
                return;
            }

            if (cfg.report_type == ReportEnum.big_degree)
            {
                if (cfg.folder == "跆委會")
                {
                    BigDegreeXlsxByCommittee(cfg, exp, znfo.zip_fldr, cmts);
                }
                else if (cfg.folder == "段位")
                {
                    BigDegreeXlsxByDegree(cfg, exp, znfo.zip_fldr, cmts);
                }
                else if (cfg.folder == "全部")
                {
                    BigDegreeXlsxByMeeting(cfg, exp, znfo.zip_fldr, cmts);
                }
                else if (cfg.is_part == "1")
                {
                    BigDegreeXlsxByMeeting(cfg, exp, znfo.zip_fldr, cmts, range_value: cfg.folder);
                }
            }
            else if (cfg.report_type == ReportEnum.small_degree)
            {
                if (cfg.folder == "跆委會")
                {
                    SmallDegreeXlsxByCommittee(cfg, exp, znfo.zip_fldr, cmts);
                }
                else if (cfg.folder == "段位")
                {
                    SmallDegreeXlsxByDegree(cfg, exp, znfo.zip_fldr, cmts);
                }
                else if (cfg.folder == "全部")
                {
                    SmallDegreeXlsxByMeeting(cfg, exp, znfo.zip_fldr, cmts);
                }
                else if (cfg.is_part == "1")
                {
                    //小段證不用切割
                    SmallDegreeXlsxByMeeting(cfg, exp, znfo.zip_fldr, cmts);
                }
            }
        }

        #region 小段證

        //一段一XLSX
        private void SmallDegreeXlsxByDegree(TConfig cfg, TExport exp, string zip_fldr, List<TCommittee> cmts)
        {
            foreach (var cmt in cmts)
            {
                string cmt_fldr = zip_fldr
                    + "\\" + cmt.display;

                if (!System.IO.Directory.Exists(cmt_fldr))
                {
                    System.IO.Directory.CreateDirectory(cmt_fldr);
                }

                foreach (var degree in cmt.DegreeList)
                {
                    string xls_path = zip_fldr
                        + "\\" + cmt.display
                        + "\\" + degree.display
                        + "." + cfg.extend;

                    //建立檔案
                    CreateSmallCertFile(cfg, exp, degree.Rows, xls_path);
                }
            }
        }

        //一縣市一XLSX
        private void SmallDegreeXlsxByCommittee(TConfig cfg, TExport exp, string zip_fldr, List<TCommittee> cmts)
        {
            foreach (var cmt in cmts)
            {
                var list = new List<TRow>();

                foreach (var degree in cmt.DegreeList)
                {
                    list.AddRange(degree.Rows);
                }

                string xls_path = zip_fldr
                    + "\\" + cmt.display
                    + "." + cfg.extend;

                //建立檔案
                CreateSmallCertFile(cfg, exp, list, xls_path);
            }
        }

        //ONE XLSX
        private void SmallDegreeXlsxByMeeting(TConfig cfg
            , TExport exp
            , string zip_fldr
            , List<TCommittee> cmts)
        {
            var list = new List<TRow>();

            foreach (var cmt in cmts)
            {
                foreach (var degree in cmt.DegreeList)
                {
                    list.AddRange(degree.Rows);
                }
            }


            var xls_path = zip_fldr + "\\小段證(全部)"
                + "." + cfg.extend;

            //建立檔案
            CreateSmallCertFile(cfg, exp, list, xls_path);
        }

        //建立檔案
        private void CreateSmallCertFile(TConfig cfg, TExport exp, List<TRow> list, string xls_path)
        {
            Spire.Xls.Workbook book = new Spire.Xls.Workbook();
            book.LoadFromFile(exp.templatePath);

            //取得樣板
            Spire.Xls.Worksheet sheetTemplate = book.Worksheets[0];

            //附加小段證 Sheet
            AddSmallCertSheet(cfg, book, sheetTemplate, list);

            //移除樣板
            sheetTemplate.Remove();

            //適合頁面
            book.ConverterSetting.SheetFitToPage = true;

            //建立檔案
            if (cfg.is_pdf)
            {
                book.SaveToFile(xls_path, Spire.Xls.FileFormat.PDF);
            }
            else
            {
                book.SaveToFile(xls_path, Spire.Xls.ExcelVersion.Version2013);
            }
        }

        private void AddSmallCertSheet(TConfig cfg, Spire.Xls.Workbook book, Spire.Xls.Worksheet sheetTemplate, List<TRow> list)
        {
            string[] col = { "P", "Q", "R", "S", "T", "U", "V", "W" };

            Spire.Xls.Worksheet sheet = book.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = "page1";
            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 1;
            sheet.PageSetup.TopMargin = 0;
            sheet.PageSetup.LeftMargin = 0;
            sheet.PageSetup.RightMargin = 0;
            sheet.PageSetup.BottomMargin = 0;
            //適合頁面
            book.ConverterSetting.SheetFitToPage = true;

            for (int i = 0; i < list.Count; i++)
            {
                var row = list[i];

                int j = i % 8;

                if (j == 0 && i > 0)
                {
                    sheet = book.CreateEmptySheet();
                    sheet.CopyFrom(sheetTemplate);
                    sheet.Name = row.in_sno;
                    sheet.PageSetup.FitToPagesWide = 1;
                    sheet.PageSetup.FitToPagesTall = 1;
                    sheet.PageSetup.TopMargin = 0;
                    sheet.PageSetup.LeftMargin = 0;
                    sheet.PageSetup.RightMargin = 0;
                    sheet.PageSetup.BottomMargin = 0;
                    //適合頁面
                    book.ConverterSetting.SheetFitToPage = true;
                }

                var cmt = row.cmt;
                var degree = row.degree;

                //編號
                sheet.Range[col[j] + "1"].Text = "編號： " + row.in_degree_id;
                //段位(數字)
                sheet.Range[col[j] + "2"].Text = "段位： " + degree.code + " 段";

                sheet.Range[col[j] + "3"].Text = "姓名：";
                //姓名
                sheet.Range[col[j] + "4"].Text = row.in_name;

                sheet.Range[col[j] + "5"].Text = "出生日期：";
                //生日年月日
                sheet.Range[col[j] + "6"].Text = " " + row.birth.short_tw_day;

                sheet.Range[col[j] + "7"].Text = "上列會員資格經本會於";
                //中華民國年月日
                sheet.Range[col[j] + "8"].Text = cfg.EndDayInfo.long_tw_day;
                sheet.Range[col[j] + "9"].Text = "審查合格，特此證明。";
            }
        }
        #endregion 小段證

        #region 大段證
        //一人一XLSX
        private void BigDegreeXlsxByMUser(TConfig cfg, TExport exp, string zip_fldr, List<TCommittee> cmts)
        {
            //c:\site\cta_git\tempvault\meeting_degree\中華民國跆拳道協會第202梯次晉段申請\A018花蓮縣跆委會\1段\姓名身分證號.pdf

            foreach (var cmt in cmts)
            {
                foreach (var degree in cmt.DegreeList)
                {
                    string degree_fldr = zip_fldr
                        + "\\" + cmt.display
                        + "\\" + degree.display;

                    if (!System.IO.Directory.Exists(degree_fldr))
                    {
                        System.IO.Directory.CreateDirectory(degree_fldr);
                    }

                    foreach (var row in degree.Rows)
                    {
                        var xls_path = degree_fldr
                            + "\\" + row.in_name + row.in_sno
                            + "." + cfg.extend;

                        //建立檔案
                        CreateBigCertFile(cfg, exp, new List<TRow> { row }, xls_path);
                    }
                }
            }
        }

        //一段一XLSX
        private void BigDegreeXlsxByDegree(TConfig cfg, TExport exp, string zip_fldr, List<TCommittee> cmts)
        {
            foreach (var cmt in cmts)
            {
                string cmt_fldr = zip_fldr
                    + "\\" + cmt.display;

                if (!System.IO.Directory.Exists(cmt_fldr))
                {
                    System.IO.Directory.CreateDirectory(cmt_fldr);
                }

                foreach (var degree in cmt.DegreeList)
                {
                    string xls_path = zip_fldr
                        + "\\" + cmt.display
                        + "\\" + degree.display
                        + "." + cfg.extend;

                    //建立檔案
                    CreateBigCertFile(cfg, exp, degree.Rows, xls_path);
                }
            }
        }

        //一縣市一XLSX
        private void BigDegreeXlsxByCommittee(TConfig cfg, TExport exp, string zip_fldr, List<TCommittee> cmts)
        {
            foreach (var cmt in cmts)
            {
                List<TRow> list = new List<TRow>();

                foreach (var degree in cmt.DegreeList)
                {
                    list.AddRange(degree.Rows);
                }

                string xls_path = zip_fldr
                    + "\\" + cmt.display
                    + "." + cfg.extend;

                //建立檔案
                CreateBigCertFile(cfg, exp, list, xls_path);
            }
        }

        //ONE XLSX
        private void BigDegreeXlsxByMeeting(TConfig cfg
            , TExport exp
            , string zip_fldr
            , List<TCommittee> cmts
            , string range_value = "")
        {
            var list = new List<TRow>();

            foreach (var cmt in cmts)
            {
                foreach (var degree in cmt.DegreeList)
                {
                    list.AddRange(degree.Rows);
                }
            }

            var range = NewRange(range_value, list.Count);

            var xls_path = zip_fldr + "\\大段證(全部)"
                + "." + cfg.extend;

            //建立檔案
            CreateBigCertFile(cfg, exp, list, range, xls_path);
        }

        private void CreateBigCertFile(TConfig cfg, TExport exp, List<TRow> list, string xls_path)
        {
            var range = new TRange();
            range.idxs = 0;
            range.idxe = list.Count - 1;
            CreateBigCertFile(cfg, exp, list, range, xls_path);
        }

        //建立檔案
        private void CreateBigCertFile(TConfig cfg, TExport exp, List<TRow> list, TRange range, string xls_path)
        {
            Spire.Xls.Workbook book = new Spire.Xls.Workbook();
            book.LoadFromFile(exp.templatePath);

            //取得樣板
            Spire.Xls.Worksheet sheetTemplate = book.Worksheets[0];

            for (int i = range.idxs; i <= range.idxe; i++)
            {
                var row = list[i];
                //附加大段證 Sheet
                AddBigCertSheet(cfg, book, sheetTemplate, row);
            }

            //移除樣板
            sheetTemplate.Remove();

            ////適合頁面
            //book.ConverterSetting.SheetFitToPage = true;

            //建立檔案
            if (cfg.is_pdf)
            {
                book.SaveToFile(xls_path, Spire.Xls.FileFormat.PDF);
            }
            else
            {
                book.SaveToFile(xls_path, Spire.Xls.ExcelVersion.Version2013);
            }
        }

        private void HeadRange(Spire.Xls.Worksheet sheet, string pos, int col, string text)
        {
            var rg = sheet[pos];
            rg.Text = text;
            rg.Style.Font.FontName = "標楷體";
            rg.Style.Font.Size = 12;
            rg.Style.Font.IsBold = true;
            rg.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
        }

        private void SetRange(Spire.Xls.Worksheet sheet, string pos, string text)
        {
            var rg = sheet[pos];
            rg.Text = text;
            rg.Style.Font.FontName = "標楷體";
            rg.Style.Font.Size = 12;
            rg.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
        }

        private void SetNumber(Spire.Xls.Worksheet sheet, string pos, int text)
        {
            var rg = sheet[pos];
            rg.NumberValue = text;
            rg.Style.Font.FontName = "標楷體";
            rg.Style.Font.Size = 12;
            rg.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
        }

        private void CreateMemberCardFile(TConfig cfg, TExport exp, List<TRow> list, string xls_path)
        {
            Spire.Xls.Workbook book = new Spire.Xls.Workbook();

            Spire.Xls.Worksheet sheet = book.CreateEmptySheet();
            sheet.Name = "清單";

            int wsRow = 1;
            HeadRange(sheet, "A" + wsRow, 1, "會員編號");
            HeadRange(sheet, "B" + wsRow, 2, "會員姓名");
            HeadRange(sheet, "C" + wsRow, 3, "出生年月日");
            HeadRange(sheet, "D" + wsRow, 4, "發證日期");
            HeadRange(sheet, "E" + wsRow, 5, "QR");
            wsRow++;

            for (int i = 0; i < list.Count; i++)
            {
                var row = list[i];
                SetRange(sheet, "A" + wsRow, row.in_card_member_id);
                SetRange(sheet, "B" + wsRow, "'" + row.in_name);
                SetRange(sheet, "C" + wsRow, "'" + row.birth.short_entw_day);
                SetRange(sheet, "D" + wsRow, row.promotion.long_tw_day);
                SetRange(sheet, "E" + wsRow, row.in_sno);
                wsRow++;
            }

            sheet.Columns[0].ColumnWidth = 15;
            sheet.Columns[1].ColumnWidth = 15;
            sheet.Columns[2].ColumnWidth = 20;
            sheet.Columns[3].ColumnWidth = 24;
            sheet.Columns[4].ColumnWidth = 15;

            book.Worksheets[0].Remove();
            book.Worksheets[0].Remove();
            book.Worksheets[0].Remove();

            //適合頁面
            book.ConverterSetting.SheetFitToPage = true;

            //建立檔案
            if (cfg.is_pdf)
            {
                book.SaveToFile(xls_path, Spire.Xls.FileFormat.PDF);
            }
            else
            {
                book.SaveToFile(xls_path, Spire.Xls.ExcelVersion.Version2013);
            }
        }

        private void CreateDegreeCardFile(TConfig cfg, TExport exp, List<TRow> list, string xls_path)
        {
            Spire.Xls.Workbook book = new Spire.Xls.Workbook();

            Spire.Xls.Worksheet sheet = book.CreateEmptySheet();
            sheet.Name = "清單";

            int wsRow = 1;
            HeadRange(sheet, "A" + wsRow, 1, "會員編號");
            HeadRange(sheet, "B" + wsRow, 2, "會員姓名");
            HeadRange(sheet, "C" + wsRow, 3, "出生年月日");
            HeadRange(sheet, "D" + wsRow, 4, "發證日期");
            HeadRange(sheet, "E" + wsRow, 5, "QR");
            HeadRange(sheet, "F" + wsRow, 5, "段位");
            wsRow++;

            for (int i = 0; i < list.Count; i++)
            {
                var row = list[i];
                SetRange(sheet, "A" + wsRow, row.in_card_member_id);
                SetRange(sheet, "B" + wsRow, "'" + row.in_name);
                SetRange(sheet, "C" + wsRow, "'" + row.birth.short_entw_day);
                SetRange(sheet, "D" + wsRow, row.promotion.long_tw_day);
                SetRange(sheet, "E" + wsRow, row.in_sno);
                SetNumber(sheet, "F" + wsRow, row.degree.coden);
                wsRow++;
            }

            sheet.Columns[0].ColumnWidth = 15;
            sheet.Columns[1].ColumnWidth = 15;
            sheet.Columns[2].ColumnWidth = 15;
            sheet.Columns[3].ColumnWidth = 24;
            sheet.Columns[4].ColumnWidth = 15;
            sheet.Columns[5].ColumnWidth = 10;

            book.Worksheets[0].Remove();
            book.Worksheets[0].Remove();
            book.Worksheets[0].Remove();

            //適合頁面
            book.ConverterSetting.SheetFitToPage = true;

            //建立檔案
            if (cfg.is_pdf)
            {
                book.SaveToFile(xls_path, Spire.Xls.FileFormat.PDF);
            }
            else
            {
                book.SaveToFile(xls_path, Spire.Xls.ExcelVersion.Version2013);
            }
        }

        //附加大段證 Sheet
        private void AddBigCertSheet(TConfig cfg, Spire.Xls.Workbook book, Spire.Xls.Worksheet sheetTemplate, TRow row)
        {
            var cmt = row.cmt;
            var degree = row.degree;

            Spire.Xls.Worksheet sheet = book.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = row.promotion_id;

            if (cfg.need_headshot == "1")
            {
                var picSrc = sheetTemplate.Pictures[2];

                var head_shot = GetFileFullPath(cfg, row.headshot_fileid, row.headshot_filename);
                if (head_shot.valid)
                {
                    var picUrl = head_shot.path;
                    var picImg = System.Drawing.Image.FromFile(head_shot.path);
                    var picture = sheet.Pictures.Add(picSrc.TopRow, picSrc.LeftColumn, picImg);
                    picture.Width = picSrc.Width;
                    picture.Height = picSrc.Height;
                    picture.Left = picSrc.Left;
                    picture.Top = picSrc.Top;
                }
                else if (head_shot.message != "")
                {
                    throw new Exception(head_shot.message);
                }
            }

            //姓名
            sheet.Range["P1"].Text = " " + row.in_name;
            //證號(體總證號)
            sheet.Range["P2"].Text = " " + row.in_degree_id;
            //生日_民國年
            sheet.Range["P3"].Text = " " + row.birth.cy;
            //生日_月
            sheet.Range["P4"].Text = " " + row.birth.cm;
            //生日_日
            sheet.Range["P5"].Text = " " + row.birth.cd;
            //出生地
            sheet.Range["P6"].Text = " " + row.in_area;
            //段位(數字)
            sheet.Range["P7"].Text = " " + degree.code;
            //課程結束_民國年
            sheet.Range["P8"].Text = " " + cfg.EndDayInfo.cy;
            //課程結束_月
            sheet.Range["P9"].Text = " " + cfg.EndDayInfo.cm;
            //課程結束_日
            sheet.Range["P10"].Text = " " + cfg.EndDayInfo.cd;
            //梯次
            sheet.Range["P11"].Text = " " + cfg.in_echelon;
            //英文名
            sheet.Range["P13"].Text = " " + row.in_en_name;
            //生日_英文格式
            sheet.Range["P14"].Text = " " + row.birth.en_day;
            //段位(數字含英文)
            sheet.Range["P15"].Text = " " + degree.en_display;
            //課程結束_英文格式
            sheet.Range["P16"].Text = " " + cfg.EndDayInfo.en_day;

            // sheet.PageSetup.FitToPagesWide = 1;
            // sheet.PageSetup.FitToPagesTall = 1;
            // sheet.PageSetup.TopMargin = 0;
            // sheet.PageSetup.LeftMargin = 0;
            // sheet.PageSetup.RightMargin = 0;
            // sheet.PageSetup.BottomMargin = 0;
        }

        #endregion 大段證


        private void FixDirAndClearFile(string path)
        {
            if (System.IO.Directory.Exists(path))
            {
                var dir = new System.IO.DirectoryInfo(path);
                dir.Delete(true);
            }

            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }
        }

        private void Zip(string zip_file, string zip_folder)
        {
            //刪除既有檔案
            if (System.IO.File.Exists(zip_file))
            {
                System.IO.File.Delete(zip_file);
            }

            //壓縮資料庫
            using (var zip = new Ionic.Zip.ZipFile(zip_file, System.Text.Encoding.UTF8))
            {
                zip.AddDirectory(zip_folder);
                zip.Comment = "This zip was created at " + System.DateTime.Now.ToString("G");
                zip.Save();
            }
        }

        private Item GetItmDegrees(TConfig cfg, string cmt_sno, string resume_sno)
        {
            string cmt_condition = cmt_sno == ""
                ? ""
                : "AND t2.in_committee_sno = '" + cmt_sno + "'";

            string rsm_condition = resume_sno == ""
                ? ""
                : "AND t2.in_sno = '" + resume_sno + "'";

            string sql = @"
                SELECT
                    t1.id               AS 'mt_id'
                    , t1.in_echelon     AS 'mt_echelon'
                    , t2.id             AS 'promotion_id'
                    , t2.in_name
                    , t2.in_en_name
                    , t2.in_sno
                    , t2.in_gender
                    , t2.in_birth
                    , t2.in_l1
                    , t2.in_area
                    , t2.in_stuff_c1
                    , t2.in_current_org
                    , t2.in_degree_area
                    , t2.in_committee
                    , t2.in_committee_sno
                    , t2.in_committee_short
                    , t2.in_makeup_yn
                    , t2.in_makeup_note
                    , t3.id             AS 'mrid'
                    , t3.in_moe_no
                    , t11.in_degree     AS 'old_degree'
                    , t11.in_degree_id
                    , DATEADD(hour, 8, ISNULL(t11.in_degree_date, t2.in_promotion_date)) AS 'promotion_date'
                    , t11.in_gl_degree
                    , t11.in_gl_degree_id
                    , t11.in_card_degree_issue
                    , t11.in_card_member_issue
                    , t11.in_card_member_id
                    , t21.value    AS 'in_l1_value'
                    , t11.in_photo AS 'headshot_fileid'
                    , t22.filename AS 'headshot_filename'
                FROM
                    IN_CLA_MEETING t1 WITH(NOLOCK)
                INNER JOIN
                    IN_CLA_MEETING_PROMOTION t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    IN_CLA_MEETING_RESUME t3 WITH(NOLOCK)
                    ON t3.in_user = t2.in_user
                INNER JOIN
                    IN_RESUME t11 WITH(NOLOCK)
                    ON t11.in_sno = t2.in_sno
                INNER JOIN 
                    VU_DEGREE t21
                    ON t21.label = t2.in_l1
                LEFT OUTER JOIN
                    [File] t22 WITH(NOLOCK)
                    ON t22.id = t11.in_photo
                WHERE
                    t1.id = '{#meeting_id}'
                    AND t2.in_score_state = N'合格'
                    {#cmt_condition}
                    {#rsm_condition}
                ORDER BY
                    t2.in_committee_sno
                    , t21.value DESC
                    , t2.in_birth
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#cmt_condition}", cmt_condition)
                .Replace("{#rsm_condition}", rsm_condition);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            TExport result = new TExport { templatePath = "", exportPath = "" };

            Item itmXls = cfg.inn.applyMethod("In_Variable_File", "<in_name>" + in_name + "</in_name>");
            result.exportPath = itmXls.getProperty("export_path", "");
            result.templatePath = itmXls.getProperty("template_path", "");

            return result;
        }


        private TZip ZipInfo(TConfig cfg, TExport exp)
        {
            //生成檔案在該活動名稱資料夾下
            //zip_name
            //    中華民國跆拳道協會第202梯次晉段申請_大段證.zip
            //zip_file
            //    c:\site\cta_git\tempvault\meeting_degree\中華民國跆拳道協會第202梯次晉段申請_大段證.zip
            //zip_fldr
            //    c:\site\cta_git\tempvault\meeting_degree\中華民國跆拳道協會第202梯次晉段申請

            TZip result = new TZip();
            result.target_folder = exp.exportPath.Trim('\\');
            result.zip_name = cfg.in_title + "_" + cfg.report_name + ".zip";
            result.zip_file = result.target_folder + "\\" + result.zip_name;
            result.zip_fldr = result.target_folder + "\\" + cfg.in_title;
            return result;
        }

        private List<TCommittee> MapCommittees(TConfig cfg, Item items)
        {
            List<TCommittee> result = new List<TCommittee>();

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                var row = MapRow(cfg, item);

                TCommittee cmt = result.Find(x => x.sno == row.in_committee_sno);
                if (cmt == null)
                {
                    cmt = NewCommittee(cfg, row);
                    result.Add(cmt);
                }

                TDegree degree = cmt.DegreeList.Find(x => x.value == row.in_l1_value);
                if (degree == null)
                {
                    degree = NewDegree(cfg, row);
                    cmt.DegreeList.Add(degree);
                }

                row.cmt = cmt;
                row.degree = degree;

                degree.Rows.Add(row);
            }

            return result;
        }

        private TCommittee NewCommittee(TConfig cfg, TRow row)
        {
            TCommittee result = new TCommittee
            {
                sno = row.in_committee_sno,
                name = row.in_committee,
                short_name = row.in_committee_short,
                DegreeList = new List<TDegree>(),
            };

            result.display = result.sno + result.short_name;

            return result;
        }

        private TDegree NewDegree(TConfig cfg, TRow row)
        {
            TDegree result = new TDegree
            {
                label = row.in_l1,
                value = row.in_l1_value,
                Rows = new List<TRow>(),
            };

            result.code = result.value.Replace("000", "");
            result.coden = GetInt(result.code);
            result.display = result.code + "段";
            result.en_display = CDegreeEnFunc(result.code);
            return result;
        }

        private TRow MapRow(TConfig cfg, Item item)
        {
            TRow result = new TRow
            {
                promotion_id = item.getProperty("promotion_id", ""),
                in_name = item.getProperty("in_name", ""),
                in_en_name = item.getProperty("in_en_name", ""),
                in_sno = item.getProperty("in_sno", ""),
                in_gender = item.getProperty("in_gender", ""),
                in_birth = item.getProperty("in_birth", ""),

                in_current_org = item.getProperty("in_current_org", ""),
                in_area = item.getProperty("in_area", ""),
                in_l1 = item.getProperty("in_l1", ""),
                in_user = item.getProperty("in_user", ""),

                old_degree = item.getProperty("in_degree", ""),
                in_degree_id = item.getProperty("in_degree_id", ""),
                in_gl_degree_id = item.getProperty("in_gl_degree_id", ""),

                mrid = item.getProperty("mrid", ""),
                in_moe_no = item.getProperty("in_moe_no", ""),

                in_committee = item.getProperty("in_committee", ""),
                in_committee_sno = item.getProperty("in_committee_sno", ""),
                in_committee_short = item.getProperty("in_committee_short", ""),

                in_card_degree_issue = item.getProperty("in_card_degree_issue", ""),
                in_card_member_issue = item.getProperty("in_card_member_issue", ""),
                in_card_member_id = item.getProperty("in_card_member_id", ""),

                in_l1_value = item.getProperty("in_l1_value", ""),
                promotion_date = item.getProperty("promotion_date", ""),

                headshot_fileid = item.getProperty("headshot_fileid", ""),
                headshot_filename = item.getProperty("headshot_filename", ""),
            };

            result.birth = MapDay(result.in_birth, 8);
            result.promotion = MapDay(result.promotion_date, 0);

            result.headshot_extname = "";
            if (result.headshot_filename != "")
            {
                var arr = result.headshot_filename.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                if (arr != null && arr.Length > 0)
                {
                    result.headshot_extname = arr.Last();
                }
            }

            return result;
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string aras_vault_url { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }

            public string type { get; set; }
            public string extend { get; set; }
            public string folder { get; set; }
            public string step { get; set; }
            public string cmt_sno { get; set; }
            public string rsm_sno { get; set; }
            public string is_part { get; set; }
            public string need_headshot { get; set; }

            // Meeting 區

            public Item itmMeeting { get; set; }
            public string in_title { get; set; }
            public string in_url { get; set; }
            public string in_register_url { get; set; }
            public string in_meeting_type { get; set; }
            public string in_echelon { get; set; }
            public string in_annual { get; set; }
            public string in_decree { get; set; }
            public string certificate_no { get; set; }
            public string in_date_s { get; set; }
            public string in_date_e { get; set; }
            public string in_coursh_hours { get; set; }
            public string in_address { get; set; }
            public string seminar_type { get; set; }
            public TDay EndDayInfo { get; set; }

            public string report_name { get; set; }
            public string report_variable { get; set; }
            public ReportEnum report_type { get; set; }

            public bool is_pdf { get; set; }

            public string url_mode { get; set; }
        }

        private enum ReportEnum
        {
            big_degree = 100,
            small_degree = 200,
            degree_card = 300,
            member_card = 400,
        }

        private class TExport
        {
            public string templatePath { get; set; }
            public string exportPath { get; set; }
        }

        private class TDay
        {
            public DateTime value { get; set; }
            public string cy { get; set; }
            public string cm { get; set; }
            public string cd { get; set; }
            public string en_day { get; set; }
            public string short_en_day { get; set; }
            public string short_entw_day { get; set; }
            public string long_tw_day { get; set; }
            public string short_tw_day { get; set; }
        }

        private class TCommittee
        {
            public string sno { get; set; }
            public string name { get; set; }
            public string short_name { get; set; }
            public List<TDegree> DegreeList { get; set; }

            public string display { get; set; }
        }

        private class TDegree
        {
            public string label { get; set; }
            public string value { get; set; }
            public string code { get; set; }
            public int coden { get; set; }
            public string display { get; set; }
            public string en_display { get; set; }
            public List<TRow> Rows { get; set; }
        }

        private class TRow
        {
            public string promotion_id { get; set; }

            public string in_name { get; set; }
            public string in_en_name { get; set; }
            public string in_sno { get; set; }
            public string in_gender { get; set; }
            public string in_birth { get; set; }

            public string in_current_org { get; set; }
            public string in_area { get; set; }
            public string in_l1 { get; set; }

            public string in_committee { get; set; }
            public string in_committee_sno { get; set; }
            public string in_committee_short { get; set; }

            public string in_user { get; set; }

            public string mrid { get; set; }
            public string in_moe_no { get; set; }


            public string in_l1_value { get; set; }
            public string promotion_date { get; set; }

            //Resume

            public string old_degree { get; set; }
            public string in_degree_id { get; set; }
            public string in_gl_degree_id { get; set; }

            public string in_card_degree_issue { get; set; }
            public string in_card_member_issue { get; set; }
            public string in_card_member_id { get; set; }

            public string headshot_fileid { get; set; }
            public string headshot_filename { get; set; }
            public string headshot_extname { get; set; }

            public TDay birth { get; set; }
            public TDay promotion { get; set; }

            public TCommittee cmt { get; set; }
            public TDegree degree { get; set; }
        }

        private class TZip
        {
            public string target_folder { get; set; }
            public string zip_name { get; set; }
            public string zip_file { get; set; }
            public string zip_fldr { get; set; }
        }

        private class TRange
        {
            public int idxs { get; set; }
            public int idxe { get; set; }
        }

        #region 圖片

        private class TPhoto
        {
            public bool valid { get; set; }
            public string id { get; set; }
            public string name { get; set; }
            public string message { get; set; }
            public string path { get; set; }
        }

        //取得檔案完整絕對路徑
        private TPhoto GetFileFullPath(TConfig cfg, string fileid, string filename)
        {
            TPhoto result = new TPhoto { id = fileid, name = filename };
            if (result.id == "")
            {
                result.valid = false;
                result.message = "";//無大頭照 id
            }
            else if (result.id == "")
            {
                result.valid = false;
                result.message = "";//該檔案無檔名
            }
            else
            {
                //路徑切出來
                string id_1 = fileid.Substring(0, 1);
                string id_2 = fileid.Substring(1, 2);
                string id_3 = fileid.Substring(3, 29);
                string path = cfg.aras_vault_url + @"\" + id_1 + @"\" + id_2 + @"\" + id_3 + @"\" + result.name;
                //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "headShot path: " + path);

                if (!System.IO.File.Exists(path))
                {
                    result.valid = false;
                    result.message = "檔案不存在";
                }
                else if (!IsImage(path))
                {
                    result.valid = false;
                    result.message = "非圖片";
                }
                else
                {
                    result.valid = true;
                    result.message = "";
                    result.path = path;
                }
            }

            return result;
        }

        //判斷檔案是否真為圖片
        private bool IsImage(string path)
        {
            bool result = false;
            try
            {
                using (var img = System.Drawing.Image.FromFile(path))
                {
                    result = true;
                }
            }
            catch (Exception e)
            {
            }
            return result;
        }
        #endregion 圖片

        private string CDegreeEnFunc(string value)
        {
            switch (value)
            {
                case "1": return "1st";
                case "2": return "2nd";
                case "3": return "3rd";
                case "4": return "4th";
                case "5": return "5th";
                case "6": return "6th";
                case "7": return "7th";
                case "8": return "8th";
                case "9": return "9th";
                default: return "";
            }
        }

        private TDay MapDay(string value, int hours)
        {
            TDay result = new TDay
            {
                value = GetDateTime(value, hours),
                cy = "",
                cm = "",
                cd = "",
                en_day = "",
                long_tw_day = "",
                short_tw_day = "",
            };

            if (result.value != DateTime.MinValue)
            {
                result.cy = (result.value.Year - 1911).ToString();
                result.cm = result.value.Month.ToString("00");
                result.cd = result.value.Day.ToString("00");

                result.en_day = result.value.ToString("dd.MMM.yyyy", new System.Globalization.CultureInfo("EN-US"));
                result.short_en_day = result.value.Year + "/" + result.cm + "/" + result.cd;
                result.short_entw_day = result.value.Year + "年" + result.cm + "月" + result.cd + "日";
                result.long_tw_day = "中華民國" + result.cy + "年" + result.cm + "月" + result.cd + "日";
                result.short_tw_day = result.cy + "年" + result.cm + "月" + result.cd + "日";
            }

            return result;
        }

        private TRange NewRange(string value, int count)
        {
            int max_idx = count - 1;

            TRange result = new TRange();

            if (value == "")
            {
                result.idxs = 0;
                result.idxe = count - 1;
                return result;
            }

            string[] arr = value.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr != null && arr.Length == 2)
            {
                result.idxs = GetInt(arr[0]) - 1;
                result.idxe = GetInt(arr[1]) - 1;

                if (result.idxs > max_idx)
                {
                    result.idxs = max_idx;
                }

                if (result.idxe > max_idx)
                {
                    result.idxe = max_idx;
                }

                //起迄交換
                if (result.idxs > result.idxe)
                {
                    int tmp = result.idxs;
                    result.idxs = result.idxe;
                    result.idxe = tmp;
                }
            }

            return result;
        }

        private DateTime GetDateTime(string value, int hours)
        {
            if (value == "") return DateTime.MinValue;
            DateTime dt = DateTime.MinValue;
            DateTime.TryParse(value, out dt);
            return dt.AddHours(hours);
        }

        private int GetInt(string value, int defV = 0)
        {
            if (value == "") return 0;

            int result = defV;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return defV;
        }
    }
}