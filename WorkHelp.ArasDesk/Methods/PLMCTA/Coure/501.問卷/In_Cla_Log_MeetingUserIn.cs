﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_Cla_Log_MeetingUserIn : Item
    {
        public In_Cla_Log_MeetingUserIn(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            //由InnoJSON.ashx呼叫
            /*
            視使用者報名狀況傳回不同結果，若使用者報名狀況為正取(in_note_state="official")，result回傳物件id
                若使用者報名狀況為備取(in_note_state="waiting")，回傳"waiting"
                若使用者報名狀況為取消(in_note_state="cancel")，回傳"cancel"
                若使用者未報名(使用者不存在)，回傳零長度空白("")
            */

            Innovator inn = this.getInnovator();
            string muEmail = this.getProperty("email", "no_data");
            string muSNO = this.getProperty("sno", "no_data");
            string meeting_id = this.getProperty("meeting_id", "no_data");


            if ((muEmail == "no_data" && muSNO == "no_data") || meeting_id == "no_data")
            {
                return inn.newError(" Missing Parameter muEmail= " + muEmail + " ; meeting_id = " + meeting_id + " ; ");
            }
            Item mu = inn.newItem("In_Cla_Meeting_User", "get");
            mu.setAttribute("select", "in_note_state,id");
            if (muEmail != "no_data" && muEmail != "") { mu.setProperty("in_mail", muEmail); }
            if (muSNO != "no_data") { mu.setProperty("in_sno", muSNO); }
            mu.setProperty("source_id", meeting_id);
            mu = mu.apply();
            //ssss

            if (mu.isEmpty())
            {
                return inn.newResult("");
            }
            else
            {
                switch (mu.getProperty("in_note_state", ""))
                {
                    case "official": return inn.newResult(mu.getID());
                    case "waiting": return inn.newResult("waiting");
                    case "cancel": return inn.newResult("cancel");
                    default: return inn.newResult("");
                }
            }
        }

        private string InFuncTime(Innovator inn, Item itmMUser)
        {
            string muid = itmMUser.getID();
            string meeting_id = itmMUser.getProperty("source_id", "");

            string sql = @"
SELECT 
	DATEADD(hour, 8, in_date_s) AS 'in_date_s'
	, DATEADD(hour, 8, in_date_e) AS 'in_date_e'
	, in_purpose
	, in_action 
FROM 
	IN_CLA_MEETING_FUNCTIONTIME WITH(NOLOCK) 
WHERE 
	source_id = '{#meeting_id}'
	AND in_action LIKE 'sheet%'
	AND in_action <> 'sheet1'
ORDER BY
	in_date_s
    ";

            Item items = inn.applySQL(sql);

            if (items.isError() || items.getResult() == "")
            {
                return muid;
            }

            var now = DateTime.Now;
            var surveytype = "";
            var in_time = 0;

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                
                DateTime dtS = GetDtmVal(item.getProperty("in_date_s", ""));
                DateTime dtE = GetDtmVal(item.getProperty("in_date_e", ""));
                if (now >= dtS && now <= dtE)
                {
                    surveytype = item.getProperty("in_action", "").Replace("sheet", "");
                    in_time++;
                }
            }

            if (in_time != 1)
            {
                return muid;
            }

            
            sql = "SELECT TOP 1 id FROM IN_CLA_MEETING_SURVEYS_RESULT WITH(NOLOCK)"
                + " WHERE in_participant = '" + muid + "'"
                + "AND in_surveytype = '" + surveytype + "'";

            Item itmMtSvResult = inn.applySQL(sql);
            if (!itmMtSvResult.isError() && itmMtSvResult.getResult() == "")
            {
                return "surveytype_" + surveytype;
            }
            else
            {
                return muid;
            }
        }

        private DateTime GetDtmVal(string value)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            return result;
        }
        
    }
}