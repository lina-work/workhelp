﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_Cla_Export_Tables2 : Item
    {
        public In_Cla_Export_Tables2(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 問卷統計
    日誌: 
        - 2022-03-22: 新增 修改投票標題及最大參選人數 (willy)
        - 2022-02-14: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Cla_Export_Tables2";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                surveytype = itmR.getProperty("surveytype", ""),
                scene = itmR.getProperty("scene", ""),
            };

            string sql = "";

            sql = "SELECT in_title FROM IN_CLA_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                return itmR;
            }
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");

            switch (cfg.scene)
            {
                case "table":
                    Table(cfg, itmR);
                    break;

                case "muser_table":
                    MUserTable(cfg, itmR);
                    break;

                case "xlsx_func":
                    Xlsx(cfg, itmR);
                    break;

                case "xlsx_meeting":
                    cfg.surveytype = "";
                    Xlsx(cfg, itmR);
                    break;

                case "modal"://跳窗
                    Modal(cfg, itmR);
                    break;

                case "update"://設定候選名單
                    Update(cfg, itmR);
                    break;

                case "edit":
                    Edit(cfg, itmR);
                    break;

                case "change"://變更問卷結果
                    Change(cfg, itmR);
                    break;

                case "remove"://清除問卷結果
                    Remove(cfg, itmR);
                    break;

                default:
                    Page(cfg, itmR);
                    break;
            }

            return itmR;
        }

        //清除問卷結果
        private void Remove(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            sql = "DELETE FROM IN_CLA_MEETING_SURVEYS_RESULT"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_surveytype = '" + cfg.surveytype + "'";

            Item itmDelete = cfg.inn.applySQL(sql);

            if (itmDelete.isError())
            {
                throw new Exception("清除資料發生錯誤");
            }
        }

        //變更問卷結果
        private void Change(TConfig cfg, Item itmReturn)
        {
            string mrid = itmReturn.getProperty("mrid", "");
            string in_answer = itmReturn.getProperty("in_answer", "");

            string sql = "SELECT id, in_answer FROM IN_CLA_MEETING_SURVEYS_RESULT WITH(NOLOCK) WHERE id = '" + mrid + "'";
            Item itemMtSvResult = cfg.inn.applySQL(sql);

            if (itemMtSvResult.isError() || itemMtSvResult.getResult() == "")
            {
                throw new Exception("查無該問卷結果: " + sql);
            }
            string old_answer = itemMtSvResult.getProperty("in_answer", "");

            string sql_old = "UPDATE IN_CLA_MEETING_SURVEYS_RESULT SET "
                + " in_answer = N'" + old_answer + "'"
                + " WHERE id = '" + mrid + "'";
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "復原 sql: " + sql_old);

            string sql_update = "UPDATE IN_CLA_MEETING_SURVEYS_RESULT SET "
                + " in_answer = N'" + in_answer + "'"
                + " WHERE id = '" + mrid + "'";
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "更新 sql: " + sql_update);

            Item itmUpd = cfg.inn.applySQL(sql_update);

            if (itmUpd.isError())
            {
                throw new Exception("變更問卷結果 發生錯誤");
            }
        }

        //設定候選名單
        private void Update(TConfig cfg, Item itmReturn)
        {
            string surveyid = itmReturn.getProperty("surveyid", "");
            string in_selectoption = itmReturn.getProperty("in_selectoption", "");

            string sql = "UPDATE IN_CLA_SURVEY SET "
                + " in_selectoption = N'" + ClearSelectOption(in_selectoption) + "'"
                + " WHERE id = '" + surveyid + "'";

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmUpd = cfg.inn.applySQL(sql);

            if (itmUpd.isError())
            {
                throw new Exception("設定候選名單 發生錯誤");
            }
        }

        //2022-03-22新增 修改投票標題及最大參選人數
        private void Edit(TConfig cfg, Item itmReturn)
        {
            string surveyid = itmReturn.getProperty("surveyid", "");
            string in_questions = itmReturn.getProperty("in_questions", "");
            string in_max_length = itmReturn.getProperty("in_max_length", "1");

            string sql = "UPDATE IN_CLA_SURVEY SET "
                + " in_questions = N'" + in_questions + "'"
                + ",in_max_length = N'" + in_max_length + "'"
                + " WHERE id = '" + surveyid + "'";

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmEdit = cfg.inn.applySQL(sql);

            if (itmEdit.isError())
            {
                throw new Exception("設定投票資料 發生錯誤");
            }
        }

        private string ClearSelectOption(string value)
        {
            string[] rows = value.Replace("\r", "").Split(new char[] { '\n', ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (rows == null || rows.Length == 0)
            {
                return "";
            }

            List<string> list = new List<string>();
            foreach (var row in rows)
            {
                string[] cols = row.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                if (cols == null || cols.Length == 0)
                {
                    continue;
                }

                string name = "";
                if (cols.Length == 1)
                {
                    name = cols[0];
                }
                else
                {
                    name = cols[1];
                }

                string no = (list.Count + 1).ToString();
                list.Add(no + "." + name);
            }

            return "@" + string.Join("@", list);
        }

        //跳窗
        private void Modal(TConfig cfg, Item itmReturn)
        {
            string surveyid = itmReturn.getProperty("surveyid", "");

            string sql = "SELECT in_action, in_purpose FROM IN_CLA_MEETING_FUNCTIONTIME WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_action = 'sheet" + cfg.surveytype + "'";
            cfg.itmMtFuncTimes = cfg.inn.applySQL(sql);

            Item itmMtSurvey = GetMtSurveyItems(cfg, cfg.surveytype, surveyid);
            string in_selectoption = itmMtSurvey.getProperty("in_selectoption", "").TrimStart('@');
            in_selectoption = in_selectoption.Replace("@", "\n");

            itmReturn.setProperty("functime_label", cfg.itmMtFuncTimes.getProperty("in_purpose", ""));
            itmReturn.setProperty("survey_label", SurveyLabel(itmMtSurvey));
            itmReturn.setProperty("survey_options", in_selectoption);
            //2022-03-22 修改功能的欄位
            itmReturn.setProperty("vote_title", itmMtSurvey.getProperty("in_questions", ""));
            itmReturn.setProperty("max_number", itmMtSurvey.getProperty("in_max_length", "1"));
        }

        #region 匯出

        private void Xlsx(TConfig cfg, Item itmReturn)
        {
            cfg.CharSet = GetCharSet();

            cfg.itmMtFuncTimes = GetMtFuncItems(cfg, cfg.surveytype);

            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name></in_name>");
            string export_Path = itmXls.getProperty("export_path", "");
            string template_Path = itmXls.getProperty("template_path", "");

            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();

            int func_count = cfg.itmMtFuncTimes.getItemCount();

            for (var i = 0; i < func_count; i++)
            {
                Item itmMtFuncTime = cfg.itmMtFuncTimes.getItemByIndex(i);
                string surveytype = itmMtFuncTime.getProperty("in_action", "").Replace("sheet", "");

                Item items = GetMtSurveyItems(cfg, surveytype, "");
                int count = items.getItemCount();
                for (int j = 0; j < count; j++)
                {
                    Item item = items.getItemByIndex(j);
                    AppendSurveySheet(cfg, workbook, surveytype, item);
                }
            }

            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();

            string in_purpose = func_count <= 1
                ? cfg.itmMtFuncTimes.getProperty("in_purpose", "")
                : "總結果";

            string ext_name = ".xlsx";

            string xls_name = cfg.mt_title
                + "_" + in_purpose
                + "_" + DateTime.Now.ToString("MMdd_HHmmss");

            string xls_file = export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);

            itmReturn.setProperty("xls_name", xls_url);
        }

        //統計
        private void AppendSurveySheet(TConfig cfg, Spire.Xls.Workbook workbook, string surveytype, Item itmSurvey)
        {
            //問項資料
            string surveyid = itmSurvey.getProperty("survey_id", "");
            string in_questions = itmSurvey.getProperty("in_questions", "");
            string sheetName = in_questions.Split(new char[] { '(' }, StringSplitOptions.RemoveEmptyEntries).First();


            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = sheetName;

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "序號", property = "no", css = TCss.Center, width = 15 });
            fields.Add(new TField { title = "候選人", property = "name", css = TCss.Center, width = 32 });
            fields.Add(new TField { title = "類別", property = "type", css = TCss.Center, width = 15 });
            fields.Add(new TField { title = "得票數", property = "count", css = TCss.Center, width = 18 });

            MapCharSetAndIndex(cfg, fields);

            //資料轉換
            var entity = GetOptResult(cfg, surveytype, surveyid);

            var fs = fields[0];
            var fe = fields.Last();

            int wsRow = 1;

            //活動標題
            var mt_title_mr = sheet.Range[fs.cs + wsRow + ":" + fe.cs + wsRow];
            mt_title_mr.Merge();
            mt_title_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            mt_title_mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            mt_title_mr.Style.Font.Size = 12;
            //mt_title_mr.Style.Font.FontName = "標楷體";
            mt_title_mr.Style.Font.IsBold = true;
            mt_title_mr.Text = cfg.mt_title;
            sheet.SetRowHeight(wsRow, 25);
            wsRow++;

            //問項標題
            var sv_title_mr = sheet.Range[fs.cs + wsRow + ":" + fe.cs + wsRow];
            sv_title_mr.Merge();
            sv_title_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;
            sv_title_mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            sv_title_mr.Style.Font.Size = 12;
            //mt_title_mr.Style.Font.FontName = "標楷體";
            //mt_title_mr.Style.Font.IsBold = true;
            sv_title_mr.Text = in_questions;
            sheet.SetRowHeight(wsRow, 25);
            wsRow++;


            ////標題列
            //SetHeadRow(sheet, wsRow, fields);
            //wsRow++;

            ////凍結窗格
            sheet.FreezePanes(wsRow, fe.ci + 1);


            //標題列
            SetHeadRow(sheet, wsRow, fields);
            wsRow++;

            string last_type = entity.TypeList.Count > 0 ? entity.TypeList[0].type : "";

            //資料容器
            Item item = cfg.inn.newItem();

            foreach(var sub in entity.TypeList)
            {
                if (sub.type != last_type)
                {
                    //標題列
                    SetHeadRow(sheet, wsRow, fields);
                    wsRow++;
                    last_type = sub.type;
                }

                int no = 1;

                for (int i = 0; i < entity.List.Count; i++)
                {

                    var opt = entity.List[i];
                    item.setProperty("no", no.ToString());
                    item.setProperty("name", opt.name);
                    item.setProperty("type", opt.type);
                    item.setProperty("count", opt.count.ToString());

                    SetItemCell(cfg, sheet, wsRow, item, fields);
                    no++;

                    wsRow++;
                }
            }

            //設定格線
            SetRangeBorder(sheet, fields, 1, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        //設定資料列
        private void SetItemCell(TConfig cfg, Spire.Xls.Worksheet sheet, int wsRow, Item item, List<TField> fields)
        {
            foreach (var field in fields)
            {
                var cr = field.cs + wsRow;
                var va = "";

                if (field.getValue != null)
                {
                    va = field.getValue(cfg, field, item);
                }
                else if (field.property != "")
                {
                    va = item.getProperty(field.property, "");
                }

                SetCell(sheet, cr, va, field.css);
            }
        }

        //設定資料格
        private void SetCell(Spire.Xls.Worksheet sheet, string cr, string value, TCss css)
        {
            var range = sheet.Range[cr];
            range.Style.Font.Size = 12;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;

            switch (css)
            {
                case TCss.None:
                    range.Text = value;
                    break;
                case TCss.Center:
                    range.Text = value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Text:
                    range.Text = "'" + value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Number:
                    range.NumberValue = GetIntVal(value);
                    range.NumberFormat = "0";
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                    break;
            }
        }

        //設定標題列
        private void SetHead(Spire.Xls.Worksheet sheet, string cr, string value)
        {
            var range = sheet.Range[cr];
            range.Text = value;
            range.Style.Font.Size = 12;
            range.Style.Font.IsBold = true;
            //range.Style.Font.Color = System.Drawing.Color.White;
            //range.Style.Color = System.Drawing.Color.FromArgb(41, 92, 144);//#295C90
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, List<TField> fields, int mnRow, int wsRow)
        {
            var tbl_ps = fields.First().cs + mnRow;
            var tbl_pe = fields.Last().cs + (wsRow - 1);
            SetRangeBorder(sheet, tbl_ps + ":" + tbl_pe);
        }

        private void MapCharSet(TConfig cfg, List<TField> fields)
        {
            foreach (var field in fields)
            {
                field.cs = cfg.CharSet[field.ci];
            }
        }

        private void MapCharSetAndIndex(TConfig cfg, List<TField> fields)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                field.ci = i + 1;
                field.cs = cfg.CharSet[field.ci];
            }
        }

        private void SetHeadRow(Spire.Xls.Worksheet sheet, int wsRow, List<TField> fields)
        {
            foreach (var field in fields)
            {
                string cr = field.cs + wsRow;
                SetHead(sheet, cr, field.title);
            }
        }

        private void AutoFit(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].AutoFitColumns();
            }
        }

        private void SetColumnWidth(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].ColumnWidth = field.width;
            }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public TCss css { get; set; }
            public int ci { get; set; }
            public string cs { get; set; }
            public int width { get; set; }
            public Func<TConfig, TField, Item, string> getValue { get; set; }

            public bool is_merge { get; set; }
        }

        private enum TCss
        {
            None = 0,
            Center = 10,
            Text = 100,
            Number = 200,
            Money = 300,
            Day = 400,
        }

        private string[] GetCharSet()
        {
            return new string[] { "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        }
        #endregion 匯出

        private void MUserTable(TConfig cfg, Item itmReturn)
        {
            var surveyid = itmReturn.getProperty("surveyid", "");
            var contents = MUserTableContents(cfg, cfg.surveytype, surveyid);
            itmReturn.setProperty("inn_tables", contents);
        }

        private class TMSResult
        {
            public string id { get; set; }
            public string in_name { get; set; }
            public string in_answer { get; set; }
            public string in_time { get; set; }

            public Item value { get; set; }
        }

        private string MUserTableContents(TConfig cfg, string surveytype, string surveyid)
        {
            TSuvey result = new TSuvey { MUser_Count = 0, List = new List<TOpt>(), TypeList = new List<TSub>() };

            Item itmSurvey = cfg.inn.applySQL("SELECT * FROM IN_CLA_SURVEY WITH(NOLOCK) WHERE id = '" + surveyid + "'");
            string in_selectoption = itmSurvey.getProperty("in_selectoption", "");
            string[] arr = in_selectoption.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr == null || arr.Length == 0)
            {
                return "";
            }

            Item items = GetMeetingSurveyResults(cfg, surveytype, surveyid);
            int count = items.getItemCount();

            if (count <= 0)
            {
                return "";
            }


            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            //標題列
            head.Append("<thead>");
            head.Append("<tr>");
            head.Append("<th class='col-md-1 text-center' data-name='no'>序號</th>");
            head.Append("<th class='col-md-1 text-center' data-name='key'>鍵</th>");
            head.Append("<th class='col-md-1 text-center' data-name='time'>時間</th>");
            head.Append("<th class='text-center' data-name='answer'>結果</th>");
            head.Append("<th class='col-md-1 text-center'>更新</th>");
            head.Append("</tr>");
            head.Append("</thead>");

            int no = 1;
            //資料列
            body.Append("<tbody>");

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("mrid", "");
                string in_name = item.getProperty("in_name", "");
                string in_sno = item.getProperty("in_sno", "");
                string in_answer = item.getProperty("in_answer", "");
                string created_on = GetDtmVal(item.getProperty("created_on", ""), hours: 8).ToString("MM-dd HH:mm");

                body.Append("<tr data-id='" + id + "'>");
                body.Append("<td class='text-center' data-name='no'>" + no + "</td>");
                body.Append("<td class='text-center' data-name='key'>" + in_sno + "</td>");
                body.Append("<td class='text-center' data-name='time'>" + created_on + "</td>");
                body.Append("<td class='text-left' data-name='answer'>" + CheckBoxList(arr, in_answer) + "</td>");
                body.Append("<td class='text-center'><label class='opt_result'> -- </label></td>");
                body.Append("</tr>");

                no++;
            }
            body.Append("</tbody>");

            string table_id = "Inn_MtSvResult_Relationship";

            StringBuilder table = new StringBuilder();
            table.Append("<h4>筆數: " + count + " 人</h4>");
            table.Append("<hr/>");
            table.Append("<table id='" + table_id + "' class='table table-hover' data-toggle='table'>");
            table.Append(head);
            table.Append(body);
            table.Append("</table>");
            table.Append("  <script>");
            table.Append("      $('#" + table_id + "').bootstrapTable();");
            table.Append("      if($(window).width() <= 768) { $('#" + table_id + "').bootstrapTable('toggleView');}");
            table.Append("  </script>");
            return table.ToString();
        }

        private string CheckBoxList(string[] options, string values)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<div class='form-inline'>");
            foreach (var option in options)
            {
                string cked = values.Contains(option) ? "checked='checked'" : "";

                builder.Append("<div class='form-control opt_box'>");
                builder.Append("  <label class='checkbox'>");

                builder.Append("  <input type='checkbox' class='checkbox opt_chk' " + cked
                    + " onclick='Option_Click(this)'"
                    + " value='" + option + "' />");

                builder.Append("  " + option);
                builder.Append("  </label>");
                builder.Append("</div>");
            }
            builder.Append("</div>");
            return builder.ToString();
        }

        private void Table(TConfig cfg, Item itmReturn)
        {
            var surveyid = itmReturn.getProperty("surveyid", "");
            var entity = GetOptResult(cfg, cfg.surveytype, surveyid);
            var contents = TableContents(cfg, entity);
            itmReturn.setProperty("inn_tables", contents);
        }

        private string TableContents(TConfig cfg, TSuvey entity)
        {
            //if (entity.TypeList.Count == 0) return "";

            StringBuilder body = new StringBuilder();

            //標題列
            body.Append("<thead>");
            body.Append("<tr>");
            body.Append("<th class='mailbox-subject text-center' data-name='no'>序號</th>");
            body.Append("<th class='mailbox-subject text-center' data-name='name'>候選人</th>");
            body.Append("<th class='mailbox-subject text-center' data-name='type'>類別</th>");
            body.Append("<th class='mailbox-subject text-center' data-name='count'>得票數</th>");
            body.Append("</tr>");
            body.Append("</thead>");

            body.Append("<tbody>");

            string last_type = entity.TypeList.Count > 0 ? entity.TypeList[0].type : "";

            foreach (var sub in entity.TypeList)
            {
                if (sub.type != last_type)
                {
                    //標題列
                    body.Append("<tr>");
                    body.Append("<td class='mailbox-subject text-center' data-name='no'>序號</td>");
                    body.Append("<td class='mailbox-subject text-center' data-name='name'>候選人</td>");
                    body.Append("<td class='mailbox-subject text-center' data-name='type'>類別</td>");
                    body.Append("<td class='mailbox-subject text-center' data-name='count'>得票數</td>");
                    body.Append("</tr>");
                    last_type = sub.type;
                }

                int no = 1;
                for (int i = 0; i < sub.List.Count; i++)
                {
                    var opt = sub.List[i];

                    body.Append("<tr>");
                    body.Append("<td class='text-center' data-name='no'>" + no + "</td>");
                    body.Append("<td class='text-center' data-name='name'>"
                        //+ "<img src='../images/vote/" + opt.img + ".png' style='width: 48px;height: 48px;'>"
                        + " " + opt.name
                        + "</td>");
                    body.Append("<td class='text-center' data-name='type'>" + opt.type + "</td>");
                    body.Append("<td class='text-center' data-name='count'>" + opt.count + "</td>");
                    body.Append("</tr>");
                    no++;
                }
            }
            body.Append("</tbody>");

            string table_id = "Inn_Result_relationship";

            StringBuilder table = new StringBuilder();
            //table.Append("<h4>有效投票人數: " + entity.MUser_Count + " 人</h4>");
            table.Append("<hr/>");
            table.Append("<table id='" + table_id + "' class='table table-hover' data-toggle='table'>");
            table.Append(body);
            table.Append("</table>");
            table.Append("  <script>");
            table.Append("      $('#" + table_id + "').bootstrapTable();");
            table.Append("      if($(window).width() <= 768) { $('#" + table_id + "').bootstrapTable('toggleView');}");
            table.Append("  </script>");
            return table.ToString();
        }

        private string TableContents2(TConfig cfg, TSuvey entity)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            //標題列
            head.Append("<thead>");
            head.Append("<tr>");
            head.Append("<th class='text-center' data-name='no'>序號</th>");
            head.Append("<th class='text-center' data-name='name'>候選人</th>");
            head.Append("<th class='text-center' data-name='type'>類別</th>");
            head.Append("<th class='text-center' data-name='count'>得票數</th>");
            head.Append("</tr>");
            head.Append("</thead>");

            int no = 1;
            //資料列
            body.Append("<tbody>");
            for (int i = 0; i < entity.List.Count; i++)
            {
                var opt = entity.List[i];
                body.Append("<tr>");
                body.Append("<td class='text-center' data-name='no'>" + no + "</td>");
                body.Append("<td class='text-center' data-name='name'>"
                    //+ "<img src='../images/vote/" + opt.img + ".png' style='width: 48px;height: 48px;'>"
                    + " " + opt.name
                    + "</td>");
                body.Append("<td class='text-center' data-name='type'>" + opt.type + "</td>");
                body.Append("<td class='text-center' data-name='count'>" + opt.count + "</td>");
                body.Append("</tr>");
                no++;
            }
            body.Append("</tbody>");

            string table_id = "Inn_Result_relationship";

            StringBuilder table = new StringBuilder();
            //table.Append("<h4>有效投票人數: " + entity.MUser_Count + " 人</h4>");
            table.Append("<hr/>");
            table.Append("<table id='" + table_id + "' class='table table-hover' data-toggle='table'>");
            table.Append(head);
            table.Append(body);
            table.Append("</table>");
            table.Append("  <script>");
            table.Append("      $('#" + table_id + "').bootstrapTable();");
            table.Append("      if($(window).width() <= 768) { $('#" + table_id + "').bootstrapTable('toggleView');}");
            table.Append("  </script>");
            return table.ToString();
        }

        private TSuvey GetOptResult(TConfig cfg, string surveytype, string surveyid)
        {
            TSuvey result = new TSuvey { MUser_Count = 0, List = new List<TOpt>(), TypeList = new List<TSub>() };

            Item itmSurvey = cfg.inn.applySQL("SELECT * FROM IN_CLA_SURVEY WITH(NOLOCK) WHERE id = '" + surveyid + "'");
            string in_questions = itmSurvey.getProperty("in_questions", "");
            string in_selectoption = itmSurvey.getProperty("in_selectoption", "");
            string[] arr = in_selectoption.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr == null || arr.Length == 0)
            {
                return result;
            }

            foreach (var name in arr)
            {
                if (result.List.Exists(x => x.name == name))
                {
                    continue;
                }

                result.List.Add(NewOpt(cfg, in_questions, name)); ;
            }

            Item items = GetMeetingSurveyResults(cfg, surveytype, surveyid);
            int count = items.getItemCount();

            if (count <= 0)
            {
                return result;
            }

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_answer = item.getProperty("in_answer", "").Trim();
                string[] answer_arr = in_answer.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (answer_arr == null || answer_arr.Length == 0)
                {
                    continue;
                }

                foreach (var answer in answer_arr)
                {
                    var opt = result.List.Find(x => x.name == answer);
                    if (opt == null)
                    {
                        opt = NewOpt(cfg, in_questions, answer);
                        result.List.Add(opt);
                    }

                    opt.count++;
                }
            }

            result.MUser_Count = count;

            result.TypeList = MapTypeList(result.List);

            result.List = result.List.OrderByDescending(x => x.count)
                .ThenBy(x => x.num).ToList();

            return result;
        }

        private List<TSub> MapTypeList(List<TOpt> options)
        {
            List<TSub> list = new List<TSub>();

            for (int i = 0; i < options.Count; i++)
            {
                var opt = options[i];
                var sub = list.Find(x => x.type == opt.type);
                if (sub == null)
                {
                    sub = new TSub
                    {
                        type = opt.type,
                        List = new List<TOpt>(),
                    };
                    list.Add(sub);
                }

                sub.List.Add(opt);
            }

            foreach (var sub in list)
            {
                sub.List = sub.List
                    .OrderByDescending(x => x.count)
                    .ThenBy(x => x.num).ToList();
            }

            return list;
        }

        private Item GetMeetingSurveyResults(TConfig cfg, string surveytype, string surveyid)
        {
            string sql = @"
                SELECT
	                t1.id                   AS 'mrid'
	                , t1.in_answer
                    , t1.created_on
	                , t2.id                 AS 'survey_id'
	                , t2.in_selectoption
	                , t3.id                 AS 'muid'
	                , t3.in_name
	                , t3.in_sno
                FROM
	                IN_CLA_MEETING_SURVEYS_RESULT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_CLA_SURVEY t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                INNER JOIN
	                IN_CLA_MEETING_USER t3 WITH(NOLOCK)
	                ON t3.id = t1.in_participant
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_surveytype = '{#surveytype}'
	                AND t1.related_id = '{#surveyid}'
                ORDER BY
	                t1.created_on
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#surveytype}", surveytype)
                .Replace("{#surveyid}", surveyid);

            Item items = cfg.inn.applySQL(sql);

            return items;
        }

        private TOpt NewOpt(TConfig cfg, string question, string name)
        {
            TOpt opt = new TOpt
            {
                question = question,
                surveyid = cfg.surveyid,
                name = name,
                type = "",
                count = 0,
            };

            if (name.Contains("("))
            {
                opt.type = name.Split(new char[] { '(' }, StringSplitOptions.RemoveEmptyEntries)
                    .Last().Replace(")", "");
            }
            else
            {
                if (question.Contains("理事長"))
                {
                    opt.type = "理事長";
                }
                else if (question.Contains("監事"))
                {
                    opt.type = "監事";
                }
            }

            var nm_arr = SplitName(opt.name);
            opt.num = GetIntVal(nm_arr[0]);
            opt.img = nm_arr[1];
            return opt;
        }

        private string[] SplitName(string value)
        {
            string[] result = { "", "" };
            string[] arr = value.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr == null || arr.Length == 0)
            {
                result[1] = "none";//無圖片
            }
            else if (arr.Length == 1)
            {
                result[1] = arr[0].Trim();
            }
            else
            {
                result[0] = arr[0].Trim();
                result[1] = arr[1].Trim();
            }
            return result;
        }

        private class TSuvey
        {
            public int MUser_Count { get; set; }
            public List<TOpt> List { get; set; }
            public List<TSub> TypeList { get; set; }
        }

        private class TSub
        {
            public string type { get; set; }
            public List<TOpt> List { get; set; }
        }

        private class TOpt
        {
            public string question { get; set; }
            public string surveyid { get; set; }
            public int num { get; set; }
            public string img { get; set; }
            public string name { get; set; }
            public string type { get; set; }
            public int count { get; set; }
        }

        //頁面
        private void Page(TConfig cfg, Item itmReturn)
        {
            cfg.itmMtFuncTimes = GetMtFuncItems(cfg);

            cfg.MtFuncTimeMap = MapFuncTime(cfg, cfg.itmMtFuncTimes);
            if (cfg.surveytype == "" || !cfg.MtFuncTimeMap.ContainsKey(cfg.surveytype))
            {
                //問卷類型未設定 OR 類型有錯
                cfg.itmMtFuncTime = cfg.itmMtFuncTimes.getItemByIndex(0);
                cfg.surveytype = GetSurveyType(cfg.itmMtFuncTime.getProperty("in_action", ""));
            }
            else
            {
                cfg.itmMtFuncTime = cfg.MtFuncTimeMap[cfg.surveytype];
            }

            //活動資訊
            itmReturn.setProperty("in_title", cfg.mt_title);
            //itmReturn.setProperty("inn_surveytype_label", cfg.itmMtFuncTime.getProperty("in_purpose", ""));

            //問卷類型選單
            AppendSurveyTypeMenu(cfg, itmReturn);
            //問卷選單
            AppendSurveyMenu(cfg, itmReturn);
        }


        //問卷類型選單
        private void AppendSurveyTypeMenu(TConfig cfg, Item itmReturn)
        {
            Item items = cfg.itmMtFuncTimes;

            int count = items.getItemCount();

            AppendEmpty(cfg, "inn_type", itmReturn);

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string label = item.getProperty("in_purpose", "") + "-結果統計";
                string in_action = item.getProperty("in_action", "");
                string surveytype = GetSurveyType(in_action);

                item.setType("inn_type");
                item.setProperty("label", label);
                item.setProperty("value", surveytype);
                if (surveytype == cfg.surveytype)
                {
                    item.setProperty("selected", "selected");
                }
                else
                {
                    item.setProperty("selected", " ");
                }

                itmReturn.addRelationship(item);
            }
        }

        //問卷選單
        private void AppendSurveyMenu(TConfig cfg, Item itmReturn)
        {
            Item items = GetMtSurveyItems(cfg, cfg.surveytype, "");
            int count = items.getItemCount();
            AppendEmpty(cfg, "inn_survey", itmReturn);

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setType("inn_survey");
                item.setProperty("label", SurveyLabel(item));
                item.setProperty("value", item.getProperty("survey_id", ""));
                itmReturn.addRelationship(item);
            }
        }

        private string SurveyLabel(Item item)
        {
            string in_sort_order = item.getProperty("in_sort_order", "");
            string in_questions = item.getProperty("in_questions", "");
            string label = in_sort_order + ". " + in_questions;
            return label;
        }

        //取得選項時程清單
        private Item GetMtFuncItems(TConfig cfg, string surveytype = "")
        {
            string condition = surveytype == ""
                ? ""
                : " AND in_action = 'sheet" + surveytype + "'";

            string sql = "";

            sql = "SELECT in_action, in_purpose FROM IN_CLA_MEETING_FUNCTIONTIME WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_action like 'sheet%'"
                + " AND in_action <> 'sheet1'"
                + condition
                + " ORDER BY in_date_s";

            return cfg.inn.applySQL(sql);
        }

        //取得問項清單
        private Item GetMtSurveyItems(TConfig cfg, string surveytype, string surveyid)
        {
            string sql = "";

            string condition = surveyid == ""
                ? ""
                : "AND t2.id = '" + surveyid + "'";

            sql = @"
                SELECT
	                t1.in_sort_order
	                , t2.id AS 'survey_id'
	                , t2.item_number
	                , t2.in_question_type
	                , t2.in_questions
	                , t2.in_selectoption
	                , t2.in_max_length
	                , t2.in_answer
                FROM
	                IN_CLA_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN
	                IN_CLA_SURVEY t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_surveytype = '{#surveytype}'
	                AND t2.in_question_type = 'multi_value'
                    {#condition}
                ORDER BY
	                t1.sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#surveytype}", surveytype)
                .Replace("{#condition}", condition);

            return cfg.inn.applySQL(sql);
        }

        private void AppendEmpty(TConfig cfg, string in_type, Item itmReturn)
        {
            Item itmEmpty = cfg.inn.newItem();
            itmEmpty.setType(in_type);
            itmEmpty.setProperty("label", "請選擇");
            itmEmpty.setProperty("value", "請選擇");
            itmReturn.addRelationship(itmEmpty);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string surveytype { get; set; }
            public string surveyid { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }

            public Item itmMtFuncTimes { get; set; }
            public Item itmMtFuncTime { get; set; }
            public Dictionary<string, Item> MtFuncTimeMap { get; set; }

            public string mt_title { get; set; }
            public string[] CharSet { get; set; }
        }

        private Dictionary<string, Item> MapFuncTime(TConfig cfg, Item items)
        {
            Dictionary<string, Item> map = new Dictionary<string, Item>();
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string surveytype = GetSurveyType(item.getProperty("in_action", ""));
                if (!map.ContainsKey(surveytype))
                {
                    map.Add(surveytype, item);
                }
            }
            return map;
        }

        private string GetSurveyType(string in_action)
        {
            return in_action.Replace("sheet", "");
        }

        private DateTime GetDtmVal(string value, int hours = 8)
        {
            if (value == "") return DateTime.MinValue;

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value, out dt))
            {
                return dt.AddHours(hours);
            }
            else
            {
                return DateTime.MinValue;
            }
        }

        private int GetIntVal(string value)
        {
            if (value == "" || value == "0") return 0;
            int result = 0;
            int.TryParse(value, out result);
            return result;
        }
    }
}