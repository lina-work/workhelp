﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.年費
{
    internal class In_Payment_Resume_Fix : Item
    {
        public In_Payment_Resume_Fix(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 113.11.16理事會通過新會員名單
                日誌: 
                    - 2025-01-06: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Payment_Resume_Fix";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
            };

            AAA_VIP_MEMBER_20250106(cfg, itmR);

            return itmR;
        }

        private void AAA_VIP_MEMBER_20250106(TConfig cfg, Item itmReturn)
        {
            var sql = "SELECT * FROM AAA_VIP_MEMBER_20250106 ORDER BY in_sno";
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType("In_Resume");
                item.setProperty("inn_new_apply", "1");
                item.setProperty("inn_new_year", "2024");
                item.apply("In_Payment_Resume");
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string scene { get; set; }
        }

    }
}
