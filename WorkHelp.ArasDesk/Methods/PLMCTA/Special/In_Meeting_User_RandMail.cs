﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Special
{
    public class In_Meeting_User_RandMail : Item
    {
        public In_Meeting_User_RandMail(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: in_mail 變更為隨機六碼數字
                日誌: 
                    - 2022-02-17: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_User_RandMail";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                code = itmR.getProperty("code", ""),
                BadNoList = new List<int>(),
            };

            //cfg.meeting_id = "EE02243B882A42DDB08BFF0010517721";

            switch(cfg.code)
            {
                case "4":
                    Code4(cfg);
                    break;

                case "6":
                    Code6(cfg);
                    break;
            }

            return itmR;

        }

        private void Code4(TConfig cfg)
        {
            cfg.Min = 1001;
            cfg.Max = 9999;
            cfg.BadNoList.Add(1444);
            cfg.BadNoList.Add(2444);
            cfg.BadNoList.Add(3444);
            cfg.BadNoList.Add(4444);
            cfg.BadNoList.Add(5444);
            cfg.BadNoList.Add(6444);
            cfg.BadNoList.Add(7444);
            cfg.BadNoList.Add(8444);
            cfg.BadNoList.Add(9444);

            //in_mail 變更為隨機六碼數字
            UpdateMUserMail(cfg);

        }

        private void Code6(TConfig cfg)
        {

            cfg.Min = 100001;
            cfg.Max = 999999;
            cfg.BadNoList.Add(144444);
            cfg.BadNoList.Add(244444);
            cfg.BadNoList.Add(344444);
            cfg.BadNoList.Add(444444);
            cfg.BadNoList.Add(544444);
            cfg.BadNoList.Add(644444);
            cfg.BadNoList.Add(744444);
            cfg.BadNoList.Add(844444);
            cfg.BadNoList.Add(944444);

            //in_mail 變更為隨機六碼數字
            UpdateMUserMail(cfg);

            ////檢查重複
            //in_mail 不允許插入重複的索引鍵，因此不會發生帳號重複
            //CheckRepeat(cfg);
        }

        //in_mail 變更為隨機碼數字
        private void UpdateMUserMail(TConfig cfg)
        {
            string sql_upd1 = "UPDATE IN_CLA_MEETING_USER SET in_mail = 'X' + in_mail"
                + " WHERE source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql_upd1);

            string sql_qry = "SELECT id, in_sno, in_mail FROM IN_CLA_MEETING_USER WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'";
            Item items = cfg.inn.applySQL(sql_qry);

            int count = items.getItemCount();

            List<int> ns = GetRandNums(cfg, count);

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_sno = item.getProperty("in_sno", "");
                string in_mail = item.getProperty("in_mail", "");
                string new_mail = ns[i].ToString();

                string sql_upd2 = "UPDATE IN_CLA_MEETING_USER SET"
                    + " in_sno = '" + new_mail + "'"
                    + ", in_mail = '" + new_mail + "'"
                    + " WHERE id = '" + id + "'";

                Item itmUpd = cfg.inn.applySQL(sql_upd2);

                if (itmUpd.isError())
                {
                    throw new Exception("發生錯誤");
                }
            }
        }

        //檢查重複
        private void CheckRepeat(TConfig cfg)
        {
            string sql = "SELECT in_mail, COUNT(*) AS 'cnt'"
                + " FROM IN_CLA_MEETING_USER WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " GROUP BY in_mail"
                + " HAVING COUNT(*) > 1";

            List<string> errs = new List<string>();

            Item itmRepeats = cfg.inn.applySQL(sql);
            if (!itmRepeats.isError() && itmRepeats.getResult() != "")
            {
                int repeat_count = itmRepeats.getItemCount();
                for (int i = 0; i < repeat_count; i++)
                {
                    Item itmRepeat = itmRepeats.getItemByIndex(i);
                    string in_mail = itmRepeat.getProperty("in_mail", "");
                    string cnt = itmRepeat.getProperty("cnt", "0");
                    if (cnt != "0")
                    {
                        errs.Add(in_mail);
                    }
                }
            }

            if (errs.Count > 0)
            {
                throw new Exception("以下號碼重複: \r\n" + string.Join("\r\n", errs));
            }
        }

        private List<int> GetRandNums(TConfig cfg, int count)
        {
            List<int> list = new List<int>();

            for (int i = 0; i < count; i++)
            {
                int no = GetRandMail(cfg, list, cfg.Min, cfg.Max);
                list.Add(no);
            }

            return list;
        }

        private int GetRandMail(TConfig cfg, List<int> list, int min, int max)
        {
            int no = Next(min, max);

            while (IsNotOk(cfg, list, no))
            {
                no = Next(min, max);
            }

            return no;
        }

        //檢查連號或壞號碼
        private bool IsNotOk(TConfig cfg, List<int> list, int no)
        {
            bool result = false;

            int idx_s = no - 3;
            int idx_e = no + 3;

            for (int i = idx_s; i < idx_e; i++)
            {
                if (list.Contains(i))
                {
                    result = true;
                    break;
                }
            }

            if (!result && cfg.BadNoList.Contains(no))
            {
                result = true;
            }

            return result;
        }

        //避免在像 Random 極短時間內使用，會因亂數種子一樣而產生相同亂數組的問題。

        private System.Security.Cryptography.RNGCryptoServiceProvider rngp = new System.Security.Cryptography.RNGCryptoServiceProvider();
        private byte[] rb = new byte[4];

        /// <summary>
        /// 產生一個非負數且最大值 max 以下的亂數
        /// </summary>
        /// <param name="max">最大值</param>
        private int Next(int max)
        {
            rngp.GetBytes(rb);
            int value = BitConverter.ToInt32(rb, 0);
            value = value % (max + 1);
            if (value < 0) value = -value;
            return value;
        }

        /// <summary>
        /// 產生一個非負數且最小值在 min 以上最大值在 max 以下的亂數
        /// </summary>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        private int Next(int min, int max)
        {
            int value = Next(max - min) + min;
            return value;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string code { get; set; }

            public int Min { get; set; }
            public int Max { get; set; }
            public List<int> BadNoList { get; set; }
        }
    }
}