﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Degree
{
    public class In_Cla_Meeting_DegreeId : Item
    {
        public In_Cla_Meeting_DegreeId(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 證號管理
                日期:
                    - 2022-03-09 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Cla_Meeting_DegreeId";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("itemid", ""),
                keyword = itmR.getProperty("keyword", ""),
                in_qry = itmR.getProperty("in_qry", ""),
                scene = itmR.getProperty("scene", ""),

                doc_name = itmR.getProperty("doc_name", ""),
                font_size = 12,
                row_height = 24,
            };

            if (cfg.meeting_id == "")
            {
                cfg.meeting_id = itmR.getProperty("meeting_id", "");
            }

            if (cfg.meeting_id != "")
            {
                //活動資訊
                MeetingInfo(cfg);
            }

            switch (cfg.scene)
            {
                case "save_from_resume"://儲存卡片資料
                    SaveFromResume(cfg, itmR);
                    break;

                case "update"://更新段證資料
                    Update(cfg, itmR);
                    break;

                case "issue_update"://一鍵勾選/勾銷
                    UpdateIssue(cfg, itmR);
                    break;

                case "table_page":
                    TablePage(cfg, itmR);
                    itmR.setProperty("hide_go_tab", "item_show_1");
                    itmR.setProperty("hide_go_table", "item_show_0");
                    break;

                default:
                    TabPage(cfg, itmR);
                    itmR.setProperty("hide_go_tab", "item_show_0");
                    itmR.setProperty("hide_go_table", "item_show_1");
                    break;
            }

            return itmR;
        }

        private void MeetingInfo(TConfig cfg)
        {
            List<string> cols = new List<string>
            {
                "keyed_name",
                "in_title",
                "in_echelon",
                "in_annual",
                "in_seminar_type",
            };

            string sql = "SELECT " + string.Join(", ", cols) + " FROM IN_CLA_MEETING WITH(NOLOCK)"
                + " WHERE id = '" + cfg.meeting_id + "'";

            cfg.itmMeeting = cfg.inn.applySQL(sql);
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
        }

        private void SaveFromResume(TConfig cfg, Item itmReturn)
        {
            string resume_id = itmReturn.getProperty("resume_id", "");
            string in_card_degree_issue = itmReturn.getProperty("in_card_degree_issue", "");
            string in_card_member_issue = itmReturn.getProperty("in_card_member_issue", "");
            string in_card_member_id = itmReturn.getProperty("in_card_member_id", "");
            string in_tcon = itmReturn.getProperty("in_tcon", "");

            Item itmResume = cfg.inn.newItem("In_Resume", "merge");
            itmResume.setAttribute("where", "id = '" + resume_id + "'");
            itmResume.setProperty("in_card_degree_issue", in_card_degree_issue);
            itmResume.setProperty("in_card_member_issue", in_card_member_issue);
            itmResume.setProperty("in_card_member_id", in_card_member_id);
            itmResume.setProperty("in_tcon", in_tcon);

            Item itmUpd = itmResume.apply();
            if (itmUpd.isError())
            {
                throw new Exception("更新失敗");
            }
        }

        //一鍵勾選/勾銷
        private void UpdateIssue(TConfig cfg, Item itmReturn)
        {
            string committee_sno = itmReturn.getProperty("committee_sno", "");
            string property = itmReturn.getProperty("property", "");
            string value = itmReturn.getProperty("value", "");

            string sql = @"
                UPDATE t1 SET
                	t1.{#property} = '{#value}'
                FROM
                	IN_RESUME t1 WITH(NOLOCK)
                INNER JOIN
                	IN_CLA_MEETING_PROMOTION t2 WITH(NOLOCK) 
                	ON t2.in_sno = t1.in_sno
                WHERE
                	t2.source_id = '{#meeting_id}'
                	AND t2.in_committee_sno = '{#committee_sno}'
                	AND t2.in_score_state = '合格'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#committee_sno}", committee_sno)
                .Replace("{#property}", property)
                .Replace("{#value}", value);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmResult = cfg.inn.applySQL(sql);
            if (itmResult.isError())
            {
                throw new Exception("執行失敗");
            }
        }

        //更新段證資料
        private void Update(TConfig cfg, Item itmReturn)
        {
            string resume_id = itmReturn.getProperty("resume_id", "");
            string property = itmReturn.getProperty("property", "");
            string value = itmReturn.getProperty("value", "");

            string mt_annual = cfg.itmMeeting.getProperty("in_annual", "");
            string mt_date = itmReturn.getProperty("in_date", "");
            string promotion_day = GetPromotionDay(cfg, mt_annual, mt_date);

            if (resume_id == "")
            {
                throw new Exception("id 錯誤");
            }

            string in_degree_date = "";
            string in_gl_degree_date = "";

            switch (property)
            {
                case "in_card_degree_issue":
                case "in_card_member_issue":
                case "in_card_member_id":
                    break;

                case "in_degree_id":
                    in_degree_date = promotion_day;
                    break;

                case "in_gl_degree_id":
                    in_gl_degree_date = promotion_day;
                    break;

                case "in_degree_date":
                case "in_gl_degree_date":
                    if (value != "")
                    {
                        DateTime dt = DateTime.MinValue;
                        if (DateTime.TryParse(value, out dt))
                        {
                            value = dt.ToString("yyyy-MM-dd");
                        }
                        else
                        {
                            throw new Exception("日期格式錯誤");
                        }
                    }
                    break;

                default:
                    throw new Exception("該欄位不允許更新");
            }

            Item itmOld = cfg.inn.newItem("In_Resume", "merge");
            itmOld.setAttribute("where", "id = '" + resume_id + "'");

            itmOld.setProperty(property, value);

            if (in_degree_date != "")
            {
                itmOld.setProperty("in_degree_date", in_degree_date);
            }
            if (in_gl_degree_date != "")
            {
                itmOld.setProperty("in_gl_degree_date", in_gl_degree_date);
            }

            if (property == "in_degree_id")
            {
                itmOld.setProperty("in_card_member_id", value);
                // string old_card_member_id = itmOld.getProperty("in_card_member_id", "");
                // if (old_card_member_id == "")
                // {
                //     itmOld.setProperty("in_card_member_id", value);
                // }
            }

            Item itmResult = itmOld.apply();

            if (itmResult.isError())
            {
                throw new Exception("更新失敗");
            }

            if (in_degree_date != "")
            {
                itmReturn.setProperty("in_degree_date", in_degree_date);
            }
            if (in_gl_degree_date != "")
            {
                itmReturn.setProperty("in_gl_degree_date", in_gl_degree_date);
            }

            //更新段證資料
            UpdateMtPromotion(cfg, itmReturn);
        }

        //更新段證資料
        private void UpdateMtPromotion(TConfig cfg, Item itmReturn)
        {
            string rid = itmReturn.getProperty("resume_id", "");
            string pid = itmReturn.getProperty("pid", "");

            string sql = @"
                UPDATE t1 SET
                	t1.in_certificate_no = t2.in_degree_id
                FROM
                	IN_CLA_MEETING_PROMOTION t1 WITH(NOLOCK)
                INNER JOIN
                	IN_RESUME t2 
                	ON t2.id = '{#rid}'
                WHERE
                	t1.id = '{#pid}'
            ";

            sql = sql.Replace("{#rid}", rid)
                .Replace("{#pid}", pid);

            cfg.inn.applySQL(sql);
        }

        private string GetPromotionDay(TConfig cfg, string mt_annual, string mt_date)
        {
            string result = "";

            int west_year = GetIntVal(mt_annual) + 1911;

            switch (mt_date)
            {
                case "03-30":
                    result = west_year + "-03-30";
                    break;
                case "03-31":
                    result = west_year + "-03-31";
                    break;
                case "06-30":
                    result = west_year + "-06-30";
                    break;
                case "09-30":
                    result = west_year + "-09-30";
                    break;
                case "12-30":
                    result = west_year + "-12-30";
                    break;
                case "12-31":
                    result = west_year + "-12-31";
                    break;
                default:
                    result = System.DateTime.Now.ToString("yyyy-MM-dd");
                    break;

            }

            return result;
        }

        //頁面
        private void TablePage(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("keyed_name", cfg.itmMeeting.getProperty("keyed_name", ""));

            Item items = GetMeetingPromotions(cfg, order_by_birth: true);
            if (items.isError() || items.getResult() == "")
            {
                return;
            }

            string tab_contents = GetTable(cfg, items).ToString();
            itmReturn.setProperty("inn_tabs", tab_contents);
        }

        private StringBuilder GetTable(TConfig cfg, Item items)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("<tr>");
            //head.Append("  <th class='mailbox-subject text-center' data-width='3%'  >No.</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='9%'  >姓名</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='10%' data-sortable='true'>英文姓名</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='11%' data-sortable='true'>出生年月日</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='3%'  data-sortable='true'>晉升</th>");
            // head.Append("  <th class='mailbox-subject text-center' data-width='9%'  >國內證號</th>");
            // head.Append("  <th class='mailbox-subject text-center' data-width='10%' >發證日期</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='9%'  >國際證號</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='10%' >發證日期</th>");
            // head.Append("  <th class='mailbox-subject text-center' data-width='3%'  >晉段卡</th>");
            // head.Append("  <th class='mailbox-subject text-center' data-width='3%'  >會員卡</th>");
            // head.Append("  <th class='mailbox-subject text-center' data-width='3%'  >卡號</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='15%'  >所屬委員會</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='3%'  >下載</th>");
            head.Append("</tr>");
            head.Append("</thead>");


            body.Append("<tbody>");
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string no = (i + 1).ToString();
                string muid = item.getProperty("muid", "");
                string pid = item.getProperty("pid", "");
                string in_sno = item.getProperty("in_sno", "");
                string in_name = item.getProperty("in_name", "");
                string in_l1 = item.getProperty("in_l1", "");
                string resume_id = item.getProperty("resume_id", "");
                string in_makeup_yn = item.getProperty("in_makeup_yn", "");

                string name = "";
                if (in_makeup_yn == "1")
                {
                    name = "<a href='javascript:void()' onclick='ShowResume_Click(this)' >"
                        + in_name
                        + " (補)"
                        + "</a>";
                }
                else
                {
                    name = "<a href='javascript:void()' onclick='ShowResume_Click(this)' >"
                        + in_name
                        + "</a>"
                        + " <a href='javascript:void()' onclick='ShowVerify_Click(this)' > <i class='fa fa-check-circle-o'><i/> </a>";
                }


                body.Append("<tr class='show_all'"
                    + " data-no='" + no + "'"
                    + " data-muid='" + muid + "'"
                    + " data-pid='" + pid + "'"
                    + " data-rid='" + resume_id + "'"
                    + " data-rname='" + in_name + "'"
                    + " data-rsno='" + in_sno + "'"
                    + ">");

                //body.Append("  <td class='text-center'> " + no + " </td>");
                body.Append("  <td class='text-center'> " + name + " </td>");
                body.Append("  <td class='text-left'> " + item.getProperty("in_en_name", "") + " </td>");
                body.Append("  <td class='text-center'> " + WestDay(item.getProperty("in_birth", "")) + " </td>");
                body.Append("  <td class='text-center'> " + item.getProperty("in_l1", "") + " </td>");

                // body.Append("  <td class='text-center'> " + InputText(cfg, item, "in_degree_id", "國內證號") + " </td>");
                // body.Append("  <td class='text-center'> " + InputDay(cfg, item, "in_degree_date", "yyyy/MM/dd") + " </td>");
                body.Append("  <td class='text-center'> " + InputText(cfg, item, "in_gl_degree_id", "國際證號") + " </td>");
                body.Append("  <td class='text-center'> " + InputDay(cfg, item, "in_gl_degree_date", "yyyy/MM/dd") + " </td>");

                // body.Append("  <td class='text-center'> " + CheckBox(cfg, item, "in_card_degree_issue") + " </td>");
                // body.Append("  <td class='text-center'> " + CheckBox(cfg, item, "in_card_member_issue") + " </td>");
                // body.Append("  <td class='text-center'> " + InputText(cfg, item, "in_card_member_id", "會員卡號") + " </td>");

                body.Append("  <td class='text-left'> " + item.getProperty("cmt_name", "") + " </td>");
                body.Append("  <td class='text-center'> " + CertDownload(cfg, item) + " </td>");

                body.Append("</tr>");
            }
            body.Append("</tbody>");

            string table_name = "nation_table";

            StringBuilder builder = new StringBuilder();
            builder.Append("<div class='box-body tablenobg' style='position: relative'>");
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.Append("</div>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            return builder;
        }

        //頁面
        private void TabPage(TConfig cfg, Item itmReturn)
        {
            Item items = GetMeetingPromotions(cfg);
            if (items.isError() || items.getResult() == "")
            {
                return;
            }

            List<TCommittee> list = MapCommittees(cfg, items);

            StringBuilder tabs = new StringBuilder();
            StringBuilder body = new StringBuilder();

            for (int i = 0; i < list.Count; i++)
            {
                string active = i == 0 ? "active" : "";
                string active2 = i == 0 ? "active in" : "";

                var cmt = list[i];
                tabs.Append("<li id='" + cmt.ctrl_li_id + "' class='nav-item " + active + "'>");
                tabs.Append("  <a id='" + cmt.ctrl_a_id + "'");
                tabs.Append("    href='#" + cmt.ctrl_pn_id + "'");
                tabs.Append("   class='nav-link inn_city' role='tab' data-toggle='pill'");
                tabs.Append("    aria-controls='pills-home' aria-selected='true' onclick='StoreTab_Click(this)'"
                    + " data-sno='" + cmt.in_sno + "'"
                    + " data-city='" + cmt.city + "'"
                    + ">");
                tabs.Append("    " + cmt.city + "(" + cmt.items.Count + ")");
                tabs.Append("  </a>");
                tabs.Append("</li>");

                body.Append("<div class='tab-pane fade " + active2 + "' id='" + cmt.ctrl_pn_id + "' role='tabpanel' >");
                body.Append("  <div class='container bg-white-hw'>");

                body.Append(GetCmtTable(cfg, cmt));

                body.Append("  </div>");
                body.Append("</div>");
            }

            StringBuilder builder = new StringBuilder();
            builder.Append("<ul class='nav nav-pills' style='justify-content: start ;' id='pills-tab' role='tablist'>");
            builder.Append(tabs);
            builder.Append("</ul>");

            builder.Append("<div class='tab-content' id='pills-tabContent'>");
            builder.Append(body);
            builder.Append("</div>");

            string tab_contents = builder.ToString();
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "contents: " + tab_contents);

            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("keyed_name", cfg.itmMeeting.getProperty("keyed_name", ""));
            itmReturn.setProperty("inn_tabs", tab_contents);
        }

        private StringBuilder GetCmtTable(TConfig cfg, TCommittee cmt)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("<tr>");
            head.Append("  <th class='mailbox-subject text-center' data-width='3%'  >No.</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='8%'  >姓名</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='12%' >英文姓名</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='11%' >出生年月日</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='3%'  >晉升</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='9%'  >國內證號</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='10%' >發證日期</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='9%'  >國際證號</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='10%' >發證日期</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='3%'  >" + HeadCheckBox(cfg, "晉段卡", "in_card_degree_issue") + "</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='3%'  >" + HeadCheckBox(cfg, "會員卡", "in_card_member_issue") + "</th>");
            // head.Append("  <th class='mailbox-subject text-center' data-width='3%'  >卡號</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='3%'  >下載</th>");
            head.Append("</tr>");
            head.Append("</thead>");

            string last_l1 = cmt.items[0].getProperty("in_l1", "");

            body.Append("<tbody>");
            int count = cmt.items.Count;
            for (int i = 0; i < count; i++)
            {
                Item item = cmt.items[i];
                string no = (i + 1).ToString();
                string muid = item.getProperty("muid", "");
                string pid = item.getProperty("pid", "");
                string in_sno = item.getProperty("in_sno", "");
                string in_name = item.getProperty("in_name", "");
                string in_l1 = item.getProperty("in_l1", "");
                string resume_id = item.getProperty("resume_id", "");
                string in_makeup_yn = item.getProperty("in_makeup_yn", "");

                string name = "";
                if (in_makeup_yn == "1")
                {
                    name = "<a href='javascript:void()' onclick='ShowResume_Click(this)' >"
                        + in_name
                        + " (補)"
                        + "</a>";
                }
                else
                {
                    name = "<a href='javascript:void()' onclick='ShowResume_Click(this)' >"
                        + in_name
                        + "</a>"
                        + " <a href='javascript:void()' onclick='ShowVerify_Click(this)' > <i class='fa fa-check-circle-o'><i/> </a>";
                }

                if (in_l1 != last_l1)
                {
                    body.Append("<tr>");
                    body.Append("  <td class='mailbox-subject text-center '>No.</td>");
                    body.Append("  <td class='mailbox-subject text-center '>姓名</td>");
                    body.Append("  <td class='mailbox-subject text-center '>英文姓名</td>");
                    body.Append("  <td class='mailbox-subject text-center '>出生年月日</td>");
                    body.Append("  <td class='mailbox-subject text-center '>晉段</td>");
                    body.Append("  <td class='mailbox-subject text-center '>國內證號</td>");
                    body.Append("  <td class='mailbox-subject text-center '>發證日期</td>");
                    body.Append("  <td class='mailbox-subject text-center '>國際證號</td>");
                    body.Append("  <td class='mailbox-subject text-center '>發證日期</td>");
                    body.Append("  <td class='mailbox-subject text-center' >" + HeadCheckBox(cfg, "晉段卡", "in_card_degree_issue") + "</td>");
                    body.Append("  <td class='mailbox-subject text-center' >" + HeadCheckBox(cfg, "會員卡", "in_card_member_issue") + "</td>");
                    body.Append("  <td class='mailbox-subject text-center' >卡號</td>");
                    body.Append("  <td class='mailbox-subject text-center '>下載</td>");
                    body.Append("</tr>");

                    last_l1 = in_l1;

                }

                body.Append("<tr class='show_all'"
                    + " id='" + cmt.in_sno + "_" + no + "'"
                    + " data-no='" + no + "'"
                    + " data-muid='" + muid + "'"
                    + " data-pid='" + pid + "'"
                    + " data-rid='" + resume_id + "'"
                    + " data-rname='" + in_name + "'"
                    + " data-rsno='" + in_sno + "'"
                    + ">");

                body.Append("  <td class='text-center'> " + no + " </td>");
                body.Append("  <td class='text-center'> " + name + " </td>");
                body.Append("  <td class='text-left'> " + item.getProperty("in_en_name", "") + " </td>");
                body.Append("  <td class='text-center'> " + WestDay(item.getProperty("in_birth", "")) + " </td>");
                body.Append("  <td class='text-center'> " + item.getProperty("in_l1", "") + " </td>");

                body.Append("  <td class='text-center'> " + InputText(cfg, item, "in_degree_id", "國內證號") + " </td>");
                body.Append("  <td class='text-center'> " + InputDay(cfg, item, "in_degree_date", "yyyy/MM/dd") + " </td>");
                body.Append("  <td class='text-center'> " + InputText(cfg, item, "in_gl_degree_id", "國際證號") + " </td>");
                body.Append("  <td class='text-center'> " + InputDay(cfg, item, "in_gl_degree_date", "yyyy/MM/dd") + " </td>");

                body.Append("  <td class='text-center'> " + CheckBox(cfg, item, "in_card_degree_issue") + " </td>");
                body.Append("  <td class='text-center'> " + CheckBox(cfg, item, "in_card_member_issue") + " </td>");
                // body.Append("  <td class='text-center'> " + InputText(cfg, item, "in_card_member_id", "會員卡號") + " </td>");

                body.Append("  <td class='text-center'> " + CertDownload(cfg, item) + " </td>");

                body.Append("</tr>");
            }
            body.Append("</tbody>");

            string table_name = cmt.ctrl_tbl_id;

            StringBuilder builder = new StringBuilder();
            builder.Append("<div class='box-body tablenobg' style='position: relative'>");
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.Append("</div>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            return builder;
        }

        private string HeadCheckBox(TConfig cfg, string title, string property)
        {
            return "<div class=''> <label> <input type='checkbox' class='checkbox inn_chk' onclick='AllIssueCheck_Click(this)'"
                + " data-title='" + title + "'"
                + " data-prop='" + property + "'"
                + " autocomplete='off'> "
                + title
                + " </label> </div>";
        }

        private string CertDownload(TConfig cfg, Item item)
        {
            return " <a href='#' onclick='CertDownload_Click(this)' > <i class='fa fa-cloud-download'><i/> </a>";
        }

        private string CheckBox(TConfig cfg, Item item, string prop)
        {
            var value = item.getProperty(prop, "");
            var chked = value == "1" ? "checked" : "";

            return "<div class='form-control'>"
                + " <input type='checkbox' class='checkbox inn_chk'"
                + " onclick='IssueCheck_Click(this)'"
                + " data-prop='" + prop + "'"
                + " " + chked
                + " > </div>";
        }

        private string InputDay(TConfig cfg, Item item, string prop, string title)
        {
            var dt = GetDtmVal(item.getProperty(prop, ""));
            var value = "";
            if (dt != DateTime.MinValue)
            {
                value = dt.ToString("yyyy/MM/dd");
            }

            return "<input type='text' class='form-control inn_ctrl inn_date' maxlength='10'"
                + " onkeyup='cacheElement(this)'"
                + " onfocus='cacheElement(this)'"
                + " data-prop='" + prop + "'"
                + " value='" + value + "'"
                + " placeholder='" + title + "'"
                + " />";
        }

        private string InputText(TConfig cfg, Item item, string prop, string title)
        {
            string value = item.getProperty(prop, "");

            return "<input type='text' class='form-control inn_ctrl inn_text' maxlength='10'"
                + " onkeyup='cacheElement(this)'"
                + " onfocus='cacheElement(this)'"
                + " data-prop='" + prop + "'"
                + " value='" + value + "'"
                + " placeholder='" + title + "'"
                + " />";
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "'"
                + " class='table table-hover table-striped'"
                + " data-toggle='table'"
                + " data-show-toggle='true'"
                + " data-show-columns='true'"
                + " data-sort-order='asc'"
                + " data-search-align='left'"
                + " data-sort-stable='true'"
                + " data-search='true'"
                + ">";
        }

        private Item GetMeetingPromotions(TConfig cfg, bool order_by_birth = false)
        {
            string order_by = order_by_birth
                ? "t1.in_birth"
                : "t1.in_committee_sno, t12.value, t1.in_birth";

            // string condition = cfg.keyword == ""
            //     ? ""
            //     : "AND ( (t1.in_name like N'%" + cfg.keyword + "%') OR (t1.in_sno like N'%" + cfg.keyword + "%') )";

            string sql = @"
                SELECT
                    t1.id AS 'pid'
                    , t1.in_name
                    , t1.in_en_name
                    , t1.in_sno
                    , t1.in_gender
                    , DATEADD(hour, 8, t1.in_birth) AS 'in_birth'
                    , t1.in_l1
                    , t12.value    AS 'in_l1_value'
                    , t1.in_makeup_yn
                    , t1.in_makeup_note
                    , t2.id AS 'muid'
                    , t3.id    AS 'resume_id'
                    , t3.in_degree
                    , t3.in_degree_id
                    , DATEADD(hour, 8, t3.in_degree_date) AS 'in_degree_date'
                    , t3.in_gl_degree
                    , t3.in_gl_degree_id
                    , DATEADD(hour, 8, t3.in_gl_degree_date) AS 'in_gl_degree_date'
                    , t1.in_committee_sno        AS 'cmt_sno'
                    , t11.in_name                AS 'cmt_name'
                    , t11.in_short_org            AS 'cmt_short_name'
                    , LEFT(t11.in_short_org, 3)    AS 'cmt_city'
                    , t3.in_card_degree_issue
                    , t3.in_card_member_issue
                    , t3.in_card_member_id
                FROM
                    IN_CLA_MEETING_PROMOTION t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_CLA_MEETING_USER t2 WITH(NOLOCK)
                    ON t2.id = t1.in_user
                INNER JOIN
                    IN_RESUME t3 WITH(NOLOCK)
                    ON t3.in_sno = t1.in_sno
                INNER JOIN
                    IN_RESUME t11 WITH(NOLOCK)
                    ON t11.in_sno = t1.in_committee_sno
                INNER JOIN
                    VU_DEGREE t12 
                    ON t12.label = t1.in_l1
                WHERE
                    t1.source_id = '{#meeting_id}'
                    AND t1.in_score_state = N'合格'
                ORDER BY
                    {#order_by}
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#order_by}", order_by);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private List<TCommittee> MapCommittees(TConfig cfg, Item items)
        {
            List<TCommittee> result = new List<TCommittee>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                string in_committee = item.getProperty("cmt_name", "");
                string in_committee_sno = item.getProperty("cmt_sno", "");
                string in_committee_short = item.getProperty("cmt_short_name", "");
                string city = item.getProperty("cmt_city", "");

                TCommittee cmt = result.Find(x => x.in_sno == in_committee_sno);

                if (cmt == null)
                {
                    cmt = new TCommittee
                    {
                        in_name = in_committee,
                        in_sno = in_committee_sno,
                        in_short_name = in_committee_short,
                        city = city,
                        items = new List<Item>(),
                    };

                    cmt.ctrl_li_id = "cmt_li_" + cmt.in_sno;
                    cmt.ctrl_pn_id = "cmt_pn_" + cmt.in_sno;
                    cmt.ctrl_a_id = "cmt_a_" + cmt.in_sno;
                    cmt.ctrl_tbl_id = "cmt_tbl_" + cmt.in_sno;

                    result.Add(cmt);
                }

                cmt.items.Add(item);
            }

            return result.OrderBy(x => x.in_sno).ToList();
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string keyword { get; set; }
            public string in_qry { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }

            public string mt_title { get; set; }
            public string doc_name { get; set; }
            public string ext_type { get; set; }
            public string[] CharSet { get; set; }
            public int font_size { get; set; }
            public int row_height { get; set; }
        }

        private class TCommittee
        {
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_short_name { get; set; }
            public string city { get; set; }

            public List<Item> items { get; set; }

            public string ctrl_li_id { get; set; }
            public string ctrl_pn_id { get; set; }
            public string ctrl_a_id { get; set; }
            public string ctrl_tbl_id { get; set; }
        }

        private class TCard
        {
            public string resume_id { get; set; }
            public string in_type { get; set; }
            public string in_issue { get; set; }
            public string in_date { get; set; }
            public string in_number { get; set; }
        }


        private string DegreeVal(string value)
        {
            switch (value)
            {
                case "1000": return "1";
                case "2000": return "2";
                case "3000": return "3";
                case "4000": return "4";
                case "5000": return "5";
                case "6000": return "6";
                case "7000": return "7";
                case "8000": return "8";
                case "9000": return "9";
                default: return "";
            }
        }

        private string TwDay(string value)
        {
            DateTime dt = GetDtmVal(value);
            if (dt == DateTime.MinValue) return "";

            string y = (dt.Year - 1911).ToString();
            string m = dt.Month.ToString();
            string d = dt.Day.ToString();

            return "中華民國" + y + "年" + m + "月" + d + "日";
        }

        private string WestDay(string value)
        {
            DateTime dt = GetDtmVal(value);
            if (dt == DateTime.MinValue) return "";

            return dt.ToString("yyyy年MM月dd日");
        }

        private DateTime GetDtmVal(string value)
        {
            if (value == "") return DateTime.MinValue;
            DateTime dt = DateTime.MinValue;
            DateTime.TryParse(value, out dt);
            return dt;
        }

        private int GetIntVal(string value)
        {
            if (value == "") return 0;
            int result = 0;
            int.TryParse(value, out result);
            return result;
        }
    }
}