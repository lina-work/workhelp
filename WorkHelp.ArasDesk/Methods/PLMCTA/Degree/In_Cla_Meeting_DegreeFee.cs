﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_Cla_Meeting_DegreeFee : Item
    {
        public In_Cla_Meeting_DegreeFee(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 晉段費用計算
    日誌: 
        - 2022-11-25: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Cla_Meeting_DegreeFee";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                cmt_sno = itmR.getProperty("cmt_sno", "").ToUpper(),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "run":
                    Run(cfg, itmR);
                    break;

                case "detail_page":
                    DetailPage(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void DetailPage(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            //取得登入者資訊
            sql = "SELECT id, in_name, in_sno, in_is_admin FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = '" + cfg.strUserId + "'";
            Item itmResume = cfg.inn.applySQL(sql);

            //取得活動資訊
            sql = "SELECT id, in_title FROM IN_CLA_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            Item itmMeeting = cfg.inn.applySQL(sql);

            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));

            bool can_view = false;
            string in_sno = itmResume.getProperty("in_sno", "").ToUpper();
            string in_is_admin = itmResume.getProperty("in_is_admin", "");
            if (in_is_admin == "1" || in_sno == cfg.cmt_sno)
            {
                can_view = true;
            }

            if (!can_view)
            {
                //結束流程
                return;
            }

            var items = GetDetailItems(cfg);
            var rows = MapGroupRow(cfg, items);
            itmReturn.setProperty("inn_tables", GetDetailTable(cfg, rows));
        }

        private string GetDetailTable(TConfig cfg, List<TRow> rows)
        {
            var head = new StringBuilder();
            var body = new StringBuilder();

            head.Append("<tr>");
            head.Append("  <th class='mailbox-subject' data-sortable='true'>項次</th>");
            head.Append("  <th class='mailbox-subject' data-sortable='true'>姓名</th>");
            head.Append("  <th class='mailbox-subject' data-sortable='true'>身分證號</th>");
            head.Append("  <th class='mailbox-subject' data-sortable='true'>所屬單位</th>");

            head.Append("  <th class='mailbox-subject' data-sortable='true'>申請<BR>段位</th>");
            head.Append("  <th class='mailbox-subject' data-sortable='true'>是否申請<BR>國際段</th>");
            head.Append("  <th class='mailbox-subject' data-sortable='true'>報名日期</th>");
            head.Append("  <th class='mailbox-subject' data-sortable='true'>最後<BR>晉升日期</th>");

            head.Append("  <th class='mailbox-subject' data-sortable='true'>國內段<BR>費用</th>");
            head.Append("  <th class='mailbox-subject' data-sortable='true'>國際段<BR>費用</th>");
            head.Append("  <th class='mailbox-subject' data-sortable='true'>年費</th>");
            head.Append("  <th class='mailbox-subject' data-sortable='true'>年數</th>");
            head.Append("  <th class='mailbox-subject' data-sortable='true'>費用<BR>合計</th>");

            head.Append("</tr>");

            int no = 0;
            for (int i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                var end_idx = row.children.Count - 1;
                for (int j = 0; j < row.children.Count; j++)
                {
                    no++;
                    var child = row.children[j];
                    var item = child.item;

                    body.Append("<tr>");
                    body.Append("  <td class='text-center' data-label='項次'>" + no + "</td>");
                    body.Append("  <td class='text-left' data-label='姓名'>" + PInfo(item, "in_name") + "</td>");
                    body.Append("  <td class='text-left' data-label='身分證號'>" + PInfo(item, "in_sno_display") + "</td>");
                    body.Append("  <td class='text-left' data-label='所屬單位'>" + CreatorInfo(cfg, item) + "</td>");

                    body.Append("  <td class='text-center' data-label='申請段位'>" + PInfo(item, "in_l1") + "</td>");
                    body.Append("  <td class='text-center' data-label='是否申請國際段'>" + GlobalDegree(item) + "</td>");
                    body.Append("  <td class='text-center' data-label='報名日期'>" + PInfo(item, "in_regdate") + "</td>");
                    body.Append("  <td class='text-center' data-label='最後晉段日期'>" + PInfo(item, "in_regdate") + "</td>");

                    body.Append("  <td class='text-right' data-label='國內段費用'>" + GetDcmStr(child.tw_amount) + "</td>");
                    body.Append("  <td class='text-right' data-label='國際段費用'>" + GetDcmStr(child.gl_amount) + "</td>");
                    body.Append("  <td class='text-right' data-label='年費'>" + GetDcmStr(child.yr_amount) + "</td>");
                    body.Append("  <td class='text-right' data-label='年數'>" + PInfo(item, "in_years") + "</td>");
                    body.Append("  <td class='text-right' data-label='費用合計'>" + GetDcmStr(child.total) + "</td>");
                    body.Append("</tr>");

                    if (j == end_idx)
                    {
                        body.Append("<tr class='group_row'>");
                        body.Append("  <td class='text-center' data-label='項次'> - </td>");
                        body.Append("  <td class='text-left' data-label='姓名'> </td>");
                        body.Append("  <td class='text-left' data-label='身分證號'> </td>");
                        body.Append("  <td class='text-left' data-label='所屬單位'>" + CreatorInfo(cfg, item) + "</td>");

                        body.Append("  <td class='text-center' data-label='申請段位'> </td>");
                        body.Append("  <td class='text-center' data-label='是否申請國際段'> </td>");
                        body.Append("  <td class='text-center' data-label='報名日期'> </td>");
                        body.Append("  <td class='text-center' data-label='最後晉段日期'> </td>");

                        body.Append("  <td class='text-right' data-label='國內段費用'> </td>");
                        body.Append("  <td class='text-right' data-label='國際段費用'> </td>");
                        body.Append("  <td class='text-right' data-label='年費'> </td>");
                        body.Append("  <td class='text-right' data-label='年數'> </td>");
                        body.Append("  <td class='text-right' data-label='費用合計'>" + GetDcmStr(row.total) + "</td>");
                        body.Append("</tr>");
                    }
                }
            }

            var table_name = "data_table";
            var builder = new StringBuilder();
            builder.Append(GetTableAttribute(table_name));
            builder.Append("  <thead>");
            builder.Append(head);
            builder.Append("  </thead>");
            builder.Append("  <tbody>");
            builder.Append(body);
            builder.Append("  </tbody>");
            builder.Append("</table>");
            return builder.ToString();
        }

        private List<TRow> MapGroupRow(TConfig cfg, Item items)
        {
            var result = new List<TRow>();
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var sno = item.getProperty("in_creator_sno", "");
                var row = result.Find(x => x.sno == sno);
                if (row == null)
                {
                    row = new TRow
                    {
                        sno = sno,
                        name = item.getProperty("in_creator", ""),
                        children = new List<TChild> (),
                        total = 0,
                    };
                    result.Add(row);
                }

                var child = new TChild
                {
                    item = item,
                    tw_amount = GetDcm(item.getProperty("in_tw_amount", "0")),
                    gl_amount = GetDcm(item.getProperty("in_gl_amount", "0")),
                    yr_amount = GetDcm(item.getProperty("in_yr_amount", "0")),
                    yr_subtotal = GetDcm(item.getProperty("in_year_subtotal", "0")),
                    total = GetDcm(item.getProperty("in_total", "0")),
                };

                row.children.Add(child);
                row.total += child.total;
            }
            return result;
        }

        private string GlobalDegree(Item item)
        {
            var in_is_gl_degree = item.getProperty("in_is_gl_degree", "");
            if (in_is_gl_degree == "是") return "是";
            return "<span style='color: red'>否</span>";
        }

        private string PInfo(Item item, string prop)
        {
            return item.getProperty(prop, "");
        }

        private string CreatorInfo(TConfig cfg, Item item)
        {
            return item.getProperty("in_creator_sno", "")
                + " " + item.getProperty("in_creator", "");
        }

        private Item GetDetailItems(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t1.id AS 'muid'
	                , t1.in_creator_sno
	                , t1.in_creator
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_sno_display
	                , t2.in_l1
	                , t2.in_degree
	                , CONVERT(VARCHAR, t2.in_regdate, 111)     AS 'in_regdate'
	                , CONVERT(VARCHAR, t2.in_degree_date, 111) AS 'in_degree_date'
	                , t2.in_is_gl_degree
	                , t2.in_tw_amount
	                , t2.in_gl_amount
	                , t2.in_yr_amount
	                , t2.in_years
	                , t2.in_year_subtotal
	                , t2.in_total
	                , t2.in_sys_note
                FROM
	                IN_CLA_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
	                IN_CLA_MEETING_USER_DEGREE_FEE t2 WITH(NOLOCK)
	                ON t2.in_muid = t1.id

                WHERE
	                t1.source_id = '{#meeting_id}'
                    AND t2.in_cmt_sno = '{#cmt_sno}'
                ORDER BY
	                t1.in_creator_sno
	                , t1.in_degree
	                , t1.in_birth DESC
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#cmt_sno}", cfg.cmt_sno);

            return cfg.inn.applySQL(sql);
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "' class='table table-hover table-bordered table-rwd rwd rwdtable' "
                + " data-toggle='table' "
                + " data-click-to-select='true' "
                + " data-striped='false' "
                + " data-search='true' "
                // + " data-pagination='true' "
                // + " data-show-pagination-switch='false'"
                + ">";
        }

        private void Run(TConfig cfg, Item itmReturn)
        {
            RemoveOldDegreeFee(cfg);

            var itmFees = GetFeeItems(cfg);
            var fee_list = MapDegreeFeeList(cfg, itmFees);
            var items = GetMUserItems(cfg);

            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_muid = item.getProperty("in_muid", "");
                var in_degree = item.getProperty("in_degree", "");
                var in_regdate = item.getProperty("in_regdate", "");
                var in_degree_date = item.getProperty("in_degree_date", "");
                var in_date = item.getProperty("in_date", "");

                if (in_degree_date == "")
                {
                    in_degree_date = in_date;
                    item.setProperty("in_degree_date", in_date);
                }

                var fee_model = GetMUserAmountModel(cfg, item, fee_list, in_degree);

                item.setType("In_Cla_Meeting_User_Degree_Fee");
                item.setAttribute("where", "in_muid = '" + in_muid + "'");
                item.setProperty("in_regdate", GetDtmStr(in_regdate, "yyyy-MM-ddTHH:mm:ss"));
                item.setProperty("in_degree_date", GetDtmStr(in_degree_date, "yyyy-MM-ddTHH:mm:ss"));
                item.setProperty("in_tw_amount", fee_model.tw_amount.ToString());
                item.setProperty("in_gl_amount", fee_model.gl_amount.ToString());
                item.setProperty("in_yr_amount", fee_model.yr_amount.ToString());
                item.setProperty("in_years", fee_model.years.ToString());
                item.setProperty("in_year_subtotal", fee_model.yr_total.ToString());
                item.setProperty("in_total", fee_model.subtotal.ToString());
                item.setProperty("in_sys_note", fee_model.message);
                item.apply("merge");
            }
        }

        private TMUserAmount GetMUserAmountModel(TConfig cfg, Item item, List<TDegreeFee> fee_list, string in_degree)
        {
            var fee_model = default(TMUserAmount);

            var no = in_degree.TrimEnd('0');

            switch (in_degree)
            {
                case "1000":
                    fee_model = GetDegreeLv1Amount(cfg, item, fee_list, no);
                    break;

                case "2000":
                case "3000":
                case "4000":
                case "5000":
                case "6000":
                case "7000":
                    fee_model = GetDegreeLv27Amount(cfg, item, fee_list, no);
                    break;

                case "8000":
                case "9000":
                    fee_model = GetDegreeLv89Amount(cfg, item, fee_list, no);
                    break;
            }

            if (fee_model == null)
            {
                throw new Exception("算法異常");
            }

            return fee_model;
        }

        private TMUserAmount GetDegreeLv1Amount(TConfig cfg, Item item, List<TDegreeFee> fee_list, string no)
        {
            var fee_row = fee_list.Find(x => x.no == no);
            if (fee_row == null)
            {
                throw new Exception("查無該段費用參數: " + no + " 段");
            }

            var result = EmptyMUserAmount(cfg);

            result.years = 1;
            result.tw_amount = fee_row.tw_amount;
            result.gl_amount = fee_row.gl_amount;
            result.yr_amount = fee_row.yr_amount;
            result.yr_total = fee_row.yr_amount * result.years;

            //一段強制申請國際段 (國內段費用 + 國際段費用 + 年費合計)
            result.subtotal = fee_row.tw_amount + result.gl_amount + fee_row.yr_amount * result.years;

            return result;
        }

        private TMUserAmount GetDegreeLv27Amount(TConfig cfg, Item item, List<TDegreeFee> fee_list, string no)
        {
            var fee_row = fee_list.Find(x => x.no == no);
            if (fee_row == null)
            {
                throw new Exception("查無該段費用參數: " + no + " 段");
            }

            var in_regdate = item.getProperty("in_regdate", "");
            var in_degree_date = item.getProperty("in_degree_date", "");
            var in_is_gl_degree = item.getProperty("in_is_gl_degree", "");

            var result = EmptyMUserAmount(cfg);

            if (in_regdate == "")
            {
                result.is_error = true;
                result.message = "無報名日期";
                return result;
            }

            if (in_degree_date == "")
            {
                result.is_error = true;
                result.message = "無晉升日期";
                return result;
            }

            var dtE = GetDtm(in_regdate);
            var dtS = GetDtm(in_degree_date);

            result.years = dtE.Year - dtS.Year;
            result.tw_amount = fee_row.tw_amount;
            result.gl_amount = fee_row.gl_amount;
            result.yr_amount = fee_row.yr_amount;
            result.yr_total = fee_row.yr_amount * result.years;

            if (in_is_gl_degree != "是")
            {
                fee_row.gl_amount = 0;
            }

            //二段~七段不強制申請國際段 (國內段費用 + 國際段費用 + 年費合計)
            result.subtotal = fee_row.tw_amount + fee_row.gl_amount + fee_row.yr_amount * result.years;

            if (result.years <= 0)
            {
                result.is_error = true;
                result.message = "年數異常";
            }

            return result;
        }

        private TMUserAmount GetDegreeLv89Amount(TConfig cfg, Item item, List<TDegreeFee> fee_list, string no)
        {
            return EmptyMUserAmount(cfg);
        }

        private TMUserAmount EmptyMUserAmount(TConfig cfg)
        {
            return new TMUserAmount
            {
                is_error = false,
                message = "",
                tw_amount = 0,
                gl_amount = 0,
                yr_amount = 0,
                yr_total = 0,
                subtotal = 0,
                years = 0,
            };
        }

        private class TMUserAmount
        {
            public bool is_error { get; set; }
            public string message { get; set; }
            public decimal tw_amount { get; set; }
            public decimal gl_amount { get; set; }
            public decimal yr_amount { get; set; }
            public decimal yr_total { get; set; }
            public decimal subtotal { get; set; }
            public int years { get; set; }
        }

        private List<TDegreeFee> MapDegreeFeeList(TConfig cfg, Item items)
        {
            var result = new List<TDegreeFee>();
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_name = item.getProperty("in_name", "");
                var in_value = item.getProperty("in_value", "");

                var arr = in_name.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                if (arr == null || arr.Length != 3)
                {
                    throw new Exception("晉段費用表 參數異常");
                }

                var no = arr[0].TrimStart('0');
                var key = arr[1].ToUpper();

                if (no == "207")
                {
                    for (int j = 2; j <= 7; j++)
                    {
                        var n = j.ToString();
                        var r = AddAndGetDegreeFeeRow(cfg, result, n);
                        SetDegreeFee(cfg, r, key, in_value);
                    }
                }
                else
                {
                    var row = AddAndGetDegreeFeeRow(cfg, result, no);
                    SetDegreeFee(cfg, row, key, in_value);
                }
            }
            return result;
        }

        private void SetDegreeFee(TConfig cfg, TDegreeFee row, string key, string in_value)
        {
            switch (key)
            {
                case "TW":
                    row.tw_amount = GetDcm(in_value);
                    break;

                case "GL":
                    row.gl_amount = GetDcm(in_value);
                    break;

                case "YR":
                    row.yr_amount = GetDcm(in_value);
                    break;
            }
        }

        private TDegreeFee AddAndGetDegreeFeeRow(TConfig cfg, List<TDegreeFee> rows, string no)
        {
            var row = rows.Find(x => x.no == no);

            if (row == null)
            {
                row = new TDegreeFee
                {
                    no = no,
                    tw_amount = 0,
                    gl_amount = 0,
                    yr_amount = 0,
                };
                rows.Add(row);
            }

            return row;
        }

        private Item GetFeeItems(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t2.in_name
	                , t2.in_value
	                , t2.in_description
                FROM
	                IN_VARIABLE t1 WITH(NOLOCK)
                INNER JOIN
	                IN_VARIABLE_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t1.in_name = 'degree_fee'
                ORDER BY
	                t2.sort_order
            ";

            return cfg.inn.applySQL(sql);
        }

        private Item GetMUserItems(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.source_id     AS 'in_cla_meeting'
	                , t1.id           AS 'in_muid'
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_l1
	                , t1.in_degree
	                , t1.in_is_gl_degree
	                , t1.in_current_org
	                , t1.in_creator
	                , t1.in_creator_sno
	                , DATEADD(HOUR, 8, t1.in_regdate) AS 'in_regdate'
	                , DATEADD(HOUR, 8, t1.in_date)    AS 'in_date'
	                , t1.in_verify_result
	                , t1.in_ass_ver_result
	                , t2.id           AS 'in_cmt_resume'
	                , t2.in_sno       AS 'in_cmt_sno'
	                , t2.in_name      AS 'in_cmt_name'
	                , t2.in_short_org AS 'in_cmt_short'
	                , t2.in_area      AS 'in_cmt_city'
	                , t3.id           AS 'in_mu_resume'
	                , DATEADD(HOUR, 8, t3.in_degree_date) AS 'in_degree_date'
                FROM
	                IN_CLA_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.in_name = t1.in_committee
	                AND t2.in_member_type = 'area_cmt'
                INNER JOIN
	                IN_RESUME t3 WITH(NOLOCK)
	                ON t3.in_sno = t1.in_sno
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t2.in_sno = '{#cmt_sno}'
	                AND ISNULL(t1.in_ass_ver_result, '') = '1'
                ORDER BY
	                t2.in_sno
	                , t1.in_creator_sno
	                , t1.in_degree
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#cmt_sno}", cfg.cmt_sno);

            return cfg.inn.applySQL(sql);
        }


        private void RemoveOldDegreeFee(TConfig cfg)
        {
            string sql = "DELETE FROM In_Cla_Meeting_User_Degree_Fee WHERE in_cla_meeting = '{#meeting_id}' AND in_cmt_sno = '{#cmt_sno}'";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#cmt_sno}", cfg.cmt_sno);

            cfg.inn.applySQL(sql);
        }

        private class TRow
        {
            public string sno { get; set; }
            public string name { get; set; }
            public List<TChild> children { get; set; }
            public decimal total { get; set; }
        }

        private class TChild
        {
            public decimal tw_amount { get; set; }
            public decimal gl_amount { get; set; }
            public decimal yr_amount { get; set; }
            public decimal yr_subtotal { get; set; }
            public decimal total { get; set; }
            public Item item { get; set; }
        }


        private class TDegreeFee
        {
            public string no { get; set; }
            public decimal tw_amount { get; set; }
            public decimal gl_amount { get; set; }
            public decimal yr_amount { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string cmt_sno { get; set; }
            public string scene { get; set; }
        }

        private string GetDtmStr(string value, string format)
        {
            var dt = GetDtm(value);
            if (dt == DateTime.MinValue) return "";
            return dt.ToString(format);
        }

        private DateTime GetDtm(string value)
        {
            if (value == null || value == "") return DateTime.MinValue;

            DateTime result = DateTime.MinValue;
            if (DateTime.TryParse(value, out result))
            {
                return result;
            }

            return DateTime.MinValue;
        }

        private string GetDcmStr(decimal value)
        {
            return value.ToString("###,###");
        }

        private decimal GetDcm(string value, decimal def = 0)
        {
            if (value == null || value == "") return 0;

            decimal result = def;
            decimal.TryParse(value, out result);
            return result;
        }

        private int GetInt(string value, int def = 0)
        {
            if (value == null || value == "") return 0;

            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}