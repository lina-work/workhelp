﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Degree
{
    public class In_Cla_Payment_List : Item
    {
        public In_Cla_Payment_List(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 列出繳費清單
    輸入: 
        - meeting id
    日誌:
        - 2021.01.06 加入跆委會 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Cla_Payment_List";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string aml = "";
            string sql = "";
            string strError = "";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
                s_type = itmR.getProperty("s_type", ""),//測試-顯示未繳費的單子
                verify = itmR.getProperty("verify", ""),
                method_type = "",
            };

            List<string> cols = new List<string>
            {
                "in_title",
                "in_url",
                "in_register_url",
                "in_need_receipt",
                "in_meeting_type",
                "in_seminar_type",
                "in_verify_mode",
                "in_can_cmt_verify",
            };

            aml = "<AML>" +
                "<Item type='in_cla_meeting' action='get' id='" + cfg.meeting_id + "' select='" + string.Join(",", cols) + "'>" +
                "</Item></AML>";

            cfg.itmMeeting = inn.applyAML(aml);

            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                itmR.setProperty("permission_type", "1");
                itmR.setProperty("permission_message", "查無活動");
                return itmR;
            }

            cfg.in_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.in_url = cfg.itmMeeting.getProperty("in_url", "");
            cfg.in_register_url = cfg.itmMeeting.getProperty("in_register_url", "");
            cfg.in_meeting_type = cfg.itmMeeting.getProperty("in_meeting_type", "");
            cfg.in_need_receipt = cfg.itmMeeting.getProperty("in_need_receipt", "");
            cfg.in_can_cmt_verify = cfg.itmMeeting.getProperty("in_can_cmt_verify", "");

            //取得登入者資訊
            sql = "SELECT *";
            sql += " FROM In_Resume WITH(NOLOCK)";
            sql += " WHERE in_user_id = '" + cfg.strUserId + "'";
            cfg.loginResume = inn.applySQL(sql);
            cfg.login_resume_id = cfg.loginResume.getProperty("id", "");
            cfg.login_group = cfg.loginResume.getProperty("in_group", "");//所屬單位
            cfg.login_sno = cfg.loginResume.getProperty("in_sno", "");//協助報名者身分證號
            cfg.login_name = cfg.loginResume.getProperty("in_name", "");//
            cfg.login_member_type = cfg.loginResume.getProperty("in_member_type", "");
            cfg.login_member_role = cfg.loginResume.getProperty("in_member_role", "");

            string codeMethod = "ALL";
            string bodyMethod = "<method>" + strMethodName + "</method><code>" + codeMethod + "</code>";
            Item itmPermit = inn.applyMethod("In_CheckIdentity", bodyMethod);
            cfg.isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            cfg.isCommittee = itmPermit.getProperty("isCommittee", "") == "1";
            cfg.isGymOwner = itmPermit.getProperty("isGymOwner", "") == "1";
            cfg.isGymAssistant = itmPermit.getProperty("isGymAssistant", "") == "1";

            bool open = GetUserResumeListStatus(cfg.inn, cfg.meeting_id, cfg.strUserId);
            if (open) cfg.isMeetingAdmin = true;

            cfg.is_degree = cfg.in_meeting_type == "degree";

            if (!cfg.isMeetingAdmin && cfg.is_degree)
            {
                Item itmRoleResult = inn.applyMethod("In_Association_Role"
                    , "<in_sno>" + cfg.login_sno + "</in_sno>"
                    + "<idt_names>ACT_ASC_Degree</idt_names>");

                cfg.isMeetingAdmin = itmRoleResult.getProperty("inn_result", "") == "1";
            }

            cfg.can_review = cfg.isMeetingAdmin || cfg.isCommittee;

            if (cfg.scene == "inspect" && !cfg.can_review)
            {
                itmR.setProperty("permission_type", "1");
                itmR.setProperty("permission_message", "您無權限瀏覽此頁面");
                return itmR;
            }

            //加入審核時間卡控
            SetMeetingFuncTime(cfg);

            switch (cfg.in_meeting_type)
            {
                case "game":
                    itmR.setProperty("hide_stuff", "");
                    itmR.setProperty("hide_item", "");
                    itmR.setProperty("name_head", "選手數");
                    break;

                default:
                    itmR.setProperty("hide_stuff", "data-visible=\"false\"");
                    itmR.setProperty("hide_item", "data-visible=\"false\"");
                    itmR.setProperty("hide_l1", "data-visible=\"false\"");
                    itmR.setProperty("name_head", "報名人數");
                    break;
            }

            if (cfg.in_need_receipt == "1")
            {
                itmR.setProperty("show_receipt", "item_show_1");
            }
            else
            {
                itmR.setProperty("show_receipt", "item_show_0");
            }

            if (cfg.InCreatePayTime)
            {
                itmR.setProperty("can_create_payment", "item_show_1");
            }
            else
            {
                itmR.setProperty("can_create_payment", "item_show_0");
            }

            //依據登入者身分的權限 會有判定條件的不同
            if (cfg.isMeetingAdmin)
            {
                cfg.SQL_OnlyMyGym = "";

                //最高負責人(MeetingAdmin) 不使用群組判定
                cfg.group = "";
                cfg.open_function = "O";
                itmR.setProperty("open_function", "O");
                itmR.setProperty("pring_show", "1");//管理者等級可以看到列印按鈕
            }
            else if (cfg.isCommittee)
            {
                cfg.SQL_OnlyMyGym = " AND t1.in_committee = N'" + cfg.login_name + "'";

                //地區管理者 只能看見自己轄區下的道館報名資料
                cfg.group = "<in_committee>" + cfg.login_name + "</in_committee>";
                cfg.open_function = "O";
                itmR.setProperty("open_function", "O");
                itmR.setProperty("pring_show", "1");//管理者等級可以看到列印按鈕
            }
            else if (cfg.isGymOwner)
            {
                cfg.SQL_OnlyMyGym = " AND t1.in_creator_sno = N'" + cfg.login_sno + "'";

                //道館主(ACT_GymOwner) 群組判定  只能看見自己的
                cfg.group = "<in_creator_sno>" + cfg.login_sno + "</in_creator_sno>";
                cfg.open_function = "X";
                itmR.setProperty("open_function", "X");
                itmR.setProperty("pring_show", "0");//不是管理者等級不能看到列印按鈕
            }
            else
            {
                throw new Exception("您無權限瀏覽此頁面");
            }


            //判斷現在的登入者是否為此課程的共同講師 
            aml = "<AML>" +
                    "<Item type='In_Cla_Meeting_Resumelist' action='get'>" +
                    "<source_id>" + cfg.meeting_id + "</source_id>" +
                    "</Item></AML>";
            Item itmMResumelist = inn.applyAML(aml);

            for (int i = 0; i < itmMResumelist.getItemCount(); i++)
            {
                Item itmMResume = itmMResumelist.getItemByIndex(i);
                //判定登入者ID是否存在於該課程共同講師裡面，如果有包含在共同講師裡面就秀出按鈕
                if (cfg.login_resume_id.Contains(itmMResume.getProperty("related_id", "")))
                {
                    //協辦者也要可以看到按鈕
                    itmR.setProperty("pring_show", "1");
                    cfg.group = "";
                }
            }

            //呼叫excel method取得計算後的人數 項目數 費用
            this.setProperty("meeting_id", cfg.meeting_id);
            this.setProperty("export_type", "pay");

            if (cfg.verify == "1")
            {
                //附加晉段審核(未審核)
                AppendDegrees(cfg, "", "degree_unverify", itmR);
                //附加晉段審核(通過)
                AppendDegrees(cfg, "1", "degree_agree", itmR);
                //附加晉段審核(不通過)
                AppendDegrees(cfg, "0", "degree_reject", itmR);
            }

            //附加未產生繳費單
            AppendMUsers(cfg, itmR);

            //附加已產生繳費單(未付款)
            AppendMPays(cfg, false, itmR);
            //附加已產生繳費單(已付款)
            AppendMPays(cfg, true, itmR);

            //2020.10.30 若無未產生+未付款+已付款 代表此單位沒有任何繳費資訊 丟出特徵讓前端將使用者導向info
            string org_excels_count = itmR.getProperty("org_excels_count", "");
            string documents_count = itmR.getProperty("documents_count", "");
            if (org_excels_count == "0" && documents_count == "0")
            {
                itmR.setProperty("in_error", "X");
            }


            string apply_sql = @"
                SELECT
                    t1.id
                    , t1.in_name
                    , t1.in_pay_amount
                    , t1.in_section_name
                    , t1.in_cancel_memo
                    , t1.in_cancel_by_id
                    , t2.item_number
                    , t3.last_name
                    , t1.in_muid
                FROM 
                    In_Meeting_news t1 WITH(NOLOCK) 
                INNER JOIN 
                    In_Meeting_pay t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id 
                LEFT OUTER JOIN
                    [USER] t3 WITH(NOLOCK)
                    ON t3.owned_by_id = t1.in_cancel_by_id
                WHERE
                    t2.in_cla_meeting = '{#meeting_id}'
                    AND t1.in_cancel_status = N'{#in_cancel_status}'  
                ORDER BY
                    t1.in_cancel_time, t1.in_name
                ";

            //取得該賽事有[申請退費的繳費單資訊]  (退費申請)
            AppendApplyRefund(cfg, apply_sql, itmR);

            //繳費單-申請變更人員
            AppendApplyExchange(cfg, apply_sql, itmR);


            itmR.setProperty("meeting_name", cfg.in_title);
            itmR.setProperty("id", cfg.meeting_id);
            itmR.setProperty("player_group", cfg.login_group);
            itmR.setProperty("method_type", cfg.method_type);
            itmR.setProperty("inn_meeting_admin", cfg.isMeetingAdmin ? "1" : "0");
            itmR.setProperty("inn_committee_admin", cfg.isCommittee ? "1" : "0");
            itmR.setProperty("inn_excel_method", cfg.method_type);

            itmR.setProperty("in_title", cfg.in_title);
            itmR.setProperty("in_url", cfg.in_url);
            itmR.setProperty("in_register_url", cfg.in_register_url);
            itmR.setProperty("in_group", cfg.login_group);
            itmR.setProperty("in_sno", cfg.login_sno);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        //取得共同講師 (判斷現在的登入者是否為此課程的共同講師)
        private bool GetUserResumeListStatus(Innovator inn, string meeting_id, string strUserId)
        {
            string sql = @"
                SELECT
                    t2.id
                FROM 
                    In_Cla_Meeting_Resumelist t1 WITH(NOLOCK) 
                INNER JOIN 
                    IN_RESUME t2 WITH(NOLOCK) 
                    ON t2.id = t1.related_id    
                WHERE 
                    t1.source_id = '{#meeting_id}' 
                    AND t2.in_user_id = '{#in_user_id}';    
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_user_id}", strUserId);

            Item item = inn.applySQL(sql);

            if (item.getResult() != "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //晉段審核資料
        private void AppendDegrees(TConfig cfg, string in_verify_result, string item_name, Item itmReturn)
        {
            bool is_unverify = item_name == "degree_unverify";

            string verify_condition = "AND ISNULL(in_verify_result, '') = N'" + in_verify_result + "'";
            if (in_verify_result == "1")
            {
                //委員會審核通過，並且協會未審核 or 審核通過
                verify_condition = "AND ISNULL(in_verify_result, '') = '1' AND ISNULL(in_ass_ver_result, '') IN ('', '1')";
            }
            else if (in_verify_result == "0")
            {
                //委員會審核退回，或者協會退回
                verify_condition = "AND (ISNULL(in_verify_result, '') = '0' OR ISNULL(in_ass_ver_result, '') = '0' )";
            }

            string sql = @"
                SELECT
                    t1.*
	                , t2.id           AS 'resume_id'
	                , t2.in_degree    AS 'resume_degree'
	                , t2.in_degree_id AS 'resume_degree_id'
                    , t3.in_pay_amount_real
                    , t3.in_pay_photo
                    , t3.in_return_mark
                    , t3.in_collection_agency
                FROM
	                IN_CLA_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.in_sno = t1.in_sno
                LEFT OUTER JOIN
                    IN_MEETING_PAY t3 WITH(NOLOCK)
                    ON t3.in_cla_meeting = t1.id
                WHERE 
                    t1.source_id = '{#meeting_id}'
	                {#SQL_OnlyMyGym}
                    {#verify_condition}
                ORDER BY
                    t1.in_degree DESC
                    , t1.in_birth
             ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#SQL_OnlyMyGym}", cfg.SQL_OnlyMyGym)
                .Replace("{#verify_condition}", verify_condition);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmMUsers = cfg.inn.applySQL(sql);

            int count = itmMUsers.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmMUser = itmMUsers.getItemByIndex(i);
                itmMUser.setProperty("in_verify_mode", cfg.itmMeeting.getProperty("in_verify_mode", ""));

                Item item = cfg.inn.newItem();
                item.setType(item_name);
                item.setProperty("meeting_id", itmMUser.getProperty("source_id", ""));
                item.setProperty("id", itmMUser.getProperty("id", ""));
                item.setProperty("in_name", itmMUser.getProperty("in_name", ""));
                item.setProperty("in_sno_display", itmMUser.getProperty("in_sno_display", ""));
                item.setProperty("in_current_org", itmMUser.getProperty("in_current_org", ""));
                item.setProperty("in_committee", itmMUser.getProperty("in_committee", ""));
                item.setProperty("in_l1", itmMUser.getProperty("in_l1", ""));
                item.setProperty("in_expense", GetMoney(itmMUser.getProperty("in_expense", "0")));

                item.setProperty("in_paynumber", itmMUser.getProperty("in_paynumber", ""));
                item.setProperty("in_pay_type", GetPayStatusIcon(cfg.inn, itmMUser));

                item.setProperty("inn_cert_valid", GetCertStatus(itmMUser.getProperty("in_cert_valid", "")));
                item.setProperty("in_verify_memo", itmMUser.getProperty("in_verify_memo", ""));
                item.setProperty("in_verify_display", GetVerifyDisplay(cfg.inn, itmMUser));

                item.setProperty("resume_id", itmMUser.getProperty("resume_id", ""));

                item.setProperty("verify_btn", VerifyButton(cfg, itmMUser, is_unverify));

                itmReturn.addRelationship(item);
            }

            itmReturn.setProperty(item_name + "_count", count < 0 ? "0" : count.ToString());
        }

        private string VerifyButton(TConfig cfg, Item item, bool is_unverify)
        {
            if (cfg.in_can_cmt_verify == "1")
            {
                if (!cfg.InVerifyTime)
                {
                    return " ";
                }
                if (is_unverify)
                {
                    return "<button class='btn btn-sm btn-primary btn-verify' onclick='show_verifyModal(this)'>審核</button>";
                }
                else
                {
                    return "<button class='btn btn-sm btn-primary' onclick='show_verifyModal(this)'>審核</button>";
                }
            }
            else
            {
                return " ";
            }
        }

        //尚未產生繳費單
        private void AppendMUsers(TConfig cfg, Item itmReturn)
        {
            int sheet_content_a_count = 0;
            int content_a_sum = 0;

            string sql = @"
                SELECT 
    				t3.in_sno AS 'committee_sno'
                    , ISNULL(t3.in_short_org, t1.in_committee) AS 'committee_short_name'
                    , t1.in_current_org
    				, t1.in_creator_sno
    				, t1.in_creator
    				, t2.in_tel
                    , count(t1.id)           AS 'in_qty'
                    , sum(t1.in_expense)     AS 'subtotal'
                FROM 
                    IN_CLA_MEETING_USER t1 WITH (NOLOCK)
    			LEFT OUTER JOIN
    				IN_RESUME t2 WITH(NOLOCK)
    				ON t2.in_sno = t1.in_creator_sno
    			LEFT OUTER JOIN
    				IN_RESUME t3 WITH(NOLOCK)
    				ON t3.in_name = t1.in_committee AND t3.in_member_type = 'area_cmt' AND t3.in_member_role = 'sys_9999'
                WHERE 
                    t1.source_id = '{#meeting_id}' 
                    AND ISNULL(t1.in_paynumber, '') = ''
                    {#SQL_OnlyMyGym}
                GROUP BY 
                    t3.in_sno
                    , ISNULL(t3.in_short_org, t1.in_committee)
                    , t1.in_current_org
    				, t1.in_creator_sno
    				, t1.in_creator
    				, t2.in_tel
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#SQL_OnlyMyGym}", cfg.SQL_OnlyMyGym);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmMUsers = cfg.inn.applySQL(sql);

            Dictionary<string, TPay> map = CaseAsMap(itmMUsers);

            foreach (var kv in map)
            {
                var entity = kv.Value;
                var item = entity.Value;

                int quantity = entity.Quantities.Sum();
                int subtotal = entity.Subtotal.Sum();
                string creator_names = string.Join("<br>", entity.CreatorNames);
                string creator_tels = string.Join("<br>", entity.CreatorTels);

                Item PaymentList = cfg.inn.newItem("sheet_content_a");
                PaymentList.setProperty("committee_short_name_a", item.getProperty("committee_short_name", ""));//所屬委員會
                PaymentList.setProperty("in_current_org_a", item.getProperty("in_current_org", ""));//所屬單位
                PaymentList.setProperty("in_qty", quantity.ToString());//報名人數
                PaymentList.setProperty("in_expense_a", subtotal.ToString());//報名費用
                PaymentList.setProperty("in_creator_a", creator_names);//協助報名者姓名
                PaymentList.setProperty("in_creator_tel_a", creator_tels);//協助報名者電話

                PaymentList.setProperty("hide_stuff", itmReturn.getProperty("hide_stuff", ""));//隱藏隊職員數
                itmReturn.addRelationship(PaymentList);

                sheet_content_a_count++;
                content_a_sum += subtotal;
            }

            itmReturn.setProperty("org_excels_count", map.Count.ToString());
            itmReturn.setProperty("sheet_count_a", sheet_content_a_count.ToString() + "張單");
            itmReturn.setProperty("sheet_sum_a", ",共" + content_a_sum.ToString("N0") + "元");//千分位格式 會補0
        }

        //已產生繳費單
        private void AppendMPays(TConfig cfg, bool has_paid, Item itmReturn)
        {
            string pay_bool = has_paid ? "已繳費" : "未繳費";
            string suffix = has_paid ? "c" : "b";

            string sub_sql = "SELECT source_id, in_pay_amount AS 'expenses', count(id) AS 'in_qty' FROM IN_MEETING_NEWS WITH(NOLOCK) GROUP BY source_id, in_pay_amount";
            if (cfg.in_meeting_type == "degree")
            {
                sub_sql = "SELECT source_id, count(id) AS 'in_qty' FROM IN_MEETING_NEWS WITH(NOLOCK) GROUP BY source_id";
            }

            string sql = @"
                SELECT 
	                t1.*
	                , t1.item_number AS 'in_paynumber'
	                , t2.in_qty
	                , t3.in_tel      AS 'in_creator_tel'
                FROM 
	                IN_MEETING_PAY AS T1 WITH (NOLOCK)
                INNER JOIN
                (
	                {#sub_sql}
                ) t2
	                ON t2.source_id = t1.id
                LEFT OUTER JOIN
	                IN_RESUME t3 WITH(NOLOCK)
	                ON t3.in_sno = t1.in_creator_sno
                WHERE 
	                t1.in_cla_meeting = N'{#meeting_id}' 
	                AND t1.pay_bool = N'{#pay_bool}'
	                {#SQL_OnlyMyGym}
                ORDER BY 
	                t1.in_current_org
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                    .Replace("{#pay_bool}", pay_bool)
                    .Replace("{#SQL_OnlyMyGym}", cfg.SQL_OnlyMyGym)
                    .Replace("{#sub_sql}", sub_sql);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmMPays = cfg.inn.applySQL(sql);

            int count = itmMPays.getItemCount();

            int sheet_content_count = 0;
            int content_sum = 0;

            for (int i = 0; i < count; i++)
            {
                Item item = itmMPays.getItemByIndex(i);

                int quantity = GetIntVal(item.getProperty("in_qty", "0"));

                string in_pay_amount_exp = item.getProperty("in_pay_amount_exp", "0");
                string in_pay_amount_real = item.getProperty("in_pay_amount_real", "");
                string in_pay_date_exp = item.getProperty("in_pay_date_exp", "");
                string in_pay_date_exp1 = item.getProperty("in_pay_date_exp1", "");

                int pay_amount_exp = GetIntVal(in_pay_amount_exp);
                int pay_amount_real = GetIntVal(in_pay_amount_real);
                if (pay_amount_real == 0 && pay_bool == "已繳費")
                {
                    pay_amount_real = GetIntVal(in_pay_amount_exp);
                }
                string pay_date_exp = GetDateTimeVal(in_pay_date_exp, "yyyy/MM/dd", hours: 8);
                string pay_date_exp1 = GetDateTimeVal(in_pay_date_exp1, "yyyy/MM/dd", hours: 8);


                Item PaymentList = cfg.inn.newItem("sheet_content_" + suffix);
                PaymentList.setProperty("in_group_" + suffix, item.getProperty("in_group", ""));//群組
                PaymentList.setProperty("in_current_org_" + suffix, item.getProperty("in_current_org", ""));//單位
                PaymentList.setProperty("in_qty", quantity.ToString());//報名人數
                PaymentList.setProperty("in_pay_amount_" + suffix, pay_amount_exp.ToString());//報名費用
                PaymentList.setProperty("item_number_" + suffix, item.getProperty("item_number", ""));//繳費編號(最乾淨)

                PaymentList.setProperty("in_pay_date_" + suffix, pay_date_exp);//繳費日期
                PaymentList.setProperty("in_pay_amount1_" + suffix, pay_amount_real.ToString());//已繳費用
                PaymentList.setProperty("in_code_1_" + suffix, item.getProperty("in_code_1", ""));//條碼1
                PaymentList.setProperty("in_code_2_" + suffix, GetCodeMask(item.getProperty("in_code_2", "")));//條碼2
                PaymentList.setProperty("in_code_3_" + suffix, item.getProperty("in_code_3", ""));//條碼3
                PaymentList.setProperty("in_pay_date_exp1_" + suffix, pay_date_exp1);// 最後繳費期限
                PaymentList.setProperty("index_number_" + suffix, item.getProperty("index_number", ""));// 業主自訂編號
                PaymentList.setProperty("pay_bool_" + suffix, item.getProperty("pay_bool", ""));// 繳費狀態
                PaymentList.setProperty("invoice_up_" + suffix, item.getProperty("invoice_up", ""));// 抬頭發票
                PaymentList.setProperty("uniform_numbers_" + suffix, item.getProperty("uniform_numbers", ""));// 統一編號
                PaymentList.setProperty("in_refund_amount", item.getProperty("in_refund_amount", ""));//退款金額

                string in_amount = (Convert.ToInt32(item.getProperty("in_pay_amount_real", "0")) - Convert.ToInt32(item.getProperty("in_refund_amount", "0"))).ToString();
                PaymentList.setProperty("in_amount_c", in_amount);

                PaymentList.setProperty("in_creator_" + suffix, item.getProperty("in_creator", ""));//協助報名者姓名
                PaymentList.setProperty("in_creator_tel_" + suffix, item.getProperty("in_creator_tel", ""));//協助報名者電話

                PaymentList.setProperty("meeting_id", cfg.meeting_id);//賽事ID
                PaymentList.setProperty("open_function", cfg.open_function);
                PaymentList.setProperty("method_type", cfg.method_type);//名單 method

                PaymentList.setProperty("in_pay_type_" + suffix, GetPayStatusIcon(cfg.inn, item));

                PaymentList.setProperty("hide_stuff", itmReturn.getProperty("hide_stuff", ""));//隱藏隊職員數
                PaymentList.setProperty("show_receipt", cfg.in_need_receipt == "1" ? "item_show_1" : "item_show_0");

                itmReturn.addRelationship(PaymentList);

                sheet_content_count++;
                content_sum += pay_amount_exp;
            }

            itmReturn.setProperty("documents_count", count.ToString());
            itmReturn.setProperty("sheet_count_" + suffix, sheet_content_count.ToString() + "張單");
            itmReturn.setProperty("sheet_sum_" + suffix, ",共" + content_sum.ToString("N0") + "元");
        }

        //退費申請
        private void AppendApplyRefund(TConfig cfg, string apply_sql, Item itmReturn)
        {
            int refund_list_count = 0;
            int refund_list_sum = 0;//退費

            string sql = apply_sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_cancel_status}", "退費申請中");

            Item pay_joins = cfg.inn.applySQL(sql);

            string last_key = "";

            for (int i = 0; i < pay_joins.getItemCount(); i++)
            {
                Item pay_join = pay_joins.getItemByIndex(i);
                string team_key = pay_join.getProperty("in_name", "");
                if (team_key == last_key)
                {
                    continue;
                }

                Item RefundLists = cfg.inn.newItem("refundlist");
                RefundLists.setProperty("inn_no", (i + 1).ToString());//項次
                RefundLists.setProperty("id", pay_join.getProperty("id", ""));//news_id
                RefundLists.setProperty("item_number", pay_join.getProperty("item_number", ""));//繳費單號
                RefundLists.setProperty("in_pay_amount", pay_join.getProperty("in_pay_amount", ""));//費用
                RefundLists.setProperty("in_name", pay_join.getProperty("in_name", ""));//組名
                RefundLists.setProperty("in_cancel_memo", pay_join.getProperty("in_cancel_memo", ""));//退費說明
                RefundLists.setProperty("in_refund_applicant", pay_join.getProperty("last_name", ""));//退費申請人
                RefundLists.setProperty("meeting_id", cfg.meeting_id);//賽事ID(超連結用)

                itmReturn.addRelationship(RefundLists);

                refund_list_count++;
                refund_list_sum += GetIntVal(pay_join.getProperty("in_pay_amount", "0"));
                last_key = team_key;
            }

            itmReturn.setProperty("refund_list", refund_list_count.ToString() + "張單");
            itmReturn.setProperty("refund_list_sum", ",共" + refund_list_sum.ToString("N0") + "元");
        }

        //申請變更人員
        private void AppendApplyExchange(TConfig cfg, string apply_sql, Item itmReturn)
        {
            int exchange_list_count = 0;

            string sql = apply_sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_cancel_status}", "變更申請中");

            Item pay_joins2 = cfg.inn.applySQL(sql);

            string last_key = "";

            for (int i = 0; i < pay_joins2.getItemCount(); i++)
            {
                Item pay_join2 = pay_joins2.getItemByIndex(i);
                string team_key = pay_join2.getProperty("in_muid", "");

                if (team_key == last_key)
                {
                    continue;
                }

                Item ExchangeLists = cfg.inn.newItem("exchangelist");
                ExchangeLists.setProperty("inn_no", (i + 1).ToString());//項次
                ExchangeLists.setProperty("id", pay_join2.getProperty("id", ""));//news_id
                ExchangeLists.setProperty("item_number", pay_join2.getProperty("item_number", ""));//繳費單號
                ExchangeLists.setProperty("in_pay_amount", pay_join2.getProperty("in_pay_amount", ""));//費用
                ExchangeLists.setProperty("in_name", pay_join2.getProperty("in_name", ""));//組名
                ExchangeLists.setProperty("in_cancel_memo", pay_join2.getProperty("in_cancel_memo", ""));//退費說明
                ExchangeLists.setProperty("in_refund_applicant", pay_join2.getProperty("last_name", ""));//退費申請人
                ExchangeLists.setProperty("meeting_id", cfg.meeting_id);//賽事ID(超連結用)

                itmReturn.addRelationship(ExchangeLists);

                exchange_list_count++;
                last_key = team_key;
            }

            itmReturn.setProperty("exchange_list", exchange_list_count.ToString() + "張單");
        }

        private Dictionary<string, TPay> CaseAsMap(Item items)
        {
            Dictionary<string, TPay> map = new Dictionary<string, TPay>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_current_org = item.getProperty("in_current_org", "");
                string in_creator = item.getProperty("in_creator", "");
                string in_tel = item.getProperty("in_tel", "");
                string in_qty = item.getProperty("in_qty", "");
                string subtotal = item.getProperty("subtotal", "");

                TPay entity = null;
                if (map.ContainsKey(in_current_org))
                {
                    entity = map[in_current_org];
                }
                else
                {
                    entity = new TPay
                    {
                        Value = item,
                        Quantities = new List<int>(),
                        Subtotal = new List<int>(),
                        CreatorNames = new List<string>(),
                        CreatorTels = new List<string>(),
                        Creators = new List<Item>()
                    };

                    map.Add(in_current_org, entity);
                }

                entity.Quantities.Add(GetIntVal(in_qty));
                entity.Subtotal.Add(GetIntVal(subtotal));
                entity.CreatorNames.Add(in_creator);
                entity.CreatorTels.Add(in_tel);

                entity.Creators.Add(item);
            }

            return map;
        }

        private void SetMeetingFuncTime(TConfig cfg)
        {
            cfg.InVerifyTime = false;
            cfg.InCreatePayTime = false;

            string tgt_action = cfg.isCommittee ? "21" : "25";

            string sql = @"
                SELECT
	                id
                    , in_action
	                , DATEADD(HOUR, 8, in_date_s) AS 'in_date_s'
	                , DATEADD(HOUR, 8, in_date_e) AS 'in_date_e'
                FROM 
	                IN_CLA_MEETING_FUNCTIONTIME WITH(NOLOCK)
                WHERE 
	                source_id = '{#meeting_id}' 
	                AND in_action IN ('21', '25)
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_action = item.getProperty("in_action", "");
                
                if (in_action == tgt_action)
                {
                    cfg.itmMtFuncTime = item;
                }
                
                switch(in_action)
                {
                    case "21": cfg.itmFuncTimeAcs = item; break;
                    case "25": cfg.itmFuncTimeCmt = item; break;
                }
            }

            if (cfg.itmMtFuncTime == null) cfg.itmMtFuncTime = cfg.inn.newItem();
            if (cfg.itmFuncTimeAcs == null) cfg.itmFuncTimeAcs = cfg.inn.newItem();
            if (cfg.itmFuncTimeCmt == null) cfg.itmFuncTimeCmt = cfg.inn.newItem();

            var now = DateTime.Now;
            if (cfg.itmMtFuncTime.isError() || cfg.itmMtFuncTime.getResult() == "")
            {
                //未設定視為不卡控
                cfg.InVerifyTime = true;
            }
            else
            {
                var dts = GetDtmVal(cfg.itmMtFuncTime.getProperty("in_date_s", ""));
                var dte = GetDtmVal(cfg.itmMtFuncTime.getProperty("in_date_e", ""));

                if (now >= dts && now <= dte)
                {
                    cfg.InVerifyTime = true;
                }
            }

            var end_time = GetDtmVal(cfg.itmFuncTimeAcs.getProperty("in_date_e", ""));
            if (end_time < now)
            {
                //委員會可建立繳費單
                cfg.InCreatePayTime = true;
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }


            //外部輸入區

            /// <summary>
            /// 活動 id
            /// </summary>
            public string meeting_id { get; set; }

            /// <summary>
            /// 情景 (inspect: 核查)
            /// </summary>
            public string scene { get; set; }

            /// <summary>
            /// 測試-顯示未繳費的單子
            /// </summary>
            public string s_type { get; set; }

            /// <summary>
            /// 需要附加資格審核資料(1: 附加)
            /// </summary>
            public string verify { get; set; }

            public Item itmFuncTimeAcs { get; set; }
            public Item itmFuncTimeCmt { get; set; }

            //權限區

            /// <summary>
            /// 最高管理者
            /// </summary>
            public bool isMeetingAdmin { get; set; }

            /// <summary>
            /// 地區管理者
            /// </summary>
            public bool isCommittee { get; set; }

            /// <summary>
            /// 道館主
            /// </summary>
            public bool isGymOwner { get; set; }

            /// <summary>
            /// 道館協助
            /// </summary>
            public bool isGymAssistant { get; set; }

            /// <summary>
            /// 可以審核
            /// </summary>
            public bool can_review { get; set; }



            //登入者資訊區

            /// <summary>
            /// 登入者 Resume 資料
            /// </summary>
            public Item loginResume { get; set; }

            /// <summary>
            /// 登入者 Resume id
            /// </summary>
            public string login_resume_id { get; set; }

            /// <summary>
            /// 登入者 會員類型
            /// </summary>
            public string login_member_type { get; set; }

            /// <summary>
            /// 登入者 會員角色
            /// </summary>
            public string login_member_role { get; set; }

            /// <summary>
            /// 登入者 所屬群組
            /// </summary>
            public string login_group { get; set; }

            /// <summary>
            /// 登入者 身分證號
            /// </summary>
            public string login_sno { get; set; }

            /// <summary>
            /// 登入者 姓名
            /// </summary>
            public string login_name { get; set; }


            // Meeting 區

            /// <summary>
            /// Meeting 資料
            /// </summary>
            public Item itmMeeting { get; set; }

            /// <summary>
            /// 活動名稱
            /// </summary>
            public string in_title { get; set; }

            /// <summary>
            /// 專屬展示網址
            /// </summary>
            public string in_url { get; set; }

            /// <summary>
            /// 專屬報名網址
            /// </summary>
            public string in_register_url { get; set; }

            /// <summary>
            /// 活動類型
            /// </summary>
            public string in_meeting_type { get; set; }

            /// <summary>
            /// 需下載收據功能
            /// </summary>
            public string in_need_receipt { get; set; }

            public string in_can_cmt_verify { get; set; }

            public string SQL_OnlyMyGym { get; set; }
            public string group { get; set; }
            public string open_function { get; set; }
            public string method_type { get; set; }

            public bool is_degree { get; set; }

            public Item itmMtFuncTime { get; set; }
            public bool InVerifyTime { get; set; }
            public bool InCreatePayTime { get; set; }
        }

        private class TPay
        {
            public Item Value { get; set; }

            /// <summary>
            /// 數量
            /// </summary>
            public List<int> Quantities { get; set; }

            /// <summary>
            /// 金額
            /// </summary>
            public List<int> Subtotal { get; set; }

            /// <summary>
            /// 協助報名者姓名s
            /// </summary>
            public List<string> CreatorNames { get; set; }

            /// <summary>
            /// 協助報名者行動電話s
            /// </summary>
            public List<string> CreatorTels { get; set; }

            /// <summary>
            /// 協助報名者列表
            /// </summary>
            public List<Item> Creators { get; set; }
        }

        //in_meeting_pay 條碼2 需加上遮罩
        private string GetCodeMask(string value)
        {
            if (value.Length == 16)
            {
                return value.Substring(0, 4) + "*******" + value.Substring(value.Length - 5, 5);
            }
            return "";
        }

        //資料狀態
        private string GetCertStatus(string value)
        {
            if (value == "1")
            {
                return "<span style='color: green'>已上傳</span>";
            }
            else if (value == "0")
            {
                return "<span style='color: red'>資料不齊</span>";
            }
            else
            {
                return "<span style='color: #2C4198'>未檢查</span>";
            }
        }

        //審核結果
        private string GetVerifyStatus(Item item)
        {
            //委員會審核
            string in_verify_result = item.getProperty("in_verify_result", "");
            //協會審核
            string in_ass_ver_result = item.getProperty("in_ass_ver_result", "");

            if (in_verify_result == "1")
            {
                return GetVerifyStatus("協會", in_ass_ver_result);
            }
            else
            {
                return GetVerifyStatus("跆委", in_verify_result);
            }
        }

        private string GetVerifyStatus(string prefix, string value)
        {
            if (value == "1")
            {
                return "<span style='color: green'>" + prefix + "通過</span>";
            }
            else if (value == "0")
            {
                return "<span style='color: red'>" + prefix + "不通過</span>";
            }
            else
            {
                return "<span style='color: #2C4198'>" + prefix + "審核中</span>";
            }
        }

        private string GetPayStatusIcon(Innovator inn, Item item)
        {
            StringBuilder builder = new StringBuilder();

            string[] properties = new string[]
            {
                "in_paynumber", //繳費單號
                "in_pay_amount_real", //實際收款金額
                "in_pay_photo", //上傳繳費照片(繳費收據)
                "in_return_mark", //審核退回說明
                "in_collection_agency", //代收機構
            };

            foreach (string property in properties)
            {
                builder.Append("<" + property + ">" + item.getProperty(property, "") + "</" + property + ">");
            }

            Item itmResult = inn.applyMethod("In_PayType_Icon", builder.ToString());

            return itmResult.getProperty("in_pay_type", "");
        }

        private string GetVerifyDisplay(Innovator inn, Item item)
        {
            StringBuilder builder = new StringBuilder();

            string[] properties = new string[]
            {
                "in_verify_mode", //活動審核模式
                "in_verify_result", //委員會審核結果
                "in_verify_memo", //委員會審核說明
                "in_ass_ver_result", //協會審核結果
                "in_ass_ver_memo", //協會審核說明
            };

            foreach (string property in properties)
            {
                builder.Append("<" + property + ">" + item.getProperty(property, "") + "</" + property + ">");
            }

            Item itmResult = inn.applyMethod("In_Verify_Message", builder.ToString());

            return itmResult.getProperty("in_verify_display", "");
        }

        private DateTime GetDtmVal(string value)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            return result;
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        private string GetMoney(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result.ToString("###,###");
        }

        private string GetDateTimeVal(string value, string format = "yyyy-MM-dd", int hours = 0)
        {
            if (value == null || value == "") return "";
            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value.Replace("/", "-"), out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }
    }
}