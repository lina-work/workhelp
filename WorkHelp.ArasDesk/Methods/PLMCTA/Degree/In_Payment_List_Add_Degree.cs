﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_Payment_List_Add_Degree : Item
    {
        public In_Payment_List_Add_Degree(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 產生繳費條碼(晉段專用)
                對象: 委員會
                描述: 委員會產生繳費單轉帳給協會
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Payment_List_Add_Degree";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                in_meeting_type = "",
                in_meeting_code = "",
                in_receice_org_code = "",
               
                in_imf_date_e = "",

                meeting_id = itmR.getProperty("meeting_id", "")
            };


            //組態初始化
            InitialConfig(cfg, itmR);
            //取得統計摘要
            var itmSummary = GetMUsersSummary(cfg);
            //取得報名者資料
            var itmMUsers = GetMUsers(cfg);
            //取得道館資料
            var itmOrgs = GetOrgs(cfg);
            //檢查繳費資料
            CheckSummaryAmount(cfg, itmSummary);
            //取得繳費單資料型
            var model = GetPayModel(cfg, itmSummary, itmMUsers, itmR);

            ////新增繳費單與明細(明細為[與會者]版本)
            //AddPaymentByMUsers(cfg, model, itmMUsers);

            //新增繳費單與明細(明細為[道館]版本)
            AddPaymentByOrgs(cfg, model, itmOrgs);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        /// <summary>
        /// 新增繳費單與明細資料
        /// </summary>
        private void AddPaymentByOrgs(TConfig cfg, TModel model, Item itmOrgs)
        {
            var documents = AppendMainPayment(cfg, model);

            //取出該賽事的關聯繳費
            string aml = "<AML>" +
                "<Item type='In_Meeting_pay' action='get'>" +
                "<" + cfg.MtProperty + ">" + cfg.meeting_id + "</" + cfg.MtProperty + ">" +
                "<id>" + documents.getProperty("id", "") + "</id>" +
                "</Item></AML>";

            Item SingleNumbers = cfg.inn.applyAML(aml);

            for (int i = 0; i < itmOrgs.getItemCount(); i++)
            {
                Item itmOrg = itmOrgs.getItemByIndex(i);
                string in_regdate = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");

                //新增到繳費資訊下
                Item in_meeting_news = cfg.inn.newItem("In_Meeting_news", "add");
                in_meeting_news.setProperty("in_ans_l3", itmOrg.getProperty("in_creator", ""));//組別彙整結果
                in_meeting_news.setProperty("in_pay_amount", itmOrg.getProperty("in_expense", ""));//應繳金額
                in_meeting_news.setProperty("in_name", itmOrg.getProperty("in_name", ""));//姓名
                in_meeting_news.setProperty("in_sno", itmOrg.getProperty("in_creator_sno", ""));//身分證
                in_meeting_news.setProperty("in_creator", cfg.pay_creator_name);//協助報名者
                in_meeting_news.setProperty("in_creator_sno", cfg.pay_creator_sno);//協助報名者帳號
                in_meeting_news.setProperty("in_regdate", in_regdate);//報名日期

                in_meeting_news.setProperty("in_l1", "");//第一層
                in_meeting_news.setProperty("in_l2", "");//第二層
                in_meeting_news.setProperty("in_l3", "");//第三層
                in_meeting_news.setProperty("in_index", "");//序號
                in_meeting_news.setProperty("in_section_name", itmOrg.getProperty("in_creator", ""));//組名

                SingleNumbers.addRelationship(in_meeting_news);
            }

            SingleNumbers.apply();

            var in_paynumber = SingleNumbers.getProperty("item_number", "");

            //更新與會者繳費單號
            UpdMUsersPaynumber(cfg, in_paynumber);
        }

        /// <summary>
        /// 新增繳費單與明細資料
        /// </summary>
        private void AddPaymentByMUsers(TConfig cfg, TModel model, Item itmMUsers)
        {
            var documents = AppendMainPayment(cfg, model);

            //取出該賽事的關聯繳費
            string aml = "<AML>" +
                "<Item type='In_Meeting_pay' action='get'>" +
                "<" + cfg.MtProperty + ">" + cfg.meeting_id + "</" + cfg.MtProperty + ">" +
                "<id>" + documents.getProperty("id", "") + "</id>" +
                "</Item></AML>";

            Item SingleNumbers = cfg.inn.applyAML(aml);

            for (int i = 0; i < itmMUsers.getItemCount(); i++)
            {
                Item itmMUser = itmMUsers.getItemByIndex(i);
                string in_regdate = itmMUser.getProperty("in_regdate", "");
                in_regdate = GetDtmStr(in_regdate, "yyyy-MM-ddTHH:mm:ss", 8);

                //新增到繳費資訊下
                Item in_meeting_news = cfg.inn.newItem("In_Meeting_news", "add");
                in_meeting_news.setProperty("in_ans_l3", itmMUser.getProperty("in_gameunit", ""));//組別彙整結果
                in_meeting_news.setProperty("in_pay_amount", itmMUser.getProperty("in_expense", ""));//應繳金額
                in_meeting_news.setProperty("in_name", itmMUser.getProperty("in_name", ""));//姓名
                in_meeting_news.setProperty("in_sno", itmMUser.getProperty("in_sno", ""));//身分證
                in_meeting_news.setProperty("in_creator", cfg.pay_creator_name);//協助報名者
                in_meeting_news.setProperty("in_creator_sno", cfg.pay_creator_sno);//協助報名者帳號
                in_meeting_news.setProperty("in_regdate", in_regdate);//報名日期

                in_meeting_news.setProperty("in_l1", itmMUser.getProperty("in_l1", ""));//第一層
                in_meeting_news.setProperty("in_l2", itmMUser.getProperty("in_l2", ""));//第二層
                in_meeting_news.setProperty("in_l3", itmMUser.getProperty("in_l3", ""));//第三層
                in_meeting_news.setProperty("in_index", itmMUser.getProperty("in_index", ""));//序號
                in_meeting_news.setProperty("in_section_name", itmMUser.getProperty("in_section_name", ""));//組名
                in_meeting_news.setProperty("in_l1_sort", itmMUser.getProperty("in_l1_sort", ""));//第一層排序

                in_meeting_news.setProperty("in_muid", itmMUser.getProperty("id", ""));//與會者id

                SingleNumbers.addRelationship(in_meeting_news);
            }

            SingleNumbers.apply();

            var in_paynumber = SingleNumbers.getProperty("item_number", "");

            //更新與會者繳費單號
            UpdMUsersPaynumber(cfg, in_paynumber);
        }

        private Item AppendMainPayment(TConfig cfg, TModel model)
        {
            //產生繳費單
            Item SingleNumber = cfg.inn.newItem("In_Meeting_pay", "add");

            SingleNumber.setProperty("owned_by_id", cfg.pay_identity_id);//建單者
            SingleNumber.setProperty("in_creator", cfg.pay_creator_name);//協助報名者
            SingleNumber.setProperty("in_creator_sno", cfg.pay_creator_sno);//協助報名者身分證字號
            SingleNumber.setProperty("in_group", cfg.pay_creator_group);//所屬群組
            SingleNumber.setProperty("in_committee", cfg.pay_committee);//所屬跆委會

            SingleNumber.setProperty("in_current_org", model.xls_current_org);//所屬單位
            SingleNumber.setProperty("in_pay_amount_exp", model.xls_money);//應繳金額

            SingleNumber.setProperty("in_pay_date_exp", cfg.due_date.Split('T')[0]);//應繳日期
            SingleNumber.setProperty("in_pay_date_exp1", cfg.last_payment_date.Split('T')[0]);//最後收費日期

            SingleNumber.setProperty("in_real_stuff", model.xls_real_stuff);//隊職員數
            SingleNumber.setProperty("in_real_player", model.xls_real_player);//選手人數
            SingleNumber.setProperty("in_real_items", model.xls_real_items);//項目總計

            SingleNumber.setProperty("in_code_1", model.B1);//條碼1
            SingleNumber.setProperty("in_code_2", "00" + model.B2);//條碼2
            SingleNumber.setProperty("in_code_3", model.B3);//條碼3
            SingleNumber.setProperty("pay_bool", "未繳費");//繳費狀態
            SingleNumber.setProperty("in_add_time", model.AddTime);//建單時間
            SingleNumber.setProperty("index_number", model.barcode2_1);//業主自訂編號

            SingleNumber.setProperty(cfg.MtProperty, cfg.meeting_id);

            return SingleNumber.apply();
        }

        //更新與會者繳費單號
        private Item UpdMUsersPaynumber(TConfig cfg, string in_paynumber)
        {
            string sql = @"
                UPDATE t2 SET
                    t2.in_paynumber = '{#in_paynumber}'
                FROM
                    IN_CLA_MEETING_USER_DEGREE_FEE t1 WITH(NOLOCK)
				INNER JOIN
					IN_CLA_MEETING_USER t2 WITH(NOLOCK)
					ON t2.id = t1.in_muid
                WHERE
					t1.in_cla_meeting = '{#meeting_id}'
					AND t1.in_cmt_sno = '{#in_cmt_sno}'
					AND ISNULL(t2.in_paynumber, '') = ''
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_cmt_sno}", cfg.login_resume_sno)
                .Replace("{#in_paynumber}", in_paynumber);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private void InitialConfig(TConfig cfg, Item itmReturn)
        {
            cfg.MtType = "In_Cla_Meeting";
            cfg.MUserType = "In_Cla_Meeting_User";
            cfg.MFuncType = "In_Cla_Meeting_Functiontime";
            cfg.MtProperty = "in_cla_meeting";

            //設定權限角色
            Item itmPermit = cfg.inn.applyMethod("In_CheckIdentity", "<method>" + cfg.strMethodName + "</method><code>ALL</code>");
            cfg.isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            cfg.isCommittee = itmPermit.getProperty("isCommittee", "") == "1";
            cfg.isGymOwner = itmPermit.getProperty("isGymOwner", "") == "1";
            cfg.isGymAssistant = itmPermit.getProperty("isGymAssistant", "") == "1";


            //取得登入者資訊
            cfg.itmLogin = cfg.inn.applySQL("SELECT * FROM In_Resume WITH(NOLOCK) WHERE in_user_id = '" + cfg.strUserId + "'");
            cfg.login_resume_group = cfg.itmLogin.getProperty("in_group", "");//所屬單位
            cfg.login_resume_name = cfg.itmLogin.getProperty("in_name", "");//登入者姓名
            cfg.login_resume_sno = cfg.itmLogin.getProperty("in_sno", "");//登入者身分證號
            cfg.login_manager_name = cfg.itmLogin.getProperty("in_manager_name", "");//登入者所屬委員會

            //取得活動資料
            cfg.itmMeeting = cfg.inn.applyAML("<AML><Item type='" + cfg.MtType + "' action='get'><id>" + cfg.meeting_id + "</id></Item></AML>");
            cfg.in_meeting_code = cfg.itmMeeting.getProperty("item_number", "");//賽事編號
            cfg.in_meeting_type = cfg.itmMeeting.getProperty("in_meeting_type", "").ToLower();//活動類型
            cfg.in_receice_org_code = cfg.itmMeeting.getProperty("in_receice_org_code", "");//收款單位編號


            if (cfg.in_receice_org_code == "")
            {
                throw new Exception("Meeting 收款單位編號不可為空白");
            }
            if (cfg.in_receice_org_code.Length != 6)
            {
                throw new Exception("收款單位編號格式錯誤");
            }

            //取得該[賽事的報名結束日]
            cfg.itmFunctiontime = cfg.inn.applyAML("<AML><Item type='" + cfg.MFuncType + "' action='get'><source_id>" + cfg.meeting_id + "</source_id><in_action>sheet1</in_action></Item></AML>");
            cfg.in_imf_date_e = cfg.itmFunctiontime.getProperty("in_date_e", "");//最後報名日期

            //如果[建單日期]與[賽事報名結束日期]差距天數小於7天 直接讓[最後繳費期限]訂在[報名結束日期]
            DateTime in_imf_date_e_dt = Convert.ToDateTime(cfg.in_imf_date_e);//最後報名日期
            TimeSpan ts = in_imf_date_e_dt - System.DateTime.Today;
            double days = ts.TotalDays;

            if (days < 7)
            {
                cfg.last_payment_date = cfg.in_imf_date_e;
                cfg.due_date = cfg.in_imf_date_e;
            }

            //繳費單歸屬
            cfg.pay_identity_id = cfg.strIdentityId;
            cfg.pay_creator_name = cfg.login_resume_name;
            cfg.pay_creator_sno = cfg.login_resume_sno;
            cfg.pay_creator_group = cfg.login_resume_group;
            cfg.pay_committee = cfg.login_resume_name;
        }

        /// <summary>
        /// 檢查繳費資料
        /// </summary>
        private void CheckSummaryAmount(TConfig cfg, Item itmSummary)
        {
            if (itmSummary.isError() || itmSummary.getResult() == "")
            {
                throw new Exception("無待繳費資料");
            }

            //費用
            string xls_money = itmSummary.getProperty("expenses", "");
            if (xls_money == "" || xls_money == "0")
            {
                throw new Exception("無待繳費用");
            }
        }
        
        /// <summary>
        /// 新增繳費單與明細資料
        /// </summary>
        private TModel GetPayModel(TConfig cfg, Item itmSummary, Item itmMUsers, Item itmReturn)
        {
            var result = new TModel
            {
                blank = " ",
                codes = "10",
                payment_mode = "A",
                project_code = "",
                index_number = "",
                barcode2 = "",
                barcode2_1 = "",
                B1 = "",
                B2 = "",
                B3 = "",
                CurrentTime = System.DateTime.Today,
                AddTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"),
            };

            int muser_count = itmMUsers.getItemCount();

            //將最後收費日期&應繳日期轉型
            DateTime in_pay_date_exp1 = Convert.ToDateTime(cfg.last_payment_date.Split('T')[0]);
            DateTime in_pay_date_exp = Convert.ToDateTime(cfg.due_date.Split('T')[0]);

            //費用
            result.xls_money = itmSummary.getProperty("expenses", "");
            result.xls_current_org = itmSummary.getProperty("in_current_org", "");//所屬單位
            result.xls_real_stuff = "0";
            result.xls_real_player = muser_count.ToString();
            result.xls_real_items = muser_count.ToString();

            //備用碼(1)+運動代碼(0)
            result.codes = GetPayCodes(cfg, result.codes);
               
            //區分繳費模式(A>繳款人 B>委託單位)
            result.project_code = GetProjectCode(result.payment_mode, GetInt(result.xls_money));

            //作廢
            result.b2_1 = cfg.in_meeting_code.Substring(cfg.in_meeting_code.Length - 4);

            //業主自訂編號: [賽事/講習序號(後3碼)] + 自訂流水號(4碼)(預設為0001)
            //[運動賽事序號(後3碼)]
            result.b2_2 = cfg.in_meeting_code.Substring(cfg.in_meeting_code.Length - 3);

            result.index_number_prefix = result.b2_2.TrimStart('0');

            //找出該[賽事]繳費單最大的[業主自訂編號]
            string sql = "SELECT MAX(index_number)";
            sql += " FROM In_Meeting_pay WITH(NOLOCK)";
            sql += " WHERE index_number LIKE '" + result.index_number_prefix + "%'";
            Item itmPay = cfg.inn.applySQL(sql);

            if (itmPay.getResult() == "")
            {
                result.barcode2 = result.b2_1 + "0001";
                result.barcode2_1 = result.b2_2 + "0001";
            }
            else
            {
                //若不是第一張單則將最大的[業主自訂編號]+1
                int number_next = GetInt(itmPay.getProperty("column1", "0")) + 1;
                result.index_number = number_next.ToString();

                string suffix = result.index_number.Substring(result.index_number.Length - 4);

                result.barcode2 = result.b2_1 + suffix;
                result.barcode2_1 = result.b2_2 + suffix;
            }

            if (result.project_code == "")
            {
                result.B1 = "";
                result.B2 = "";
                result.B3 = "";
            }
            else
            {
                //這是超商繳費
                //第一碼(最後收費日期(DateTime),項目代碼(String))
                result.B1 = BarCode1_OutPut(in_pay_date_exp1, result.project_code);

                //第二碼(賽事編號(String),業者自訂編號(String))
                result.B2 = cfg.in_receice_org_code + result.barcode2_1;

                string checkhum = cfg.inn.applyMethod("In_GetRank_CheckSum"
                    , "<account>" + result.B2 + "</account><pay>" + result + "</pay>").getResult();

                result.B2 = result.B2 + checkhum;

                //第三碼(應繳日期(DateTime),費用(String),一碼(String),二碼(String))
                result.B3 = BarCode3_OutPut(in_pay_date_exp, result.xls_money, result.B1, result.B2);
            }

            return result;
        }

        private string GetPayCodes(TConfig cfg, string codes)
        {
            //預設為 10
            string result = codes;

            //當收款單位編號長度為 6 碼
            if (cfg.in_receice_org_code.Length == 6)
            {
                result = "";
            }

            return result;
        }

        private string GetProjectCode(string payment_mode, int money)
        {
            string project_code = "";
            switch (payment_mode)
            {
                case "A":
                    if (money <= 20000)
                    {
                        project_code = "6NR";
                    }
                    else if (money > 20000 && money <= 40000)
                    {
                        project_code = "6NS";
                    }
                    else if (money > 40000 && money <= 60000)
                    {
                        project_code = "6BD";
                    }
                    break;
                case "B":
                    if (money <= 20000)
                    {
                        project_code = "68Q";
                    }
                    else if (money > 20000 && money <= 40000)
                    {
                        project_code = "68R";
                    }
                    else if (money > 40000 && money <= 60000)
                    {
                        project_code = "6BF";
                    }
                    break;
            }
            return project_code;
        }

        //產生條碼**********************************
        int ichecksum1 = 0, ichecksum2 = 0;

        //判斷是否為英文或數字
        public bool IsNumberOREng(string str)
        {
            System.Text.RegularExpressions.Regex reg1 = new System.Text.RegularExpressions.Regex(@"^[A-Za-z0-9$+-.% ]+$");
            return reg1.IsMatch(str);

        }
        //判斷是否為數字
        public bool IsPositive_Number(string str)
        {
            System.Text.RegularExpressions.Regex reg1 = new System.Text.RegularExpressions.Regex(@"^[0-9]*[1-9][0-9]*$");
            return reg1.IsMatch(str);

        }
        //產生第一段條碼(代收期限,代收項目)
        public string BarCode1_OutPut(DateTime date, String num)
        {
            //確認代收項目
            bool bIsMatch;
            bIsMatch = IsNumberOREng(num.ToString());
            if (bIsMatch == false)
            {
                throw new System.ArgumentException("代收項目編號含有未定義之特殊字元。");
            }
            else
            {
                if (num.ToString().Length != 3)
                {
                    throw new System.ArgumentException("代收項目編號長度應為3。");
                }
            }
            if (date < DateTime.Today)
            {
                // throw new System.ArgumentException("代收期限不應小於今日。");
            }

            string strDate = string.Format("{0}{1}{2}", (date.Year - 1911).ToString().Substring(1, 2), date.Month.ToString("00"), date.Day.ToString("00"));
            return strDate + num;
        }

        //產生第二段條碼(業者自訂)
        public string BarCode2_OutPut(String num, String custom)
        {
            //確認業主自訂編號
            bool bIsMatch = false;
            bIsMatch = IsNumberOREng(custom.ToString());
            if (bIsMatch == false)
            {
                throw new System.ArgumentException("業主自訂編號含有未定義之特殊字元。");
            }
            else
            {
                if (custom.ToString().Length > 10)
                {
                    throw new System.ArgumentException("業主自訂編號超出字數限制。");
                }
            }

            string strCustom = custom.PadRight(10, '0');
            return "00" + num + strCustom;
        }

        //產生第三段條碼(應繳日,金額,第一碼,第二碼)
        public string BarCode3_OutPut(DateTime date, string strpay, String barcode1, String barcode2)
        {
            //確認金額
            bool bIsMatch = false;

            if (strpay != "0")
            {
                bIsMatch = IsPositive_Number(strpay);
                if (bIsMatch == false)
                {
                    throw new System.ArgumentException("金額應為正整數。");
                }
                else
                {
                    if (strpay.Length > 9)
                    {
                        throw new System.ArgumentException("金額超過位數限制。");
                    }
                }
            }

            int pay = int.Parse(strpay);

            char[] cBarcode1, cBarcode2;
            char[] cDate, CMoney;
            string strCheckSum1, strCheckSum2;

            cBarcode1 = barcode1.ToUpper().ToCharArray(0, barcode1.Length);
            CharLoop(cBarcode1);
            cBarcode2 = barcode2.ToUpper().ToCharArray(0, barcode2.Length);
            CharLoop(cBarcode2);
            cDate = date.ToString("MMdd").ToCharArray(0, date.ToString("MMdd").Length);
            CharLoop(cDate);
            CMoney = pay.ToString().PadLeft(9, '0').ToCharArray(0, 9);
            CharLoop(CMoney);

            switch (ichecksum1)
            {
                case 0:
                    strCheckSum1 = "A";
                    break;
                case 10:
                    strCheckSum1 = "B";
                    break;
                default:
                    strCheckSum1 = ichecksum1.ToString();
                    break;
            }

            switch (ichecksum2)
            {
                case 0:
                    strCheckSum2 = "X";
                    break;
                case 10:
                    strCheckSum2 = "Y";
                    break;
                default:
                    strCheckSum2 = ichecksum2.ToString();
                    break;
            }
            ichecksum1 = 0;
            ichecksum2 = 0;
            return date.ToString("MMdd") + strCheckSum1 + strCheckSum2 + pay.ToString().PadLeft(9, '0');
        }

        private void CharLoop(char[] value)
        {
            for (int iCount = 0; iCount < value.Length; iCount++)
            {
                //偶數字串
                if (iCount % 2 > 0)
                {
                    ichecksum2 = GetCheckSum(value[iCount], ichecksum2);
                }
                //奇數字串
                else
                {
                    ichecksum1 = GetCheckSum(value[iCount], ichecksum1);
                }
            }
        }

        private int GetCheckSum(char value, int checksum)
        {
            if (Char.IsNumber(value))
            {
                //是數字
                return (checksum + ((int)value - 48)) % 11;
            }
            else
            {
                //是英文
                switch (value)
                {
                    case 'A':
                        return (checksum + 1) % 11;
                    case 'B':
                        return (checksum + 2) % 11;
                    case 'C':
                        return (checksum + 3) % 11;
                    case 'D':
                        return (checksum + 4) % 11;
                    case 'E':
                        return (checksum + 5) % 11;
                    case 'F':
                        return (checksum + 6) % 11;
                    case 'G':
                        return (checksum + 7) % 11;
                    case 'H':
                        return (checksum + 8) % 11;
                    case 'I':
                        return (checksum + 9) % 11;
                    case 'J':
                        return (checksum + 1) % 11;
                    case 'K':
                        return (checksum + 2) % 11;
                    case 'L':
                        return (checksum + 3) % 11;
                    case 'M':
                        return (checksum + 4) % 11;
                    case 'N':
                        return (checksum + 5) % 11;
                    case 'O':
                        return (checksum + 6) % 11;
                    case 'P':
                        return (checksum + 7) % 11;
                    case 'Q':
                        return (checksum + 8) % 11;
                    case 'R':
                        return (checksum + 9) % 11;
                    case 'S':
                        return (checksum + 2) % 11;
                    case 'T':
                        return (checksum + 3) % 11;
                    case 'U':
                        return (checksum + 4) % 11;
                    case 'V':
                        return (checksum + 5) % 11;
                    case 'W':
                        return (checksum + 6) % 11;
                    case 'X':
                        return (checksum + 7) % 11;
                    case 'Y':
                        return (checksum + 8) % 11;
                    case 'Z':
                        return (checksum + 9) % 11;
                    case '+':
                        return (checksum + 1) % 11;
                    case '%':
                        return (checksum + 2) % 11;
                    case '-':
                        return (checksum + 6) % 11;
                    case '.':
                        return (checksum + 7) % 11;
                    case ' ':
                        return (checksum + 8) % 11;
                    case '$':
                        return (checksum + 9) % 11;
                    case '/':
                        return (checksum + 0) % 11;
                    default:
                        return (checksum + 0) % 11;
                }
                return (checksum + ((((int)value - 65) % 9) + 1) % 11);
            }
        }

        //統計摘要(理應只一筆資料)
        private Item GetMUsersSummary(TConfig cfg)
        {
            string sql = @"
                SELECT
                    '{#in_cmt_name}'  AS 'in_current_org'
                    , sum(in_total)   AS 'expenses'
                    , count(*)        AS 'in_qty'
                FROM
                    IN_CLA_MEETING_USER_DEGREE_FEE WITH(NOLOCK)
                WHERE
					in_cla_meeting = '{#meeting_id}'
					AND in_cmt_sno = '{#in_cmt_sno}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_cmt_name}", cfg.login_resume_name)
                .Replace("{#in_cmt_sno}", cfg.login_resume_sno);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //報名者資料
        private Item GetMUsers(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.in_total AS 'in_expense'
					, t1.in_muid
					, t2.id
					, t2.in_gameunit
					, t2.in_name
					, t2.in_sno
					, t2.in_l1
					, t2.in_l2
					, t2.in_l3
					, t2.in_index
					, t2.in_section_name
					, t2.in_l1_sort
					, t2.in_regdate
                FROM
                    IN_CLA_MEETING_USER_DEGREE_FEE t1 WITH(NOLOCK)
				INNER JOIN
					IN_CLA_MEETING_USER t2 WITH(NOLOCK)
					ON t2.id = t1.in_muid
                WHERE
					in_cla_meeting = '{#meeting_id}'
					AND in_cmt_sno = '{#in_cmt_sno}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_cmt_name}", cfg.login_resume_name)
                .Replace("{#in_cmt_sno}", cfg.login_resume_sno);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //報名者道館彙總資料
        private Item GetOrgs(TConfig cfg)
        {
            string sql = @"
                SELECT
					in_creator_sno
					, in_creator
                    , sum(in_total)   AS 'in_expense'
                    , count(*)        AS 'in_qty'
                FROM
                    IN_CLA_MEETING_USER_DEGREE_FEE WITH(NOLOCK)
                WHERE
					in_cla_meeting = '{#meeting_id}'
					AND in_cmt_sno = '{#in_cmt_sno}'
				GROUP BY
					in_creator_sno
					, in_creator
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_cmt_sno}", cfg.login_resume_sno);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public bool isMeetingAdmin { get; set; }
            public bool isCommittee { get; set; }
            public bool isGymOwner { get; set; }
            public bool isGymAssistant { get; set; }


            public string meeting_id { get; set; }


            public Item itmLogin { get; set; }
            public string login_resume_group { get; set; }
            public string login_resume_name { get; set; }
            public string login_resume_sno { get; set; }
            public string login_manager_name { get; set; }


            /// <summary>
            /// 活動資訊 (IN_MEETING OR IN_CLA_MEETING)
            /// </summary>
            public Item itmMeeting { get; set; }

            /// <summary>
            /// 時程資訊 (IN_MEETING_Functiontime OR IN_CLA_MEETING_Functiontime)
            /// </summary>
            public Item itmFunctiontime { get; set; }

            /// <summary>
            /// 活動 ItemType
            /// </summary>
            public string MtType { get; set; }

            /// <summary>
            /// 與會者 ItemType
            /// </summary>
            public string MUserType { get; set; }

            /// <summary>
            /// 選項時程 ItemType
            /// </summary>
            public string MFuncType { get; set; }

            /// <summary>
            /// 繳費單 meeting id 欄位
            /// </summary>
            public string MtProperty { get; set; }



            /// <summary>
            /// 繳費單 Identity id
            /// </summary>
            public string pay_identity_id { get; set; }

            /// <summary>
            /// 繳費單 建單者姓名
            /// </summary>
            public string pay_creator_name { get; set; }

            /// <summary>
            /// 繳費單 建單者身分證號
            /// </summary>
            public string pay_creator_sno { get; set; }

            /// <summary>
            /// 繳費單 建單者所屬群組
            /// </summary>
            public string pay_creator_group { get; set; }

            /// <summary>
            /// 繳費單 建單者所屬委員會
            /// </summary>
            public string pay_committee { get; set; }


            /// <summary>
            /// 活動類型
            /// </summary>
            public string in_meeting_type { get; set; }

            /// <summary>
            /// 活動編號
            /// </summary>
            public string in_meeting_code { get; set; }

            /// <summary>
            /// 收款單位編號
            /// </summary>
            public string in_receice_org_code { get; set; }

            /// <summary>
            /// 最後報名日期
            /// </summary>
            public string in_imf_date_e { get; set; }

            /// <summary>
            /// 最後繳費期限
            /// </summary>
            public string last_payment_date { get; set; }

            /// <summary>
            /// 最後繳費期限
            /// </summary>
            public string due_date { get; set; }

            /// <summary>
            /// 隊職員數
            /// </summary>
            public int in_staffs { get; set; }

            /// <summary>
            /// 選手人數
            /// </summary>
            public int in_players { get; set; }

            /// <summary>
            /// 項目總計
            /// </summary>
            public int in_items { get; set; }

            /// <summary>
            /// 費用
            /// </summary>
            public int in_expense { get; set; }
        }

        private class TModel
        {
            public string blank { get; set; }
            public string payment_mode { get; set; }
            public string index_number { get; set; }

            public string codes { get; set; }
            public string project_code { get; set; }

            public string xls_money { get; set; }
            public string xls_current_org { get; set; }
            public string xls_real_stuff { get; set; }
            public string xls_real_player { get; set; }
            public string xls_real_items { get; set; }

            public string barcode2 { get; set; }
            public string barcode2_1 { get; set; }
            public string B1 { get; set; }
            public string B2 { get; set; }
            public string B3 { get; set; }
            public DateTime CurrentTime { get; set; }
            public string AddTime { get; set; }

            public string b2_1 { get; set; }
            public string b2_2 { get; set; }
            public string index_number_prefix { get; set; }
        }

        private string GetDtmStr(string value, string format = "yyyy-MM-dd", int hours = 0)
        {
            if (value == null || value == "") return "";
            return GetDtm(value, hours).ToString(format);
        }

        private DateTime GetDtm(string value, int hours = 0)
        {
            if (value == null || value == "") return DateTime.Now;

            var result = DateTime.Now;
            if (DateTime.TryParse(value.Replace("/", "-"), out result))
            {
                return result.AddHours(hours);
            }
            else
            {
                return DateTime.Now;
            }
        }

        private int GetInt(string value, int defV = 0)
        {
            if (value == null || value == "") return 0;

            int result = defV;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return defV;
        }
    }
}