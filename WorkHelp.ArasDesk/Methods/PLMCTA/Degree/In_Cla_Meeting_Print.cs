﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Degree
{
    public class In_Cla_Meeting_Print : Item
    {
        public In_Cla_Meeting_Print(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 帳號歷史紀錄
                輸入: 
                   - user id
                日誌:
                   - 2024.12.30 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]QueryLoginNameHistory";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                id = itmR.getProperty("id", ""),
                login_name = itmR.getProperty("login_name", ""),
                scene = itmR.getProperty("scene", ""),
            };

            if (cfg.scene == "") cfg.scene = "page";


            switch (cfg.scene)
            {
                case "page":
                    Page(cfg, itmR);
                    break;

                case "query_user_id":
                    QueryUserId(cfg, itmR);
                    break;

            }

            return itmR;
        }

        private void QueryUserId(TConfig cfg, Item itmReturn)
        {
            cfg.itmPermit = cfg.inn.applyMethod("In_CheckIdentity", "<method>" + cfg.strMethodName + "</method><code>ALL</code>");
            cfg.isMeetingAdmin = cfg.itmPermit.getProperty("isMeetingAdmin", "") == "1";
            if (!cfg.isMeetingAdmin) throw new Exception("無權限");

            cfg.itmUser = cfg.inn.applySQL("SELECT id, login_name FROM [USER] WITH(NOLOCK) WHERE login_name = '" + cfg.login_name + "'");
            if (cfg.itmUser.getResult() == "") throw new Exception("查無帳號");

            itmReturn.setProperty("query_user_id", cfg.itmUser.getProperty("id", ""));
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            //取得登入者權限
            cfg.itmPermit = cfg.inn.applyMethod("In_CheckIdentity", "<method>" + cfg.strMethodName + "</method><code>ALL</code>");
            cfg.isMeetingAdmin = cfg.itmPermit.getProperty("isMeetingAdmin", "") == "1";
            if (!cfg.isMeetingAdmin)
            {
                itmReturn.setProperty("login_name", "無權限");
                return;
            }

            cfg.itmUser = cfg.inn.applySQL("SELECT id, login_name, last_name FROM [USER] WITH(NOLOCK) WHERE id = '" + cfg.id + "'");
            if (cfg.itmUser.getResult() == "")
            {
                itmReturn.setProperty("login_name", "查無帳號");
                return;
            }

            cfg.last_name = cfg.itmUser.getProperty("last_name", "");
            cfg.login_name = cfg.itmUser.getProperty("login_name", "");
            itmReturn.setProperty("login_name", cfg.login_name + " " + cfg.last_name);

            var items1 = GetPlayerRecordItems(cfg);
            var items2 = GetSeminarRecordItems(cfg);
            AppendItems(cfg, items1, "inn_player", "選手或隊職員", "比賽", itmReturn);
            AppendItems(cfg, items2, "inn_trainee", "學員或成員", "講習", itmReturn);
        }

        private void AppendItems(TConfig cfg, Item items, string inn_type, string inn_desc, string inn_note, Item itmReturn)
        {
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_l1 = item.getProperty("in_l1", "");
                var in_l2 = item.getProperty("in_l2", "");
                var in_l3 = item.getProperty("in_l3", "");
                var in_expense = item.getProperty("in_expense", "");
                var in_paynumber = item.getProperty("in_paynumber", "");
                var pay_bool = item.getProperty("pay_bool", "");
                var created_on = item.getProperty("created_on", "");

                created_on = created_on.Replace(" ", "<BR>");

                var sect_info = in_l1 + " / " + in_l2 + " / " + in_l3;

                var pay_info = in_paynumber;
                if (pay_info == "") pay_info = "無繳費單";
                var expense = GetInt(in_expense);
                if (expense > 0)
                {
                    pay_info += "<BR>" + in_expense + GetPayStatus(cfg, pay_bool);
                }

                item.setType(inn_type);
                item.setProperty("no", (i + 1).ToString());
                item.setProperty("inn_sect", sect_info);
                item.setProperty("inn_desc", inn_desc);
                item.setProperty("inn_note", inn_note);
                item.setProperty("created_on", created_on);
                item.setProperty("inn_pay", pay_info);
                itmReturn.addRelationship(item);
            }
        }

        private string GetPayStatus(TConfig cfg, string pay_bool)
        {
            switch (pay_bool)
            {
                case "已繳費": return " / <span class='bg-success'>已繳費</span>";
                default: return pay_bool;
            }
        }

        private Item GetPlayerRecordItems(TConfig cfg)
        {
            var sql = @"
                SELECT
	                t1.id
	                , t1.in_title
	                , t2.in_name
	                , t2.in_l1
	                , t2.in_l2
	                , t2.in_l3
	                , t2.in_index
	                , t2.in_expense
	                , t2.in_paynumber
	                , t3.pay_bool
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, t2.created_on), 120) AS 'created_on'
                FROM
	                IN_MEETING t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_USER t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                LEFT OUTER JOIN
	                IN_MEETING_PAY t3 WITH(NOLOCK)
	                ON t3.in_meeting = t1.id
	                AND t3.item_number = t2.in_paynumber
                WHERE
	                t2.in_sno = '{#login_name}'
                ORDER BY
	                t1.created_on
            ";
            sql = sql.Replace("{#login_name}", cfg.login_name);

            return cfg.inn.applySQL(sql);
        }

        private Item GetSeminarRecordItems(TConfig cfg)
        {
            var sql = @"
                SELECT
	                t1.id
	                , t1.in_title
	                , t2.in_name
	                , t2.in_l1
	                , t2.in_committee	AS 'in_l2'
	                , t2.in_degree_area	AS 'in_l3'
	                , t2.in_l3
	                , t2.in_index
	                , t2.in_expense
	                , t2.in_paynumber
	                , t3.pay_bool
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, t2.created_on), 120) AS 'created_on'
                FROM
	                IN_CLA_MEETING t1 WITH(NOLOCK)
                INNER JOIN
	                IN_CLA_MEETING_USER t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                LEFT OUTER JOIN
	                IN_MEETING_PAY t3 WITH(NOLOCK)
	                ON t3.in_cla_meeting = t1.id
	                AND t3.item_number = t2.in_paynumber
                WHERE
	                t2.in_sno = '{#login_name}'
                ORDER BY
	                t1.created_on


            ";
            sql = sql.Replace("{#login_name}", cfg.login_name);

            return cfg.inn.applySQL(sql);
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public Item itmPermit { get; set; }
            public Item itmUser { get; set; }
            public bool isMeetingAdmin { get; set; }
            public string id { get; set; }
            public string login_name { get; set; }
            public string last_name { get; set; }
            public string scene { get; set; }
        }

        private int GetInt(string value)
        {
            if (value == "") return 0;
            int result = 0;
            int.TryParse(value, out result);
            return result;
        }
    }
}