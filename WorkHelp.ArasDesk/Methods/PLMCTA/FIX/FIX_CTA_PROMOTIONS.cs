﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class FIX_CTA_PROMOTIONS : Item
    {
        public FIX_CTA_PROMOTIONS(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 啟用帳號
    日誌: 
        - 2023-02-04: 調整 (lina)
        - 2022-04-01: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "FIX_CTA_PROMOTIONS_" + System.DateTime.Now.ToString("yyyyMMdd");

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                resume_id = itmR.getProperty("resume_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "enable_gym_members":
                    EnableGymMembers(cfg, itmR);
                    break;

                case "enable_one_member":
                    if (cfg.resume_id != "")
                    {
                        EnableOneMember(cfg, itmR);
                    }
                    break;

                case "disabled_one_member":
                    if (cfg.resume_id != "")
                    {
                        DisableOneMember(cfg, itmR);
                    }
                    break;
            }

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private void EnableOneMember(TConfig cfg, Item itmReturn)
        {
            Item itmOldResume = cfg.inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE id = '" + cfg.resume_id + "'");
            //string resume_id = item.getProperty("id", "");
            string in_sno = itmOldResume.getProperty("in_sno", "");
            string in_name = itmOldResume.getProperty("in_name", "");
            string in_password_plain = itmOldResume.getProperty("in_password_plain", "");
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "啟用: " + in_name + "(" + in_sno + "), OLD PWD: " + in_password_plain);

            EnableUser(cfg, itmOldResume, itmReturn);
        }

        private void EnableUser(TConfig cfg, Item itmOldResume, Item itmReturn)
        {
            string resume_id = itmOldResume.getProperty("id", "");
            string in_sno = itmOldResume.getProperty("in_sno", "");
            string in_temp_memo = itmOldResume.getProperty("in_temp_memo", "");
            string in_password_plain = itmOldResume.getProperty("in_password_plain", "");
            //if (in_temp_memo == "error") return;

            if (!IsValidSid(cfg, itmOldResume, in_sno))
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName + "_ERR", "EnableUser 身分證號不合格: " + in_sno);
                LogError(cfg, itmOldResume);
                return;
            }

            string pwd = in_sno.Length > 4 ? in_sno.Substring(in_sno.Length - 4, 4) : "1234";
            if (in_password_plain != "")
            {
                //Resume 已有密碼
                pwd = in_password_plain;
            }

            cfg.NewPwd = pwd;
            cfg.NewPwd_md5 = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(cfg.NewPwd, "md5");
            cfg.SequenceVal = itmOldResume.getProperty("in_sno", itmOldResume.getProperty("login_name", ""));
            cfg.login_name = itmOldResume.getProperty("login_name", "");
            cfg.in_user_id = "";
            cfg.identity_id = "";

            //建立使用者
            MergeUser(cfg, itmOldResume);
            if (cfg.in_user_id == "") return;

            //建立角色
            MergeIdentity(cfg, itmOldResume);
            if (cfg.identity_id == "") return;

            //成員角色
            MergeResumeResume(cfg, itmOldResume);

            //建立與會者
            MergeMeetingUser(cfg, itmOldResume);

            //更新講師履歷
            UpdateResume(cfg, itmOldResume);

        }

        //建立使用者
        private void MergeUser(TConfig cfg, Item itmOldResume)
        {
            try
            {
                string sql_qry = "SELECT id FROM [USER] WITH(NOLOCK)"
                + " WHERE login_name = '" + cfg.login_name + "'";

                Item itmOld = cfg.inn.applySQL(sql_qry);
                if (!itmOld.isError() && itmOld.getResult() != "")
                {
                    cfg.in_user_id = itmOld.getProperty("id", "");
                    return;
                }

                //建立使用者
                Item itmUser = cfg.inn.newItem("User", "merge");
                itmUser.setAttribute("where", "login_name='" + itmOldResume.getProperty("login_name", "") + "'");
                itmUser.setProperty("last_name", itmOldResume.getProperty("in_name", ""));//姓
                itmUser.setProperty("first_name", cfg.SequenceVal);//名
                itmUser.setProperty("user_no", cfg.SequenceVal);//員工編號
                itmUser.setProperty("login_name", itmOldResume.getProperty("login_name"));//登入帳號
                itmUser.setProperty("cell", itmOldResume.getProperty("in_tel", ""));//行動電話
                itmUser.setProperty("email", itmOldResume.getProperty("in_email", ""));//電子郵件
                itmUser.setProperty("in_company", itmOldResume.getProperty("in_company", ""));//公司別
                itmUser.setProperty("logon_enabled", "1");//可登入
                itmUser.setProperty("in_rank", cfg.inn.getItemByKeyedName("in_rank", "SYS-000000").getID());//職級
                itmUser.setProperty("in_app_page", "pages/c.aspx?page=UserDashboard.html&method=in_user_dashboard");
                itmUser.setProperty("password", cfg.NewPwd_md5);

                itmUser = itmUser.apply();
                if (!itmUser.isError() && itmUser.getResult() != "")
                {
                    cfg.in_user_id = itmUser.getID();
                }
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName + "_ERR", "MergeUser Error: " + ex.Message);
                LogError(cfg, itmOldResume);
            }
        }

        //建立角色
        private void MergeIdentity(TConfig cfg, Item itmOldResume)
        {
            try
            {
                Item itmIdentity = cfg.inn.newItem("Identity", "get");
                itmIdentity.setProperty("in_user", cfg.in_user_id);
                itmIdentity = itmIdentity.apply();

                cfg.identity_id = itmIdentity.getID();

                //MeetingUser
                Item itmMember;
                itmMember = cfg.inn.newItem("Member", "merge");
                itmMember.setAttribute("where", "source_id='95277C725C954FEE959DCE2073DA1F9C' and related_id='" + cfg.identity_id + "'");
                itmMember.setProperty("source_id", "95277C725C954FEE959DCE2073DA1F9C");
                itmMember.setRelatedItem(itmIdentity);
                itmMember = itmMember.apply();

                //All Employees
                itmMember = cfg.inn.newItem("Member", "merge");
                itmMember.setAttribute("where", "source_id='B32BD81D1AD04207BF1E61E39A4E0E13' and related_id='" + cfg.identity_id + "'");
                itmMember.setProperty("source_id", "B32BD81D1AD04207BF1E61E39A4E0E13");
                itmMember.setRelatedItem(itmIdentity);
                itmMember = itmMember.apply();

                //此為申請館主之用,要把報名者加入 7AC8B33DA0F246DDA70BB244AB231E29 ACT_GymOwner
                itmMember = cfg.inn.newItem("Member", "merge");
                itmMember.setAttribute("where", "source_id='7AC8B33DA0F246DDA70BB244AB231E29' and related_id='" + cfg.identity_id + "'");
                itmMember.setProperty("source_id", "7AC8B33DA0F246DDA70BB244AB231E29");
                itmMember.setRelatedItem(itmIdentity);
                itmMember = itmMember.apply();
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName + "_ERR", "MergeIdentity Error: " + ex.Message);
                LogError(cfg, itmOldResume);
            }
        }

        //成員角色
        private void MergeResumeResume(TConfig cfg, Item itmOldResume)
        {
            try
            {
                string role_remark = "會員";

                //self
                Item itmResumeRole = cfg.inn.newItem("In_Resume_Resume", "merge");
                itmResumeRole.setAttribute("where", "source_id='" + cfg.resume_id + "' and related_id='" + cfg.resume_id + "'");
                itmResumeRole.setProperty("source_id", cfg.resume_id);
                itmResumeRole.setProperty("related_id", cfg.resume_id);
                itmResumeRole.setProperty("in_resume_role", "sys_9999");
                itmResumeRole.setProperty("in_resume_remark", role_remark);
                itmResumeRole = itmResumeRole.apply();
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName + "_ERR", "MergeResumeResume Error: " + ex.Message);
                LogError(cfg, itmOldResume);
            }
        }

        //建立與會者
        private void MergeMeetingUser(TConfig cfg, Item itmOldResume)
        {
            try
            {
                string meeting_id = "249FDB244E534EB0AA66C8E9C470E930";
                string birth = GetWestBirthDay(itmOldResume.getProperty("in_birth", ""));

                string sql_qry = "SELECT id FROM IN_MEETING_USER WITH(NOLOCK)"
                    + " WHERE source_id = '" + meeting_id + "'"
                    + " AND in_mail = '" + cfg.login_name + "'";

                Item itmOld = cfg.inn.applySQL(sql_qry);
                if (!itmOld.isError() && itmOld.getResult() != "")
                {
                    return;
                }

                Item itmMUser = cfg.inn.newItem("In_Meeting_User", "merge");
                itmMUser.setAttribute("where", "source_id='" + meeting_id + "' AND in_mail='" + cfg.login_name + "'");
                itmMUser.setProperty("source_id", meeting_id);
                itmMUser.setProperty("in_name", itmOldResume.getProperty("in_name", ""));
                itmMUser.setProperty("in_sno", itmOldResume.getProperty("login_name", ""));
                itmMUser.setProperty("in_sno_display", GetSidDisplay(itmOldResume.getProperty("login_name", "")));
                itmMUser.setProperty("in_mail", itmOldResume.getProperty("login_name", ""));
                itmMUser.setProperty("in_email", itmOldResume.getProperty("in_email", ""));
                itmMUser.setProperty("in_gender", itmOldResume.getProperty("in_gender", ""));
                itmMUser.setProperty("in_birth", birth);
                itmMUser.setProperty("in_note_state", "official");
                itmMUser.setProperty("in_confirm_mail", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
                itmMUser.setProperty("in_regdate", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
                itmMUser.setProperty("in_tel", itmOldResume.getProperty("in_tel", ""));
                itmMUser.setProperty("in_creator", "lwu001");
                itmMUser.setProperty("in_creator_sno", "lwu001");
                itmMUser.setProperty("in_current_org", itmOldResume.getProperty("in_name", ""));
                itmMUser.setProperty("in_group", itmOldResume.getProperty("in_name", ""));
                itmMUser.setProperty("in_role", "player");
                itmMUser.setProperty("in_index", "00001");
                itmMUser.setProperty("in_add", itmOldResume.getProperty("in_resident_add", ""));

                itmMUser = itmMUser.apply();
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName + "_ERR", "MergeMeetingUser Error: " + ex.Message);
                LogError(cfg, itmOldResume);
            }
        }

        //更新講師履歷
        private void UpdateResume(TConfig cfg, Item itmOldResume)
        {
            try
            {
                string sql_update = "UPDATE IN_RESUME SET"
                        + " password = '" + cfg.NewPwd_md5 + "'"
                        + ", in_password_plain = '" + cfg.NewPwd + "'"
                        + ", in_user_id = '" + cfg.in_user_id + "'"
                        + ", owned_by_id = '" + cfg.identity_id + "'"
                        + ", in_is_teacher = '1'"
                        + ", in_need_sync = 0"
                        + ", in_temp_memo = 'enabled'"
                        + " WHERE id = '" + cfg.resume_id + "'";

                cfg.inn.applySQL(sql_update);
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName + "_ERR", "UpdateResume Error: " + ex.Message);
                LogError(cfg, itmOldResume);
            }
        }

        private void DisableOneMember(TConfig cfg, Item itmReturn)
        {
            Item itmOldResume = cfg.inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE id = '" + cfg.resume_id + "'");

            //string resume_id = item.getProperty("id", "");
            string in_sno = itmOldResume.getProperty("in_sno", "");
            string in_name = itmOldResume.getProperty("in_name", "");
            string in_user_id = itmOldResume.getProperty("in_user_id", "");
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "停用: " + in_name + "(" + in_sno + ")");

            string sql = "";
            Item itmSQL = null;

            if (in_user_id != "")
            {
                sql = "UPDATE [USER] SET logon_enabled = 0 WHERE id = '" + in_user_id + "'";
                itmSQL = cfg.inn.applySQL(sql);
            }

            sql = "UPDATE IN_RESUME SET in_is_teacher = '0' WHERE id = '" + cfg.resume_id + "'";
            itmSQL = cfg.inn.applySQL(sql);
        }

        private void EnableGymMembers(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            sql = @"
                SELECT 
	                 TOP 1 *
                FROM 
	                IN_RESUME WITH(NOLOCK)
	            WHERE
	                in_need_sync = 1
            ";

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "過檔開始 _# 總筆數: " + count);

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                string resume_id = item.getProperty("id", "");
                string in_name = item.getProperty("in_name", "");
                string in_sno = item.getProperty("in_sno", "");
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, (i + 1) + ". " + in_name + "(" + in_sno + ")");

                EnableUser(cfg, item, itmReturn);
            }

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "過檔結束.");
        }

        private void LogError(TConfig cfg, Item itmReturn)
        {
            cfg.inn.applySQL("UPDATE IN_RESUME SET in_need_sync = 0, in_temp_memo = 'error' WHERE id = '" + cfg.resume_id + "'");
            itmReturn.setProperty("is_error", "1");
        }

        private string GetWestBirthDay(string value)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value.Replace("/", "-"), out dt))
            {
                return dt.AddHours(8).ToString("yyyy-MM-ddTHH:mm:ss");
            }
            else
            {
                return value;
            }
        }

        //檢查身分證號
        private bool IsValidSid(TConfig cfg, Item itmOldResume, string value)
        {
            if (value == null || value == "") return false;

            try
            {

                var result = false;
                var timesArr = new int[] { 1, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
                var idSno = value.Trim();

                if (string.IsNullOrWhiteSpace(idSno)) return result;

                if (idSno.Length == 0) return result;
                if (idSno.Length != 10) return result;

                var a = new[] { 10, 11, 12, 13, 14, 15, 16, 17, 34, 18, 19, 20, 21, 22, 35, 23, 24, 25, 26, 27, 28, 29, 32, 30, 31, 33 };
                var b = new int[11];

                b[1] = a[(idSno[0]) - 65] % 10;

                var c = b[0] = a[(idSno[0]) - 65] / 10;

                for (var i = 1; i <= 9; i++)
                {
                    b[i + 1] = idSno[i] - 48;
                    c += b[i] * (10 - i);
                }

                if (((c % 10) + b[10]) % 10 == 0)
                {
                    result = true;
                }

                return result;
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName + "_ERR", "IsValidSid Error: " + ex.Message);
                LogError(cfg, itmOldResume);
                return false;
            }
        }

        //身分證字號(外顯)
        private string GetSidDisplay(string in_sno)
        {
            if (in_sno.Length == 10)
            {
                //10碼(身分證處理)
                string in_sno_1 = in_sno.Substring(0, 2);
                string in_sno_2 = in_sno.Substring(6, 4);
                return in_sno_1 + "****" + in_sno_2;//A1****6789
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper _InnH { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string resume_id { get; set; }
            public string scene { get; set; }

            public string NewPwd { get; set; }
            public string NewPwd_md5 { get; set; }
            public string SequenceVal { get; set; }
            public string login_name { get; set; }
            public string in_user_id { get; set; }
            public string identity_id { get; set; }
        }
    }
}