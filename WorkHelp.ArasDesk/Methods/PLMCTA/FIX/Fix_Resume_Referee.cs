﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.FIX
{
    public class Fix_Resume_Referee : Item
    {
        public Fix_Resume_Referee(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 修正裁判講習資料
                    - IR 拉到國際
                    - P 拉到品勢
                說明:
                    - 1. 檢查來源欄位與目標欄位
                    - 2. 檢查資料轉移後的筆數是否相符
                日誌: 
                    - 2022-06-27: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "Fix_Resume_Referee";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),
            };

            //lR拉到國際
            Item itmReferees = GetResumeReferees(cfg, "IR");
            AddResumeReferees(cfg, itmReferees, "");

            return itmR;
        }

        private void AddResumeReferees(TConfig cfg, Item itmList, string type_name)
        {
            int count = itmList.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmSource = itmList.getItemByIndex(i);
                AddResumeReferee(cfg, itmSource, type_name);
                RemoveResumeReferee(cfg, itmSource);

            }
        }

        private void RemoveResumeReferee(TConfig cfg, Item itmSource)
        {
            string id = itmSource.getProperty("id", "");
            Item itmOld = cfg.inn.newItem("IN_RESUME_REFEREE", "delete");
            itmOld.setAttribute("where", "id = '" + id + "'");
            itmOld = itmOld.apply();
        }

        private void AddResumeReferee(TConfig cfg, Item itmSource, string type_name)
        {
            List<string> list = new List<string>();
            list.Add("source_id");
            list.Add("in_seminar_type");
            list.Add("in_date");
            list.Add("in_year");
            list.Add("in_level");
            list.Add("in_echelon");
            list.Add("in_know_score");
            list.Add("in_tech_score");
            list.Add("in_evaluation");
            list.Add("in_note");
            list.Add("in_certificate");
            list.Add("in_certificate_no");
            list.Add("in_hours");
            list.Add("in_has_muser");
            list.Add("in_user");
            list.Add("in_cla_user");
            list.Add("in_old_no");

            Item itmNew = cfg.inn.newItem(type_name, "add");

            foreach (var pro in list)
            {
                itmNew.setProperty(pro, itmSource.getProperty(pro, ""));
            }

            itmNew = itmNew.apply();
        }

        private Item GetResumeReferees(TConfig cfg, string in_level)
        {
            string sql = @"
                SELECT 
	                t1.id AS 'resume_id'
	                , t1.in_name
	                , t1.in_sno
	                , t2.source_id 
	                , t2.in_seminar_type 
	                , CONVERT(VARCHAR, DATEADD(hour, 8, t2.in_date), 120) AS 'in_date'
	                , t2.in_year 
	                , t2.in_level 
	                , t2.in_echelon 
	                , t2.in_know_score 
	                , t2.in_tech_score 
	                , t2.in_evaluation 
	                , t2.in_note 
	                , t2.in_certificate 
	                , t2.in_certificate_no 
	                , t2.in_hours 
	                , t2.in_has_muser 
	                , t2.in_user 
	                , t2.in_cla_user 
	                , t2.in_old_no 
                FROM 
	                IN_RESUME t1 WITH(NOLOCK) 
                INNER JOIN 
	                IN_RESUME_REFEREE t2 WITH(NOLOCK) 
	                ON t2.source_id = t1.id
                WHERE
	                ISNULL(t2.in_level, '') = '{#in_level}'
                ORDER BY
	                t1.in_sno
	                , t2.in_year
            ";

            sql = sql.Replace("{#in_level}", in_level);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }
        }
    }
}
