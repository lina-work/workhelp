﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.FIX
{
    public class In_CTA_Promotion2 : Item
    {
        public In_CTA_Promotion2(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 中華跆協-晉段資料中繼檔
                日期: 
                    - 2022/01/19: 晉段紀錄重整 (lina)
                    - 2021/01/12: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Cta_Promotion2";

            Item itmR = this;

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                scene = itmR.getProperty("scene", ""),
                rawCount = itmR.getProperty("rawCount", "1"),
            };

            switch (cfg.scene)
            {
                case "scene":
                    Run(cfg, itmR);
                    break;

                default:
                    Page(cfg, itmR);
                    break;
            }

            return itmR;

        }

        private void Run(TConfig cfg, Item itmReturn)
        {
            Item items = GetNotEnabledUser(cfg, 1);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                string resume_id = item.getProperty("resume_id", "");
                string in_name = item.getProperty("in_name", "");
                string in_sno = item.getProperty("in_sno", "");

                string msg = (i + 1) + ". " + in_name + "(" + in_sno + ")";

                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "    - " + msg);

                Item itmData = cfg.inn.newItem("In_Resume");
                itmData.setProperty("resume_id", resume_id);
                itmData.setProperty("scene", "enable_one_member");

                Item itmDataResult = itmData.apply("FIX_CTA_PROMOTIONS");

                if (itmDataResult.isError())
                {
                    throw new Exception(msg + "啟用失敗");
                }
            }
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            Item items = GetNotEnabledUser(cfg, 20);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setType("inn_resume");
                itmReturn.addRelationship(item);
            }
        }

        private Item GetNotEnabledUser(TConfig cfg, int topCount)
        {
            string sql = @"
                SELECT TOP {#topCount}
                    t1.id AS 'resume_id'
                    , t1.in_name
	                , t1.in_sno
	                , t1.in_gender
	                , DATEADD(hour, 8, t1.in_birth) AS 'in_birth'
	                , t1.in_tel
	                , t1.in_password_plain
	                , t1.in_member_type
	                , t1.in_is_teacher
                FROM 
	                IN_RESUME t1 WITH(NOLOCK)
                WHERE
	                t1.in_is_teacher = 0
	                AND t1.in_birth < '2004-12-31 16:00:00'
                ORDER BY
	                t1.in_birth DESC
	        ";

            //     string sql = @"
            //         SELECT TOP {#topCount}
            //             t1.id AS 'resume_id'
            //             , t1.in_name
            //         , t1.in_gender
            //         , DATEADD(hour, 8, t1.in_birth) AS 'in_birth'
            //         , t1.in_sno
            //         , t1.in_tel
            //         , t1.in_password_plain
            //         , t1.in_member_type
            //         , t1.in_is_teacher
            //         FROM 
            //         IN_RESUME t1 WITH(NOLOCK)
            //         LEFT OUTER JOIN
            //         (
            //         SELECT t101.id, t102.in_number FROM [Member] t101 WITH(NOLOCK)
            //         INNER JOIN [IDENTITY] t102 WITH(NOLOCK) ON t102.id = t101.related_id 
            //         WHERE t101.source_id='7AC8B33DA0F246DDA70BB244AB231E29'
            //         ) t2 ON t2.in_number = t1.in_sno
            //         WHERE
            //         t2.id IS NULL
            //         AND t1.in_birth < '2004-12-31 16:00:00'
            //         ORDER BY
            //         t1.in_birth DESC
            // ";

            sql = sql.Replace("{#topCount}", topCount.ToString());

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string scene { get; set; }
            public string rawCount { get; set; }
        }
    }
}