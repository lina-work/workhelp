﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class FIX_CTA_PROMOTIONS_FOR_DEGREE : Item
    {
        public FIX_CTA_PROMOTIONS_FOR_DEGREE(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "FIX_CTA_PROMOTIONS_FOR_DEGREE" + System.DateTime.Now.ToString("yyyyMMdd_HHmm");

            string sql = @"
SELECT TOP 200
    t11.muser_name, t11.muser_sno, t12.id, t12.in_name, t12.in_sno 
FROM
(
	SELECT DISTINCT t2.in_name AS 'muser_name', t2.in_sno AS 'muser_sno'
	FROM IN_CLA_MEETING t1 WITH(NOLOCK) 
	INNER JOIN IN_CLA_MEETING_USER t2 WITH(NOLOCK) 
	ON t2.source_id = t1.id
	WHERE t1.in_meeting_type = 'degree'
	AND t1.id = 'A6C1757E10384AF1816390BC0FF46075'
)t11
LEFT OUTER JOIN
	IN_RESUME t12 WITH(NOLOCK) 
	ON t12.in_sno = t11.muser_sno
WHERE
	ISNULL(t12.in_is_teacher, 0) = 0
ORDER BY
	t12.in_sno
";

            Item items = inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string muser_name = item.getProperty("muser_name", "").Trim();
                string muser_sno = item.getProperty("muser_sno", "").Trim();

                string resume_id = item.getProperty("id", "").Trim();
                string in_name = item.getProperty("in_name", "").Trim();
                string in_sno = item.getProperty("in_sno", "").Trim();

                if (in_sno == "")
                {
                    CCO.Utilities.WriteDebug("ERR_" + strMethodName, (i + 1).ToString() + "/" + count.ToString() + ": " + muser_name + " " + muser_sno);
                    continue;
                }

                CCO.Utilities.WriteDebug(strMethodName, (i + 1).ToString() + "/" + count.ToString() + ": " + in_name + " " + in_sno);

                if (resume_id == "") continue;

                item.setType("In_Resume");
                item.setProperty("resume_id", resume_id);
                item.setProperty("scene", "enable_one_member");
                item.apply("FIX_CTA_PROMOTIONS");
            }

            return this;
        }
    }
}