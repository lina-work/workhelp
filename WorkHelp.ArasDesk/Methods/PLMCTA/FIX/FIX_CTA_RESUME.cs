﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.FIX
{
    public class FIX_CTA_RESUME : Item
    {
        public FIX_CTA_RESUME(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 整體修正
                日誌: 
                    - 2023-02-08: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            //Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "FIX_CTA_RESUME";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                scene = itmR.getProperty("scene", ""),
            };

            cfg.scene = "all_check_resume";
            cfg.min = 1;
            cfg.max = 1000;

            switch (cfg.scene)
            {
                case "all_check_resume":
                    AllCheckResume(cfg, itmR);
                    break;

                case "second_check_sid":
                    SecondCheckSid(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void AllCheckResume(TConfig cfg, Item itmReturn)
        {
            var items = GetRangeResumeItems(cfg, cfg.min, cfg.max);
            var count = items.getItemCount();
            if (count <= 0) return;

            var mt_id = "249FDB244E534EB0AA66C8E9C470E930";

            var log = new StringBuilder();
            log.AppendLine();
            log.AppendLine(System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " | ALLCHECK.START: " + count);

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var person = MapPerson(cfg, item);

                var info = person.name + "(" + person.sno + ")";
                var errs = new List<string>();

                log.AppendLine(System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " |     - " + info);

                var non_members = GetNonMemberList(cfg, cfg.strIdentityId);
                if (non_members != "")
                {
                    errs.Add(" - LOSS " + non_members);
                    MergeIdentity(cfg, cfg.strIdentityId);
                }

                var itmMUser = cfg.inn.applySQL("SELECT id FROM IN_MEETING_USER WITH(NOLOCK) WHERE source_id = '" + mt_id + "' AND in_mail = '" + person.login_name + "'");
                if (itmMUser.isError() || itmMUser.getResult() == "")
                {
                    errs.Add(" - LOSS In_Meeting_User");
                    MergeMeetingUser(cfg, person);
                }

                var itmResumeResume = cfg.inn.applySQL("SELECT id FROM IN_RESUME_RESUME WITH(NOLOCK) WHERE source_id = '" + person.id + "' AND related_id = '" + person.id + "'");
                if (itmResumeResume.isError() || itmResumeResume.getResult() == "")
                {
                    errs.Add(" - LOSS In_Resume_Resume");
                    MergeResumeResume(cfg, person);
                }

                if (person.in_is_teacher != "1")
                {
                    errs.Add(" - LOSS in_is_teacher");
                    cfg.inn.applySQL("UPDATE IN_RESUME SET in_is_teacher = '1' WHERE id = '" + person.id + "'");
                }

                if (errs.Count > 0)
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName + "_ERROR", info + "\r\n" + string.Join("\r\n", errs));
                }
            }

            log.AppendLine(System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " | ALLCHECK.END.");

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, log.ToString());
        }

        //建立與會者
        private void MergeMeetingUser(TConfig cfg, TPerson person)
        {
            try
            {
                Item itmOldResume = cfg.inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE id = '" + person.id + "'");

                string meeting_id = "249FDB244E534EB0AA66C8E9C470E930";
                string birth = GetWestBirthDay(itmOldResume.getProperty("in_birth", ""));

                Item itmMUser = cfg.inn.newItem("In_Meeting_User", "merge");
                itmMUser.setAttribute("where", "source_id='" + meeting_id + "' AND in_mail='" + person.login_name + "'");
                itmMUser.setProperty("source_id", meeting_id);
                itmMUser.setProperty("in_name", itmOldResume.getProperty("in_name", ""));
                itmMUser.setProperty("in_sno", itmOldResume.getProperty("login_name", ""));
                itmMUser.setProperty("in_sno_display", GetSidDisplay(itmOldResume.getProperty("login_name", "")));
                itmMUser.setProperty("in_mail", itmOldResume.getProperty("login_name", ""));
                itmMUser.setProperty("in_email", itmOldResume.getProperty("in_email", ""));
                itmMUser.setProperty("in_gender", itmOldResume.getProperty("in_gender", ""));
                itmMUser.setProperty("in_birth", birth);
                itmMUser.setProperty("in_note_state", "official");
                itmMUser.setProperty("in_confirm_mail", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
                itmMUser.setProperty("in_regdate", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
                itmMUser.setProperty("in_tel", itmOldResume.getProperty("in_tel", ""));
                itmMUser.setProperty("in_creator", "lwu001");
                itmMUser.setProperty("in_creator_sno", "lwu001");
                itmMUser.setProperty("in_current_org", itmOldResume.getProperty("in_name", ""));
                itmMUser.setProperty("in_group", itmOldResume.getProperty("in_name", ""));
                itmMUser.setProperty("in_role", "player");
                itmMUser.setProperty("in_index", "00001");
                itmMUser.setProperty("in_add", itmOldResume.getProperty("in_resident_add", ""));

                itmMUser = itmMUser.apply();
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName + "_ERR", "MergeMeetingUser Error: " + ex.Message);
            }
        }

        //建立角色
        private void MergeIdentity(TConfig cfg, string identity_id)
        {
            try
            {
                Item itmMember;

                //MeetingUser
                itmMember = cfg.inn.newItem("Member", "merge");
                itmMember.setAttribute("where", "source_id='95277C725C954FEE959DCE2073DA1F9C' and related_id='" + identity_id + "'");
                itmMember.setProperty("source_id", "95277C725C954FEE959DCE2073DA1F9C");
                itmMember.setProperty("related_id", identity_id);
                itmMember = itmMember.apply();

                //All Employees
                itmMember = cfg.inn.newItem("Member", "merge");
                itmMember.setAttribute("where", "source_id='B32BD81D1AD04207BF1E61E39A4E0E13' and related_id='" + identity_id + "'");
                itmMember.setProperty("source_id", "B32BD81D1AD04207BF1E61E39A4E0E13");
                itmMember.setProperty("related_id", identity_id);
                itmMember = itmMember.apply();

                //此為申請館主之用,要把報名者加入 7AC8B33DA0F246DDA70BB244AB231E29 ACT_GymOwner
                itmMember = cfg.inn.newItem("Member", "merge");
                itmMember.setAttribute("where", "source_id='7AC8B33DA0F246DDA70BB244AB231E29' and related_id='" + identity_id + "'");
                itmMember.setProperty("source_id", "7AC8B33DA0F246DDA70BB244AB231E29");
                itmMember.setProperty("related_id", identity_id);
                itmMember = itmMember.apply();
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName + "_ERR", "MergeIdentity Error: " + ex.Message);
            }
        }

        //成員角色
        private void MergeResumeResume(TConfig cfg, TPerson person)
        {
            try
            {
                string role_remark = "會員";

                //self
                Item itmResumeRole = cfg.inn.newItem("In_Resume_Resume", "merge");
                itmResumeRole.setAttribute("where", "source_id='" + person.id + "' and related_id='" + person.id + "'");
                itmResumeRole.setProperty("source_id", person.id);
                itmResumeRole.setProperty("related_id", person.id);
                itmResumeRole.setProperty("in_resume_role", "sys_9999");
                itmResumeRole.setProperty("in_resume_remark", role_remark);
                itmResumeRole = itmResumeRole.apply();
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName + "_ERR", "MergeResumeResume Error: " + ex.Message);
            }
        }

        private string GetNonMemberList(TConfig cfg, string idt_id)
        {
            var list = new List<string>();
            var items = GetNonMembers(cfg, idt_id);
            var count = items.getItemCount();

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var keyed_name = item.getProperty("keyed_name", "");
                list.Add(keyed_name);
            }

            return string.Join("、", list);
        }

        private void SecondCheckSid(TConfig cfg, Item itmReturn)
        {
            var items = GetErrorResumeItems(cfg);
            var count = items.getItemCount();
            if (count <= 0) return;

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var person = MapPerson(cfg, item);

                //清除檢測紀錄
                RemoveCheckLog(cfg, person);

                if (person.gender == "")
                {
                    person.is_error = true;
                    person.errs.Add("無性別");
                }

                //檢查身分證號
                if (!person.is_error)
                {
                    RunCheckSid(cfg, person);
                }

                if (!person.is_error)
                {
                    if (person.is_taiwan_sid)
                    {
                        CheckTaiwanSno(person);
                    }
                    else if (person.is_resident_sid_old)
                    {
                        CheckResidentSnoOld(person);
                    }
                    else if (person.is_resident_sid_new)
                    {
                        CheckResidentSnoNew(person);
                    }
                }

                //新增檢測紀錄
                AddCheckLog(cfg, person);

                if (!person.is_error)
                {
                    cfg.inn.applySQL("UPDATE IN_RESUME SET in_temp_memo = 'can_fix' WHERE id = '" + person.id + "'");
                }
            }
        }

        private void RunCheckSid(TConfig cfg, TPerson person)
        {
            //檢查格式A~Z(1) 0~9(9)(台灣身分證)
            var regex_1 = new System.Text.RegularExpressions.Regex("^[A-Z]{1}[0-9]{9}$");
            //檢查格式A~Z(1)A~Z(2) 0~9(8)(居留證)
            var regex_2 = new System.Text.RegularExpressions.Regex("^[A-Z]{1}[A-Z]{1}[0-9]{8}$");
            //檢查格式A~Z(1)3~0(2) 0~9(8)(居留證)
            var regex_3 = new System.Text.RegularExpressions.Regex("^[A-Z]{1}[89]{1}[0-9]{8}$");
            //檢查格式A~Z(1)A~Z(2) 0~9(6)(外國護照)
            var regex_4 = new System.Text.RegularExpressions.Regex("^[A-Z]{1}[A-Z]{1}[0-9]{6}$");

            var len = person.sno.Length;
            if (len == 8)
            {
                if (regex_4.IsMatch(person.sno))
                {
                    person.is_passport = true;
                }
                else
                {
                    person.is_error = true;
                    person.errs.Add("未通過外國護照正規檢核");
                }
            }
            else if (len == 10)
            {
                if (regex_1.IsMatch(person.sno))
                {
                    person.is_taiwan_sid = true;
                }
                else if (regex_2.IsMatch(person.sno))
                {
                    person.is_resident_sid_old = true;
                }
                else if (regex_3.IsMatch(person.sno))
                {
                    person.is_resident_sid_new = true;
                }
                else
                {
                    person.is_error = true;
                    person.errs.Add("未通過身分證、居留證新舊版正規檢核");
                }
            }
            else
            {
                person.is_error = true;
                person.errs.Add("SID長度錯誤");
            }
        }

        //檢查身分證號
        private void CheckTaiwanSno(TPerson person)
        {
            //存放檢查碼之外的數字
            int[] seed = new int[10];
            //字母陣列
            string[] charMapping = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "X", "Y", "W", "Z", "I", "O" };

            string sno = person.sno;
            string city_code = sno.Substring(0, 1);
            string gender_code = sno.Substring(1, 1);
            string gender_text = "";

            //依據身分證第2碼判定性別
            if (gender_code == "1")
            {
                gender_text = "男";
            }
            else if (gender_code == "2")
            {
                gender_text = "女";
            }

            if (person.gender != gender_text)
            {
                person.is_error = true;
                person.errs.Add("身分證字號: 性別錯誤");
            }

            for (int index = 0; index < charMapping.Length; index++)
            {
                if (charMapping[index] == city_code)
                {
                    index += 10;
                    seed[0] = index / 10;
                    seed[1] = (index % 10) * 9;
                    break;
                }
            }
            for (int index = 2; index < 10; index++)
            {
                seed[index] = Convert.ToInt32(sno.Substring(index - 1, 1)) * (10 - index);
            }

            //檢查是否為正確的身分證(依據身分證檢查規則)
            var check_code = Convert.ToInt32(sno.Substring(9, 1));
            if ((10 - (seed.Sum() % 10)) % 10 != check_code)
            {
                person.is_error = true;
                person.errs.Add("身分證字號: 格式錯誤");
            }
        }

        private void CheckResidentSnoOld(TPerson person)
        {
            //存放檢查碼之外的數字
            int[] seed = new int[10];

            //字母陣列
            string[] charMapping = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "X", "Y", "W", "Z", "I", "O" };

            string sno = person.sno;
            string city_code = sno.Substring(0, 1);
            string gender_code = sno.Substring(1, 1);
            string gender_text = "";

            string sex = "";
            string nationality = "";
            char[] strArr = sno.ToCharArray(); // 字串轉成char陣列

            int verifyNum = 0;
            int[] pidResidentFirstInt = { 1, 10, 9, 8, 7, 6, 5, 4, 9, 3, 2, 2, 11, 10, 8, 9, 8, 7, 6, 5, 4, 3, 11, 3, 12, 10 };
            char[] pidCharArray = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

            // 第一碼
            verifyNum += pidResidentFirstInt[Array.BinarySearch(pidCharArray, strArr[0])];
            // 原居留證第二碼英文字應轉換為10~33，並僅取個位數*6，這裡直接取[(個位數*6) mod 10]
            int[] pidResidentSecondInt = { 0, 8, 6, 4, 2, 0, 8, 6, 2, 4, 2, 0, 8, 6, 0, 4, 2, 0, 8, 6, 4, 2, 6, 0, 8, 4 };
            // 第二碼
            verifyNum += pidResidentSecondInt[Array.BinarySearch(pidCharArray, strArr[1])];
            // 第三~八碼
            for (int i = 2, j = 7; i < 9; i++, j--)
            {
                verifyNum += Convert.ToInt32(strArr[i].ToString(), 10) * j;
            }

            // 檢查碼
            verifyNum = (10 - (verifyNum % 10)) % 10;
            bool ok = verifyNum == Convert.ToInt32(strArr[9].ToString(), 10);
            if (ok)
            {
                if (strArr[1] == 'A' || strArr[1] == 'C')
                {
                    gender_text = "男";
                }
                else if (strArr[1] == 'B' || strArr[1] == 'D')
                {
                    gender_text = "女";
                }

                if (person.gender != gender_text)
                {
                    person.is_error = true;
                    person.errs.Add("舊居留證: 性別錯誤");
                }
            }
            else
            {
                person.is_error = true;
                person.errs.Add("舊居留證: 格式錯誤");
            }
        }

        private void CheckResidentSnoNew(TPerson person)
        {
            //存放檢查碼之外的數字
            int[] seed = new int[10];

            //字母陣列
            string[] charMapping = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "X", "Y", "W", "Z", "I", "O" };

            string sno = person.sno;
            string city_code = sno.Substring(0, 1);
            string gender_code = sno.Substring(1, 1);
            string gender_text = "";

            //依據身分證第2碼判定性別
            if (gender_code == "8")
            {
                gender_text = "男";
            }
            else if (gender_code == "9")
            {
                gender_text = "女";
            }

            if (person.gender != gender_text)
            {
                person.is_error = true;
                person.errs.Add("新居留證: 性別錯誤");
            }

            for (int index = 0; index < charMapping.Length; index++)
            {
                if (charMapping[index] == city_code)
                {
                    index += 10;
                    seed[0] = index / 10;
                    seed[1] = (index % 10) * 9;
                    break;
                }
            }
            for (int index = 2; index < 10; index++)
            {
                seed[index] = GetInt(sno.Substring(index - 1, 1)) * (10 - index);
            }

            //檢查是否為正確的身分證(依據身分證檢查規則)
            if ((10 - (seed.Sum() % 10)) % 10 != GetInt(sno.Substring(9, 1)))
            {
                person.is_error = true;
                person.errs.Add("新居留證: 格式錯誤");
            }
        }

        private TPerson MapPerson(TConfig cfg, Item item)
        {
            var result = new TPerson
            {
                id = item.getProperty("id", ""),
                name = item.getProperty("in_name", "").Trim().ToUpper(),
                sno = item.getProperty("in_sno", "").Trim().ToUpper(),
                gender = item.getProperty("in_gender", "").Trim().ToUpper(),
                birth = item.getProperty("in_birth", "").Trim().ToUpper(),
                login_name = item.getProperty("login_name", "").Trim().ToUpper(),
                in_is_teacher = item.getProperty("in_is_teacher", "").Trim().ToUpper(),
                rno = item.getProperty("rno", "").Trim().ToUpper(),
                in_user_id = item.getProperty("in_user_id", "").Trim(),
                owned_by_id = item.getProperty("owned_by_id", "").Trim(),
                is_taiwan_sid = false,
                is_resident_sid_old = false,
                is_resident_sid_new = false,
                is_passport = false,
                errs = new List<string>(),
            };

            return result;
        }

        private Item GetNonMembers(TConfig cfg, string idt_id)
        {
            string sql = @"
                SELECT 
                    * 
                FROM 
	                VU_MEMBER_DEF_ROLES
                WHERE
	                keyed_name NOT IN
	                (
		                SELECT 
			                t2.keyed_name
		                FROM 
			                MEMBER t1 WITH(NOLOCK) 
		                INNER JOIN 
			                [IDENTITY] t2 WITH(NOLOCK) ON t2.id = t1.source_id 
		                WHERE 
			                t1.related_id = '{#idt_id}'
	                )
            ";

            sql = sql.Replace("{#idt_id}", idt_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetRangeResumeItems(TConfig cfg, int min, int max)
        {
            string sql = @"
			    SELECT 
				    *
			    FROM
			    (
                    SELECT 
                        id
                        , in_name
                        , in_sno
	                    , in_gender
	                    , CONVERT(VARCHAR, DATEADD(HOUR, 8, in_birth), 111) AS 'in_birth'
					    , login_name
					    , ISNULL(in_is_teacher, '') AS 'in_is_teacher'
					    , in_user_id
					    , owned_by_id
						, in_member_type
					    , ROW_NUMBER()OVER (ORDER BY login_name) AS 'rno'
                    FROM 
	                    IN_RESUME WITH(NOLOCK)
                    WHERE
					    ISNULL(in_temp_memo, '') <> 'error'
						AND ISNULL(in_name, '') <> ''
						AND ISNULL(in_birth, '') <> ''
						AND LEN(in_sno) = 10
						AND in_member_type NOT IN ('area_cmt', 'asc', 'vip_gym')
			    ) t1
			    WHERE 
				    t1.rno BETWEEN {#min} AND {#max} 
            ";

            sql = sql.Replace("{#min}", min.ToString())
                .Replace("{#max}", max.ToString());

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetErrorResumeItems(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                id
	                , in_name
	                , in_sno
	                , in_gender
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, in_birth), 111) AS 'in_birth'
                FROM 
	                IN_RESUME WITH(NOLOCK)
                WHERE 
	                ISNULL(in_temp_memo, '') = 'error'
            ";

            return cfg.inn.applySQL(sql);
        }

        private void RemoveCheckLog(TConfig cfg, TPerson person)
        {
            string sql = "DELETE FROM In_Resume_ErrData WHERE in_resume_id = '" + person.id + "'";
            cfg.inn.applySQL(sql);
        }

        private void AddCheckLog(TConfig cfg, TPerson person)
        {
            if (person.errs.Count == 0) return;

            var itmNew = cfg.inn.newItem("In_Resume_ErrData", "add");
            itmNew.setProperty("in_resume_id", person.id);
            itmNew.setProperty("in_name", person.name);
            itmNew.setProperty("in_sno", person.sno);
            itmNew.setProperty("in_gender", person.gender);
            itmNew.setProperty("in_birth", person.birth);
            itmNew.setProperty("in_note", string.Join("；", person.errs));
            itmNew.apply();
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }
            public string scene { get; set; }
            public int min { get; set; }
            public int max { get; set; }
        }

        private class TPerson
        {
            public string id { get; set; }
            public string name { get; set; }
            public string sno { get; set; }
            public string gender { get; set; }
            public string birth { get; set; }

            public string login_name { get; set; }
            public string in_is_teacher { get; set; }
            public string rno { get; set; }

            public string in_user_id { get; set; }
            public string owned_by_id { get; set; }

            public bool is_taiwan_sid { get; set; }
            public bool is_resident_sid_old { get; set; }
            public bool is_resident_sid_new { get; set; }
            public bool is_passport { get; set; }
            public bool is_error { get; set; }
            public List<string> errs { get; set; }
        }

        //身分證字號(外顯)
        private string GetSidDisplay(string in_sno)
        {
            if (in_sno.Length == 10)
            {
                //10碼(身分證處理)
                string in_sno_1 = in_sno.Substring(0, 2);
                string in_sno_2 = in_sno.Substring(6, 4);
                return in_sno_1 + "****" + in_sno_2;//A1****6789
            }
            else
            {
                return "";
            }
        }

        private string GetWestBirthDay(string value)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value.Replace("/", "-"), out dt))
            {
                return dt.AddHours(8).ToString("yyyy-MM-ddTHH:mm:ss");
            }
            else
            {
                return value;
            }
        }

        private int GetInt(string value)
        {
            if (value == "") return 0;
            var result = 0;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return 0;
        }
    }
}