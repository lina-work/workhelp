﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using System.Web.UI;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.帳號
{
    internal class QueryLoginNameHistory : Item
    {
        public QueryLoginNameHistory(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 帳號歷史紀錄
                輸入: 
                   - user id
                日誌:
                   - 2024.12.30 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]QueryLoginNameHistory";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                id = itmR.getProperty("id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            if (cfg.scene == "") cfg.scene = "page";

            //取得登入者權限
            cfg.itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>ALL</code>");
            cfg.isMeetingAdmin = cfg.itmPermit.getProperty("isMeetingAdmin", "") == "1";
            if (!cfg.isMeetingAdmin)
            {
                itmR.setProperty("inn_page_message", "無權限");
                return itmR;
            }

            cfg.itmUser = inn.applySQL("SELECT id, login_name FROM [USER] WITH(NOLOCK) WHERE id = '" + cfg.id + "'");
            if (cfg.itmUser.getResult() == "")
            {
                itmR.setProperty("inn_page_message", "查無帳號");
                return itmR;
            }

            cfg.in_sno = cfg.itmUser.getProperty("login_name", "");

            switch(cfg.scene)
            {
                case "page":
                    Page(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            var items1 = GetPlayerRecordItems(cfg);
            AppendItems(cfg, items1, "inn_player", "選手或隊職員", "比賽", itmReturn);
        }

        private void AppendItems (TConfig cfg, Item items, string inn_type, string inn_desc, string inn_note, Item itmReturn)
        {
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType(inn_type);
                item.setProperty("inn_desc", inn_desc);
                item.setProperty("inn_note", inn_note);
                itmReturn.addRelationship(item);
            }
        }

        private Item GetPlayerRecordItems(TConfig cfg)
        {
            var sql = @"
                SELECT
	                t1.id
	                , t1.in_title
	                , t2.in_name
	                , t2.in_l1
	                , t2.in_l2
	                , t2.in_l3
	                , t2.in_index
	                , t2.in_expense
	                , t2.in_paynumber
	                , t3.pay_bool
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, t2.created_on), 120) AS 'created_on'
                FROM
	                IN_MEETING t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_USER t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                LEFT OUTER JOIN
	                IN_MEETING_PAY t3 WITH(NOLOCK)
	                ON t3.in_meeting = t1.id
	                AND t3.item_number = t2.in_paynumber
                WHERE
	                t2.in_sno = '{#in_sno}'
                ORDER BY
	                t1.created_on
            ";
            sql = sql.Replace("{#in_sno}", cfg.in_sno);

            return cfg.inn.applySQL(sql);
        }
        
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public Item itmPermit { get; set; }
            public Item itmUser { get; set; }
            public bool isMeetingAdmin { get; set; }
            public string id { get; set; }
            public string in_sno { get; set; }
            public string scene { get; set; }
        }

    }
}