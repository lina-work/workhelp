﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aras.IOM;
using Aras.IOME.ConflictDetection;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Game
{
    public class In_MeetingRegistryResult : Item
    {
        public In_MeetingRegistryResult(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 提供正取的報名清單
                位置: In_MeetingRegistryResult.html
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_MeetingRegistryResult";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                pay_number = itmR.getProperty("pay_number", ""),
                org_photo = itmR.getProperty("org_photo", ""),
                CurrentTime = System.DateTime.Today,
            };

            cfg.itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            cfg.isMeetingAdmin = cfg.itmPermit.getProperty("isMeetingAdmin", "") == "1";
            cfg.isGymOwner = cfg.itmPermit.getProperty("isGymOwner", "") == "1";
            cfg.isGymAssistant = cfg.itmPermit.getProperty("isGymAssistant", "") == "1";

            //無權限
            if (!cfg.isMeetingAdmin && !cfg.isGymOwner && !cfg.isGymAssistant)
            {
                return itmR;
            }

            cfg.itmLogin = cfg.inn.newItem("In_Resume", "get");
            cfg.itmLogin.setProperty("in_user_id", cfg.strUserId);
            cfg.itmLogin = cfg.itmLogin.apply();

            if (cfg.itmLogin.isError() || cfg.itmLogin.getResult() == "")
            {
                return itmR;
            }

            //登入者資訊
            cfg.login_group = cfg.itmLogin.getProperty("in_group", "");

            //賽事名稱
            Item itmGameName = cfg.inn.applySQL("SELECT in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = 'meeting_game_name'");
            if (itmGameName.getResult() != "")
            {
                cfg.GameName = itmGameName.getProperty("in_value", "");
                cfg.IsJudo = cfg.GameName == "judo";
            }
            else
            {
                cfg.GameName = "";
                cfg.IsJudo = false;
            }

            //賽事報名項目
            cfg.itmSurveyLevel = GetSurveyLevels(cfg);
            cfg.user_order_by = cfg.itmSurveyLevel.getProperty("user_order_by", "");

            //賽事資訊
            itmR = inn.applyAML("<AML><Item type='In_Meeting' action='get' id='" + cfg.meeting_id + "'></Item></AML>");

            string today = cfg.CurrentTime.ToString("yyyy-MM-dd");
            cfg.in_pay_mode = itmR.getProperty("in_pay_mode", "");
            cfg.in_isfull = itmR.getProperty("in_isfull", "");
            cfg.in_date_s = itmR.getProperty("in_state_time_start", today);//開始時間
            cfg.in_state_time = itmR.getProperty("in_state_time_end", today);//結束時間
            cfg.Meeting_Time_s = Convert.ToDateTime(cfg.in_date_s);//將開始時間轉型
            cfg.Meeting_Time_e = Convert.ToDateTime(cfg.in_state_time);//將結束時間轉型
            cfg.in_verify_mode = itmR.getProperty("in_verify_mode", "0");
            cfg.verify_mode = GetIntVal(cfg.in_verify_mode);
            cfg.in_member_type = itmR.getProperty("in_member_type", "");
            cfg.is_team_bill = cfg.in_member_type == "團隊計費";

            //是否為共同講師
            cfg.is_in_teacher = IsInTeacher(cfg);

            if (cfg.isMeetingAdmin || cfg.is_in_teacher)
            {
                cfg.is_gym = false;
            }
            else
            {
                cfg.is_gym = cfg.isGymOwner || cfg.isGymAssistant;
            }

            //附加條件
            cfg.group_condition = cfg.is_gym ? "AND t1.in_group = N'" + cfg.login_group + "'" : "";
            cfg.paynumber_condition = cfg.pay_number != "" ? "AND ISNULL(in_paynumber, '') = N'" + cfg.pay_number + "'" : "";


            //道館送出報名狀態
            cfg.gym_is_closed = "";
            cfg.gym_really_closed = "";
            if (cfg.is_gym)
            {
                cfg.gym_is_closed = "0";
                cfg.gym_really_closed = "0";
                cfg.is_gym_closed = IsGymClosed(cfg);
                if (cfg.is_gym_closed)
                {
                    cfg.gym_is_closed = "1";
                    cfg.gym_really_closed = "1";
                }
            }

            //賽事狀態判定(丟給前台)
            //尚未開始
            if (System.DateTime.Now < cfg.Meeting_Time_s)//今日<開始
            {
                cfg.gym_is_closed = "1";
            }
            //額滿
            else if (cfg.in_isfull == "1")//已額滿 == 1
            {
                cfg.gym_is_closed = "1";
            }
            //報名中
            else if (System.DateTime.Now < cfg.Meeting_Time_e && System.DateTime.Now > cfg.Meeting_Time_s)//今日<結束&&今日>開始
            {

            }
            //報名截止
            else if (System.DateTime.Now > cfg.Meeting_Time_e)//今日>結束
            {
                cfg.gym_is_closed = "1";
            }

            cfg.like_closed = cfg.gym_is_closed == "1";
            cfg.really_closed = cfg.gym_really_closed == "1";
            cfg.really_full = cfg.in_isfull == "1";

            itmR.setProperty("pay_show", "O");
            itmR.setProperty("gym_is_closed", cfg.gym_is_closed);
            itmR.setProperty("gym_really_closed", cfg.gym_really_closed);

            //單位編輯模式
            cfg.itmOrgMode = GetOrgMode(inn, cfg.org_photo);


            //與會者
            AppendMeetingUsers(cfg, itmR);

            //分組統計
            AppendGroupingStatistics(cfg, itmR);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;

        }

        //與會者
        private void AppendMeetingUsers(TConfig cfg, Item itmReturn)
        {
            cfg.hide_note_col = false;
            cfg.hide_pay_col = true;
            cfg.hide_refund_col = true;

            //無金流模式、土銀模式、QrCode 模式
            if (cfg.in_pay_mode == "2" || cfg.in_pay_mode == "3" || cfg.in_pay_mode == "4")
            {
                cfg.hide_pay_col = false;
            }

            var items = GetMUsers(cfg);
            var list = MapMUsers(cfg, items);

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("  <tr>");
            if (cfg.is_team_bill)
            {
                head.Append("    <th class='mailbox-subject' data-sortable='true'>隊別</th>");
            }
            head.Append("    <th class='mailbox-subject' data-sortable='true'>組名</th>");
            head.Append("    <th class='mailbox-subject' data-sortable='true'>序號</th>");
            head.Append("    <th class='mailbox-subject' data-sortable='true'>姓名</th>");
            head.Append("    <th class='mailbox-subject' data-sortable='true'>身分證字號</th>");
            head.Append("    <th class='mailbox-subject' data-sortable='true'>所屬單位</th>");
            head.Append("    <th class='mailbox-subject' data-sortable='true'>備註</th>");
            head.Append("    <th class='mailbox-subject' style='max-width:1em'>繳費狀態</th>");

            if (!cfg.hide_refund_col)
            {
                head.Append("    <th class='mailbox-subject' data-sortable='true'>申請狀態</th>");
            }

            head.Append("    <th class='mailbox-subject' style='max-width:1em'>功能</th>");
            head.Append("  </tr>");
            head.Append("</thead>");

            body.Append("<tbody>");
            for (int i = 0; i < list.Count; i++)
            {
                var obj = list[i];
                body.Append("<tr id='mu_" + obj.muid + "'");
                body.Append(" data-id='" + obj.muid + "'");
                body.Append(" data-srcid='" + cfg.meeting_id + "'");
                body.Append(" data-keywords='" + "," + obj.in_l1 + "," + "'");
                body.Append(" data-has-stuffs='" + obj.inn_has_stuffs + "'");
                body.Append(" data-in-l1='" + obj.in_l1 + "'");
                body.Append(" data-in-l2='" + obj.in_l2 + "'");
                body.Append(" data-in-l3='" + obj.in_l3 + "'");
                body.Append(" data-pay-number='" + obj.in_paynumber + "'");
                body.Append(" >");

                if (cfg.is_team_bill)
                {
                    body.Append("<td data-label='隊別'>" + obj.in_team + "</td>");
                }
                body.Append("<td data-label='組名'>" + obj.in_section_name + "</td>");
                body.Append("<td data-label='序號'>" + obj.in_index + "</td>");
                body.Append("<td data-label='姓名'>" + NameInfo(cfg, obj) + "</td>");
                body.Append("<td data-label='身分證字號'>" + obj.in_sno_display + "</td>");
                body.Append("<td data-label='所屬單位'>" + OrgInfo(cfg, obj) + "</td>");
                body.Append("<td data-label='備註'>" + obj.inn_note + "</td>");
                body.Append("<td data-label='繳費單號'>" + PayInfo(cfg, obj) + "</td>");

                if (!cfg.hide_refund_col)
                {
                    body.Append("<td data-label='退費申請狀態'>" + obj.in_cancel_status + "</td>");
                }

                body.Append("<td>" + Buttons(cfg, obj) + "</td>");


                body.Append("</tr>");
            }
            body.Append("</tbody>");

            StringBuilder table = new StringBuilder();
            table.Append("<table");
            table.Append(" extractor='In_Meeting_User'");
            table.Append(" id='MeetingMaster_relationship'");
            table.Append(" class='table table-hover table-bordered table-rwd rwd rwdtable'");
            table.Append(" data-search-align='left'");
            table.Append(" data-sort-stable='true'");
            table.Append(" data-search='true'");
            table.Append(" >");
            table.Append(head);
            table.Append(body);
            table.Append("</table>");

            itmReturn.setProperty("inn_table", table.ToString());
        }

        private string Buttons(TConfig cfg, TMUser obj)
        {
            StringBuilder btns = new StringBuilder();

            if (!obj.hide_edit)
            {
                btns.AppendLine("<button class='btn btn-success badge choseme edit_mode' onclick=\"EditUsers('" + obj.muid + "','')\"><i class='fa fa-pencil'></i> 修改</button>");
            }

            if (!obj.hide_delete)
            {
                btns.AppendLine("<button class='btn btn-danger badge edit_mode' onclick=\"DelSameIndexUsers('" + obj.muid + "')\"><i class='fa fa-trash'></i> 刪除整組</button>");
            }

            if (!obj.hide_view)
            {
                btns.AppendLine("<button class='btn btn-primary badge choseme view_mode' onclick=\"EditUsers('" + obj.muid + "', 'viewmode')\"><i class='fa fa-eye'></i> 檢視</button>");
            }

            if (!obj.hide_refund)
            {
                btns.AppendLine("<button class='btn btn-danger badge choseme view_mode' onclick=\"ApplyCancel('" + obj.in_paynumber + "')\"><i class='fa fa-wrench'></i>申請</button>");
            }
            if (!obj.hide_exchange)
            {
                btns.AppendLine("<button class='btn btn-danger badge choseme view_mode' onclick=\"EditUsers('" + obj.muid + "', 'exchange')\"><i class='fa fa-exchange'></i>變更人員</button>");
            }
            return btns.ToString();
        }

        private string PayInfo(TConfig cfg, TMUser obj)
        {
            return "<a href='../pages/c.aspx"
                + "?page=detail_list_n1.html"
                + "&method=In_Payment_DetailList"
                + "&meeting_id=" + cfg.meeting_id
                + "&item_number=" + obj.in_paynumber + "'>"
                + obj.pay_status
                + "</a>";
        }

        private string OrgInfo(TConfig cfg, TMUser obj)
        {
            string upload_mode = cfg.itmOrgMode.getProperty("org_upload_mode", "");
            string text_mode = cfg.itmOrgMode.getProperty("org_text_mode", "");

            return "<a class='" + upload_mode + "'"
                + " onclick=\"show_PhotoUploadModal_org('" + obj.in_sno + "', '" + obj.in_group + "','" + obj.in_current_org + "')\">"
                + obj.in_current_org
                + "</a>"
                + "<label class='" + text_mode + "'>" + obj.in_current_org + "</label>";
        }

        private string NameInfo(TConfig cfg, TMUser obj)
        {
            return "<a onclick=\"show_PhotoUploadModal('" + obj.muid + "', '" + obj.in_sno + "')\">"
                + "<i class='fa fa-cloud-upload in_in_photo1' data-photo='" + obj.in_photo1 + "'></i>"
                + obj.in_name
                + "</a>";
        }

        private List<TMUser> MapMUsers(TConfig cfg, Item items)
        {
            List<TMUser> result = new List<TMUser>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                TMUser obj = new TMUser
                {
                    muid = item.getProperty("id", ""),
                    in_group = item.getProperty("in_group", ""),
                    in_current_org = item.getProperty("in_current_org", ""),
                    in_section_name = item.getProperty("in_section_name", ""),
                    in_l1 = item.getProperty("in_l1", ""),
                    in_l2 = item.getProperty("in_l2", ""),
                    in_l3 = item.getProperty("in_l3", ""),
                    in_index = item.getProperty("in_index", ""),
                    in_team = item.getProperty("in_team", ""),
                    in_name = item.getProperty("in_name", ""),
                    in_sno = item.getProperty("in_sno", ""),
                    in_sno_display = item.getProperty("in_sno_display", ""),

                    in_paynumber = item.getProperty("in_paynumber", ""),
                    in_paytime = item.getProperty("in_paytime", ""),
                    in_pay_photo = item.getProperty("in_pay_photo", ""),

                    in_return_mark = item.getProperty("in_return_mark", ""),
                    in_cancel_status = item.getProperty("in_cancel_status", ""),

                    in_verify_result = item.getProperty("in_verify_result", ""),
                    in_ass_ver_result = item.getProperty("in_ass_ver_result", ""),

                    inn_note = "",
                    inn_stuffs = "",
                    inn_has_stuffs = "0",
                };

                //審核不通過
                obj.is_reject = obj.in_verify_result == "0" || obj.in_ass_ver_result == "0";

                if (obj.in_cancel_status != "")
                {
                    //有申請資訊，不隱藏申請欄位
                    cfg.hide_refund_col = false;
                }

                if (obj.in_paytime != "")
                {
                    //有繳費時間為綠色
                    obj.pay_type_color = "G";
                    obj.pay_status = "<span style='color: green;'> <i class='fa fa-list-alt'></i> 已繳費</span>";
                    obj.pay_time = obj.in_paytime.Split('T')[0];
                }
                else
                {
                    //沒有繳費時間為紅色
                    obj.pay_type_color = "R";

                    obj.pay_status = " ";
                    if (obj.in_pay_photo != "" && obj.in_return_mark == "1")
                    {
                        obj.pay_status = "<span style='color: red;'> <i class='fa fa-list-alt'></i> 審核退回</span>";
                    }
                    else if (obj.in_pay_photo != "")
                    {
                        obj.pay_status = "<span style='color: green;'> <i class='fa fa-list-alt'></i> 審核中</span>";
                    }
                    else if (obj.in_paynumber != "")
                    {
                        obj.pay_status = "<span style='color: red;'> <i class='fa fa-list-alt'></i> 未繳費</span>";
                    }
                }

                if (cfg.IsJudo)
                {
                    obj.is_format = obj.in_l1 == "格式組";

                    string inn_stuffs = GetFormatVal(cfg, obj, item);
                    if (inn_stuffs != "")
                    {
                        obj.inn_stuffs = inn_stuffs;
                        obj.inn_has_stuffs = "1";
                    }
                }

                //協會審核
                if (cfg.verify_mode == 2)
                {
                    obj.inn_note = GetVerifyDisplay(cfg.inn, item);
                }

                obj.hide_edit = true;
                obj.hide_delete = true;
                obj.hide_view = true;
                obj.hide_refund = true;
                obj.hide_exchange = true;

                //額滿，但未關閉單位
                if (cfg.really_full && !cfg.really_closed)
                {
                    if (obj.in_paytime != "")
                    {
                        obj.hide_view = false;
                        if (obj.in_cancel_status == "變更申請通過")
                        {
                            obj.hide_exchange = false;
                        }
                        else
                        {
                            obj.hide_refund = false;
                        }
                    }
                    else if (obj.in_paynumber != "")
                    {
                        obj.hide_view = false;
                    }
                    else
                    {
                        obj.hide_edit = false;
                        obj.hide_delete = false;
                    }
                }
                else if (cfg.like_closed)
                {
                    obj.hide_view = false;
                    if (obj.in_paytime != "")
                    {
                        if (obj.in_cancel_status == "變更申請通過")
                        {
                            obj.hide_exchange = false;
                        }
                        else
                        {
                            obj.hide_refund = false;
                        }
                    }
                }
                else if (obj.in_paytime != "")
                {
                    //已繳費
                    obj.hide_view = false;
                    if (obj.in_cancel_status == "變更申請通過")
                    {
                        obj.hide_exchange = false;
                    }
                    else
                    {
                        obj.hide_refund = false;
                    }
                }
                else if (obj.in_paynumber != "")
                {
                    if (obj.is_reject)
                    {
                        obj.hide_edit = false;
                    }
                    else
                    {
                        //有繳費單未繳款
                        obj.hide_view = false;
                    }
                }
                else
                {
                    obj.hide_edit = false;
                    obj.hide_delete = false;
                }

                result.Add(obj);
            }

            return result;
        }

        private string GetFormatVal(TConfig cfg, TMUser obj, Item item)
        {
            string Stuffs = "";
            if (cfg.IsJudo && obj.is_format)
            {
                string in_exe_a1 = item.getProperty("in_exe_a1", "");
                string in_exe_a2 = item.getProperty("in_exe_a2", "");
                string in_exe_a3 = item.getProperty("in_exe_a3", "");

                if (in_exe_a1 != "") Stuffs += "被施術者姓名:" + in_exe_a1 + ",";
                if (in_exe_a2 != "") Stuffs += "被施術者生日:" + in_exe_a2.Split('T')[0] + ",";
                if (in_exe_a3 != "") Stuffs += "施術者身分證:" + in_exe_a3 + ",";
            }
            return Stuffs;
        }


        private Item GetMUsers(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.*
                    , t2.in_verify_mode
                    , t3.in_pay_photo
                    , t3.in_return_mark
                    , t4.in_cancel_status
                FROM
                    IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                LEFT OUTER JOIN
                    IN_MEETING_PAY t3 WITH(NOLOCK)
                    ON t3.in_meeting = t1.source_id
                    AND t3.item_number = t1.in_paynumber
                LEFT OUTER JOIN
                    IN_MEETING_NEWS t4 WITH(NOLOCK)
                    ON t4.source_id = t3.id
                    AND t4.in_muid = t1.id
                WHERE
                    t1.source_id = '{#meeting_id}'
                    AND t1.in_note_state = 'official'
                    {#group_condition}
                    {#paynumber_condition}
                ORDER BY
                    {#user_order_by}
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#group_condition}", cfg.group_condition)
                .Replace("{#paynumber_condition}", cfg.paynumber_condition)
                .Replace("{#user_order_by}", cfg.user_order_by);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //分組統計
        private void AppendGroupingStatistics(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            string meeting_id = cfg.meeting_id;
            string group_condition = cfg.group_condition.Replace("t1.", "");

            string distinct_cols = cfg.itmSurveyLevel.getProperty("distinct_cols", "");

            //計算各分組的人數( in_l1)

            sql = "SELECT DISTINCT in_l1 FROM IN_MEETING_USER WITH(NOLOCK) WHERE source_id = '{#meeting_id}' AND in_note_state = 'official' {#group_condition}"
                .Replace("{#meeting_id}", meeting_id)
                .Replace("{#group_condition}", group_condition);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + strMethodName);

            Item l1_list = cfg.inn.applySQL(sql);

            Item l1_all = cfg.inn.newItem("inn_l1_group");
            l1_all.setProperty("inn_l1_name", "全部");
            l1_all.setProperty("inn_count", "");
            itmReturn.addRelationship(l1_all);

            sql = "SELECT DISTINCT {#distinct_cols} FROM IN_MEETING_USER WITH(NOLOCK) WHERE source_id = '{#meeting_id}' AND in_note_state = 'official' {#group_condition} AND in_l1 = N'{#in_l1}'";

            int count = l1_list.getItemCount();
            int total = 0;

            for (int i = 0; i < count; i++)
            {
                Item item = l1_list.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");

                string temp_sql = sql.Replace("{#meeting_id}", meeting_id)
                    .Replace("{#group_condition}", group_condition)
                    .Replace("{#in_l1}", in_l1)
                    .Replace("{#distinct_cols}", distinct_cols);

                //CCO.Utilities.WriteDebug(strMethodName, "sql: " + temp_sql);

                Item in_l1_result = cfg.inn.applySQL(temp_sql);
                int in_l1_count = in_l1_result.getItemCount();

                Item l1_option = cfg.inn.newItem("inn_l1_group");
                l1_option.setProperty("inn_l1_name", in_l1);
                l1_option.setProperty("inn_count", in_l1_count.ToString());
                itmReturn.addRelationship(l1_option);

                //累加各類組數
                total = total + in_l1_count;
            }

            l1_all.setProperty("inn_count", total.ToString());

            string l1_hide = cfg.itmSurveyLevel.getProperty("l1_hide", "");
            string l2_hide = cfg.itmSurveyLevel.getProperty("l2_hide", "");
            string l3_hide = cfg.itmSurveyLevel.getProperty("l3_hide", "");

            itmReturn.setProperty("l1_hide", l1_hide);
            itmReturn.setProperty("l2_hide", l2_hide);
            itmReturn.setProperty("l3_hide", l3_hide);

            if (l2_hide == "")
            {
                string l2_values = GetSurveyOptionContents(cfg, "in_l2", group_condition);
                itmReturn.setProperty("l2_values", l2_values);
            }
            if (l3_hide == "")
            {
                string l3_values = GetSurveyOptionContents(cfg, "in_l3", group_condition);
                itmReturn.setProperty("l3_values", l3_values);
            }
        }

        private string GetSurveyOptionContents(TConfig cfg, string in_property, string group_condition)
        {
            string sql = "";

            if (in_property == "in_l2")
            {
                sql = @"
                SELECT t1.in_l1 AS 'in_filter', t1.in_l2 AS 'in_value', count(t1.team_index) AS 'in_count' FROM
                (
                    SELECT 
                        DISTINCT in_l1, in_l2, in_l3, (in_l1 + '-' + ISNULL(in_l2, '') + '-' + ISNULL(in_l3, '') + '-' + in_index) AS 'team_index' 
                    FROM 
                        IN_MEETING_USER WITH(NOLOCK) 
                    WHERE 
                        source_id = '{#meeting_id}'
                        {#group_condition}
                ) t1
                GROUP BY t1.in_l1, t1.in_l2
                ";
            }
            else if (in_property == "in_l3")
            {
                sql = @"
                SELECT t1.in_l1 AS 'in_l1', t1.in_l2 AS 'in_filter', ISNULL(t1.in_l3, '') AS 'in_value', count(t1.team_index) AS 'in_count' FROM
                (
                    SELECT 
                        DISTINCT in_l1, in_l2, in_l3, (in_l1 + '-' + ISNULL(in_l2, '') + '-' + ISNULL(in_l3, '') + '-' + in_index) AS 'team_index' 
                    FROM 
                        IN_MEETING_USER WITH(NOLOCK) 
                    WHERE 
                        source_id = '{#meeting_id}'
                        {#group_condition}
                ) t1
                GROUP BY t1.in_l1, t1.in_l2, t1.in_l3 
                ";
            }

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#group_condition}", group_condition);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                return "";
            }

            return ConvertSurveyOptionContents(items);

        }

        /// <summary>
        /// 將問項明細轉換為字典內容字串
        /// </summary>
        private string ConvertSurveyOptionContents(Item items)
        {
            Dictionary<string, List<string>> dictionary = new Dictionary<string, List<string>>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_filter = item.getProperty("in_filter", "");
                string in_value = item.getProperty("in_value", "");
                string in_count = item.getProperty("in_count", "");

                List<string> list = null;
                if (dictionary.ContainsKey(in_filter))
                {
                    list = dictionary[in_filter];
                }
                else
                {
                    list = new List<string>();
                    dictionary.Add(in_filter, list);
                }

                if (!list.Contains(in_value))
                {
                    list.Add(in_value + "_#" + in_count);
                }
            }

            List<string> list2 = new List<string>();
            foreach (KeyValuePair<string, List<string>> kv in dictionary)
            {
                string key = kv.Key;
                string values = string.Join("@", kv.Value.Where(x => !string.IsNullOrEmpty(x)));
                string row = key + ":" + values;
                list2.Add(row);
            }

            return string.Join("@@", list2.Where(x => !string.IsNullOrEmpty(x)));
        }

        //取得報名問項
        private Item GetSurveyLevels(TConfig cfg)
        {
            Item itmResult = cfg.inn.newItem();

            string sql = @"
                SELECT 
                    t1.*
                FROM 
                    IN_SURVEY t1 WITH(NOLOCK)
                INNER JOIN 
                    IN_MEETING_SURVEYS t2 WITH(NOLOCK) ON t2.related_id = t1.id 
                WHERE 
                    t2.source_id = N'{#meeting_id}' 
                    AND t1.in_client_remove = 0
                    AND t1.in_property IN ('in_l1', 'in_l2', 'in_l3')
                ORDER BY 
                    t2.sort_order
            ";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql_levels: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                return itmResult;
            }

            int count = items.getItemCount();

            string level_fields = "";
            string number_key_level = "";
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_property = item.getProperty("in_property", "").Trim();
                string in_is_nokey = item.getProperty("in_is_nokey", "").Trim();

                if (level_fields != "")
                {
                    level_fields += ",";
                }
                level_fields += in_property;

                if (in_is_nokey == "1")
                {
                    if (number_key_level != "")
                    {
                        number_key_level += ",";
                    }
                    number_key_level += in_property;
                }
            }

            if (level_fields == "")
            {
                level_fields = "in_l1";
            }

            if (number_key_level == "")
            {
                number_key_level = "in_l1";
            }

            string default_cols = "";
            string user_order_by = "";
            string distinct_cols = "";
            string l1_hide = "";
            string l2_hide = "";
            string l3_hide = "";

            if (level_fields.Contains("in_l3"))
            {
                default_cols = "in_l1, in_l2, in_l3";
            }
            else if (level_fields.Contains("in_l2"))
            {
                default_cols = "in_l1, in_l2";
                l3_hide = "item_show_0";

            }
            else if (level_fields.Contains("in_l1"))
            {
                default_cols = "in_l1";
                l2_hide = "item_show_0";
                l3_hide = "item_show_0";
            }
            else
            {
                l1_hide = "item_show_0";
                l2_hide = "item_show_0";
                l3_hide = "item_show_0";
            }

            //定序處理
            if (number_key_level.Contains("in_l3"))
            {
                distinct_cols = "in_l1, in_l2, in_l3, in_index";
                user_order_by = "t1.in_group, t1.in_current_org, t1.in_l1, t1.in_l2, t1.in_l3, t1.in_index, t1.in_name";
            }
            else if (number_key_level.Contains("in_l2"))
            {
                distinct_cols = "in_l1, in_l2, in_index";
                user_order_by = "t1.in_group, t1.in_current_org, t1.in_l1, t1.in_l2, t1.in_index, t1.in_name";
            }
            else if (number_key_level.Contains("in_l1"))
            {
                distinct_cols = "in_l1, in_index";
                user_order_by = "t1.in_group, t1.in_current_org, t1.in_l1, t1.in_index, t1.in_name";
            }

            itmResult.setProperty("meeting_id", cfg.meeting_id);
            //定序欄位
            itmResult.setProperty("number_key_level", number_key_level);
            //預設欄位組合
            itmResult.setProperty("default_cols", default_cols);
            //分組統計欄位組合
            itmResult.setProperty("distinct_cols", distinct_cols);
            //定序欄位組合
            itmResult.setProperty("user_order_by", user_order_by);
            //第一階選單是否隱藏
            itmResult.setProperty("l1_hide", l1_hide);
            //第二階選單是否隱藏
            itmResult.setProperty("l2_hide", l2_hide);
            //第三階選單是否隱藏
            itmResult.setProperty("l3_hide", l3_hide);

            return itmResult;
        }

        //取得共同講師 (判斷現在的登入者是否為此課程的共同講師)
        private bool IsInTeacher(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t2.id
                FROM 
                    In_Meeting_Resumelist t1 WITH(NOLOCK) 
                INNER JOIN 
                    IN_RESUME t2 WITH(NOLOCK) 
                    ON t2.id = t1.related_id    
                WHERE 
                    t1.source_id = '{#meeting_id}' 
                    AND t2.in_user_id = '{#in_user_id}';    
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_user_id}", cfg.strUserId);

            Item item = cfg.inn.applySQL(sql);

            if (item.getResult() != "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //道館是否已送出報名資料
        private bool IsGymClosed(TConfig cfg)
        {
            string sql = "SELECT id FROM In_Meeting_Gymlist WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_group = N'" + cfg.login_group + "'";

            Item item = cfg.inn.applySQL(sql);

            if (item.getResult() != "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //取得單位編輯模式
        private Item GetOrgMode(Innovator inn, string org_photo)
        {
            Item item = inn.newItem();
            if (org_photo == "1")
            {
                item.setProperty("org_upload_mode", "item_show_1");
                item.setProperty("org_text_mode", "item_show_0");
            }
            else
            {
                item.setProperty("org_upload_mode", "item_show_0");
                item.setProperty("org_text_mode", "item_show_1");
            }
            return item;
        }

        private string GetVerifyDisplay(Innovator inn, Item item)
        {
            StringBuilder builder = new StringBuilder();

            string[] properties = new string[]
            {
                "in_verify_mode", //活動審核模式
                "in_verify_result", //委員會審核結果
                "in_verify_memo", //委員會審核說明
                "in_ass_ver_result", //協會審核結果
                "in_ass_ver_memo", //協會審核說明
            };

            foreach (string property in properties)
            {
                builder.Append("<" + property + ">" + item.getProperty(property, "") + "</" + property + ">");
            }

            Item itmResult = inn.applyMethod("In_Verify_Message", builder.ToString());

            return itmResult.getProperty("in_verify_display", "");
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string pay_number { get; set; }
            public string org_photo { get; set; }

            public Item itmPermit { get; set; }
            public Item itmLogin { get; set; }
            public Item itmSurveyLevel { get; set; }
            public Item itmOrgMode { get; set; }

            public bool isMeetingAdmin { get; set; }
            public bool isGymOwner { get; set; }
            public bool isGymAssistant { get; set; }

            public string login_group { get; set; }
            public string user_order_by { get; set; }

            public bool is_in_teacher { get; set; }
            public bool is_gym { get; set; }
            public bool is_gym_closed { get; set; }

            public string gym_is_closed { get; set; }
            public string gym_really_closed { get; set; }

            public DateTime CurrentTime { get; set; }

            public string group_condition { get; set; }
            public string paynumber_condition { get; set; }

            public string in_isfull { get; set; }
            public string in_date_s { get; set; }
            public string in_state_time { get; set; }

            /// <summary>
            /// 付款方式 (1: 報名(不產生繳費單)、2: 繳費(無金流模式)、3: 繳費(土銀模式)、4: 繳費(QrCode模式))
            /// </summary>
            public string in_pay_mode { get; set; }

            /// <summary>
            /// 審核模式 (1:不需審核、2: 協會審核、3: 委員會審核)
            /// </summary>
            public string in_verify_mode { get; set; }
            public int verify_mode { get; set; }

            public string in_member_type { get; set; }
            public bool is_team_bill { get; set; }

            public DateTime Meeting_Time_s { get; set; }
            public DateTime Meeting_Time_e { get; set; }

            public bool hide_note_col { get; set; }
            public bool hide_pay_col { get; set; }
            public bool hide_refund_col { get; set; }

            public string GameName { get; set; }
            public bool IsJudo { get; set; }

            public bool really_closed { get; set; }
            public bool like_closed { get; set; }
            public bool really_full { get; set; }
        }

        private class TMUser
        {
            public string muid { get; set; }
            public string in_sno { get; set; }
            public string in_name { get; set; }
            public string in_section_name { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_index { get; set; }
            public string in_team { get; set; }
            public string in_photo1 { get; set; }
            public string in_sno_display { get; set; }
            public string in_group { get; set; }
            public string in_current_org { get; set; }

            public string in_paynumber { get; set; }
            public string in_paytime { get; set; }
            public string in_pay_photo { get; set; }
            public string in_return_mark { get; set; }
            public string in_cancel_status { get; set; }

            public string pay_type_color { get; set; }
            public string pay_status { get; set; }
            public string pay_time { get; set; }

            public string org_upload_mode { get; set; }
            public string org_text_mode { get; set; }

            public string keywords { get; set; }
            public string inn_stuffs { get; set; }
            public string inn_has_stuffs { get; set; }
            public string inn_note { get; set; }

            public string in_verify_result { get; set; }
            public string in_ass_ver_result { get; set; }

            public bool hide_edit { get; set; }
            public bool hide_delete { get; set; }
            public bool hide_view { get; set; }
            public bool hide_refund { get; set; }
            public bool hide_exchange { get; set; }

            public bool is_format { get; set; }
            public bool is_reject { get; set; }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        private DateTime GetDateTime(string value)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            return result;
        }
    }
}