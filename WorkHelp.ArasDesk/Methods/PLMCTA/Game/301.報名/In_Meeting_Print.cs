﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Game
{
    public class In_Meeting_Print : Item
    {
        public In_Meeting_Print(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 報名清冊
                輸入: 
                   - meeting id
                日誌:
                   - 2020.09.16 改寫 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Meeting_Print";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                exe_type = itmR.getProperty("exe_type", ""),
            };

            //取得登入者權限
            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>ALL</code>");
            cfg.isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            cfg.isCommittee = itmPermit.getProperty("isCommittee", "") == "1";
            cfg.isGymOwner = itmPermit.getProperty("isGymOwner", "") == "1";
            cfg.isGymAssistant = itmPermit.getProperty("isGymAssistant", "") == "1";

            //是否為共同講師
            bool open = GetUserResumeListStatus(cfg);
            if (open) cfg.isMeetingAdmin = true;

            //檢查登入者權限
            if (!cfg.isMeetingAdmin && !cfg.isCommittee && !cfg.isGymOwner && !cfg.isGymAssistant)
            {
                itmR.setProperty("permission_type", "1");
                itmR.setProperty("permission_message", "您無權限瀏覽此頁面");
                return itmR;
            }

            //設定流程
            if (cfg.isMeetingAdmin)
            {
                itmR.setProperty("inn_admin_flow", "");
                itmR.setProperty("inn_committee_flow", "item_show_0");
                itmR.setProperty("inn_user_flow", "item_show_0");
                itmR.setProperty("inn_meeting_admin", "1");
            }
            else if (cfg.isCommittee)
            {
                itmR.setProperty("inn_admin_flow", "item_show_0");
                itmR.setProperty("inn_committee_flow", "");
                itmR.setProperty("inn_user_flow", "item_show_0");
                itmR.setProperty("inn_meeting_admin", "1");
            }
            else
            {
                itmR.setProperty("inn_admin_flow", "item_show_0");
                itmR.setProperty("inn_committee_flow", "item_show_0");
                itmR.setProperty("inn_user_flow", "");
                itmR.setProperty("inn_meeting_admin", "0");
            }

            List<string> cols = new List<string> 
            {
                "in_title",
                "in_customer_tel",
                "in_url",
                "in_register_url",
                "in_pdf",
                "in_need_receipt",
                "in_meeting_type",
                "in_pay_mode",
                "in_allowed_reapply",
                "in_bank_1",
                "in_bank_2",
                "in_bank_account",
                "in_bank_name",
                "in_bank_note",
                "in_verify_mode",
                "in_member_type",
            };
            
            //取得賽事資訊
            string aml = "<AML>" +
                "<Item type='in_meeting' action='get' id='" + cfg.meeting_id + "' select='" + string.Join(",", cols )+ "'>" +
                "</Item></AML>";
            cfg.itmMeeting = inn.applyAML(aml);
            if (IsError(cfg.itmMeeting))
            {
                itmR.setProperty("permission_type", "1");
                itmR.setProperty("permission_message", "查無賽事資訊");
                return itmR;
            }

            cfg.in_meeting_type = cfg.itmMeeting.getProperty("in_meeting_type", "");
            cfg.in_member_type = cfg.itmMeeting.getProperty("in_member_type", "");
            cfg.is_team_bill = cfg.in_member_type == "團隊計費";

            //取得登入者資訊
            cfg.itmLogin = inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = '" + cfg.strUserId + "'");
            if (IsError(cfg.itmLogin))
            {
                itmR.setProperty("permission_type", "1");
                itmR.setProperty("permission_message", "查無登入者資訊");
                return itmR;
            }

            cfg.in_creator_sno = cfg.itmLogin.getProperty("in_sno", "");

            //取得單位結束報名狀態
            Item itmGymClosed = GetMeetingGymList(cfg);
            if (!itmGymClosed.isError() && itmGymClosed.getItemCount() > 0)
            {
                itmR.setProperty("inn_gym_is_closed", "1");
                itmR.setProperty("inn_gym_real_closed", "1");
            }

            //取得問項資訊
            Item itmLevels = GetSurveyLevels(cfg);
            string user_order_by = itmLevels.getProperty("user_order_by", "");

            if (cfg.in_meeting_type == "seminar")
            {
                cfg.exe_type = "item";
            }

            if (cfg.exe_type == "item")
            {
                user_order_by = "in_l1, in_l2, in_l3, in_current_org, in_index, in_name";
            }

            //取得與會者資訊
            Item items = GetMeetingUsers(cfg, user_order_by);
            if (IsError(items, isSingle: false))
            {
                // itmR.setProperty("permission_type", "1");
                // itmR.setProperty("permission_message", "查無報名資訊");
                // return itmR;
            }

            //裝箱
            TBox box = MapMeeting(cfg);

            box.l1_title = itmLevels.getProperty("in_l1_title", "");
            box.l2_title = itmLevels.getProperty("in_l2_title", "");
            box.l3_title = itmLevels.getProperty("in_l3_title", "");
            box.l1_show = itmLevels.getProperty("l1_hide", "") == "" ? true : false;
            box.l2_show = itmLevels.getProperty("l2_hide", "") == "" ? true : false;
            box.l3_show = itmLevels.getProperty("l3_hide", "") == "" ? true : false;

            Dictionary<string, TSummary> dictionary = MapItems(items, box);
            if (dictionary.Count == 0)
            {
                // itmR.setProperty("permission_type", "1");
                // itmR.setProperty("permission_message", "報名資訊錯誤");
                // return itmR;
            }

            //有未確認的報名人員
            itmR.setProperty("inn_gym_un_confirmed", box.has_nonconfirmed ? "1" : "0");
            //已產生過繳費單
            itmR.setProperty("inn_gym_has_payment", box.has_payment ? "1" : "0");
            if (box.has_payment && !box.has_nonconfirmed)
            {
                //有繳費單但沒有未確認的報名人員
                //視為關閉狀態
                itmR.setProperty("inn_gym_is_closed", "1");
            }

            //附加賽事資訊
            AppendMeetingInfo(cfg, itmR);

            //附加登入者資訊
            AppendLoginInfo(cfg, itmR);

            //附加報名摘要
            if (cfg.exe_type == "item")
            {
                //取得取得 in_l1 options 排序
                Dictionary<string, int> l1_options = GetLv1Options(cfg);

                //附加排序值
                BindLv1Options(dictionary, l1_options);

                var sort_dictionary = dictionary.OrderBy(e => e.Value.SortOrder);

                AppendItemSummary(cfg, box, sort_dictionary, itmR);
                //附加分組名單
                AppendTables(cfg, box, sort_dictionary, itmR);
            }
            else
            {
                var sort_dictionary = dictionary.OrderBy(e => e.Value.CreatorOrg);

                AppendPlayerSummary(cfg, box, sort_dictionary, itmR);
                //附加分組名單
                AppendTables(cfg, box, sort_dictionary, itmR);
            }


            //附加統計按鈕狀態
            AppendBtnStatus(cfg, itmR);

            //附加注意事項
            AppendNotices(cfg, itmR);

            return itmR;

        }

        //取得共同講師 (判斷現在的登入者是否為此課程的共同講師)
        private bool GetUserResumeListStatus(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t2.id
                FROM 
                    In_Meeting_Resumelist t1 WITH(NOLOCK) 
                INNER JOIN 
                    IN_RESUME t2 WITH(NOLOCK) 
                    ON t2.id = t1.related_id    
                WHERE 
                    t1.source_id = '{#meeting_id}' 
                    AND t2.in_user_id = '{#in_user_id}';    
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_user_id}", cfg.strUserId);

            Item item = cfg.inn.applySQL(sql);

            if (item.getResult() != "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //附加排序值
        private void BindLv1Options(Dictionary<string, TSummary> dictionary, Dictionary<string, int> options)
        {
            foreach (var kv in dictionary)
            {
                TSummary summary = kv.Value;
                if (options.ContainsKey(kv.Key))
                {
                    summary.SortOrder = options[kv.Key];
                }
            }
        }

        //附加統計按鈕狀態
        private void AppendBtnStatus(TConfig cfg, Item itmReturn)
        {
            string inn_gym_is_closed = itmReturn.getProperty("inn_gym_is_closed", "");
            string inn_gym_real_closed = itmReturn.getProperty("inn_gym_real_closed", "");

            string hide_confirm_box = "";
            string hide_payment_btn = "item_show_0";
            string hide_item_statistics = "item_show_0";
            string hide_org_statistics = "item_show_0";

            if (cfg.isMeetingAdmin)
            {
                hide_confirm_box = "item_show_0";
                hide_payment_btn = "";

                if (cfg.in_meeting_type != "seminar")
                {
                    if (cfg.exe_type == "item")
                    {
                        hide_org_statistics = "";
                    }
                    else
                    {
                        hide_item_statistics = "";
                    }
                }
            }
            else
            {
                if (inn_gym_is_closed == "1")
                {
                    hide_confirm_box = "item_show_0";
                    hide_payment_btn = "";
                }
            }

            itmReturn.setProperty("hide_confirm_box", hide_confirm_box);
            itmReturn.setProperty("hide_payment_btn", hide_payment_btn);

            itmReturn.setProperty("hide_item_statistics", hide_item_statistics);
            itmReturn.setProperty("hide_org_statistics", hide_org_statistics);
        }

        //附加注意事項
        private void AppendNotices(TConfig cfg, Item itmReturn)
        {
            //有未確認的報名人員
            string inn_gym_un_confirmed = itmReturn.getProperty("inn_gym_un_confirmed", "");
            //已產生過繳費單
            string inn_gym_has_payment = itmReturn.getProperty("inn_gym_has_payment", "");

            //付款方式
            string in_pay_mode = cfg.itmMeeting.getProperty("in_pay_mode", "");
            //是否允許再報
            string in_allowed_reapply = cfg.itmMeeting.getProperty("in_allowed_reapply", "");

            string inn_cbx_contetnt = "我已確認報名資料無誤，並進行繳費";
            string inn_btn_contetnt1 = "請確認報名完成";
            string inn_btn_contetnt2 = "線上繳費 GO";
            string inn_closing_message = "結束報名並產生繳費單";
            string inn_closed_message = "已送出本次報名資訊並產生繳費單";

            switch (in_pay_mode)
            {
                case "1": //報名，不產生繳費單
                    inn_cbx_contetnt = "我已確認報名資料填寫完成";
                    inn_btn_contetnt2 = "確認送出";
                    inn_closing_message = "結束報名";
                    inn_closed_message = "已送出本次報名資訊";
                    break;

                case "2": //繳費，無金流模式
                case "3": //繳費，土銀模式
                case "4": //繳費，QrCode 模式
                    break;

                default:
                    break;
            }

            if (in_pay_mode != "1" && in_allowed_reapply == "1" && inn_gym_un_confirmed == "1" && inn_gym_has_payment == "1")
            {
                //允許再報，有產生過繳費單，又有未確認的報名資料
                inn_cbx_contetnt = "我已確認再報資料填寫完成";
                inn_btn_contetnt1 = "請確認再報完成";
                inn_btn_contetnt2 = "產生繳費單";
            }

            itmReturn.setProperty("inn_cbx_contetnt", inn_cbx_contetnt);
            itmReturn.setProperty("inn_btn_contetnt1", inn_btn_contetnt1);
            itmReturn.setProperty("inn_btn_contetnt2", inn_btn_contetnt2);
            itmReturn.setProperty("inn_closing_message", inn_closing_message);
            itmReturn.setProperty("inn_closed_message", inn_closed_message);
        }

        //附加賽事資訊
        private void AppendMeetingInfo(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("meeting_name", cfg.itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("in_customer_tel", cfg.itmMeeting.getProperty("in_customer_tel", "").Replace("(請加上@)", ""));
            itmReturn.setProperty("in_url", cfg.itmMeeting.getProperty("in_url", ""));

            itmReturn.setProperty("in_register_url", cfg.itmMeeting.getProperty("in_register_url", ""));
            itmReturn.setProperty("inn_meeting_admin", cfg.isMeetingAdmin ? "1" : "0");
            itmReturn.setProperty("inn_excel_method", cfg.isMeetingAdmin ? "in_meeting_export_excel_admin" : "in_meeting_export_excel_gym");

            itmReturn.setProperty("in_bank_1", cfg.itmMeeting.getProperty("in_bank_1", ""));
            itmReturn.setProperty("in_bank_2", cfg.itmMeeting.getProperty("in_bank_2", ""));
            itmReturn.setProperty("in_bank_name", cfg.itmMeeting.getProperty("in_bank_name", ""));
            itmReturn.setProperty("in_bank_account", cfg.itmMeeting.getProperty("in_bank_account", ""));
            itmReturn.setProperty("in_bank_note", cfg.itmMeeting.getProperty("in_bank_note", ""));

            itmReturn.setProperty("in_verify_mode", cfg.itmMeeting.getProperty("in_verify_mode", ""));
        }

        //附加登入者資訊
        private void AppendLoginInfo(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("in_sno", cfg.itmLogin.getProperty("in_sno", ""));
            itmReturn.setProperty("in_group", cfg.itmLogin.getProperty("in_group", ""));
            itmReturn.setProperty("in_name", cfg.itmLogin.getProperty("in_name", ""));
            itmReturn.setProperty("in_tel", cfg.itmLogin.getProperty("in_tel", ""));
            itmReturn.setProperty("in_add", cfg.itmLogin.getProperty("in_add", ""));

            itmReturn.setProperty("print_datetime", DateTime.Now.ToString("yyyy/MM/dd HH:mm"));
            itmReturn.setProperty("print_user", cfg.itmLogin.getProperty("in_name", ""));

            itmReturn.setProperty("inn_email", cfg.itmLogin.getProperty("in_email", " "));
        }

        //附加報名摘要
        private void AppendItemSummary(TConfig cfg, TBox box, IOrderedEnumerable<KeyValuePair<string, TSummary>> dictionary, Item itmReturn)
        {
            itmReturn.setProperty("head_2", box.l1_title);
            itmReturn.setProperty("head_4", "隊職員數");
            itmReturn.setProperty("head_5", "報名人數");
            itmReturn.setProperty("head_6", "參賽項目總計");
            itmReturn.setProperty("head_11", "報名費用");

            itmReturn.setProperty("hide_head_2", "");
            itmReturn.setProperty("hide_head_4", "data-visible=\"false\"");
            itmReturn.setProperty("hide_head_5", "");
            itmReturn.setProperty("hide_head_6", "data-visible=\"false\"");
            itmReturn.setProperty("hide_head_11", "");

            foreach (var kv in dictionary)
            {
                TSummary summary = kv.Value;

                Item item = cfg.inn.newItem("coverpage");
                item.setProperty("col_2", kv.Key);
                item.setProperty("col_4", summary.StaffCount.ToString());

                if (kv.Key == "隊職員")
                {
                    item.setProperty("col_5", summary.StaffCount.ToString());
                }
                else
                {
                    item.setProperty("col_5", summary.UserCount.ToString());
                }

                item.setProperty("col_6", summary.TeamCount.ToString());
                item.setProperty("col_11", summary.TotalAmount.ToString());

                if (cfg.is_team_bill)
                {
                    item.setProperty("col_11", "--");
                }

                itmReturn.addRelationship(item);
            }

            //報名總費用
            int total_amount = dictionary.Sum(x => x.Value.TotalAmount);
            itmReturn.setProperty("total_amount", total_amount.ToString());

            if (cfg.is_team_bill)
            {
                itmReturn.setProperty("total_amount", cfg.in_member_type);
            }
        }

        //附加報名摘要
        private void AppendPlayerSummary(TConfig cfg, TBox box, IOrderedEnumerable<KeyValuePair<string, TSummary>> dictionary, Item itmReturn)
        {
            itmReturn.setProperty("head_2", "所屬單位");
            itmReturn.setProperty("head_4", "隊職員數");
            itmReturn.setProperty("head_5", "報名人數");
            itmReturn.setProperty("head_6", "參賽項目總計");
            itmReturn.setProperty("head_11", "報名費用");

            itmReturn.setProperty("hide_head_2", "");
            itmReturn.setProperty("hide_head_4", "");
            itmReturn.setProperty("hide_head_5", "");
            itmReturn.setProperty("hide_head_6", "");
            itmReturn.setProperty("hide_head_11", "");

            foreach (var kv in dictionary)
            {
                TSummary summary = kv.Value;

                Item item = cfg.inn.newItem("coverpage");
                item.setProperty("col_2", summary.CreatorOrg);
                item.setProperty("col_4", summary.StaffCount.ToString());
                item.setProperty("col_5", summary.UserCount.ToString());
                item.setProperty("col_6", summary.TeamCount.ToString());
                item.setProperty("col_11", summary.TotalAmount.ToString());
                
                if(cfg.is_team_bill)
                {
                    item.setProperty("col_11", "--");
                }
                itmReturn.addRelationship(item);
            }

            //報名總費用
            int total_amount = dictionary.Sum(x => x.Value.TotalAmount);
            itmReturn.setProperty("total_amount", total_amount.ToString());

            if (cfg.is_team_bill)
            {
                itmReturn.setProperty("total_amount", cfg.in_member_type);
            }
        }

        //附加分組名單
        private void AppendTables(TConfig cfg, TBox box, IOrderedEnumerable<KeyValuePair<string, TSummary>> dictionary, Item itmReturn)
        {
            StringBuilder builder = new StringBuilder();

            foreach (var kv in dictionary)
            {
                var summary = kv.Value;

                var sort_list = summary.Tables.OrderBy(e => e.Value.SortOrder);

                foreach (var table_kv in sort_list)
                {
                    var table = table_kv.Value;

                    AppendTable(cfg, builder, box, table);
                }
            }

            itmReturn.setProperty("inn_tables", builder.ToString());
        }

        //附加分組名單
        private void AppendTable(TConfig cfg, StringBuilder builder, TBox box, TTable table)
        {
            builder.Append("<div class=\"showsheet_" + table.Name + " page-block\">");
            builder.Append("  <h2 class=\"text-center\">" + table.Head + "</h2>");

            if (table.Template == "staff")
            {
                builder.Append("  <h4 class=\"sheet-title\">" + table.Title + "<span> </span> </h4>");
            }
            else
            {
                builder.Append("  <h4 class=\"sheet-title\">" + table.Title + "<span>報名費總計: " + table.TotalAmount.ToString() + "</span> </h4>");
            }
            builder.Append("  <table id=\"" + table.Name + "\" class=\"table table-hover table-bordered table-rwd rwd\">");

            switch (table.Template)
            {
                case "organization":
                    AppendOrgTable(cfg, builder, box, table);
                    break;
                case "single":
                    AppendSingleTable(cfg, builder, box, table);
                    break;
                case "due":
                    AppendDueTable(cfg, builder, box, table);
                    break;
                case "mix":
                    break;
                case "team":
                    AppendTeamTable(cfg, builder, box, table);
                    break;
                case "staff":
                    AppendStaffTable(cfg, builder, box, table);
                    break;
                default:
                    break;
            }

            builder.Append("  </table>");
            // builder.Append("  <script>");
            // builder.Append("      $('#" + table.Name + "').bootstrapTable();");
            // builder.Append("      if($(window).width() <= 768) { $('#" + table.Name + "').bootstrapTable('toggleView');}");
            // builder.Append("  </script>");
            builder.Append("  <div style='height: 25px;'> </div>");//2020.10.06 補了間隔 讓其可以蓋章
            builder.Append("  <div class=\"pull-left signature\">" + box.sign0 + "</div>");
            builder.Append("  <div class=\"pull-left signature\">" + box.sign1 + "</div>");
            builder.Append("  <div class=\"pull-left signature\">" + box.sign2 + "</div>");
            builder.Append("  <div class=\"pull-left signature\">" + box.sign3 + "</div>");
            builder.Append("</div>");
        }

        //附加單位名單
        private void AppendOrgTable(TConfig cfg, StringBuilder builder, TBox box, TTable table)
        {
            TMap map = ConvertMap(table.Rows);

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("  <tr>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">姓名</th>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">所屬單位</th>");
            // head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">序號</th>");
            if (box.l1_show) head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">" + box.l1_title + "</th>");
            if (box.l2_show) head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">" + box.l2_title + "</th>");
            if (box.l3_show) head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">" + box.l3_title + "</th>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">報名費用</th>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">繳費狀態</th>");
            head.Append("  </tr>");
            head.Append("</thead>");

            body.Append("<tbody>");
            Item last_row = null;

            foreach (var list in map.Lists)
            {
                foreach (var row in list)
                {
                    string in_l1 = row.getProperty("in_l1", "");

                    body.Append("  <tr>");
                    body.Append("    <td>" + row.getProperty("in_name", "") + "</td>");
                    body.Append("    <td>" + row.getProperty("in_current_org", "") + "</td>");
                    // body.Append("    <td>" + row.getProperty("in_index", "") + "</td>");
                    if (box.l1_show) body.Append("    <td>" + in_l1 + "</td>");
                    if (box.l2_show) body.Append("    <td>" + row.getProperty("in_l2", "") + "</td>");
                    if (box.l3_show) body.Append("    <td>" + row.getProperty("in_l3", "") + "</td>");

                    if (isSameTeam(last_row, row))
                    {
                        body.Append("    <td> </td>");
                    }
                    else
                    {
                        body.Append("    <td class=\"text-right\">" + GetPayAmount(cfg, row) + "</td>");
                    }
                    if (in_l1 == "隊職員")
                    {
                        body.Append("    <td class=\"text-center\"> </td>");
                    }
                    else
                    {
                        body.Append("    <td class=\"text-center\">" + GetPayStatusIcon(cfg, row) + "</td>");
                    }
                    body.Append("  </tr>");

                    last_row = row;
                }
            }
            body.Append("</tbody>");

            builder.Append(head);
            builder.Append(body);
        }

        private string GetPayAmount(TConfig cfg, Item item)
        {
            if (cfg.is_team_bill)
            {
                return cfg.in_member_type;
            }
            else
            {
                return item.getProperty("in_expense", "");
            }
        }

        private string GetPayStatusIcon(TConfig cfg, Item item)
        {
            string in_l1 = item.getProperty("in_l1", "");

            if (in_l1 == "隊職員")
            {
                return "";
            }

            StringBuilder builder = new StringBuilder();

            string[] properties = new string[]
            {
                "in_paynumber", //繳費單號
                "in_pay_amount_real", //實際收款金額
                "in_pay_photo", //上傳繳費照片(繳費收據)
                "in_return_mark", //審核退回說明
                "in_collection_agency", //代收機構
            };

            foreach (string property in properties)
            {
                builder.Append("<" + property + ">" + item.getProperty(property, "") + "</" + property + ">");
            }

            //未確認
            builder.Append("<option>1</option>");

            Item itmResult = cfg.inn.applyMethod("In_PayType_Icon", builder.ToString());

            return itmResult.getProperty("in_pay_type", "");
        }

        /// <summary>
        /// 是否同組隊員
        /// </summary>
        private bool isSameTeam(Item last_row, Item current_row)
        {
            if (last_row == null)
            {
                return false;
            }

            return last_row.getProperty("team_key", "last") == current_row.getProperty("team_key", "current");
        }

        //附加單打名單
        private void AppendSingleTable(TConfig cfg, StringBuilder builder, TBox box, TTable table)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("  <tr>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">姓名</th>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">所屬單位</th>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">序號</th>");
            if (box.l1_show) head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">" + box.l1_title + "</th>");
            if (box.l2_show) head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">" + box.l2_title + "</th>");
            if (box.l3_show) head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">" + box.l3_title + "</th>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">報名費用</th>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">報名狀態</th>");
            head.Append("  </tr>");
            head.Append("</thead>");

            body.Append("<tbody>");
            foreach (var row in table.Rows)
            {
                body.Append("  <tr>");
                body.Append("    <td>" + row.getProperty("in_name", "") + "</td>");
                body.Append("    <td>" + row.getProperty("in_current_org", "") + "</td>");
                body.Append("    <td>" + row.getProperty("in_index", "") + "</td>");
                if (box.l1_show) body.Append("    <td>" + row.getProperty("in_l1", "") + "</td>");
                if (box.l2_show) body.Append("    <td>" + row.getProperty("in_l2", "") + "</td>");
                if (box.l3_show) body.Append("    <td>" + row.getProperty("in_l3", "") + "</td>");
                body.Append("    <td class=\"text-right\">" + GetPayAmount(cfg, row) + "</td>");
                body.Append("    <td class=\"text-center\">" + GetPayStatusIcon(cfg, row) + "</td>");
                body.Append("  </tr>");
            }
            body.Append("</tbody>");

            builder.Append(head);
            builder.Append(body);
        }

        //附加雙人名單
        private void AppendDueTable(TConfig cfg, StringBuilder builder, TBox box, TTable table)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("  <tr>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">報名組別</th>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">隊別</th>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">序號</th>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">隊員1</th>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">隊員2</th>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">報名費用</th>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">報名狀態</th>");
            head.Append("  </tr>");
            head.Append("</thead>");

            body.Append("<tbody>");
            foreach (var team_kv in table.Teams)
            {
                var team = team_kv.Value;
                if (team.Items.Count == 0)
                {
                    continue;
                }

                var row1 = team.Items.Count > 0 ? team.Items[0] : null;
                var row2 = team.Items.Count > 1 ? team.Items[1] : null;

                body.Append("  <tr>");
                body.Append("    <td>" + row1.getProperty("in_section_name", "") + "</td>");
                body.Append("    <td>" + row1.getProperty("in_team", "") + "</td>");
                body.Append("    <td>" + row1.getProperty("in_index", "") + "</td>");
                body.Append("    <td>" + row1.getProperty("in_name", "") + "</td>");
                if (row2 == null)
                {
                    body.Append("    <td> </td>");
                }
                else
                {
                    body.Append("    <td>" + row2.getProperty("in_name", "") + "</td>");
                }
                body.Append("    <td class=\"text-right\">" + GetPayAmount(cfg, row1) + "</td>");
                body.Append("    <td class=\"text-center\">" + GetPayStatusIcon(cfg, row1) + "</td>");
                body.Append("  </tr>");
            }
            body.Append("</tbody>");

            builder.Append(head);
            builder.Append(body);
        }

        //附加團體名單
        private void AppendTeamTable(TConfig cfg, StringBuilder builder, TBox box, TTable table)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("  <tr>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">報名組別</th>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">隊別</th>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">序號</th>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">隊員</th>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">報名費用</th>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">報名狀態</th>");
            head.Append("  </tr>");
            head.Append("</thead>");

            body.Append("<tbody>");
            foreach (var team_kv in table.Teams)
            {
                var team = team_kv.Value;
                var row = team.Items[0];

                var names = string.Join(", ", team.Items.Select(x => x.getProperty("in_name", "")));

                body.Append("  <tr>");
                body.Append("    <td>" + row.getProperty("in_section_name", "") + "</td>");
                body.Append("    <td>" + row.getProperty("in_team", "") + "</td>");
                body.Append("    <td>" + row.getProperty("in_index", "") + "</td>");
                body.Append("    <td>" + names + "</td>");
                body.Append("    <td class=\"text-right\">" + GetPayAmount(cfg, row) + "</td>");
                body.Append("    <td class=\"text-center\">" + GetPayStatusIcon(cfg, row) + "</td>");
                body.Append("  </tr>");
            }
            body.Append("</tbody>");

            builder.Append(head);
            builder.Append(body);
        }

        //附加隊職員名單
        private void AppendStaffTable(TConfig cfg, StringBuilder builder, TBox box, TTable table)
        {
            TMap map = ConvertMap(table.Rows);

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("  <tr>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">姓名</th>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">所屬單位</th>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">身分</th>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">職務</th>");
            head.Append("  </tr>");
            head.Append("</thead>");

            body.Append("<tbody>");

            foreach (var list in map.Lists)
            {
                foreach (var row in list)
                {
                    body.Append("  <tr>");
                    body.Append("    <td>" + row.getProperty("in_name", "") + "</td>");
                    body.Append("    <td>" + row.getProperty("in_current_org", "") + "</td>");
                    body.Append("    <td>" + row.getProperty("in_l1", "") + "</td>");
                    body.Append("    <td>" + row.getProperty("in_l2", "") + "</td>");
                    body.Append("  </tr>");
                }
            }

            body.Append("</tbody>");

            builder.Append(head);
            builder.Append(body);
        }

        /// <summary>
        /// 轉換選手與隊職員
        /// </summary>
        private TMap ConvertMap(List<Item> rows)
        {
            TMap map = new TMap
            {
                StaffList_Leader = new List<Item>(),
                StaffList_Manager = new List<Item>(),
                StaffList_Teacher = new List<Item>(),
                StaffList_Other = new List<Item>(),
                Players = new List<Item>(),
                Lists = new List<List<Item>>(),
            };

            foreach (var row in rows)
            {
                var in_l1 = row.getProperty("in_l1", "");
                var in_l2 = row.getProperty("in_l2", "");

                if (in_l1 != "隊職員")
                {
                    map.Players.Add(row);
                }
                else
                {
                    switch (in_l2)
                    {
                        case "領隊":
                            map.StaffList_Leader.Add(row);
                            break;

                        case "管理":
                            map.StaffList_Manager.Add(row);
                            break;

                        case "教練":
                            map.StaffList_Teacher.Add(row);
                            break;

                        default:
                            map.StaffList_Other.Add(row);
                            break;
                    }
                }
            }

            map.Lists.Add(map.StaffList_Leader);
            map.Lists.Add(map.StaffList_Manager);
            map.Lists.Add(map.StaffList_Teacher);
            map.Lists.Add(map.StaffList_Other);
            map.Lists.Add(map.Players);
            return map;
        }

        private class TMap
        {
            /// <summary>
            /// 隊職員-領隊
            /// </summary>
            public List<Item> StaffList_Leader { get; set; }

            /// <summary>
            /// 隊職員-管理
            /// </summary>
            public List<Item> StaffList_Manager { get; set; }

            /// <summary>
            /// 隊職員-教練
            /// </summary>
            public List<Item> StaffList_Teacher { get; set; }

            /// <summary>
            /// 隊職員-其他
            /// </summary>
            public List<Item> StaffList_Other { get; set; }

            /// <summary>
            /// 選手
            /// </summary>
            public List<Item> Players { get; set; }

            /// <summary>
            /// 總集合
            /// </summary>
            public List<List<Item>> Lists { get; set; }
        }

        #region 存取資料
        //取得報名問項
        private Item GetSurveyLevels(TConfig cfg)
        {
            Item itmResult = cfg.inn.newItem();

            string sql = @"
                SELECT 
                    t1.*
                FROM 
                    IN_SURVEY t1 WITH(NOLOCK)
                INNER JOIN 
                    IN_MEETING_SURVEYS t2 WITH(NOLOCK) ON t2.related_id = t1.id 
                WHERE 
                    t2.source_id = N'{#meeting_id}' 
                    AND t1.in_client_remove = N'0'
                    AND t1.in_property IN (N'in_l1', N'in_l2', N'in_l3')
                ORDER BY 
                    t2.sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql_levels: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                return itmResult;
            }

            int count = items.getItemCount();

            string level_fields = "";
            string number_key_level = "";
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_questions = ClearValue(item.getProperty("in_questions", ""));
                string in_property = item.getProperty("in_property", "").Trim();
                string in_is_nokey = item.getProperty("in_is_nokey", "").Trim();

                if (level_fields != "")
                {
                    level_fields += ",";
                }
                level_fields += in_property;

                if (in_is_nokey == "1")
                {
                    if (number_key_level != "")
                    {
                        number_key_level += ",";
                    }
                    number_key_level += in_property;
                }

                itmResult.setProperty(in_property + "_title", in_questions);
            }

            if (level_fields == "")
            {
                level_fields = "in_l1";
            }

            if (number_key_level == "")
            {
                number_key_level = "in_l1";
            }

            string default_cols = "";
            string user_order_by = "";
            string distinct_cols = "";
            string l1_hide = "";
            string l2_hide = "";
            string l3_hide = "";

            if (level_fields.Contains("in_l3"))
            {
                default_cols = "in_l1, in_l2, in_l3";
            }
            else if (level_fields.Contains("in_l2"))
            {
                default_cols = "in_l1, in_l2";
                l3_hide = "item_show_0";

            }
            else if (level_fields.Contains("in_l1"))
            {
                default_cols = "in_l1";
                l2_hide = "item_show_0";
                l3_hide = "item_show_0";
            }
            else
            {
                l1_hide = "item_show_0";
                l2_hide = "item_show_0";
                l3_hide = "item_show_0";
            }

            //定序處理
            if (number_key_level.Contains("in_l3"))
            {
                distinct_cols = "in_l1, in_l2, in_l3, in_index";
                user_order_by = "in_current_org, in_l1, in_l2, in_l3, in_index, in_name";
            }
            else if (number_key_level.Contains("in_l2"))
            {
                distinct_cols = "in_l1, in_l2, in_index";
                user_order_by = "in_current_org, in_l1, in_l2, in_index, in_name";
            }
            else if (number_key_level.Contains("in_l1"))
            {
                distinct_cols = "in_l1, in_index";
                user_order_by = "in_current_org, in_l1, in_index, in_name";
            }

            itmResult.setProperty("meeting_id", cfg.meeting_id);
            //定序欄位
            itmResult.setProperty("number_key_level", number_key_level);
            //預設欄位組合
            itmResult.setProperty("default_cols", default_cols);
            //分組統計欄位組合
            itmResult.setProperty("distinct_cols", distinct_cols);
            //定序欄位組合
            itmResult.setProperty("user_order_by", user_order_by);
            //第一階選單是否隱藏
            itmResult.setProperty("l1_hide", l1_hide);
            //第二階選單是否隱藏
            itmResult.setProperty("l2_hide", l2_hide);
            //第三階選單是否隱藏
            itmResult.setProperty("l3_hide", l3_hide);

            return itmResult;
        }

        private string ClearValue(string value)
        {
            if (value.Contains("."))
            {
                return value.Split('.').Last().Trim();
            }
            return value.Trim();
        }

        //取得報名人員
        private Item GetMeetingUsers(TConfig cfg, string user_order_by)
        {
            //string group_condition = isMeetingAdmin ? "" : "AND t1.in_group = N'" + in_group + "'";
            string creator_condition = cfg.isMeetingAdmin ? "" : "AND t1.in_creator_sno = N'" + cfg.in_creator_sno + "'";

            string sql = "";

            sql = @"
                SELECT
                    t1.source_id
                    , t1.id
                    , t1.in_group
                    , t1.in_current_org
                    , t1.in_name
                    , t1.in_sno
                    , t1.in_index
                    , t1.in_team
                    , t1.in_l1
                    , t1.in_l2
                    , t1.in_l3
                    , t1.in_expense
                    , t1.in_section_name
                    , t1.in_paynumber
                    , t1.in_creator
                    , t1.in_creator_sno
                    , t2.in_tel AS 'in_creator_tel'
                    , t2.in_add AS 'in_creator_add'
                    , t3.pay_bool
                    , t3.in_pay_amount_real
                    , t3.in_pay_photo
                    , t3.in_return_mark
                    , t3.in_collection_agency
                    , t4.in_group AS 'is_closed'
                FROM
                    IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_RESUME t2 WITH(NOLOCK)
                    ON t2.in_sno = t1.in_creator_sno
                LEFT OUTER JOIN
                    IN_MEETING_PAY t3 WITH(NOLOCK)
                    ON t3.in_meeting = t1.source_id
                    AND t3.item_number = t1.in_paynumber
                LEFT OUTER JOIN
                    IN_MEETING_GYMLIST t4 WITH(NOLOCK)
                    ON t4.source_id = t1.source_id
                    AND t4.in_creator_sno = t1.in_creator_sno
                WHERE
                    t1.source_id = '{#meeting_id}'
                    {#filter_condition}
                ORDER BY
                    {#user_order_by}
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#filter_condition}", creator_condition)
                .Replace("{#user_order_by}", user_order_by);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //取得第一階問項排序
        private Dictionary<string, int> GetLv1Options(TConfig cfg)
        {
            string sql = "";

            sql = @"
                SELECT
                    t3.in_value
                    , t3.sort_order
                FROM
                    IN_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN
                    IN_SURVEY t2 WITH(NOLOCK)
                    ON t2.id = t1.related_id
                INNER JOIN
                    IN_SURVEY_OPTION t3 WITH(NOLOCK)
                    ON t3.source_id = t1.related_id
                WHERE
                    t1.source_id = '{#meeting_id}'
                    AND t2.IN_PROPERTY = N'in_l1'
                ORDER BY
                    t3.sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Dictionary<string, int> result = new Dictionary<string, int>();

            Item items = cfg.inn.applySQL(sql);

            if (IsError(items, isSingle: false))
            {
                return result;
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_value = item.getProperty("in_value", "");
                string sort_order = item.getProperty("sort_order", "");
                int int_sort_order = GetIntVal(sort_order);

                if (!result.ContainsKey(in_value))
                {
                    result.Add(in_value, int_sort_order);
                }
            }

            return result;
        }

        //取得單位結束報名資料
        private Item GetMeetingGymList(TConfig cfg)
        {
            string sql = @"
                SELECT
                    *
                FROM
                    IN_MEETING_GYMLIST WITH(NOLOCK)
                WHERE
                    source_id = '{#meeting_id}'
                    AND in_creator_sno = N'{#in_creator_sno}'
            ";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_creator_sno}", cfg.in_creator_sno);

            return cfg.inn.applySQL(sql);
        }

        #endregion 存取資料

        #region 轉換資料

        /// <summary>
        /// List 轉為 Dictionary
        /// </summary>
        private Dictionary<string, TSummary> MapItems(Item items, TBox box)
        {
            Dictionary<string, TSummary> dictionary = new Dictionary<string, TSummary>();

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                Boxing(box, item);

                item.setProperty("team_key", box.team_key);
                item.setProperty("pay_status", box.pay_status);

                TSummary summary = AddAndGetSummary(dictionary, box);

                if (box.is_staff)
                {
                    if (!summary.Staffs.ContainsKey(box.id))
                    {
                        summary.Staffs.Add(box.id, box.item);
                    }
                    item.setProperty("in_expense", " ");
                }
                else
                {
                    if (!summary.Users.ContainsKey(box.in_sno))
                    {
                        summary.Users.Add(box.in_sno, box.item);
                    }

                    AddTeam(summary.Teams, box);
                }


                box.summary_id = i + 1;
                box.table_id = summary.Tables.Count + 1;

                AddTables(summary.Tables, box);
            }

            foreach (var kv in dictionary)
            {
                var summary = kv.Value;
                summary.StaffCount = summary.Staffs.Count;
                summary.UserCount = summary.Users.Count;
                summary.TeamCount = summary.Teams.Count;
                summary.TotalAmount = summary.Teams.Sum(x => x.Value.Amount);

                foreach (var tb_kv in summary.Tables)
                {
                    var table = tb_kv.Value;
                    table.TotalAmount = table.Teams.Sum(x => x.Value.Amount);
                }
            }

            int total_amount = dictionary.Sum(x => x.Value.TotalAmount);
            return dictionary;
        }

        /// <summary>
        /// 加入並回傳摘要
        /// </summary>
        private TSummary AddAndGetSummary(Dictionary<string, TSummary> dictionary, TBox box)
        {
            if (dictionary.ContainsKey(box.summary_key))
            {
                return dictionary[box.summary_key];
            }
            else
            {
                TSummary summary = new TSummary
                {
                    CreatorOrg = box.in_current_org,
                    CreatorName = box.in_creator,
                    CreatorSno = box.in_creator_sno,
                    CreatorTel = box.in_creator_tel,
                    CreatorAdd = box.in_creator_add,
                    Staffs = new Dictionary<string, Item>(),
                    Users = new Dictionary<string, Item>(),
                    Teams = new Dictionary<string, TTeam>(),
                    Tables = new Dictionary<string, TTable>(),
                };

                dictionary.Add(box.summary_key, summary);

                return summary;
            }
        }

        /// <summary>
        /// 加入 Team
        /// </summary>
        private void AddTeam(Dictionary<string, TTeam> map, TBox box)
        {
            string key = box.team_key;

            if (map.ContainsKey(key))
            {
                map[key].Items.Add(box.item);
            }
            else
            {
                TTeam team = new TTeam
                {
                    Name = key,
                    Amount = GetIntVal(box.in_expense),
                    Items = new List<Item>()
                };
                team.Items.Add(box.item);
                map.Add(key, team);
            }
        }

        /// <summary>
        /// 加入 Tables
        /// </summary>
        private void AddTables(Dictionary<string, TTable> map, TBox box)
        {
            string key = box.table_key;

            TTable table = null;
            if (map.ContainsKey(key))
            {
                table = map[key];
            }
            else
            {
                TTemplate t = GetTemplate(box);
                table = new TTable
                {
                    Head = box.meeting_title,

                    Name = "tb_" + box.summary_id + "_" + box.table_id,
                    Title = box.table_key,

                    Template = t.Name,
                    SortOrder = t.SortOrder,

                    Rows = new List<Item>(),
                    Teams = new Dictionary<string, TTeam>(),
                };
                map.Add(key, table);
            }

            table.Rows.Add(box.item);

            AddTeam(table.Teams, box);
        }
        #endregion 轉換資料

        #region 資料模型

        /// <summary>
        /// 摘要
        /// </summary>
        private class TSummary
        {
            /// <summary>
            /// 報名單位
            /// </summary>
            public string CreatorOrg { get; set; }

            /// <summary>
            /// 報名人員
            /// </summary>
            public string CreatorName { get; set; }

            /// <summary>
            /// 報名人員身分證號
            /// </summary>
            public string CreatorSno { get; set; }

            /// <summary>
            /// 連絡電話
            /// </summary>
            public string CreatorTel { get; set; }

            /// <summary>
            /// 連絡地址
            /// </summary>
            public string CreatorAdd { get; set; }

            /// <summary>
            /// 隊職員數
            /// </summary>
            public int StaffCount { get; set; }

            /// <summary>
            /// 選手人數 or 學員人數
            /// </summary>
            public int UserCount { get; set; }

            /// <summary>
            /// 項目數
            /// </summary>
            public int TeamCount { get; set; }

            /// <summary>
            /// 報名費用
            /// </summary>
            public int TotalAmount { get; set; }

            /// <summary>
            /// 隊職員表
            /// </summary>
            public Dictionary<string, Item> Staffs { get; set; }

            /// <summary>
            /// 選手表 OR 學員表
            /// </summary>
            public Dictionary<string, Item> Users { get; set; }

            /// <summary>
            /// 項目表
            /// </summary>
            public Dictionary<string, TTeam> Teams { get; set; }

            /// <summary>
            /// 階層表
            /// </summary>
            public Dictionary<string, TTable> Tables { get; set; }

            /// <summary>
            /// 排序
            /// </summary>
            public int SortOrder { get; set; }
        }

        /// <summary>
        /// 表格
        /// </summary>
        private class TTable
        {
            /// <summary>
            /// 大標題
            /// </summary>
            public string Head { get; set; }

            /// <summary>
            /// 表格名稱
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// 表格標題
            /// </summary>
            public string Title { get; set; }

            /// <summary>
            /// 樣板類型
            /// </summary>
            public string Template { get; set; }

            /// <summary>
            /// 樣板排序
            /// </summary>
            public int SortOrder { get; set; }

            /// <summary>
            /// 資料列
            /// </summary>
            public List<Item> Rows { get; set; }

            /// <summary>
            /// 項目表
            /// </summary>
            public Dictionary<string, TTeam> Teams { get; set; }

            /// <summary>
            /// 報名費總計
            /// </summary>
            public int TotalAmount { get; set; }
        }

        /// <summary>
        /// 隊形
        /// </summary>
        private class TTeam
        {
            /// <summary>
            /// 隊伍鍵
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// 報名費用
            /// </summary>
            public int Amount { get; set; }

            /// <summary>
            /// 明細
            /// </summary>
            public List<Item> Items { get; set; }
        }

        /// <summary>
        /// 樣型
        /// </summary>
        private class TTemplate
        {
            /// <summary>
            /// 樣板
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// 排序
            /// </summary>
            public int SortOrder { get; set; }
        }

        /// <summary>
        /// 箱型
        /// </summary>
        private class TBox
        {
            public string exe_type { get; set; }

            public string meeting_type { get; set; }
            public string meeting_title { get; set; }
            public string meeting_mode { get; set; }

            public string sign0 { get; set; }
            public string sign1 { get; set; }
            public string sign2 { get; set; }
            public string sign3 { get; set; }

            public string summary_property { get; set; }
            public string summary_key { get; set; }

            public string[] table_properties { get; set; }
            public string table_key { get; set; }

            public string l1_title { get; set; }
            public string l2_title { get; set; }
            public string l3_title { get; set; }

            public bool l1_show { get; set; }
            public bool l2_show { get; set; }
            public bool l3_show { get; set; }

            public int summary_id { get; set; }
            public int table_id { get; set; }

            public string source_id { get; set; }
            public string id { get; set; }
            public string in_sno { get; set; }
            public string in_name { get; set; }

            public string in_group { get; set; }
            public string in_current_org { get; set; }

            public string in_team { get; set; }
            public string in_section_name { get; set; }

            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_index { get; set; }

            public string in_expense { get; set; }
            public string in_paynumber { get; set; }
            public string pay_status { get; set; }

            public string team_key { get; set; }

            public string in_creator { get; set; }
            public string in_creator_sno { get; set; }
            public string in_creator_tel { get; set; }
            public string in_creator_add { get; set; }

            public bool is_staff { get; set; }
            public bool is_closed { get; set; }

            public bool has_payment { get; set; }
            public bool has_nonconfirmed { get; set; }

            public Item item { get; set; }

            public Func<Item, bool, string[], string> GetTableKey { get; set; }
        }
        #endregion 資料模型

        /// <summary>
        /// 取得打包箱
        /// </summary>
        private TBox MapMeeting(TConfig cfg)
        {
            TBox box = new TBox
            {
                exe_type = cfg.exe_type,
                meeting_title = cfg.itmMeeting.getProperty("in_title", ""),
                meeting_type = cfg.itmMeeting.getProperty("in_meeting_type", ""),
            };

            if (cfg.exe_type == "item")
            {
                box.meeting_mode = "item";
                box.summary_property = "in_l1";
                box.GetTableKey = GetTableKey1;
                box.table_properties = new string[] { "in_l1", };
            }
            else
            {
                box.meeting_mode = "gym";
                box.summary_property = "in_current_org";

                box.GetTableKey = GetTableKey1;
                box.table_properties = new string[] { "in_l1" };
                

                if (cfg.isMeetingAdmin)
                {
                    box.meeting_mode = "";
                    box.GetTableKey = GetTableKey1;
                    box.table_properties = new string[]{"in_current_org" };
                }
            }

            string in_pdf = cfg.itmMeeting.getProperty("in_pdf", "");

            if (in_pdf != "")
            {
                string[] pdf_params = in_pdf.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (pdf_params != null && pdf_params.Length > 0)
                {
                    for (int i = 0; i < pdf_params.Length; i++)
                    {
                        cfg.itmMeeting.setProperty("sign" + i, pdf_params[i]);
                    }
                }
            }

            box.sign0 = cfg.itmMeeting.getProperty("sign0", "");
            box.sign1 = cfg.itmMeeting.getProperty("sign1", "");
            box.sign2 = cfg.itmMeeting.getProperty("sign2", "");
            box.sign3 = cfg.itmMeeting.getProperty("sign3", "");

            return box;
        }


        /// <summary>
        /// 裝箱
        /// </summary>
        private void Boxing(TBox box, Item item)
        {
            box.summary_key = item.getProperty(box.summary_property, "");

            box.source_id = item.getProperty("source_id", "");
            box.id = item.getProperty("id", "");

            box.in_name = item.getProperty("in_name", "");
            box.in_sno = item.getProperty("in_sno", "").ToUpper();

            box.in_group = item.getProperty("in_group", "");
            box.in_current_org = item.getProperty("in_current_org", "");

            box.in_team = item.getProperty("in_team", "");
            box.in_section_name = item.getProperty("in_section_name", "");

            box.in_l1 = item.getProperty("in_l1", "");
            box.in_l2 = item.getProperty("in_l2", "");
            box.in_l3 = item.getProperty("in_l3", "");
            box.in_index = item.getProperty("in_index", "");

            box.in_expense = item.getProperty("in_expense", "");
            box.pay_status = item.getProperty("pay_bool", "");
            box.is_closed = item.getProperty("is_closed", "") != "";

            box.team_key = GetKeyValues(item, new string[]
            {
                "in_l1",
                "in_l2",
                "in_l3",
                "in_index",
            });


            box.in_creator = item.getProperty("in_creator", "");
            box.in_creator_sno = item.getProperty("in_creator_sno", "");
            box.in_creator_tel = item.getProperty("in_creator_tel", "");
            box.in_creator_add = item.getProperty("in_creator_add", "");

            box.is_staff = box.in_l1 == "隊職員";

            if (box.pay_status == "")
            {
                if (!box.is_staff)
                {
                    box.has_nonconfirmed = true;
                    box.pay_status = box.is_closed ? "" : "<span style='color: #2C4198'>未確認</span>";
                }
            }
            else if (box.pay_status == "未繳費")
            {
                box.pay_status = "<span style='color: red'>未繳費</span>";
                box.has_payment = true;
            }
            else
            {
                box.has_payment = true;
            }

            box.table_key = box.GetTableKey(item, box.is_staff, box.table_properties);

            box.item = item;
        }

        /// <summary>
        /// 生成鍵值
        /// </summary>
        private string GetKeyValues(Item item, string[] properties)
        {
            string[] values = new string[properties.Length];
            for (int i = 0; i < properties.Length; i++)
            {
                values[i] = item.getProperty(properties[i], "");
            }
            return string.Join("-", values);
        }

        private string GetTableKey1(Item item, bool is_staff, string[] properties)
        {
            return GetKeyValues(item, properties);
        }

        private string GetTableKey2(Item item, bool is_staff, string[] properties)
        {
            if (is_staff)
            {
                return item.getProperty("in_l1");
            }
            else
            {
                return item.getProperty("in_l1") + "-" + item.getProperty("in_l2");
            }
        }

        //取得樣板
        private TTemplate GetTemplate(TBox box)
        {
            TTemplate entity = new TTemplate();

            if (box.meeting_mode == "")
            {
                entity.Name = "organization";
                entity.SortOrder = 100;
            }
            else if (box.is_staff)
            {
                entity.Name = "staff";
                entity.SortOrder = 1000;
            }
            else if (box.in_l1.Contains("團體"))
            {
                entity.Name = "team";
                entity.SortOrder = 900;
            }
            else if (box.in_l1.Contains("混雙"))
            {
                entity.Name = "mix";
                entity.SortOrder = 200;
            }
            else if (box.in_l1.Contains("雙打"))
            {
                entity.Name = "due";
                entity.SortOrder = 300;
            }
            else if (box.in_l1.Contains("雙人"))
            {
                entity.Name = "due";
                entity.SortOrder = 400;
            }
            else
            {
                entity.Name = "single";
                entity.SortOrder = 100;
            }
            return entity;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string exe_type { get; set; }
            
            public bool isMeetingAdmin { get; set; }
            public bool isCommittee { get; set; }
            public bool isGymOwner { get; set; }
            public bool isGymAssistant { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmLogin { get; set; }

            public string in_creator_sno { get; set; }
            
            public string in_meeting_type { get; set; }
            public string in_member_type { get; set; }
            public bool is_team_bill { get; set; }
        }

        private int GetIntVal(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return 0;
            }

            int result = 0;
            Int32.TryParse(value, out result);
            return result;
        }

        private bool IsError(Item item, bool isSingle = true)
        {
            if (item.isError() || item.getResult() == "")
            {
                return true;
            }
            else if (isSingle && item.getItemCount() != 1)
            {
                return true;
            }
            else if (item.getItemCount() <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}