﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Seminar
{
    public class In_Cla_Meeting_Certificate_Update : Item
    {
        public In_Cla_Meeting_Certificate_Update(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: In_Cla_Meeting_Certificate_Update
    日期：
        - 2022-01-11 調整 (lina)
        - 2021-09-02 創建 (David)
*/

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Cla_Meeting_Certificate_Update";

            // 暫時增加權限
            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("source_id", ""),
            };

            Item itmMResumes = GetResumeSeminar(cfg);

            List<TMResume> list = MapMResumes(cfg, itmMResumes);

            //更新學員履歷
            Update_In_Cla_Meeting_Resume_HiddenColumnValues(cfg, list);

            //更新 各項服務記錄
            UpdateToRefereeGroupOrCoachGroup(cfg, list);

            //更新 年度記錄
            MergeResumeYears(cfg, list);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        /// <summary>
        /// 當資料被更新資後 根據審核類型 更新 IN_VALID, in_stat_0 欄位的值
        /// </summary>
        private void Update_In_Cla_Meeting_Resume_HiddenColumnValues(TConfig cfg, List<TMResume> list)
        {
            StringBuilder sb_Sqls = new StringBuilder();
            for (int i = 0; i < list.Count; i++)
            {
                TMResume entity = list[i];
                Item itmTec = entity.Value;

                string in_valid_type = itmTec.getProperty("in_valid_type", "").ToString().Trim();
                string id = itmTec.getID();
                string in_valid = ""; // 需發證
                string in_stat_0 = ""; // 可領證(是否合格)

                string sql = "";

                // set value
                switch (in_valid_type)
                {
                    case "":
                        in_valid = "0";
                        in_stat_0 = "X";
                        break;
                    case "合格":
                        in_valid = "1";
                        in_stat_0 = "O";
                        break;
                    case "不合格":
                        in_valid = "1";
                        in_stat_0 = "O";
                        break;
                    case "未到":
                        in_valid = "0";
                        in_stat_0 = "X";
                        break;
                    case "扣考":
                        in_valid = "0";
                        in_stat_0 = "X";
                        break;
                    case "更改報名":
                        in_valid = "0";
                        in_stat_0 = "X";
                        break;
                    case "由主辦單位提供(合格)":
                        in_valid = "1";
                        in_stat_0 = "O";
                        break;
                    case "由主辦單位提供(不合格)":
                        in_valid = "1";
                        in_stat_0 = "O";
                        break;
                    case "審核中":
                        in_valid = "0";
                        in_stat_0 = "X";
                        break;

                    default:
                        in_valid = "";
                        in_stat_0 = "";
                        break;
                }

                List<string> updates = new List<string>();
                if (0 < in_valid.Length)
                {
                    updates.Add(
                        string.Format(
                            @"
                    [IN_VALID] = '{0}' 
                    "
                            , in_valid
                        )
                    );
                }

                if (in_valid.Length > 0)
                {
                    updates.Add(
                        string.Format(
                            @"
                    [in_stat_0] = '{0}' 
                    "
                            , in_stat_0
                        )
                    );
                }

                if (updates.Count > 0)
                {
                    string updateString = "";
                    updateString = "SET " + String.Join(",", updates);

                    // in_stat_0
                    sql = string.Format(
                        @"
                    UPDATE In_Cla_Meeting_Resume
                    {0}
                    WHERE id = '{1}';
                "
                        , updateString
                        , id
                    );
                    sb_Sqls.Append(sql);
                }
            }

            string sqls = sb_Sqls.ToString();

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "show SQls" + sqls);

            cfg.inn.applySQL(sqls);
        }

        private void UpdateToRefereeGroupOrCoachGroup(TConfig cfg, List<TMResume> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                TMResume entity = list[i];

                //學員編號必須 有值
                if (entity.in_name_num.Equals("")) continue;

                string itemType = ""; // 對應ItemName
                string action = "";

                string in_level = entity.in_level;

                switch (entity.in_seminar_type)
                {
                    case "coach"://國內教練
                        itemType = "In_Resume_Coach";
                        in_level = GetMixLevel(entity.in_name_num, in_level);
                        break;

                    case "coach_gl"://國際教練
                        itemType = "In_Resume_Coach_Gl";
                        in_level = GetMixLevel(entity.in_name_num, in_level);
                        break;

                    case "coach_enr"://國內教練增能
                        itemType = "In_Resume_Coach";
                        break;

                    case "coach_enr_gl"://國際教練增能能
                        itemType = "In_Resume_Coach_Gl";
                        break;

                    case "referee"://國內對打裁判
                        itemType = "In_Resume_Referee";
                        in_level = GetMixLevel(entity.in_name_num, in_level);
                        break;

                    case "referee_gl"://國際對打裁判
                        itemType = "In_Resume_Referee_Gl";
                        in_level = GetMixLevel(entity.in_name_num, in_level);
                        break;

                    case "poomsae"://國內品勢裁判
                        itemType = "In_Resume_Poomsae";
                        in_level = GetMixLevel(entity.in_name_num, in_level);
                        break;

                    case "poomsae_gl"://國際品勢裁判
                        itemType = "In_Resume_Poomsae_Gl";
                        in_level = GetMixLevel(entity.in_name_num, in_level);
                        break;

                    default:
                        throw new Exception("講習類型未設定");
                }

                entity.in_level = in_level;

                if (CheckIsPassStatus_In_Valid_Type(entity.in_valid_type))
                {
                    action = "merge";
                }
                else
                {
                    action = "delete";
                }

                string condition = "source_id = '" + entity.in_resume_id + "'"
                    + " AND in_seminar_type = '" + entity.in_seminar_type + "'"
                    + " AND in_year = '" + entity.west_annual + "'"
                    + " AND in_level = '" + entity.in_level + "'"
                    + " AND in_echelon = '" + entity.in_echelon + "'"
                    + "";

                // string condition = "source_id = '" + entity.in_resume_id + "'"
                //     + " AND in_cla_user = '" + entity.muid + "'"
                //     + "";

                Item ItemMerge = cfg.inn.newItem(itemType, action);
                ItemMerge.setAttribute("where", condition);
                ItemMerge.setProperty("source_id", entity.in_resume_id);
                ItemMerge.setProperty("in_cla_user", entity.muid);

                //是否發證 //待確認 
                if (entity.in_stat_0.Equals("O"))
                {
                    ItemMerge.setProperty("in_certificate", "1");
                }
                else
                {
                    ItemMerge.setProperty("in_certificate", "0");
                }


                ItemMerge.setProperty("in_certificate_no", entity.in_certificate_no);//證號

                if (!entity.in_certificate_date.Equals(""))
                {
                    //登錄日期
                    ItemMerge.setProperty("in_date", entity.in_certificate_date);
                }

                ItemMerge.setProperty("in_seminar_type", entity.in_seminar_type);
                ItemMerge.setProperty("in_year", entity.west_annual);//西元年度
                ItemMerge.setProperty("in_level", entity.in_level);//級別(活動主檔)
                ItemMerge.setProperty("in_echelon", entity.in_echelon);//梯次
                ItemMerge.setProperty("in_evaluation", entity.in_year_note);//本年度講習紀錄
                ItemMerge.setProperty("in_hours", entity.in_coursh_hours);//時數
                ItemMerge.setProperty("in_know_score", entity.in_score_o_5);//學科成績
                ItemMerge.setProperty("in_tech_score", entity.in_score_o_3);//術科成績
                ItemMerge.setProperty("in_note", entity.in_note);//備註
                ItemMerge.setProperty("in_has_muser", "1");//是否有與會者

                Item itmResult = ItemMerge.apply();

                if (itmResult.isError())
                {
                    throw new Exception("建立[" + itemType + "]發生錯誤");
                }
            }
        }

        private void MergeResumeYears(TConfig cfg, List<TMResume> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                TMResume entity = list[i];

                //學員編號必須 有值
                if (entity.in_name_num.Equals(""))
                {
                    continue;
                }

                string action = "";

                if (CheckIsPassStatus_In_Valid_Type(entity.in_valid_type))
                {
                    action = "merge";
                }
                else
                {
                    action = "delete";
                }

                string condition = "source_id = '" + entity.in_resume_id + "'"
                    + " AND in_type = '" + entity.in_seminar_type + "'"
                    + " AND in_year = '" + entity.west_annual + "'"
                    + "";

                Item itmRYear = cfg.inn.newItem("IN_RESUME_YEARS", action);
                itmRYear.setAttribute("where", condition);
                itmRYear.setProperty("source_id", entity.in_resume_id);
                itmRYear.setProperty("in_cla_user", entity.muid);

                if (entity.in_certificate_date != "")
                {
                    itmRYear.setProperty("in_date", entity.in_certificate_date);//登錄日期
                }

                itmRYear.setProperty("in_type", entity.in_seminar_type);//類型
                itmRYear.setProperty("in_year", entity.west_annual);//西元年度
                itmRYear.setProperty("in_note", entity.in_note);//備註

                //ItemMerge.setProperty("in_company", "");//公司別
                // itmRYear.setProperty("in_old_no", "");//記錄編號
                // itmRYear.setProperty("in_old_property", ""); //記錄欄位
                // itmRYear.setProperty("in_service", "");//裁判服務

                Item itmResult = itmRYear.apply();

                if (itmResult.isError())
                {
                    throw new Exception("建立服務年度發生錯誤");
                }
            }
        }

        private List<TMResume> MapMResumes(TConfig cfg, Item itmMResumes)
        {
            List<TMResume> result = new List<TMResume>();

            int count = itmMResumes.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmTec = itmMResumes.getItemByIndex(i);

                var entity = new TMResume();

                entity.in_resume_id = PropVal(itmTec, "in_resume_id"); // (講師履歷ID)IN_RESUME_ID 
                entity.in_annual = PropVal(itmTec, "in_annual", defv: "0"); // 年度(民國) 
                entity.west_annual = (GetIntVal(entity.in_annual) + 1911).ToString(); // 年度(西元) 
                entity.in_seminar_type = PropVal(itmTec, "in_seminar_type"); // 講習類型
                entity.in_coursh_hours = PropVal(itmTec, "in_coursh_hours"); // 修課時數
                entity.in_echelon = PropVal(itmTec, "in_echelon"); // 梯次
                entity.in_level = PropVal(itmTec, "in_level"); // 級別(級數) 
                entity.in_valid = PropVal(itmTec, "in_valid"); // 需發證(是否結業) 
                entity.in_stat_0 = PropVal(itmTec, "in_stat_0"); // 可領證(是否合格) 
                entity.in_certificate_no = PropVal(itmTec, "in_certificate_no"); // 證書號碼
                entity.in_score_o_5 = PropVal(itmTec, "in_score_o_5"); // 學科成績
                entity.in_score_o_3 = PropVal(itmTec, "in_score_o_3"); // 術科成績
                entity.in_year_note = PropVal(itmTec, "in_year_note"); // 本年度講習紀錄
                entity.in_note = PropVal(itmTec, "in_note"); // 備註說明

                entity.in_certificate_date = PropVal(itmTec, "in_certificate_date"); // 發證日期

                if (entity.in_certificate_date != "")
                {
                    entity.in_certificate_date = DateTimeStr(entity.in_certificate_date);
                }

                entity.id = PropVal(itmTec, "id"); // In_Cla_Meeting_Resume.ID
                entity.in_valid_type = PropVal(itmTec, "in_valid_type"); // 審核類型

                entity.meeting_id = PropVal(itmTec, "source_id"); // 對應課程
                entity.muid = PropVal(itmTec, "muid"); // 與會者 id
                entity.in_name_num = PropVal(itmTec, "in_name_num"); // 與會者 學員編號

                entity.Value = itmTec;

                result.Add(entity);
            }

            return result;
        }


        /// <summary>
        /// 講習紀錄
        /// </summary>
        private Item GetResumeSeminar(TConfig cfg)
        {
            //IN_RESUME_ID = (講師履歷ID)IN_RESUME_ID 
            //in_year = 年度(西元) 
            //in_seminar_type = 講習類型 
            //in_coursh_hours = 修課時數 
            //in_echelon = 梯次 
            //in_level = 級別(級數) 
            //source_id = 對應課程 
            //in_valid = 需發證(是否結業) 
            //in_stat_0 = 可領證(是否合格) 
            //in_certificate_no = 證書號碼 
            //in_score_o_5 = 學科成績 
            //in_score_o_3 = 術科成績 
            //in_year_note = 本年度講習紀錄 
            //in_note = 備註說明 
            //in_certificate_date = 發證日期
            //ID = In_Cla_Meeting_ResumeID
            //in_valid_type = 審核類型
            //IN_NAME_NUM = 學員編號

            string sql = @"
                SELECT 
                    t1.id
                    , t1.source_id
                    , t1.in_valid
                    , t1.in_stat_0
                    , t1.in_score_o_5
                    , t1.in_score_o_3
                    , t1.in_year_note
                    , t1.in_note
                    , t1.in_certificate_no
                    , t1.in_certificate_level
                    , t1.in_certificate_date
                    , t1.in_valid_type
                    , t2.id              AS 'muid'
                    , t2.in_name_num
                    , t11.id as 'IN_RESUME_ID'
                    , t12.in_annual
                    , t12.in_meeting_type
                    , t12.in_seminar_type
                    , t12.in_coursh_hours
                    , t12.in_echelon
                    , t12.in_level
                FROM 
			        In_Cla_Meeting_Resume t1 WITH(NOLOCK)
                INNER JOIN
			        In_Cla_Meeting_User t2 WITH(NOLOCK) 
                    ON t2.id = t1.in_user
                LEFT OUTER JOIN 
			        In_Cla_Meeting_Technical t3　WITH(NOLOCK)　
                    ON t3.in_user = t1.in_user
                LEFT OUTER JOIN 
			        IN_RESUME t11 WITH(NOLOCK) 
                    ON t11.in_sno = t2.in_sno
                LEFT OUTER JOIN 
			        In_Cla_Meeting t12 WITH(NOLOCK) 
                    on t12.id = t2.source_id
                WHERE 
			        t1.source_id = '{0}'
                ORDER BY 
			        ISNULL(t2.in_name_num, -1)
            ";

            sql = string.Format(sql, cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string meeting_id { get; set; }
        }

        private class TMResume
        {
            public string id { get; set; }

            public string in_resume_id { get; set; }
            public string in_seminar_type { get; set; }
            public string in_coursh_hours { get; set; }

            public string in_level { get; set; }
            public string in_echelon { get; set; }
            public string in_stat_0 { get; set; }
            public string in_score_o_5 { get; set; }
            public string in_score_o_3 { get; set; }
            public string in_year_note { get; set; }
            public string in_note { get; set; }
            public string in_certificate_no { get; set; }
            public string in_certificate_date { get; set; }
            public string in_valid { get; set; }
            public string in_valid_type { get; set; }

            public string meeting_id { get; set; }
            public string in_annual { get; set; }
            public string west_annual { get; set; }

            public string muid { get; set; }
            public string in_name_num { get; set; }

            public Item Value { get; set; }
        }

        /// <summary>
        /// 檢查審查狀態是否為通過的狀態
        /// </summary>
        /// <returns></returns>
        private bool CheckIsPassStatus_In_Valid_Type(string in_valid_type)
        {
            string stringToCheck = in_valid_type;
            string[] stringArray = { "合格", "不合格", "由主辦單位提供(合格)", "由主辦單位提供(不合格)" };

            foreach (string v in stringArray)
            {
                if (stringToCheck.Equals(v))
                {
                    return true;
                }
            }

            return false;
        }

        private string PropVal(Item item, string prop, string defv = "")
        {
            return item.getProperty(prop, defv).ToString().Trim();
        }

        //根據學員編號設定級數: 同一堂課中，有 ABC 三種級數，EX 品勢
        private string GetMixLevel(string in_name_num, string in_level)
        {
            if (in_name_num == "") return in_level;

            var c1 = in_name_num.ToUpper()[0];

            switch (c1)
            {
                case 'A': return "A級";
                case 'B': return "B級";
                case 'C': return "C級";
                default: return in_level;
            }
        }

        private string DateTimeStr(string value, string format = "yyyy-MM-dd")
        {
            DateTime result = DateTime.MinValue;
            if (DateTime.TryParse(value, out result))
            {
                return result.AddHours(0).ToString(format);
            }
            return "";
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}