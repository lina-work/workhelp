﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Seminar
{
    public class In_Cla_Meeting_User_Merge : Item
    {
        public In_Cla_Meeting_User_Merge(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 講習變更
    日誌: 
        - 2024-09-05: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Cla_Meeting_User_Merge";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                muid = itmR.getProperty("muid", ""),
                in_name = itmR.getProperty("in_name", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "move_meeting":
                    ChangeMeeting(cfg, itmR);
                    break;
                case "copy_meeting":
                    ChangeMeeting(cfg, itmR);
                    break;

                case "modal":
                    Modal(cfg, itmR);
                    break;

                case "find_player":
                    FindPlayer(cfg, itmR);
                    break;

                case "find_org_player":
                    FindOrgPlayer(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void ChangeMeeting(TConfig cfg, Item itmReturn)
        {
            var old_meeting_id = cfg.meeting_id;
            var new_meeting_id = itmReturn.getProperty("new_meeting_id", "");

            var itmOldMUser = cfg.inn.newItem("In_Cla_Meeting_User", "get");
            itmOldMUser.setProperty("id", cfg.muid);
            itmOldMUser = itmOldMUser.apply();

            if (itmOldMUser.isError() || itmOldMUser.getResult() == "") throw new Exception("查無與會者資料");

            var itmOldMeeting = cfg.inn.applySQL("SELECT id, in_title FROM IN_CLA_MEETING WITH(NOLOCK) WHERE id = '" + old_meeting_id + "'");
            if (itmOldMeeting.isError() || itmOldMeeting.getResult() == "") throw new Exception("查無原活動資料");

            var itmNewMeeting = cfg.inn.applySQL("SELECT id, in_title FROM IN_CLA_MEETING WITH(NOLOCK) WHERE id = '" + new_meeting_id + "'");
            if (itmNewMeeting.isError() || itmNewMeeting.getResult() == "") throw new Exception("查無新活動資料");

            var x = new TMUser
            {
                muid = cfg.muid,
                new_meeting_id = new_meeting_id,
                new_meeting_title = itmNewMeeting.getProperty("in_title", ""),
                old_meeting_id = old_meeting_id,
                old_meeting_title = itmOldMeeting.getProperty("in_title", ""),
                in_name = itmOldMUser.getProperty("in_name", ""),
                in_sno = itmOldMUser.getProperty("in_sno", "").ToUpper(),
                in_mail = itmOldMUser.getProperty("in_mail", ""),
                in_l1 = itmOldMUser.getProperty("in_l1", ""),
                in_paynumber = itmOldMUser.getProperty("in_paynumber", ""),
                pay_id = "",
                pay_detail_id = "",
                pay_keyed_name_old = "",
                pay_keyed_name_new = "",
                action = "",
                new_index = "",
                new_muid = "",
                itmOldMUser = itmOldMUser,
                itmNewMUser = cfg.inn.newItem(),
            };

            if (cfg.scene == "move_meeting") x.action = "搬移";
            if (cfg.scene == "copy_meeting") x.action = "複製";

            var sql = "SELECT source_id, id, in_name, in_sno, in_paynumber FROM IN_CLA_MEETING_USER WITH(NOLOCK) WHERE source_id = '" + x.new_meeting_id + "' AND in_mail = '" + x.in_mail + "'";
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            x.itmNewMUser = cfg.inn.applySQL(sql);
            if (x.itmNewMUser.getResult() != "") throw new Exception(x.in_name + " 已報名 " + x.new_meeting_title);

            //當有繳費單，檢查繳費單是否只有一人，並且是同個人
            if (x.in_paynumber != "")
            {
                var itmPay = cfg.inn.applySQL("SELECT TOP 1 id, keyed_name, pay_bool FROM IN_MEETING_PAY WITH(NOLOCK) WHERE in_cla_meeting = '" + x.old_meeting_id + "' AND item_number = '" + x.in_paynumber + "'");
                if (itmPay.isError() || itmPay.getResult() == "") throw new Exception("繳費單資料錯誤");

                x.pay_id = itmPay.getProperty("id", "");
                x.pay_keyed_name_old = itmPay.getProperty("keyed_name", "");
                x.pay_keyed_name_new = x.new_meeting_title + " " + x.in_sno + " " + x.in_name;

                var itmPayDetails = cfg.inn.applySQL("SELECT * FROM IN_MEETING_NEWS WITH(NOLOCK) WHERE source_id = '" + x.pay_id + "'");
                if (itmPayDetails.isError() || itmPayDetails.getResult() == "") throw new Exception("繳費單明細錯誤");

                var detailCount = itmPayDetails.getItemCount();
                if (detailCount > 1) throw new Exception("繳費單明細不只一人，無法變更繳費單資料");

                var detail_sno = itmPayDetails.getProperty("in_sno", "").ToUpper();
                if (detail_sno != x.in_sno) throw new Exception("繳費單明細身分證號不同，請檢查繳費單與報名者資料 _# " + x.in_paynumber);

                x.pay_detail_id = itmPayDetails.getProperty("id", "");
            }

            var w1 = "學員[" + x.in_name + "](" + x.muid + ")";
            var w2 = "原活動[" + x.old_meeting_title + "](" + x.old_meeting_id + ")";
            var w3 = "新活動[" + x.new_meeting_title + "](" + x.new_meeting_id + ")";
            var w4 = x.in_paynumber == "" ? "" : "，繳費單[" + x.in_paynumber + "](" + x.pay_id + "): " + x.pay_keyed_name_old;
            var message = w1 + "從" + w2 + x.action + "至" + w3 + w4;
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, message);

            if (cfg.scene == "move_meeting")
            {
                MoveMeetingUser(cfg, x, itmReturn);
            }
            else if (cfg.scene == "copy_meeting")
            {
                CopyMeetingUser(cfg, x, itmReturn);
                ResetMeetingPay(cfg, x, itmReturn);
            }
        }

        private void ResetMeetingPay(TConfig cfg, TMUser x, Item itmReturn)
        {
            if (x.pay_detail_id == "") return;

            var sql1 = "UPDATE IN_MEETING_NEWS SET"
                + "  in_regdate = getutcdate()"
                + ", in_index = '" + x.new_index + "'"
                + ", in_muid = '" + x.new_muid + "'"
                + " WHERE id = '" + x.pay_detail_id + "'"
                ;

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql1);
            cfg.inn.applySQL(sql1);


            var sql2 = "UPDATE IN_MEETING_PAY SET"
                + "  keyed_name = '" + x.pay_keyed_name_new + "'"
                + ", in_cla_meeting = '" + x.new_meeting_id + "'"
                + " WHERE in_cla_meeting = '" + x.old_meeting_id + "'"
                + " AND id = '" + x.pay_id + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql2);
            cfg.inn.applySQL(sql2);
        }

        private void CopyMeetingUser(TConfig cfg, TMUser x, Item itmReturn)
        {
            var itmOld = x.itmOldMUser;
            x.new_index = GetLevelMaxIndex(cfg, x.new_meeting_id, itmOld);

            var itmNew = cfg.inn.newItem("In_Cla_Meeting_User", "add");
            itmNew.setProperty("source_id", x.new_meeting_id);
            CopyItemValue(itmNew, itmOld, "related_id");
            itmNew.setProperty("in_index", x.new_index);

            var now = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
            itmNew.setProperty("in_regdate", now);
            itmNew.setProperty("in_confirm_mail", now);

            CopyItemValue(itmNew, itmOld, "in_name");
            CopyItemValue(itmNew, itmOld, "in_gender");
            CopyItemValue(itmNew, itmOld, "in_birth");
            CopyItemValue(itmNew, itmOld, "in_sno");
            CopyItemValue(itmNew, itmOld, "in_sno_display");
            CopyItemValue(itmNew, itmOld, "in_mail");

            CopyItemValue(itmNew, itmOld, "in_creator");
            CopyItemValue(itmNew, itmOld, "in_creator_sno");
            CopyItemValue(itmNew, itmOld, "in_current_org");
            CopyItemValue(itmNew, itmOld, "in_group");
            CopyItemValue(itmNew, itmOld, "in_committee");
            CopyItemValue(itmNew, itmOld, "in_committee_sno");

            CopyItemValue(itmNew, itmOld, "in_note_state");
            CopyItemValue(itmNew, itmOld, "in_note_state_pre");

            CopyItemValue(itmNew, itmOld, "in_gameunit");
            CopyItemValue(itmNew, itmOld, "in_section_name");
            CopyItemValue(itmNew, itmOld, "in_l1");
            CopyItemValue(itmNew, itmOld, "in_l2");
            CopyItemValue(itmNew, itmOld, "in_l3");
            CopyItemValue(itmNew, itmOld, "in_l1_sort");
            CopyItemValue(itmNew, itmOld, "in_expense");
            CopyItemValue(itmNew, itmOld, "in_paynumber");//待確認
            CopyItemValue(itmNew, itmOld, "in_paytime");//待確認
            //CopyItemValue(itmNew, itmOld, "in_name_num");//不複製

            CopyItemValue(itmNew, itmOld, "in_tel");
            CopyItemValue(itmNew, itmOld, "in_tel_1");
            CopyItemValue(itmNew, itmOld, "in_email");
            CopyItemValue(itmNew, itmOld, "in_add");
            CopyItemValue(itmNew, itmOld, "in_add_code");
            CopyItemValue(itmNew, itmOld, "in_area");
            CopyItemValue(itmNew, itmOld, "in_resident_add_code");
            CopyItemValue(itmNew, itmOld, "in_resident_add");
            CopyItemValue(itmNew, itmOld, "in_lunch");
            CopyItemValue(itmNew, itmOld, "in_blood_type");
            CopyItemValue(itmNew, itmOld, "in_emrg_contact1");
            CopyItemValue(itmNew, itmOld, "in_emrg_tel1");
            CopyItemValue(itmNew, itmOld, "in_education");
            CopyItemValue(itmNew, itmOld, "in_work_org");
            CopyItemValue(itmNew, itmOld, "in_work_history");
            CopyItemValue(itmNew, itmOld, "in_job");
            CopyItemValue(itmNew, itmOld, "in_title");
            CopyItemValue(itmNew, itmOld, "in_photo1");
            CopyItemValue(itmNew, itmOld, "in_country");

            CopyItemValue(itmNew, itmOld, "in_aborigine_yn");
            CopyItemValue(itmNew, itmOld, "in_aborigine_tribe");
            CopyItemValue(itmNew, itmOld, "in_new_immigrant_yn");
            CopyItemValue(itmNew, itmOld, "in_new_immigrant_country");

            CopyItemValue(itmNew, itmOld, "in_exe_a1");
            CopyItemValue(itmNew, itmOld, "in_exe_a2");
            CopyItemValue(itmNew, itmOld, "in_exe_a3");
            CopyItemValue(itmNew, itmOld, "in_exe_a4");
            CopyItemValue(itmNew, itmOld, "in_exe_a5");
            CopyItemValue(itmNew, itmOld, "in_exe_a6");
            CopyItemValue(itmNew, itmOld, "in_exe_a7");
            CopyItemValue(itmNew, itmOld, "in_exe_a8");
            CopyItemValue(itmNew, itmOld, "in_exe_a9");

            CopyItemValue(itmNew, itmOld, "in_stuff_c1");
            CopyItemValue(itmNew, itmOld, "in_stuff_c2");
            CopyItemValue(itmNew, itmOld, "in_stuff_c4");
            CopyItemValue(itmNew, itmOld, "in_stuff_c5");

            CopyItemValue(itmNew, itmOld, "in_guardian");

            CopyItemValue(itmNew, itmOld, "in_en_name");

            CopyItemValue(itmNew, itmOld, "in_degree");
            CopyItemValue(itmNew, itmOld, "in_degree_label");
            CopyItemValue(itmNew, itmOld, "in_degree_id");
            CopyItemValue(itmNew, itmOld, "in_degree_area");
            CopyItemValue(itmNew, itmOld, "in_degree_date");
            CopyItemValue(itmNew, itmOld, "in_gl_degree_id");
            CopyItemValue(itmNew, itmOld, "in_poomsae_id");
            CopyItemValue(itmNew, itmOld, "in_poomsae_level");
            CopyItemValue(itmNew, itmOld, "in_referee_id");
            CopyItemValue(itmNew, itmOld, "in_date");
            CopyItemValue(itmNew, itmOld, "in_is_gl_degree");


            CopyItemValue(itmNew, itmOld, "in_booking");
            CopyItemValue(itmNew, itmOld, "in_booking_note");
            CopyItemValue(itmNew, itmOld, "in_retraining");

            CopyItemValue(itmNew, itmOld, "in_official_leave_org");
            CopyItemValue(itmNew, itmOld, "in_official_leave_add");
            CopyItemValue(itmNew, itmOld, "in_official_leave_code");
            CopyItemValue(itmNew, itmOld, "in_official_leave_note");

            CopyItemValue(itmNew, itmOld, "in_is_changed");
            CopyItemValue(itmNew, itmOld, "in_cert_add");
            CopyItemValue(itmNew, itmOld, "in_cert_code");
            CopyItemValue(itmNew, itmOld, "in_re_bank_acc");
            CopyItemValue(itmNew, itmOld, "in_re_bank_code");
            CopyItemValue(itmNew, itmOld, "in_echelon");

            CopyItemValue(itmNew, itmOld, "in_cert_yn");
            CopyItemValue(itmNew, itmOld, "in_cert_id");
            CopyItemValue(itmNew, itmOld, "in_cert_date");
            CopyItemValue(itmNew, itmOld, "in_cert_level");

            CopyItemValue(itmNew, itmOld, "in_tcon");

            itmNew = itmNew.apply();
            x.new_muid = itmNew.getID();
        }

        private void MoveMeetingUser(TConfig cfg, TMUser x, Item itmReturn)
        {
            var sql1 = "UPDATE IN_CLA_MEETING_USER SET"
                + " source_id = '" + x.new_meeting_id + "'"
                + " WHERE source_id = '" + x.old_meeting_id + "'"
                + " AND id = '" + x.muid + "'";

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql1);

            cfg.inn.applySQL(sql1);

            if (x.in_paynumber == "") return;

            var sql2 = "UPDATE IN_MEETING_PAY SET"
                + "  keyed_name = '" + x.pay_keyed_name_new + "'"
                + ", in_cla_meeting = '" + x.new_meeting_id + "'"
                + " WHERE in_cla_meeting = '" + x.old_meeting_id + "'"
                + " AND id = '" + x.pay_id + "'";

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql2);

            cfg.inn.applySQL(sql2);
        }

        //取得該組當前最大序號
        private static string GetLevelMaxIndex(TConfig cfg, string meeting_id, Item applicant)
        {
            string sql = "";

            string in_l1 = applicant.getProperty("in_l1", "");

            sql = "SELECT max(in_index) as c1 FROM IN_CLA_MEETING_USER WITH(NOLOCK) WHERE source_id = '" + meeting_id + "'";
            sql += " AND in_l1 = N'" + in_l1 + "'";

            var itmIndex = cfg.inn.applySQL(sql);
            if (itmIndex.getResult() == "")
            {
                return "00001";
            }
            else
            {
                int intIndex = int.Parse(itmIndex.getProperty("c1", "1")) + 1;
                return intIndex.ToString("00000");
            }
        }

        private void Modal(TConfig cfg, Item itmReturn)
        {
            cfg.itmMeeting = cfg.inn.applySQL("SELECT id, in_title, in_meeting_type FROM IN_CLA_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            var in_meeting_type = cfg.itmMeeting.getProperty("in_meeting_type", "");

            //活動資訊
            AppendMeeting(cfg, itmReturn);

            var itmMUser = FindMeetingUser(cfg, cfg.muid, "");
            itmReturn.setProperty("muid", cfg.muid);
            CopyItemValue(itmReturn, itmMUser, "in_current_org");
            CopyItemValue(itmReturn, itmMUser, "in_name");
            CopyItemValue(itmReturn, itmMUser, "in_gender");
            CopyItemValue(itmReturn, itmMUser, "in_sno");
            CopyItemValue(itmReturn, itmMUser, "in_sno_display");
            CopyItemValue(itmReturn, itmMUser, "reg_birth");
            CopyItemValue(itmReturn, itmMUser, "in_tel");
            CopyItemValue(itmReturn, itmMUser, "in_email");
            CopyItemValue(itmReturn, itmMUser, "in_l1");
            CopyItemValue(itmReturn, itmMUser, "in_index");
            CopyItemValue(itmReturn, itmMUser, "in_paynumber");
            CopyItemValue(itmReturn, itmMUser, "in_expense");
            CopyItemValue(itmReturn, itmMUser, "in_ass_ver_memo");
            CopyItemValue(itmReturn, itmMUser, "in_creator");
            CopyItemValue(itmReturn, itmMUser, "pay_bool");

            var sql = @"
                SELECT 
	                id
	                , in_title
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, in_state_start), 111)	AS 'seminar_day'
	                , in_real_taking
	                , in_meeting_type
                FROM 
	                IN_CLA_MEETING WITH(NOLOCK) 
                WHERE 
	                in_is_main = 0 
	                AND in_is_template = 0 
                    AND in_meeting_type = '{#in_meeting_type}'
	                AND in_state_start >= DATEADD(YEAR, -1, GETUTCDATE())
                ORDER BY 
	                in_state_start DESC
            ";

            sql = sql.Replace("{#in_meeting_type}", in_meeting_type);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType("inn_meeting");
                item.setProperty("no", (i + 1).ToString());
                itmReturn.addRelationship(item);
            }
        }

        private void CopyItemValue(Item itmA, Item itmB, string propA, string propB = "")
        {
            if (propB == "")
            {
                itmA.setProperty(propA, itmB.getProperty(propA, ""));
            }
            else
            {
                itmA.setProperty(propA, itmB.getProperty(propB, ""));
            }
        }

        //活動資訊
        private void AppendMeeting(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("modal_title", cfg.itmMeeting.getProperty("in_title", ""));
        }

        private void FindPlayer(TConfig cfg, Item itmReturn)
        {
            var items = FindMeetingUser(cfg, "", cfg.in_name);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType("inn_muser");
                item.setProperty("no", (i + 1).ToString());
                itmReturn.addRelationship(item);
            }
        }

        private void FindOrgPlayer(TConfig cfg, Item itmReturn)
        {
            var items = FindMeetingOrgUser(cfg, cfg.in_name);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType("inn_muser");
                item.setProperty("no", (i + 1).ToString());
                itmReturn.addRelationship(item);
            }
        }

        private Item FindMeetingOrgUser(TConfig cfg, string in_current_org)
        {
            var cond = "AND t1.in_current_org LIKE N'%" + in_current_org + "%'";

            var sql = @"
                SELECT 
	                t1.id
	                , t1.in_current_org
	                , t1.in_name
	                , t1.in_gender
	                , t1.in_sno
                    , t1.in_sno_display
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, t1.in_birth), 111)	AS 'reg_birth'
                    , t1.in_tel
                    , t1.in_email
	                , t1.in_l1
	                , t1.in_index
	                , t1.in_paynumber
                    , t1.in_expense
                    , t1.in_ass_ver_memo
	                , t1.in_creator
	                , t1.in_creator_sno
	                , t2.in_password_plain
	                , t3.pay_bool
                FROM 
	                IN_CLA_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK) ON t2.in_sno = t1.in_creator_sno
                LEFT OUTER JOIN
	                IN_MEETING_PAY t3 WITH(NOLOCK) ON t3.item_number = t1.in_paynumber
                WHERE 
	                t1.source_id = '{#meeting_id}'
	                {#cond}
                ORDER BY
	                t1.in_creator_sno
	                , t1.in_name
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#cond}", cond);

            return cfg.inn.applySQL(sql);
        }

        private Item FindMeetingUser(TConfig cfg, string muid, string in_name)
        {
            var cond = muid == ""
                ? "AND t1.in_name LIKE N'%" + in_name + "%'"
                : "AND t1.id = '" + muid + "'";

            var sql = @"
                SELECT 
	                t1.id
	                , t1.in_current_org
	                , t1.in_name
	                , t1.in_gender
	                , t1.in_sno
                    , t1.in_sno_display
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, t1.in_birth), 111)	AS 'reg_birth'
                    , t1.in_tel
                    , t1.in_email
	                , t1.in_l1
	                , t1.in_index
	                , t1.in_paynumber
                    , t1.in_expense
                    , t1.in_ass_ver_memo
	                , t1.in_creator
	                , t1.in_creator_sno
	                , t2.in_password_plain
	                , t3.pay_bool
                FROM 
	                IN_CLA_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK) ON t2.in_sno = t1.in_creator_sno
                LEFT OUTER JOIN
	                IN_MEETING_PAY t3 WITH(NOLOCK) ON t3.item_number = t1.in_paynumber
                WHERE 
	                t1.source_id = '{#meeting_id}'
	                {#cond}
                ORDER BY
	                t1.in_creator_sno
	                , t1.in_name
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#cond}", cond);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string muid { get; set; }
            public string in_name { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
        }

        private class TMUser
        {
            public string muid { get; set; }
            public string new_meeting_id { get; set; }
            public string new_meeting_title { get; set; }
            public string old_meeting_id { get; set; }
            public string old_meeting_title { get; set; }
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_mail { get; set; }
            public string in_l1 { get; set; }
            public string in_paynumber { get; set; }
            public string pay_id { get; set; }
            public string pay_detail_id { get; set; }
            public string pay_keyed_name_old { get; set; }
            public string pay_keyed_name_new { get; set; }
            
            public string action { get; set; }

            public Item itmOldMUser { get; set; }
            public Item itmNewMUser { get; set; }

            public string new_muid { get; set; }
            public string new_index { get; set; }
        }

        private int GetInt(string value)
        {
            if (value == "") return 0;
            var result = 0;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return 0;
        }

        private DateTime GetDtm(string value)
        {
            if (value == "") return DateTime.MinValue;
            var result = DateTime.MinValue;
            if (DateTime.TryParse(value, out result))
            {
                return result;
            }
            return DateTime.MinValue;
        }
    }
}