﻿using Aras.IOM;
using System.Collections.Generic;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.證照
{
    internal class CertificateDateFix : Item
    {
        public CertificateDateFix(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 發證日期修復
                日誌: 
                    - 2024-12-05: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "CertificateDateFix";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            if (!isMeetingAdmin) return itmR;

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                in_type = itmR.getProperty("in_type", ""),
                scene = itmR.getProperty("scene", ""),
            };

            if (cfg.scene == "") cfg.scene = "page";
            if (cfg.in_type == "") cfg.scene = "教練證";

            switch (cfg.scene)
            {
                case "page":
                    if (cfg.in_type == "教練證")
                    {
                        CoachPage(cfg, itmR);
                    }
                    else if (cfg.in_type == "裁判證")
                    {
                        RefereePage(cfg, itmR);
                    }
                    break;
                case "update_cert_date":
                    UpdateCertDate(cfg, itmR);
                    break;
            }
            return itmR;
        }

        private void UpdateCertDate(TConfig cfg, Item itmReturn)
        {
            var id = itmReturn.getProperty("id", "");
            var prop = itmReturn.getProperty("property", "");
            var value = itmReturn.getProperty("value", "");
            var sql = "";
            switch (prop)
            {
                case "in_instructor_date":
                case "in_instructor_expired":
                    sql = "UPDATE [IN_RESUME] SET [" + prop + "] = CONVERT(VARCHAR, DATEADD(HOUR, -8, '" + value + "'), 120) WHERE [id] = '" + id + "'";
                    break;
            }

            if (sql != "")
            {
                //throw new Exception(sql);
                cfg.inn.applySQL(sql);
            }
        }

        private void CoachPage(TConfig cfg, Item itmReturn)
        {
            var items = GetResumeCoachItems(cfg);
            RunPage(cfg, items, "instructor", itmReturn);
        }

        private void RefereePage(TConfig cfg, Item itmReturn)
        {
            var items = GetResumeCoachItems(cfg);
            RunPage(cfg, items, "referee", itmReturn);
        }

        private void RunPage(TConfig cfg, Item items, string suffix, Item itmReturn)
        {
            var map = new Dictionary<string, Item>();
            var count = items.getItemCount();

            var total_count = "0";
            if (count > 0) total_count = count.ToString("###,###");
            itmReturn.setProperty("total_count", total_count);

            var no = 1;
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_name = item.getProperty("in_name", "");
                var login_name = item.getProperty("login_name", "");
                var cert_id = item.getProperty("cert_id", "");
                var show_name = in_name + "<BR>" + login_name + "<BR>" + cert_id;

                if (map.ContainsKey(login_name)) continue;
                map.Add(login_name, item);

                item.setType("inn_resume");
                item.setProperty("no", (no).ToString());
                item.setProperty("show_name", show_name);
                item.setProperty("inn_id_prop", "in_" + suffix + "_id");
                item.setProperty("inn_dt_prop", "in_" + suffix + "_date");
                item.setProperty("inn_ep_prop", "in_" + suffix + "_expired");
                itmReturn.addRelationship(item);

                no++;
            }

            itmReturn.setProperty("inn_id_prop", "in_" + suffix + "_id");
            itmReturn.setProperty("inn_dt_prop", "in_" + suffix + "_date");
            itmReturn.setProperty("inn_ep_prop", "in_" + suffix + "_expired");
        }

        private Item GetResumeRefereeItems(TConfig cfg)
        {
            var sql = @"
                SELECT
	                t1.id
	                , t1.in_name
	                , UPPER(t1.login_name) AS 'login_name'
	                , t1.in_referee_id AS 'cert_id'
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, t1.in_referee_date), 111) AS 'in_instructor_date'
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, t1.in_referee_expired), 111) AS 'in_instructor_expired'
	                , t2.in_file1
	                , t2.in_file2
	                , t2.created_on
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, t1.created_on), 111) AS 'created_on'
                FROM
	                IN_RESUME t1 WITH(NOLOCK)
                INNER JOIN
	                IN_RESUME_CERTIFICATE t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t2.in_type LIKE '%裁判證%' 
	                AND t2.in_type NOT LIKE '%國際%' 
	                AND t2.in_file2 IS NOT NULL
                ORDER BY
	                t1.login_name
	                , t2.created_on DESC
            ";
            return cfg.inn.applySQL(sql);
        }

        private Item GetResumeCoachItems(TConfig cfg)
        {
            var sql = @"
                SELECT
	                t1.id
	                , t1.in_name
	                , UPPER(t1.login_name)  AS 'login_name'
	                , t1.in_instructor_id   AS 'cert_id'
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, t1.in_instructor_date), 111)    AS 'cert_date'
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, t1.in_instructor_expired), 111) AS 'expired_date'
	                , t2.in_file1
	                , t2.in_file2
                FROM
	                IN_RESUME t1 WITH(NOLOCK)
                INNER JOIN
	                IN_RESUME_CERTIFICATE t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t2.in_type LIKE '%教練證%'
	                AND t2.in_type NOT LIKE '%國際%' 
	                AND t2.in_file2 IS NOT NULL
                ORDER BY
	                t1.login_name
	                , t2.created_on DESC
            ";
            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string in_type { get; set; }
            public string scene { get; set; }
        }
    }
}