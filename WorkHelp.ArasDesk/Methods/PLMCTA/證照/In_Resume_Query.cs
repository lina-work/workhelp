﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.證照
{
    public class In_Resume_Query : Item
    {
        public In_Resume_Query(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 查詢裁判級別
                日誌: 
                    - 2024-10-08: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Resume_Query";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                in_sno = itmR.getProperty("in_sno", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "query_referee_level":
                    QueryRefereeLevel(cfg, itmR);
                    break;
            }

            return itmR;
        }

        //查詢裁判級別
        private void QueryRefereeLevel(TConfig cfg, Item itmReturn)
        {
            var sql = "SELECT TOP 1 id, in_referee_level FROM IN_RESUME WITH(NOLOCK) WHERE in_sno = '" + cfg.in_sno + "'";
            var itmData = cfg.inn.applySQL(sql);
            if (itmData.isError() || itmData.getResult() == "")
            {
                throw new Exception("查無人員資料: " + cfg.in_sno);
            }
            var in_referee_level = itmData.getProperty("in_referee_level", "").Trim();
            if (in_referee_level == "")
            {
                throw new Exception("查無裁判級別: " + cfg.in_sno);
            }
            in_referee_level = in_referee_level.Replace("級", "").Trim();
            itmReturn.setProperty("in_referee_level", in_referee_level);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string in_sno { get; set; }
            public string scene { get; set; }
        }

    }
}
