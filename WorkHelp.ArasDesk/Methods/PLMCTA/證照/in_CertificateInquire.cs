﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using System.Drawing.Printing;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.證照
{
    public class in_CertificateInquire : Item
    {
        public in_CertificateInquire(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 證照查詢
                日期: 
                    - 2024-09-13 大改 (lina)
                    - 2022-01-07 調整 (lina)
                    - 2021-05-04 創建 (Alan)
            */

            //System.Diagnostics.Debugger.Break();
            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]In_CertificateInquire";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                cert_type = itmR.getProperty("cert_type", ""),
                cert_level = itmR.getProperty("cert_level", ""),
                in_name = itmR.getProperty("in_name", ""),
                in_current_org = itmR.getProperty("in_current_org", ""),
                in_cert_number = itmR.getProperty("in_cert_number", ""),
                manager_name = itmR.getProperty("manager_name", ""),
                scene = itmR.getProperty("scene", ""),
            };

            //取得登入者資訊
            string sql = "SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = N'" + inn.getUserID() + "'";
            Item itmLoginResume = inn.applySQL(sql);
            if (IsError(itmLoginResume))
            {
                throw new Exception("登入者履歷異常");
            }
            cfg.in_is_admin = itmLoginResume.getProperty("in_is_admin", "");

            //要檢視的對象 Resume
            Item itmResumeView = GetTargetResume(CCO, strMethodName, inn, itmLoginResume, itmR);
            cfg.view_resume_id = itmResumeView.getProperty("id", "");
            cfg.view_resume_sno = itmResumeView.getProperty("in_sno", "");
            cfg.view_member_type = itmResumeView.getProperty("in_member_type", "");

            //權限檢核
            string codeMethod = "ALL";
            string bodyMethod = "<method>" + strMethodName + "</method><code>" + codeMethod + "</code>";
            Item itmPermit = inn.applyMethod("In_CheckIdentity", bodyMethod);

            cfg.isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            cfg.isCommittee = itmPermit.getProperty("isCommittee", "") == "1";
            if (cfg.in_is_admin == "1")
            {
                cfg.isMeetingAdmin = true;
            }

            switch (cfg.scene)
            {
                case "query":
                    Query(cfg, itmR);
                    break;

                case "export_xlsx":
                    ExportXlsx(cfg, itmResumeView, itmR);
                    break;
            }

            return itmR;
        }

        private string GetNameLink(Item item)
        {
            var rid = item.getProperty("rid", "");
            var in_name = item.getProperty("in_name", "");
            var url = "c.aspx?page=SingleMemberView.html&method=In_Group_Resume_View&id=" + rid;
            return "<a href='" + url + "' target='_blank' rel='noopener noreferrer'>" + in_name + "</a>";
        }

        private string GetSnoDisplay(Item item)
        {
            var in_sno = item.getProperty("in_sno", "").PadLeft(10, '□');
            return in_sno.Substring(0, 2) + "****" + in_sno.Substring(6, 4);
        }

        private void Query(TConfig cfg, Item itmReturn)
        {
            Item itmResumes = null;

            if (cfg.isMeetingAdmin)
            {
                itmResumes = GetCertificateList(cfg, "", 0);
            }
            else if (cfg.view_member_type == "area_cmt" || cfg.view_member_type == "prjt_cmt")
            {
                itmResumes = GetCertificateList(cfg, cfg.view_resume_id, 1);
            }
            else
            {
                itmResumes = GetCertificateList(cfg, cfg.view_resume_sno, 2);
            }

            if (!itmResumes.isError() && itmResumes.getResult() != "")
            {
                QueryTable(cfg, itmResumes, itmReturn);
            }
        }

        //證照清單
        private void QueryTable(TConfig cfg, Item items, Item itmReturn)
        {
            string item_numbers = "";
            string modes = "";
            string meeting_id = "";

            Dictionary<string, string> modeList = new Dictionary<string, string>();

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "page_cert_type", title = "證照類別", css = "text-center" });
            if (cfg.hasNameCondition)
            {
                fields.Add(new TField { property = "cert_lv", title = "證照級別", css = "text-center" });
            }
            else
            {
                fields.Add(new TField { property = "page_cert_level", title = "證照級別", css = "text-center" });
            }
            fields.Add(new TField { property = "no", title = "No.", css = "text-center" });
            fields.Add(new TField { property = "name_link", title = "姓名", css = "text-center" });
            fields.Add(new TField { property = "in_sno_display", title = "身分證字號", css = "text-center" });
            fields.Add(new TField { property = "in_gender", title = "性別", css = "text-center" });
            fields.Add(new TField { property = "reg_birth", title = "西元生日", css = "text-center" });
            fields.Add(new TField { property = "in_tel", title = "聯絡號碼", css = "text-center" });
            fields.Add(new TField { property = "cert_id", title = "證照號碼", css = "text-center text-danger" });
            fields.Add(new TField { property = "cert_dt", title = "證照日期", css = "text-center text-danger" });
            fields.Add(new TField { property = "in_current_org", title = "所屬單位", css = "text-center" });
            fields.Add(new TField { property = "manager_name", title = "管理單位", css = "text-center" });
            fields.Add(new TField { property = "in_member_status", title = "會員狀態", css = "text-center" });
            //fields.Add(new TField { property = "in_effective_start", title = "效期起<BR>(發證日)", css = "text-center" });
            //fields.Add(new TField { property = "in_effective_end", title = "效期迄", css = "text-center" });
            //var file_properties = new List<string> { "in_file1", "in_file2" };
            //fields.Add(new TField { property = "inn_files", title = "下載", need_clear = true, ClearValue = DownloadFiles, properties = file_properties, css = "text-left" });
            //fields.Add(new TField { property = "inn_func", title = "功能", css = "text-center" });
            // fields.Add(new TField { property = "in_receipt", title = "下載<BR>收據", css = "text-center"});

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            foreach (var field in fields)
            {
                if (field.hide)
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true' data-visible='false'>" + field.title + "</th>");
                }
                else
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true'>" + field.title + "</th>");
                }
            }
            head.AppendLine("</thead>");

            int count = items.getItemCount(); ;
            var cert_type_label = itmReturn.getProperty("cert_type_label", "");
            var cert_level_label = itmReturn.getProperty("cert_level_label", "");
            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setProperty("no", (i + 1).ToString());
                item.setProperty("page_cert_type", cert_type_label);
                item.setProperty("page_cert_level", cert_level_label);
                item.setProperty("in_sno_display", GetSnoDisplay(item));
                item.setProperty("name_link", GetNameLink(item));
                if (cfg.isDegree)
                {
                    item.setProperty("cert_lv", item.getProperty("degree_display", ""));
                }
                if (cfg.isGloabalDegree)
                {
                    item.setProperty("cert_lv", item.getProperty("gl_degree_display", ""));
                }
                body.AppendLine("<tr>");
                foreach (var field in fields)
                {
                    if (field.need_clear)
                    {
                        item.setProperty(field.property, field.ClearValue(cfg, item, field));
                    }

                    body.AppendLine("<td class='" + field.css + "' data-field='" + field.property + "' >" + field.GetStr(item) + "</td>");
                }
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");


            string table_name = "resume_table";

            var builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            itmReturn.setProperty("inn_table", builder.ToString());

            //foreach (var g in modeList)
            //{
            //    modes += g.Key;
            //    meeting_id = g.Value;
            //}

            //itmReturn.setProperty("inn_table", builder.ToString());
            //itmReturn.setProperty("item_numbers", item_numbers);
            //itmReturn.setProperty("modes", modes);
            //itmReturn.setProperty("meeting_ids", meeting_id);

        }
        private TCertiQuery GetCertificateConditions(TConfig cfg, string view_filter, int type)
        {
            cfg.isDegree = false;
            cfg.isGloabalDegree = false;
            cfg.hasNameCondition = false;

            var pack = new TCertiQuery
            {
                colCmd = "",
                condCmd = "",
                sortCmd = "t1.in_birth, t1.in_name",
                columns = new List<string>(),
                conditions = new List<string>()
            };

            if (type == 1)
            {
                pack.conditions.Add("t1.in_manager_org = N'" + view_filter + "'");
            }
            else if (type == 2)
            {
                pack.conditions.Add("t1.in_sno = N'" + view_filter + "'");
            }

            switch (cfg.cert_type)
            {
                case "in_degree":
                    pack.certLv = "in_degree";
                    pack.certId = "in_degree_id";
                    pack.certDt = "in_degree_date";
                    cfg.isDegree = true;
                    break;
                case "in_gl_degree":
                    pack.certLv = "in_gl_degree";
                    pack.certId = "in_gl_degree_id";
                    pack.certDt = "in_gl_degree_date";
                    cfg.isGloabalDegree = true;
                    break;
                case "in_coach":
                    pack.certLv = "in_instructor_level";
                    pack.certId = "in_instructor_id";
                    pack.certDt = "in_instructor_date";
                    break;
                case "in_gl_coach":
                    pack.certLv = "in_gl_instructor_level";
                    pack.certId = "in_gl_instructor_id";
                    pack.certDt = "in_gl_instructor_date";
                    break;
                case "in_gl_para_coach":
                    pack.certLv = "in_gl_para_instructor_level";
                    pack.certId = "in_gl_para_instructor_id";
                    pack.certDt = "in_gl_para_instructor_date";
                    break;
                case "in_referee":
                    pack.certLv = "in_referee_level";
                    pack.certId = "in_referee_id";
                    pack.certDt = "in_referee_date";
                    break;
                case "in_gl_referee":
                    pack.certLv = "in_gl_referee_level";
                    pack.certId = "in_gl_referee_id";
                    pack.certDt = "in_gl_referee_date";
                    break;
                case "in_poomsae":
                    pack.certLv = "in_poomsae_level";
                    pack.certId = "in_poomsae_id";
                    pack.certDt = "in_poomsae_date";
                    break;
                case "in_gl_poomsae":
                    pack.certLv = "in_gl_poomsae_level";
                    pack.certId = "in_gl_poomsae_id";
                    pack.certDt = "in_gl_poomsae_date";
                    break;
            }

            pack.columns.Add("t1." + pack.certLv + " AS 'cert_lv'");
            pack.columns.Add("t1." + pack.certId + " AS 'cert_id'");
            pack.columns.Add("CONVERT(VARCHAR, DATEADD(HOUR, 8, t1." + pack.certDt + "), 111) AS 'cert_dt'");

            if (cfg.in_name != "")
            {
                cfg.hasNameCondition = true;
                pack.conditions.Add("t1.in_name LIKE '%' + N'" + cfg.in_name + "' + '%'");
            }
            if (cfg.in_current_org != "")
            {
                cfg.hasNameCondition = true;
                pack.conditions.Add("t1.in_current_org LIKE '%' + N'" + cfg.in_current_org + "' + '%'");
            }
            if (cfg.manager_name != "")
            {
                cfg.hasNameCondition = true;
                pack.conditions.Add("t2.in_name LIKE '%' + N'" + cfg.manager_name + "' + '%'");
                pack.conditions.Add("ISNULL(t1." + pack.certLv + ", '') <> ''");
            }
            if (cfg.in_cert_number != "")
            {
                cfg.hasNameCondition = true;
                pack.conditions.Add("t1."+ pack.certId + " LIKE N'%" + cfg.in_cert_number + "%'");
            }

            if (!cfg.hasNameCondition)
            {
                if (string.IsNullOrWhiteSpace(cfg.cert_level))
                {
                    pack.conditions.Add("ISNULL(t1." + pack.certLv + ", '') = ''");
                }
                else
                {
                    pack.conditions.Add("ISNULL(t1." + pack.certLv + ", '') = '" + cfg.cert_level + "'");
                }
            }

            pack.colCmd = pack.columns.Count == 0 ? "" : ", " + string.Join(", ", pack.columns);
            pack.condCmd = pack.conditions.Count == 0 ? "" : "AND " + string.Join(" AND ", pack.conditions);
            if (cfg.hasNameCondition)
            {
                pack.sortCmd = "t1.in_degree DESC, t1.in_birth, t1.in_name";
            }
            return pack;
        }

        //取得成員
        private Item GetCertificateList(TConfig cfg, string view_filter, int type)
        {
            var pack = GetCertificateConditions(cfg, view_filter, type);

            var sql = @"
                SELECT
	                t1.id AS 'rid'
	                , t1.in_name
	                , UPPER(t1.in_sno) AS 'in_sno'
	                , t1.in_gender
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, t1.in_birth), 111) AS 'reg_birth'
	                , t1.in_tel
	                , t1.in_tel_1
                    , t1.in_member_status
	                , t1.in_current_org
                    , t1.in_resident_add_code
                    , t1.in_resident_add
	                , ISNULL(t2.in_name, '') AS 'manager_name'
	                , ISNULL(t2.in_short_org, '') AS 'manager_alias'
                    , t11.label AS 'degree_display'
                    , t12.label AS 'gl_degree_display'
                    {#columns}
                FROM 
	                IN_RESUME t1 WITH (NOLOCK) 
                LEFT OUTER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.id = t1.in_manager_org
                LEFT OUTER JOIN
                    VU_DEGREE t11
                    ON t11.value = t1.in_degree
                LEFT OUTER JOIN
                    VU_DEGREE t12
                    ON t12.value = t1.in_gl_degree
                WHERE
	                ISNULL(t1.in_member_type, '') NOT IN ('vip_gym', 'vip_group', 'area_cmt')
                    {#condition}
                ORDER BY 
	                {#orderBy}
            ";

            sql = sql.Replace("{#columns}", pack.colCmd)
                .Replace("{#condition}", pack.condCmd)
                .Replace("{#orderBy}", pack.sortCmd);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        ////取得成員
        //private Item GetCertificateListOld(TConfig cfg, string view_filter, int type)
        //{
        //    string condition = "";

        //    if (type == 1)
        //    {
        //        condition += " AND t1.in_manager_org = N'" + view_filter + "'";
        //    }
        //    else if (type == 2)
        //    {
        //        condition += " AND t1.in_sno = N'" + view_filter + "'";
        //    }

        //    if (cfg.title != "All" && cfg.title != "")
        //    {
        //        condition += " AND t2.in_photokey LIKE 'in_photo" + cfg.title + "%'";
        //    }
        //    else
        //    {
        //        condition += " AND (t2.in_photokey LIKE 'in_photo6%' OR t2.in_photokey LIKE 'in_photo7%' OR t2.in_photokey LIKE 'in_photo8%' OR t2.in_photokey LIKE 'in_photo10%')";
        //    }

        //    if (cfg.in_level != "All" && cfg.in_level != "")
        //    {
        //        condition += " AND t2.in_certificate_level = '" + cfg.in_level + "'";
        //    }


        //    if (cfg.filter != "")
        //    {
        //        condition += "AND (T2.in_type like N'%" + cfg.filter + "%' OR T2.in_certificate_level like N'%" + cfg.filter + "%' OR T2.in_certificate_name like N'%" + cfg.filter + "%' OR T2.in_certificate_no like N'%" + cfg.filter + "%' OR T1.in_sno like N'%" + cfg.filter + "%' OR T1.in_name like N'%" + cfg.filter + "%' OR CONVERT(varchar(100), DATEADD(hour, 8, in_effective_start), 23) like N'%" + cfg.filter + "%' OR CONVERT(varchar(100), DATEADD(hour, 8, in_effective_end), 23) like N'%" + cfg.filter + "%' )";
        //    }

        //    if (cfg.date_s != "" || cfg.date_e != "")
        //    {
        //        if (cfg.date_s != "" && cfg.date_e != "")
        //        {
        //            condition += " AND CONVERT(varchar(100), DATEADD(hour, 8, in_effective_start), 23) BETWEEN '" + cfg.date_s + "' AND '" + cfg.date_e + "'";
        //        }
        //        else if (cfg.date_s != "")
        //        {
        //            condition += " AND CONVERT(varchar(100), DATEADD(hour, 8, in_effective_start), 23) >= '" + cfg.date_s + "'";
        //        }
        //        else
        //        {
        //            condition += " AND CONVERT(varchar(100), DATEADD(hour, 8, in_effective_start), 23) <= '" + cfg.date_e + "'";
        //        }
        //    }

        //    string sql = @"
        //        SELECT t1.id as rid, REPLACE (in_sno,SUBSTRING(in_sno,3,4),'****') AS in_display_sno, * 
        //        FROM IN_RESUME AS T1 WITH (NOLOCK) 
        //        JOIN IN_RESUME_CERTIFICATE AS T2 WITH (NOLOCK) 
        //        ON T1.id = T2.SOURCE_ID
        //        WHERE IN_PHOTOKEY like 'in_photo%' {#condition}
        //        ORDER BY IN_TYPE, ISNULL(in_effective_start,'2200/01/01')
        //    ";

        //    sql = sql.Replace("{#condition}", condition);
        //    // if (cfg.mode =="xls")
        //    // {
        //    //     sql = sql.Replace("ORDER BY ITEM_NUMBER", "ORDER BY in_title,ITEM_NUMBER");
        //    // }
        //    //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

        //    return cfg.inn.applySQL(sql);

        //}

        //取得證照名稱
        private List<Item> GetCertificateName(TConfig cfg)
        {
            string sql = @"
            	SELECT 
					 value
					, label_zt AS 'label' 
				FROM 
					[List] t1 WITH (NOLOCK) 
            	INNER JOIN 
					[Value] t2 WITH (NOLOCK) 
            		ON t2.source_id = t1.id
            	WHERE 
					t1.name = 'In_Meeting_CertificateType'
					AND t2.value IN ('6', '7', '8', '10')
				ORDER BY
					t2.sort_order
            ";

            List<Item> result = GetList(cfg.inn.applySQL(sql));
            AppendAll(cfg, result);
            return result;
        }

        //取得證照級別
        private List<Item> GetCertificateLevel(TConfig cfg)
        {
            string sql = @"
            	SELECT 
					 value
					, label_zt AS 'label' 
				FROM 
					[List] t1 WITH (NOLOCK) 
            	INNER JOIN 
					[Value] t2 WITH (NOLOCK) 
            		ON t2.source_id = t1.id
            	WHERE 
					t1.name = 'In_Meeting_CertificateLevel'
				ORDER BY
					t2.sort_order
            ";

            List<Item> result = GetList(cfg.inn.applySQL(sql));
            AppendAll(cfg, result);
            return result;
        }

        private string GetIntStr(string value)
        {
            if (value == "" || value == "0") return "0";

            int result = 0;
            Int32.TryParse(value, out result);
            return result.ToString("N0");
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "' class='table table-hover table-bordered table-rwd rwd rwdtable' "
                + " data-toggle='table' "
                + " data-click-to-select='true' "
                + " data-striped='false' "
                 + " data-pagination='true' "
                 + " data-show-pagination-switch='false'"
                 + " data-page-size='25'"
                + ">";
        }


        //取得檢視對象 Resume
        private Item GetTargetResume(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmLoginResume, Item itmReturn)
        {
            itmLoginResume.setType("In_Resume");
            itmLoginResume.setProperty("target_resume_id", itmReturn.getProperty("id", ""));
            itmLoginResume.setProperty("login_resume_id", itmLoginResume.getID());
            itmLoginResume.setProperty("login_member_type", itmLoginResume.getProperty("in_member_type", ""));
            itmLoginResume.setProperty("login_is_admin", itmLoginResume.getProperty("in_is_admin", ""));
            return itmLoginResume.apply("In_Get_Target_Resume");
        }

        #region 匯出
        private void ExportXlsx(TConfig cfg, Item itmResumeView, Item itmReturn)
        {
            Item itmResumes = null;

            if (cfg.isMeetingAdmin)
            {
                itmResumes = GetCertificateList(cfg, "", 0);
            }
            else if (cfg.view_member_type == "area_cmt" || cfg.view_member_type == "prjt_cmt")
            {
                itmResumes = GetCertificateList(cfg, cfg.view_resume_id, 1);
            }
            else
            {
                itmResumes = GetCertificateList(cfg, cfg.view_resume_sno, 2);
            }

            cfg.cert_type_label = itmReturn.getProperty("cert_type_label", "");
            cfg.cert_level_label = itmReturn.getProperty("cert_level_label", "");

            string xls_title = cfg.cert_type_label;
            if (cfg.cert_level_label != "") xls_title += "_" + cfg.cert_level_label;

            ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook();
            if (itmResumes == null || !IsError(itmResumes, isSingle: false))
            {
                AppendSheet(cfg, workbook, itmResumes, cfg.cert_type_label);
            }

            Item itmPath = GetExcelPath(cfg, "export_path");

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".xlsx";
            string xls_name = xls_title + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");
            string xls_file = export_path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveAs(xls_file);

            itmReturn.setProperty("xls_name", xls_url);
        }

        private Item GetExcelPath(TConfig cfg, string in_name)
        {
            Item itmResult = cfg.inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
            SELECT 
                TOP 1 t2.in_name AS 'variable', t1.in_name, t1.in_value 
            FROM 
                In_Variable_Detail t1 WITH(NOLOCK)
            INNER JOIN 
                In_Variable t2 WITH(NOLOCK) ON t2.ID = t1.SOURCE_ID 
            WHERE
                t2.in_name = N'meeting_excel'  
                AND t1.in_name = N'{#in_name}'
            ";

            sql = sql.Replace("{#in_name}", in_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError())
            {
                throw new Exception("未設定匯出路徑 _# " + in_name);
            }

            itmResult.setProperty("export_path", item.getProperty("in_value", ""));

            return itmResult;
        }
        //附加證照類別 Sheet
        private void AppendSheet(TConfig cfg, ClosedXML.Excel.XLWorkbook workbook, Item items, string sheet_name)
        {
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheets.Add(sheet_name);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "page_cert_type", title = "證照類別", css = "text-center", format = "center" });
            if (cfg.hasNameCondition)
            {
                fields.Add(new TField { property = "cert_lv", title = "證照級別", css = "text-center", format = "center" });
            }
            else
            {
                fields.Add(new TField { property = "page_cert_level", title = "證照級別", css = "text-center", format = "center" });
            }
            fields.Add(new TField { property = "in_name", title = "姓名", css = "text-center" });
            fields.Add(new TField { property = "in_sno_display", title = "身分證字號", css = "text-center", format = "center" });
            fields.Add(new TField { property = "in_gender", title = "性別", css = "text-center", format = "center" });
            fields.Add(new TField { property = "reg_birth", title = "西元生日", css = "text-center", format = "center" });
            fields.Add(new TField { property = "in_tel", title = "行動電話", css = "text-center", format = "tel" });
            fields.Add(new TField { property = "cert_id", title = "證照號碼", css = "text-center text-danger", format = "text" });
            fields.Add(new TField { property = "cert_dt", title = "證照日期", css = "text-center text-danger", format = "center" });
            fields.Add(new TField { property = "in_current_org", title = "所屬單位", css = "text-center" });
            fields.Add(new TField { property = "manager_name", title = "管理單位", css = "text-center" });
            fields.Add(new TField { property = "in_member_status", title = "會員狀態", css = "text-center", format = "center" });
            fields.Add(new TField { property = "in_tel_1", title = "住家電話", css = "text-center", format = "text" });
            fields.Add(new TField { property = "in_resident_add_code", title = "戶籍郵遞區號", css = "text-center", format = "text" });
            fields.Add(new TField { property = "in_resident_add", title = "戶籍地址", css = "text-center" });

            int wsRow = 1;
            int wsCol = 1;
            int field_count = fields.Count;

            for (int i = 0; i < field_count; i++)
            {
                TField field = fields[i];
                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);
                SetHeadCell(cell, field.title);
            }
            wsRow++;

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setProperty("no", (i + 1).ToString());
                item.setProperty("page_cert_type", cfg.cert_type_label);
                item.setProperty("page_cert_level", cfg.cert_level_label);
                item.setProperty("in_sno_display", GetSnoDisplay(item));
                item.setProperty("name_link", GetNameLink(item));
                if (cfg.isDegree)
                {
                    item.setProperty("cert_lv", item.getProperty("degree_display", ""));
                }
                if (cfg.isGloabalDegree)
                {
                    item.setProperty("cert_lv", item.getProperty("gl_degree_display", ""));
                }

                SetItemCell(sheet, wsRow, wsCol, item, fields);
                wsRow++;

            }

            //自動調整欄寬
            sheet.Columns().AdjustToContents();
        }
        ClosedXML.Excel.XLColor head_bg_color = ClosedXML.Excel.XLColor.FromHtml("#295C90");
        //設定標題列
        private void SetHeadCell(ClosedXML.Excel.IXLCell cell, string title)
        {
            cell.Value = title;

            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell.Style.Font.Bold = true;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Fill.BackgroundColor = head_bg_color;
            cell.Style.Font.FontColor = ClosedXML.Excel.XLColor.White;

            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        //設定資料列
        private void SetBodyCell(ClosedXML.Excel.IXLCell cell, string value, string format)
        {
            switch (format)
            {
                case "center":
                    cell.Value = value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "yyyy/MM/dd":
                case "yyyy/MM/dd HH:mm":
                    cell.Value = GetDateTimeValue(value, format, 8);
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "$ #,##0":
                    cell.Value = value;
                    cell.Style.NumberFormat.Format = "$ #,##0";
                    break;

                case "tel":
                    if (value.StartsWith("09"))
                    {
                        cell.Value = value.Replace("-", "");
                        cell.Style.NumberFormat.Format = "0000-000-000";
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    else
                    {
                        cell.Value = "'" + value;
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    break;

                case "text":
                    cell.Value = "'" + value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                default:
                    cell.Value = value;
                    break;
            }
            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        //設定物件與資料列
        private void SetItemCell(ClosedXML.Excel.IXLWorksheet sheet, int wsRow, int wsCol, Item item, List<TField> fields)
        {
            int count = fields.Count;
            for (int i = 0; i < count; i++)
            {
                TField field = fields[i];

                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);

                if (field.property == "")
                {
                    SetBodyCell(cell, "", "");
                }
                else
                {
                    string value = item.getProperty(field.property, "");
                    SetBodyCell(cell, value, field.format);
                }
            }
        }
        #endregion 匯出

        private class TField
        {
            public string property { get; set; }
            public string title { get; set; }
            public string css { get; set; }
            public string format { get; set; }
            public bool hide { get; set; }
            public bool need_clear { get; set; }
            public List<string> properties { get; set; }
            public Func<TConfig, Item, TField, string> ClearValue { get; set; }
            public string GetStr(Item item)
            {
                return item.getProperty(this.property, "");
            }
        }

        /// <summary>
        /// 下載檔案
        /// </summary>
        private string DownloadFiles(TConfig cfg, Item item, TField field)
        {
            StringBuilder builder = new StringBuilder();
            foreach (string property in field.properties)
            {
                string value = item.getProperty(property, "");
                if (value != "")
                {
                    if (builder.Length > 0) builder.Append(" ");
                    builder.Append("<a><i class='fa fa-picture-o' onclick='CertFile_Click(this)' data-photo='" + value + "'></i></a>");
                }
            }

            return builder.ToString();
        }

        private string GetPayBoolLink(Item itmResume)
        {
            string meeting_type = itmResume.getProperty("meeting_type", "");
            string meeting_id = itmResume.getProperty("meeting_id", "");
            string item_number = itmResume.getProperty("item_number", "");
            string pay_bool = itmResume.getProperty("pay_bool", "");

            if (pay_bool.Contains("已繳費"))
            {
                pay_bool = "<span style='color:green;'><i class='fa fa-list-alt'></i>" + pay_bool + "</span>";
            }
            else
            {
                pay_bool = "<span style='color:red;'><i class='fa fa-list-alt'></i>" + pay_bool + "</span>";
            }

            if (meeting_type == "payment" || meeting_type == "game" || meeting_type == "verify")
            {
                //onclick=\"ajaxindicatorstart('載入中...');\"
                return "<a target='_blank' href='../pages/c.aspx?page=detail_list_n1.html&method=In_Payment_DetailList&meeting_id=" + meeting_id + "&item_number=" + item_number + "' >" + pay_bool + "</a>";
            }
            else
            {
                //onclick=\"ajaxindicatorstart('載入中...');\"
                return "<a target='_blank' href='../pages/c.aspx?page=Cla_detail_list_n1.html&method=In_Cla_Payment_DetailList&meeting_id=" + meeting_id + "&item_number=" + item_number + "' >" + pay_bool + "</a>";
            }
        }

        private string GetSidDisplay(Item itmResume)
        {
            return GetSidDisplay(itmResume.getProperty("in_sno", ""));
        }

        //生日(外顯)
        private string GetBirthDisplay(Item itmResume)
        {
            string value = itmResume.getProperty("in_birth", "");

            if (value.Contains("1900") || value.Contains("1899"))
            {
                return "";
            }

            return GetDateTimeValue(value, "yyyy年MM月dd日", 8);
        }

        //身分證字號(外顯)
        private string GetSidDisplay(string in_sno)
        {
            if (in_sno.Length == 10)
            {
                //10碼(身分證處理)
                string in_sno_1 = in_sno.Substring(0, 2);
                string in_sno_2 = in_sno.Substring(6, 4);
                return in_sno_1 + "****" + in_sno_2;//A1****6789
            }
            else
            {
                return "";
            }
        }

        private string GetDateTimeValue(string value, string format, int hours = 0)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }

        private bool IsError(Item item, bool isSingle = true)
        {
            if (item.isError() || item.getResult() == "")
            {
                return true;
            }
            else if (isSingle && item.getItemCount() != 1)
            {
                return true;
            }
            else if (item.getItemCount() <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //下拉選單
        private void AppendMenu(TConfig cfg, List<Item> items, string type_name, Item itmReturn)
        {
            if (items == null || items.Count == 0)
            {
                return;
            }

            int count = items.Count;

            for (int i = 0; i < count; i++)
            {
                Item item = items[i];

                item.setType(type_name);
                item.setProperty("inn_label", item.getProperty("label", ""));
                item.setProperty("inn_value", item.getProperty("value", ""));
                itmReturn.addRelationship(item);
            }
        }

        //檢查是否有特殊字元
        private string CheckStr(string fileName)
        {
            string[] Special_text = new string[] { "?", "=", ".", "*", "[", "@", "#", "$", "%", "^", "&", ".", "+", "-", "]", " ", "/", @"\" }; //特殊字元

            if (!string.IsNullOrWhiteSpace(fileName))
            {
                foreach (string _text in Special_text)
                {
                    if (fileName.Contains(_text))
                    {
                        fileName = fileName.Replace(_text, "");
                    }
                }
            }

            if (fileName.Length > 31)
            {
                fileName = fileName.Substring(0, 31);
            }

            return fileName;
        }

        private void AppendAll(TConfig cfg, List<Item> list)
        {
            Item itmAll = cfg.inn.newItem();
            itmAll.setProperty("label", "請選擇");
            itmAll.setProperty("value", "請選擇");
            // itmAll.setProperty("label", "全部");
            // itmAll.setProperty("value", "All");
            list.Insert(0, itmAll);
        }

        private List<Item> GetList(Item items)
        {
            List<Item> result = new List<Item>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                result.Add(items.getItemByIndex(i));
            }
            return result;
        }

        private class TCertiQuery
        {
            public List<string> columns { get; set; }
            public List<string> conditions { get; set; }
            public string colCmd { get; set; }
            public string condCmd { get; set; }
            public string sortCmd { get; set; }
            public string certLv { get; set; }
            public string certId { get; set; }
            public string certDt { get; set; }
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string cert_type { get; set; }
            public string cert_level { get; set; }
            public string in_name { get; set; }
            public string in_current_org { get; set; }
            public string in_cert_number { get; set; }
            public string manager_name { get; set; }
            public string scene { get; set; }

            public bool hasNameCondition { get; set; }
            public bool isDegree { get; set; }
            public bool isGloabalDegree { get; set; }

            public string in_is_admin { get; set; }
            public bool isMeetingAdmin { get; set; }
            public bool isCommittee { get; set; }

            public string view_resume_id { get; set; }
            public string view_resume_sno { get; set; }
            public string view_member_type { get; set; }

            public string cert_type_label { get; set; }
            public string cert_level_label { get; set; }
        }
    }
}