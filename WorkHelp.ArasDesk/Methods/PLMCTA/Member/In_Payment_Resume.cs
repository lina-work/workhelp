﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Member
{
    public class AAA_Method : Item
    {
        public AAA_Method(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: Resume 會費繳費單
                日期: 
                    - 2023/01/06: 依 Elsa 需求，變更為一年一單，不取消舊單 (lina)
                    - 2021/12/16: 一般社團、學校社團年費調整為 1,000 元 (lina)
                    - 2021/12/06: 2022年-年費 (lina)
                    - 2021/01/21: 增加註冊功能: 入會費+年費 (lina)
                    - 2020/12/24: 創建 (lina)
            */

            System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]In_Payment_Resume";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;
            // itmR.setProperty("in_sno", "T12454");
            // itmR.setProperty("inn_new_apply", "1");
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            //繳費單歸屬
            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                scene = itmR.getProperty("scene", ""),
            };

            DateTime now = DateTime.Now;
            if (now.Month == 12)
            {
                now = new DateTime(now.Year + 1, 1, 1);
            }

            cfg.dtNow = now;

            if (cfg.scene == "")
            {
                CreatePaymentsV2023(cfg, itmR);

                //bool go_next = CreatePayments(CCO, strMethodName, inn, itmR);
                //if (go_next)
                //{
                //    //取消去年年費繳費單(未繳費)
                //    CancelLastYearPayments(CCO, strMethodName, inn, itmR);
                //}
            }
            else if (cfg.scene == "remove")
            {
                RemovePayments(cfg, itmR);
            }

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }


        //產生年費繳費單(2023版本)
        private bool CreatePaymentsV2023(TConfig cfg, Item itmReturn)
        {
            var resume = MapResume(cfg, itmReturn);

            if (!resume.need_create_payment)
            {
                string msg = "當前會員類型為【" + resume.member_type_label + "】 不可產生會費繳費單: "
                    + resume.in_name + "(" + resume.login_name + ")";

                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, msg);

                string sql_upd = "UPDATE IN_RESUME SET in_member_pay_no = '' WHERE id = '" + resume.id + "'";

                cfg.inn.applySQL(sql_upd);

                return false;
            }

            //最新年度會費活動
            resume.NewestMeeting = GetMeeting(cfg, resume.meeting_member_type, resume.TwYear);
            if (resume.NewestMeeting == null)
            {
                throw new Exception("查無最新年度會費活動");
            }

            //新註冊
            if (resume.inn_new_apply == "1")
            {
                //入會費
                var cfg1 = NewPayConfigs(resume.NewestMeeting, resume.option_value_join, resume.NewestMeeting.WestYear);
                resume.PayConfigs.Add(cfg1);
                //年費
                var cfg2 = NewPayConfigs(resume.NewestMeeting, resume.option_value_year, resume.NewestMeeting.WestYear);
                resume.PayConfigs.Add(cfg2);

                //刪除該年度繳費單
                RemovePayment(cfg, resume);

                //產生繳費單
                CreatePayment(cfg, resume);
            }
            else
            {
                //由介面指定年度
                var west_year = GetIntValue(itmReturn.getProperty("year", cfg.dtNow.Year.ToString()));
                var tw_year = (west_year - 1911).ToString();

                TMeeting payMeeting = GetMeeting(cfg, resume.meeting_member_type, tw_year);

                if (payMeeting == null)
                {
                    payMeeting = GetMeeting(cfg, resume.meeting_member_type, tw_year.ToString());
                    payMeeting.WestYear = west_year;
                    payMeeting.TwYear = tw_year;
                }

                //年費
                var cfg3 = NewPayConfigs(payMeeting, resume.option_value_year, west_year);
                resume.PayConfigs.Add(cfg3);

                if (!ExistsYearPayment(cfg, resume, west_year))
                {
                    //產生繳費單
                    CreatePayment(cfg, resume);
                }
            }

            return true;
        }

        //取消去年年費繳費單(未繳費)
        private void CancelLastYearPayments(TConfig cfg, Item itmReturn)
        {
            string in_sno = itmReturn.getProperty("in_sno", "");
            string inn_new_apply = itmReturn.getProperty("inn_new_apply", "");

            if (inn_new_apply == "1")
            {
                //新註冊，申請入會，不需取消去年年費繳費單
                return;
            }


            int last_west_year = cfg.dtNow.Year - 1;
            int last_tw_year = last_west_year - 1911;

            string sql = @"
                SELECT
	                t1.id
	                , t1.item_number
	                , t1.in_pay_amount_exp
	                , t2.in_title       AS 'mt_title'
	                , t2.in_member_type AS 'mt_member_type'
	                , t2.in_annual      AS 'mt_annual'
                FROM 
	                IN_MEETING_PAY t1 WITH(NOLOCK) 
                INNER JOIN
	                IN_MEETING t2 WITH(NOLOCK)
	                ON t2.id = t1.in_meeting
	                AND t2.in_meeting_type = 'payment'
                WHERE
	                t2.in_annual = '{#last_tw_year}'
	                AND t1.pay_bool = N'未繳費'
	                AND t1.in_creator_sno = '{#in_sno}'
            ";

            sql = sql.Replace("{#last_tw_year}", last_tw_year.ToString())
                .Replace("{#in_sno}", in_sno);

            //CCO.Utilities.WriteDebug(strMethodName, sql);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string mpid = item.getProperty("id", "");
                string item_number = item.getProperty("item_number", "");
                string mt_title = item.getProperty("mt_title", "");
                string mt_member_type = item.getProperty("mt_member_type", "");

                string msg = "        - " + (i + 1) + ". 取消繳費單(去年): "
                    + in_sno
                    + "|" + item_number
                    + "|" + mt_title
                    + "|" + mt_member_type;

                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, msg);

                string sql_cancel = "UPDATE IN_MEETING_PAY SET pay_bool = N'已取消' WHERE id = '" + mpid + "'";
                Item itmSQL = cfg.inn.applySQL(sql_cancel);
            }
        }

        //刪除繳費單
        private void RemovePayments(TConfig cfg, Item itmReturn)
        {
            TResume resume = MapResume(cfg, itmReturn);

            //最新年度會費活動
            resume.NewestMeeting = GetMeeting(cfg, resume.meeting_member_type, resume.TwYear);
            if (resume.NewestMeeting == null)
            {
                throw new Exception("查無最新年度會費活動");
            }

            //刪除繳費單
            RemovePayment(cfg, resume);
        }

        //產生年費繳費單
        private bool CreatePayments(TConfig cfg, Item itmReturn)
        {
            TResume resume = MapResume(cfg, itmReturn);

            if (!resume.need_create_payment)
            {
                string msg = "當前會員類型為【" + resume.member_type_label + "】 不可產生會費繳費單: "
                    + resume.in_name + "(" + resume.login_name + ")";

                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, msg);

                string sql_upd = "UPDATE IN_RESUME SET in_member_pay_no = '' WHERE id = '" + resume.id + "'";

                cfg.inn.applySQL(sql_upd);

                return false;
            }

            //最新年度會費活動
            resume.NewestMeeting = GetMeeting(cfg, resume.meeting_member_type, resume.TwYear);
            if (resume.NewestMeeting == null)
            {
                throw new Exception("查無最新年度會費活動");
            }

            //新註冊
            if (resume.inn_new_apply == "1")
            {
                //入會費
                var cfg1 = NewPayConfigs(resume.NewestMeeting, resume.option_value_join, resume.NewestMeeting.WestYear);
                resume.PayConfigs.Add(cfg1);
                //年費
                var cfg2 = NewPayConfigs(resume.NewestMeeting, resume.option_value_year, resume.NewestMeeting.WestYear);
                resume.PayConfigs.Add(cfg2);
            }
            else
            {
                //不指定年度，代表要產生該 Resume 所有積欠年度的繳費單

                int base_tw_year = 110;

                foreach (var year in resume.Unpaid_WestYear_List)
                {
                    int west_year = year;
                    string tw_year = (year - 1911).ToString();

                    TMeeting payMeeting = GetMeeting(cfg, resume.meeting_member_type, tw_year);

                    if (payMeeting == null)
                    {
                        payMeeting = GetMeeting(cfg, resume.meeting_member_type, base_tw_year.ToString());
                        payMeeting.WestYear = west_year;
                        payMeeting.TwYear = tw_year;
                    }

                    //年費
                    var cfg3 = NewPayConfigs(payMeeting, resume.option_value_year, west_year);
                    resume.PayConfigs.Add(cfg3);
                }
            }

            //刪除繳費單
            RemovePayment(cfg, resume);

            //產生繳費單
            CreatePayment(cfg, resume);

            return true;
        }

        private TMPayConfig NewPayConfigs(TMeeting meeting, string option_value, int west_year)
        {
            if (!meeting.SurveyOptions.ContainsKey(option_value))
            {
                throw new Exception("[" + meeting.Title + "]費用項目不包含[" + option_value + "]");
            }

            return new TMPayConfig
            {
                PayMeeting = meeting,
                PayOption = meeting.SurveyOptions[option_value],
                WestYear = west_year,
                TwYear = (west_year - 1911).ToString(),
            };
        }

        /// <summary>
        /// 已存在該年度繳費單
        /// </summary>
        private bool ExistsYearPayment(TConfig cfg, TResume resume, int west_year)
        {
            TMeeting meeting = resume.NewestMeeting;

            var sql = @"
                SELECT TOP 1
	                t1.id
					, t1.in_l2
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PAY t2 WITH(NOLOCK)
	                ON t2.in_meeting = t1.source_id
	                AND t2.item_number = t1.in_paynumber
                WHERE 
	                t1.source_id = '{#meeting_id}' 
	                AND t1.in_creator_sno = '{#in_creator_sno}' 
					AND t1.in_l2 = '{#west_year}'
            ";

            sql = sql.Replace("{#meeting_id}", meeting.Id)
                .Replace("{#in_creator_sno}", resume.login_name)
                .Replace("{#west_year}", west_year.ToString());

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            var itmMUsers = cfg.inn.applySQL(sql);
            var count = itmMUsers.getItemCount();
            return count >= 1;
        }

        /// <summary>
        /// 刪除繳費單
        /// </summary>
        private void RemovePayment(TConfig cfg, TResume resume)
        {
            string aml = "";
            string sql = "";
            int count = 0;

            TMeeting meeting = resume.NewestMeeting;

            sql = @"
                SELECT 
	                DISTINCT t1.id AS 'muid'
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PAY t2 WITH(NOLOCK)
	                ON t2.in_meeting = t1.source_id
	                AND t2.item_number = t1.in_paynumber
                WHERE 
	                t1.source_id = '{#meeting_id}' 
	                AND t1.in_creator_sno = '{#in_creator_sno}' 
	                AND t2.pay_bool = N'未繳費'
            ";

            sql = sql.Replace("{#meeting_id}", meeting.Id)
                .Replace("{#in_creator_sno}", resume.login_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmMUsers = cfg.inn.applySQL(sql);

            count = itmMUsers.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmMUser = itmMUsers.getItemByIndex(i);
                string muid = itmMUser.getProperty("muid", "");

                //刪除問卷結果
                aml = @"<AML>
        				    <Item type='In_Meeting_Surveys_result' action='delete' where=""in_participant='{#muid}' and in_surveytype='1' ""/>
        				</AML>"
                    .Replace("{#muid}", muid);

                cfg.inn.applyAML(aml);

                //刪除學員履歷
                aml = "<AML>" +
                    "<Item type='in_meeting_resume' action='delete' where=\"in_user='" + muid + "'\">" +
                    "</Item></AML>";

                cfg.inn.applyAML(aml);

                //刪除簽到記錄
                aml = "<AML>" +
                "<Item type='in_meeting_record' action='delete' where=\"in_participant='" + muid + "'\">" +
                "</Item></AML>";

                cfg.inn.applyAML(aml);

                //刪除與會者
                aml = "<AML>" +
                    "<Item type='in_meeting_user' action='delete' id='" + muid + "'>" +
                    "</Item></AML>";

                cfg.inn.applyAML(aml);
            }

            sql = @"
                SELECT 
					t1.id AS 'mpid'
                FROM 
	                IN_MEETING_PAY t1 WITH(NOLOCK)
                WHERE 
	                t1.in_meeting = '{#meeting_id}' 
	                AND t1.in_creator_sno = '{#in_creator_sno}' 
	                AND t1.pay_bool = N'未繳費'
            ";

            sql = sql.Replace("{#meeting_id}", meeting.Id)
                .Replace("{#in_creator_sno}", resume.login_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmMPays = cfg.inn.applySQL(sql);

            count = itmMPays.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmMPay = itmMPays.getItemByIndex(i);
                string mpid = itmMPay.getProperty("mpid", "");
                string sql_cancel = "UPDATE IN_MEETING_PAY SET pay_bool = N'已取消' WHERE id = '" + mpid + "'";
                cfg.inn.applySQL(sql_cancel);
            }
        }

        /// <summary>
        /// 產生繳費單
        /// </summary>
        private void CreatePayment(TConfig cfg, TResume resume)
        {
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "[產生繳費單]" + resume.member_type_label + ": " + resume.in_name + "(" + resume.login_name + ")");

            TMeeting meeting = resume.NewestMeeting;
            Item itmResume = resume.Value;

            bool need_new_pay = false;

            foreach (var payConfig in resume.PayConfigs)
            {
                //要新增在最新年度的會費活動，in_l1 必須存在課程問項，否則會新增失敗
                string in_l1 = payConfig.PayOption.getProperty("in_value", "");
                string in_l2 = payConfig.WestYear.ToString();
                string in_sno = itmResume.getProperty("in_sno", "");
                string in_mail = in_sno + "-" + in_l1 + "-" + in_l2;

                bool has_add = AddMeetingUser(cfg, meeting.Id, meeting.Surveys, itmResume, in_l1, in_l2, in_mail);

                if (has_add)
                {
                    need_new_pay = true;
                    CheckMoney(cfg, payConfig, resume, meeting.Id, in_mail);
                }
            }

            // if (!need_new_pay)
            // {
            //     return;
            // }

            Item itmPayData = cfg.inn.newItem();
            itmPayData.setType("In_Meeting_Pay");
            itmPayData.setProperty("meeting_id", meeting.Id);
            itmPayData.setProperty("in_group", itmResume.getProperty("in_group", ""));
            itmPayData.setProperty("current_orgs", "," + itmResume.getProperty("in_current_org", ""));
            itmPayData.setProperty("invoice_up", "," + "");
            itmPayData.setProperty("uniform_numbers", "," + "");
            itmPayData.setProperty("is_manual_create", "1");
            itmPayData.setProperty("pay_resume_user_id", itmResume.getProperty("in_user_id", ""));
            Item itmPayResult = itmPayData.apply("In_Payment_List_Add2");

            if (itmPayResult.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "[產生繳費單]產生失敗 _# dom: " + itmPayData.dom.InnerXml);
                return;
            }
            
            string in_payno = itmPayResult.getProperty("numbers", "");
            string sql_pay = "SELECT TOP 1 * FROM IN_MEETING_PAY WITH(NOLOCK) WHERE in_meeting = '" + meeting.Id + "' AND item_number = '" + in_payno + "'";
            // cfg.CCO.Utilities.WriteDebug(strMethodName, "sql_pay: " + sql_pay);
            Item itmMPay = cfg.inn.applySQL(sql_pay);
            if (!itmMPay.isError() && itmMPay.getResult() != "")
            {
                var sql_update = "UPDATE IN_RESUME SET in_member_pay_no = '{#in_member_pay_no}', in_member_pay_amount = '{#in_member_pay_amount}' WHERE id = '{#resume_id}'";

                sql_update = sql_update.Replace("{#resume_id}", resume.id)
                    .Replace("{#in_member_pay_no}", in_payno)
                    .Replace("{#in_member_pay_amount}", itmMPay.getProperty("in_pay_amount_exp", "0"));

                // cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql_update: " + sql_update);

                cfg.inn.applySQL(sql_update);
            }
        }

        private void CheckMoney(TConfig cfg
            , TMPayConfig payConfig
            , TResume resume
            , string meeting_id
            , string in_mail)
        {

            if (payConfig.WestYear == resume.NewestMeeting.WestYear)
            {
                return;
            }

            string sql = "SELECT TOP 1 id, in_expense FROM IN_MEETING_USER WITH(NOLOCK)"
                + " WHERE source_id = '" + meeting_id + "'"
                + " AND in_mail = N'" + in_mail + "'";

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmMUser = cfg.inn.applySQL(sql);
            if (itmMUser.isError() || itmMUser.isEmpty())
            {
                return;
            }

            //非當年度費用，查找舊年度費用
            string muid = itmMUser.getProperty("id", "");
            string new_expense = itmMUser.getProperty("in_expense", "");
            string old_expense = GetOldExpense(cfg, payConfig.PayMeeting.Id, resume);
            if (old_expense == "" || old_expense == new_expense)
            {
                return;
            }

            sql = "UPDATE IN_MEETING_USER SET in_expense = " + old_expense + " WHERE id = '" + muid + "'";
            Item itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("舊年度費用發生錯誤");
            }
        }

        private string GetOldExpense(TConfig cfg, string meeting_id, TResume resume)
        {
            string sql = @"
                SELECT 
	                t2.in_questions
	                , t3.in_grand_filter
	                , t3.in_filter
	                , t3.in_value
	                , t3.in_label
	                , t3.in_expense_value
                FROM 
	                IN_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN 
	                IN_SURVEY t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                INNER JOIN 
	                IN_SURVEY_OPTION t3 WITH(NOLOCK)
	                ON t3.source_id = t1.related_id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t2.in_property = 'in_l1'
	                AND t3.in_value = N'{#in_value}'
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_value}", resume.option_value_year);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmData = cfg.inn.applySQL(sql);

            if (itmData.isError() || itmData.getResult() == "")
            {
                return "";
            }
            else
            {
                return itmData.getProperty("in_expense_value", "0");
            }
        }

        /// <summary>
        /// 新增與會者
        /// </summary>
        private bool AddMeetingUser(TConfig cfg
            , string meeting_id
            , List<Item> itmSurveys
            , Item itmResume
            , string in_l1
            , string in_l2
            , string in_mail)
        {

            itmResume.setProperty("in_l1", in_l1);
            itmResume.setProperty("in_l2", in_l2);
            itmResume.setProperty("in_mail", in_mail);

            if (!IsRegistered(cfg, meeting_id, itmResume))
            {
                NewRegister(cfg, itmResume, itmResume, meeting_id, itmSurveys);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 新建與會者 (註冊模式)
        /// </summary>
        private void NewRegister(TConfig cfg, Item itmParent, Item itmEntity, string meeting_id, List<Item> lstSurveys)
        {
            string in_mail = itmEntity.getProperty("in_mail", "");
            string in_org = itmEntity.getProperty("in_org", "");

            string survey_type = "1";
            string method = "register_meeting";

            string parameters = GetParamValues(lstSurveys, itmEntity);
            string isUserId = itmParent.getProperty("in_user_id", "");
            string isIndId = itmParent.getProperty("owned_by_id", "");

            string aml = ""
                + "<surveytype>" + survey_type + "</surveytype>"
                + "<meeting_id>" + meeting_id + "</meeting_id>"
                + "<agent_id>" + "</agent_id>"
                + "<isUserId>" + isUserId + "</isUserId>"
                + "<isIndId>" + isIndId + "</isIndId>"
                + "<method>" + method + "</method>"
                + "<email>" + in_mail + "</email>"
                + "<in_org>" + in_org + "</in_org>"
                + "<parameter>" + parameters + "</parameter>"
                + "";

            //CCO.Utilities.WriteDebug(strMethodName, "aml: " + aml);

            Item itmMethod = cfg.inn.applyMethod("In_meeting_register", aml);

            if (itmMethod.isError())
            {
                throw new Exception("創建成員帳號發生錯誤");
            }
        }

        /// <summary>
        /// 轉換 Resume
        /// </summary>
        private TResume MapResume(TConfig cfg, Item itmReturn)
        {
            string in_sno = itmReturn.getProperty("in_sno", "");
            string inn_new_apply = itmReturn.getProperty("inn_new_apply", "");

            if (in_sno == "")
            {
                throw new Exception("帳號 不可為空白");
            }

            Item itmResume = cfg.inn.applySQL("SELECT TOP 1 * FROM IN_RESUME WITH(NOLOCK) WHERE in_sno = '" + in_sno + "'");
            if (itmResume.isError() || itmResume.getItemCount() != 1)
            {
                throw new Exception("Resume 資料錯誤");
            }

            TResume entity = new TResume
            {
                Value = itmResume,
                id = itmResume.getProperty("id", ""),
                in_name = itmResume.getProperty("in_name", ""),
                login_name = itmResume.getProperty("login_name", ""),
                in_member_unit = itmResume.getProperty("in_member_unit", ""),
                in_member_type = itmResume.getProperty("in_member_type", ""),
                in_member_status = itmResume.getProperty("in_member_status", ""),
                in_member_last_fee = itmResume.getProperty("in_member_last_fee", ""),
                in_member_last_year = itmResume.getProperty("in_member_last_year", ""),
                in_apply_year = itmResume.getProperty("in_apply_year", "0"),
                in_sys_note = itmResume.getProperty("in_sys_note", ""),

                inn_new_apply = inn_new_apply,
                WestYear = cfg.dtNow.Year.ToString(),
                TwYear = (cfg.dtNow.Year - 1911).ToString(),
                PayConfigs = new List<TMPayConfig>(),

                Start_WestYear = cfg.dtNow.Year,
                End_WestYear = cfg.dtNow.Year,
                Records = new List<TPayRecord>(),
                Paid_WestYear_List = new List<int>(),
                Unpaid_WestYear_List = new List<int>(),
            };
            entity.apply_year = GetIntValue(entity.in_apply_year);

            // if (entity.in_sys_note != "")
            // {
            //     throw new Exception(entity.in_sys_note + "，不產生年費繳費單: ");
            // }


            if (entity.in_member_type == "vip_mbr")
            {
                AppendResumePayRecords_2022_Mbr(cfg, entity);
            }
            else
            {
                AppendResumePayRecords_2022(cfg, entity);
            }
            //AppendResumePayRecords_2021(CCO, strMethodName, inn, entity);

            switch (entity.in_member_type)
            {
                case "reg":
                    entity.member_type_label = "被報名者";
                    entity.need_create_payment = false;
                    break;

                case "mbr"://新註冊，需先入會
                    entity.member_type_label = "一般會員";
                    entity.need_create_payment = true;
                    entity.meeting_member_type = "個人會員";
                    entity.option_value_join = "個人入會費";
                    entity.option_value_year = "個人會員年費";
                    break;

                case "vip_mbr":
                case "vip_minority":
                    entity.member_type_label = "個人會員";
                    entity.need_create_payment = true;
                    entity.meeting_member_type = "個人會員";
                    entity.option_value_join = "個人入會費";
                    entity.option_value_year = "個人會員年費";
                    break;

                case "vip_group":
                    entity.member_type_label = "一般團體會員";
                    entity.need_create_payment = true;
                    entity.meeting_member_type = "個人會員";
                    entity.option_value_join = "一般團體入會費";
                    entity.option_value_year = "一般團體會員年費";
                    break;

                case "gym"://新註冊，需先入會
                    entity.member_type_label = "道館";
                    entity.need_create_payment = true;
                    entity.meeting_member_type = "個人會員";
                    entity.option_value_join = "一般團體入會費";
                    entity.option_value_year = "一般團體會員年費";
                    break;

                case "vip_gym":
                    if (entity.in_member_unit == "社")
                    {
                        entity.member_type_label = "團體會員";
                        entity.need_create_payment = true;
                        entity.meeting_member_type = "團體會員";
                        entity.option_value_join = "學校社團入會費";
                        entity.option_value_year = "學校社團會員年費";
                    }
                    else
                    {
                        entity.member_type_label = "團體會員";
                        entity.need_create_payment = true;
                        entity.meeting_member_type = "團體會員";
                        entity.option_value_join = "道館社團入會費";
                        entity.option_value_year = "道館社團會員年費";
                    }
                    break;

                case "schl":
                    entity.member_type_label = "學校";
                    entity.need_create_payment = true;
                    entity.meeting_member_type = "團體會員";
                    entity.option_value_join = "學校社團入會費";
                    entity.option_value_year = "學校社團會員年費";
                    break;

                case "vip_schl":
                    entity.member_type_label = "學校社團";
                    entity.need_create_payment = true;
                    entity.option_value_join = "學校社團入會費";
                    entity.option_value_year = "學校社團會員年費";
                    break;

                case "area_cmt":
                    //entity.member_type_label = "地區委員會";
                    //entity.need_create_payment = false;
                    entity.member_type_label = "一般團體會員";
                    entity.need_create_payment = true;
                    entity.meeting_member_type = "個人會員";
                    entity.option_value_join = "一般團體入會費";
                    entity.option_value_year = "一般團體會員年費";
                    break;

                case "prjt_cmt":
                    entity.member_type_label = "專項委員會";
                    entity.need_create_payment = false;
                    break;

                case "asc"://協會人員無須繳費
                    // entity.member_type_label = "協會";
                    // entity.need_create_payment = false;

                    entity.member_type_label = "一般會員";
                    entity.need_create_payment = true;
                    entity.meeting_member_type = "個人會員";
                    entity.option_value_join = "個人入會費";
                    entity.option_value_year = "個人會員年費";

                    break;

                case "other":
                    entity.member_type_label = "其他";
                    entity.need_create_payment = false;
                    break;

                case "sys":
                    entity.member_type_label = "一般會員";
                    entity.need_create_payment = false;
                    break;

                default:
                    break;
            }

            return entity;
        }

        /// <summary>
        /// 處理未付款年度(個人會員)
        /// </summary>
        private void AppendResumePayRecords_2022_Mbr(TConfig cfg, TResume resume)
        {
            int minY = 2019;//系統起算年度 = 民國 108
            int maxY = resume.End_WestYear;

            string status = resume.in_member_status;

            if (status == "合格會員")
            {
                if (NeedPaid(resume, maxY))
                {
                    resume.Unpaid_WestYear_List.Add(maxY);
                }
            }
            else if (status == "暫時停權" || status == "出會")
            {
                for (int y = minY; y <= maxY; y++)
                {
                    if (NeedPaid(resume, y))
                    {
                        resume.Unpaid_WestYear_List.Add(y);
                    }
                }
            }
        }

        /// <summary>
        /// 處理未付款年度
        /// </summary>
        private void AppendResumePayRecords_2022(TConfig cfg, TResume resume)
        {
            int minY = 2020;//系統起算年度 = 民國 109
            int maxY = resume.End_WestYear;

            string status = resume.in_member_status;

            if (status == "合格會員")
            {
                if (NeedPaid(resume, maxY))
                {
                    resume.Unpaid_WestYear_List.Add(maxY);
                }
            }
            else if (status == "暫時停權" || status == "出會")
            {
                for (int y = minY; y <= maxY; y++)
                {
                    if (NeedPaid(resume, y))
                    {
                        resume.Unpaid_WestYear_List.Add(y);
                    }
                }
            }
        }

        /// <summary>
        /// 處理未付款年度
        /// </summary>
        private void AppendResumePayRecords_2021(TConfig cfg, TResume resume)
        {
            if (resume.in_member_status == "合格會員")
            {
                resume.Unpaid_WestYear_List.Add(resume.End_WestYear);
                return;
            }
            else if (resume.in_member_status == "暫時停權")
            {
                // int min_west_year = resume.End_WestYear - 2;
                int min_west_year = resume.End_WestYear - 1;
                for (int west_year = min_west_year; west_year <= resume.End_WestYear; west_year++)
                {
                    if (NeedPaid(resume, west_year))
                    {
                        resume.Unpaid_WestYear_List.Add(west_year);
                    }
                }
            }
            else if (resume.in_member_status == "出會")
            {
                // int min_west_year = resume.End_WestYear - 2;
                int min_west_year = resume.End_WestYear - 1;
                for (int west_year = min_west_year; west_year <= resume.End_WestYear; west_year++)
                {
                    resume.Unpaid_WestYear_List.Add(west_year);
                }
            }
        }

        private bool NeedPaid(TResume resume, int west_year)
        {
            if (resume.apply_year > west_year)
            {
                return false;
            }

            bool need = true;
            string pro = PayYearProperty(west_year);
            string val = resume.Value.getProperty(pro, "");//20XX has paid

            if (val == "1")
            {
                need = false;
            }

            return need;
        }

        private string PayYearProperty(int west_year)
        {
            switch (west_year)
            {
                case 2019: return "in_pay_year3";
                case 2020: return "in_pay_year2";
                case 2021: return "in_pay_year1";

                case 2022: return "in_pay_year4";
                case 2023: return "in_pay_year5";
                case 2024: return "in_pay_year6";

                case 2025: return "in_pay_year7";
                case 2026: return "in_pay_year8";
                case 2027: return "in_pay_year9";

                case 2028: return "in_pay_year10";
                case 2029: return "in_pay_year11";
                case 2030: return "in_pay_year12";

                case 2031: return "in_pay_year13";

                default: throw new Exception("繳費年度異常: " + west_year);
            }
        }

        /// <summary>
        /// 處理未繳費年度 (歷年)
        /// </summary>
        private void AppendResumePayRecords_ALL(TConfig cfg, TResume resume)
        {
            string sql = "SELECT * FROM IN_RESUME_PAY WITH(NOLOCK) WHERE source_id = '" + resume.id + "'";
            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();

            if (count <= 0)
            {
                resume.Unpaid_WestYear_List.Add(resume.End_WestYear);
                return;
            }

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_year = item.getProperty("in_year", "");
                int west_year = GetIntValue(in_year);

                if (west_year < resume.Start_WestYear)
                {
                    resume.Start_WestYear = west_year;
                }

                resume.Paid_WestYear_List.Add(west_year);
            }

            for (int i = resume.Start_WestYear; i <= resume.End_WestYear; i++)
            {
                int west_year = i;
                if (!resume.Paid_WestYear_List.Contains(west_year))
                {
                    resume.Unpaid_WestYear_List.Add(west_year);
                }
            }
        }

        /// <summary>
        /// 講師履歷資料模型
        /// </summary>
        private class TResume
        {
            public string id { get; set; }

            /// <summary>
            /// 姓名 or 設立名稱
            /// </summary>
            public string in_name { get; set; }

            /// <summary>
            /// 系統帳號
            /// </summary>
            public string login_name { get; set; }

            /// <summary>
            /// 社館類型
            /// </summary>
            public string in_member_unit { get; set; }

            /// <summary>
            /// 會員類型
            /// </summary>
            public string in_member_type { get; set; }

            /// <summary>
            /// 會員狀態
            /// </summary>
            public string in_member_status { get; set; }

            /// <summary>
            /// 最後繳費年度
            /// </summary>
            public string in_member_last_year { get; set; }

            /// <summary>
            /// 最後繳費金額
            /// </summary>
            public string in_member_last_fee { get; set; }

            /// <summary>
            /// 申請設立年度 OR 入會年度
            /// </summary>
            public string in_apply_year { get; set; }

            /// <summary>
            /// 申請設立年度 OR 入會年度
            /// </summary>
            public int apply_year { get; set; }

            /// <summary>
            /// 系統備註
            /// </summary>
            public string in_sys_note { get; set; }

            /// <summary>
            /// 原始資料
            /// </summary>
            public Item Value { get; set; }

            /// <summary>
            /// 最新年度的繳費繳費參考
            /// </summary>
            public TMeeting NewestMeeting { get; set; }

            /// <summary>
            /// 各個年度的繳費參考
            /// </summary>
            public List<TMPayConfig> PayConfigs { get; set; }

            /// <summary>
            /// 是否需要產生繳費單
            /// </summary>
            public bool need_create_payment { get; set; }

            /// <summary>
            /// 會員類型標籤
            /// </summary>
            public string member_type_label { get; set; }

            /// <summary>
            /// 活動會員類型
            /// </summary>
            public string meeting_member_type { get; set; }

            /// <summary>
            /// 入會費 in_value (個人入會費、一般團體入會費、道館社團入會費、學校社團入會費)
            /// </summary>
            public string option_value_join { get; set; }

            /// <summary>
            /// 年費 in_value (個人會員年費、一般團體會員年費、道館社團會員年費、學校社團會員年費)
            /// </summary>
            public string option_value_year { get; set; }

            /// <summary>
            /// 年度(西元)
            /// </summary>
            public string WestYear { get; set; }

            /// <summary>
            /// 年度(民國)
            /// </summary>
            public string TwYear { get; set; }

            /// <summary>
            /// 是否為新申請入會
            /// </summary>
            public string inn_new_apply { get; set; }

            /// <summary>
            /// 會費繳納記錄
            /// </summary>
            public List<TPayRecord> Records { get; set; }

            /// <summary>
            /// 起算年度
            /// </summary>
            public int Start_WestYear { get; set; }

            /// <summary>
            /// 結算年度
            /// </summary>
            public int End_WestYear { get; set; }

            /// <summary>
            /// 已繳費年度
            /// </summary>
            public List<int> Paid_WestYear_List { get; set; }

            /// <summary>
            /// 未繳費年度
            /// </summary>
            public List<int> Unpaid_WestYear_List { get; set; }
        }

        private class TPayRecord
        {
            public Item Value { get; set; }
            public int WestYear { get; set; }
        }


        private class TMPayConfig
        {
            public TMeeting PayMeeting { get; set; }
            public Item PayOption { get; set; }
            public int WestYear { get; set; }
            public string TwYear { get; set; }
        }

        /// <summary>
        /// 繳費設定參考
        /// </summary>
        private class TMeeting
        {
            /// <summary>
            /// 年度(西元)
            /// </summary>
            public int WestYear { get; set; }

            /// <summary>
            /// 年度(民國)
            /// </summary>
            public string TwYear { get; set; }

            /// <summary>
            /// 年度 Meeting
            /// </summary>
            public Item Value { get; set; }

            /// <summary>
            /// 年度 Meeting 問項
            /// </summary>
            public List<Item> Surveys { get; set; }

            /// <summary>
            /// 繳費項目細項 (In_Survey_Option, key: in_value)
            /// </summary>
            public Dictionary<string, Item> SurveyOptions { get; set; }

            /// <summary>
            /// meeting id
            /// </summary>
            public string Id { get; set; }

            /// <summary>
            /// meeting title
            /// </summary>
            public string Title { get; set; }
        }

        #region 資料存取

        /// <summary>
        /// 取得問項清單
        /// </summary>
        private List<Item> GetMeetingSurveyList(TConfig cfg, string meeting_id)
        {
            string sql = @"
            SELECT
                t3.id,
                t3.in_property
            FROM
                IN_MEETING t1 WITH(NOLOCK)
            INNER JOIN
                IN_MEETING_SURVEYS t2 WITH(NOLOCK) ON t2.source_id = t1.id
            INNER JOIN
                IN_SURVEY t3 WITH(NOLOCK) ON t3.id = t2.related_id
            WHERE
                t1.id = '{#meeting_id}'
                AND ISNULL(in_property, '') <> ''
            ORDER BY
                t2.sort_order
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                throw new Exception("取得問項清單發生錯誤 _# sql: " + sql);
            }

            List<Item> list = new List<Item>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                list.Add(items.getItemByIndex(i));
            }

            return list;
        }
        /// <summary>
        /// 取得問項選項 (In_Survey_Option, key: in_value)
        /// </summary>
        private Dictionary<string, Item> GetSurveyOptions(TConfig cfg, string meeting_id)
        {
            string sql = @"
                SELECT
                    t4.*
                FROM
                    IN_MEETING t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_SURVEYS t2 WITH(NOLOCK) 
				    ON t2.source_id = t1.id
                INNER JOIN
                    IN_SURVEY t3 WITH(NOLOCK) 
				    ON t3.id = t2.related_id
                INNER JOIN
                    IN_SURVEY_OPTION t4 WITH(NOLOCK) 
				    ON t4.source_id = t3.id
                WHERE
                    t1.id = '{#meeting_id}'
                    AND t3.in_property = N'in_l1'
                ORDER BY
                    t4.sort_order
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                throw new Exception("取得問項選項發生錯誤 _# sql: " + sql);
            }

            Dictionary<string, Item> map = new Dictionary<string, Item>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string key = item.getProperty("in_value", "");
                if (map.ContainsKey(key))
                {

                }
                else
                {
                    map.Add(key, item);
                }
            }

            return map;
        }

        /// <summary>
        /// 取得會費繳納組態 (in_member_type: 團體會員、個人會員, year: 西元年)
        /// </summary>
        private TMeeting GetMeeting(TConfig cfg, string in_member_type, string in_annual)
        {
            string sql = "";
            Item items = null;
            int count = 0;

            //設定會費活動

            sql = @"
                SELECT 
                    TOP 1 id
                    , in_title
                    , in_annual
                FROM 
                    IN_MEETING WITH(NOLOCK) 
                WHERE 
                    in_meeting_type = N'payment' 
                    AND in_member_type = N'{#in_member_type}'
                    AND in_annual = N'{#in_annual}'
				ORDER BY 
					created_on DESC
            ";

            sql = sql.Replace("{#in_member_type}", in_member_type)
                .Replace("{#in_annual}", in_annual);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            items = cfg.inn.applySQL(sql);

            count = items.getItemCount();

            if (count != 1)
            {
                return null;
            }

            Item itmMeeting = items.getItemByIndex(0);

            TMeeting meeting = new TMeeting();
            meeting.Value = itmMeeting;
            meeting.Id = itmMeeting.getProperty("id", "");
            meeting.Title = itmMeeting.getProperty("in_title", "");

            meeting.Surveys = GetMeetingSurveyList(cfg, meeting.Id);
            meeting.SurveyOptions = GetSurveyOptions(cfg, meeting.Id);

            int annual = GetIntValue(itmMeeting.getProperty("in_annual", "0"));
            meeting.TwYear = annual.ToString();
            meeting.WestYear = annual + 1911;

            return meeting;
        }

        /// <summary>
        /// 是否已報名
        /// </summary>
        private bool IsRegistered(TConfig cfg, string meeting_id, Item itmResume)
        {
            string sql = @"
                SELECT TOP 1
	                id 
                FROM 
	                IN_MEETING_USER WITH(NOLOCK) 
                WHERE 
	                source_id = '{#meeting_id}' 
	                AND in_creator_sno = N'{#in_creator_sno}' 
	                AND in_l1 = N'{#in_l1}' 
	                AND in_l2 = N'{#in_l2}'
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_creator_sno}", itmResume.getProperty("in_sno", ""))
                .Replace("{#in_l1}", itmResume.getProperty("in_l1", ""))
                .Replace("{#in_l2}", itmResume.getProperty("in_l2", ""));

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            return count == 1;
        }

        #endregion 資料存取

        /// <summary>
        /// 組成 Url 參數
        /// </summary>
        /// <param name="list">問項清單</param>
        /// <param name="itmEntity">與會者資料</param>
        private string GetParamValues(List<Item> list, Item itmEntity)
        {
            string result = "";
            int count = list.Count;
            for (int i = 0; i < count; i++)
            {
                Item item = list[i];
                string id = item.getProperty("id", "");
                string property = item.getProperty("in_property", "");

                string value = itmEntity.getProperty(property, "");

                if (value == "")
                {
                    continue;
                }

                if (property == "in_birth")
                {
                    value = GetDateTimeValue(value, "yyyy-MM-dd");
                }

                if (result != "")
                {
                    result += "&amp;";
                }

                result += id + "=" + value;
            }
            return result;
        }


        private int GetIntValue(string value)
        {
            int result = 0;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            else
            {
                return 0;
            }
        }

        private string GetDateTimeValue(string value, string format = "yyyy-MM-dd", int hours = 0)
        {
            if (value == "") return "";

            string day = value.Replace("/", "-");

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(day, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string scene { get; set; }
            
            public DateTime dtNow { get; set; }
        }
    }
}