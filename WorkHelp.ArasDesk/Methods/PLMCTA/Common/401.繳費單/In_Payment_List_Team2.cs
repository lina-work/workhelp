﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using Newtonsoft.Json.Linq;
using Spire.Doc;
using Spire.Doc.Documents.Rendering;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_Payment_List_Team2 : Item
    {
        public In_Payment_List_Team2(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 團隊計費
                日誌: 
                    - 2024-09-26: 品勢團體計費 (lina)
                    - 2022-02-10: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Payment_List_Team";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                in_creator_sno = itmR.getProperty("in_creator_sno", ""),
                scene = itmR.getProperty("scene", ""),
            };

            if (cfg.meeting_id == "")
            {
                throw new Exception("賽事 id 不可為空值");
            }

            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title, in_url, in_course_fees, in_poomsae_fees FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("賽事資料錯誤");
            }

            cfg.in_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.in_url = cfg.itmMeeting.getProperty("in_url", "");
            cfg.in_course_fees = cfg.itmMeeting.getProperty("in_course_fees", "0");
            cfg.in_poomsae_fees = cfg.itmMeeting.getProperty("in_poomsae_fees", "0");
            cfg.course_fees = GetInt(cfg.in_course_fees);
            cfg.poomsae_fees = GetInt(cfg.in_poomsae_fees);

            itmR.setProperty("mt_title", cfg.in_title);

            switch (cfg.scene)
            {
                case "page":
                    Page(cfg, itmR);
                    break;

                    //case "pay":
                    //    Calculate(cfg, itmR);
                    //    break;
            }

            return itmR;
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            cfg.itmLogin = cfg.inn.newItem("In_Resume", "get");
            cfg.itmLogin.setProperty("in_user_id", cfg.strUserId);
            cfg.itmLogin = cfg.itmLogin.apply();

            string login_sno = cfg.itmLogin.getProperty("in_sno", "");

            Item itmRoleResult = cfg.inn.applyMethod("In_Association_Role"
                , "<in_sno>" + login_sno + "</in_sno>"
                + "<idt_names>ACT_ASC_Accounting,ACT_ASC_Training</idt_names>");

            cfg.isMeetingAdmin = itmRoleResult.getProperty("inn_result", "") == "1";

            if (!cfg.isMeetingAdmin)
            {
                //非管理者，只限看到自己的資料
                cfg.in_creator_sno = login_sno;
            }

            if (cfg.in_creator_sno == "")
            {
                cfg.in_creator_sno = login_sno;
                //throw new Exception("協助報名者身分證號 不可為空值");
            }

            //單位應付費用統計
            var orgs = MUsersToOrgs(cfg);
            var org_table = OrgTableContents(cfg, orgs);

            itmReturn.setProperty("inn_table", org_table);
        }

        //單位報名 Table
        private string OrgTableContents(TConfig cfg, List<TOrg> orgs)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = " " });
            fields.Add(new TField { title = "報名帳號" });
            fields.Add(new TField { title = "所屬單位" });
            fields.Add(new TField { title = "報名費合計" });
            fields.Add(new TField { title = "已付款" });
            fields.Add(new TField { title = "未付款" });
            fields.Add(new TField { title = "未確認" });

            var head = new StringBuilder();
            var body = new StringBuilder();

            //設定標題列
            AppendHeads(cfg, fields, head);

            string cssC = "class='text-center' style='vertical-align: middle;'";
            string cssL = "class='text-left' style='vertical-align: middle;'";
            string cssR = "class='text-right' style='vertical-align: middle;'";

            //設定資料列
            for (var i = 0; i < orgs.Count; i++)
            {
                var org = orgs[i];
                org.id = "org-" + (i + 1);
                var btn = "<span class='event-btn bg-primary' onclick='ToggleSubRowClick(this)' data-state='hide' data-id='" + org.id + "'> &nbsp; <i class='fa fa-plus'></i> &nbsp; </span>";

                body.Append("<tr>");
                body.Append("  <td " + cssC + "'>" + btn + "</td>");
                body.Append("  <td " + cssC + "'>" + org.in_creator_sno + "</td>");
                body.Append("  <td " + cssC + "'>" + org.key + "</td>");
                body.Append("  <td " + cssR + "'>" + "$" + org.bill_amount.ToString("###,##0") + "</td>");
                body.Append("  <td " + cssR + "'>" + GetPaidAmomunt(cfg, org) + "</td>");
                body.Append("  <td " + cssR + "'>" + GetNoPayAmomunt(cfg, org) + "</td>");
                body.Append("  <td " + cssR + "'>" + org.waiting_amount + "</td>");
                body.Append("</tr>");

                body.Append("<tr id='" + org.id + "' style='display:none'>");
                body.Append("  <td colspan='" + fields.Count + "'>" + GenerateSecondTable(cfg, org) + "</td>");
                body.Append("</tr>");
            }

            string table_id = "data_table";

            StringBuilder table = new StringBuilder();
            table.Append("<table id='" + table_id + "' class='table table-bordered'>");
            table.Append(head);
            body.Append("<tbody>");
            table.Append(body);
            body.Append("</tbody>");
            table.Append("</table>");

            return table.ToString();
        }

        private string GetPaidAmomunt(TConfig cfg, TOrg org)
        {
            var rows = org.meeting_pay_list_f;
            if (rows.Count == 0) return "$0";
            if (rows.Count == 1) return "<span class='text-danger' onclick='ShowItemNumber(this)' data-id='" + rows[0].item_number + "'>$" + rows[0].pay_amount_real.ToString("###,##0");

            var total = 0;
            var lines = new List<string>();
            for (var i = 0; i < rows.Count; i++)
            {
                var p = rows[i];
                total += p.pay_amount_real;
                lines.Add(p.item_number + ": " + p.pay_amount_real);
            }

            return "$" + total.ToString("###,##0")
                + "<br>"
                + string.Join("<br>", lines);
        }

        private string GetNoPayAmomunt(TConfig cfg, TOrg org)
        {
            var rows = org.meeting_pay_list_u;
            if (rows.Count == 0) return "$0";
            if (rows.Count == 1) return "<span class='text-danger' onclick='ShowItemNumber(this)' data-id='" + rows[0].item_number + "'>$" + rows[0].pay_amount_exp.ToString("###,##0");

            var total = 0;
            var lines = new List<string>();
            for (var i = 0; i < rows.Count; i++)
            {
                var p = rows[i];
                total += p.pay_amount_exp;
                lines.Add(p.item_number + ": " + p.pay_amount_exp);
            }

            return "$" + total.ToString("###,##0")
                + "<br>"
                + string.Join("<br>", lines);
        }

        private string GenerateSecondTable(TConfig cfg, TOrg org)
        {
            var body = new StringBuilder();

            var cssC = "class='text-center bg-warning' style='vertical-align: middle; border: 1px solid red;'";
            var cssR = "class='text-right bg-warning' style='vertical-align: middle; border: 1px solid red;'";
            var cssL = "class='text-left bg-warning' style='vertical-align: middle; border: 1px solid red;'";

            var sorted = org.pays.OrderBy(x => x.sort).ToList();
            for (var i = 0; i < sorted.Count; i++)
            {
                var pay = sorted[i];
                pay.id = org.id + "-pay-" + (i + 1);
                if (pay.sect_type_name == "隊職員") continue;

                var btn = "<span class='event-btn bg-warning' onclick='ToggleSubRowClick(this)' data-state='hide' data-id='" + pay.id + "'> &nbsp; <i class='fa fa-plus'></i> &nbsp; </span>";

                body.Append("<tr>");
                body.Append("  <td " + cssC + ">" + btn + "</td>");
                body.Append("  <td " + cssL + ">" + org.key + "</td>");
                body.Append("  <td " + cssL + ">" + pay.sect_type_name + "</td>");
                body.Append("  <td " + cssL + ">" + pay.sect_age_name + "</td>");
                body.Append("  <td " + cssL + ">" + pay.sect_gender_name + "</td>");
                body.Append("  <td " + cssL + ">" + pay.sect_team_name + "</td>");
                body.Append("  <td " + cssR + ">" + pay.bill_amount.ToString("###,##0") + "</td>");
                body.Append("</tr>");

                body.Append("<tr id='" + pay.id + "' style='display:none'>");
                body.Append("  <td colspan='7'>" + GeneratePersonTable(cfg, org, pay) + "</td>");
                body.Append("</tr>");
            }

            StringBuilder table = new StringBuilder();
            table.Append("<table id='" + org.id + "-sub-table' class='table table-bordered'>");
            table.Append(body);
            table.Append("</table>");

            return table.ToString();
        }

        private string GeneratePersonTable(TConfig cfg, TOrg org, TPayItem pay)
        {
            var body = new StringBuilder();

            var cssC = "class='text-center' style='background-color: #F5EAD1; vertical-align: middle;'";
            var cssR = "class='text-right' style='background-color: #F5EAD1; vertical-align: middle;'";
            var cssL = "class='text-left' style='background-color: #F5EAD1; vertical-align: middle;'";

            for (var i = 0; i < pay.teams.Count; i++)
            {
                var team = pay.teams[i];
                body.Append("<tr>");
                body.Append("  <td " + cssL + ">" + (i + 1) + "</td>");
                body.Append("  <td " + cssL + ">" + team.in_l1 + "</td>");
                body.Append("  <td " + cssL + ">" + team.in_l2 + "</td>");
                body.Append("  <td " + cssL + ">" + team.in_l3 + "</td>");
                body.Append("  <td " + cssL + ">" + team.in_index + "</td>");
                body.Append("  <td " + cssR + ">" + team.expense.ToString("###,##0") + "</td>");
                body.Append("  <td " + cssL + ">" + GetTeamMembers(cfg, org, pay, team) + "</td>");
                body.Append("</tr>");
            }

            StringBuilder table = new StringBuilder();
            table.Append("<table id='" + pay.id + "-sub-table' class='table table-bordered'>");
            table.Append(body);
            table.Append("</table>");

            return table.ToString();
        }

        private string GetTeamMembers(TConfig cfg, TOrg org, TPayItem pay, TPayTeam team)
        {
            var names = team.persons.Select(x => x.in_name);
            return string.Join("、", names);
        }

        private void AppendHeads(TConfig cfg, List<TField> fields, StringBuilder head)
        {
            head.Append("<thead>");
            head.Append(" <tr>");
            foreach (var field in fields)
            {
                head.Append("<th class='text-center bg-primary' rowspan='1' data-field='" + field.prop + "'>");
                head.Append(field.title);
                head.Append("</th>");
            }
            head.Append("</tr>");
            head.Append("</thead>");
        }

        private string PayStatusIcon(TConfig cfg, TField field, Item item)
        {
            StringBuilder builder = new StringBuilder();

            string[] properties = new string[]
            {
                "in_paynumber", //繳費單號
                "in_pay_amount_real", //實際收款金額
                "in_pay_photo", //上傳繳費照片(繳費收據)
                "in_return_mark", //審核退回說明
                "in_collection_agency", //代收機構
            };

            item.setProperty("in_paynumber", item.getProperty("item_number", ""));

            foreach (string property in properties)
            {
                builder.Append("<" + property + ">" + item.getProperty(property, "") + "</" + property + ">");
            }

            Item itmResult = cfg.inn.applyMethod("In_PayType_Icon", builder.ToString());

            return itmResult.getProperty("in_pay_type", "");
        }

        private List<TOrg> MUsersToOrgs(TConfig cfg)
        {
            var items = GetMeetingUserItems(cfg);
            var orgs = new List<TOrg>();
            MUsersToOrgs(cfg, orgs, items);

            var itmPays = GetMeetingPayItems(cfg);
            MPaysToOrgs(cfg, orgs, itmPays);

            return orgs;
        }

        private void MPaysToOrgs(TConfig cfg, List<TOrg> orgs, Item items)
        {
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var payment = MapMeetingPay(cfg, item);
                var org = orgs.Find(x => x.key == payment.key);
                if (org != null)
                {
                    org.meeting_pay_list.Add(payment);
                }

                if (payment.has_paid)
                {
                    org.meeting_pay_list_f.Add(payment);
                }
                else if (payment.has_merge)
                {
                    org.meeting_pay_list_m.Add(payment);
                }
                else
                {
                    org.meeting_pay_list_u.Add(payment);
                }
            }
        }

        private TMeetingPay MapMeetingPay(TConfig cfg, Item item)
        {
            var x = new TMeetingPay
            {
                key = item.getProperty("in_current_org", ""),
                item_number = item.getProperty("item_number", ""),
                in_pay_amount_exp = item.getProperty("in_pay_amount_exp", ""),
                in_pay_amount_real = item.getProperty("in_pay_amount_real", ""),
                in_pay_date_real = item.getProperty("in_pay_date_real", ""),
                pay_bool = item.getProperty("pay_bool", ""),
                in_collection_agency = item.getProperty("in_collection_agency", ""),
            };

            x.pay_amount_exp = GetInt(x.in_pay_amount_exp);
            x.pay_amount_real = GetInt(x.in_pay_amount_real);
            if (x.pay_bool == "已繳費")
            {
                x.has_paid = true;
            }
            else if (x.pay_bool == "合併")
            {
                x.has_merge = true;
            }
            return x;
        }

        private class TMeetingPay
        {
            public string key { get; set; }
            public string item_number { get; set; }
            public string in_pay_amount_exp { get; set; }
            public string in_pay_amount_real { get; set; }
            public string in_pay_date_real { get; set; }
            public string pay_bool { get; set; }
            public string in_collection_agency { get; set; }

            public int pay_amount_exp { get; set; }
            public int pay_amount_real { get; set; }
            public bool has_paid { get; set; }
            public bool has_merge { get; set; }
        }

        private void MUsersToOrgs(TConfig cfg, List<TOrg> orgs, Item items)
        {
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var muser = MapMUser(cfg, item);

                var org = orgs.Find(x => x.key == muser.org_key);
                if (org == null)
                {
                    org = MapOrg(cfg, muser);
                    orgs.Add(org);
                }

                var pay = org.pays.Find(x => x.key == muser.pay_key);
                if (pay == null)
                {
                    pay = MapPay(cfg, org, muser);
                    org.pays.Add(pay);
                }

                var team = pay.teams.Find(x => x.key == muser.team_key);
                if (team == null)
                {
                    team = MapTeam(cfg, org, pay, muser);
                    pay.teams.Add(team);
                    pay.sub_amount += team.expense;
                }

                var player = pay.players.Find(x => x.key == muser.in_sno);
                if (player == null)
                {
                    player = MapPlayer(cfg, org, pay, muser);
                    pay.players.Add(player);
                    //pay.sub_amount += player.expense;
                }

                player.items.Add(muser);

                team.persons.Add(muser);
            }

            for (var i = 0; i < orgs.Count; i++)
            {
                var org = orgs[i];
                for (var j = 0; j < org.pays.Count; j++)
                {
                    var pay = org.pays[j];
                    ResetPayAmount(cfg, org, pay);
                }
                org.bill_amount = org.pays.Sum(x => x.bill_amount);
            }
        }

        private void ResetPayAmount(TConfig cfg, TOrg org, TPayItem pay)
        {
            if (pay.is_poomsae && pay.players.Count <= 2)
            {
                pay.bill_amount = pay.players.Count * 800;
            }
            else
            {
                if (pay.sub_amount > pay.max_amount)
                {
                    pay.bill_amount = pay.max_amount;
                }
                else
                {
                    pay.bill_amount = pay.sub_amount;
                }
            }
        }

        private TPayTeam MapTeam(TConfig cfg, TOrg org, TPayItem pay, TMUser muser)
        {
            var x = new TPayTeam
            {
                key = muser.team_key,
                in_l1 = muser.in_l1,
                in_l2 = muser.in_l2,
                in_l3 = muser.in_l3,
                in_index = muser.in_index,
                expense = muser.expense,
                persons = new List<TMUser>()
            };
            return x;
        }

        private TPayPlayer MapPlayer(TConfig cfg, TOrg org, TPayItem pay, TMUser muser)
        {
            var x = new TPayPlayer
            {
                key = muser.in_sno,
                expense = muser.expense,
                items = new List<TMUser>(),
            };
            return x;
        }

        private TPayItem MapPay(TConfig cfg, TOrg org, TMUser muser)
        {
            var x = new TPayItem
            {
                key = muser.pay_key,
                sort = muser.pay_sort,
                sect_type_name = muser.sect_type_name,
                sect_age_name = muser.sect_age_name,
                sect_gender_name = muser.sect_gender_name,
                sect_team_name = muser.sect_team_name,
                teams = new List<TPayTeam>(),
                players = new List<TPayPlayer>(),
                is_staff = muser.is_staff,
                is_poomsae = muser.is_poomsae,
                is_fight = muser.is_fight,
                max_amount = 0,
                sub_amount = 0,
            };
            if (x.is_staff)
            {
                x.max_amount = 0;
            }
            else if (muser.is_poomsae)
            {
                x.max_amount = cfg.poomsae_fees;
            }
            else
            {
                x.max_amount = cfg.course_fees;
            }
            return x;
        }

        private TOrg MapOrg(TConfig cfg, TMUser muser)
        {
            var result = new TOrg
            {
                key = muser.in_current_org,
                in_creator = muser.in_creator,
                in_creator_sno = muser.in_creator_sno,
                bill_amount = 0,
                created_amount = 0,
                waiting_amount = 0,
                pays = new List<TPayItem>(),
                meeting_pay_list = new List<TMeetingPay>(),
                meeting_pay_list_f = new List<TMeetingPay>(),
                meeting_pay_list_u = new List<TMeetingPay>(),
                meeting_pay_list_m = new List<TMeetingPay>(),
            };
            return result;
        }

        private TMUser MapMUser(TConfig cfg, Item item)
        {
            var x = new TMUser
            {
                id = item.getProperty("id", ""),
                in_name = item.getProperty("in_name", ""),
                in_sno = item.getProperty("in_sno", ""),
                in_gender = item.getProperty("in_gender", ""),
                in_l1 = item.getProperty("in_l1", ""),
                in_l2 = item.getProperty("in_l2", ""),
                in_l3 = item.getProperty("in_l3", ""),
                in_index = item.getProperty("in_index", ""),
                in_team = item.getProperty("in_team", ""),
                in_expense = item.getProperty("in_expense", "0"),
                in_paynumber = item.getProperty("in_paynumber", ""),
                in_creator = item.getProperty("in_creator", ""),
                in_creator_sno = item.getProperty("in_creator_sno", ""),
                in_current_org = item.getProperty("in_current_org", "").Trim(),
            };

            x.expense = GetInt(x.in_expense);
            x.has_paynumber = x.in_paynumber != "";

            ResetSectType(cfg, x);
            ResetSectAge(cfg, x);
            ResetSectGender(cfg, x);
            ResetSectTeam(cfg, x);

            x.org_key = x.in_current_org;
            x.item_key = GetItemKey(cfg, x);
            x.team_key = GetTeamKey(cfg, x);
            ResetPayKey(cfg, x);

            return x;
        }

        private string GetItemKey(TConfig cfg, TMUser x)
        {
            return x.in_l1 + "-" + x.in_l2 + "-" + x.in_l3;
        }

        private string GetTeamKey(TConfig cfg, TMUser x)
        {
            return x.in_l1 + "-" + x.in_l2 + "-" + x.in_l3 + "-" + x.in_index;
        }

        private void ResetPayKey(TConfig cfg, TMUser x)
        {
            //對打-青年組-男-不分隊別
            //對打-青年組-男-A隊
            //對打-青年組-男-B隊
            //對打-青年組-混-A隊
            //品勢-青年組-男-不分隊別
            x.pay_key = x.sect_type_name + "-" + x.sect_age_name + "-" + x.sect_gender_name + "-" + x.sect_team_name;
            x.pay_sort = x.sect_type_code * 1000 + x.sect_age_code * 100 + x.sect_gender_code * 10 + x.sect_team_code;
        }

        private void ResetSectType(TConfig cfg, TMUser x)
        {
            x.sect_type_name = "對打";
            x.sect_type_code = 1;
            x.is_fight = true;
            x.is_staff = x.in_l1 == "隊職員";
            x.is_poomsae = x.in_l1.Contains("品勢");

            if (x.is_staff)
            {
                x.sect_type_name = "隊職員";
                x.sect_type_code = 3;
                x.is_fight = false;
            }
            if (x.is_poomsae)
            {
                x.sect_type_name = "品勢";
                x.sect_type_code = 2;
                x.is_fight = false;
            }
        }

        private void ResetSectAge(TConfig cfg, TMUser x)
        {
            if (x.in_l2.Contains("青年"))
            {
                x.sect_age_name = "青年";
                x.sect_age_code = 1;
            }
            else if (x.in_l2.Contains("青少年"))
            {
                x.sect_age_name = "青少年";
                x.sect_age_code = 2;
            }
            else
            {
                x.sect_age_name = "無年齡";
                x.sect_age_code = 3;
            }
        }

        private void ResetSectGender(TConfig cfg, TMUser x)
        {
            if (x.in_l2.Contains("混合"))
            {
                // x.sect_gender_name = "男女混合";
                // x.sect_gender_code = 1;
                if (x.in_gender == "男")
                {
                    x.sect_gender_name = "男子";
                    x.sect_gender_code = 2;
                }
                else if (x.in_gender == "女")
                {
                    x.sect_gender_name = "女子";
                    x.sect_gender_code = 3;
                }
                else
                {
                    x.sect_gender_name = "無性別";
                    x.sect_gender_code = 4;
                }
            }
            else if (x.in_l2.Contains("男"))
            {
                x.sect_gender_name = "男子";
                x.sect_gender_code = 2;
            }
            else if (x.in_l2.Contains("女"))
            {
                x.sect_gender_name = "女子";
                x.sect_gender_code = 3;
            }
            else
            {
                x.sect_gender_name = "無性別";
                x.sect_gender_code = 4;
            }
        }

        private void ResetSectTeam(TConfig cfg, TMUser x)
        {
            if (x.in_team == "")
            {
                x.sect_team_name = "不分隊別";
                x.sect_team_code = 1;
            }
            else
            {

                x.sect_team_name = x.in_team + "隊";
                x.sect_team_code = 2;
            }
        }

        private Item GetMeetingUserItems(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                t1.in_current_org
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_index
	                , t1.in_team
	                , t1.in_name
	                , t1.in_gender
	                , UPPER(t1.in_sno) AS 'in_sno'
	                , t1.in_paynumber
	                , t1.in_creator
	                , UPPER(t1.in_creator_sno) AS 'in_creator_sno'
	                , t12.in_expense_value AS 'in_expense'
                FROM 
                    IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    VU_MEETING_SVY_L2 t12
                    ON t12.source_id = t1.source_id
                    AND t12.in_filter = t1.in_l1
                    AND t12.in_value = t1.in_l2
                LEFT OUTER JOIN
                    VU_MEETING_SVY_L3 t13
                    ON t13.source_id = t1.source_id
                    AND t13.in_grand_filter = t1.in_l1
                    AND t13.in_filter = t1.in_l2
                    AND t13.in_value = t1.in_l3
                WHERE 
                    t1.source_id = '{#meeting_id}'
                ORDER BY 
                    t1.in_creator_sno
                    , t12.sort_order
                    , t13.sort_order
                    , t1.in_team
                    , t1.in_index
                ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_creator_sno}", cfg.in_creator_sno);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingPayItems(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                in_creator_sno
	                , in_current_org 
	                , item_number
	                , in_pay_amount_exp
	                , in_pay_amount_real
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, in_pay_date_real), 111) AS 'in_pay_date_real'
	                , pay_bool
	                , in_collection_agency
                FROM 
	                IN_MEETING_PAY WITH(NOLOCK)
                WHERE 
	                in_meeting = '{#meeting_id}'
                    AND pay_bool NOT IN (N'已取消')
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql); ;
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string in_creator_sno { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public string in_title { get; set; }
            public string in_url { get; set; }
            public string in_course_fees { get; set; }
            public string in_poomsae_fees { get; set; }
            public int course_fees { get; set; }
            public int poomsae_fees { get; set; }

            public Item itmPermit { get; set; }
            public Item itmLogin { get; set; }
            public bool isMeetingAdmin { get; set; }
        }

        private class TOrg
        {
            public string id { get; set; }
            public string key { get; set; }
            public string in_creator { get; set; }
            public string in_creator_sno { get; set; }

            /// <summary>
            /// 應付項目表
            /// </summary>
            public List<TPayItem> pays { get; set; }
            /// <summary>
            /// 總金額
            /// </summary>
            public int bill_amount { get; set; }
            /// <summary>
            /// 已付金額
            /// </summary>
            public int paid_amount { get; set; }
            /// <summary>
            /// 未付款金額
            /// </summary>
            public int created_amount { get; set; }
            /// <summary>
            /// 未成單金額
            /// </summary>
            public int waiting_amount { get; set; }

            /// <summary>
            /// 繳費單
            /// </summary>
            public List<TMeetingPay> meeting_pay_list { get; set; }
            public List<TMeetingPay> meeting_pay_list_f { get; set; }
            public List<TMeetingPay> meeting_pay_list_u { get; set; }
            public List<TMeetingPay> meeting_pay_list_m { get; set; }
        }

        private class TPayItem
        {
            public string id { get; set; }
            public string key { get; set; }
            public string sect_type_name { get; set; }
            public string sect_age_name { get; set; }
            public string sect_gender_name { get; set; }
            public string sect_team_name { get; set; }
            public int sort { get; set; }
            public int bill_amount { get; set; }
            public int max_amount { get; set; }
            public int sub_amount { get; set; }
            public List<TPayTeam> teams { get; set; }
            public List<TPayPlayer> players { get; set; }
            public bool is_staff { get; set; }
            public bool is_fight { get; set; }
            public bool is_poomsae { get; set; }
        }

        private class TPayTeam
        {
            public string key { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_index { get; set; }
            public int expense { get; set; }
            public List<TMUser> persons { get; set; }
        }

        private class TPayPlayer
        {
            public string key { get; set; }
            public int expense { get; set; }
            public List<TMUser> items { get; set; }
        }

        private class TMUser
        {
            public string id { get; set; }
            public string in_creator { get; set; }
            public string in_creator_sno { get; set; }
            public string in_current_org { get; set; }
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_gender { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_index { get; set; }
            public string in_team { get; set; }
            public string in_expense { get; set; }
            public string in_paynumber { get; set; }

            public string sect_type_name { get; set; }
            public int sect_type_code { get; set; }
            public string sect_age_name { get; set; }
            public int sect_age_code { get; set; }
            public string sect_gender_name { get; set; }
            public int sect_gender_code { get; set; }
            public string sect_team_name { get; set; }
            public int sect_team_code { get; set; }

            public string org_key { get; set; }
            public string item_key { get; set; }
            public string team_key { get; set; }
            public string pay_key { get; set; }
            public int pay_sort { get; set; }

            public bool is_staff { get; set; }
            public bool is_fight { get; set; }
            public bool is_poomsae { get; set; }
            public bool has_paynumber { get; set; }

            public int expense { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string prop { get; set; }
            public Func<TConfig, TField, Item, string> getVal { get; set; }
            public bool is_child { get; set; }
            public bool is_money { get; set; }
            public bool is_refund { get; set; }
        }

        private int GetInt(string value, int def = 0)
        {
            if (value == "" || value == "0") return 0;
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}