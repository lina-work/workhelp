﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using Newtonsoft.Json.Linq;
using Spire.Doc;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_Payment_List_Team : Item
    {
        public In_Payment_List_Team(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 團隊計費
                日誌: 
                    - 2022-02-10: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Payment_List_Team";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                in_creator_sno = itmR.getProperty("in_creator_sno", ""),
                scene = itmR.getProperty("scene", ""),
            };

            if (cfg.meeting_id == "")
            {
                throw new Exception("賽事 id 不可為空值");
            }

            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title, in_url, in_course_fees FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("賽事資料錯誤");
            }

            cfg.in_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.in_url = cfg.itmMeeting.getProperty("in_url", "");
            cfg.in_course_fees = cfg.itmMeeting.getProperty("in_course_fees", "0");
            cfg.course_fees = GetIntVal(cfg.in_course_fees);

            switch (cfg.scene)
            {
                case "page":
                    Page(cfg, itmR);
                    break;

                case "pay":
                    Calculate(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            cfg.itmLogin = cfg.inn.newItem("In_Resume", "get");
            cfg.itmLogin.setProperty("in_user_id", cfg.strUserId);
            cfg.itmLogin = cfg.itmLogin.apply();

            string login_sno = cfg.itmLogin.getProperty("in_sno", "");

            Item itmRoleResult = cfg.inn.applyMethod("In_Association_Role"
                , "<in_sno>" + login_sno + "</in_sno>"
                + "<idt_names>ACT_ASC_Accounting,ACT_ASC_Training</idt_names>");

            cfg.isMeetingAdmin = itmRoleResult.getProperty("inn_result", "") == "1";

            if (!cfg.isMeetingAdmin)
            {
                //非管理者，只限看到自己的資料
                cfg.in_creator_sno = login_sno;
            }

            if (cfg.in_creator_sno == "")
            {
                cfg.in_creator_sno = login_sno;
                //throw new Exception("協助報名者身分證號 不可為空值");
            }

            //單位應付費用統計
            var orgs = MUsersToOrgs(cfg);

            //單位已建費用統計
            var itmPays = GetPayList(cfg);
            var amounts = OrgWaitPayAmount(cfg, itmPays);

            //計算費用
            CalculateAmount(cfg, orgs, amounts);

            itmReturn.setProperty("inn_table", OrgTableContents(cfg, orgs));
            itmReturn.setProperty("inn_pay_table", PayTableContents(cfg, itmPays));

            itmReturn.setProperty("in_title", cfg.in_title);
            itmReturn.setProperty("in_url", cfg.in_url);
            itmReturn.setProperty("inn_course_fees", Dollar(cfg, cfg.in_course_fees));

            if (orgs.Count > 0)
            {
                itmReturn.setProperty("in_creator", orgs[0].in_creator);
            }
        }

        //繳費單 Table
        private string PayTableContents(TConfig cfg, Item itmPays)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "繳費單號", prop = "item_number" });
            fields.Add(new TField { title = "條碼2", prop = "in_code_2" });
            fields.Add(new TField { title = "繳費狀態", prop = "pay_bool", getVal = PayStatusIcon });
            fields.Add(new TField { title = "金額", prop = "in_pay_amount_exp", getVal = Dollar, is_money = true });
            fields.Add(new TField { title = "退款金額", prop = "in_refund_amount", getVal = Dollar, is_refund = true });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            //設定標題列
            AppendHeads(cfg, fields, head);

            //設定資料列
            body.Append("<tbody>");
            int count = itmPays.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmPay = itmPays.getItemByIndex(i);
                body.Append("<tr>");
                SetTDs(cfg, fields, itmPay, body, 1);
                body.Append("</tr>");
            }
            body.Append("</tbody>");

            string table_id = "pay_table";

            StringBuilder table = new StringBuilder();
            table.Append("<table id='" + table_id + "' class='table table-bordered'>");
            table.Append(head);
            table.Append(body);
            table.Append("</table>");

            return table.ToString();
        }

        //單位報名 Table
        private string OrgTableContents(TConfig cfg, List<TOrg> orgs)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "所屬單位", prop = "in_current_org" });
            fields.Add(new TField { title = "報名費合計", prop = "bill_amount", getVal = Dollar, is_money = true });
            fields.Add(new TField { title = "已付款", prop = "paid_amount", getVal = Dollar, is_money = true });
            fields.Add(new TField { title = "未付款", prop = "unpaid_amount", getVal = Dollar, is_money = true });
            fields.Add(new TField { title = "未確認", prop = "strpay", getVal = Dollar, is_money = true });
            fields.Add(new TField { title = "隊別", prop = "inn_group_name", is_child = true });
            fields.Add(new TField { title = "選手數", prop = "inn_group_qry", is_child = true });
            fields.Add(new TField { title = "報名費小計", prop = "inn_group_amount", getVal = Dollar, is_child = true, is_money = true });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            //設定標題列
            AppendHeads(cfg, fields, head);

            //設定資料列
            body.Append("<tbody>");
            foreach (var org in orgs)
            {
                body.Append("<tr>");
                AppendOrgTR(cfg, org, fields, body);
                body.Append("</tr>");
            }
            body.Append("</tbody>");

            string table_id = "data_table";

            StringBuilder table = new StringBuilder();
            table.Append("<table id='" + table_id + "' class='table table-bordered'>");
            table.Append(head);
            table.Append(body);
            table.Append("</table>");

            return table.ToString();
        }

        private void AppendHeads(TConfig cfg, List<TField> fields, StringBuilder head)
        {
            head.Append("<thead>");
            head.Append(" <tr>");
            foreach (var field in fields)
            {
                head.Append("<th class='text-center' rowspan='1' data-field='" + field.prop + "'>");
                head.Append(field.title);
                head.Append("</th>");
            }
            head.Append("</tr>");
            head.Append("</thead>");
        }

        private void AppendOrgTR(TConfig cfg, TOrg org, List<TField> fields, StringBuilder body)
        {
            var org_fields = fields.FindAll(x => !x.is_child);
            var team_fields = fields.FindAll(x => x.is_child);

            Item item = cfg.inn.newItem();
            //設定單位資料
            SetOrgVal(item, org);

            int rs = org.teams.Count;
            SetTDs(cfg, org_fields, item, body, rs + 1);

            bool is_merge = rs > 1;
            if (is_merge) body.Append("</tr>");

            foreach (var team in org.teams)
            {
                //設定隊伍資料
                SetTeamVal(item, team);

                if (is_merge) body.Append("<tr>");
                SetTDs(cfg, team_fields, item, body, 1);
                if (is_merge) body.Append("</tr>");
            }
        }

        private void SetTDs(TConfig cfg, List<TField> fields, Item item, StringBuilder body, int rs)
        {
            foreach (var field in fields)
            {
                SetTDVal(cfg, field, item, body, rs);
            }
        }

        private void SetTDVal(TConfig cfg, TField field, Item item, StringBuilder body, int rs)
        {
            string value = "";
            if (field.getVal != null)
            {
                value = field.getVal(cfg, field, item);
            }
            else
            {
                value = item.getProperty(field.prop, "");
            }

            string css = "text-center";
            if (field.is_money)
            {
                css = "text-right";
            }
            else if (field.is_refund)
            {
                css = "text-right";
                if (value != "$0")
                {
                    css += " bg-primary";
                }
            }

            body.Append("<td class='" + css + "' rowspan='" + rs + "' data-field='" + field.prop + "' style='vertical-align: middle;'>");
            body.Append(value);
            body.Append("</td>");
        }

        private string Dollar(TConfig cfg, TField field, Item item)
        {
            string value = item.getProperty(field.prop, "0");
            return Dollar(cfg, value);
        }

        private string Dollar(TConfig cfg, string value)
        {
            if (value.Length > 3)
            {
                return "$" + value.Insert(value.Length - 3, ",");
            }
            else
            {
                return "$" + value;
            }
        }

        private string PayStatusIcon(TConfig cfg, TField field, Item item)
        {
            StringBuilder builder = new StringBuilder();

            string[] properties = new string[]
            {
                "in_paynumber", //繳費單號
                "in_pay_amount_real", //實際收款金額
                "in_pay_photo", //上傳繳費照片(繳費收據)
                "in_return_mark", //審核退回說明
                "in_collection_agency", //代收機構
            };

            item.setProperty("in_paynumber", item.getProperty("item_number", ""));

            foreach (string property in properties)
            {
                builder.Append("<" + property + ">" + item.getProperty(property, "") + "</" + property + ">");
            }

            Item itmResult = cfg.inn.applyMethod("In_PayType_Icon", builder.ToString());

            return itmResult.getProperty("in_pay_type", "");
        }

        private void Calculate(TConfig cfg, Item itmReturn)
        {
            //單位應付費用統計
            var orgs = MUsersToOrgs(cfg);

            //單位已建費用統計
            var itmPays = GetPayList(cfg);
            var amounts = OrgWaitPayAmount(cfg, itmPays);

            //計算費用
            CalculateAmount(cfg, orgs, amounts);

            //附加 Team Item Relationship
            AppendOrgTeams(cfg, orgs, itmReturn);
        }

        //轉換
        private Dictionary<string, Item> MapOrgTeams(Item items)
        {
            Dictionary<string, Item> result = new Dictionary<string, Item>();
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_current_org = item.getProperty("in_current_org", "");
                if (!result.ContainsKey(in_current_org))
                {
                    result.Add(in_current_org, item);
                }
            }
            return result;
        }

        //附加 Team Item Relationship
        private void AppendOrgTeams(TConfig cfg, List<TOrg> orgs, Item itmReturn)
        {
            foreach (var org in orgs)
            {
                foreach (var team in org.teams)
                {
                    Item item = cfg.inn.newItem();
                    item.setType("inn_org_teampay");
                    item.setProperty("in_creator_sno", cfg.in_creator_sno);

                    //單位
                    SetOrgVal(item, org);

                    //隊伍: 男子組-A隊
                    SetTeamVal(item, team);

                    itmReturn.addRelationship(item);
                }
            }
        }

        private void SetOrgVal(Item item, TOrg org)
        {
            item.setProperty("in_current_org", org.name);
            item.setProperty("filed_amount", org.filed_amount.ToString());
            item.setProperty("bill_amount", org.bill_amount.ToString());
            item.setProperty("strpay", org.wait_amount.ToString());
            item.setProperty("unpaid_amount", org.unpaid_amount.ToString());
            item.setProperty("paid_amount", org.paid_amount.ToString());
        }

        private void SetTeamVal(Item item, TTeamSum team)
        {
            item.setProperty("inn_group_name", team.key);
            item.setProperty("inn_group_qry", team.list.Count.ToString());
            item.setProperty("inn_group_amount", team.bill_amount.ToString());
        }

        //計算費用
        private void CalculateAmount(TConfig cfg, List<TOrg> orgs, List<TPayAmount> pay_info_list)
        {
            foreach (var org in orgs)
            {
                var pay_info = pay_info_list.Find(x => x.in_current_org == org.name);
                if (pay_info != null)
                {
                    //該單位 已產生繳費單款項
                    org.filed_amount = pay_info.total;
                    org.unpaid_amount = pay_info.unpaid;
                    org.paid_amount = pay_info.paid;
                }
                else
                {
                    org.filed_amount = 0;
                    org.unpaid_amount = 0;
                    org.paid_amount = 0;
                }

                //該單位 應付款項
                org.bill_amount = org.teams.Sum(x => x.bill_amount);

                //該單位 未產生繳費單款項
                org.wait_amount = org.bill_amount - org.filed_amount;

                if (org.wait_amount < 0)
                {
                    org.wait_amount = 0;
                }
            }
        }

        private Item GetPayList(TConfig cfg)
        {
            string sql = @"
                SELECT 
                    *
                FROM
                    IN_MEETING_PAY WITH(NOLOCK)
                WHERE
                    in_meeting = '{#meeting_id}'
                    AND in_creator_sno = '{#in_creator_sno}'
                    AND pay_bool IN (N'已繳費', N'未繳費', N'合併')
                ORDER BY
                    item_number
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_creator_sno}", cfg.in_creator_sno);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql); ;

            return items;
        }

        //該單位已產生繳費單款項
        private List<TPayAmount> OrgWaitPayAmount(TConfig cfg, Item items)
        {
            List<TPayAmount> result = new List<TPayAmount>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_current_org = item.getProperty("in_current_org", "").Trim();
                string pay_bool = item.getProperty("pay_bool", "");
                string in_pay_amount_exp = item.getProperty("in_pay_amount_exp", "0");
                int pay_amount = GetIntVal(in_pay_amount_exp);

                TPayAmount obj = result.Find(x => x.in_current_org == in_current_org);

                if (obj == null)
                {
                    obj = new TPayAmount
                    {
                        in_current_org = in_current_org,
                        unpaid = 0,
                        paid = 0,
                        total = 0,
                    };
                    result.Add(obj);
                }

                obj.total += pay_amount;

                switch (pay_bool)
                {
                    case "未繳費":
                    case "合併"://未繳費才可合併
                        obj.unpaid += pay_amount;
                        break;

                    case "已繳費":
                        obj.paid += pay_amount;
                        break;
                }
            }

            return result;
        }

        private List<TOrg> MUsersToOrgs(TConfig cfg)
        {
            List<TOrg> result = new List<TOrg>();

            string sql = "SELECT * FROM IN_MEETING_USER WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_creator_sno = '" + cfg.in_creator_sno + "'"
                + " ORDER BY in_l1, in_l2, in_l3, in_index";

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                TMUser muser = MapMUser(cfg, item);
                string key = muser.in_current_org;

                TOrg org = result.Find(x => x.name == key);
                if (org == null)
                {
                    org = MapOrg(cfg, muser);
                    result.Add(org);
                }

                //數量統計
                ResetPaySum(cfg, org, muser);

                if (muser.is_staff)
                {
                    continue;
                }

                //男女分隊統計
                TTeamSum gender_team = org.teams.Find(x => x.key == muser.gender_team_key);
                if (gender_team == null)
                {
                    gender_team = MapGenderTeam(cfg, org, muser);
                    org.teams.Add(gender_team);
                }

                gender_team.expense += muser.expense;
                gender_team.list.Add(item);

                if (gender_team.expense > cfg.course_fees)
                {
                    gender_team.bill_amount = cfg.course_fees;
                }
                else
                {
                    gender_team.bill_amount = gender_team.expense;
                }
            }

            return result;
        }

        private void ResetPaySum(TConfig cfg, TOrg org, TMUser muser)
        {
            var info = org.info;
            var list = org.nopay_item_list;

            if (muser.is_staff)
            {
                info.total_staff_count++;
            }
            else
            {
                info.total_player_count++;
            }

            //未產生繳費單
            if (!muser.has_paynumber)
            {
                if (muser.is_staff)
                {
                    info.nopay_staff_count++;
                }
                else
                {
                    info.nopay_player_count++;
                }

                if (!list.Contains(muser.item_key))
                {
                    list.Add(muser.item_key);
                    info.nopay_item_count++;
                }
            }
        }
        private TTeamSum MapGenderTeam(TConfig cfg, TOrg org, TMUser muser)
        {
            TTeamSum result = new TTeamSum
            {
                key = muser.gender_team_key,
                in_gender = muser.in_gender,
                in_team = muser.in_team,
                bill_amount = 0,
                expense = 0,
                list = new List<Item>(),
            };

            return result;
        }

        private TOrg MapOrg(TConfig cfg, TMUser muser)
        {
            TOrg result = new TOrg
            {
                name = muser.in_current_org,
                bill_amount = 0,
                teams = new List<TTeamSum>(),
                info = new TPaySum(),
                nopay_item_list = new List<string>(),
                in_creator = muser.in_creator,
            };

            return result;
        }

        private TMUser MapMUser(TConfig cfg, Item item)
        {
            TMUser result = new TMUser
            {
                id = item.getProperty("id", ""),
                in_gender = item.getProperty("in_gender", ""),
                in_creator = item.getProperty("in_creator", ""),
                in_creator_sno = item.getProperty("in_creator_sno", ""),
                in_current_org = item.getProperty("in_current_org", "").Trim(),
                in_l1 = item.getProperty("in_l1", ""),
                in_l2 = item.getProperty("in_l2", ""),
                in_l3 = item.getProperty("in_l3", ""),
                in_index = item.getProperty("in_index", ""),
                in_team = item.getProperty("in_team", ""),
                in_expense = item.getProperty("in_expense", "0"),
                in_paynumber = item.getProperty("in_paynumber", ""),
            };

            result.expense = GetIntVal(result.in_expense);
            result.is_staff = result.in_l1 == "隊職員";
            result.has_paynumber = result.in_paynumber != "";

            result.item_key = result.in_l1 + result.in_l2 + result.in_l3 + result.in_index;

            result.gender_team_key = result.in_gender + "子組-" + result.in_team + "隊";
            if (result.in_l2.Contains("青年"))
            {
                result.gender_team_key = "青年" + result.gender_team_key;
            }
            else if (result.in_l2.Contains("青少年"))
            {
                result.gender_team_key = "青少年" + result.gender_team_key;
            }
            return result;
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string in_creator_sno { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public string in_title { get; set; }
            public string in_url { get; set; }
            public string in_course_fees { get; set; }
            public int course_fees { get; set; }

            public Item itmLogin { get; set; }
            public bool isMeetingAdmin { get; set; }
        }

        private class TOrg
        {
            public string name { get; set; }
            public int bill_amount { get; set; }
            public int filed_amount { get; set; }
            public int wait_amount { get; set; }

            public int unpaid_amount { get; set; }
            public int paid_amount { get; set; }

            public List<TTeamSum> teams { get; set; }

            public TPaySum info { get; set; }
            public List<string> nopay_item_list { get; set; }
            public string in_creator { get; set; }
        }

        private class TTeamSum
        {
            public string key { get; set; }
            public string in_gender { get; set; }
            public string in_team { get; set; }
            public int expense { get; set; }
            public int bill_amount { get; set; }
            public List<Item> list { get; set; }
        }

        private class TMUser
        {
            public string id { get; set; }
            public string in_creator { get; set; }
            public string in_creator_sno { get; set; }
            public string in_current_org { get; set; }
            public string in_gender { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_index { get; set; }
            public string in_team { get; set; }
            public string in_expense { get; set; }
            public string in_paynumber { get; set; }

            public int expense { get; set; }
            public bool is_staff { get; set; }
            public bool has_paynumber { get; set; }
            public string item_key { get; set; }

            public string gender_team_key { get; set; }
        }

        private class TPaySum
        {
            public int total_player_count { get; set; }
            public int total_staff_count { get; set; }
            public int total_item_count { get; set; }

            public int nopay_player_count { get; set; }
            public int nopay_staff_count { get; set; }
            public int nopay_item_count { get; set; }
        }

        private class TPayAmount
        {
            public string in_current_org { get; set; }
            public int paid { get; set; }
            public int unpaid { get; set; }
            public int total { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string prop { get; set; }
            public Func<TConfig, TField, Item, string> getVal { get; set; }
            public bool is_child { get; set; }
            public bool is_money { get; set; }
            public bool is_refund { get; set; }
        }

        private int GetIntVal(string value, int def = 0)
        {
            if (value == "" || value == "0") return 0;

            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}