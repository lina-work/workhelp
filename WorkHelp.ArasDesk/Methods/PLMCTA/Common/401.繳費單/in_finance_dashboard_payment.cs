﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class in_finance_dashboard_payment : Item
    {
        public in_finance_dashboard_payment(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 繳費單查詢
                日期: 
                    - 2022-01-21 調整協助報名者欄位 (lina)
                    - 2022-01-06 增加退費狀態 (lina)
                    - 2021-02-19 創建 (Alan)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_finance_dashboard_payment";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            string scene = this.getProperty("scene", "");

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                mode = itmR.getProperty("mode", ""),
                member_type = itmR.getProperty("type", ""),
                meeting_id = itmR.getProperty("in_title", ""),
                in_type = itmR.getProperty("in_type", ""),
                title_name = itmR.getProperty("in_title_name", ""),
                pay_bool = itmR.getProperty("in_pay_bool", ""),
                filter = itmR.getProperty("in_filter", ""),
                date_type = itmR.getProperty("in_date_type", ""),
                date_s = itmR.getProperty("in_date_s", ""),
                date_e = itmR.getProperty("in_date_e", "")
            };

            //是否為比賽
            cfg.is_game = cfg.in_type == "game";

            //取得登入者資訊
            string sql = "SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = N'" + inn.getUserID() + "'";
            Item itmLoginResume = inn.applySQL(sql);
            if (IsError(itmLoginResume))
            {
                throw new Exception("登入者履歷異常");
            }
            cfg.in_is_admin = itmLoginResume.getProperty("in_is_admin", "");

            //要檢視的對象 Resume
            Item itmResumeView = GetTargetResume(CCO, strMethodName, inn, itmLoginResume, itmR);
            string view_resume_id = itmResumeView.getProperty("id", "");
            string view_resume_sno = itmResumeView.getProperty("in_sno", "");
            string view_member_type = itmResumeView.getProperty("in_member_type", "");

            //權限檢核
            string codeMethod = "ALL";
            string bodyMethod = "<method>" + strMethodName + "</method><code>" + codeMethod + "</code>";
            Item itmPermit = inn.applyMethod("In_CheckIdentity", bodyMethod);

            cfg.isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            cfg.isCommittee = itmPermit.getProperty("isCommittee", "") == "1";

            //財務單位檢核
            Item itmRoleResult = inn.applyMethod("In_Association_Role"
                    , "<in_sno>" + itmLoginResume.getProperty("in_sno", "") + "</in_sno>"
                    + "<idt_names>ACT_ASC_Accounting</idt_names>");

            bool is_finance = itmRoleResult.getProperty("inn_result", "") == "1";
            if (cfg.isMeetingAdmin || is_finance)
            {
                cfg.is_finance = true;
            }

            if (cfg.in_is_admin == "1")
            {
                cfg.isMeetingAdmin = true;
            }

            Item itmResumes = null;
            List<Item> itmMtTitles = null;
            List<Item> itmPayBools = null;

            if (cfg.isMeetingAdmin)
            {
                itmResumes = GetPaymentList(cfg, "", 0);
                itmMtTitles = GetTitleGroup(cfg, "", 0);
                itmPayBools = GetPayBoolGroup(cfg, "", 0);
            }
            else if (view_member_type == "area_cmt" || view_member_type == "prjt_cmt")
            {
                itmResumes = GetPaymentList(cfg, view_resume_id, 2);
                itmMtTitles = GetTitleGroup(cfg, view_resume_id, 2);
                itmPayBools = GetPayBoolGroup(cfg, view_resume_id, 2);
            }
            else
            {
                itmResumes = GetPaymentList(cfg, view_resume_sno, 2);
                itmMtTitles = GetTitleGroup(cfg, view_resume_sno, 2);
                itmPayBools = GetPayBoolGroup(cfg, view_resume_sno, 2);
            }


            if (cfg.mode == "xls")
            {
                Export(cfg, itmResumeView, itmResumes, itmR);
            }
            else
            {
                if (itmResumes == null || !IsError(itmResumes, isSingle: false))
                {
                    if (scene != "load")
                    {
                        Query(cfg, itmResumes, itmR);
                    }
                }

                //下拉清單 項目名稱
                AppendMenu(cfg, itmMtTitles, "inn_title", itmR, need_type: true);
                AppendMenu(cfg, itmPayBools, "inn_pay_bool", itmR);
            }

            return itmR;
        }

        //取得繳費單
        private Item GetPaymentList(TConfig cfg, string view_filter, int type)
        {
            string condition = "WHERE 1 = 1";

            if (type == 1)
            {
                condition += " AND t4.in_manager_org = N'" + view_filter + "'";
            }
            else if (type == 2)
            {
                condition += " AND t4.in_sno = N'" + view_filter + "'";
            }

            if (cfg.meeting_id != "All" && cfg.meeting_id != "")
            {
                condition += " AND (t2.id = '" + cfg.meeting_id + "' OR t3.id = '" + cfg.meeting_id + "')";
            }

            if (cfg.pay_bool != "All" && cfg.meeting_id != "")
            {
                if (cfg.pay_bool == "退費")
                {
                    condition += " AND ISNULL(T1.in_refund_amount, 0) > 0";
                }
                else
                {
                    condition += " AND IIF(in_collection_agency is not null and pay_bool = '未繳費','已繳費.', pay_bool) = N'" + cfg.pay_bool + "'";
                }
            }

            if (cfg.filter != "")
            {
                condition += "AND (T1.in_code_2 LIKE N'%" + cfg.filter + "%'"
                    + " OR T1.item_number       LIKE N'%" + cfg.filter + "%'"
                    + " OR in_pay_amount_exp    LIKE N'%" + cfg.filter + "%'"
                    + " OR in_pay_date_exp      LIKE N'%" + cfg.filter + "%'"
                    + " OR in_pay_date_exp1     LIKE N'%" + cfg.filter + "%'"
                    + " OR in_pay_date_real     LIKE N'%" + cfg.filter + "%'"
                    + " OR in_creator           LIKE N'%" + cfg.filter + "%'"
                    + " OR in_creator_sno       LIKE N'%" + cfg.filter + "%'"
                    + " OR in_credit_date       LIKE N'%" + cfg.filter + "%'"
                    + " OR IIF(in_collection_agency is not null AND pay_bool = '未繳費','已繳費.', pay_bool) LIKE N'%" + cfg.filter + "%'"
                    + " OR IIF(T1.in_meeting is null, T3.in_meeting_type, T2.in_meeting_type) LIKE N'%" + cfg.filter + "%'"
                    + " OR IIF(T1.in_meeting is null , t3.in_title, t2.in_title) LIKE N'%" + cfg.filter + "%'"
                    + " OR t4.in_current_org LIKE N'%" + cfg.filter + "%'"
                    + " OR t4.in_tel LIKE N'%" + cfg.filter + "%'"
                    + ")";
            }

            if (cfg.date_s != "" || cfg.date_e != "")
            {
                if (cfg.date_s != "" && cfg.date_e != "")
                {
                    condition += " AND CONVERT(varchar(100), DATEADD(hour, 8," + cfg.date_type + "), 23) BETWEEN '" + cfg.date_s + "' AND '" + cfg.date_e + "'";
                }
                else if (cfg.date_s != "")
                {
                    condition += " AND CONVERT(varchar(100), DATEADD(hour, 8," + cfg.date_type + "), 23) >= '" + cfg.date_s + "'";
                }
                else
                {
                    condition += " AND CONVERT(varchar(100), DATEADD(hour, 8," + cfg.date_type + "), 23) <= '" + cfg.date_e + "'";
                }
            }

            string sql = @"
                SELECT * FROM 
                (
                    SELECT 
                        T1.in_code_2
                        , T1.item_number
                        , T1.in_pay_amount_exp
                        , T1.in_pay_date_exp, in_pay_date_exp1
                        , T1.in_pay_date_real
                        , T1.in_refund_amount
                        , T1.in_creator_sno
                        , T1.in_creator
                        , T1.in_current_org AS 'register_org'
                        , IIF(in_collection_agency is not null AND pay_bool = '未繳費', '已繳費.', pay_bool) AS pay_bool
                        , IIF(T1.in_meeting is null ,T3.in_meeting_type,T2.in_meeting_type) AS meeting_type
                        , IIF(T1.in_meeting is null , t3.in_title, t2.in_title) AS in_title
                        , T4.in_current_org
                        , T4.in_manager_org
                        , T4.in_sno
                        , IIF(T1.in_meeting is null , t3.id, t2.id) AS meeting_id
                        , T4.in_tel
                        , T4.in_member_type
                        , IIF(T1.in_meeting is null ,'cla', 'meeting') AS mode
                        , T11.in_name AS 'register_name'
                    FROM 
                        IN_MEETING_PAY AS T1 WITH (NOLOCK)
                    LEFT JOIN 
                        IN_MEETING AS T2 WITH (NOLOCK)
                        ON T2.id = T1.in_meeting
                    LEFT JOIN 
                        IN_CLA_MEETING AS T3 WITH (NOLOCK)
                        ON T3.id = T1.in_cla_meeting
                    LEFT JOIN 
                        IN_RESUME AS T4 WITH (NOLOCK)
                        ON T4.in_sno = T1.in_creator_sno
                    LEFT OUTER JOIN
					( 
						SELECT source_id, ROW_NUMBER() OVER (PARTITION BY source_id ORDER BY created_on) AS 'RN', in_name
						FROM IN_MEETING_NEWS WITH(NOLOCK)
					) T11 ON T11.source_id = T1.id AND T11.RN = 1
                    {#condition}
                ) AS LIST
                WHERE pay_bool != N'對帳失敗'
                ORDER BY item_number desc
            ";

            sql = sql.Replace("{#condition}", condition);

            if (cfg.mode == "xls")
            {
                sql = sql.Replace("ORDER BY ITEM_NUMBER", "ORDER BY in_title, ITEM_NUMBER");
            }

            // cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);

        }

        //取得項目名稱 Group
        private List<Item> GetTitleGroup(TConfig cfg, string view_filter, int type)
        {
            string condition = "";

            if (type == 1)
            {
                condition = " WHERE t3.in_manager_org = '" + view_filter + "'";
            }
            else if (type == 2)
            {
                condition = " WHERE t3.in_sno = '" + view_filter + "'";
            }

            string sql1 = "SELECT DISTINCT 'game'  AS 'type', t2.id AS 'value', t2.in_title AS 'label' FROM IN_MEETING_PAY t1 WITH(NOLOCK)"
                + " INNER JOIN IN_MEETING t2 WITH(NOLOCK) ON t2.id = t1.in_meeting"
                + " INNER JOIN IN_RESUME t3 WITH(NOLOCK) ON t3.in_sno = t1.in_creator_sno"
                + condition;

            string sql2 = "SELECT DISTINCT 'class' AS 'type', t2.id AS 'value', t2.in_title AS 'label' FROM IN_MEETING_PAY t1 WITH(NOLOCK)"
                + " INNER JOIN IN_CLA_MEETING t2 WITH(NOLOCK) ON t2.id = t1.in_cla_meeting"
                + " INNER JOIN IN_RESUME t3 WITH(NOLOCK) ON t3.in_sno = t1.in_creator_sno"
                + condition;


            string sql = sql1
                + "\r\n" + " UNION ALL "
                + "\r\n" + sql2
                + "\r\n" + " ORDER BY label";

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            var result = new List<Item>();

            int count = items.getItemCount();

            Item itmAll = cfg.inn.newItem();
            itmAll.setProperty("value", "All");
            itmAll.setProperty("label", "全部");
            result.Add(itmAll);

            for (int i = 0; i < count; i++)
            {
                result.Add(items.getItemByIndex(i));
            }

            return result;
        }

        private Item GetPayBool(TConfig cfg, string label, string value)
        {
            Item item = cfg.inn.newItem();
            item.setProperty("label", label);
            item.setProperty("value", value);
            return item;
        }

        //取得繳費狀態 Group
        private List<Item> GetPayBoolGroup(TConfig cfg, string view_filter, int type)
        {
            var result = new List<Item>();
            result.Add(GetPayBool(cfg, "全部", "All"));
            result.Add(GetPayBool(cfg, "已取消", "已取消"));
            result.Add(GetPayBool(cfg, "已繳費", "已繳費"));
            result.Add(GetPayBool(cfg, "未繳費", "未繳費"));
            result.Add(GetPayBool(cfg, "退費", "退費"));
            result.Add(GetPayBool(cfg, "合併", "合併"));
            return result;

            // string condition ="";

            // if (type == 1)
            // {
            //     condition = " WHERE t4.in_manager_org = N'" + view_filter +"'" ;
            // }
            // else if (type ==2)
            // {
            //     condition = " WHERE t4.in_sno = N'" + view_filter + "'" ;
            // }

            // string sql = @"
            // SELECT * FROM 
            // (
            //     SELECT '全部' AS label , 'All' AS value
            //     UNION ALL
            //     SELECT pay_bool AS label, pay_bool AS value FROM 
            //     (
            //         SELECT  IIF(in_collection_agency is not null and pay_bool='未繳費','已繳費.',pay_bool) AS pay_bool
            //         FROM in_meeting_pay AS T1 WITH (NOLOCK)
            //         LEFT JOIN in_meeting AS T2 WITH (NOLOCK)
            //         ON T1.in_meeting = T2.id
            //         LEFT JOIN in_cla_meeting AS T3 WITH (NOLOCK)
            //         ON T1.in_cla_meeting = T3.id
            //         LEFT JOIN IN_RESUME AS T4 WITH (NOLOCK)
            //         ON t1.IN_CREATOR_SNO = T4.in_sno
            //         {#condition}
            //     ) AS LIST
            //     GROUP BY pay_bool
            // ) AS GList
            // WHERE label !='對帳失敗'
            // ORDER BY (
            //     CASE 
            //     WHEN label='全部' THEN -1
            //     WHEN NOT(label='全部') THEN 0
            //     END
            // ), label
            // ";

            // sql = sql.Replace("{#condition}", condition);

            // cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            // return cfg.inn.applySQL(sql);

        }

        //繳費單清單
        private void Query(TConfig cfg, Item items, Item itmReturn)
        {
            string item_numbers = "";
            string modes = "";
            string meeting_id = "";

            string head_creator = "協助報名者<BR>/報名者";
            if (cfg.is_game)
            {
                head_creator = "協助報名者<BR>/所屬單位";
            }

            Dictionary<string, string> modeList = new Dictionary<string, string>();

            List<TField> fields = new List<TField>();
            // fields.Add(new TField { property = "meeting_type", title = "類別", css = "text-center"});
            fields.Add(new TField { property = "item_number", title = "繳費單號", css = "text-center" });
            fields.Add(new TField { property = "in_title", title = "項目名稱", css = "text-left" });
            fields.Add(new TField { property = "in_creator", title = head_creator, css = "text-center" });
            // fields.Add(new TField { property = "in_tel", title = "聯絡方式", format = "tel" });
            fields.Add(new TField { property = "in_code_2", title = "條碼2", format = "text-center" });
            fields.Add(new TField { property = "pay_bool", title = "繳費狀態", css = "text-center" });
            fields.Add(new TField { property = "in_pay_amount_exp", title = "金額", css = "text-right" });

            fields.Add(new TField { property = "in_refund_amount", title = "退款金額", css = "text-right", is_refund = true });

            fields.Add(new TField { property = "in_pay_date_exp", title = "應繳日期<BR>實繳日期", css = "text-center" });
            // fields.Add(new TField { property = "in_pay_date_exp1", title = "最後<BR>收費日期", css = "text-center" });
            // fields.Add(new TField { property = "in_pay_date_real", title = "實繳日期", css = "text-center" });
            fields.Add(new TField { property = "in_credit_date", title = "代收機構<BR>入帳日", css = "text-center" });
            fields.Add(new TField { property = "in_receipt", title = "下載<BR>收據", css = "text-center" });

            fields.Add(new TField { property = "in_member_type", title = "類別", css = "text-center", hide = true });
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            foreach (var field in fields)
            {
                if (field.hide)
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true' data-visible='false'>" + field.title + "</th>");
                }
                else
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true'>" + field.title + "</th>");
                }
            }
            head.AppendLine("</thead>");

            int count = items.getItemCount(); ;

            string hide_print = "1";
            if (cfg.is_finance) hide_print = "0";

            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string pay_bool = GetPayBoolLink(item);
                string item_number = item.getProperty("item_number", "");
                meeting_id = item.getProperty("meeting_id", "");
                string mode = item.getProperty("mode", "");

                item.setProperty("pay_bool", pay_bool);

                if (!modeList.ContainsKey(mode))
                {
                    modeList.Add(mode, meeting_id);
                }

                if (pay_bool.Contains("已繳費"))
                {
                    string btn1 = " ";

                    if (cfg.is_finance)
                    {
                        btn1 = "<button class='btn btn-success pull-right radio_receipt_download' style='margin-right: 10px'"
                            + " onclick=\"receipt_download('" + item_number + "','" + meeting_id + "','" + mode + "')\">"
                            + "<i class='fa fa-download'></i></button>";
                    }

                    string btn2 = "<button class='btn btn-success pull-right radio_receipt_download' style='margin-right: 10px'"
                        + " onclick=\"Invoice_Click('" + item_number + "','" + meeting_id + "','" + mode + "', '" + hide_print + "')\">"
                        + "<i class='fa fa-edit'></i></button>";

                    item.setProperty("in_receipt", btn1 + "&nbsp;" + btn2);
                    item_numbers += item_number + ",";
                }

                string value = "";
                string phone = "";

                string in_pay_date_exp = item.getProperty("in_pay_date_exp", "");
                in_pay_date_exp = GetDateTimeValue(in_pay_date_exp, "yyyy/MM/dd", 8);

                string in_pay_date_real = item.getProperty("in_pay_date_real", "");
                in_pay_date_real = GetDateTimeValue(in_pay_date_real, "yyyy/MM/dd", 8);

                item.setProperty("in_pay_date_exp", in_pay_date_exp + "<BR>" + in_pay_date_real);
                //item.setProperty("in_pay_date_real", in_pay_date_real);

                value = item.getProperty("in_pay_date_exp1", "");
                item.setProperty("in_pay_date_exp1", GetDateTimeValue(value, "yyyy/MM/dd", 8));


                value = item.getProperty("in_credit_date", "");
                item.setProperty("in_credit_date", GetDateTimeValue(value, "yyyy/MM/dd", 8));
                value = item.getProperty("in_pay_amount_exp", "");
                item.setProperty("in_pay_amount_exp", GetIntStr(value));//Convert.ToInt16(value).ToString("N0"));

                item.setProperty("in_creator", CreatorInfo(cfg, item));

                value = item.getProperty("in_refund_amount", "");
                item.setProperty("in_refund_amount", GetIntStr(value));

                // phone ="聯絡方式：" +  item.getProperty("in_tel","");
                // item.setProperty("in_creator", "<span onclick='alertPhone(\"" + phone + "\")'>"+ value +"<i class='fa fa-phone-square'></i></span>" );

                body.AppendLine("<tr>");
                foreach (var field in fields)
                {
                    var v = field.GetStr(item);
                    var c = field.css;

                    if (field.is_refund && v != "0")
                    {
                        c += " bg-primary";
                    }

                    body.AppendLine("<td class='" + c + "' data-field='" + field.property + "' >" + v + "</td>");
                }
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");


            string table_name = "resume_table";

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            foreach (var g in modeList)
            {
                modes += g.Key;
                meeting_id = g.Value;
            }

            itmReturn.setProperty("inn_table", builder.ToString());
            itmReturn.setProperty("item_numbers", item_numbers);
            itmReturn.setProperty("modes", modes);
            itmReturn.setProperty("meeting_ids", meeting_id);
        }

        private string CreatorInfo(TConfig cfg, Item item, bool is_xls = false)
        {
            string result = "";
            string in_creator = item.getProperty("in_creator", "");
            string in_tel = item.getProperty("in_tel", "");
            string register_org = item.getProperty("register_org", "");
            string register_name = item.getProperty("register_name", "");

            string ln = is_xls ? System.Environment.NewLine : "<BR>";

            if (cfg.is_game)
            {
                result = in_creator + ln + in_tel + ln + "/" + register_org;
            }
            else
            {
                result = in_creator + ln + in_tel + ln + "/" + register_name;
            }

            return result;
        }

        private string GetIntStr(string value)
        {
            if (value == "" || value == "0") return "0";

            int result = 0;
            Int32.TryParse(value, out result);
            return result.ToString("N0");
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "' class='table table-hover table-bordered table-rwd rwd rwdtable' "
                + " data-toggle='table' "
                + " data-click-to-select='true' "
                + " data-striped='false' "
                 + " data-pagination='true' "
                 + " data-show-pagination-switch='false'"
                 + " data-page-size='25'"
                + ">";
        }


        //取得檢視對象 Resume
        private Item GetTargetResume(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmLoginResume, Item itmReturn)
        {
            itmLoginResume.setType("In_Resume");
            itmLoginResume.setProperty("target_resume_id", itmReturn.getProperty("id", ""));
            itmLoginResume.setProperty("login_resume_id", itmLoginResume.getID());
            itmLoginResume.setProperty("login_member_type", itmLoginResume.getProperty("in_member_type", ""));
            itmLoginResume.setProperty("login_is_admin", itmLoginResume.getProperty("in_is_admin", ""));
            return itmLoginResume.apply("In_Get_Target_Resume");
        }

        #region 匯出
        private void Export(TConfig cfg, Item itmResumeView, Item itmResumes, Item itmReturn)
        {
            string xls_title = "";

            if (cfg.pay_bool == "All")
            {
                cfg.pay_bool = "繳費清單";
            }
            else
            {
                cfg.pay_bool += "清單";
            }

            if (cfg.title_name == "全部")
            {
                xls_title = cfg.pay_bool;
            }
            else
            {
                xls_title = cfg.title_name + "_" + cfg.pay_bool;
            }

            ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook();
            string title_name = "";
            if (itmResumes == null || !IsError(itmResumes, isSingle: false))
            {

                if (cfg.title_name == "全部")
                {
                    for (int i = 0; i < itmResumes.getItemCount(); i++)
                    {
                        Item itmResume = itmResumes.getItemByIndex(i);
                        if (title_name != CheckStr(itmResume.getProperty("in_title", "")))
                        {
                            title_name = CheckStr(itmResume.getProperty("in_title", ""));
                            AppendSheet(cfg, workbook, title_name, itmResumes, 1);
                        }
                    }
                }
                else
                {
                    cfg.pay_bool = CheckStr(cfg.pay_bool);
                    AppendSheet(cfg, workbook, cfg.pay_bool, itmResumes, 0);
                }
            }

            Item itmXls = cfg.inn.applyMethod("In_Variable_File", "<in_name></in_name>");

            string export_path = itmXls.getProperty("export_path", "").TrimEnd('\\');
            string template_path = itmXls.getProperty("template_path", "");

            string ext_name = ".xlsx";
            string xls_name = xls_title + "_" + DateTime.Now.ToString("MMdd_HHmmss");
            string xls_file = export_path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveAs(xls_file);

            itmReturn.setProperty("xls_name", xls_url);
        }

        //附加道館社團 Sheet
        private void AppendSheet(TConfig cfg, ClosedXML.Excel.XLWorkbook workbook, string sheet_name, Item itmResumes, int type)
        {
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheets.Add(sheet_name);

            string head_creator = "協助報名者/報名者";
            if (cfg.is_game) head_creator = "協助報名者/所屬單位";

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "item_number", title = "繳費單號", format = "center" });
            fields.Add(new TField { property = "in_title", title = "項目名稱", format = "left" });
            fields.Add(new TField { property = "in_creator_sno", title = "會員編號", format = "center" });
            fields.Add(new TField { property = "in_creator2", title = head_creator, format = "center" });
            fields.Add(new TField { property = "in_tel", title = "聯絡方式", format = "tel" });
            fields.Add(new TField { property = "in_code_2", title = "條碼2", format = "text" });
            fields.Add(new TField { property = "pay_bool", title = "繳費狀態", format = "center" });
            fields.Add(new TField { property = "in_pay_amount_exp", title = "金額", format = "$ #,##0" });
            fields.Add(new TField { property = "in_refund_amount", title = "退款金額", format = "$ #,##0" });
            fields.Add(new TField { property = "in_pay_date_exp", title = "應繳日期", format = "yyyy/MM/dd" });
            // fields.Add(new TField { property = "in_pay_date_exp1", title = "最後收費日期", format = "yyyy/MM/dd"  });
            fields.Add(new TField { property = "in_pay_date_real", title = "實繳日期", format = "yyyy/MM/dd" });
            fields.Add(new TField { property = "in_credit_date", title = "代收機構入帳日", format = "yyyy/MM/dd" });
            int wsRow = 1;
            int wsCol = 1;
            int field_count = fields.Count;

            for (int i = 0; i < field_count; i++)
            {
                TField field = fields[i];
                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);
                SetHeadCell(cell, field.title);
            }
            wsRow++;

            int count = itmResumes.getItemCount();


            for (int i = 0; i < count; i++)
            {
                Item item = itmResumes.getItemByIndex(i);
                item.setProperty("in_creator2", CreatorInfo(cfg, item, is_xls: true));

                if (type == 1 && CheckStr(item.getProperty("in_title")) == sheet_name)
                {
                    SetItemCell(sheet, wsRow, wsCol, item, fields);
                    wsRow++;
                }
                else if (type == 0)
                {
                    SetItemCell(sheet, wsRow, wsCol, item, fields);
                    wsRow++;
                }

            }

            //自動調整欄寬
            sheet.Columns().AdjustToContents();
        }

        ClosedXML.Excel.XLColor head_bg_color = ClosedXML.Excel.XLColor.FromHtml("#295C90");

        //設定標題列
        private void SetHeadCell(ClosedXML.Excel.IXLCell cell, string title)
        {
            cell.Value = title;

            cell.Style.Alignment.Vertical = ClosedXML.Excel.XLAlignmentVerticalValues.Center;
            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell.Style.Font.Bold = true;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Fill.BackgroundColor = head_bg_color;
            cell.Style.Font.FontColor = ClosedXML.Excel.XLColor.White;

            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        //設定資料列
        private void SetBodyCell(ClosedXML.Excel.IXLCell cell, string value, string format)
        {
            switch (format)
            {
                case "center":
                    cell.Value = value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "yyyy/MM/dd":
                case "yyyy/MM/dd HH:mm":
                    cell.Value = GetDateTimeValue(value, format, 8);
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "$ #,##0":
                    cell.Value = value;
                    cell.Style.NumberFormat.Format = "$ #,##0";
                    break;

                case "tel":
                    if (value.StartsWith("09"))
                    {
                        cell.Value = value.Replace("-", "");
                        cell.Style.NumberFormat.Format = "0000-000-000";
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    else
                    {
                        cell.Value = "'" + value;
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    break;

                case "text":
                    cell.Value = "'" + value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                default:
                    cell.Value = value;
                    break;
            }

            //垂直置中
            cell.Style.Alignment.Vertical = ClosedXML.Excel.XLAlignmentVerticalValues.Center;

            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        //設定物件與資料列
        private void SetItemCell(ClosedXML.Excel.IXLWorksheet sheet, int wsRow, int wsCol, Item item, List<TField> fields)
        {
            int count = fields.Count;
            for (int i = 0; i < count; i++)
            {
                TField field = fields[i];

                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);

                if (field.property == "")
                {
                    SetBodyCell(cell, "", "");
                }
                else
                {
                    string value = item.getProperty(field.property, "");
                    SetBodyCell(cell, value, field.format);
                }
            }
        }
        #endregion 匯出

        private class TField
        {
            public string property { get; set; }
            public string title { get; set; }
            public string css { get; set; }
            public string format { get; set; }
            public bool hide { get; set; }

            public string GetStr(Item item)
            {
                return item.getProperty(this.property, "");
            }

            public bool is_refund { get; set; }
        }

        private string GetPayBoolLink(Item itmResume)
        {
            string meeting_type = itmResume.getProperty("meeting_type", "");
            string meeting_id = itmResume.getProperty("meeting_id", "");
            string item_number = itmResume.getProperty("item_number", "");
            string pay_bool = itmResume.getProperty("pay_bool", "");
            string mode = itmResume.getProperty("mode", "");

            if (pay_bool.Contains("已繳費"))
            {
                pay_bool = "<span style='color:green;'><i class='fa fa-list-alt'></i>" + pay_bool + "</span>";
            }
            else
            {
                pay_bool = "<span style='color:red;'><i class='fa fa-list-alt'></i>" + pay_bool + "</span>";
            }

            if (mode == "cla")
            {
                return "<a target='_blank' href='../pages/c.aspx?page=cla_detail_list_n1.html"
                    + "&method=In_Cla_Payment_DetailList&meeting_id=" + meeting_id
                    + "&item_number=" + item_number + "' >" + pay_bool + "</a>";
            }
            else
            {
                return "<a target='_blank' href='../pages/c.aspx?page=detail_list_n1.html"
                    + "&method=In_Payment_DetailList&meeting_id=" + meeting_id +
                    "&item_number=" + item_number + "' >" + pay_bool + "</a>";
            }
        }

        private string GetSidDisplay(Item itmResume)
        {
            return GetSidDisplay(itmResume.getProperty("in_sno", ""));
        }

        //生日(外顯)
        private string GetBirthDisplay(Item itmResume)
        {
            string value = itmResume.getProperty("in_birth", "");

            if (value.Contains("1900") || value.Contains("1899"))
            {
                return "";
            }

            return GetDateTimeValue(value, "yyyy年MM月dd日", 8);
        }

        //身分證字號(外顯)
        private string GetSidDisplay(string in_sno)
        {
            if (in_sno.Length == 10)
            {
                //10碼(身分證處理)
                string in_sno_1 = in_sno.Substring(0, 2);
                string in_sno_2 = in_sno.Substring(6, 4);
                return in_sno_1 + "****" + in_sno_2;//A1****6789
            }
            else
            {
                return "";
            }
        }

        private string GetDateTimeValue(string value, string format, int hours = 0)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }

        private bool IsError(Item item, bool isSingle = true)
        {
            if (item.isError() || item.getResult() == "")
            {
                return true;
            }
            else if (isSingle && item.getItemCount() != 1)
            {
                return true;
            }
            else if (item.getItemCount() <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //下拉選單
        private void AppendMenu(TConfig cfg, List<Item> items, string type_name, Item itmReturn, bool need_type = false)
        {
            if (items == null || items.Count <= 0)
            {
                return;
            }

            for (int i = 0; i < items.Count; i++)
            {
                Item item = items[i];

                item.setType(type_name);
                item.setProperty("inn_label", item.getProperty("label", ""));
                item.setProperty("inn_value", item.getProperty("value", ""));

                if (need_type)
                {
                    item.setProperty("inn_type", item.getProperty("type", ""));
                }

                itmReturn.addRelationship(item);
            }
        }

        //檢查是否有特殊字元
        private string CheckStr(string fileName)
        {
            string[] Special_text = new string[] { "?", "=", ".", "*", "[", "@", "#", "$", "%", "^", "&", ".", "+", "-", "]", " ", "/", @"\" }; //特殊字元

            if (!string.IsNullOrWhiteSpace(fileName))
            {
                foreach (string _text in Special_text)
                {
                    if (fileName.Contains(_text))
                    {
                        fileName = fileName.Replace(_text, "");
                    }
                }
            }

            if (fileName.Length > 31)
            {
                fileName = fileName.Substring(0, 31);
            }

            return fileName;
        }
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string mode { get; set; }
            public string member_type { get; set; }
            public string meeting_id { get; set; }
            public string in_type { get; set; }
            public string title_name { get; set; }
            public string pay_bool { get; set; }
            public string filter { get; set; }
            public string date_type { get; set; }
            public string date_s { get; set; }
            public string date_e { get; set; }

            public bool isMeetingAdmin { get; set; }
            public bool isCommittee { get; set; }

            public string in_is_admin { get; set; }
            public bool is_game { get; set; }

            public bool is_finance { get; set; }
        }
    }
}