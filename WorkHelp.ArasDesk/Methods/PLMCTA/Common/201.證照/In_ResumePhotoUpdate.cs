﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_ResumePhotoUpdate : Item
    {
        public In_ResumePhotoUpdate(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 更新上傳照片至In_Resume_Certificate
                日誌:
                    - 2022.04.27: IN_RESUME_CERTIFICATE 更新鍵值改為 source_id, in_photokey (lina)
            */

            System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_ResumePhotoUpdate";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "this dom: " + itmR.dom.InnerXml);

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                meeting_id = this.getProperty("meeting_id", ""),
            };

            string aml = "";
            string sql = "";
            string strError = "";


            string resumeID = itmR.getProperty("id", "");
            string muid = itmR.getProperty("muid", "");
            string mode = itmR.getProperty("mode", "");
            string type = itmR.getProperty("type", "");
            string in_ud_photo = itmR.getProperty("in_photo", "");
            string meeting_certificate = "";

            Item itmResume;
            string in_sno = "";
            string resume_sno = "";

            sql = @"SELECT * FROM IN_Resume WITH (NOLOCK) WHERE id = '" + resumeID + "'";
            itmResume = inn.applySQL(sql);
            resume_sno = itmResume.getProperty("in_sno", "");

            if (type == "Verifi")
            {
                in_sno = "_" + itmResume.getProperty("in_sno", "");
            }
            else
            {
                in_sno = "";
            }


            if (mode == "cla")
            {
                meeting_certificate = "IN_CLA_MEETING_CERTIFICATE";
            }
            else
            {
                meeting_certificate = "IN_MEETING_CERTIFICATE";
            }

            sql = @"
SELECT 
	  'in_photo' + in_type + ISNULL(in_country,'') + ISNULL(in_level,'')                               AS 'in_photokey'
	, 'in_photo' + in_type + ISNULL(in_country,'') + ISNULL(in_level,'') + ISNULL(sides,'')            AS 'id_photo'
	, ISNULL(t3.label_zt,'') 
		+ IIF (t4.label_zt IS NULL, '', IIF (t2.label_zt = '裁判證', t4.label_zt + '級', t4.label_zt )) 
		+ t2.label_zt  AS 'typename'
	, sort 
	, 'certificate_no' + in_type + ISNULL(in_country,'') + ISNULL(in_level,'') + ISNULL(sides,'')      AS 'id_no'
	, 'certificate_name' + in_type + ISNULL(in_country,'') + ISNULL(in_level,'') + ISNULL(sides,'')    AS 'id_name'
	, 'effective_s' + in_type + ISNULL(in_country,'') + ISNULL(in_level,'') + ISNULL(sides,'')         AS 'id_eff_s'
	, 'effective_e' + in_type + ISNULL(in_country,'') + ISNULL(in_level,'')  + ISNULL(sides,'')        AS 'id_eff_e'
	, 'certificate_option' + in_type + ISNULL(in_country,'') + ISNULL(in_level,'')  + ISNULL(sides,'') AS 'id_opt'
FROM {#meeting_certificate} AS T1 WITH (NOLOCK)
INNER JOIN VU_Mt_CertType         AS T2 ON T1.in_type = t2.VALUE
LEFT OUTER JOIN VU_Mt_CertCountry AS T3 ON T1.in_country = t3.VALUE
LEFT OUTER JOIN VU_Mt_CertLevel   AS T4 ON T1.in_level = t4.VALUE
LEFT OUTER JOIN 
    (
    	SELECT '_p01' AS sides , 1 as sort
    	UNION ALL
    	SELECT '_n01' AS sides , 2 as sort
    ) AS T5
    ON 1=1
WHERE source_id = '{#meeting_id}' AND is_enable = 1 and in_type != 0
ORDER BY in_type, in_country, in_level,sort
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                    .Replace("{#meeting_certificate}", meeting_certificate);


            if (mode == "All")
            {
                sql = @"
SELECT
	  'in_photo' + in_type + ISNULL(in_country,'') + ISNULL(in_level,'')                              AS 'in_photokey'
	, 'in_photo' + in_type + ISNULL(in_country,'') + ISNULL(in_level,'')  + ISNULL(sides,'')          AS 'id_photo'
	, ISNULL(t3.label_zt,'') 
		+ IIF (t4.label_zt IS NULL, '', IIF(t2.label_zt = '裁判證', t4.label_zt + '級', t4.label_zt )) 
		+ t2.label_zt AS 'typename'
	, sort 
	, 'certificate_no' + in_type + ISNULL(in_country,'') + ISNULL(in_level,'')  + ISNULL(sides,'')    AS 'id_no'
	, 'certificate_name' + in_type + ISNULL(in_country,'') + ISNULL(in_level,'')  + ISNULL(sides,'')  AS 'id_name'
	, 'effective_s' + in_type + ISNULL(in_country,'') + ISNULL(in_level,'')  + ISNULL(sides,'')       AS 'id_eff_s'
	, 'effective_e' + in_type + ISNULL(in_country,'') + ISNULL(in_level,'')  + ISNULL(sides,'')       AS 'id_eff_e'
	, 'certificate_option' + in_type + ISNULL(in_country,'') + ISNULL(in_level,'') + ISNULL(sides,'') AS 'id_opt'
FROM
(
	 SELECT in_type,IN_COUNTRY,in_level, ISNULL(is_hideinfo,1) AS is_hideinfo, IS_ENABLE FROM IN_MEETING_CERTIFICATE WITH(NOLOCK)
	 UNION ALL
	 SELECT in_type,IN_COUNTRY,in_level, ISNULL(is_hideinfo,1) AS is_hideinfo, IS_ENABLE FROM IN_CLA_MEETING_CERTIFICATE WITH(NOLOCK)
) AS T1
INNER JOIN VU_Mt_CertType         AS T2 ON T1.in_type = t2.VALUE
LEFT OUTER JOIN VU_Mt_CertCountry AS T3 ON T1.IN_COUNTRY = t3.VALUE
LEFT OUTER JOIN VU_Mt_CertLevel   AS T4 ON T1.in_level = t4.VALUE
LEFT OUTER JOIN 
    (
    	SELECT '_p01' AS sides , 1 as sort
    	UNION ALL
    	SELECT '_n01' AS sides , 2 as sort
    ) AS T5
    ON 1 = 1
WHERE is_enable = 1 AND in_type != 0
GROUP BY in_type, t2.label_zt , in_country, t3.label_zt , in_level, t4.label_zt , is_hideinfo, is_enable, T5.sides, T5.sort
ORDER BY in_type + ISNULL(t3.label_zt,'') + IIF (t4.label_zt IS NULL, '', IIF (t2.label_zt ='裁判證', t4.label_zt + '級', t4.label_zt )) + t2.label_zt, sort
                ";
            }

            Item itmCertificates = inn.applySQL(sql);

            if (itmCertificates.isError() || itmCertificates.getItemCount() <= 0)
            {
                // throw new Exception("系統找無相關證照設定上傳");
            }
            else
            {
                string in_photo = "";
                string id_photo = "";
                string typeName = "";
                string sort = "";
                string photoType = "";
                string photoKey = "";
                string id_photo2 = "";
                string in_photo2 = "";
                string id_no = "";
                string id_name = "";
                string id_eff_s = "";
                string id_eff_e = "";
                string id_opt = "";
                string in_no = "";
                string in_name = "";
                string in_eff_s = "";
                string in_eff_e = "";
                string in_opt = "";

                string amlStr = "";
                List<string> crt_upd_cols = new List<string>();
                List<string> rsm_upd_cols = new List<string>();
                
                bool isUpdate = false;

                for (int i = 0; i < itmCertificates.getItemCount(); i++)
                {
                    isUpdate = false;

                    amlStr = "";
                    crt_upd_cols = new List<string>();
                    rsm_upd_cols = new List<string>();

                    Item itmCertificate = itmCertificates.getItemByIndex(i);

                    id_photo = itmCertificate.getProperty("id_photo", "");               //對應欄位ID
                    id_photo = id_photo.ToLower();

                    string photo_property1 = id_photo + in_sno.ToLower();
                    string photo_property2 = id_photo + in_sno.ToUpper();
                    in_photo = this.getProperty(photo_property1, "");                  //取得上傳照片檔案ID
                    if (in_photo == "")
                    {
                        in_photo = this.getProperty(photo_property2, "");                  //取得上傳照片檔案ID
                    }

                    typeName = itmCertificate.getProperty("typename", "");               //證照種類
                    if(typeName.Contains("國際國際"))
                    {
                        typeName = typeName.Replace("國際國際", "國際");
                    }

                    sort = itmCertificate.getProperty("sort", "");
                    id_no = itmCertificate.getProperty("id_no", "").ToLower();
                    id_name = itmCertificate.getProperty("id_name", "").ToLower();
                    id_eff_s = itmCertificate.getProperty("id_eff_s", "").ToLower();
                    id_eff_e = itmCertificate.getProperty("id_eff_e", "").ToLower();
                    id_opt = itmCertificate.getProperty("id_opt", "").ToLower();


                    in_no = this.getProperty(id_no + in_sno, "");                     //證照編號
                    in_name = this.getProperty(id_name + in_sno, "");                   //證照名稱 

                    in_eff_s = GetDateTimeStr(this, id_eff_s + in_sno, "效期起");
                    in_eff_e = GetDateTimeStr(this, id_eff_e + in_sno, "效期迄");
                    
                    
                    in_opt = this.getProperty(id_opt + in_sno, "");
                    photoKey = itmCertificate.getProperty("in_photokey", "");            //對應KEY值
                    photoKey = photoKey.ToLower();

                    crt_upd_cols.Add("in_type = N'" + typeName + "'");
                    crt_upd_cols.Add("in_photokey = N'" + photoKey + "'");
                    amlStr += "<in_photokey>" + photoKey + "</in_photokey>";

                    if (in_no != "")
                    {
                        //回填證號至Resume
                        if (photoKey.Contains("6") && photoKey.Contains("gl"))
                        {
                            //國際段證
                            rsm_upd_cols.Add("in_gl_degree_id = N'" + in_no + "'");
                        }
                        else if (photoKey.Contains("6"))
                        {
                            //國內段證或段證
                            rsm_upd_cols.Add("in_degree_id = N'" + in_no + "'");
                        }

                        if (photoKey.Contains("7") && photoKey.Contains("gl"))
                        {
                            //國際裁判證
                            rsm_upd_cols.Add("in_gl_referee_id = N'" + in_no + "'");
                        }
                        else if (photoKey.Contains("7"))
                        {
                            //國內裁判證或裁判證
                            rsm_upd_cols.Add("in_referee_id = N'" + in_no + "'");
                        }

                        if (photoKey.Contains("8") && photoKey.Contains("gl"))
                        {
                            //國際教練證
                            rsm_upd_cols.Add("in_gl_instructor_id = N'" + in_no + "'");
                        }
                        else if (photoKey.Contains("8"))
                        {
                            //國內教練證或教練證
                            rsm_upd_cols.Add("in_instructor_id = N'" + in_no + "'");
                        }

                        crt_upd_cols.Add("in_certificate_no = N'" + in_no + "'");
                        amlStr += "<in_certificate_no>" + in_no + "</in_certificate_no>";
                        isUpdate = true;
                    }

                    if (in_name != "")
                    {
                        crt_upd_cols.Add("in_certificate_name = N'" + in_name + "'");
                        amlStr += "<in_certificate_name>" + in_name + "</in_certificate_name>";
                        isUpdate = true;
                    }

                    if (in_opt != "")
                    {
                        //級別須回填Resume
                        Item itmResumeInfo = inn.newItem("in_resume");
                        itmResumeInfo.setProperty("in_degree_label", in_opt);

                        Item itmDegreeRslt = itmResumeInfo.apply("In_Degree_Value");

                        string in_degree = itmDegreeRslt.getProperty("in_degree", "");

                        //回填級別至Resume
                        if (photoKey.Contains("6") && photoKey.Contains("gl"))
                        {
                            //國際級段證
                            rsm_upd_cols.Add("in_gl_degree = N'" + in_degree + "'");
                        }
                        else if (photoKey.Contains("6"))
                        {
                            //國內級段證或級段證
                            rsm_upd_cols.Add("in_degree = N'" + in_degree + "'");
                        }

                        if (photoKey.Contains("7") && photoKey.Contains("gl"))
                        {
                            //國際裁判證
                            rsm_upd_cols.Add("in_gl_referee_level = N'" + in_opt + "'");
                        }
                        else if (photoKey.Contains("7"))
                        {
                            //國內裁判證或裁判證
                            rsm_upd_cols.Add("in_referee_level = N'" + in_opt + "'");
                        }

                        if (photoKey.Contains("8") && photoKey.Contains("gl"))
                        {
                            //國際教練證
                            rsm_upd_cols.Add("in_gl_instructor_level = N'" + in_opt + "'");
                        }
                        else if (photoKey.Contains("8"))
                        {
                            //國內教練證或教練證
                            rsm_upd_cols.Add("in_instructor_level = N'" + in_opt + "'");
                        }

                        crt_upd_cols.Add("in_certificate_level = N'" + in_opt + "'");
                        amlStr += "<in_certificate_level>" + in_opt + "</in_certificate_level>";
                        isUpdate = true;
                    }

                    if (in_eff_s != "")
                    {
                        crt_upd_cols.Add("in_effective_start = '" + insDateTimeValue(in_eff_s) + "'");
                        amlStr += "<in_effective_start>" + insDateTimeValue(in_eff_s, "yyyy-MM-ddTHH:mm:ss", false) + "</in_effective_start>";
                        isUpdate = true;
                    }

                    if (in_eff_e != "")
                    {
                        crt_upd_cols.Add("in_effective_end = '" + insDateTimeValue(in_eff_e) + "'");
                        amlStr += "<in_effective_end>" + insDateTimeValue(in_eff_e, "yyyy-MM-ddTHH:mm:ss", false) + "</in_effective_end>";
                        isUpdate = true;
                    }


                    if (sort == "1") //該證照是否為正反面
                    {
                        Item itmCertificate2 = itmCertificates.getItemByIndex(i + 1);
                        id_photo2 = itmCertificate2.getProperty("id_photo", "");     //對應欄位ID
                        id_photo2 = id_photo2.ToLower();
                        in_photo2 = this.getProperty(id_photo2 + in_sno, "");        //取得上傳照片檔案ID
                        photoType = id_photo + "," + id_photo2;

                        crt_upd_cols.Add("in_phototype = N'" + photoType + "'");
                       
                        if (in_photo != "" && in_photo2 != "")
                        {
                            crt_upd_cols.Add("in_file1 = '" + in_photo + "'");
                            crt_upd_cols.Add("in_file2 = '" + in_photo2 + "'");
                            amlStr += "<in_file1>" + in_photo + "</in_file1><in_file2>" + in_photo2 + "</in_file2>";
                            isUpdate = true;
                        }
                        else if (in_photo != "")
                        {
                            crt_upd_cols.Add("in_file1 = '" + in_photo + "'");
                            amlStr += "<in_file1>" + in_photo + "</in_file1>";
                            isUpdate = true;
                        }
                        else if (in_photo2 != "")
                        {
                            crt_upd_cols.Add("in_file2 = '" + in_photo2 + "'");
                            amlStr += "<in_file2>" + in_photo2 + "</in_file2>";
                            isUpdate = true;
                        }

                        i++;
                    }
                    else
                    {

                        if (in_photo != "")
                        {
                            photoType = id_photo;
                            crt_upd_cols.Add("in_phototype = '" + photoType + "'");
                            crt_upd_cols.Add("in_file1 = '" + in_photo + "'");
                            amlStr += "<in_file1>" + in_photo + "</in_file1>";
                            isUpdate = true;
                        }
                    }

                    if (isUpdate || in_ud_photo != "")
                    {
                        updateResumePhoto(cfg, resumeID, typeName, photoType, photoKey, crt_upd_cols, amlStr);
                        //lina 2021.03.05 更新證照資料，將審核狀態變更為未審核
                        updateMUserVerify(cfg, muid, resume_sno, mode);
                        //將證照編號與級別回填至Resume
                        updateResumeInfo(cfg, rsm_upd_cols, resume_sno, resumeID);
                    }
                }
            }


            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;

        }

        private void updateResumePhoto(TConfig cfg
            , string resumeID
            , string typeName
            , string photoType
            , string photoKey
            , List<string> cols
            , string amlStr)
        {
            string sql = "";
            string aml = "";

            //判斷是否存在(存在更新 不存在新增)
            sql = "SELECT id FROM IN_RESUME_CERTIFICATE WITH(NOLOCK)"
                + " WHERE source_id = '{#resumeID}'"
                + " AND in_photokey =  '{#in_photokey}'";

            sql = sql.Replace("{#resumeID}", resumeID)
                    .Replace("{#in_photokey}", photoKey);

            Item itmSql = cfg.inn.applySQL(sql);

            if (itmSql.getResult() == "")
            {
                //新增
                aml = "<AML><Item type='In_Resume_Certificate' action='add'>" +
                    "<source_id>" + resumeID + "</source_id>" +
                    "<in_type>" + typeName + "</in_type>" +
                    amlStr +
                    "<in_phototype>" + photoType + "</in_phototype>" +
                    "</Item></AML>";

                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "updateResumePhoto _# aml: " + aml);
                
                cfg.inn.applyAML(aml);
            }
            else
            {
                //更新
                sql = "UPDATE IN_RESUME_CERTIFICATE SET {#sqlStr} WHERE source_id = '{#resumeID}' AND in_photokey =  '{#in_photokey}'";

                sql = sql.Replace("{#sqlStr}", string.Join(", ", cols))
                    .Replace("{#resumeID}", resumeID)
                    .Replace("{#in_photokey}", photoKey);

                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "updateResumePhoto _# sql: " + sql);

                cfg.inn.applySQL(sql);
            }
        }

        private void updateResumeInfo(TConfig cfg, List<string> cols, string resume_sno, string resume_id)
        {
            if (cols.Count <= 0) return;

            string sql = "UPDATE IN_RESUME SET " + string.Join(", ", cols) + " WHERE id = '" + resume_id + "'";
            
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        //將審核狀態變更為未審核
        private void updateMUserVerify(TConfig cfg, string muid, string resume_sno, string mode)
        {
            string typName = mode == "cla"
                ? "IN_CLA_MEETING_USER"
                : "IN_MEETING_USER";

            Item itmOlds = null;
            if (string.IsNullOrWhiteSpace(muid))
            {
                itmOlds = cfg.inn.newItem(typName, "get");
                itmOlds.setProperty("source_id", cfg.meeting_id);
                itmOlds.setProperty("in_sno", resume_sno);
                itmOlds = itmOlds.apply();
            }
            else
            {
                itmOlds = cfg.inn.newItem(typName, "get");
                itmOlds.setProperty("id", muid);
                itmOlds = itmOlds.apply();
            }

            if (itmOlds == null || itmOlds.isError() || itmOlds.getResult() == "")
            {
                //查無與會者資料，不處理
                return;
            }

            int count = itmOlds.getItemCount();
            for(int i = 0; i < count; i++)
            {
                Item itmOld = itmOlds.getItemByIndex(i);
                updateMUserVerify(cfg, typName, itmOld);
            }
        }

        private void updateMUserVerify(TConfig cfg, string typName, Item itmOld)
        {
            string muid = itmOld.getProperty("id", "");
            string in_verify_time = itmOld.getProperty("in_verify_time", "");
            string in_ass_ver_time = itmOld.getProperty("in_ass_ver_time", "");

            Item itmMUser = cfg.inn.newItem(typName, "merge");
            itmMUser.setAttribute("where", "id = '" + muid + "'");

            if (in_ass_ver_time != "")
            {
                //協會已審過，by pass 委員會審核
                if (in_verify_time == "")
                {
                    itmMUser.setProperty("in_verify_result", "");
                }
                else
                {
                    itmMUser.setProperty("in_verify_result", "1");
                }
                itmMUser.setProperty("in_ass_ver_result", "");
            }
            else
            {
                itmMUser.setProperty("in_verify_result", "");
                itmMUser.setProperty("in_ass_ver_result", "");
            }

            itmMUser = itmMUser.apply();
        }

        private string GetDateTimeStr(Item item, string property, string title)
        {
            string result = item.getProperty(property, "");
            if (result != "")
            {
                result = Convert.ToDateTime(result).ToString("yyyy/MM/dd");
                if (Convert.ToInt32(Convert.ToDateTime(result).ToString("yyyy")) < 1900)
                {
                    throw new Exception(title + "_請輸入西元日期(例:1990/01/01)");
                }
            }
            return result;
        }

        private string insDateTimeValue(string value, string format = "yyyy-MM-ddTHH:mm:ss", bool bAdd8Hour = true)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value, out dt))
            {
                if (bAdd8Hour)
                {
                    return dt.AddHours(-8).ToString(format);
                }
                else
                {
                    return dt.ToString(format);
                }
            }
            else
            {
                return value;
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
        }
    }
}