﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_Meeting_CertDownload : Item
    {
        public In_Meeting_CertDownload(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 
                    - 證照下載(賽事講習共用)
                輸出: 
                    zip
                重點: 
                    - 需 DotNetZip.dll
                    - 先清空下載路徑下的檔案，再從 Aras Vault 複製
                日期: 
                    - 2021/04/12: 加上委員會資料夾條件 (lina)
                    - 2021/01/22: 加上證照過濾條件 (lina)
                    - 2021/01/21: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Meeting_CertDownload";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                inn = inn,
                aras_vault_url = @"C:\Aras\Vault\" + strDatabaseName,

                scene = itmR.getProperty("scene", ""),
                mode = itmR.getProperty("mode", ""),
                meeting_id = itmR.getProperty("meeting_id", ""),
                inn_committee = itmR.getProperty("inn_committee", ""),
                in_type = itmR.getProperty("in_type", ""),
                in_sub = itmR.getProperty("in_sub", ""),
                in_value = itmR.getProperty("in_value", ""),
                in_key = itmR.getProperty("in_key", ""),
                MeetingName = "IN_MEETING",
                MUserName = "IN_MEETING_USER",
                MCertName = "IN_MEETING_CERTIFICATE",
                MRListName = "IN_MEETING_RESUMELIST",
            };

            cfg.need_cmt_folder = cfg.inn_committee == "1";

            if (cfg.mode == "cla")
            {
                cfg.MeetingName = "IN_CLA_MEETING";
                cfg.MUserName = "IN_CLA_MEETING_USER";
                cfg.MCertName = "IN_CLA_MEETING_CERTIFICATE";
                cfg.MRListName = "IN_CLA_MEETING_RESUMELIST";
            }

            cfg.itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>ALL</code>");
            cfg.isMeetingAdmin = cfg.itmPermit.getProperty("isMeetingAdmin", "") == "1";

            //是否為共同講師
            bool open = GetUserResumeListStatus(cfg);
            if (open) cfg.isMeetingAdmin = true;

            if (!cfg.isMeetingAdmin)
            {
                throw new Exception("您無權限瀏覽此頁面");
            }



            cfg.itmMeeting = GetMeeting(cfg);
            cfg.meeting_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.clear_title = ClearName(cfg.meeting_title);

            switch (cfg.scene)
            {
                case "ui":
                    Query(cfg, itmR);
                    break;

                case "download":
                    Download(cfg, itmR);
                    break;
            }

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private void Download(TConfig cfg, Item itmReturn)
        {
            SetExportInfo(cfg);

            var itmMCerts = GetMCerts(cfg, cfg.in_type);
            var itmMUsers = GetMUers(cfg);

            var mcerts = MapMCerts(cfg, itmMCerts);
            var musers = MapMUsers(cfg, itmMUsers);

            if (mcerts.Count == 0 || musers.Count == 0)
            {
                throw new Exception("查無資料");
            }

            //Copy
            foreach (var mcert in mcerts)
            {
                foreach (var kv in musers)
                {
                    var muser = kv.Value;
                    if (muser.PhotoBoxes.ContainsKey(mcert.key))
                    {
                        ChekUser(cfg, mcert, muser, muser.PhotoBoxes[mcert.key]);
                    }
                }
            }

            //Zip
            string zip_url = cfg.in_type == "ALL"
                ? ZipAllPhotos(cfg, mcerts)
                : ZipPhotos(cfg, mcerts);

            itmReturn.setProperty("zip_url", zip_url);
        }

        //打包圖檔(全部)
        private string ZipAllPhotos(TConfig cfg, List<TMCert> mcerts)
        {
            string file = cfg.clear_title + "(ALL)" + ".zip";
            string zip_file = cfg.DownloadFold + "\\" + file;
            string zip_url = cfg.DownloadPath + "/" + file;
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, zip_file);

            //刪除既有檔案
            if (System.IO.File.Exists(zip_file))
            {
                System.IO.File.Delete(zip_file);
            }

            using (var zip = new Ionic.Zip.ZipFile(zip_file, System.Text.Encoding.UTF8))
            {
                zip.AddDirectory(cfg.DownloadMeetingFold);
                zip.Comment = "This zip was created at " + System.DateTime.Now.ToString("G");
                zip.Save();
            }

            return zip_url;
        }

        //打包圖檔(單種證照)
        private string ZipPhotos(TConfig cfg, List<TMCert> mcerts)
        {
            string file = "";
            string zip_file = "";
            string zip_url = "";

            //count = 1
            foreach (var mcert in mcerts)
            {
                file = cfg.clear_title + "_" + mcert.name + ".zip";
                zip_file = cfg.DownloadFold + "\\" + file;
                zip_url = cfg.DownloadPath + "/" + file;
                //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, zip_file);

                //刪除既有檔案
                if (System.IO.File.Exists(zip_file))
                {
                    System.IO.File.Delete(zip_file);
                }

                using (var zip = new Ionic.Zip.ZipFile(zip_file, System.Text.Encoding.UTF8))
                {
                    zip.AddDirectory(mcert.folder);
                    zip.Comment = "This zip was created at " + System.DateTime.Now.ToString("G");
                    zip.Save();
                }
            }

            return zip_url;
        }

        //檢查 MeetingUser 是否符合規則 (為了處理一堆選手上傳了教練的教練證)
        private void ChekUser(TConfig cfg, TMCert mcert, TMUser muser, TPhotoBox box)
        {
            Item itmMCert = mcert.Value;
            Item itmMUser = muser.Value;

            string in_property = itmMCert.getProperty("in_property", "");
            string in_value = itmMCert.getProperty("in_value", "");
            string in_operator = itmMCert.getProperty("in_operator", "");

            bool is_match = true;

            if (in_property != "")
            {
                string user_value = itmMUser.getProperty(in_property, "");

                switch (in_operator)
                {
                    case "eq": //等於
                        is_match = user_value == in_value;
                        break;

                    case "ne": //不等於
                        is_match = user_value != in_value;
                        break;

                    case "contains": //包含
                        is_match = user_value.Contains(in_value);
                        break;

                    default:
                        is_match = true;
                        break;
                }
            }

            if (is_match)
            {
                CopyPhoto(cfg, mcert, muser, box);
            }
        }

        //複製照片
        private void CopyPhoto(TConfig cfg, TMCert mcert, TMUser muser, TPhotoBox box)
        {
            try
            {
                string target_folder = mcert.folder;
                if (cfg.need_cmt_folder)
                {
                    //依【委員會區】分資料夾
                    string cmt_folder = muser.committee_sno + muser.committee_short_name;
                    target_folder = mcert.folder + "\\" + cmt_folder;
                    if (!System.IO.Directory.Exists(target_folder))
                    {
                        System.IO.Directory.CreateDirectory(target_folder);
                    }
                }

                if (!mcert.is_sides)
                {
                    var photo = box.Photos[0];
                    var destFileName = target_folder + "\\" + muser.filename + "." + photo.ext;

                    if (cfg.in_sub == "1")
                    {
                        var imgSrc = System.Drawing.Image.FromFile(photo.path);
                        //var imgNew = ResizeImg(imgSrc, 131, 175);
                        var imgNew = ResizeImg(imgSrc, 120, 160);
                        imgNew.Save(destFileName);

                    }
                    else
                    {
                        System.IO.File.Copy(photo.path, destFileName, overwrite: true);
                    }


                }
                else
                {
                    for (int i = 0; i < box.Photos.Count; i++)
                    {
                        var photo = box.Photos[i];
                        var destFileName = target_folder + "\\" + muser.filename + "-" + photo.no + "." + photo.ext;
                        System.IO.File.Copy(photo.path, destFileName, overwrite: true);
                    }
                }
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "CopyPhoto: " + ex.Message);
            }
        }

        static System.Drawing.Bitmap ResizeImg(System.Drawing.Image image, int width, int height)
        {
            var destRect = new System.Drawing.Rectangle(0, 0, width, height);
            var destImage = new System.Drawing.Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = System.Drawing.Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
                graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

                using (var wrapMode = new System.Drawing.Imaging.ImageAttributes())
                {
                    wrapMode.SetWrapMode(System.Drawing.Drawing2D.WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, System.Drawing.GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        private List<TMCert> MapMCerts(TConfig cfg, Item items)
        {
            List<TMCert> list = new List<TMCert>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_type = item.getProperty("in_type", "");
                string in_country = item.getProperty("in_country", "").ToUpper();
                string in_level = item.getProperty("in_level", "");
                string is_sides = item.getProperty("is_sides", "");
                string label = item.getProperty("label", "");

                string key = "in_photo" + in_type + in_country + in_level;
                key = key.ToLower();

                string name = GetCertName(label, in_country);

                TMCert entity = new TMCert
                {
                    key = key,
                    name = name,
                    is_sides = is_sides == "1",
                    folder = cfg.DownloadMeetingFold + "\\" + name,
                    Value = item,
                };

                //先移除資料夾與底下所有檔案
                if (System.IO.Directory.Exists(entity.folder))
                {
                    System.IO.Directory.Delete(entity.folder, true);
                }

                //再建立資料夾
                if (!System.IO.Directory.Exists(entity.folder))
                {
                    System.IO.Directory.CreateDirectory(entity.folder);
                }

                list.Add(entity);
            }

            return list;
        }

        private Dictionary<string, TMUser> MapMUsers(TConfig cfg, Item items)
        {
            Dictionary<string, TMUser> map = new Dictionary<string, TMUser>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_sno = item.getProperty("in_sno", "");
                string in_name = item.getProperty("in_name", "");
                string committee_sno = item.getProperty("committee_sno", "");
                string committee_short_name = item.getProperty("committee_short_name", "");

                string headshot_key = "in_photo0";
                string headshot_id = item.getProperty("headshot_id", "");
                string headshot_name = item.getProperty("headshot_name", "");

                string cert_key = item.getProperty("cert_key", "").ToLower();
                string cert_id = item.getProperty("cert_id", "");
                string cert_name = item.getProperty("cert_name", "");

                string cert_id_2 = item.getProperty("cert_id_2", "");
                string cert_name_2 = item.getProperty("cert_name_2", "");

                if (committee_short_name == "")
                {
                    committee_short_name = "無";
                }

                TMUser entity = null;
                if (map.ContainsKey(in_sno))
                {
                    entity = map[in_sno];
                }
                else
                {
                    entity = new TMUser
                    {
                        committee_sno = committee_sno,
                        committee_short_name = committee_short_name,
                        in_name = in_name,
                        in_sno = in_sno,
                        filename = in_name + in_sno,
                        PhotoBoxes = new Dictionary<string, TPhotoBox>(),
                        Value = item,
                    };
                    map.Add(in_sno, entity);
                }

                //大頭照
                if (headshot_id != "" && headshot_name != "")
                {
                    AppendBoxPhoto(cfg, entity.PhotoBoxes, headshot_key, headshot_id, headshot_name);
                }

                //file1
                if (cert_id != "" && cert_name != "")
                {
                    AppendBoxPhoto(cfg, entity.PhotoBoxes, cert_key, cert_id, cert_name, "1");
                }

                //file2
                if (cert_id_2 != "" && cert_name_2 != "")
                {
                    AppendBoxPhoto(cfg, entity.PhotoBoxes, cert_key, cert_id_2, cert_name_2, "2");
                }
            }

            return map;
        }

        private void AppendBoxPhoto(TConfig cfg, Dictionary<string, TPhotoBox> map, string key, string id, string name, string no = "")
        {
            TPhotoBox box = null;
            if (map.ContainsKey(key))
            {
                box = map[key];
            }
            else
            {
                box = new TPhotoBox { Photos = new List<TPhoto>() };
                map.Add(key, box);
            }

            TPhoto photo = NewPhoto(cfg, id, name, no);
            box.Photos.Add(photo);
        }

        private TPhoto NewPhoto(TConfig cfg, string fileid, string filename, string no)
        {
            TPhoto result = new TPhoto
            {
                id = fileid,
                name = filename,
                no = no,
            };

            //路徑切出來
            string id_1 = fileid.Substring(0, 1);
            string id_2 = fileid.Substring(1, 2);
            string id_3 = fileid.Substring(3, 29);
            string fold = cfg.aras_vault_url + @"\" + id_1 + @"\" + id_2 + @"\" + id_3;
            string path = cfg.aras_vault_url + @"\" + id_1 + @"\" + id_2 + @"\" + id_3 + @"\" + filename;
            string ext = filename.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries).Last();
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "headShot path: " + path);

            if (!System.IO.File.Exists(path))
            {
                result.valid = false;
                result.message = "檔案不存在";
            }
            else if (!IsImage(path))
            {
                result.valid = false;
                result.message = "非圖片";
            }
            else
            {
                result.valid = true;
                result.message = "";
                result.path = path;
                result.ext = ext;
            }
            return result;
        }

        //判斷檔案是否真為圖片
        private bool IsImage(string path)
        {
            bool result = false;
            try
            {
                using (var img = System.Drawing.Image.FromFile(path))
                {
                    result = true;
                }
            }
            catch (Exception e)
            {
            }
            return result;
        }

        private void Query(TConfig cfg, Item itmReturn)
        {
            CloneValue(itmReturn, cfg.itmMeeting, "id");
            CloneValue(itmReturn, cfg.itmMeeting, "in_url");
            CloneValue(itmReturn, cfg.itmMeeting, "in_title");
            ShortDate(itmReturn, cfg.itmMeeting, "in_date_s");
            ShortDate(itmReturn, cfg.itmMeeting, "in_date_e");

            Item itmMCerts = GetMCerts(cfg, "ALL");

            int count = itmMCerts.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmMCert = itmMCerts.getItemByIndex(i);

                itmMCert.setType("inn_cert");
                itmMCert.setProperty("inn_content", GetCertContent1(cfg, itmMCert));
                itmReturn.addRelationship(itmMCert);
            }

            Item itmHeadShot = cfg.inn.newItem("inn_cert");
            itmHeadShot.setProperty("label", "照片(大段證-自訂大小)");
            itmHeadShot.setProperty("in_type", "0");
            itmHeadShot.setProperty("in_sub", "1");
            itmHeadShot.setProperty("inn_content", GetCertContent2(cfg, itmHeadShot));
            itmReturn.addRelationship(itmHeadShot);
        }

        private string GetCertContent1(TConfig cfg, Item item)
        {
            string in_type = item.getProperty("in_type", "").ToUpper();
            string in_sub = "0";
            string in_country = item.getProperty("in_country", "").ToUpper();
            string label = item.getProperty("label", "");

            label = GetCertName(label, in_country);

            return "<li class='list-group-item'>"
                + label
                + "<a data-type='" + in_type + "'"
                + " data-sub='" + in_sub + "'"
                + " data-value='" + label + "'"
                + " onclick='Download_Click(this)'>"
                + " 打包下載"
                + "</a></li>";
        }

        private string GetCertContent2(TConfig cfg, Item item)
        {
            string in_type = item.getProperty("in_type", "").ToUpper();
            string in_sub = "0";
            string in_country = item.getProperty("in_country", "").ToUpper();
            string label = item.getProperty("label", "");

            label = GetCertName(label, in_country);

            return "<li class='list-group-item'>"
                + label
                + "<input id='big_degree_w' class='form-control' value='120' >"
                + " x "
                + "<input id='big_degree_h' class='form-control' value='160' >"
                + "</li>";
        }

        private void CloneValue(Item itmTarget, Item itmSource, string property)
        {
            itmTarget.setProperty(property, itmSource.getProperty(property, ""));
        }

        private void ShortDate(Item itmTarget, Item itmSource, string property)
        {
            string value = itmSource.getProperty(property, "");
            itmTarget.setProperty(property, GetDateTimeValue(value, "yyyy/MM/dd HH:mm", 8));
        }

        /// <summary>
        /// 取得賽事資料
        /// </summary>
        private Item GetMeeting(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                id
                    , in_title
                    , in_url
                    , in_date_s
                    , in_date_e
                    , in_meeting_type
                FROM 
	                {#MeetingName} WITH(NOLOCK) 
                WHERE 
	                id = '{#meeting_id}' 
            ";

            sql = sql.Replace("{#MeetingName}", cfg.MeetingName)
                .Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        //取得共同講師 (判斷現在的登入者是否為此課程的共同講師)
        private bool GetUserResumeListStatus(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t2.id
                FROM 
                    {#MRListName} t1 WITH(NOLOCK) 
                INNER JOIN 
                    IN_RESUME t2 WITH(NOLOCK) 
                    ON t2.id = t1.related_id    
                WHERE 
                    t1.source_id = '{#meeting_id}' 
                    AND t2.in_user_id = '{#in_user_id}';    
            ";

            sql = sql.Replace("{#MRListName}", cfg.MRListName)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_user_id}", cfg.strUserId);

            Item item = cfg.inn.applySQL(sql);

            if (item.getResult() != "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 取得證照設定
        /// </summary>
        private Item GetMCerts(TConfig cfg, string in_type)
        {
            string type_condition = in_type == "ALL"
                ? ""
                : "AND t1.in_type = '" + in_type + "'";

            string sql = @"
                SELECT
					t1.*
	                , t2.label
                FROM
	                {#MCertName} t1 WITH(NOLOCK)
                INNER JOIN
                (
	                SELECT
		                t12.value
		                , t12.label_zt AS 'label'
		                , t12.sort_order
	                FROM
	                    [LIST] t11 WITH(NOLOCK)
	                INNER JOIN
	                    [VALUE] t12 WITH(NOLOCK)
	                ON
		                t12.source_id = t11.id
	                WHERE
		                t11.name = N'In_Meeting_CertificateType'
                ) t2
	                ON t2.value = t1.in_type
                WHERE
	                t1.source_id = '{#meeting_id}'
                    AND t1.is_enable = 1
                    {#type_condition}
                ORDER BY
	                t2.sort_order
            ";

            sql = sql.Replace("{#MCertName}", cfg.MCertName)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#type_condition}", type_condition);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得與會者
        /// </summary>
        private Item GetMUers(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t1.in_sno
	                , t1.in_name
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_photo1     AS 'headshot_id'
	                , t3.filename      AS 'headshot_name'
	                , t4.in_photokey   AS 'cert_key'
	                , t4.in_phototype  AS 'cert_type'
	                , t4.in_file1      AS 'cert_id'
	                , t5.filename      AS 'cert_name'
	                , t4.in_file2      AS 'cert_id_2'
	                , t6.filename      AS 'cert_name_2'
	                , t11.in_sno       AS 'committee_sno'
	                , t11.in_short_org AS 'committee_short_name'
                FROM
	                {#MUserName} t1 WITH(NOLOCK)
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.in_sno = t1.in_sno
                LEFT OUTER JOIN
	                [FILE] t3 WITH(NOLOCK)
	                ON t3.id = t1.in_photo1
                INNER JOIN
	                IN_RESUME_CERTIFICATE t4 WITH(NOLOCK)
	                ON t4.source_id = t2.id
                LEFT OUTER JOIN
	                [FILE] t5 WITH(NOLOCK)
	                ON t5.id = t4.in_file1
                LEFT OUTER JOIN
	                [FILE] t6 WITH(NOLOCK)
	                ON t6.id = t4.in_file2
                LEFT OUTER JOIN 
                    IN_RESUME t11 WITH(NOLOCK)
                    ON t11.in_name = t1.in_committee AND t11.in_member_type = 'area_cmt' AND t11.in_member_role = 'sys_9999'
                WHERE
	                t1.source_id = '{#meeting_id}'
                ORDER BY
	                t1.in_sno
	                , t4.in_phototype
            ";

            sql = sql.Replace("{#MUserName}", cfg.MUserName)
                .Replace("{#meeting_id}", cfg.meeting_id);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        #region 通用函式


        /// <summary>
        /// 匯出設定資訊
        /// </summary>
        private void SetExportInfo(TConfig cfg)
        {
            string sql = @"
                SELECT 
                    t1.in_name
                    , t1.in_value 
                FROM 
                    In_Variable t1 WITH(NOLOCK)
                WHERE
					t1.in_name = N'meeting_photo'
            ";

            Item itmPath = cfg.inn.applySQL(sql);

            //C:\site\cta_test\tempvault\meeting_photo
            string export_fold = itmPath.getProperty("in_value", "").TrimEnd('\\');

            // /tempvault/meeting_photo
            int pos = export_fold.IndexOf("tempvault");
            int len = export_fold.Length - pos;
            string export_path = export_fold.Substring(pos, len).Replace("\\", "/");

            cfg.DownloadFold = export_fold;
            cfg.DownloadPath = export_path;
            //C:\site\cta_test\tempvault\meeting_photo\7E8421A74A204F6A8AE0D007FBE8BB18
            cfg.DownloadMeetingFold = export_fold + "\\" + cfg.meeting_id;
            // /tempvault/meeting_photo/7E8421A74A204F6A8AE0D007FBE8BB18
            cfg.DownloadMeetingPath = export_path + "/" + cfg.meeting_id;

            if (!System.IO.Directory.Exists(cfg.DownloadMeetingFold))
            {
                System.IO.Directory.CreateDirectory(cfg.DownloadMeetingFold);
            }
        }

        #endregion 通用函式

        #region 資料模型

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            /// <summary>
            /// Aras Vault Site 絕對路徑
            /// </summary>
            public string aras_vault_url { get; set; }

            //外部輸入區

            /// <summary>
            /// cla 講習
            /// </summary>
            public string mode { get; set; }

            /// <summary>
            /// 場景
            /// </summary>
            public string scene { get; set; }

            /// <summary>
            /// 活動 id
            /// </summary>
            public string meeting_id { get; set; }

            /// <summary>
            /// 是否依【委員會區】分資料夾 (1: 是)
            /// </summary>
            public string inn_committee { get; set; }

            /// <summary>
            /// 是否依【委員會區】分資料夾 (1: 是)
            /// </summary>
            public bool need_cmt_folder { get; set; }

            /// <summary>
            /// 證照類型 (ALL: 全打包)
            /// </summary>
            public string in_type { get; set; }

            /// <summary>
            /// 證照類型名稱
            /// </summary>
            public string in_value { get; set; }

            /// <summary>
            /// 證照鍵值
            /// </summary>
            public string in_key { get; set; }


            /// <summary>
            /// Meeting ItemType
            /// </summary>
            public string MeetingName { get; set; }

            /// <summary>
            /// MeetingUser ItemType
            /// </summary>
            public string MUserName { get; set; }

            /// <summary>
            /// IN_MEETING_CERTIFICATE ItemType
            /// </summary>
            public string MCertName { get; set; }

            /// <summary>
            /// IN_MEETING_RESUMELIST ItemType
            /// </summary>
            public string MRListName { get; set; }


            //權限區
            public Item itmPermit { get; set; }
            public bool isMeetingAdmin { get; set; }


            /// <summary>
            /// 活動名稱
            /// </summary>
            public string meeting_title { get; set; }

            /// <summary>
            /// 活動名稱(過濾掉特出字元)
            /// </summary>
            public string clear_title { get; set; }

            /// <summary>
            /// 下載資料的所在資料夾 (絕對路徑)
            /// </summary>
            public string DownloadFold { get; set; }

            /// <summary>
            /// 下載資料的所在資料夾 (相對路徑)
            /// </summary>
            public string DownloadPath { get; set; }

            /// <summary>
            /// 下載資料的所在資料夾 (絕對路徑)
            /// </summary>
            public string DownloadMeetingFold { get; set; }

            /// <summary>
            /// 下載資料的所在資料夾 (相對路徑)
            /// </summary>
            public string DownloadMeetingPath { get; set; }

            /// <summary>
            /// 下載資料的檔案名稱
            /// </summary>
            public string DownloadName { get; set; }

            public Item itmMeeting { get; set; }

            public string in_sub { get; set; }
        }

        private class TMUser
        {
            /// <summary>
            /// 委員會帳號
            /// </summary>
            public string committee_sno { get; set; }
            /// <summary>
            /// 委員會
            /// </summary>
            public string committee_short_name { get; set; }
            /// <summary>
            /// 姓名
            /// </summary>
            public string in_name { get; set; }
            /// <summary>
            /// 身分證號
            /// </summary>
            public string in_sno { get; set; }
            /// <summary>
            /// 姓名身分證號
            /// </summary>
            public string filename { get; set; }
            /// <summary>
            /// 各種照片箱 (一種箱子目前最多三張照片)
            /// </summary>
            public Dictionary<string, TPhotoBox> PhotoBoxes { get; set; }

            public Item Value { get; set; }
        }

        /// <summary>
        /// 照片箱
        /// </summary>
        private class TPhotoBox
        {
            public List<TPhoto> Photos { get; set; }
        }

        /// <summary>
        /// 照片
        /// </summary>
        private class TPhoto
        {
            public string id { get; set; }
            public string name { get; set; }
            public string no { get; set; }
            public string path { get; set; }
            public string ext { get; set; }
            public bool valid { get; set; }
            public string message { get; set; }
        }

        /// <summary>
        /// 證照
        /// </summary>
        private class TMCert
        {
            public string key { get; set; }
            public string name { get; set; }
            public string folder { get; set; }
            public bool is_sides { get; set; }
            public Item Value { get; set; }
        }

        #endregion 資料模型


        private string ClearName(string value)
        {
            var builder = new StringBuilder(value);
            var nc = "_";
            var arr = new string[] { "\\", "/", ":", "*", "?", "\"", "<", ">", "|" };

            foreach (var c in arr)
            {
                builder.Replace(c, nc);
            }
            return builder.ToString();
        }

        private string GetCertName(string label, string in_country)
        {
            string name = label;
            if (name == "大頭照") name = "照片";

            if (in_country == "TW")
            {
                name = "國內" + name;
            }
            else if (in_country == "GL")
            {
                name = "國際" + name;
            }
            return name;
        }

        private int GetIntValue(string value)
        {
            int result = 0;
            if (Int32.TryParse(value, out result))
            {
                return result;
            }
            else
            {
                return 0;
            }
        }

        private string GetDateTimeValue(string value, string format = "yyyy-MM-dd", int hours = 0)
        {
            if (value == "") return "";

            string day = value.Replace("/", "-");

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(day, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }
    }
}