﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_Meeting_SurveyAB : Item
    {
        public In_Meeting_SurveyAB(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: AB卷設定
                日誌: 
                    - 2022-07-15: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_SurveyAB";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                code = itmR.getProperty("code", ""),
                survey = itmR.getProperty("survey", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "run":
                    Run(cfg, itmR);
                    break;

                case "update":
                    Update(cfg, itmR);
                    break;

                default:
                    Page(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Update(TConfig cfg, Item itmReturn)
        {
            string nid = itmReturn.getProperty("nid", "");
            string newCode = itmReturn.getProperty("code", "");

            //Item itmOld = cfg.inn.applySQL("SELECT * FROM IN_CLA_MEETING_USER_SURVEY WITH(NOLOCK) WHERE id = '"+nid+"'");
            //if (itmMain.isError() || itmMain.getResult() == "")
            //{
            //    throw new Exception("查無題號資料");
            //}

            string sql = "SELECT * FROM IN_CLA_MEETING_USER_SURVEY WITH(NOLOCK) WHERE id = '" + nid + "'";
            Item itmOld = cfg.inn.applySQL(sql);
            if (itmOld.isError() || itmOld.getResult() == "")
            {
                throw new Exception("查無題號資料");
            }

            string survey = itmOld.getProperty("in_surveytype", "");

            sql = @"
                SELECT
                    *
                FROM
                    IN_CLA_MEETING_USER_SURVEY t1 WITH(NOLOCK)
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                    AND t1.in_surveytype = '{#in_surveytype}'
                    AND t1.in_type = '{#in_type}'
                    AND t1.in_code = '{#in_code}'
                ORDER BY
                    t1.in_code
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_surveytype}", survey)
                .Replace("{#in_type}", "m")
                .Replace("{#in_code}", newCode);

            Item itmMain = cfg.inn.applySQL(sql);
            if (itmMain.isError() || itmMain.getResult() == "")
            {
                throw new Exception("查無題號來源資料");
            }

            string newNos = itmMain.getProperty("in_nos", "");
            if (newNos == "")
            {
                throw new Exception("題號來源資料有誤");
            }

            sql = "UPDATE IN_CLA_MEETING_USER_SURVEY SET in_code = '" + newCode + "', in_nos = '" + newNos + "' WHERE id = '" + nid + "'";

            Item itmUpdate = cfg.inn.applySQL(sql);
            if (itmUpdate.isError())
            {
                throw new Exception("更新失敗");
            }
        }

        private void Run(TConfig cfg, Item itmReturn)
        {
            int question_count = SurveyQuestionCount(cfg);
            if (question_count <= 0)
            {
                throw new Exception("查無題目");
            }

            //清除舊題號
            ClearNos(cfg);

            //建立活動隨機題號
            var nosList = RunMeetingNos(cfg, question_count);
            var c = nosList.Count;

            //建立學員隨機題號
            var items = MUserItems(cfg);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string muid = item.getProperty("muid", "");

                var nosIdx = i % c;
                var obj = nosList[nosIdx];

                var in_code = obj.code;
                var nos = obj.nos;

                Item itmNo = cfg.inn.newItem("IN_CLA_MEETING_USER_SURVEY", "add");
                itmNo.setProperty("in_meeting", cfg.meeting_id);
                itmNo.setProperty("in_surveytype", cfg.survey);
                itmNo.setProperty("in_user", muid);
                itmNo.setProperty("in_code", in_code);
                itmNo.setProperty("in_type", "u");
                itmNo.setProperty("in_nos", nos);
                Item itmNoResult = itmNo.apply();
            }
        }

        private List<TObj> RunMeetingNos(TConfig cfg, int question_count)
        {
            var map = new List<TObj>();

            int page_count = GetInt(cfg.code);
            int end_idx = page_count - 1;


            for (int i = 0; i < page_count; i++)
            {
                string in_code = "";
                switch (i)
                {
                    case 0:
                        in_code = "A";
                        break;
                    case 1:
                        in_code = "B";
                        break;
                    case 2:
                        in_code = "C";
                        break;
                    case 3:
                        in_code = "D";
                        break;
                    case 4:
                        in_code = "E";
                        break;
                    case 5:
                        in_code = "F";
                        break;
                    case 6:
                        in_code = "G";
                        break;
                    case 7:
                        in_code = "H";
                        break;
                    case 8:
                        in_code = "I";
                        break;
                    case 9:
                        in_code = "J";
                        break;
                }

                var newNos = InnSport.Core.Utilities.TUtility.RandomArray(question_count).ToList();
                string nos = string.Join(",", newNos);
                map.Add(new TObj { code = in_code, nos = nos });

                Item itmNo = cfg.inn.newItem("IN_CLA_MEETING_USER_SURVEY", "add");
                itmNo.setProperty("in_meeting", cfg.meeting_id);
                itmNo.setProperty("in_surveytype", cfg.survey);
                itmNo.setProperty("in_code", in_code);
                itmNo.setProperty("in_type", "m");
                itmNo.setProperty("in_nos", nos);
                Item itmNoResult = itmNo.apply();

                if (itmNoResult.isError())
                {
                    throw new Exception("產生隨機卷號發生錯誤");
                }

                if (i != end_idx)
                {
                    System.Threading.Thread.Sleep(30);
                }
            }

            return map;
        }

        private void ClearNos(TConfig cfg)
        {
            string sql = @"
                DELETE FROM
                    IN_CLA_MEETING_USER_SURVEY
                WHERE 
                    in_meeting = '{#meeting_id}'
                    AND in_surveytype = '{#in_surveytype}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_surveytype}", cfg.survey);

            cfg.inn.applySQL(sql);
        }

        private int SurveyQuestionCount(TConfig cfg)
        {
            string sql = @"
                SELECT 
                    count(*) AS 'cnt'
                FROM 
                    IN_CLA_MEETING_SURVEYS WITH(NOLOCK) 
                WHERE 
                    source_id = '{#meeting_id}' 
                    AND in_surveytype = '{#in_surveytype}'";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_surveytype}", cfg.survey);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getResult() == "")
            {
                throw new Exception("查無問卷題目");
            }

            string cnt = items.getItemByIndex(0).getProperty("cnt", "0");

            return GetInt(cnt);
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            Item itmMeeting = MeetingInfo(cfg);
            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("in_meeting_type", itmMeeting.getProperty("in_meeting_type", ""));

            Item items = MUserItems(cfg);
            int count = items.getItemCount();
            if (count <= 0)
            {
                throw new Exception("查無學員資料");
            }

            List<string> codes = SurveyCodes(cfg);

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("<tr>");
            head.Append("  <th class='mailbox-subject text-center' data-width='15%' >編號</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='15%' >姓名</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='15%' >性別</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='15%' >身分證字號</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='15%' >出生年月日</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='15%' >AB卷</th>");
            head.Append("</tr>");
            head.Append("</thead>");


            body.Append("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string no = item.getProperty("in_name_num", "");
                string muid = item.getProperty("muid", "");
                string mrid = item.getProperty("mrid", "");
                string rid = item.getProperty("rid", "");
                string in_name = item.getProperty("in_name", "");
                string in_gender = item.getProperty("in_gender", "");
                string in_sno = item.getProperty("in_sno", "");

                body.Append("<tr class='show_all'"
                    + " data-muid='" + muid + "'"
                    + ">");

                body.Append("  <td class='text-center'> " + no + " </td>");
                body.Append("  <td class='text-center'> " + in_name + " </td>");
                body.Append("  <td class='text-center'> " + in_gender + " </td>");
                body.Append("  <td class='text-center'> " + in_sno + " </td>");
                body.Append("  <td class='text-center'> " + item.getProperty("in_birth", "") + " </td>");
                body.Append("  <td class='text-center'> " + CodeMenu(cfg, item, "in_code", codes) + " </td>");
                body.Append("</tr>");
            }
            body.Append("</tbody>");

            string table_name = "data_table";

            StringBuilder builder = new StringBuilder();
            builder.Append("<div class='box-body tablenobg' style='position: relative'>");
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.Append("</div>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            itmReturn.setProperty("inn_table", builder.ToString());
        }

        private string CodeMenu(TConfig cfg, Item item, string prop, List<string> codes)
        {
            string nid = item.getProperty("nid", "");
            string value = item.getProperty(prop, "");
            if (value != "")
            {
                value = value.ToUpper().Trim();
            }

            StringBuilder builder = new StringBuilder();
            builder.Append("<select class='form-control code_menu '");
            builder.Append("  data-nid='" + nid + "'");
            builder.Append("  data-value='" + value + "'");
            builder.Append("  data-prop='" + prop + "'");
            builder.Append("  onchange='doUpdateOption(this)'>");
            builder.Append("    <option value=''>--</option>");
            foreach (var code in codes)
            {
                builder.Append("    <option value='" + code + "'>" + code + "</option>");
            }
            //builder.Append("    <option value='A'>A</option>");
            //builder.Append("    <option value='B'>B</option>");
            //builder.Append("    <option value='C'>C</option>");
            //builder.Append("    <option value='D'>D</option>");
            //builder.Append("    <option value='E'>E</option>");
            //builder.Append("    <option value='F'>F</option>");
            builder.Append("</select>");
            return builder.ToString();
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "'"
                + " class='table table-hover table-striped'"
                + " data-toggle='table'"
                + " data-show-toggle='false'"
                + " data-show-columns='false'"
                + " data-search-align='left'"
                + " data-sort-stable='true'"
                + " data-search='false'"
                + ">";
        }

        private Item MeetingInfo(TConfig cfg)
        {
            List<string> cols = new List<string>
            {
                "id",
                "in_title",
                "CONVERT(VARCHAR, DATEADD(hour, 8, in_date_s), 111) AS 'in_date_s'",
                "CONVERT(VARCHAR, DATEADD(hour, 8, in_date_e), 111) AS 'in_date_e'",
                "in_meeting_type",
                "in_seminar_type",
                "in_level",
            };

            string sql = "SELECT " + string.Join(", ", cols) + " FROM IN_CLA_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmMeeting = cfg.inn.applySQL(sql);

            if (itmMeeting.isError() || itmMeeting.getResult() == "")
            {
                throw new Exception("活動資料錯誤");
            }

            return itmMeeting;

        }

        /// <summary>
        /// 取得卷類清單
        /// </summary>
        private List<string> SurveyCodes(TConfig cfg)
        {
            List<string> result = new List<string>();

            string sql = @"
                SELECT
                    t1.in_code
                FROM
                    IN_CLA_MEETING_USER_SURVEY t1 WITH(NOLOCK)
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                    AND t1.in_surveytype = '{#in_surveytype}'
                    AND t1.in_type = 'm'
                ORDER BY
                    t1.in_code
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_surveytype}", cfg.survey);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_code = item.getProperty("in_code", "");
                if (in_code != "")
                {
                    result.Add(in_code);
                }
            }

            return result;
        }

        private Item MUserItems(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.id AS 'muid'
                    , t1.in_name
                    , t1.in_gender
                    , t1.in_sno
                    , CONVERT(VARCHAR, DATEADD(hour, 8, t1.in_birth), 111) AS 'in_birth'
                    , t1.in_name_num
                    , t2.id AS 'mrid'
                    , t2.in_hours     -- '課程時數'
                    , t2.in_score_o_5 -- '學科成績'
                    , t2.in_score_o_3 -- '術科成績'
                    , t2.in_stat_0
                    , t2.in_note
                    , t3.id AS 'rid'
                    , t3.in_instructor_id
                    , t3.in_instructor_level
                    , t3.in_gl_instructor_id
                    , t3.in_gl_instructor_level
                    , t3.in_referee_id
                    , t3.in_referee_level
                    , t3.in_gl_referee_id
                    , t3.in_gl_referee_level
					, t4.id AS 'nid'
					, t4.in_code
					, t4.in_nos
                FROM
                    IN_CLA_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
                    IN_CLA_MEETING_RESUME t2 WITH(NOLOCK)
                    ON t2.in_user = t1.id
                LEFT OUTER JOIN
                    IN_RESUME t3 WITH(NOLOCK)
                    ON t3.in_sno = t1.in_sno
                LEFT OUTER JOIN
                    IN_CLA_MEETING_USER_SURVEY t4 WITH(NOLOCK)
                    ON t4.in_user = t1.id
                    AND t4.in_surveytype = '{#in_surveytype}'
                WHERE
                    t1.source_id = '{#meeting_id}'
                ORDER BY
                    t1.in_name_num
                    , t1.in_birth
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_surveytype}", cfg.survey);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }

            public string survey { get; set; }
            public string code { get; set; }

        }

        private class TObj
        {
            public string code { get; set; }
            public string nos { get; set; }
        }

        private int GetInt(string value, int defV = 0)
        {
            if (value == "") return 0;

            int result = defV;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return defV;
        }
    }
}