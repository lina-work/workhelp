﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_Check_Register_Group : Item
    {
        public In_Check_Register_Group(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 驗證報名人員資料
                日期: 
                    - 2022-08-30 講習比對證號、發證日期；晉段: 級證號、國技院 ID、國內段證號、啟蒙教練
                    - 2020-10-20 已存在人員報名輸入不同姓名.西元生日.性別顯示更新提示訊息 (Alan)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Check_Register_Group";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                mode = itmR.getProperty("mode", ""),
                in_meeting_type = itmR.getProperty("in_meeting_type", ""),
                in_seminar_type = itmR.getProperty("in_seminar_type", ""),
                users = itmR.getProperty("users", ""),

                tb_mt = "IN_MEETING",
                tb_msvy = "IN_MEETING_SURVEYS",
                tb_svy = "IN_SURVEY"
            };

            if (cfg.mode == "cla")
            {
                cfg.tb_mt = "IN_CLA_MEETING";
                cfg.tb_msvy = "IN_CLA_MEETING_SURVEYS";
                cfg.tb_svy = "IN_CLA_SURVEY";
            }

            string cols = "id, in_title, in_meeting_type, in_seminar_type, in_level";
            if (cfg.mode != "cla")
            {
                cols = "id, in_title, in_meeting_type, in_seminar_type, '' AS 'in_level'";
            }

            string sql = "SELECT " + cols
            + " FROM " + cfg.tb_mt + " WITH(NOLOCK)"
            + " WHERE id = '" + cfg.meeting_id + "'";

            cfg.itmMeeting = cfg.inn.applySQL(sql);
            cfg.mt_seminar_type = cfg.itmMeeting.getProperty("in_seminar_type", "");
            cfg.mt_level = cfg.itmMeeting.getProperty("in_level", "").Replace("級", "").Trim().ToUpper();

            //轉型
            var rows = MapRows(cfg);
            if (rows == null || rows.Count == 0)
            {
                throw new Exception("無報名資料");
            }

            //取得登入者資訊
            sql = "Select *";
            sql += " from In_Resume WITH(NOLOCK)";
            sql += " where in_user_id = '" + cfg.strUserId + "'";
            Item itmLoginResume = inn.applySQL(sql);

            //登入者
            string login_user_sno = itmLoginResume.getProperty("in_sno", "");
            cfg.isAdmin = login_user_sno == "lwu001" || login_user_sno == "M001";
            //cfg.isAdmin = false;

            cfg.cs = "<span style='color: rgb(255, 0, 0);font-weight:bold;'>";
            cfg.ce = "</span>";

            //匹配證照欄位
            cfg.itmRSetting = cfg.inn.applyMethod("In_Cert_Property"
                , "<in_seminar_type>" + cfg.in_seminar_type + "</in_seminar_type>");
            cfg.need_cert = cfg.itmRSetting.getProperty("need_cert", "") == "1";

            Item itmSurvies = GetMeetingSurveyItems(cfg);
            //判斷[段位]是否需要檢查
            cfg.need_check_degree_lv = IsNeedCheckDegree(cfg, itmSurvies, new List<string> { "in_degree", "in_degree_label" });
            //判斷[段證號]是否需要檢查
            cfg.need_check_degree_id = IsNeedCheckDegree(cfg, itmSurvies, new List<string> { "in_degree_id" });
            //判斷[晉升日期]是否需要檢查
            cfg.need_check_degree_dt = IsNeedCheckDegree(cfg, itmSurvies, new List<string> { "in_degree_date" });

            var errs = new StringBuilder();
            var confirms = new StringBuilder();

            int cnt = rows.Count;
            for (int i = 0; i < cnt; i++)
            {
                var row = rows[i];
                if (string.IsNullOrWhiteSpace(row.in_sno))
                {
                    continue;
                }

                row.need_check_cert_date = IsNeedCheckCertDate(cfg, row);

                row.in_birth = GetLocalDate(row.in_birth);

                Item itmResume = inn.newItem("In_Resume", "get");
                itmResume.setProperty("in_sno", row.in_sno);
                itmResume = itmResume.apply();

                if (itmResume.isError())
                {
                    continue;
                }

                var no = (i + 1).ToString();
                var entity = MapRow(cfg, itmResume);

                var wrp = new TWrapper
                {
                    errs = new List<string>(),
                    cfms = new List<string>(),
                    upList = inn.newItem(),
                };

                wrp.upList.setProperty("in_name", entity.in_name);
                wrp.upList.setProperty("in_gender", entity.in_gender);
                wrp.upList.setProperty("in_birth", entity.in_birth);

                RunMatch(cfg, wrp, row.in_name, entity.in_name, "姓名", "in_name");
                RunMatch(cfg, wrp, row.in_gender, entity.in_gender, "性別", "in_gender");
                RunMatchDay(cfg, wrp, row.in_birth, entity.in_birth, "西元生日", "in_birth");

                //報名時檢查[段位]
                RunCheckDegreeLv(cfg, wrp, row, entity);
                //報名時檢查[段證號]
                RunCheckDegreeId(cfg, wrp, row, entity);
                //報名時檢查[晉升日期]
                RunCheckDegreeDt(cfg, wrp, row, entity);

                switch (cfg.in_meeting_type)
                {
                    case "seminar":
                        if (cfg.meeting_id == "F552082D36CB43E79C83770E5D70E2FC")
                        {
                            //by pass
                        }
                        else if (cfg.need_cert)
                        {
                            RunSeminarMatch(cfg, wrp, row, entity);
                        }
                        break;

                    case "degree":
                        RunDegreeMatch(cfg, wrp, row, entity);
                        break;
                }

                if (wrp.errs.Count > 0)
                {
                    errs.Append(no + ".【" + row.in_sno + "】<br>資料與系統不符： <br> " + String.Join(" <br> ", wrp.errs) + " <br><br> ");
                    confirms.Append(no + ".【" + row.in_sno + "】<br>" + String.Join(" <br> ", wrp.cfms) + " <br><br> ");
                    wrp.upList.setProperty("in_sno", row.in_sno);
                    itmR.addRelationship(wrp.upList);
                }
            }

            if (errs.Length > 0)
            {
                itmR.setProperty("state", "error");
                itmR.setProperty("message", errs.ToString());
                itmR.setProperty("confirm_msg", "確定使用以下資訊報名?<br><br> " + confirms.ToString());
                Item itmVariable = inn.getItemByKeyedName("In_Variable", "in_metting_isUpdateUserInfo");
                string isUpdateUserInfo = itmVariable.getProperty("in_value", "");
                itmR.setProperty("isUpdateUserInfo", isUpdateUserInfo);
            }
            else
            {
                itmR.setProperty("state", "ok");
                itmR.setProperty("message", "報名資料全部符合");
            }

            //把身分換回來
            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private bool IsNeedCheckCertDate(TConfig cfg, TRow row)
        {
            bool result = true;

            if (cfg.in_meeting_type == "seminar" && cfg.mt_level == "C")
            {
                //C級講習+新訓，不檢查[發證日期欄位]
                if (!string.IsNullOrWhiteSpace(row.in_retraining) && row.in_retraining.Contains("新訓"))
                {
                    result = false;
                }
            }

            return result;
        }

        //報名時檢查[段位]
        private void RunCheckDegreeLv(TConfig cfg, TWrapper wrp, TRow row, TRow entity)
        {
            if (!cfg.need_check_degree_lv) return;

            string new_degree = ClearDegree(row.in_degree);
            string new_degree_label = ClearDegree(row.in_degree_label);
            if (new_degree == "") new_degree = new_degree_label;

            string old_degree = ClearDegree(entity.in_degree);

            if (new_degree == "" && new_degree_label == "")
            {
                wrp.errs.Add("- 無[段位]資料");
            }
            else if (old_degree == "")
            {
                wrp.errs.Add("- 系統無[段位]資料");
            }
            else
            {
                var itmOldDgree = cfg.inn.applyMethod("In_Degree_Value", "<in_degree_label>" + old_degree + "</in_degree_label>");
                var itmNewDgree = cfg.inn.applyMethod("In_Degree_Value", "<in_degree_label>" + new_degree + "</in_degree_label>");

                var old_dgr_code = itmOldDgree.getProperty("in_degree", "");
                var new_dgr_code = itmNewDgree.getProperty("in_degree", "");

                RunMatch(cfg, wrp, new_dgr_code, old_dgr_code, "段位", "in_degree");
            }
        }

        //報名時檢查[段證號]
        private void RunCheckDegreeId(TConfig cfg, TWrapper wrp, TRow row, TRow entity)
        {
            if (!cfg.need_check_degree_id) return;

            string new_degree_id = ClearDegree(row.in_degree_id);
            string old_degree_id = ClearDegree(entity.in_degree_id);

            if (new_degree_id == "")
            {
                wrp.errs.Add("- 無[段證號]資料");
            }

            if (old_degree_id == "")
            {
                wrp.errs.Add("- 系統無[段證號]資料");
            }


            RunMatch(cfg, wrp, new_degree_id, old_degree_id, "段證號", "in_degree_id");
        }

        //報名時檢查[晉升日期]
        private void RunCheckDegreeDt(TConfig cfg, TWrapper wrp, TRow row, TRow entity)
        {
            if (!cfg.need_check_degree_dt) return;

            string new_degree_dt = ClearDegree(row.in_degree_date);
            string old_degree_dt = ClearDegree(entity.in_degree_date);

            if (new_degree_dt == "")
            {
                wrp.errs.Add("- 無[晉段日期]資料");
            }

            if (old_degree_dt == "")
            {
                wrp.errs.Add("- 系統無[晉段日期]資料");
            }


            RunMatchDay(cfg, wrp, new_degree_dt, old_degree_dt, "晉段日期", "in_degree_date");
        }

        private string ClearDegree(string value)
        {
            if (value == null)
            {
                return "";
            }
            if (value.Contains("請選擇") || value == "無" || value == "0")
            {
                return "";
            }

            return value;
        }

        //[講習]比對講師履歷
        private void RunSeminarMatch(TConfig cfg, TWrapper wrp, TRow row, TRow entity)
        {
            //國內教練、裁判證號(in_instructor_id、in_referee_id...)

            string cert_title = cfg.itmRSetting.getProperty("cert_title", "");
            string cert_id_pro = cfg.itmRSetting.getProperty("cert_id_pro", "");
            string cert_lv_pro = cfg.itmRSetting.getProperty("cert_lv_pro", "");
            string cert_dt_pro = cfg.itmRSetting.getProperty("cert_dt_pro", "");

            string old_cert_id = entity.Value.getProperty(cert_id_pro, "");
            string old_cert_dt = GetLocalDate(entity.Value.getProperty(cert_dt_pro, ""));

            string new_cert_id = row.in_cert_id;
            string new_cert_dt = row.in_cert_date;

            if (new_cert_id == "") new_cert_id = row.in_exe_a2;
            if (new_cert_dt == "") new_cert_dt = row.in_exe_a8;

            if (new_cert_id == "")
            {
                if (cfg.in_seminar_type.Contains("referee"))
                {
                    new_cert_id = row.in_referee_id;
                }
                else if (cfg.in_seminar_type.Contains("coach"))
                {
                    new_cert_id = row.in_stuff_c5;
                }
            }

            RunMatch(cfg, wrp, new_cert_id, old_cert_id, cert_title + "證號", cert_id_pro);

            if (row.need_check_cert_date)
            {
                RunMatchDay(cfg, wrp, new_cert_dt, old_cert_dt, cert_title + "發證日", cert_dt_pro);
            }
        }

        //[晉段]比對講師履歷
        private void RunDegreeMatch(TConfig cfg, TWrapper wrp, TRow row, TRow entity)
        {
            //級段證號(in_exe_a1)
            //國技院ID(in_tcon)
            //國內段證號(in_degree_id)
            //發證日(in_degree_date)
            //啟蒙教練(in_stuff_c1)

            if (string.IsNullOrWhiteSpace(row.in_l1) || row.in_l1 == "壹段")
            {
                //八級要升一段(需確認資料有無進系統)
                RunMatch(cfg, wrp, row.in_exe_a1, entity.in_degree_id, "級段證號", "in_exe_a1");
                //RunMatchDay(cfg, wrp, row.in_exe_a2, entity.in_degree_date, "八級晉升日期", "in_degree_date");
            }
            else
            {
                RunMatch(cfg, wrp, row.in_degree_id, entity.in_degree_id, "國內段證號", "in_degree_id");
                if (string.IsNullOrWhiteSpace(row.in_date))
                {
                    RunMatchDay(cfg, wrp, row.in_degree_date, entity.in_degree_date, "發證日期", "in_degree_date");
                }
                else
                {
                    RunMatchDay(cfg, wrp, row.in_date, entity.in_degree_date, "發證日期", "in_degree_date");
                }
            }

            RunMatch(cfg, wrp, row.in_stuff_c1, entity.in_stuff_c1, "啟蒙教練", "in_stuff_c1");
            RunMatch(cfg, wrp, row.in_tcon, entity.in_tcon, "國技院ID", "in_tcon");
        }

        private void RunMatchDay(TConfig cfg, TWrapper wrp, string sNew, string sOld, string title, string prop)
        {
            var d1 = sNew == null ? "" : sNew.Replace("/", "-");
            var d2 = sOld == null ? "" : sOld.Replace("/", "-");

            RunMatch(cfg, wrp, d1, d2, title, prop);
        }

        private void RunMatch(TConfig cfg, TWrapper wrp, string sNew, string sOld, string title, string prop)
        {
            if (cfg.isAdmin)
            {
                if (sNew != sOld)
                {
                    wrp.errs.Add("- " + title.Replace("西元", "") + " " + sNew + " 與系統 " + sOld + " 不符");
                    wrp.cfms.Add("- " + title + " 報名資訊調整為" + cfg.cs + sNew + cfg.ce);
                    wrp.upList.setProperty(prop, sNew);
                }
            }
            else
            {
                if (sNew != sOld)
                {
                    wrp.errs.Add("- " + title);
                    wrp.cfms.Add("- " + title + " 報名資訊調整為" + cfg.cs + sNew + cfg.ce);
                    wrp.upList.setProperty(prop, sNew);
                }
            }
        }

        private TRow MapRow(TConfig cfg, Item itmResume)
        {
            return new TRow
            {
                in_name = itmResume.getProperty("in_name", ""),
                in_sno = itmResume.getProperty("in_sno", ""),
                in_birth = GetLocalDate(itmResume.getProperty("in_birth", "")),
                in_gender = itmResume.getProperty("in_gender", ""),
                in_en_name = itmResume.getProperty("in_en_name", ""),

                in_degree = itmResume.getProperty("in_degree", ""),
                in_degree_id = itmResume.getProperty("in_degree_id", ""),
                in_degree_date = GetLocalDate(itmResume.getProperty("in_degree_date", "")),

                in_gl_degree_id = itmResume.getProperty("in_gl_degree_id", ""),
                in_gl_degree_date = GetLocalDate(itmResume.getProperty("in_gl_degree_date", "")),

                in_stuff_c1 = itmResume.getProperty("in_stuff_c1", ""),
                in_stuff_c2 = itmResume.getProperty("in_stuff_c2", ""),
                in_stuff_c5 = itmResume.getProperty("in_stuff_c5", ""),

                in_date = GetLocalDate(itmResume.getProperty("in_date", "")),
                in_tcon = itmResume.getProperty("in_tcon", ""),

                Value = itmResume,

            };
        }

        private List<TRow> MapRows(TConfig cfg)
        {
            var result = default(List<TRow>);
            try
            {
                if (cfg.users != null && cfg.users != "")
                {
                    //這行會避免json轉換為XML十日期被轉換格式。
                    Newtonsoft.Json.JsonConvert.DefaultSettings = () => new Newtonsoft.Json.JsonSerializerSettings
                    {
                        DateParseHandling = Newtonsoft.Json.DateParseHandling.None
                    };

                    result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TRow>>(cfg.users);
                }
            }
            catch (Exception ex)
            {
            }

            if (result == null)
            {
                result = new List<TRow>();
            }
            return result;
        }

        //取得報名表問項
        private Item GetMeetingSurveyItems(TConfig cfg)
        {
            string sql = @"
                SELECT 
                    t2.id
                    , t2.in_property 
                FROM 
                    {#tb_msvy} t1 WITH(NOLOCK)
                INNER JOIN
                    {#tb_svy} t2 WITH(NOLOCK) 
                    ON t2.id = t1.related_id
                WHERE 
                    t1.source_id = '{#meeting_id}'
					AND t1.in_surveytype = '1'
					AND t2.in_property IN ('in_degree', 'in_degree_label', 'in_degree_id', 'in_degree_date')
            ";

            sql = sql.Replace("{#tb_msvy}", cfg.tb_msvy)
                .Replace("{#tb_svy}", cfg.tb_svy)
                .Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getResult() == "")
            {
                throw new Exception("查無報名問項設定");
            }

            return items;

        }
        //判斷該項目是否需要檢查
        private bool IsNeedCheckDegree(TConfig cfg, Item items, List<string> properties)
        {
            var result = false;

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_property = item.getProperty("in_property", "");
                if (properties.Contains("in_property"))
                {
                    result = true;
                    break;
                }
            }

            //雖有該問項，但活動類型不匹配 => 強制關閉檢查
            switch (cfg.in_meeting_type)
            {
                case "game":
                case "seminar":
                case "degree":
                    break;
                default:
                    result = false;
                    break;
            }

            return result;
        }

        //判斷[段證號]是否需要檢查
        private void IsNeedCheckDegreeId(TConfig cfg, Item items)
        {
            cfg.need_check_degree_id = false;

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_property = item.getProperty("in_property", "");

                if (in_property == "in_degree_id")
                {
                    cfg.need_check_degree_id = true;
                    break;
                }
            }

            //雖有該問項，但活動類型不匹配 => 強制關閉檢查
            switch (cfg.in_meeting_type)
            {
                case "game":
                case "seminar":
                case "degree":
                    break;
                default:
                    cfg.need_check_degree_id = false;
                    break;
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string in_meeting_type { get; set; }
            public string in_seminar_type { get; set; }
            public string users { get; set; }
            public string mode { get; set; }

            public bool isAdmin { get; set; }
            public string cs { get; set; }
            public string ce { get; set; }

            public Item itmRSetting { get; set; }
            public bool need_cert { get; set; }
            public bool need_check_degree_lv { get; set; }
            public bool need_check_degree_id { get; set; }
            public bool need_check_degree_dt { get; set; }

            public string tb_mt { get; set; }
            public string tb_msvy { get; set; }
            public string tb_svy { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_seminar_type { get; set; }
            public string mt_level { get; set; }
        }

        private class TRow
        {
            public string in_l1 { get; set; }
            public string in_no { get; set; }
            public string in_sno { get; set; }
            public string in_name { get; set; }
            public string in_gender { get; set; }
            public string in_birth { get; set; }
            public string in_en_name { get; set; }


            public string in_exe_a1 { get; set; }
            public string in_exe_a2 { get; set; }
            public string in_exe_a8 { get; set; }

            public string in_cert_yn { get; set; }
            public string in_cert_id { get; set; }
            public string in_cert_date { get; set; }

            public string in_stuff_c1 { get; set; }
            public string in_stuff_c2 { get; set; }
            public string in_stuff_c5 { get; set; }

            public string in_date { get; set; }
            public string in_tcon { get; set; }

            public string in_degree { get; set; }
            public string in_degree_label { get; set; }

            public string in_degree_id { get; set; }
            public string in_degree_date { get; set; }
            public string in_gl_degree_id { get; set; }
            public string in_gl_degree_date { get; set; }

            public string in_referee_id { get; set; }
            public string in_retraining { get; set; }

            public bool need_check_cert_date { get; set; }
            public Item Value { get; set; }
        }

        private class TWrapper
        {
            public List<string> errs { get; set; }
            public List<string> cfms { get; set; }
            public Item upList { get; set; }
        }

        private string GetLocalDate(string value, int hours = 0)
        {
            if (value == "") return "";

            DateTime dt = DateTime.MinValue;
            if (DateTime.TryParse(value, out dt))
            {
                return dt.AddHours(hours).ToString("yyyy-MM-dd");
            }
            else
            {
                return dt.ToString("yyyy-MM-dd");
            }
        }
    }
}