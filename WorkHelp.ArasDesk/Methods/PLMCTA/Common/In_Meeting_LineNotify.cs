﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_Meeting_LineNotify : Item
    {
        public In_Meeting_LineNotify(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: Line 快速通知
                日誌: 
                    - 2022-08-25: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_LineNotify";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),
                topCount = "300",

                filter = itmR.getProperty("filter", ""),
                keyword = itmR.getProperty("keyword", ""),
                in_type = itmR.getProperty("in_type", ""),
                meeting_id = itmR.getProperty("meeting_id", ""),
                meeting_title = itmR.getProperty("meeting_title", ""),
                mode = itmR.getProperty("mode", ""),
                need_token = itmR.getProperty("need_token", "") == "1",
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "menu":
                    Menu(cfg, itmR);
                    break;

                case "table":
                    Table(cfg, itmR);
                    break;

                case "send":
                    SendNotify(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void SendNotify(TConfig cfg, Item itmReturn)
        {
            var message = itmReturn.getProperty("message", "");
            var values = itmReturn.getProperty("values", "");

            if (message == "") throw new Exception("請設定訊息內容");
            if (values == "") throw new Exception("查無名單");

            var rows = ToRows(cfg, values);
            if (rows == null || rows.Count <= 0) throw new Exception("查無名單");

            var log = new StringBuilder();
            log.AppendLine("發送 Line 通知 = " + message);
            foreach (var row in rows)
            {
                if (string.IsNullOrEmpty(row.name)) continue;
                if (string.IsNullOrEmpty(row.sno)) continue;
                if (string.IsNullOrEmpty(row.token)) continue;
                log.AppendLine(" - " + row.name + "(" + row.sno + ")");

                //SendMsg(cfg, row.token, message);
            }

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, log.ToString());
        }

        private List<TRow> ToRows(TConfig cfg, string value)
        {
            try
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject<List<TRow>>(value);
            }
            catch (Exception ex)
            {
                return new List<TRow>();
            }
        }

        /// <summary>
        /// 發送訊息
        /// </summary>
        /// <returns></returns>
        private void SendMsg(TConfig cfg, string token, string message)
        {
            try
            {
                string url = "https://notify-api.line.me/api/notify";

                System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls;
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                var httpWebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
                httpWebRequest.Method = "POST";
                httpWebRequest.KeepAlive = true;
                httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                httpWebRequest.Headers.Set("Authorization", "Bearer " + token);

                string text = "";
                text = text + "message=\r\n" + message;

                string text2 = string.Empty;

                text = text + "&imageThumbnail=" + text2;
                text = text + "&imageFullsize=" + text2;
                byte[] bytes = Encoding.UTF8.GetBytes(text);

                using (var stream = httpWebRequest.GetRequestStream())
                {
                    stream.Write(bytes, 0, bytes.Length);
                }

                //不等待
                System.Threading.ThreadPool.QueueUserWorkItem(o =>
                {
                    RunSend(cfg, httpWebRequest);
                });

                //HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (Exception ex)
            {
                InnSport.Core.Logging.TLog.Watch(message: cfg.strMethodName + "." + "SendMsg _# ex: " + ex.Message);
            }
        }

        private void RunSend(TConfig cfg, System.Net.HttpWebRequest httpWebRequest)
        {
            try
            {
                httpWebRequest.GetResponse();
            }
            catch (Exception ex)
            {
                InnSport.Core.Logging.TLog.Watch(message: cfg.strMethodName + "." + "RunSend _# ex: " + ex.Message);
            }
        }

        private void Table(TConfig cfg, Item itmReturn)
        {
            var list = GetItemList(cfg);
            var contens = TableContents(cfg, list);
            itmReturn.setProperty("inn_count", list.Count.ToString());
            itmReturn.setProperty("inn_table", contens);
        }

        private string TableContents(TConfig cfg, List<Item> list)
        {
            StringBuilder table = new StringBuilder();
            table.Append("<table id='data_table' class='table table-bordered table-hover' data-toggle='table'>");
            table.Append("  <thead>");
            table.Append("    <tr>");
            table.Append("      <th scope='col' class='text-center'>" + HeadCheckBox(cfg) + "</th>");
            table.Append("      <th scope='col' style='color: red'>No.</th>");
            table.Append("      <th scope='col'>姓名</th>");
            table.Append("      <th scope='col'>帳號</th>");
            table.Append("      <th scope='col'>Token</th>");
            table.Append("      <th scope='col'>功能</th>");
            table.Append("    </tr>");
            table.Append("  </thead>");

            table.Append("  <tbody>");

            int no = 1;
            int count = list.Count;
            for (int i = 0; i < count; i++)
            {
                var item = list[i];
                var in_line_notify = item.getProperty("in_line_notify", "").Trim();

                //過濾未綁定 LINE
                if (cfg.need_token && in_line_notify == "")
                {
                    continue;
                }

                table.Append("<tr>");
                table.Append("  <td>" + BodyCheckBox(cfg, item) + "</td>");
                table.Append("  <td>" + no + "</td>");
                table.Append("  <td>" + item.getProperty("in_name", "") + "</td>");
                table.Append("  <td>" + item.getProperty("login_name", "") + "</td>");
                table.Append("  <td>" + item.getProperty("in_line_notify", "") + "</td>");
                table.Append("  <td>" + BodyButtons(cfg, item) + "</td>");
                table.Append("</tr>");

                no++;
            }
            table.Append("  </tbody>");

            table.Append("</table>");

            return table.ToString();

        }

        private string HeadCheckBox(TConfig cfg)
        {
            return "<div class='form-control'>"
                + " <input type='checkbox' class='checkbox'"
                + " onclick='HeadCheck_Click(this)'"
                + " > </div>";
        }

        private string BodyCheckBox(TConfig cfg, Item item)
        {
            var in_name = item.getProperty("in_name", "");
            var in_sno = item.getProperty("in_sno", "");
            var in_line_notify = item.getProperty("in_line_notify", "").Trim();
            if (in_line_notify == "") return "&nbsp;";

            return "<div class='form-control'>"
                + " <input type='checkbox' class='checkbox inn_chk'"
                + " checked='checked' "
                + " data-name='" + in_name + "'"
                + " data-sno='" + in_sno + "'"
                + " data-token='" + in_line_notify + "'"
                + " > </div>";
        }

        private string BodyButtons(TConfig cfg, Item item)
        {
            var in_name = item.getProperty("in_name", "");
            var in_sno = item.getProperty("in_sno", "");
            var in_line_notify = item.getProperty("in_line_notify", "").Trim();
            if (in_line_notify == "") return "&nbsp;";

            return "<button class='btn btn-sm btn-primary'"
            + " data-name='" + in_name + "'"
            + " data-sno='" + in_sno + "'"
            + " data-token='" + in_line_notify + "'"
            + " onclick='AddToWaitList(this)'"
            + ">"
            + "加入"
            + "</button>";
        }

        private List<Item> GetItemList(TConfig cfg)
        {
            switch (cfg.filter)
            {
                case "in_muser": return RunTableItems(cfg, GetMtUserItems);
                case "verify_muser": return RunTableItems(cfg, GetMtVerUserItems);
                case "in_creator": return RunTableItems(cfg, GetMtCreatorItems);
                case "asc": return RunTableItems(cfg, GetAssociationItems);
                case "assign_user": return RunTableItems(cfg, GetSysUserItems);
                default: return new List<Item>();
            }
        }

        private List<Item> RunTableItems(TConfig cfg, Func<TConfig, string, Item> getItems)
        {
            var list = new List<Item>();
            var no_keyword = true;

            if (cfg.keyword != "")
            {
                var words = cfg.keyword.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                if (words != null && words.Length > 0)
                {
                    no_keyword = false;
                    foreach (var word in words)
                    {
                        var items = getItems(cfg, word);
                        MergeList(list, items);
                    }
                }

            }

            if (no_keyword)
            {
                MergeList(list, getItems(cfg, ""));
            }

            return list;
        }

        private void MergeList(List<Item> list, Item items)
        {
            if (items == null || items.isError() || items.getResult() == "")
            {
                return;
            }

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                list.Add(item);
            }
        }

        private void Menu(TConfig cfg, Item itmReturn)
        {
            var items = MeetingItems(cfg);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType("in_meeting_title");
                itmReturn.addRelationship(item);
            }
        }

        private Item MeetingItems(TConfig cfg)
        {

            string sql = @"
                SELECT
                    '{#mode}' AS 'mode'
                    , id
                    , item_number 
                    , in_title 
                    , CONVERT(VARCHAR, in_date_s, 111) AS 'in_date_s'
                    , CONVERT(VARCHAR, in_date_e, 111) AS 'in_date_e'
                    , in_address 
                FROM 
                    {#table} WITH(NOLOCK) 
                WHERE 
                    ISNULL(in_is_template, 0) = 0
                    AND ISNULL(in_is_main, 0) = 0
                    AND in_meeting_type = '{#in_type}'
            ";

            string sql1 = sql.Replace("{#table}", "IN_MEETING")
                .Replace("{#in_type}", cfg.in_type)
                .Replace("{#mode}", "");

            string sql2 = sql.Replace("{#table}", "IN_CLA_MEETING")
                .Replace("{#in_type}", cfg.in_type)
                .Replace("{#mode}", "cla");

            string sql_qry = "SELECT * FROM (" + sql1 + " UNION " + sql2 + ") t1 ORDER BY t1.item_number DESC";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql_qry);
        }

        private Item GetMtUserItems(TConfig cfg, string word)
        {
            string table = cfg.mode == "cla"
                ? "IN_CLA_MEETING_USER"
                : "IN_MEETING_USER";

            string cond = word == ""
                ? ""
                : "AND (t1.in_name LIKE N'" + word + "%' OR t1.in_sno LIKE N'" + word + "%')";

            string sql = @"
                SELECT TOP {#topCount}
                    t1.in_name
                    , t1.in_sno
                    , t1.in_birth
                    , t1.in_gender
                    , t2.login_name 
                    , t2.in_line_notify 
                FROM 
                    {#table} t1 WITH(NOLOCK) 
                INNER JOIN
                    [USER] t2 WITH(NOLOCK)
                    ON t2.id = t1.related_id
                WHERE 
                    t1.source_id = '{#meeting_id}'
                    {#cond}
                ORDER BY
                    t1.in_sno
            ";

            sql = sql.Replace("{#topCount}", cfg.topCount)
                .Replace("{#table}", table)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#cond}", cond);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetMtVerUserItems(TConfig cfg, string word)
        {
            string table = cfg.mode == "cla"
                ? "IN_CLA_MEETING_USER"
                : "IN_MEETING_USER";

            string cond = word == ""
                ? ""
                : "AND (t1.in_name LIKE N'" + word + "%' OR t1.in_sno LIKE N'" + word + "%')";

            string sql = @"
                SELECT TOP {#topCount}
                    t1.in_name
                    , t1.in_sno
                    , t1.in_birth
                    , t1.in_gender
                    , t2.login_name 
                    , t2.in_line_notify 
                FROM 
                    {#table} t1 WITH(NOLOCK) 
                INNER JOIN
                    [USER] t2 WITH(NOLOCK)
                    ON t2.id = t1.related_id
                WHERE 
                    t1.source_id = '{#meeting_id}'
                    AND ISNULL(in_ass_ver_result, 0) = 1
                    {#cond}
                ORDER BY
                 t1.in_sno
            ";

            sql = sql.Replace("{#topCount}", cfg.topCount)
                .Replace("{#table}", table)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#cond}", cond);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetMtCreatorItems(TConfig cfg, string word)
        {
            string table = cfg.mode == "cla"
                ? "IN_CLA_MEETING_USER"
                : "IN_MEETING_USER";

            string cond = word == ""
                ? ""
                : "AND (t1.in_creator LIKE N'" + word + "%' OR t1.in_creator_sno LIKE N'" + word + "%')";

            string sql = @"
                SELECT DISTINCT
                    t1.in_creator        AS 'in_name'
                    , t1.in_creator_sno  AS 'in_sno'
                    , t3.in_line_notify 
                FROM 
                    {#table} t1 WITH(NOLOCK)
                INNER JOIN
                    IN_RESUME t2 WITH(NOLOCK) 
                    ON t2.in_sno = t1.in_creator_sno
                INNER JOIN
                    [USER] t3 WITH(NOLOCK)
                    ON t3.login_name = t2.login_name
                WHERE 
                    t1.source_id = '{#meeting_id}'
                ORDER BY
                    t1.in_creator_sno
            ";

            sql = sql.Replace("{#topCount}", cfg.topCount)
                .Replace("{#table}", table)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#cond}", cond);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }
        
        private Item GetAssociationItems(TConfig cfg, string word)
        {
            string sql = @"
                SELECT
	                t1.in_name
	                , t1.in_sno
	                , t2.login_name 
	                , t2.in_line_notify 
                FROM
	                IN_RESUME t1 WITH(NOLOCK)
                INNER JOIN
	                [USER] t2 WITH(NOLOCK)
	                ON t2.login_name = t1.login_name
                WHERE
	                ISNULL(t1.in_is_admin, 0) = 1
	                AND t1.in_sno NOT IN (''
		                , 'lwu001'
		                , 'lwu002'
		                , 'lwu003'
		                , 'mu001'
		                , 'ma001'
		                , 'admin'
		                , 'demo'
		                , 'root'
		                , 'M001'
	                )
                ORDER BY
	                t1.in_sno
            ";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetSysUserItems(TConfig cfg, string word)
        {
            if (word == "") return null;

            string sql = @"
                SELECT TOP {#topCount}
                    t1.in_name
                    , t1.in_sno
                    , t2.login_name 
                    , t2.in_line_notify 
                FROM 
                    IN_RESUME t1 WITH(NOLOCK)
                INNER JOIN
                    [USER] t2 WITH(NOLOCK) 
                    ON t2.login_name = t1.login_name
                WHERE 
                    t1.in_name LIKE N'{#keyword}%'
                    OR t1.in_sno LIKE N'{#keyword}%'
                ORDER BY
                    t1.in_sno
            ";

            sql = sql.Replace("{#topCount}", cfg.topCount)
                .Replace("{#keyword}", word);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }
            public string topCount { get; set; }

            public string filter { get; set; }
            public string keyword { get; set; }
            public string in_type { get; set; }
            public string meeting_id { get; set; }
            public string meeting_title { get; set; }
            public string mode { get; set; }
            public string scene { get; set; }

            public bool need_token { get; set; }
        }

        private class TRow
        {
            public string name { get; set; }
            public string sno { get; set; }
            public string token { get; set; }

        }
    }
}