﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_Resume_Search : Item
    {
        public In_Resume_Search(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 查詢人員資料
    日誌: 
        - 2022-04-22: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Resume_Search";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            var keyword = itmR.getProperty("keyword", "").Trim();
            var rows = new List<TResume>();

            if (keyword != "")
            {
                string sql = @"
                    SELECT TOP 50 
                        id
                        , in_name
                        , in_sno
                        , in_gender
	                    , DATEADD(hour, 8, in_birth) AS 'in_birth'
                    FROM
                        IN_RESUME WITH(NOLOCK)
                    WHERE
                        LEN(IN_SNO) = 10
                        AND 
                        (
                            (in_name like '{#keyword}%')
                            OR
                            (in_sno like '{#keyword}%')
                        )
                    ORDER BY in_name
                ";

                sql = sql.Replace("{#keyword}", keyword);

                Item items = inn.applySQL(sql);

                int count = items.getItemCount();

                for (int i = 0; i < count; i++)
                {
                    var item = items.getItemByIndex(i);
                    var row = new TResume
                    {
                        in_name = item.getProperty("in_name", ""),
                        in_sno = item.getProperty("in_sno", ""),
                        in_gender = item.getProperty("in_gender", ""),
                        in_birth = GetDateTimeStr(item.getProperty("in_birth", "")),
                    };
                    rows.Add(row);
                }
            }


            var str2 = Newtonsoft.Json.JsonConvert.SerializeObject(rows);

            itmR.setProperty("json", str2);

            return itmR;
        }

        private class TResume
        {
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_gender { get; set; }
            public string in_birth { get; set; }
        }

        private string GetDateTimeStr(string value)
        {
            if (value == "") return "";

            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value.Replace("/", "-"), out result);
            return result.ToString("yyyy/MM/dd");
        }
    }
}