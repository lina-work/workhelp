﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using System.Net;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class Simple_Line_Notify : Item
    {
        public Simple_Line_Notify(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "Simple_Line_Notify";

            Item itmR = this;
            CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            string user = itmR.getProperty("user", "");
            string msg = itmR.getProperty("msg", "");

            if (msg == "" || user != "innosoft")
            {
                return itmR;
            }

            string site = "plmcta";
            string keyed_name = "M001 中華跆協";
            string postUrl = "http://act.innosoft.com.tw/notify/LineNotifySend.aspx"
                + "?site=" + site
                + "&identitys=" + keyed_name
                + "&msg=" + msg
                + "&action=send";

            LineNotify(CCO, inn, strMethodName, postUrl);


            site = "iact";
            keyed_name = "A160988973 劉奕成";
            postUrl = "http://act.innosoft.com.tw/notify/LineNotifySend.aspx"
                + "?site=" + site
                + "&identitys=" + keyed_name
                + "&msg=" + msg
                + "&action=send";

            //LineNotify(CCO, inn, strMethodName, postUrl);


            return itmR;
        }

        private void LineNotify(Aras.Server.Core.CallContext CCO, Innovator inn, string strMethodName, string postUrl)
        {
            try
            {
                HttpWebRequest hwrSendRequest = (HttpWebRequest)WebRequest.Create(postUrl);
                hwrSendRequest.Method = "POST";
                hwrSendRequest.ContentType = "application/json";

                WebResponse wrToDoResponse;
                Stream stmReqStream = hwrSendRequest.GetRequestStream();
                byte[] byarRequestBody = Encoding.UTF8.GetBytes("");
                stmReqStream.Write(byarRequestBody, 0, byarRequestBody.Length);
                string strResponse;

                wrToDoResponse = hwrSendRequest.GetResponse();
                StreamReader srResponse = new StreamReader(wrToDoResponse.GetResponseStream());
                strResponse = srResponse.ReadToEnd();
            }
            catch (Exception ex)
            {
                CCO.Utilities.WriteDebug(strMethodName, ex.Message);
            }

        }
    }
}
