﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using System.Net;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_Notice_Send : Item
    {
        public In_Notice_Send(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
目的: 新版發送通知(通知設定發送功能)
輸入: 
輸出:
位置: 
做法: 
    物件動作:in_notice_send
    下拉清單:in_notice_title
    關聯:
        In_Meeting_NoticeSetting
        In_Cla_Meeting_NoticeSetting
        In_Resume_NoticeRecord
    method:
    In_Notice_function

*/


            string strMethodName = "In_Notice_Send";
            //System.Diagnostics.Debugger.Break();
            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);
            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
            _InnH.AddLog(strMethodName, "MethodSteps");

            //System.Diagnostics.Debugger.Break();


            string sql = "";
            string aml = "";
            string userID = inn.getUserID();
            string mode = this.getProperty("in_notice_mode", ""); // 發送模式 email or line
            string in_message = this.getProperty("in_message", "");
            string meeting_id = this.getProperty("source_id", "");
            string in_notice_title = this.getProperty("in_notice_title", "");
            string in_sno = this.getProperty("in_sno", "");
            string muid = this.getProperty("muid", "");
            string in_recipient = this.getProperty("in_recipient", "");
            string in_func = this.getProperty("in_func", "");
            string in_method = this.getProperty("in_method", "");
            string in_file = this.getProperty("in_file", "");
            string meetingType = "";//in_meeting or in_cla_meeting

            try
            {
                meetingType = this.getPropertyItem("source_id").getType();
            }
            catch
            {
                meetingType = this.getProperty("meetingType");
            }


            aml = "<AML>";
            aml += "<Item type='User' action='get'>";
            aml += "<id>" + userID + "</id>";
            aml += "</Item></AML>";
            Item itmLoginUser = inn.applyAML(aml);

            //登入帳號
            string loginID = itmLoginUser.getProperty("login_name", "");
            //登入姓名
            string loginName = itmLoginUser.getProperty("last_name", "");

            sql = @"SELECT * 
    FROM EMAIL_MESSAGE WITH (NOLOCK)
    WHERE ID = '{#in_message}'";
            sql = sql.Replace("{#in_message}", in_message);
            Item itmMsg = inn.applySQL(sql);

            //信件主旨 in_title
            string subject = itmMsg.getProperty("subject", "");

            //信件內容 in_name in_register_url
            string body_html = itmMsg.getProperty("body_html", "");

            Item itmUsers = inn.newItem();

            if (in_func != "")
            {
                aml = "<in_func>" + in_func + "</in_func>";
                aml += "<meetingType>" + meetingType + "</meetingType>";
                aml += "<meeting_id>" + meeting_id + "</meeting_id>";
                aml += "<in_message>" + in_message + "</in_message>";
                aml += "<in_sno>" + in_sno + "</in_sno>";
                aml += "<in_recipient>" + in_recipient + "</in_recipient>";
                aml += "<muid>" + muid + "</muid>";


                switch (in_func)
                {
                    case "method":
                        sql = @"SELECT name FROM [method] WITH (NOLOCK)
                WHERE id = '" + in_method + "'";
                        Item itmMet = inn.applySQL(sql);
                        if (!itmMet.isError() && itmMet.getItemCount() > 0)
                        {
                            in_method = itmMet.getProperty("name");
                        }
                        itmUsers = inn.applyMethod(in_method, aml);
                        break;
                    default:
                        itmUsers = inn.applyMethod("In_Notice_function", aml);
                        break;
                }

            }
            else
            {
                throw new Exception("設定檔案有誤，無法發送訊息");
            }

            // string filename = "";
            // aml = "<AML>" +
            // "<Item type='File' action='get'>" +
            // "<id>" + in_file + "</id>" +
            // "</Item></AML>";
            // Item Files = inn.applyAML(aml);

            // if (Files.getItemCount() > 0)
            // {
            //     filename = Files.getProperty("filename", "");//取得檔名
            // }

            // string url = @"C:\Aras\Vault\PLMCTA\";//讀取大頭照的路徑
            // //檔案路徑切出來
            // string id_1 = in_file.Substring(0, 1);
            // string id_2 = in_file.Substring(1, 2);
            // string id_3 = in_file.Substring(3, 29);


            int count = itmUsers.getItemCount();

            if (!itmUsers.isError() && count > 0)
            {
                for (int i = 0; i < count; i++)
                {
                    Item itmUser = itmUsers.getItemByIndex(i);

                    string in_title = itmUser.getProperty("in_title", "");
                    string in_register_url = itmUser.getProperty("in_register_url", "");
                    string in_name = itmUser.getProperty("in_name", "");
                    string in_email = itmUser.getProperty("in_email", "");
                    string resumeID = itmUser.getProperty("resume_id", "");
                    string in_creator = itmUser.getProperty("in_creator", "");
                    string in_l1 = itmUser.getProperty("in_l1", "");
                    string in_l2 = itmUser.getProperty("in_l2", "");
                    string in_l3 = itmUser.getProperty("in_l3", "");
                    string in_url = itmUser.getProperty("in_url", "");
                    string body_htmlTxt = body_html.Replace("{#in_name}", in_name)
                                                    .Replace("{#in_register_url}", in_register_url)
                                                    .Replace("{#in_title}", in_title)
                                                    .Replace("{#in_creator}", in_creator)
                                                    .Replace("{#in_l1}", in_l1)
                                                    .Replace("{#in_l2}", in_l2)
                                                    .Replace("{#in_l3}", in_l3)
                                                    ;
                    string meetingUserId = itmUser.getProperty("user_id", "");
                    string OuterInnovatorURL = _InnH.GetInVariable("app_url").getProperty("in_value", "");
                    string url = OuterInnovatorURL + "pages/c.aspx?page=" + in_url + "&method=In_MeetingUserResponse&in_meetingid=" + meeting_id + "";
                    //取代通知訊息內容
                    string subjectTxt = subject.Replace("{#in_title}", in_title);


                    // int qty = Convert.ToInt32(itmUser.getProperty("qty","")) ;
                    // if (qty > 0 && in_func !="recipient"){
                    //     continue;
                    // }

                    try
                    {
                        if (mode == "email")
                        {

                            body_htmlTxt = body_htmlTxt.Replace("{#in_url}", "<a href=" + url + ">展示網址</a>");
                            System.Net.Mail.MailAddress from = new System.Net.Mail.MailAddress("tanchi@innosoft.com.tw", "MeetingAdmin");
                            System.Net.Mail.MailAddress to = new System.Net.Mail.MailAddress(in_email, in_name);
                            System.Net.Mail.MailMessage myMail = new System.Net.Mail.MailMessage(from, to);

                            // add ReplyTo
                            System.Net.Mail.MailAddress replyTo = new System.Net.Mail.MailAddress("no-reply@example.com");
                            myMail.ReplyToList.Add(replyTo);

                            // set subject and encoding
                            myMail.Subject = subjectTxt;
                            myMail.SubjectEncoding = System.Text.Encoding.UTF8;


                            // set body-message and encoding
                            myMail.Body = body_htmlTxt;
                            myMail.BodyEncoding = System.Text.Encoding.UTF8;
                            // text or html
                            myMail.IsBodyHtml = true;


                            // 附檔功能
                            // System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(url + id_1 + @"\" + id_2 + @"\" + id_3 + @"\" + filename);
                            // myMail.Attachments.Add(attachment);

                            CCO.Email.SetupSmtpMailServerAndSend(myMail);

                            //新增
                            aml = "<AML><Item type='In_Resume_NoticeRecord' action='add'>" +
                                "<source_id>" + resumeID + "</source_id>" +
                                "<" + meetingType.ToLower() + ">" + meeting_id + "</" + meetingType.ToLower() + ">" +
                                "<in_notice_title>" + in_notice_title + "</in_notice_title>" +
                                "<in_message>" + in_message + "</in_message>" +
                                "<in_notice_date_s>" + System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss") + "</in_notice_date_s>" +
                                "<in_notice_mode>" + mode + "</in_notice_mode>" +
                                "<in_notice_status>Y</in_notice_status>" +
                                "<in_creator_sno>" + loginID + "</in_creator_sno>" +
                                "<in_creator>" + loginName + "</in_creator>" +
                                "</Item></AML>";

                            inn.applyAML(aml);

                        }
                        else if (mode == "line")
                        {
                            string keyed_name = itmUser.getProperty("keyed_name", "");

                            string postUrl = "http://act.innosoft.com.tw/notify/LineNotifySend.aspx?site=plmcta"
                                + "&identitys=" + keyed_name
                                + "&msg=" + body_htmlTxt
                                + "&action=send";


                            string json = "";

                            HttpWebRequest hwrSendRequest = (HttpWebRequest)WebRequest.Create(postUrl);
                            hwrSendRequest.Method = "POST";
                            hwrSendRequest.ContentType = "application/json";

                            //string cdnoReqSoap=strSoapSkeleton.Replace("{#jsoncontent}",strReqJSON);
                            WebResponse wrToDoResponse;
                            Stream stmReqStream = hwrSendRequest.GetRequestStream();
                            byte[] byarRequestBody = Encoding.UTF8.GetBytes(json);
                            stmReqStream.Write(byarRequestBody, 0, byarRequestBody.Length);
                            string strResponse;

                            wrToDoResponse = hwrSendRequest.GetResponse();
                            StreamReader srResponse = new StreamReader(wrToDoResponse.GetResponseStream());
                            strResponse = srResponse.ReadToEnd();

                            //新增
                            aml = "<AML><Item type='In_Resume_NoticeRecord' action='add'>" +
                                "<source_id>" + resumeID + "</source_id>" +
                                "<" + meetingType.ToLower() + ">" + meeting_id + "</" + meetingType.ToLower() + ">" +
                                "<in_notice_title>" + in_notice_title + "</in_notice_title>" +
                                "<in_message>" + in_message + "</in_message>" +
                                "<in_notice_date_s>" + System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss") + "</in_notice_date_s>" +
                                "<in_notice_mode>" + mode + "</in_notice_mode>" +
                                "<in_notice_status>Y</in_notice_status>" +
                                "<in_creator_sno>" + loginID + "</in_creator_sno>" +
                                "<in_creator>" + loginName + "</in_creator>" +
                                "</Item></AML>";

                            inn.applyAML(aml);

                        }
                    }
                    catch (Exception ex)
                    {
                        CCO.Utilities.WriteDebug(strMethodName, ex.Message);
                    }

                }

            }

            return inn.newResult("ok");
        }
    }
}