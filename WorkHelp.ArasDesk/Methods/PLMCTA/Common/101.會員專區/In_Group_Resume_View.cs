﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using System.Xml;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_Group_Resume_View : Item
    {
        public In_Group_Resume_View(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 道館社團資料
                日誌: 
                    - 2024-04-26 保存時暫時會員轉合格會員，建立繳費單-年費+會費
                    - 2022-08-30 個人資料修改同步至與會者資料 (lina)
                    - 2020-12-29 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Group_Resume_View";
            string strLogFileName = "[" + strDatabaseName + "]user_event_" + System.DateTime.Now.ToString("yyyy-MM-dd");

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strLogFileName = strLogFileName,
                inn = inn,
                strUserId = inn.getUserID(),
                page = itmR.getProperty("page", "").ToLower(),
                scene = itmR.getProperty("scene", ""),
                member_type = itmR.getProperty("member_type", ""),

            };

            //取得登入者資訊
            cfg.itmLoginResume = inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = '" + cfg.strUserId + "'");
            if (IsError(cfg.itmLoginResume))
            {
                throw new Exception("登入者履歷異常");
            }

            //權限檢核
            cfg.itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            cfg.isMeetingAdmin = cfg.itmPermit.getProperty("isMeetingAdmin", "") == "1";

            //登入者 resume id
            cfg.login_resume_id = cfg.itmLoginResume.getID();

            //要檢視的對象 Resume
            cfg.itmResumeView = GetTargetResume(cfg.CCO, cfg.strMethodName, cfg.inn, cfg.itmLoginResume, itmR);
            cfg.view_resume_id = cfg.itmResumeView.getProperty("id", "");
            cfg.view_resume_sno = cfg.itmResumeView.getProperty("in_sno", "");
            cfg.view_resume_meeting = cfg.itmResumeView.getProperty("in_meeting", "");
            cfg.view_is_org = cfg.itmResumeView.getProperty("in_org", "");

            //成員類型
            cfg.view_member_type = cfg.itmResumeView.getProperty("in_member_type", "");
            cfg.view_member_role = cfg.itmResumeView.getProperty("in_member_role", "");
            cfg.view_manager_name = cfg.itmResumeView.getProperty("in_name", "");

            if (!cfg.isMeetingAdmin)
            {
                bool is_manage_section = IsManageSection(cfg);
                if (is_manage_section) cfg.isMeetingAdmin = true;
            }

            //2022-02-08 只有 isMeetingAdmin=true 才能修改的部分
            if (cfg.isMeetingAdmin)
            {
                itmR.setProperty("hide_permission_btn", "");
                itmR.setProperty("hide_card_box", "item_show_1");
            }
            else
            {
                itmR.setProperty("hide_permission_btn", "item_show_0");
                itmR.setProperty("hide_card_box", "item_show_0");
            }

            switch (cfg.scene)
            {
                case "save":
                    Save(cfg, itmR);
                    break;
                case "verify":
                    Query(cfg, itmR);
                    if (cfg.isMeetingAdmin)
                    {
                        SetNewLoginName(cfg, itmR);
                        itmR.setProperty("hide_verify_btn", "");
                    }
                    else
                    {
                        itmR.setProperty("hide_verify_btn", "item_show_0");
                    }
                    break;

                case "agree":
                    AgreeApply(cfg, itmR);
                    CreatePayment(cfg, itmR);
                    break;

                case "reject":
                    RejectApply(cfg, itmR);
                    break;

                default:
                    Query(cfg, itmR);
                    itmR.setProperty("hide_verify_btn", "item_show_0");
                    AddEnableUser(cfg, itmR);
                    break;
            }

            string in_note = itmR.getProperty("in_note", "").Trim();
            if (in_note == "")
            {
                itmR.setProperty("in_note", "&nbsp;");
            }

            return itmR;
        }
        //建立繳費單-年費+會費
        private Item CreatePaymentByResume(Item itmOldResume)
        {
            itmOldResume.setProperty("MixPayYear", "1"); //合併年費與入會費
            return itmOldResume.apply("In_Payment_Resume");
        }
        //啟用帳號
        private void AddEnableUser(TConfig cfg, Item itmReturn)
        {
            if (!cfg.isMeetingAdmin) return;

            string resume_enable_btn = " ";
            string in_is_teacher = cfg.itmResumeView.getProperty("in_is_teacher", "");

            if (in_is_teacher != "1")
            {
                resume_enable_btn = "<button class='btn btn-success pull-right' style='margin-right: 5px' data-rid='" + cfg.view_resume_id + "'"
                    + " onclick='ResumeEnable_Click(this)'>啟用</button>";
            }

            itmReturn.setProperty("resume_enable_btn", resume_enable_btn);
        }

        //取得新帳號
        private void SetNewLoginName(TConfig cfg, Item itmReturn)
        {
            var sql = "";
            var len = 6;
            var prefix = "";
            var first_login_name = "";
            var new_login_name = "";

            var in_member_type = itmReturn.getProperty("in_member_type", "");

            switch (in_member_type)
            {
                case "vip_gym":
                    prefix = "G"; //道館社團
                    first_login_name = "G00001";
                    sql = "SELECT MAX(login_name) AS 'max_login_name' FROM IN_RESUME WITH(NOLOCK)"
                        + " WHERE LEN(login_name) = 6 AND login_name LIKE 'G%'"
                        + " AND login_name NOT LIKE 'GRP%'";
                    break;

                case "vip_group":
                    prefix = "GRP"; //一般團體
                    first_login_name = "GRP001";
                    sql = "SELECT MAX(login_name) AS 'max_login_name' FROM IN_RESUME WITH(NOLOCK)"
                        + " WHERE LEN(login_name) = 6 AND login_name LIKE 'GRP%'";
                    break;
            }

            if (sql == "") return;

            Item itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError()) return;


            if (itmSQL.getResult() == "")
            {
                new_login_name = first_login_name;
            }
            else
            {
                len = len - prefix.Length;

                string max_login_name = itmSQL.getProperty("max_login_name", "");

                // string login_no = max_login_name.Replace(prefix, "").Trim('0');
                string login_no = Convert.ToInt32(max_login_name.Replace(prefix, "")).ToString();
                int new_login_no = GetIntVal(login_no) + 1;
                new_login_name = prefix + new_login_no.ToString().PadLeft(len, '0');

            }

            itmReturn.setProperty("inn_new_login_name", new_login_name);
        }

        //會員申請審核通過
        private void AgreeApply(TConfig cfg, Item itmReturn)
        {
            DateTime dtNow = System.DateTime.Now;

            TVerify verify = new TVerify
            {
                in_verify_result = "1",
                in_ass_ver_memo = "",
                in_verify_time = dtNow.ToString("yyyy-MM-ddTHH:mm:ss"),
                in_verify_by_id = cfg.inn.getUserAliases(),
                in_year = dtNow.Year.ToString(),
                in_member_status = "合格會員",
                is_agree = true,

                resume_id = itmReturn.getProperty("id", ""),
                resume_name = itmReturn.getProperty("in_name", ""),
                old_login_name = itmReturn.getProperty("old_login_name", ""),
                new_login_name = itmReturn.getProperty("new_login_name", ""),
                action = "通過",

            };

            ExecVerify(cfg, verify, itmReturn);
        }

        //會員申請審核未通過
        private void RejectApply(TConfig cfg, Item itmReturn)
        {
            DateTime dtNow = System.DateTime.Now;

            TVerify verify = new TVerify
            {
                in_verify_result = "0",
                in_ass_ver_memo = itmReturn.getProperty("in_reject_reason", ""),
                in_verify_time = dtNow.ToString("yyyy-MM-ddTHH:mm:ss"),
                in_verify_by_id = cfg.inn.getUserAliases(),
                in_year = dtNow.Year.ToString(),
                in_member_status = "審核未通過",
                is_agree = false,

                resume_id = itmReturn.getProperty("id", ""),
                resume_name = itmReturn.getProperty("in_name", ""),
                old_login_name = itmReturn.getProperty("old_login_name", ""),
                new_login_name = itmReturn.getProperty("new_login_name", ""),
                action = "未通過",
            };

            ExecVerify(cfg, verify, itmReturn);
        }

        //執行審核
        private void ExecVerify(TConfig cfg, TVerify verify, Item itmReturn)
        {
            if (!cfg.isMeetingAdmin) return;

            string ln = Environment.NewLine;

            cfg.CCO.Utilities.WriteDebug(cfg.strLogFileName, "【會員申請審核】" + verify.action
                + ln + "編號：" + verify.resume_id
                + ln + "會員：" + verify.resume_name
                + ln + "原帳號：" + verify.old_login_name
                + ln + "新帳號：" + verify.new_login_name);

            string sql = "";
            Item itmSQL = null;

            Item itmResume = cfg.inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE id = '" + verify.resume_id + "' AND in_member_status = N'暫時會員'");
            if (itmResume.isError() || itmResume.getResult() == "")
            {
                throw new Exception("查無該會員資料或已非暫時會員");
            }

            string resume_in_sno = itmResume.getProperty("in_sno", "");
            string resume_login_name = itmResume.getProperty("login_name", "");
            string resume_member_type = itmResume.getProperty("in_member_type", "");
            string resume_user_id = itmResume.getProperty("in_user_id", "");

            //單位帳號
            bool is_org = resume_member_type == "vip_gym" || resume_member_type == "vip_group";
            if (!is_org)
            {
                throw new Exception("非道館社團或一般團體");
            }

            //需新帳號
            bool need_new_id = verify.is_agree && is_org;
            if (need_new_id && verify.old_login_name != resume_login_name)
            {
                throw new Exception("該會員帳號已變更");
            }

            //檢查新帳號
            if (need_new_id)
            {
                Item itmUser = cfg.inn.applySQL("SELECT id FROM [USER] WITH(NOLOCK) WHERE login_name = '" + verify.new_login_name + "'");
                if (itmUser.isError())
                {
                    throw new Exception("檢查新帳號發生錯誤");
                }
                if (itmUser.getResult() != "")
                {
                    throw new Exception("新帳號已存在");
                }
            }

            //給予meetingAdmin權限 以使用aras更新權限
            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("MeetingAdmin");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);


            //單位註冊 meeting_id 陣列
            string[] register_meeting_ids = new string[]
            {
                "'83B87AE0033640AA8DBA7AF2CF659479'", //道館、訓練站
                "'38CAB90DF1274E048520A801948AC65C'", //學校社團
                "'5F73936711E04DC799CB02587F4FF7E0'", //一般團體
                "'249FDB244E534EB0AA66C8E9C470E930'", //個人會員
            };
            string ids = string.Join(", ", register_meeting_ids);

            Item itmE = cfg.inn.newItem("in_meeting_user", "edit");
            itmE.setAttribute("where", "source_id in (" + ids + ") and in_sno ='" + resume_in_sno + "'");
            itmE.setProperty("in_ass_ver_result", verify.in_verify_result);
            itmE.setProperty("in_ass_ver_memo", verify.in_ass_ver_memo);
            itmE.setProperty("in_ass_ver_time", verify.in_verify_time);
            itmE.setProperty("in_ass_ver_identity", verify.in_verify_by_id);
            itmSQL = itmE.apply();

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            // //更新審核結果
            // sql = "UPDATE In_Meeting_User SET"
            //     + " in_ass_ver_result = '" + verify.in_verify_result + "'"
            //     + ", in_ass_ver_memo = N'" + verify.in_ass_ver_memo + "'"
            //     + ", in_ass_ver_time = '" + verify.in_verify_time + "'"
            //     + ", in_ass_ver_identity = '" + verify.in_verify_by_id + "'"
            //     + " WHERE source_id IN (" + ids + ") AND in_sno = '" + resume_in_sno + "'";

            // itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception("審核失敗");

            if (verify.is_agree)
            {

                //in_reg_date 入會日期
                //in_apply_year 入會年度

                sql = " UPDATE In_Resume SET"
                    + "     in_member_status = N'" + verify.in_member_status + "'"
                    + "   , in_apply_year = N'" + verify.in_year + "'"
                    + "   , in_reg_date = created_on"
                    + "   , in_change_date = '" + verify.in_verify_time + "'"
                    + "   , in_source = '" + resume_in_sno + "'";
                //更新館證編號
                if (cfg.member_type == "vip_gym")
                {
                    string aml = "";
                    aml = "<resume_id>" + verify.resume_id + "</resume_id>";
                    Item itmRid = cfg.inn.applyMethod("in_get_resident_id", aml);
                    sql = sql + "   , in_resident_id = N'" + itmRid.getProperty("resident_id", "") + "'";
                }
                sql = sql + " WHERE id = '" + verify.resume_id + "'";
            }
            else
            {
                sql = " UPDATE In_Resume SET"
                    + "     in_member_status = N'" + verify.in_member_status + "'"
                    + "   , in_note = N'" + verify.in_ass_ver_memo + "'"
                    + "   , in_change_date = '" + verify.in_verify_time + "'"
                    + " WHERE id = '" + verify.resume_id + "'";
            }

            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception("更新資料失敗");

            if (need_new_id)
            {
                //變更帳號
                Item itmData = cfg.inn.newItem("In_Resume");
                itmData.setProperty("in_user_id", resume_user_id);
                itmData.setProperty("new_login_name", verify.new_login_name);

                itmSQL = itmData.apply("In_UpdateUser_Identity_card");
                if (itmSQL.isError()) throw new Exception("修改登入帳號失敗");
            }
        }

        private class TVerify
        {
            public string in_verify_result { get; set; }
            public string in_ass_ver_memo { get; set; }
            public string in_verify_time { get; set; }
            public string in_verify_by_id { get; set; }
            public string in_year { get; set; }
            public string in_member_status { get; set; }
            public bool is_agree { get; set; }
            public string resume_id { get; set; }
            public string resume_name { get; set; }
            public string old_login_name { get; set; }
            public string new_login_name { get; set; }
            public string action { get; set; }
        }

        //是否為管理單位
        private bool IsManageSection(TConfig cfg)
        {
            //lina 2022.01.07: 協會要求開放晉段組管理會員權限
            //lina 2022.06.27: 協會要求開放裁判組、訓練組管理個人會員權限

            string login_name = cfg.itmLoginResume.getProperty("login_name", "");
            string idt_names = "ACT_ASC_Secretary,ACT_ASC_Chairman";
            switch (cfg.view_member_type)
            {
                case "u_gym":
                case "vip_gym"://道館社團
                    idt_names = "ACT_ASC_Degree,CT_ASC_Admin,ACT_ASC_Secretary,ACT_ASC_Chairman";
                    break;
                case "vip_group"://一般團體
                    idt_names = "ACT_ASC_Degree,CT_ASC_Admin,ACT_ASC_Secretary,ACT_ASC_Chairman";
                    break;

                case "reg":
                case "u_mbr":
                case "vip_mbr":
                case "vip_minority"://人員資料 (財務、晉段、裁判、訓練、行政、正副秘書長、理事長)
                    idt_names = "ACT_ASC_Accounting,ACT_ASC_Degree,ACT_ASC_Referee,ACT_ASC_Training,ACT_ASC_Admin,ACT_ASC_Secretary,ACT_ASC_Chairman";
                    break;

                default://其他資料 (財務、晉段、裁判、訓練、行政、正副秘書長、理事長)
                    idt_names = "ACT_ASC_Accounting,ACT_ASC_Degree,ACT_ASC_Referee,ACT_ASC_Training,ACT_ASC_Admin,ACT_ASC_Secretary,ACT_ASC_Chairman";
                    break;
            }

            Item itmRoleResult = cfg.inn.applyMethod("In_Association_Role"
                , "<in_sno>" + login_name + "</in_sno>"
                + "<idt_names>" + idt_names + "</idt_names>");

            return itmRoleResult.getProperty("inn_result", "") == "1";
        }

        // 建立繳費單
        private Item CreatePayment(TConfig cfg, Item itmReturn)
        {
            string id = itmReturn.getProperty("id", "");

            Item itmOldResume = cfg.inn.newItem("In_Resume", "get");
            itmOldResume.setProperty("id", id);
            itmOldResume = itmOldResume.apply();

            if (itmOldResume.isError())
            {
                throw new Exception("講師履歷資料存取發生錯誤");
            }
            bool need_create_payment = false;
            string in_member_status = itmOldResume.getProperty("in_member_status", "");
            string change_in_member_status = itmReturn.getProperty("in_member_status", "");
            //2024-04-26 Panda 暫時會員 轉 合格會員 產生繳費單

            Item payment = CreatePaymentByResume(itmOldResume);
            if (payment.isError())
            {
                InnSport.Core.Logging.TLog.Watch(message: "產生年費、會費繳費單錯誤");
            }
            return payment;

        }
        #region 儲存

        //儲存
        private void Save(TConfig cfg, Item itmReturn)
        {
            string login_name = itmReturn.getProperty("in_sno", "");

            Item itmOldResume = cfg.inn.newItem("In_Resume", "get");
            itmOldResume.setProperty("login_name", login_name);
            itmOldResume = itmOldResume.apply();

            if (itmOldResume.isError())
            {
                throw new Exception("講師履歷資料存取發生錯誤");
            }

            string resume_id = itmOldResume.getProperty("id", "");
            string resume_sno = itmOldResume.getProperty("in_sno", "");
            string in_member_type = itmOldResume.getProperty("in_member_type", "");
            string in_member_role = itmOldResume.getProperty("in_member_role", "");
            string login_member_type = cfg.itmLoginResume.getProperty("in_member_type", "");
            string in_member_status = itmOldResume.getProperty("in_member_status", "");
            string change_in_member_status = itmReturn.getProperty("in_member_status", "");
            Item itmUpdResume = cfg.inn.newItem("In_Resume");
            itmUpdResume.setAttribute("where", "[In_Resume].id='" + resume_id + "'");
            bool need_create_payment = false;
            bool need_upd_muser = false;

            //2024-04-26 Panda 暫時會員 轉 合格會員 產生繳費單
            if (in_member_status == "暫時會員" && change_in_member_status == "合格會員")
            {
                need_create_payment = true;
            }
            switch (in_member_type)
            {
                case "reg":
                case "mbr":
                case "vip_mbr":
                case "vip_minority":
                    SetMemberResume(cfg, itmUpdResume, itmOldResume, itmReturn, login_member_type);
                    //2024-04-26 add by panda 新增繳費單
                    need_upd_muser = true;
                    break;

                case "vip_group":
                case "gym":
                case "vip_gym":
                case "schl":
                case "vip_schl":
                    SetGymResume(cfg.CCO, cfg.strMethodName, cfg.inn, itmUpdResume, itmOldResume, itmReturn, login_member_type);
                    //2024-04-26 add by panda 新增繳費單
                    break;

                case "area_cmt":
                case "prjt_cmt":
                    if (in_member_role == "cmt_8100" || in_member_role == "cmt_8200")
                    {
                        SetCommitteeStaffResume(cfg.CCO, cfg.strMethodName, cfg.inn, itmUpdResume, itmOldResume, itmReturn);
                    }
                    else
                    {
                        SetCommitteeResume(cfg.CCO, cfg.strMethodName, cfg.inn, itmUpdResume, itmOldResume, itmReturn, login_member_type);
                    }
                    break;

                case "asc":
                    if (in_member_role == "sys_9999")
                    {
                        SetAscResume(cfg.CCO, cfg.strMethodName, cfg.inn, itmUpdResume, itmOldResume, itmReturn);
                    }
                    else
                    {
                        SetMemberResume(cfg, itmUpdResume, itmOldResume, itmReturn, login_member_type);
                        need_upd_muser = true;
                    }
                    break;
                case "other":
                case "sys":
                default:
                    //SetGymResume(cfg.CCO, cfg.strMethodName, cfg.inn, itmUpdResume, itmOldResume, itmReturn, login_member_type);
                    SetMemberResume(cfg, itmUpdResume, itmOldResume, itmReturn, login_member_type);
                    need_upd_muser = true;
                    break;
            }

            itmUpdResume = itmUpdResume.apply("merge");
            if (itmUpdResume.isError())
            {
                throw new Exception("講師履歷資料更新發生錯誤");
            }
            else
            {
                string modify_sno = cfg.itmLoginResume.getProperty("in_sno", "");
                string target_sno = itmOldResume.getProperty("in_sno", "");

                StringBuilder builer = new StringBuilder();
                builer.AppendLine("[" + cfg.strDatabaseName + "]" + modify_sno + " 變更 " + target_sno + " 資料：");
                builer.AppendLine("----- OLD -----");
                builer.AppendLine(itmOldResume.dom.InnerXml);
                builer.AppendLine("----- New -----");
                builer.AppendLine(itmUpdResume.dom.InnerXml);

                InnSport.Core.Logging.TLog.Watch(message: builer.ToString());

                if (need_upd_muser)
                {
                    //個人資料修改同步與會者資料(限定協會管理者)
                    UpdateMeetingUser(cfg, resume_id, resume_sno);
                }
                if (need_create_payment == true)
                {
                    Item payment = CreatePaymentByResume(itmUpdResume);
                    if (payment.isError())
                    {
                        InnSport.Core.Logging.TLog.Watch(message: "產生年費、會費繳費單錯誤");
                    }
                }
            }
        }

        //縣市委員會成員
        private void SetCommitteeStaffResume(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmUpdResume, Item itmOldResume, Item itmReturn)
        {
            itmUpdResume.setProperty("in_name", itmReturn.getProperty("in_name", ""));
            itmUpdResume.setProperty("in_title", itmReturn.getProperty("in_title", ""));
            itmUpdResume.setProperty("in_manager_area", itmReturn.getProperty("in_manager_area", ""));
            itmUpdResume.setProperty("in_tel_1", itmReturn.getProperty("in_tel_1", ""));
            itmUpdResume.setProperty("in_tel_2", itmReturn.getProperty("in_tel_2", ""));
            itmUpdResume.setProperty("in_tel", itmReturn.getProperty("in_tel", ""));
            itmUpdResume.setProperty("in_add_code", itmReturn.getProperty("in_add_code", ""));
            itmUpdResume.setProperty("in_add", itmReturn.getProperty("in_add", ""));
            itmUpdResume.setProperty("in_email", itmReturn.getProperty("in_email", ""));
            itmUpdResume.setProperty("in_note", itmReturn.getProperty("in_note", ""));
        }

        //縣市委員會
        private void SetCommitteeResume(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmUpdResume, Item itmOldResume, Item itmReturn, string member_type)
        {
            string oldEmail = itmOldResume.getProperty("in_email", "");
            if (oldEmail == "無" || oldEmail == "" || oldEmail == "na@na.n")
            {
                itmUpdResume.setProperty("in_email", itmReturn.getProperty("in_email", ""));
            }

            itmUpdResume.setProperty("in_emrg_contact1", itmReturn.getProperty("in_emrg_contact1", ""));
            itmUpdResume.setProperty("in_emrg_tel1", itmReturn.getProperty("in_emrg_tel1", ""));
            itmUpdResume.setProperty("in_tel", itmReturn.getProperty("in_tel", ""));

            if (member_type == "asc")
            {
                itmUpdResume.setProperty("in_name", itmReturn.getProperty("in_name", ""));
                itmUpdResume.setProperty("in_head_coach", itmReturn.getProperty("in_head_coach", ""));
                itmUpdResume.setProperty("in_principal", itmReturn.getProperty("in_principal", ""));
                itmUpdResume.setProperty("in_tel_1", itmReturn.getProperty("in_tel_1", ""));
                itmUpdResume.setProperty("in_tel_2", itmReturn.getProperty("in_tel_2", ""));

                itmUpdResume.setProperty("in_fax", itmReturn.getProperty("in_fax", ""));

                itmUpdResume.setProperty("in_add_code", itmReturn.getProperty("in_add_code", ""));
                itmUpdResume.setProperty("in_add", itmReturn.getProperty("in_add", ""));
                itmUpdResume.setProperty("in_url", itmReturn.getProperty("in_url", ""));
                itmUpdResume.setProperty("in_email", itmReturn.getProperty("in_email", ""));

                itmUpdResume.setProperty("in_note", itmReturn.getProperty("in_note", ""));
                itmUpdResume.setProperty("in_email_exam", itmReturn.getProperty("in_email_exam", ""));
                itmUpdResume.setProperty("in_manager_area", itmReturn.getProperty("in_manager_area", ""));
                itmUpdResume.setProperty("in_member_status", itmReturn.getProperty("in_member_status", ""));
                itmUpdResume.setProperty("in_member_last_year", itmReturn.getProperty("in_member_last_year", ""));

                string in_reg_date = itmReturn.getProperty("in_reg_date", "");
                if (in_reg_date != "")
                {
                    string upd_reg_date = GetDateTimeByFormat(in_reg_date, "yyyy-MM-ddTHH:mm:ss");
                    itmUpdResume.setProperty("in_reg_date", upd_reg_date);
                    if (upd_reg_date != "" && upd_reg_date.Length > 4)
                    {
                        //入會年度
                        itmUpdResume.setProperty("in_apply_year", upd_reg_date.Substring(0, 4));
                    }
                }

                string in_change_date = itmReturn.getProperty("in_change_date", "");
                if (in_change_date != "")
                {
                    itmUpdResume.setProperty("in_change_date", GetDateTimeByFormat(in_change_date, "yyyy-MM-ddTHH:mm:ss"));
                }
            }
        }

        //協會
        private void SetAscResume(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmUpdResume, Item itmOldResume, Item itmReturn)
        {
            itmUpdResume.setProperty("in_name", itmReturn.getProperty("in_name", ""));
            itmUpdResume.setProperty("in_principal", itmReturn.getProperty("in_principal", ""));
            itmUpdResume.setProperty("in_head_coach", itmReturn.getProperty("in_head_coach", ""));
            itmUpdResume.setProperty("in_assistant_coaches", itmReturn.getProperty("in_assistant_coaches", ""));
            itmUpdResume.setProperty("in_tel_1", itmReturn.getProperty("in_tel_1", ""));
            itmUpdResume.setProperty("in_tel_2", itmReturn.getProperty("in_tel_2", ""));
            itmUpdResume.setProperty("in_fax", itmReturn.getProperty("in_fax", ""));
            itmUpdResume.setProperty("in_tel", itmReturn.getProperty("in_tel", ""));
            itmUpdResume.setProperty("in_add_code", itmReturn.getProperty("in_add_code", ""));
            itmUpdResume.setProperty("in_add", itmReturn.getProperty("in_add", ""));
            itmUpdResume.setProperty("in_url", itmReturn.getProperty("in_url", ""));
            itmUpdResume.setProperty("in_email", itmReturn.getProperty("in_email", ""));
            itmUpdResume.setProperty("in_note", itmReturn.getProperty("in_note", ""));
        }

        //個人會員
        private void SetMemberResume(TConfig cfg, Item itmUpdResume, Item itmOldResume, Item itmReturn, string member_type)
        {
            string oldEmail = itmOldResume.getProperty("in_email", "");
            if (oldEmail == "無" || oldEmail == "" || oldEmail == "na@na.n")
            {
                itmUpdResume.setProperty("in_email", itmReturn.getProperty("in_email", ""));
            }

            itmUpdResume.setProperty("in_area", itmReturn.getProperty("in_area", ""));
            itmUpdResume.setProperty("in_country", itmReturn.getProperty("in_country", ""));
            itmUpdResume.setProperty("in_guardian", itmReturn.getProperty("in_guardian", ""));
            itmUpdResume.setProperty("in_blood_type", itmReturn.getProperty("in_blood_type", ""));

            itmUpdResume.setProperty("in_education", itmReturn.getProperty("in_education", ""));
            itmUpdResume.setProperty("in_work_history", itmReturn.getProperty("in_work_history", ""));
            itmUpdResume.setProperty("in_work_org", itmReturn.getProperty("in_work_org", ""));
            itmUpdResume.setProperty("in_title", itmReturn.getProperty("in_title", ""));
            itmUpdResume.setProperty("in_add_code", itmReturn.getProperty("in_add_code", ""));
            itmUpdResume.setProperty("in_add", itmReturn.getProperty("in_add", ""));

            itmUpdResume.setProperty("in_emrg_contact1", itmReturn.getProperty("in_emrg_contact1", ""));
            itmUpdResume.setProperty("in_emrg_tel1", itmReturn.getProperty("in_emrg_tel1", ""));

            if (cfg.isMeetingAdmin)
            {
                itmUpdResume.setProperty("in_name", itmReturn.getProperty("in_name", ""));
                itmUpdResume.setProperty("in_en_name", itmReturn.getProperty("in_en_name", ""));
                itmUpdResume.setProperty("in_resident_add_code", itmReturn.getProperty("in_resident_add_code", ""));
                itmUpdResume.setProperty("in_resident_add", itmReturn.getProperty("in_resident_add", ""));
                itmUpdResume.setProperty("in_tel_1", itmReturn.getProperty("in_tel_1", ""));
                itmUpdResume.setProperty("in_tel", itmReturn.getProperty("in_tel", ""));
                itmUpdResume.setProperty("in_email", itmReturn.getProperty("in_email", ""));
                itmUpdResume.setProperty("in_gender", itmReturn.getProperty("in_gender", ""));

                //所屬單位
                itmUpdResume.setProperty("in_current_org", itmReturn.getProperty("in_current_org", ""));

                //所屬委員會
                string in_manager_org = itmReturn.getProperty("in_manager_org", "");
                string in_manager_name = itmReturn.getProperty("in_manager_name", "");
                if (in_manager_org != "")
                {
                    Item org_resume = cfg.inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE id = '" + in_manager_org + "'");
                    if (!org_resume.isError() && org_resume.getResult() != "")
                    {
                        itmUpdResume.setProperty("in_manager_org", org_resume.getProperty("id"));
                        itmUpdResume.setProperty("in_manager_name", org_resume.getProperty("in_name"));
                        itmUpdResume.setProperty("in_manager_area", org_resume.getProperty("in_manager_area"));
                    }
                }
                else
                {
                    itmUpdResume.setProperty("in_manager_name", in_manager_name);
                }


                itmUpdResume.setProperty("in_degree", itmReturn.getProperty("in_degree", ""));
                itmUpdResume.setProperty("in_degree_id", itmReturn.getProperty("in_degree_id", ""));
                itmUpdResume.setProperty("in_degree_date", itmReturn.getProperty("in_degree_date", "").Replace("/", "-"));
                itmUpdResume.setProperty("in_gl_degree", itmReturn.getProperty("in_gl_degree", ""));
                itmUpdResume.setProperty("in_gl_degree_id", itmReturn.getProperty("in_gl_degree_id", ""));
                itmUpdResume.setProperty("in_gl_degree_date", itmReturn.getProperty("in_gl_degree_date", "").Replace("/", "-"));

                itmUpdResume.setProperty("in_instructor_level", itmReturn.getProperty("in_instructor_level", ""));
                itmUpdResume.setProperty("in_instructor_id", itmReturn.getProperty("in_instructor_id", ""));
                itmUpdResume.setProperty("in_instructor_date", itmReturn.getProperty("in_instructor_date", "").Replace("/", "-"));
                itmUpdResume.setProperty("in_gl_instructor_level", itmReturn.getProperty("in_gl_instructor_level", ""));
                itmUpdResume.setProperty("in_gl_instructor_id", itmReturn.getProperty("in_gl_instructor_id", ""));
                itmUpdResume.setProperty("in_gl_instructor_date", itmReturn.getProperty("in_gl_instructor_date", "").Replace("/", "-"));
                /*
                itmUpdResume.setProperty("in_instructor_level", itmReturn.getProperty("in_instructor_level", ""));
                itmUpdResume.setProperty("in_instructor_id", itmReturn.getProperty("in_instructor_id", ""));
                itmUpdResume.setProperty("in_instructor_date", itmReturn.getProperty("in_instructor_date", "").Replace("/", "-"));      */
                itmUpdResume.setProperty("in_gl_para_instructor_level", itmReturn.getProperty("in_gl_para_instructor_level", ""));
                itmUpdResume.setProperty("in_gl_para_instructor_id", itmReturn.getProperty("in_gl_para_instructor_id", ""));
                itmUpdResume.setProperty("in_gl_para_instructor_date", itmReturn.getProperty("in_gl_para_instructor_date", "").Replace("/", "-"));

                itmUpdResume.setProperty("in_referee_level", itmReturn.getProperty("in_referee_level", ""));
                itmUpdResume.setProperty("in_referee_id", itmReturn.getProperty("in_referee_id", ""));
                itmUpdResume.setProperty("in_referee_date", itmReturn.getProperty("in_referee_date", "").Replace("/", "-"));
                itmUpdResume.setProperty("in_gl_referee_level", itmReturn.getProperty("in_gl_referee_level", ""));
                itmUpdResume.setProperty("in_gl_referee_id", itmReturn.getProperty("in_gl_referee_id", ""));
                itmUpdResume.setProperty("in_gl_referee_date", itmReturn.getProperty("in_gl_referee_date", "").Replace("/", "-"));

                itmUpdResume.setProperty("in_poomsae_level", itmReturn.getProperty("in_poomsae_level", ""));
                itmUpdResume.setProperty("in_poomsae_id", itmReturn.getProperty("in_poomsae_id", ""));
                itmUpdResume.setProperty("in_poomsae_date", itmReturn.getProperty("in_poomsae_date", "").Replace("/", "-"));
                itmUpdResume.setProperty("in_gl_poomsae_level", itmReturn.getProperty("in_gl_poomsae_level", ""));
                itmUpdResume.setProperty("in_gl_poomsae_id", itmReturn.getProperty("in_gl_poomsae_id", ""));
                itmUpdResume.setProperty("in_gl_poomsae_date", itmReturn.getProperty("in_gl_poomsae_date", "").Replace("/", "-"));

                itmUpdResume.setProperty("in_stuff_c1", itmReturn.getProperty("in_stuff_c1", ""));
                itmUpdResume.setProperty("in_stuff_c2", itmReturn.getProperty("in_stuff_c2", ""));

                itmUpdResume.setProperty("in_note", itmReturn.getProperty("in_note", ""));
                itmUpdResume.setProperty("in_email_exam", itmReturn.getProperty("in_email_exam", ""));
                itmUpdResume.setProperty("in_member_status", itmReturn.getProperty("in_member_status", ""));
                itmUpdResume.setProperty("in_member_last_year", itmReturn.getProperty("in_member_last_year", ""));

                string in_birth = itmReturn.getProperty("in_birth", "");
                if (in_birth != "")
                {
                    itmUpdResume.setProperty("in_birth", GetDateTimeByFormat(in_birth, "yyyy-MM-ddTHH:mm:ss"));
                }

                string in_reg_date = itmReturn.getProperty("in_reg_date", "");
                if (in_reg_date != "")
                {
                    string upd_reg_date = GetDateTimeByFormat(in_reg_date, "yyyy-MM-ddTHH:mm:ss");
                    itmUpdResume.setProperty("in_reg_date", upd_reg_date);
                    if (upd_reg_date != "" && upd_reg_date.Length > 4)
                    {
                        //入會年度
                        itmUpdResume.setProperty("in_apply_year", upd_reg_date.Substring(0, 4));
                    }
                }

                string in_change_date = itmReturn.getProperty("in_change_date", "");
                if (in_change_date != "")
                {
                    itmUpdResume.setProperty("in_change_date", GetDateTimeByFormat(in_change_date, "yyyy-MM-ddTHH:mm:ss"));
                }
            }
        }

        //個人資料修改同步與會者資料(限定協會管理者)
        private void UpdateMeetingUser(TConfig cfg, string resume_id, string resume_sno)
        {
            if (!cfg.isMeetingAdmin) return;

            Item itmResume = cfg.inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE id = '" + resume_id + "'");
            if (itmResume.isError() || itmResume.getResult() == "")
            {
                throw new Exception("查無講師履歷資料");
            }

            //比賽-基本資料欄位
            UppMUserStep1(cfg, resume_id, "IN_MEETING", "IN_MEETING_USER");
            //講習-基本資料欄位
            UppMUserStep1(cfg, resume_id, "IN_CLA_MEETING", "IN_CLA_MEETING_USER");


            //講習-證照欄位
            UppMUserStep2(cfg, itmResume, "IN_CLA_MEETING", "IN_CLA_MEETING_USER", "IN_CLA_MEETING_SURVEYS", "IN_CLA_SURVEY");

        }

        private void UppMUserStep2(TConfig cfg, Item itmResume, string tb_meeting, string tb_muser, string tb_msvy, string tb_svy)
        {
            string sql = @"
                SELECT 
	                t1.id            AS 'meeting_id'
	                , t1.in_title
	                , t1.in_meeting_type
	                , t1.in_seminar_type
	                , t2.id          AS 'muid'
	                , t2.in_l1
	                , t2.in_date
	                , t2.in_degree_id
	                , t2.in_gl_degree_id
	                , t2.in_exe_a1
	                , t2.in_exe_a2
	                , t2.in_exe_a8
	                , t2.in_referee_id
	                , t2.in_stuff_c5
                FROM
	                {#tb_meeting} t1 WITH(NOLOCK)
                INNER JOIN
	                {#tb_muser} t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t1.in_close_msg = N'開放報名中'
	                AND t2.in_sno = '{#resume_sno}'
            ";

            sql = sql.Replace("{#tb_meeting}", tb_meeting)
                .Replace("{#tb_muser}", tb_muser)
                .Replace("{#resume_sno}", itmResume.getProperty("in_sno", ""));

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);


            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string meeting_id = item.getProperty("meeting_id");
                string in_meeting_type = item.getProperty("in_meeting_type");
                switch (in_meeting_type)
                {
                    case "seminar":
                        UpdMUserSeminar(cfg, itmResume, item, tb_muser, tb_msvy, tb_svy);
                        break;

                    case "degree":
                        UpdMUserDegree(cfg, itmResume, item, tb_muser);
                        break;
                }
            }
        }

        private void UpdMUserDegree(TConfig cfg, Item itmResume, Item itmMUser, string tb_muser)
        {
            string muid = itmMUser.getProperty("muid");
            string in_l1 = itmMUser.getProperty("in_l1");

            string mu_exe_a1 = itmMUser.getProperty("in_exe_a1");//級證號碼
            string mu_exe_a2 = itmMUser.getProperty("in_exe_a2");//八級晉升日期

            string mu_degree_id = itmMUser.getProperty("in_degree_id");//國內段證號
            string mu_date = itmMUser.getProperty("in_date");//晉升日期
            string mu_gl_degree_id = itmMUser.getProperty("in_gl_degree_id");//國際段證號

            string rm_degree_id = itmResume.getProperty("in_degree_id", "");
            string rm_degree_date = itmResume.getProperty("in_degree_date", "");
            string rm_gl_degree_id = itmResume.getProperty("in_gl_degree_id", "");
            string rm_gl_degree_date = itmResume.getProperty("in_gl_degree_date", "");

            //將講師履歷資料覆蓋與會者
            var new_exe_a1 = rm_degree_id;
            var new_exe_a2 = GetDateTimeValue(rm_degree_date, "yyyy/MM/dd", 8);
            if (new_exe_a1 == "") new_exe_a1 = mu_exe_a1;
            if (new_exe_a2 == "") new_exe_a2 = mu_exe_a2;

            var new_degree_id = rm_degree_id;
            var new_degree_date = GetDateTimeValue(rm_degree_date, "yyyy/MM/dd", 8);
            var new_gl_degree_id = rm_gl_degree_id;
            if (new_degree_id == "") new_degree_id = mu_degree_id;
            if (new_degree_date == "") new_degree_date = mu_date;
            if (new_gl_degree_id == "") new_gl_degree_id = mu_gl_degree_id;

            string sql = "";
            switch (in_l1)
            {
                // case "壹段"://升一段，更新[級證號碼](in_exe_a1)、[八級晉升日期](in_exe_a2)
                //     sql = "UPDATE " + tb_muser + " SET"
                //         + "  in_exe_a1 = '" + new_exe_a1 + "'"
                //         + ", in_exe_a2 = '" + new_exe_a2 + "'"
                //         + " WHERE id = '" + muid + "'"
                //         ;
                //     break;

                default:
                    sql = "UPDATE " + tb_muser + " SET"
                        + "  in_degree_id = '" + new_degree_id + "'"
                        + ", in_date = '" + new_degree_date + "'"
                        + ", in_gl_degree_id = '" + new_gl_degree_id + "'"
                        + " WHERE id = '" + muid + "'"
                        ;
                    break;
            }

            cfg.inn.applySQL(sql);

        }

        private void UpdMUserSeminar(TConfig cfg, Item itmResume, Item itmMUser, string tb_muser, string tb_msvy, string tb_svy)
        {
            string meeting_id = itmMUser.getProperty("meeting_id");

            string cert_id_prop = "in_exe_a2";
            string cert_dt_prop = "in_exe_a8";
            string cert_referee = "in_referee_id";
            string cert_coach = "in_stuff_c5";

            string sql = @"
                    SELECT 
	                    in_property
	                    , in_questions
                    FROM
	                    {#table1} t1 WITH(NOLOCK)
                    INNER JOIN
	                    {#table2} t2 WITH(NOLOCK) 
	                    ON t2.id = t1.related_id
                    WHERE 
	                    t1.source_id = '{#meeting_id}'
	                    AND t2.in_property IN ('in_exe_a2', 'in_exe_a8', 'in_referee_id', 'in_stuff_c5')
                ";

            sql = sql.Replace("{#table1}", tb_msvy)
                .Replace("{#table2}", tb_svy)
                .Replace("{#meeting_id}", meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql2);

            Item itmSvys = cfg.inn.applySQL(sql);
            int count = itmSvys.getItemCount();

            bool need_update1 = false;
            bool need_update2 = false;
            bool need_update3 = false;
            bool need_update4 = false;

            for (int i = 0; i < count; i++)
            {
                Item itmSvy = itmSvys.getItemByIndex(i);
                string in_property = itmSvy.getProperty("in_property", "");
                string in_questions = itmSvy.getProperty("in_questions", "");

                if (in_property == cert_dt_prop && in_questions.Contains("發證日期"))
                {
                    need_update1 = true;
                }

                if (in_property == cert_id_prop && in_questions.Contains("證號"))
                {
                    need_update2 = true;
                }

                if (in_property == cert_referee)
                {
                    need_update3 = true;
                }

                if (in_property == cert_coach)
                {
                    need_update4 = true;
                }
            }

            var rsetting = MapResumeProps(cfg, itmMUser);
            var mu_cert_id_prop = cert_id_prop; //in_exe_a2

            if (need_update3)
            {
                mu_cert_id_prop = cert_referee; //in_referee_id
                need_update2 = true;
            }
            if (need_update4)
            {
                mu_cert_id_prop = cert_coach; //in_stuff_c5
                need_update2 = true;
            }

            if (need_update1 && need_update2)
            {
                UpdMUserCert(cfg, itmResume, itmMUser, rsetting, tb_muser, mu_cert_id_prop);
            }
        }

        private void UpdMUserCert(TConfig cfg, Item itmResume, Item itmMUser, TResumeProp rsetting, string tb_muser, string mu_cert_no_prop)
        {
            string in_exe_a2 = itmMUser.getProperty(mu_cert_no_prop, "");
            string in_exe_a8 = itmMUser.getProperty("in_exe_a8", "");

            string cert_id = itmResume.getProperty(rsetting.cert_id_pro, "");
            string cert_dt = itmResume.getProperty(rsetting.cert_dt_pro, "");

            if (cert_id == "" && in_exe_a2 != "")
            {
                //講師履歷沒有資料
                cert_id = in_exe_a2;
            }

            if (cert_dt == "" && in_exe_a8 != "")
            {
                //講師履歷沒有資料
                cert_dt = in_exe_a8;
            }

            if (cert_id == "" && cert_dt == "")
            {
                return;
            }

            cert_dt = GetDateTimeValue(cert_dt, "yyyy/MM/dd", 8);

            string sql = "UPDATE " + tb_muser + " SET"
                + "  " + mu_cert_no_prop + " = '" + cert_id + "'"
                + ", in_exe_a8 = '" + cert_dt + "'"
                + " WHERE id = '" + itmMUser.getProperty("muid", "") + "'"
                ;

            cfg.inn.applySQL(sql);
        }

        private void UppMUserStep1(TConfig cfg, string resume_id, string tb_meeting, string tb_muser)
        {
            string sql = @"
                UPDATE t1 SET 
	                t1.in_name = t3.in_name
	                , t1.in_en_name = t3.in_en_name
	                , t1.in_birth = t3.in_birth
	                , t1.in_gender = t3.in_gender
	                , t1.in_add_code = t3.in_add_code
	                , t1.in_add = t3.in_add
	                , t1.in_resident_add_code = t3.in_resident_add_code
	                , t1.in_resident_add = t3.in_resident_add
                FROM
	                {#tb_muser} t1 WITH(NOLOCK)
                INNER JOIN
	                {#tb_meeting} t2 WITH(NOLOCK)
	                ON t2.id = t1.source_id
                INNER JOIN 
	                IN_RESUME t3 WITH(NOLOCK)
	                ON t3.in_sno = t1.in_sno
                WHERE
	                t2.in_close_msg = N'開放報名中'
	                AND t3.id = '{#resume_id}'
            ";

            sql = sql.Replace("{#tb_meeting}", tb_meeting)
                .Replace("{#tb_muser}", tb_muser)
                .Replace("{#resume_id}", resume_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item itmResult = cfg.inn.applySQL(sql);

            if (itmResult.isError())
            {
                throw new Exception("個人資料修改同步與會者資料 發生錯誤");
            }
        }

        //道館社團
        private void SetGymResume(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmUpdResume, Item itmOldResume, Item itmReturn, string member_type)
        {
            string oldEmail = itmOldResume.getProperty("in_email", "");
            if (oldEmail == "無" || oldEmail == "" || oldEmail == "na@na.n")
            {
                itmUpdResume.setProperty("in_email", itmReturn.getProperty("in_email", ""));
            }

            itmUpdResume.setProperty("in_add_code", itmReturn.getProperty("in_add_code", ""));
            itmUpdResume.setProperty("in_add", itmReturn.getProperty("in_add", ""));
            itmUpdResume.setProperty("in_tel_2", itmReturn.getProperty("in_tel_2", ""));
            itmUpdResume.setProperty("in_tel", itmReturn.getProperty("in_tel", ""));

            if (member_type == "asc")
            {
                itmUpdResume.setProperty("in_name", itmReturn.getProperty("in_name", ""));
                itmUpdResume.setProperty("in_principal", itmReturn.getProperty("in_principal", ""));
                itmUpdResume.setProperty("in_head_coach", itmReturn.getProperty("in_head_coach", ""));
                itmUpdResume.setProperty("in_assistant_coaches", itmReturn.getProperty("in_assistant_coaches", ""));
                itmUpdResume.setProperty("in_resident_add_code", itmReturn.getProperty("in_resident_add_code", ""));
                itmUpdResume.setProperty("in_resident_add", itmReturn.getProperty("in_resident_add", ""));
                itmUpdResume.setProperty("in_resident_id", itmReturn.getProperty("in_resident_id", ""));
                itmUpdResume.setProperty("in_tel_1", itmReturn.getProperty("in_tel_1", ""));
                itmUpdResume.setProperty("in_fax", itmReturn.getProperty("in_fax", ""));
                itmUpdResume.setProperty("in_url", itmReturn.getProperty("in_url", ""));
                itmUpdResume.setProperty("in_email", itmReturn.getProperty("in_email", ""));
                itmUpdResume.setProperty("in_note", itmReturn.getProperty("in_note", ""));

                itmUpdResume.setProperty("in_email_exam", itmReturn.getProperty("in_email_exam", ""));
                itmUpdResume.setProperty("in_member_unit", itmReturn.getProperty("in_member_unit", ""));
                itmUpdResume.setProperty("in_member_last_year", itmReturn.getProperty("in_member_last_year", ""));
                itmUpdResume.setProperty("in_member_status", itmReturn.getProperty("in_member_status", ""));

                string in_reg_date = itmReturn.getProperty("in_reg_date", "");
                if (in_reg_date != "")
                {
                    string upd_reg_date = GetDateTimeByFormat(in_reg_date, "yyyy-MM-ddTHH:mm:ss");
                    itmUpdResume.setProperty("in_reg_date", upd_reg_date);
                    if (upd_reg_date != "" && upd_reg_date.Length > 4)
                    {
                        //入會年度
                        itmUpdResume.setProperty("in_apply_year", upd_reg_date.Substring(0, 4));
                    }
                }

                string in_change_date = itmReturn.getProperty("in_change_date", "");
                if (in_change_date != "")
                {
                    itmUpdResume.setProperty("in_change_date", GetDateTimeByFormat(in_change_date, "yyyy-MM-ddTHH:mm:ss"));
                }

                string in_resident_date = itmReturn.getProperty("in_resident_date", "");
                if (in_resident_date != "")
                {
                    string sql = "";
                    sql = @"UPDATE in_resume SET in_resident_date = '{#in_resident_date}' 
                            WHERE id ='" + itmOldResume.getProperty("id", "") + "'";
                    sql = sql.Replace("{#in_resident_date}", in_resident_date);
                    inn.applySQL(sql);
                }

                string old_manager_org = itmOldResume.getProperty("in_manager_org", "");
                string new_manager_org = itmReturn.getProperty("in_manager_org", "");

                if (old_manager_org != new_manager_org)
                {
                    string new_manager_name = "";
                    string new_manager_area = "";
                    if (new_manager_org != "")
                    {
                        Item org_resume = inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE id = '" + new_manager_org + "'");
                        if (!org_resume.isError() && org_resume.getResult() != "")
                        {
                            new_manager_name = org_resume.getProperty("in_name", "");
                            new_manager_area = org_resume.getProperty("in_manager_area", "");
                        }
                    }
                    itmUpdResume.setProperty("in_manager_org", new_manager_org);
                    itmUpdResume.setProperty("in_manager_name", new_manager_name);
                    itmUpdResume.setProperty("new_manager_area", new_manager_area);
                }

                //lina 2021.04.08 根據申請表擴充欄位
                itmUpdResume.setProperty("in_principal_tel", itmReturn.getProperty("in_principal_tel", ""));
                itmUpdResume.setProperty("in_head_coach_tel", itmReturn.getProperty("in_head_coach_tel", ""));
                itmUpdResume.setProperty("in_head_coach_sno", itmReturn.getProperty("in_head_coach_sno", ""));
                itmUpdResume.setProperty("in_degree", itmReturn.getProperty("in_degree", ""));
                itmUpdResume.setProperty("in_degree_id", itmReturn.getProperty("in_degree_id", ""));
                itmUpdResume.setProperty("in_instructor_level", itmReturn.getProperty("in_instructor_level", ""));
                itmUpdResume.setProperty("in_instructor_id", itmReturn.getProperty("in_instructor_id", ""));
                itmUpdResume.setProperty("in_aider_1", itmReturn.getProperty("in_aider_1", ""));
                itmUpdResume.setProperty("in_aider_2", itmReturn.getProperty("in_aider_2", ""));
                itmUpdResume.setProperty("in_aider_degree_1", itmReturn.getProperty("in_aider_degree_1", ""));
                itmUpdResume.setProperty("in_aider_degree_2", itmReturn.getProperty("in_aider_degree_2", ""));

                //教練生日
                string in_birth = itmReturn.getProperty("in_birth", "");
                if (in_birth != "")
                {
                    itmUpdResume.setProperty("in_birth", GetDateTimeByFormat(in_birth, "yyyy-MM-ddTHH:mm:ss"));
                }
            }
        }

        #endregion 儲存

        //查詢
        private void Query(TConfig cfg, Item itmReturn)
        {
            //複製全部 property
            var nodes = cfg.itmResumeView.node.ChildNodes;
            foreach (XmlNode node in nodes)
            {
                string property = node.Name;
                itmReturn.setProperty(property, cfg.itmResumeView.getProperty(property, ""));
            }

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_birth", format = "yyyy-MM-dd", need_clear = true, ClearValue = Clear1900 });
            fields.Add(new TField { property = "in_email", need_clear = true, ClearValue = ClearEmail });
            fields.Add(new TField { property = "in_reg_date", format = "yyyy-MM-dd", need_clear = true, ClearValue = Clear1900 });
            fields.Add(new TField { property = "in_change_date", format = "yyyy-MM-dd", need_clear = true, ClearValue = Clear1900 });
            fields.Add(new TField { property = "member_type_display", need_clear = true, ClearValue = MemberTypeDisplay });
            fields.Add(new TField { property = "in_degree_date", format = "yyyy-MM-dd", need_clear = true, ClearValue = Clear1900 });
            fields.Add(new TField { property = "in_gl_degree_date", format = "yyyy-MM-dd", need_clear = true, ClearValue = Clear1900 });
            fields.Add(new TField { property = "in_instructor_date", format = "yyyy-MM-dd", need_clear = true, ClearValue = Clear1900 });
            fields.Add(new TField { property = "in_gl_instructor_date", format = "yyyy-MM-dd", need_clear = true, ClearValue = Clear1900 });
            fields.Add(new TField { property = "in_referee_date", format = "yyyy-MM-dd", need_clear = true, ClearValue = Clear1900 });
            fields.Add(new TField { property = "in_gl_referee_date", format = "yyyy-MM-dd", need_clear = true, ClearValue = Clear1900 });
            fields.Add(new TField { property = "in_poomsae_date", format = "yyyy-MM-dd", need_clear = true, ClearValue = Clear1900 });
            fields.Add(new TField { property = "in_gl_poomsae_date", format = "yyyy-MM-dd", need_clear = true, ClearValue = Clear1900 });
            fields.Add(new TField { property = "in_card_member_date", format = "yyyy-MM-dd", need_clear = true, ClearValue = Clear1900 });

            //複寫 value
            OverrideValue(cfg, itmReturn, cfg.itmResumeView, fields);

            itmReturn.setProperty("group_id", cfg.view_resume_id);

            //跆委會選單
            Item itmCommittees = GetCommittees(cfg.CCO, cfg.strMethodName, cfg.inn);
            AppendMenu(cfg.CCO, cfg.strMethodName, cfg.inn, itmCommittees, "inn_committ", itmReturn);

            //會員狀態選單
            Item itmMStatuses = GetMStatuses(cfg.CCO, cfg.strMethodName, cfg.inn);
            AppendMenu(cfg.CCO, cfg.strMethodName, cfg.inn, itmMStatuses, "inn_mstatus", itmReturn);

            //發信狀況選單
            AppendMenu(cfg.CCO, cfg.strMethodName, cfg.inn, "In_Email_ExamStatus", "inn_estatus", itmReturn);

            //級段選單
            AppendMenu(cfg.CCO, cfg.strMethodName, cfg.inn, "In_Black_Belt_List", "inn_belts", itmReturn);

            //繳費單
            AppendMeetingPay(cfg, cfg.itmResumeView, itmReturn);

            //會費繳納現況
            AppendResumePay(cfg, cfg.view_resume_id, itmReturn);

            //個人
            if (cfg.view_is_org == "0")
            {
                //晉段記錄
                AppendResumePromotion(cfg, cfg.view_resume_id, itmReturn);

                //國內教練
                AppendResumeRelations(cfg, cfg.view_resume_id, "In_Resume_Coach", "inn_instructor", itmReturn);
                //國際教練
                AppendResumeRelations(cfg, cfg.view_resume_id, "In_Resume_Coach_Gl", "inn_instructor_1", itmReturn);

                // 國內對打裁判
                AppendResumeRelations(cfg, cfg.view_resume_id, "In_Resume_Referee", "inn_referee", itmReturn);
                // 國際對打裁判
                AppendResumeRelations(cfg, cfg.view_resume_id, "In_Resume_Referee_Gl", "inn_referee_1", itmReturn);

                // 國內品勢裁判
                AppendResumeRelations(cfg, cfg.view_resume_id, "In_Resume_Poomsae", "inn_poomsae", itmReturn);
                // 國際品勢裁判
                AppendResumeRelations(cfg, cfg.view_resume_id, "In_Resume_Poomsae_Gl", "inn_poomsae_1", itmReturn);

                //國際帕拉教練
                AppendResumeRelations(cfg, cfg.view_resume_id, "In_Resume_Coach_Para_Gl", "inn_para_instructor_1", itmReturn);

                // 國內測驗官
                AppendResumeRelations(cfg, cfg.view_resume_id, "In_Resume_Examiner", "inn_examiner", itmReturn);

                //年度記錄
                AppendResumeYears(cfg, cfg.view_resume_id, itmReturn);

                //選手成績
                AppendResumeCompetitions(cfg, cfg.view_resume_id, itmReturn);

                //講習紀錄
                AppendResumeSeminar(cfg, cfg.view_resume_id, itmReturn);

                //執法紀錄
                AppendResumeStaff(cfg, cfg.view_resume_id, itmReturn);
            }

            //證照列表
            AppendResumeCertificate(cfg, cfg.view_resume_id, itmReturn);

            //Alan 縣市委員會使用
            //所屬地區選單
            Item itmAreas = GetAreas(cfg.CCO, cfg.strMethodName, cfg.inn);
            AppendMenu(cfg.CCO, cfg.strMethodName, cfg.inn, itmAreas, "inn_Area", itmReturn);

            if (cfg.view_member_role == "sys_9999")
            {
                if (cfg.isMeetingAdmin && (cfg.view_member_type == "asc" || cfg.view_member_type == "sys"))
                {
                    //成員清單
                    AppendAscResumeRole(cfg, cfg.itmResumeView, itmReturn);
                }
                else if (cfg.view_member_type == "area_cmt" || cfg.view_member_type == "prjt_cmt")
                {
                    //道館社團會員清單
                    Item itmGyms = GetGyms(cfg.CCO, cfg.strMethodName, cfg.inn, cfg.view_manager_name);
                    AppendGymTable(cfg.CCO, cfg.strMethodName, cfg.inn, itmGyms, itmReturn);

                    //委員會成員清單
                    Item itmStaffs = GetStaffs(cfg.CCO, cfg.strMethodName, cfg.inn, cfg.view_manager_name);
                    AppendStaffTable(cfg.CCO, cfg.strMethodName, cfg.inn, itmStaffs, itmReturn);
                }
                else
                {
                    //成員清單
                    AppendResumeRole(cfg, cfg.itmResumeView, itmReturn);
                }
            }

            //儲存按鈕控制
            string login_member_type = cfg.itmLoginResume.getProperty("in_member_type", "");
            string login_member_role = cfg.itmLoginResume.getProperty("in_member_role", "");
            string login_member_email = cfg.itmLoginResume.getProperty("in_email", "");

            string email_status = "read_disabled";

            if (login_member_email == "無" || login_member_email == "" || login_member_email == "na@na.n")
            {
                email_status = "";
            }

            string asc_limit = "0";
            string hide_save_btn = "item_show_0";
            string hide_save_btn_user = "item_show_0";
            string hide_save_btn_gym = "item_show_0";
            string hide_save_btn_cmt = "item_show_0";
            string read_status = "read_disabled";
            string hide_photo_btn = "item_show_0";
            string hide_save_btn_user_asc = "item_show_0";
            string hide_save_btn_cmt_asc = "item_show_0";
            string hide_save_btn_gym_asc = "item_show_0";
            string hide_asc = "item_show_0";
            string hide_user = "item_show_0";
            string asc_pw = cfg.itmResumeView.getProperty("in_password_plain", "");

            switch (login_member_type)
            {
                case "reg":
                case "mbr":
                case "vip_mbr":
                case "vip_minority":
                    hide_save_btn_user = "";
                    hide_photo_btn = "";
                    hide_save_btn_user_asc = "";
                    hide_user = "";
                    break;

                case "vip_group":
                case "gym":
                case "vip_gym":
                case "schl":
                case "vip_schl":
                    hide_save_btn_gym = "";
                    hide_save_btn_gym_asc = "";
                    hide_user = "";
                    break;

                case "area_cmt":
                case "prjt_cmt":
                    hide_save_btn_cmt = "";
                    hide_save_btn_cmt_asc = "";
                    hide_user = "";
                    break;
                case "sys":
                case "asc":
                    if (login_member_role == "sys_9999" || cfg.isDegreeManager)
                    {
                        asc_limit = "1";
                        hide_save_btn = "";
                        read_status = "";
                        email_status = "";
                        hide_photo_btn = "";
                        hide_save_btn_user_asc = "";
                        hide_save_btn_cmt_asc = "";
                        hide_save_btn_gym_asc = "";
                        hide_asc = "";
                        itmReturn.setProperty("asc_pw", asc_pw);
                    }
                    else
                    {
                        hide_save_btn_user = "";
                        hide_user = "";
                    }
                    break;
                case "other":

                default:
                    hide_save_btn_user = "";
                    hide_photo_btn = "";
                    hide_save_btn_user_asc = "";
                    hide_user = "";
                    break;
            }

            if (cfg.isMeetingAdmin)
            {
                asc_limit = "1";
                hide_save_btn = "";
                read_status = "";
                email_status = "";
                hide_photo_btn = "";
                hide_save_btn_user_asc = "";
                hide_save_btn_cmt_asc = "";
                hide_save_btn_gym_asc = "";
                hide_asc = "";
                itmReturn.setProperty("asc_pw", asc_pw);
            }

            itmReturn.setProperty("hide_save_btn", hide_save_btn);
            itmReturn.setProperty("hide_save_btn_user", hide_save_btn_user);
            itmReturn.setProperty("hide_save_btn_gym", hide_save_btn_gym);
            itmReturn.setProperty("hide_save_btn_cmt", hide_save_btn_cmt);
            itmReturn.setProperty("hide_save_btn_user_asc", hide_save_btn_user_asc);
            itmReturn.setProperty("hide_save_btn_cmt_asc", hide_save_btn_cmt_asc);
            itmReturn.setProperty("hide_save_btn_gym_asc", hide_save_btn_gym_asc);
            itmReturn.setProperty("hide_user", hide_user);
            itmReturn.setProperty("hide_asc", hide_asc);
            itmReturn.setProperty("hide_photo_btn", hide_photo_btn);
            itmReturn.setProperty("asc_limit", asc_limit);
            itmReturn.setProperty("read_status", read_status);
            itmReturn.setProperty("email_status", email_status);

            itmReturn.setProperty("inn_photo_display", cfg.itmResumeView.getProperty("in_photo", ""));

            //設定上傳證照 Meeting User id
            string sql = "SELECT TOP 1 source_id, id FROM IN_MEETING_USER WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.view_resume_meeting + "'"
                + " AND in_sno = '" + cfg.view_resume_sno + "'";

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);
            Item itmMUser = cfg.inn.applySQL(sql);
            if (!itmMUser.isError() && itmMUser.getItemCount() == 1)
            {
                itmReturn.setProperty("meeting_id", itmMUser.getProperty("source_id", ""));
                itmReturn.setProperty("muid", itmMUser.getProperty("id", ""));
            }
            else
            {
                itmReturn.setProperty("meeting_id", "nodata");
                itmReturn.setProperty("muid", "nodata");
            }
        }

        //成員清單
        private void AppendAscResumeRole(TConfig cfg, Item itmResumeView, Item itmReturn)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "id" });
            fields.Add(new TField { property = "in_link" });
            fields.Add(new TField { property = "in_principal" });
            fields.Add(new TField { property = "in_head_coach" });
            fields.Add(new TField { property = "in_emrg_contact1" });
            fields.Add(new TField { property = "role_label" });
            fields.Add(new TField { property = "role_remark" });

            string resume_id = itmResumeView.getProperty("id", "");

            Item items = GetGymMembers(cfg.CCO, cfg.strMethodName, cfg.inn, resume_id);

            if (items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            int count = items.getItemCount();

            TAssociation asc = new TAssociation
            {
                Staffs = new List<Item>(),
                AreaCmts = new List<Item>(),
                PrjtCmts = new List<Item>(),
                VipMembers = new List<Item>(),
                VipGroups = new List<Item>(),
            };

            for (int i = 0; i < count; i++)
            {
                Item source = items.getItemByIndex(i);
                string role_type = source.getProperty("role_type", "");
                string id = source.getProperty("id", "");
                string in_name = source.getProperty("in_name", "");

                switch (role_type)
                {
                    case "asc":
                        asc.Staffs.Add(source);
                        source.setProperty("in_link", GetSingleMemberLink(id, in_name));
                        break;
                    case "area_cmt":
                        asc.AreaCmts.Add(source);
                        source.setProperty("in_link", GetCommitteeLink(id, in_name));
                        break;
                    case "prjt_cmt":
                        asc.PrjtCmts.Add(source);
                        source.setProperty("in_link", GetCommitteeLink(id, in_name));
                        break;
                    case "vip_mbr":
                    case "vip_minority":
                        asc.VipMembers.Add(source);
                        source.setProperty("in_link", GetSingleMemberLink(id, in_name));
                        break;
                    case "vip_group":
                        asc.VipGroups.Add(source);
                        source.setProperty("in_link", GetGymMemberLink(id, in_name));
                        break;
                }
            }

            AppendAscMemebers(cfg, asc.Staffs, "inn_members", fields, itmReturn);
            AppendAscMemebers(cfg, asc.AreaCmts, "inn_area_cmt_members", fields, itmReturn);
            AppendAscMemebers(cfg, asc.PrjtCmts, "inn_prjt_cmt_members", fields, itmReturn);
            AppendAscMemebers(cfg, asc.VipMembers, "inn_vip_mbr_members", fields, itmReturn);
            AppendAscMemebers(cfg, asc.VipGroups, "inn_vip_group_members", fields, itmReturn);
        }

        private void AppendAscMemebers(TConfig cfg, List<Item> items, string itType, List<TField> fields, Item itmReturn)
        {
            for (int i = 0; i < items.Count; i++)
            {
                Item source = items[i];
                string no = (i + 1).ToString();

                Item item = cfg.inn.newItem();
                item.setType(itType);
                item.setProperty("no", no);
                OverrideValue(cfg, item, source, fields);
                itmReturn.addRelationship(item);
            }
        }

        //執法紀錄
        private void AppendResumeStaff(TConfig cfg, string resume_id, Item itmReturn)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_meeting_type" });
            fields.Add(new TField { property = "in_title" });
            fields.Add(new TField { property = "in_job" });
            fields.Add(new TField { property = "in_term_s" });
            fields.Add(new TField { property = "in_term_e" });
            fields.Add(new TField { property = "in_note" });

            Item items = GetResumeStaffs(cfg.CCO, cfg.strMethodName, cfg.inn, resume_id);

            if (items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item source = items.getItemByIndex(i);
                string in_title = source.getProperty("in_title", "");
                string in_term_s = GetDateTimeValue(source.getProperty("in_term_s", ""), "yyyy/MM/dd", 8);
                string in_term_e = GetDateTimeValue(source.getProperty("in_term_e", ""), "yyyy/MM/dd", 8);

                if (in_title.Length > 20)
                {
                    source.setProperty("in_title", in_title.Substring(0, 20));
                }
                source.setProperty("in_term_s", in_term_s);
                source.setProperty("in_term_e", in_term_e);

                Item item = cfg.inn.newItem();
                item.setType("inn_staff");
                OverrideValue(cfg, item, source, fields);
                itmReturn.addRelationship(item);
            }
        }


        //成員清單
        private void AppendResumeRole(TConfig cfg, Item itmResumeView, Item itmReturn)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "no" });
            fields.Add(new TField { property = "id" });
            fields.Add(new TField { property = "in_link" });
            fields.Add(new TField { property = "role_label" });
            fields.Add(new TField { property = "role_remark" });

            string resume_id = itmResumeView.getProperty("id", "");
            string in_member_type = itmResumeView.getProperty("in_member_type", "");
            string in_member_role = itmResumeView.getProperty("in_member_role", "");

            Item items = GetGymMembers(cfg.CCO, cfg.strMethodName, cfg.inn, resume_id);

            if (items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item source = items.getItemByIndex(i);
                string no = (i + 1).ToString();
                string id = source.getProperty("id", "");
                string in_name = source.getProperty("in_name", "");
                source.setProperty("no", no);
                source.setProperty("in_link", GetSingleMemberLink(id, in_name));

                Item item = cfg.inn.newItem();
                item.setType("inn_members");
                OverrideValue(cfg, item, source, fields);
                itmReturn.addRelationship(item);
            }
        }

        //講習紀錄
        private void AppendResumeSeminar(TConfig cfg, string resume_id, Item itmReturn)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "id" });
            fields.Add(new TField { property = "in_title" });
            fields.Add(new TField { property = "in_certificate_no" });
            fields.Add(new TField { property = "in_date" });
            fields.Add(new TField { property = "hide_cert_btn" });
            fields.Add(new TField { property = "cert_status" });
            fields.Add(new TField { property = "meeting_id" });
            fields.Add(new TField { property = "in_score_o_5" });
            fields.Add(new TField { property = "in_score_o_3" });
            fields.Add(new TField { property = "in_year_note" });
            fields.Add(new TField { property = "in_note" });

            Item items = GetResumeSeminar(cfg.CCO, cfg.strMethodName, cfg.inn, resume_id);

            if (items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item source = items.getItemByIndex(i);

                //委外辦理
                string in_extra_option = source.getProperty("in_extra_option", "");
                bool is_outsource = in_extra_option.Contains("Consignment");

                string day_s = GetDateTimeValue(source.getProperty("in_date_s", ""), "yyyy/MM/dd", 8);
                string day_e = GetDateTimeValue(source.getProperty("in_date_e", ""), "yyyy/MM/dd", 8);

                source.setProperty("in_date", day_s + "-" + day_e);

                //modify on 2021/08/26 顯示邏輯修改
                string rIn_valid = source.getProperty("in_valid", "").Trim();
                string rIn_valid_type = source.getProperty("in_valid_type", "").Trim();
                string rIn_stat_0 = source.getProperty("in_stat_0", "").Trim(); // 是否合格

                string in_certificate_date = source.getProperty("in_certificate_date", "");
                string in_year_note = source.getProperty("in_year_note", "");
                string in_note = source.getProperty("in_note", "");
                string in_score_o_5 = source.getProperty("in_score_o_5", "0");
                string in_score_o_3 = source.getProperty("in_score_o_3", "0");
                if (in_score_o_5 == "") in_score_o_5 = "0";
                if (in_score_o_3 == "") in_score_o_3 = "0";

                // //是否隱藏分數
                // if(!rIn_stat_0.Equals("O"))
                // {
                //     source.setProperty("in_score_o_5","");
                //     source.setProperty("in_score_o_3","");
                // }

                if (rIn_valid_type == "")
                {
                    rIn_valid_type = "審核中";
                }

                //不可列印
                source.setProperty("hide_cert_btn", "item_show_0");

                //lina 2022.07.29 所有資料未填審核類型均不可外顯
                //--(學科(v)、術科成績(v)、本年度講習紀錄(補)、備註說明(補)、發證日期(補) )
                source.setProperty("in_score_o_5", "");
                source.setProperty("in_score_o_3", "");
                source.setProperty("in_year_note", "");
                source.setProperty("in_note", "");
                source.setProperty("in_certificate_date", "");

                //lina, 2021/11/02 講習紀錄列印與成績邏輯調整
                switch (rIn_valid_type)
                {
                    case "由主辦單位提供(不合格)":
                    case "由主辦單位提供(合格)":
                        //可看到成績、不可列印
                        source.setProperty("in_score_o_5", in_score_o_5);
                        source.setProperty("in_score_o_3", in_score_o_3);
                        source.setProperty("in_year_note", in_year_note);
                        source.setProperty("in_note", in_note);
                        source.setProperty("in_certificate_date", in_certificate_date);
                        source.setProperty("hide_cert_btn", "item_show_0");
                        source.setProperty("cert_status", rIn_valid_type);
                        break;

                    case "合格":
                    case "不合格":
                        if (is_outsource)
                        {
                            //委外辦理、不可列印
                            source.setProperty("hide_cert_btn", "item_show_0");
                            source.setProperty("cert_status", "由主辦單位提供");
                        }
                        else
                        {
                            source.setProperty("hide_cert_btn", "");
                            source.setProperty("cert_status", "");
                        }
                        source.setProperty("in_score_o_5", in_score_o_5);
                        source.setProperty("in_score_o_3", in_score_o_3);
                        source.setProperty("in_year_note", in_year_note);
                        source.setProperty("in_note", in_note);
                        source.setProperty("in_certificate_date", in_certificate_date);
                        break;

                    default:
                        //lina 2022.04.14 協會要求[講習結業證書未審核完成可下載]，因此調整為有成績即可下載
                        if (in_score_o_3 != "0" || in_score_o_5 != "0")
                        {
                            source.setProperty("hide_cert_btn", "");
                        }

                        source.setProperty("cert_status", rIn_valid_type);

                        if (is_outsource)
                        {
                            //委外辦理
                            source.setProperty("hide_cert_btn", "item_show_0");
                            source.setProperty("cert_status", "由主辦單位提供");
                        }

                        break;
                }

                // lina, 2022.06.27: 跆協要求只開放國內這五種類型可下載講習證書 _# START:

                //講習類別
                var in_seminar_type = source.getProperty("in_seminar_type", "").ToLower();

                //允許下載的講習類別
                var arr = new string[]
                {
                    "coach",       //國內教練
                    "coach_enr",   //國內教練增能
                    "referee",     //國內對打裁判
                    "referee_enr", //國內裁判增能
                    "poomsae",     //國內品勢裁判
                    "examiner"     //國內測驗官
                };

                if (!arr.Contains(in_seminar_type))
                {
                    //強制關閉下載 Button
                    source.setProperty("hide_cert_btn", "item_show_0");
                }

                // lina, 2022.06.27: 跆協要求只開放國內這五種類型可下載講習證書 _# END.

                Item item = cfg.inn.newItem();
                item.setType("inn_seminar");
                OverrideValue(cfg, item, source, fields);
                itmReturn.addRelationship(item);
            }
        }

        //證照列表
        private void AppendResumeCertificate(TConfig cfg, string resume_id, Item itmReturn)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "id" });
            fields.Add(new TField { property = "in_type" });
            fields.Add(new TField { property = "in_certificate_no" });
            fields.Add(new TField { property = "in_certificate_name" });

            var date_properties = new List<string> { "in_effective_start", "in_effective" };
            fields.Add(new TField { property = "inn_effective", format = "yyyy年MM月dd日", need_clear = true, ClearValue = DateRange, properties = date_properties });

            var file_properties = new List<string> { "in_file1", "in_file2" };
            fields.Add(new TField { property = "inn_files", need_clear = true, ClearValue = DownloadFiles, properties = file_properties });

            Item items = GetResumeCertificates(cfg.CCO, cfg.strMethodName, cfg.inn, resume_id);

            if (items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item source = items.getItemByIndex(i);

                Item item = cfg.inn.newItem();
                item.setType("inn_certificate");
                OverrideValue(cfg, item, source, fields);
                itmReturn.addRelationship(item);
            }
        }

        //下拉選單
        private void AppendMenu(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string list_name, string type_name, Item itmReturn)
        {
            Item items = GetValues(CCO, strMethodName, inn, list_name: list_name);
            AppendMenu(CCO, strMethodName, inn, items, type_name, itmReturn);
        }

        //下拉選單
        private void AppendMenu(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item items, string type_name, Item itmReturn)
        {
            if (items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                item.setType(type_name);
                item.setProperty("inn_label", item.getProperty("label", ""));
                item.setProperty("inn_value", item.getProperty("value", ""));
                itmReturn.addRelationship(item);
            }
        }

        //繳費單
        private void AppendMeetingPay(TConfig cfg, Item itmResumeView, Item itmReturn)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "id" });
            fields.Add(new TField { property = "meeting_id" });
            fields.Add(new TField { property = "in_title" });
            fields.Add(new TField { property = "in_paynumber" });
            fields.Add(new TField { property = "in_pay_amount_exp", need_clear = true, ClearValue = ClearMoney });
            fields.Add(new TField { property = "pay_bool" });
            fields.Add(new TField { property = "in_group" });
            fields.Add(new TField { property = "pay_status", need_clear = true, ClearValue = GetPayStatusIcon });
            fields.Add(new TField { property = "mode" });

            string in_sno = itmResumeView.getProperty("in_sno", "");

            List<Item> list = new List<Item>();
            AppendList(list, GetMeetingPays(cfg.CCO, cfg.strMethodName, cfg.inn, in_sno));
            AppendList(list, GetClaMeetingPays(cfg.CCO, cfg.strMethodName, cfg.inn, in_sno));

            if (list.Count <= 0)
            {
                return;
            }

            int count = list.Count;

            for (int i = 0; i < count; i++)
            {
                Item source = list[i];

                Item item = cfg.inn.newItem();
                item.setType("inn_meeting_pay");
                OverrideValue(cfg, item, source, fields);
                itmReturn.addRelationship(item);
            }

            string login_member_type = cfg.itmLoginResume.getProperty("in_member_type", "");
            string login_member_role = cfg.itmLoginResume.getProperty("in_member_role", "");

            string hide_remove_pay = "data-visible='false'";
            if (login_member_type == "asc" && login_member_role == "sys_9999")
            {
                hide_remove_pay = "";
            }
            itmReturn.setProperty("hide_remove_pay", hide_remove_pay);
        }

        private void AppendList(List<Item> list, Item items)
        {
            if (items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                list.Add(item);
            }
        }

        //會費繳納現況
        private void AppendResumePay(TConfig cfg, string resume_id, Item itmReturn)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "id" });
            fields.Add(new TField { property = "in_date", format = "yyyy年MM月dd日", need_clear = true, ClearValue = Clear1900 });
            fields.Add(new TField { property = "in_year", need_clear = true, ClearValue = MapChineseYear });
            fields.Add(new TField { property = "in_amount", need_clear = true, ClearValue = ClearMoney });
            fields.Add(new TField { property = "in_note", need_clear = true, ClearValue = NoteAndNumber });

            Item items = GetResumePays(cfg.CCO, cfg.strMethodName, cfg.inn, resume_id);

            if (items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item source = items.getItemByIndex(i);

                Item item = cfg.inn.newItem();
                item.setType("inn_pay");
                item.setProperty("tree_no", i.ToString());
                item.setProperty("tree_color", "treeBg" + ((i + 1) % 2).ToString());
                OverrideValue(cfg, item, source, fields);
                itmReturn.addRelationship(item);
            }
        }

        //晉段資料
        private void AppendResumePromotion(TConfig cfg, string resume_id, Item itmReturn)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "id" });
            fields.Add(new TField { property = "in_echelon" });
            fields.Add(new TField { property = "in_manager_area" });
            fields.Add(new TField { property = "in_apply_degree", need_clear = true, ClearValue = GetDegreeDisplay });
            fields.Add(new TField { property = "in_date", format = "yyyy年MM月dd日", need_clear = true, ClearValue = Clear1900 });
            fields.Add(new TField { property = "in_is_global", need_clear = true, ClearValue = GetYN });
            fields.Add(new TField { property = "in_note" });
            fields.Add(new TField { property = "inn_photo", need_clear = true, ClearValue = CardInfo });

            Item items = GetResumePromotions(cfg.CCO, cfg.strMethodName, cfg.inn, resume_id);

            if (items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item source = items.getItemByIndex(i);

                Item item = cfg.inn.newItem();
                item.setType("inn_promotion");
                OverrideValue(cfg, item, source, fields);
                itmReturn.addRelationship(item);
            }
        }

        /// <summary>
        /// 晉段備註+晉段卡圖片
        /// </summary>
        private string CardInfo(TConfig cfg, Item item, TField field)
        {
            string result = "";
            string in_score_photo = item.getProperty("in_score_photo", "");

            if (in_score_photo != "")
            {
                result += " <i class='fa fa-file-photo-o' style='color: #3c8dbc' onclick='openPhotoSwipe3(this)' data-fid='" + in_score_photo + "' ></i>";
            }

            return result;
        }

        //教練服務、裁判服務、品勢裁判服務
        private void AppendResumeRelations(TConfig cfg, string resume_id, string item_type, string in_type, Item itmReturn)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "id" });
            fields.Add(new TField { property = "in_year", need_clear = true, ClearValue = MapChineseYear });
            fields.Add(new TField { property = "in_level" });
            fields.Add(new TField { property = "in_echelon" });
            fields.Add(new TField { property = "in_know_score" });
            fields.Add(new TField { property = "in_tech_score" });
            fields.Add(new TField { property = "in_evaluation" });
            fields.Add(new TField { property = "in_note" });
            fields.Add(new TField { property = "in_certificate", need_clear = true, ClearValue = CheckBox });
            //2022-01-13 修改時會帶入的值
            fields.Add(new TField { property = "in_certificate_no" });
            fields.Add(new TField { property = "in_hours" });

            Item items = GetResumeRelations(cfg.CCO, cfg.strMethodName, cfg.inn, resume_id, item_type);

            if (items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item source = items.getItemByIndex(i);

                Item item = cfg.inn.newItem();
                item.setType(in_type);
                OverrideValue(cfg, item, source, fields);
                itmReturn.addRelationship(item);
            }
        }

        //講習年度記錄
        private void AppendResumeYears(TConfig cfg, string resume_id, Item itmReturn)
        {
            Item items = GetResumeRelations(cfg.CCO, cfg.strMethodName, cfg.inn, resume_id, "IN_RESUME_YEARS");

            if (items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            Dictionary<string, List<string>> map = new Dictionary<string, List<string>>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_type = item.getProperty("in_type", "");
                string in_year = item.getProperty("in_year", "");
                string in_tw_year = (GetIntVal(in_year) - 1911).ToString();

                List<string> list = null;
                if (map.ContainsKey(in_type))
                {
                    list = map[in_type];
                }
                else
                {
                    list = new List<string>();
                    map.Add(in_type, list);
                }
                list.Add(in_tw_year);
            }

            // 國內教練, 國內教練增能
            itmReturn.setProperty("inn_instructor_years", GetYearTable(cfg, map, "coach,coach_enr", 5));
            // 國際教練, 國際教練增能
            itmReturn.setProperty("inn_instructor_1_years", GetYearTable(cfg, map, "coach_gl,coach_enr_gl", 5));

            //國內對打裁判
            itmReturn.setProperty("inn_referee_years", GetYearTable(cfg, map, "referee", 5));
            //國際對打裁判
            itmReturn.setProperty("inn_referee_1_years", GetYearTable(cfg, map, "referee_gl", 5));
            //國內品勢裁判
            itmReturn.setProperty("inn_poomsae_years", GetYearTable(cfg, map, "poomsae", 5));
            //國際品勢裁判
            itmReturn.setProperty("inn_poomsae_1_years", GetYearTable(cfg, map, "poomsae_gl", 5));

        }

        //選手成績
        private void AppendResumeCompetitions(TConfig cfg, string resume_id, Item itmReturn)
        {
            string sql = @"
                SELECT
                    id,
                    in_comp_area,
                    in_comp_title,
                    in_comp_section,
                    CONVERT(varchar, DATEADD(hour, 8, in_comp_date), 111) as in_comp_date ,
                    in_comp_rank,
                    in_comp_item
                FROM
                    in_resume_user WITH (NOLOCK) 
                WHERE
                    source_id = '{#resume_id}'
                ORDER BY in_comp_date DESC
            ";

            sql = sql.Replace("{#resume_id}", resume_id);
            Item itmRecords = cfg.inn.applySQL(sql);



            if (itmRecords.getItemCount() > 0)
            {

                for (int i = 0; i < itmRecords.getItemCount(); i++)
                {
                    Item itmRecord = itmRecords.getItemByIndex(i);
                    itmRecord.setType("inn_competition");
                    itmRecord.setProperty("comp_id", itmRecord.getProperty("id", ""));
                    itmRecord.setProperty("comp_area", itmRecord.getProperty("in_comp_area", ""));
                    itmRecord.setProperty("comp_title", itmRecord.getProperty("in_comp_title", ""));
                    itmRecord.setProperty("comp_section", itmRecord.getProperty("in_comp_section", ""));
                    itmRecord.setProperty("comp_date", itmRecord.getProperty("in_comp_date", ""));
                    string comp_rank = itmRecord.getProperty("in_comp_rank", "");
                    itmRecord.setProperty("comp_rank_o", comp_rank);
                    switch (comp_rank)
                    {
                        case "第一名":
                            comp_rank = "<i class='fa fa-trophy' style='color: goldenrod;'></i>" + comp_rank;
                            break;
                        case "第二名":
                            comp_rank = "<i class='fa fa-trophy' style='color: silver;'></i>" + comp_rank;
                            break;
                        case "第三名":
                            comp_rank = "<i class='fa fa-trophy' style='color: rgb(117, 14, 14);'></i>" + comp_rank;
                            break;

                    }
                    itmRecord.setProperty("comp_rank", comp_rank);
                    itmRecord.setProperty("comp_item", itmRecord.getProperty("in_comp_item", ""));
                    itmRecord.setProperty("tree_no", i.ToString());
                    itmRecord.setProperty("tree_color", "treeBg" + ((i + 1) % 2).ToString());
                    itmReturn.addRelationship(itmRecord);
                }
            }

        }

        /// <summary>
        /// 自定義排序
        /// </summary>
        /// <param name="info1"></param>
        /// <param name="info2"></param>
        /// <returns></returns>
        private static int SortCompare(string info1, string info2)
        {
            // // Id 從小到大
            // return int.Parse(info1).CompareTo(int.Parse(info2));

            // Id 從大到小
            return int.Parse(info2).CompareTo(int.Parse(info1));
        }

        /// <summary>
        /// 取得ItemTypes 五筆講習紀錄資料 並且排序
        /// </summary>
        /// <param name="cfg">debug</param>
        /// <param name="map">data</param>
        /// <param name="keys">複數ItemType</param>
        /// <param name="maxCount">最大筆數</param>
        /// <returns></returns>
        private string GetYearTable(TConfig cfg, Dictionary<string, List<string>> map, string keys, int maxCount)
        {
            List<string> listKeys = keys.Split(',').ToList();
            List<string> sourceBox = new List<string>();
            listKeys.ForEach(delegate (string value)
            {
                List<string> source = map.ContainsKey(value)
                ? map[value]
                : new List<string>();
                if (0 != source.Count)
                {
                    sourceBox.AddRange(source);
                }
                // cfg.CCO.Utilities.WriteDebug("show this", " this source Count:"+ source.Count.ToString());    
                // cfg.CCO.Utilities.WriteDebug("show this", " total sourceBox Count:"+ sourceBox.Count.ToString());
            });

            //一定會生出 指定筆數 多不顯示少補空白 List
            List<string> list = FilterYearList(cfg, sourceBox, maxCount);

            //產HTML CODE
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("  <tr>");

            body.Append("<tbody>");
            body.Append("  <tr>");

            for (int i = 0; i < list.Count; i++)
            {
                head.Append("<th class='col-sm-2 text-center'>" + (i + 1) + "</th>");
                body.Append("<td >" + list[i] + "</td>");
            }

            head.Append("  </tr>");
            head.Append("</thead>");

            body.Append("  </tr>");
            body.Append("</tbody>");

            StringBuilder builder = new StringBuilder();
            builder.Append("<table class='table table-bordered'>");
            builder.Append(head);
            builder.Append(body);
            builder.Append("</table>");
            return builder.ToString();
        }

        private List<string> FilterYearList(TConfig cfg, List<string> list, int max_count)
        {
            List<string> result = new List<string>();
            //先行排序
            list.Sort(SortCompare);
            if (list.Count <= max_count)
            {
                for (int i = 0; i < max_count; i++)
                {
                    if (i >= list.Count)
                    {
                        result.Add(" ");
                    }
                    else
                    {
                        result.Add(list[i]);
                    }
                }
            }
            else if (list.Count > max_count)
            {
                //取指定筆數資料
                result = list.GetRange(0, max_count);
                // int min = list.Count - max_count;
                // for (int i = min; i < list.Count; i++)
                // {
                //     cfg.CCO.Utilities.WriteDebug("show this", "ListValue:" + list[i]);
                //     result.Add(list[i]);
                // }
            }
            return result;
        }

        //跆委會(專項與地區)
        private Item GetCommittees(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn)
        {
            string sql = @"
                SELECT
                    id AS 'value'
                    , in_name AS 'label'
                FROM
                    IN_RESUME WITH (NOLOCK) 
                WHERE
                    (in_member_type = 'area_cmt' AND in_member_role = 'sys_9999')
                    OR 
                    (in_member_type = N'prjt_cmt' AND login_name IN (N'A100', N'A101') )
                ORDER BY 
                    login_name
            ";

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //會員狀態選項
        private Item GetMStatuses(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn)
        {
            string sql = @"
                SELECT
                	value AS 'value'
                	, label_zt AS 'label'
                FROM [list] AS T1 WITH (NOLOCK)
                JOIN [VALUE] AS T2 WITH (NOLOCK)
                ON T1.id =T2.SOURCE_ID
                WHERE name ='In_Member_Status'
                ORDER BY SORT_ORDER
            ";

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //取得委員會下的道館資訊
        private Item GetGyms(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string in_manager_name)
        {
            string sql = @"
                SELECT 
                    id, in_name, in_org, in_name, in_head_coach, in_principal
                FROM IN_RESUME WITH (NOLOCK)
                WHERE
                    in_manager_name = N'{#in_manager_name}' AND in_email_exam= N'良好' AND in_member_type =N'vip_gym' 
            ";
            sql = sql.Replace("{#in_manager_name}", in_manager_name);

            return inn.applySQL(sql);
        }

        //取得委員會下的成員資訊
        private Item GetStaffs(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string in_manager_name)
        {
            string sql = @"
                SELECT 
                    id
                    , in_name
                    , in_org
                    , in_name
                    , in_title
                    , in_member_type
                    , in_member_role
                FROM 
					IN_RESUME WITH (NOLOCK)
                WHERE
                    in_manager_name = N'{#in_manager_name}' 
					AND in_member_type = 'area_cmt'
					AND in_member_role <> 'sys_9999'
                ORDER BY  
					in_member_role DESC
            ";

            sql = sql.Replace("{#in_manager_name}", in_manager_name);

            return inn.applySQL(sql);
        }

        //取得團體下的成員資訊
        private Item GetGymMembers(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string resume_id)
        {
            Item itmList = inn.applySQL("SELECT * FROM [LIST] WHERE [name] = 'In_Member_Role'");
            string list_id = itmList.getProperty("id", "");

            string sql = @"
                SELECT
	                t2.*
	                , t1.in_resume_type    AS 'role_type'
	                , t1.in_resume_role    AS 'role_value'
	                , t3.label_zt          AS 'role_label'
	                , t1.in_resume_remark  AS 'role_remark'
                FROM
	                IN_RESUME_RESUME t1 WITH(NOLOCK)
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                LEFT OUTER JOIN
	                [VALUE] t3 WITH(NOLOCK)
	                ON t3.value = t1.in_resume_role
	                AND t3.source_id = '{#list_id}'
                WHERE
	                t1.source_id = '{#resume_id}'
	                AND t1.related_id <> '{#resume_id}'
	            ORDER BY
	                t3.sort_order
	                , t2.in_sno
            ";

            sql = sql.Replace("{#list_id}", list_id)
                .Replace("{#resume_id}", resume_id);
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //所屬地區
        private Item GetAreas(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn)
        {
            string sql = @"
                SELECT 
                    value AS 'value'
                    , value AS 'label'
                FROM 
                    [list] AS T1 WITH (NOLOCK)
                JOIN 
                    [value] AS T2 WITH (NOLOCK)
                ON T1.id =T2.SOURCE_ID
                WHERE 
                    name ='in_area_list'
                ORDER BY 
                    SORT_ORDER
            ";

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //取得下拉選單
        private Item GetValues(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string list_name)
        {
            string sql = @"
                SELECT
                    t2.value
			        , t2.label_zt AS 'label'
                FROM
                    [LIST] t1 WITH(NOLOCK)
		        INNER JOIN
			        [VALUE] t2 WITH(NOLOCK)
			        ON t2.source_id = t1.id
                WHERE
			        t1.name = N'{#list_name}'
		        ORDER BY
			        t2.sort_order
            ";

            sql = sql.Replace("{#list_name}", list_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //繳費單
        private Item GetMeetingPays(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string in_creator_sno)
        {
            string sql = @"
                SELECT
                	t2.id AS 'meeting_id'
                	, '' AS 'mode'
					, t2.in_title
					, t1.item_number         AS 'in_paynumber'
                	, t1.in_pay_amount_exp
                    , t1.in_pay_amount_real
                    , t1.in_pay_photo
                    , t1.in_return_mark
                    , t1.in_collection_agency
                	, t1.pay_bool
					, t1.in_group
                FROM 
                	IN_MEETING_PAY t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING t2 WITH(NOLOCK)
                	ON t2.id = t1.in_meeting
                WHERE
                	t1.in_creator_sno = '{#in_creator_sno}'
                	AND t1.pay_bool NOT IN (N'已取消')
                ORDER BY
                    t1.item_number
            ";

            sql = sql.Replace("{#in_creator_sno}", in_creator_sno);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //繳費單
        private Item GetClaMeetingPays(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string in_creator_sno)
        {
            string sql = @"
                SELECT
                	t2.id AS 'meeting_id'
                	, 'cla' AS 'mode'
					, t2.in_title
					, t1.item_number         AS 'in_paynumber'
                	, t1.in_pay_amount_exp
                    , t1.in_pay_amount_real
                    , t1.in_pay_photo
                    , t1.in_return_mark
                    , t1.in_collection_agency
                	, t1.pay_bool
					, t1.in_group
                FROM 
                	IN_MEETING_PAY t1 WITH(NOLOCK)
                INNER JOIN
                	IN_CLA_MEETING t2 WITH(NOLOCK)
                	ON t2.id = t1.in_cla_meeting
                WHERE
                	t1.in_creator_sno = '{#in_creator_sno}'
                	AND t1.pay_bool NOT IN (N'已取消')
                ORDER BY
                    t1.item_number
            ";

            sql = sql.Replace("{#in_creator_sno}", in_creator_sno);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //會費繳納現況
        private Item GetResumePays(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string resume_id)
        {
            string sql = @"
                SELECT
                    *
                FROM
                    IN_RESUME_PAY WITH (NOLOCK) 
                WHERE
                    source_id = '{#resume_id}'
                ORDER BY 
                    in_year
                    , in_date
            ";

            sql = sql.Replace("{#resume_id}", resume_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //晉段記錄
        private Item GetResumePromotions(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string resume_id)
        {
            string sql = @"
                SELECT
                    *
                FROM
                    IN_RESUME_PROMOTION WITH (NOLOCK) 
                WHERE
                    source_id = '{#resume_id}'
                ORDER BY 
                    in_date
            ";

            sql = sql.Replace("{#resume_id}", resume_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //服務
        private Item GetResumeRelations(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string resume_id, string item_type)
        {
            string sql = @"
                SELECT
                    *
                FROM
                    {#item_type} WITH (NOLOCK) 
                WHERE
                    source_id = '{#resume_id}'
                ORDER BY 
                    in_year
                    , in_date
            ";

            sql = sql.Replace("{#item_type}", item_type)
                .Replace("{#resume_id}", resume_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //講習紀錄
        private Item GetResumeSeminar(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string resume_id)
        {
            string sql = @"
                SELECT 
                	t4.id
                	, '中跆爐字第' + t3.in_certificate_no + '號' AS 'in_certificate_no'
                    , t4.in_stat_0
                	, t4.in_score_o_5
                	, t4.in_score_o_3
                	, t4.in_valid
                	, t4.in_valid_type
                	, t4.in_year_note
                	, t4.in_note
                	, CONVERT(VARCHAR, DATEADD(HOUR, 8, t4.in_certificate_date), 111) AS 'in_certificate_date'
                	, t3.id AS 'meeting_id'
                	, t3.in_title
                	, t3.in_date_s
                	, t3.in_date_e
                	, t3.in_date_e
                	, t3.in_seminar_type
                	, t3.in_extra_option
                FROM 
                	IN_RESUME t1 WITH(NOLOCK)
                INNER JOIN
                	IN_CLA_MEETING_USER t2 WITH(NOLOCK)
                	ON t2.in_sno = t1.in_sno
                INNER JOIN
                	IN_CLA_MEETING t3 WITH(NOLOCK)
                	ON t3.id = t2.source_id
                INNER JOIN
                	IN_CLA_MEETING_RESUME t4 WITH(NOLOCK)
                	ON t4.in_user = t2.id
                WHERE 
                	t1.id = '{#resume_id}' 
                ORDER BY 
                	t3.in_date_e DESC
                	, t3.in_date_s DESC
            ";

            sql = sql.Replace("{#resume_id}", resume_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //證照列表
        private Item GetResumeCertificates(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string resume_id)
        {
            string sql = @"
                SELECT
                    *
                FROM
                    IN_RESUME_CERTIFICATE WITH (NOLOCK) 
                WHERE
                    source_id = '{#resume_id}'
                ORDER BY 
                    CASE WHEN in_type like '%舊資料%' THEN 2 ELSE 1 END, -- 將包含字串'舊資料'的項目排在最後
                    in_phototype
            ";

            sql = sql.Replace("{#resume_id}", resume_id);
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //執法紀錄
        private Item GetResumeStaffs(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string resume_id)
        {
            string sql = @"
                SELECT
                    v.label_zt AS in_meeting_type 
                    , t.in_title 
                    , t.in_job 
                    , t.in_term_s
                    , t.in_term_e 
                    , t.in_note 
                FROM
                    In_Resume_Staff t WITH (NOLOCK) 
                INNER JOIN 
                    Value v WITH (NOLOCK) 
                    ON v.value = t.in_meeting_type 
                INNER JOIN 
                    List l WITH (NOLOCK) 
                    ON l.id = v.source_id
                WHERE
                    t.source_id = '{#resume_id}'
                AND 
                    l.name = 'In_Meeting_Type' 
                ORDER BY 
                    t.sort_order
            ";

            sql = sql.Replace("{#resume_id}", resume_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //取得檢視對象 Resume
        private Item GetTargetResume(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmLoginResume, Item itmReturn)
        {
            itmLoginResume.setType("In_Resume");
            itmLoginResume.setProperty("target_resume_id", itmReturn.getProperty("id", ""));
            itmLoginResume.setProperty("login_resume_id", itmLoginResume.getID());
            itmLoginResume.setProperty("login_member_type", itmLoginResume.getProperty("in_member_type", ""));
            itmLoginResume.setProperty("login_is_admin", itmLoginResume.getProperty("in_is_admin", ""));
            return itmLoginResume.apply("In_Get_Target_Resume");
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strLogFileName { get; set; }
            public Innovator inn { get; set; }
            public string strUserId { get; set; }
            public string page { get; set; }
            public string scene { get; set; }

            public Item itmLoginResume { get; set; }
            public Item itmPermit { get; set; }
            //要檢視的對象 Resume
            public Item itmResumeView { get; set; }

            public bool isMeetingAdmin { get; set; }
            public bool isDegreeManager { get; set; }

            //登入者 resume id
            public string login_resume_id { get; set; }

            public string view_resume_id { get; set; }
            public string view_resume_sno { get; set; }
            public string view_resume_meeting { get; set; }
            public string view_is_org { get; set; }
            public string view_member_type { get; set; }
            public string view_member_role { get; set; }
            public string view_manager_name { get; set; }
            public string member_type { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public string format { get; set; }
            public string css { get; set; }
            public bool need_clear { get; set; }
            public List<string> properties { get; set; }
            public Func<TConfig, Item, TField, string> ClearValue { get; set; }
            public string GetStr(Item item)
            {
                return item.getProperty(this.property, "");
            }
        }

        private class TAssociation
        {
            public List<Item> Staffs { get; set; }
            public List<Item> AreaCmts { get; set; }
            public List<Item> PrjtCmts { get; set; }
            public List<Item> VipMembers { get; set; }
            public List<Item> VipGroups { get; set; }
        }

        private void OverrideValue(TConfig cfg, Item itmTarget, Item itmSource, List<TField> fields)
        {
            foreach (var field in fields)
            {
                if (field.need_clear)
                {
                    itmTarget.setProperty(field.property, field.ClearValue(cfg, itmSource, field));
                }
                else
                {
                    itmTarget.setProperty(field.property, itmSource.getProperty(field.property, ""));
                }
            }
        }

        /// <summary>
        /// 清除 NA mail
        /// </summary>
        private string ClearEmail(TConfig cfg, Item item, TField field)
        {
            string value = item.getProperty(field.property, "");
            if (value.Contains("na@na.n"))
            {
                return "";
            }
            else
            {
                return value;
            }
        }

        private string MemberTypeDisplay(TConfig cfg, Item item, TField field)
        {
            string value = item.getProperty("in_member_type", "");

            switch (value)
            {
                case "mbr": return "個人資料";
                case "vip_mbr": return "個人會員資料";
                case "vip_minority": return "準會員資料";
                case "vip_group": return "一般團體資料";
                case "gym": return "道館資料";
                case "vip_gym": return "道館社團資料";
                case "area_cmt": return "縣市委員會資料";
                case "prjt_cmt": return "專項委員會資料";
                case "acs": return "協會資料";
                default: return "個人資料";
            }
        }

        /// <summary>
        /// 清除 1900 的值
        /// </summary>
        private string Clear1900(TConfig cfg, Item item, TField field)
        {
            string value = item.getProperty(field.property, "");
            if (value.Contains("1900") || value.Contains("1899"))
            {
                return "";
            }
            else
            {
                return GetDateTimeByFormat(value, field.format, hours: 8);
            }
        }

        /// <summary>
        /// 日期1~日期2 (properties 限定兩個值)
        /// </summary>
        private string DateRange(TConfig cfg, Item item, TField field)
        {
            string v1 = item.getProperty(field.properties[0], "");
            string v2 = item.getProperty(field.properties[1], "");

            string r1 = GetDateTimeByFormat(v1, field.format, hours: 8);
            string r2 = GetDateTimeByFormat(v2, field.format, hours: 8);

            if (r1 == "" && r2 == "")
            {
                return "";
            }
            else
            {
                return r1 + "~" + r2;
            }
        }

        /// <summary>
        /// 下載檔案
        /// </summary>
        private string DownloadFiles(TConfig cfg, Item item, TField field)
        {
            StringBuilder builder = new StringBuilder();
            foreach (string property in field.properties)
            {
                string value = item.getProperty(property, "");
                if (value != "")
                {
                    if (builder.Length > 0) builder.Append(" ");
                    builder.Append("<a><i class='fa fa-picture-o' onclick='CertFile_Click(this)' data-photo='" + value + "'></i></a>");
                }
            }

            return builder.ToString();
        }

        /// <summary>
        /// 轉為民國年
        /// </summary>
        private string MapChineseYear(TConfig cfg, Item item, TField field)
        {
            string value = item.getProperty(field.property, "");
            if (value == "")
            {
                return "";
            }
            else
            {
                int result = GetIntVal(value) - 1911;
                return result.ToString();
            }
        }

        private string ClearMoney(TConfig cfg, Item item, TField field)
        {
            string value = item.getProperty(field.property, "");
            if (value == "" || value == "0")
            {
                return "0";
            }
            else
            {
                int result = GetIntVal(value);
                //0會被清空字串
                return result.ToString("###,###");
            }
        }

        private string NoteAndNumber(TConfig cfg, Item item, TField field)
        {
            string in_note = item.getProperty("in_note", "");
            string in_paynumber = item.getProperty("in_paynumber", "");
            string in_l1 = item.getProperty("in_l1", "");
            if (in_l1 != "") in_l1 = " " + in_l1;

            return in_paynumber == ""
                ? in_note
                : in_note + " (" + in_paynumber + in_l1 + ")";
        }

        /// <summary>
        /// Checkbox
        /// </summary>
        private string CheckBox(TConfig cfg, Item item, TField field)
        {
            string value = item.getProperty(field.property, "");
            if (value == "1")
            {
                return "checked='checked'";
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// 轉為是 or 否
        /// </summary>
        private string GetYN(TConfig cfg, Item item, TField field)
        {
            string value = item.getProperty(field.property, "");

            if (value == "0")
            {
                return "否";
            }
            else if (value == "1")
            {
                return "是";
            }
            else
            {
                return "";
            }
        }

        private string GetDegreeDisplay(TConfig cfg, Item item, TField field)
        {
            string value = item.getProperty(field.property, "");

            switch (value)
            {
                case "": return "無段位";
                case "10": return "白帶";
                case "30": return "黃帶";
                case "40": return "黃綠帶";
                case "50": return "綠帶";
                case "60": return "綠藍帶";
                case "70": return "藍帶";
                case "80": return "藍紅帶";
                case "90": return "紅帶";
                case "100": return "紅黑帶";
                case "150": return "黑帶一品";
                case "250": return "黑帶二品";
                case "350": return "黑帶三品";
                case "1000": return "壹段";
                case "2000": return "貳段";
                case "3000": return "參段";
                case "4000": return "肆段";
                case "5000": return "伍段";
                case "6000": return "陸段";
                case "7000": return "柒段";
                case "8000": return "捌段";
                case "9000": return "玖段";

                case "11000": return "11";
                case "12000": return "12";
                case "13000": return "13";
                default: return "";
            }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        private string GetDateTimeByFormat(string value, string format, int hours = 0)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value.Replace("/", "-"), out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }

        private string GetSubtotal(string count, string amount)
        {
            int quantity = 0;
            int price = 0;
            if (!int.TryParse(count, out quantity) || !int.TryParse(amount, out price))
            {
                return "0";
            }
            return (quantity * price).ToString("###,###");
        }

        private bool IsError(Item item, bool isSingle = true)
        {
            if (item.isError() || item.getResult() == "")
            {
                return true;
            }
            else if (isSingle && item.getItemCount() != 1)
            {
                return true;
            }
            else if (item.getItemCount() <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        //附加委員會成員 Table
        private void AppendStaffTable(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item items, Item itmReturn)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_link", title = "姓名", css = "text-left" });
            fields.Add(new TField { property = "in_title", title = "職稱", css = "text-left" });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            foreach (var field in fields)
            {
                head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true'>" + field.title + "</th>");
            }
            head.AppendLine("</thead>");

            int count = items.getItemCount(); ;

            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_name = item.getProperty("in_name", "");
                string in_org = item.getProperty("in_org", "");
                item.setProperty("in_link", GetDashboardStaffLink(id, in_name, in_org));

                body.AppendLine("<tr>");
                foreach (var field in fields)
                {
                    body.AppendLine("<td class='" + field.css + "' data-field='" + field.property + "' >" + field.GetStr(item) + "</td>");
                }
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");


            string table_name = "resume_table";

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name, "#member_toolbar"));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            itmReturn.setProperty("inn_StaffTable", builder.ToString());

        }

        //附加道館社團 Table
        private void AppendGymTable(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item items, Item itmReturn)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_link", title = "設立名稱", css = "text-left" });
            fields.Add(new TField { property = "in_principal", title = "負責人", css = "text-left" });
            fields.Add(new TField { property = "in_head_coach", title = "總教練", css = "text-left" });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            foreach (var field in fields)
            {
                head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true'>" + field.title + "</th>");
            }
            head.AppendLine("</thead>");

            int count = items.getItemCount(); ;

            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_name = item.getProperty("in_name", "");
                string in_org = item.getProperty("in_org", "");
                string in_member_type = item.getProperty("in_member_type", "");
                item.setProperty("in_link", GetDashboardLink(id, in_name, in_org, in_member_type));

                body.AppendLine("<tr>");
                foreach (var field in fields)
                {
                    body.AppendLine("<td class='" + field.css + "' data-field='" + field.property + "' >" + field.GetStr(item) + "</td>");
                }
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");


            string table_name = "gym_table";

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name, "#gym_toolbar"));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            itmReturn.setProperty("inn_GymTable", builder.ToString());

        }

        //協會成員
        private string GetAscStaffLink(string resume_id, string in_name, string in_org)
        {
            return "<a href='../pages/c.aspx?page=AdminStaffView.html&method=In_Group_Resume_View&id=" + resume_id + "' >"
                   + in_name
                   + "</a>";
        }

        //委員會成員
        private string GetDashboardStaffLink(string resume_id, string in_name, string in_org)
        {
            return "<a href='../pages/c.aspx?page=ManagerStaffView.html&method=In_Group_Resume_View&id=" + resume_id + "' >"
                   + in_name
                   + "</a>";
        }

        private string GetDashboardLink(string resume_id, string in_name, string in_org, string in_member_type)
        {
            if (in_org == "1")
            {
                if (in_member_type == "vip_group")
                {
                    return "<a href='../pages/c.aspx?page=GroupMemberView.html&method=In_Group_Resume_View&id=" + resume_id + "' >"
                        + in_name
                        + "</a>";
                }
                else
                {
                    //onclick=\"ajaxindicatorstart('載入中...');\"
                    return "<a href='../pages/c.aspx?page=GymMemberView.html&method=In_Group_Resume_View&id=" + resume_id + "' >"
                        + in_name
                        + "</a>";
                }
            }
            else
            {
                //onclick=\"ajaxindicatorstart('載入中...');\"
                return "<a href='../pages/c.aspx?page=SingleMemberView.html&method=In_Group_Resume_View&id=" + resume_id + "' >"
                    + in_name
                    + "</a>";
            }
        }

        private string GetCommitteeLink(string resume_id, string in_name)
        {
            //onclick=\"ajaxindicatorstart('載入中...');\"
            return "<a href='../pages/c.aspx?page=ManagerView.html&method=In_Group_Resume_View&id=" + resume_id + "' >"
                + in_name
                + "</a>";
        }

        private string GetGymMemberLink(string resume_id, string in_name)
        {
            //onclick=\"ajaxindicatorstart('載入中...');\"
            return "<a href='../pages/c.aspx?page=GymMemberView.html&method=In_Group_Resume_View&id=" + resume_id + "' >"
                + in_name
                + "</a>";
        }

        private string GetSingleMemberLink(string resume_id, string in_name)
        {
            //onclick=\"ajaxindicatorstart('載入中...');\"
            return "<a href='../pages/c.aspx?page=SingleMemberView.html&method=In_Group_Resume_View&id=" + resume_id + "' >"
                + in_name
                + "</a>";
        }

        private string GetTableAttribute(string table_name, string toolbar_name)
        {
            return "<table id='" + table_name + "' class='table table-hover table-bordered table-rwd rwd rwdtable' "
                + " data-toggle='table' "
                + " data-toolbar='" + toolbar_name + "' "
                + " data-click-to-select='true' "
                + " data-striped='false' "
                + " data-search='true' "
                + " data-pagination='true' "
                + " data-page-size='25' "
                + " data-show-pagination-switch='false'"
                + ">";
        }

        private string GetPayStatusIcon(TConfig cfg, Item item, TField field)
        {
            StringBuilder builder = new StringBuilder();

            string[] properties = new string[]
            {
                "in_paynumber", //繳費單號
                "in_pay_amount_real", //實際收款金額
                "in_pay_photo", //上傳繳費照片(繳費收據)
                "in_return_mark", //審核退回說明
                "in_collection_agency", //代收機構
            };

            foreach (string property in properties)
            {
                builder.Append("<" + property + ">" + item.getProperty(property, "") + "</" + property + ">");
            }

            Item itmResult = cfg.inn.applyMethod("In_PayType_Icon", builder.ToString());

            return itmResult.getProperty("in_pay_type", "");

        }

        private TResumeProp MapResumeProps(TConfig cfg, Item itmMeeting)
        {
            string in_seminar_type = itmMeeting.getProperty("in_seminar_type", "");

            var result = new TResumeProp
            {
                need_cert = false,
                cert_title = "",
                cert_id_pro = "",
                cert_lv_pro = "",
            };

            switch (in_seminar_type)
            {
                case "referee"://國內對打裁判
                case "referee_enr"://國內裁判增能
                    result.need_cert = true;
                    result.cert_title = "裁判證";
                    result.cert_id_pro = "in_referee_id";
                    result.cert_lv_pro = "in_referee_level";
                    result.cert_dt_pro = "in_referee_date";
                    break;
                case "referee_gl"://國際對打裁判
                case "referee_enr_gl"://國際裁判增能
                    result.need_cert = true;
                    result.cert_title = "國際裁判證";
                    result.cert_id_pro = "in_gl_referee_id";
                    result.cert_lv_pro = "in_gl_referee_level";
                    result.cert_dt_pro = "in_gl_referee_date";
                    break;
                case "poomsae"://國內品勢裁判
                    result.need_cert = true;
                    result.cert_title = "品勢裁判證";
                    result.cert_id_pro = "in_poomsae_id";
                    result.cert_lv_pro = "in_poomsae_level";
                    result.cert_dt_pro = "in_poomsae_date";
                    break;
                case "poomsae_gl"://國際品勢裁判
                    result.need_cert = true;
                    result.cert_title = "國際品勢裁判證";
                    result.cert_id_pro = "in_gl_poomsae_id";
                    result.cert_lv_pro = "in_gl_poomsae_level";
                    result.cert_dt_pro = "in_gl_poomsae_date";
                    break;
                case "coach"://國內教練
                case "coach_enr"://國內教練增能
                    result.need_cert = true;
                    result.cert_title = "教練證";
                    result.cert_id_pro = "in_instructor_id";
                    result.cert_lv_pro = "in_instructor_level";
                    result.cert_dt_pro = "in_instructor_date";
                    break;
                case "coach_gl"://國際教練
                case "coach_enr_gl"://國際教練增能
                    result.need_cert = true;
                    result.cert_title = "國際教練證";
                    result.cert_id_pro = "in_gl_instructor_id";
                    result.cert_lv_pro = "in_gl_instructor_level";
                    result.cert_dt_pro = "in_gl_instructor_date";
                    break;
                case "degree_tw"://國內段
                    result.need_cert = true;
                    result.cert_title = "國內段";
                    result.cert_id_pro = "in_degree_id";
                    result.cert_lv_pro = "in_degree";
                    result.cert_dt_pro = "in_degree_date";
                    break;
                case "degree_gl"://國際段
                    result.need_cert = true;
                    result.cert_title = "國際段";
                    result.cert_id_pro = "in_gl_degree_id";
                    result.cert_lv_pro = "in_gl_degree";
                    result.cert_dt_pro = "in_gl_degree_date";
                    break;
                default:
                    result.need_cert = false;
                    break;
            }

            return result;
        }

        private class TResumeProp
        {
            public bool need_cert { get; set; }
            public string cert_title { get; set; }
            public string cert_id_pro { get; set; }
            public string cert_dt_pro { get; set; }
            public string cert_lv_pro { get; set; }
        }

        private string GetDateTimeValue(string value, string format, int hours = 0)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }
    }
}