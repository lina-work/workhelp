﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_Meeting_Survey_Drag : Item
    {
        public In_Meeting_Survey_Drag(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 報名問項拖曳
                日誌: 
                    - 2022-09-07: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_Survey_Drag";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                meeting_survey_id = itmR.getProperty("meeting_survey_id", ""),
                mode = itmR.getProperty("mode", ""),
                scene = itmR.getProperty("scene", ""),
                is_option = itmR.getProperty("is_option", ""),

                MtTable = "IN_MEETING",
                MtSvyTable = "IN_MEETING_SURVEYS",
                SvyTable = "IN_SURVEY",
                SvyOptTable = "IN_SURVEY_OPTION",
            };

            if (cfg.mode == "cla")
            {
                cfg.MtTable = "IN_CLA_MEETING";
                cfg.MtSvyTable = "IN_CLA_MEETING_SURVEYS";
                cfg.SvyTable = "IN_CLA_SURVEY";
                cfg.SvyOptTable = "IN_CLA_SURVEY_OPTION";
            }

            Item itmMeeting = cfg.inn.applySQL("SELECT keyed_name, in_title FROM " + cfg.MtTable + " WITH(NOLOCK)  WHERE id = '" + cfg.meeting_id + "'");
            if (itmMeeting.isError() || itmMeeting.getResult() == "")
            {
                itmMeeting = cfg.inn.newItem();
            }
            cfg.itmMeeting = itmMeeting;

            switch (cfg.scene)
            {
                case "change_no":
                    ChangeInSortOrder(cfg, itmR);
                    break;

                case "change":
                    ChangeSortOrder(cfg, itmR);
                    break;

                case "clear_all_no":
                    ClearAllNo(cfg, itmR);
                    break;

                case "option_page":
                    itmR.setProperty("hide_btns", "item_show_0");
                    itmR.setProperty("hide_survey", "item_show_1");
                    OptionPage(cfg, itmR);
                    break;

                default:
                    itmR.setProperty("hide_btns", "item_show_1");
                    itmR.setProperty("hide_survey", "item_show_0");
                    Page(cfg, itmR);
                    break;
            }


            return itmR;
        }

        //清除全部題號
        private void ClearAllNo(TConfig cfg, Item itmReturn)
        {
            string sql = "UPDATE " + cfg.MtSvyTable + " SET in_sort_order = '' WHERE source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
        }

        //更新題號
        private void ChangeInSortOrder(TConfig cfg, Item itmReturn)
        {
            string id = itmReturn.getProperty("id", "");
            string no = itmReturn.getProperty("no", "");

            string sql = "UPDATE " + cfg.MtSvyTable + " SET in_sort_order = '" + no + "' WHERE id = '" + id + "'";
            cfg.inn.applySQL(sql);
        }

        //更新排序
        private void ChangeSortOrder(TConfig cfg, Item itmReturn)
        {
            string value = itmReturn.getProperty("value", "");
            if (value == "")
            {
                throw new Exception("參數錯誤");
            }

            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TRow>>(value);
            if (list == null || list.Count == 0)
            {
                throw new Exception("資料不可為空值");
            }

            if (cfg.is_option == "1")
            {
                ChangeOptionSortOrder(cfg, list, itmReturn);
            }
            else
            {
                ChangeSurveySortOrder(cfg, list, itmReturn);
            }
        }

        //更新排序
        private void ChangeOptionSortOrder(TConfig cfg, List<TRow> list, Item itmReturn)
        {
            foreach (var row in list)
            {
                string sql = "UPDATE " + cfg.SvyOptTable + " SET"
                    + " sort_order = " + row.no + "00"
                    + " WHERE id = '" + row.id + "'";

                cfg.inn.applySQL(sql);
            }
        }
        
        //更新排序
        private void ChangeSurveySortOrder(TConfig cfg, List<TRow> list, Item itmReturn)
        {
            foreach (var row in list)
            {
                string sql = "UPDATE " + cfg.MtSvyTable + " SET"
                    + " sort_order = " + row.no + "00"
                    + " WHERE id = '" + row.id + "'";

                cfg.inn.applySQL(sql);
            }

            //刷新題號
            string sql_main = @"
                UPDATE t1 SET
                	t1.in_sort_order = RIGHT(REPLICATE('0', 2) + CAST(t2.rn AS NVARCHAR), 2) 
                FROM
                	{#table} t1 WITH(NOLOCK)
                INNER JOIN
                (
                	SELECT
                		id
                		, ROW_NUMBER() OVER (ORDER BY sort_order) AS 'rn'
                	FROM 
                		{#table} WITH(NOLOCK)
                
                	WHERE
                		source_id = '{#meeting_id}'
                		AND in_surveytype = '{#in_surveytype}'
                		AND ISNULL(in_sort_order, '') <> ''
                ) t2 ON t2.id = t1.id
                WHERE 
                	t1.source_id = '{#meeting_id}'
                	AND t1.in_surveytype = '{#in_surveytype}'
            ";

            sql_main = sql_main.Replace("{#table}", cfg.MtSvyTable)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_surveytype}", "1");

            cfg.inn.applySQL(sql_main);
        }

        private void OptionPage(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("keyed_name", cfg.itmMeeting.getProperty("keyed_name", ""));
            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));

            var items = GetOptionItems(cfg);
            var page = OptionTableWrapper(cfg, items);

            itmReturn.setProperty("inn_title", page.title);
            itmReturn.setProperty("inn_table", page.contents);
        }

        private TPage OptionTableWrapper(TConfig cfg, Item items)
        {
            StringBuilder table = new StringBuilder();
            table.Append("<table class='table table-hover' id='myTable'>");
            table.Append("  <thead>");
            table.Append("    <tr>");
            table.Append("      <th style='width: 10%' scope='col' style='color: red'>編號</th>");
            table.Append("      <th style='width: 20%' scope='col'>原排序</th>");
            table.Append("      <th style='width: 35%' scope='col'>標籤</th>");
            table.Append("      <th style='width: 35%' scope='col'>過濾值</th>");
            table.Append("    </tr>");
            table.Append("  </thead>");

            table.Append("  <tbody>");

            int no = 1;
            int count = items.getItemCount();
            string title = "";
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_questions = item.getProperty("in_questions", "");
                string sort_order = item.getProperty("sort_order", "");
                string in_label = item.getProperty("in_label", "");
                string in_value = item.getProperty("in_value", "");
                string in_filter = item.getProperty("in_filter", "");

                title = in_questions;

                table.Append("    <tr id='" + no + "'>");
                table.Append("      <td class='index'><label data-id='" + id + "' style='color: red'>" + no + "</label></td>");
                table.Append("      <td class='indexSort'>" + sort_order + "</td>");
                table.Append("      <td class='indexTitle'>" + in_label + "</td>");
                table.Append("      <td class='indexTitle'>" + in_filter + "</td>");
                // table.Append("      <td class='indexTitle'>" + in_value + "</td>");
                table.Append("    </tr>");

                no++;
            }
            table.Append("  </tbody>");

            table.Append("</table>");

            return new TPage
            {
                title = title,
                contents = table.ToString(),
            };
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("keyed_name", cfg.itmMeeting.getProperty("keyed_name", ""));
            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));

            var items = GetSurveyItems(cfg);
            var page = SurveyTableWrapper(cfg, items);

            itmReturn.setProperty("inn_title", page.title);
            itmReturn.setProperty("inn_table", page.contents);
        }

        private TPage SurveyTableWrapper(TConfig cfg, Item items)
        {
            StringBuilder table = new StringBuilder();
            table.Append("<table class='table table-hover' id='myTable'>");
            table.Append("  <thead>");
            table.Append("    <tr>");
            table.Append("      <th style='width: 10%' scope='col' style='color: red'>編號</th>");
            table.Append("      <th style='width: 10%' scope='col'>原排序</th>");
            table.Append("      <th style='width: 10%' scope='col'>題號</th>");
            table.Append("      <th style='width: 50%' scope='col'>項目</th>");
            table.Append("      <th style='width: 20%' scope='col'>使用者欄位</th>");
            table.Append("    </tr>");
            table.Append("  </thead>");

            table.Append("  <tbody>");

            int no = 1;
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string sort_order = item.getProperty("sort_order", "");
                string in_questions = item.getProperty("in_questions", "");
                string in_property = item.getProperty("in_property", "");

                table.Append("    <tr id='" + no + "'>");
                table.Append("      <td class='index'><label data-id='" + id + "' style='color: red'>" + no + "</label></td>");
                table.Append("      <td class='indexSort'>" + sort_order + "</td>");
                table.Append("      <td class='indexNo'>" + QNoInput(cfg, item) + "</td>");
                table.Append("      <td class='indexTitle'>" + in_questions + "</td>");
                table.Append("      <td class='indexTitle'>" + in_property + "</td>");
                table.Append("    </tr>");

                no++;
            }
            table.Append("  </tbody>");

            table.Append("</table>");

            return new TPage
            {
                title = "",
                contents = table.ToString(),
            };
        }

        private string QNoInput(TConfig cfg, Item item)
        {
            string id = item.getProperty("id", "");
            string in_sort_order = item.getProperty("in_sort_order", "");

            return "<input type='text' class='text-center inn_no' "
                    + " data-id='" + id + "'"
                    + " data-ono='" + in_sort_order + "'"
                    + " onkeyup='Event_KeyUp(this)'"
                    + " value='" + in_sort_order + "'>";
        }


        private Item GetSurveyItems(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t1.id
	                , t1.sort_order
	                , t1.in_sort_order
	                , t2.in_questions
	                , t2.in_property
                FROM 
	                {#MtSvyTable} t1 WITH(NOLOCK)
                INNER JOIN
	                {#SvyTable} t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_surveytype = '1'
                ORDER BY
	                t1.sort_order
            ";

            sql = sql.Replace("{#MtSvyTable}", cfg.MtSvyTable)
                .Replace("{#SvyTable}", cfg.SvyTable)
                .Replace("{#meeting_id}", cfg.meeting_id);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetOptionItems(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t2.in_questions
	                , t2.in_property
	                , t3.*
                FROM 
	                {#MtSvyTable} t1 WITH(NOLOCK)
                INNER JOIN
	                {#SvyTable} t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                INNER JOIN
	                {#SvyOptTable} t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
                WHERE
	                t1.id = '{#meeting_survey_id}'
                ORDER BY
	                t3.sort_order
            ";

            sql = sql.Replace("{#MtSvyTable}", cfg.MtSvyTable)
                .Replace("{#SvyTable}", cfg.SvyTable)
                .Replace("{#SvyOptTable}", cfg.SvyOptTable)
                .Replace("{#meeting_survey_id}", cfg.meeting_survey_id);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);

        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string meeting_survey_id { get; set; }
            public string survey_id { get; set; }
            public string mode { get; set; }
            public string scene { get; set; }
            public string is_option { get; set; }

            public string MtTable { get; set; }
            public string MtSvyTable { get; set; }
            public string SvyTable { get; set; }
            public string SvyOptTable { get; set; }

            public Item itmMeeting { get; set; }
        }

        private class TPage
        {
            public string title { get; set; }
            public string contents { get; set; }
        }

        private class TRow
        {
            public string id { get; set; }
            public string no { get; set; }
        }
    }
}