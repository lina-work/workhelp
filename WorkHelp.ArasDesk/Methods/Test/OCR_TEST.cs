﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Test.Common
{
    public class OCR_TEST : Item
    {
        public OCR_TEST(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: OCR 測試
                日誌: 
                    - 2022-06-21: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "OCR_TEST";

            Item itmR = this;
            CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                aras_vault_url = @"C:\Aras\Vault\" + strDatabaseName,
                
                fileid = itmR.getProperty("fileid", ""),
                filetype = itmR.getProperty("filetype", ""),

                need_transfer = true,
                mock_file = "",
            };

            cfg.fileid = "9612E9937AC84AA79A2E17A774BB6A87";
            cfg.need_transfer = false;
            cfg.mock_file = @"C:\site\ctpc\tempvault\ocr_txt\9612E9937AC84AA79A2E17A774BB6A87.txt";

            string app_url = _InnH.GetInVariable("app_url").getProperty("in_value", "In_Variable.app_url");
            if (app_url == "")
            {
                throw new Exception("iPLM 站台網址錯誤");
            }
            string photo_path = _InnH.GetInVariable("meeting_photo").getProperty("in_value", "In_Variable.meeting_photo");
            if (photo_path == "")
            {
                throw new Exception("iPLM 站台網址 PDF 資料夾錯誤");
            }
            string pdf_path = photo_path.Replace("meeting_photo", "ocr_pdf");
            if (!System.IO.Directory.Exists(pdf_path))
            {
                System.IO.Directory.CreateDirectory(pdf_path);
            }

            //取得 Aras 檔案完整資料
            var file = GetFileFullPath(cfg, cfg.fileid);
            if (!file.valid)
            {
                throw new Exception(file.message);
            }

            file.new_file = cfg.fileid + ".pdf";
            file.new_path = pdf_path.TrimEnd('\\') + "\\" + file.new_file;

            //檢查檔案是否已存在
            if (System.IO.File.Exists(file.new_path))
            {
                System.IO.File.Delete(file.new_path);
            }

            //從 Aras Vault 複製檔案至 iPLM
            System.IO.File.Copy(file.path, file.new_path);
            if (!System.IO.File.Exists(file.new_path))
            {
                throw new Exception("檔案複製失敗");
            }

            //取得下載網址
            var end_folder = pdf_path.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries).Last();

            file.new_url = app_url.TrimEnd('/')
                + "/" + "tempvault"
                + "/" + end_folder
                + "/" + file.new_file;

            // file_info.new_url = "https://act.innosoft.com.tw/judo/images/AAA.pdf";

            if (cfg.need_transfer)
            {
                file.pdf_contents = PdfText(cfg, file);
            }
            else
            {
                file.pdf_contents = System.IO.File.ReadAllText(cfg.mock_file, System.Text.Encoding.UTF8);
            }

            CCO.Utilities.WriteDebug(strMethodName, "file: " + Newtonsoft.Json.JsonConvert.SerializeObject(file));

            if (string.IsNullOrWhiteSpace(file.pdf_contents))
            {
                throw new Exception("無文字內容");
            }

            file.rows = file.pdf_contents.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            if (file.rows == null || file.rows.Length == 0)
            {
                throw new Exception("文字內容無法分割");
            }

            itmR.setProperty("doc_text", file.pdf_contents);

            //新建文件
            AppendDocument(cfg, file, itmR);

            return itmR;
        }

            //新建文件
        private void AppendDocument(TConfig cfg, TFile file,Item itmReturn)
        {
            string sql = "";
            string doc_id = "";

            sql = @"SELECT id, source_id FROM DOCUMENT_FILE WITH(NOLOCK) WHERE related_id = '"+cfg.fileid+"'";
            Item itmDocFile = cfg.inn.applySQL(sql);
            if (!itmDocFile.isError() && itmDocFile.getResult() != "")
            {
                itmReturn.setProperty("doc_id", itmDocFile.getProperty("source_id", ""));
                return;
            }

            Item itmDoc = null;
            switch(cfg.filetype)
            {
                case "實施辦法":
                    itmDoc = AppendSeminarDoc(cfg, file);
                    break;

                default:
                    throw new Exception("Mapping pattern 未建立");
            }
           
            if (itmDoc == null || itmDoc.isError() || itmDoc.getID() == "")
            {
                throw new Exception("文件建立失敗");
            }

            doc_id = itmDoc.getID();
            itmDocFile = cfg.inn.newItem("Document File", "add");
            itmDocFile.setProperty("source_id", doc_id);
            itmDocFile.setProperty("related_id", file.fileid);
            itmDocFile.setProperty("in_note", "ocr api");
            itmDocFile = itmDocFile.apply();

            itmReturn.setProperty("doc_id", doc_id);
        }

        private Item AppendSeminarDoc(TConfig cfg, TFile file)
        {
            Item itmDoc = cfg.inn.newItem("Document", "add");
            itmDoc.setProperty("name", RowVal(file.rows, 1));
            itmDoc.setProperty("classification", "實施辦法");
            itmDoc.setProperty("description", file.pdf_contents);
            itmDoc = itmDoc.apply();
            return itmDoc;
        }

        private string RowVal(string[] arr, int idx)
        {
            return arr.Length > idx ? arr[idx] : "";
        }

        private string PdfText(TConfig cfg, TFile file)
        {
            var result = "";

            var apikey = "K83266293788957";
            var apilang = "cht";

            try
            {
                var httpClient = new System.Net.Http.HttpClient();
                httpClient.Timeout = new TimeSpan(1, 1, 1);

                var url = "https://api.ocr.space/parse/imageurl"
                    + "?apikey=" + apikey
                    + "&url=" + file.new_url
                    + "&language=" + apilang
                    + "&isOverlayRequired=true";

                var task1 = httpClient.GetAsync(url);
                var rep = task1.Result;//在這裡會等待task返回。
                var task2 = rep.Content.ReadAsStringAsync();
                var strContent = task2.Result;//在這裡會等待task返回。
                var ocrResult = Newtonsoft.Json.JsonConvert.DeserializeObject<Rootobject>(strContent);

                var builder = new StringBuilder();
                if (ocrResult.OCRExitCode == 1)
                {
                    for (int i = 0; i < ocrResult.ParsedResults.Count(); i++)
                    {
                        builder.Append(ocrResult.ParsedResults[i].ParsedText);
                    }
                }
                else
                {
                    builder.Append("ERROR: " + strContent);
                }

                result = builder.ToString();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }

        private string getSelectedLanguage(int code)
        {

            //https://ocr.space/OCRAPI#PostParameters

            //Czech = cze; Danish = dan; Dutch = dut; English = eng; Finnish = fin; French = fre; 
            //German = ger; Hungarian = hun; Italian = ita; Norwegian = nor; Polish = pol; Portuguese = por;
            //Spanish = spa; Swedish = swe; ChineseSimplified = chs; Greek = gre; Japanese = jpn; Russian = rus;
            //Turkish = tur; ChineseTraditional = cht; Korean = kor

            string strLang = "";
            switch (code)
            {
                case 0:
                    strLang = "ara";
                    break;

                case 1:
                    strLang = "chs";
                    break;

                case 2:
                    strLang = "cht";
                    break;
                case 3:
                    strLang = "cze";
                    break;
                case 4:
                    strLang = "dan";
                    break;
                case 5:
                    strLang = "dut";
                    break;
                case 6:
                    strLang = "eng";
                    break;
                case 7:
                    strLang = "fin";
                    break;
                case 8:
                    strLang = "fre";
                    break;
                case 9:
                    strLang = "ger";
                    break;
                case 10:
                    strLang = "gre";
                    break;
                case 11:
                    strLang = "hun";
                    break;
                case 12:
                    strLang = "jap";
                    break;
                case 13:
                    strLang = "kor";
                    break;
                case 14:
                    strLang = "nor";
                    break;
                case 15:
                    strLang = "pol";
                    break;
                case 16:
                    strLang = "por";
                    break;
                case 17:
                    strLang = "spa";
                    break;
                case 18:
                    strLang = "swe";
                    break;
                case 19:
                    strLang = "tur";
                    break;

            }
            return strLang;

        }

        /// <summary>
        /// 取得新 Aras Id
        /// </summary>
        private string NewArasId()
        {
            return System.Guid.NewGuid().ToString().Replace("-", "").ToUpper();
        }

        //取得檔案完整絕對路徑
        private TFile GetFileFullPath(TConfig cfg, string fileid)
        {
            TFile result = new TFile { fileid = fileid, name = "" };

            if (fileid != "")
            {
                Item itmFile = cfg.inn.applySQL("SELECT * FROM [FILE] WITH(NOLOCK) WHERE id = '" + fileid + "'");
                if (!itmFile.isError() && itmFile.getResult() != "")
                {
                    result.name = itmFile.getProperty("filename", "");
                }
            }

            if (result.name != "")
            {
                //路徑切出來
                string id_1 = fileid.Substring(0, 1);
                string id_2 = fileid.Substring(1, 2);
                string id_3 = fileid.Substring(3, 29);
                string path = cfg.aras_vault_url + @"\" + id_1 + @"\" + id_2 + @"\" + id_3 + @"\" + result.name;
                //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "headShot path: " + path);

                if (!System.IO.File.Exists(path))
                {
                    result.valid = false;
                    result.message = "檔案不存在";
                }
                else
                {
                    result.info = new System.IO.FileInfo(path);
                    result.ext = result.info.Extension;
                    result.short_name = result.info.Name.Replace(result.info.Extension, "");

                    result.valid = true;
                    result.message = "";
                    result.path = path;
                }
            }
            else
            {
                //無圖片，不提示訊息
                result.valid = false;
                result.message = "";
            }

            return result;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }
            public string aras_vault_url { get; set; }
            
            public string fileid { get; set; }
            public string filetype { get; set; }
            
            public bool need_transfer { get; set; }
            public string mock_file { get; set; }
        }

        private class TFile
        {
            public bool valid { get; set; }
            public string fileid { get; set; }
            public string message { get; set; }
            public string name { get; set; }
            public string path { get; set; }
            public string ext { get; set; }
            public string short_name { get; set; }

            public string new_file { get; set; }
            public string new_path { get; set; }
            public string new_url { get; set; }
            public string pdf_contents { get; set; }

            public System.IO.FileInfo info { get; set; }
            public string[] rows { get; set; }
        }

        private class Rootobject
        {
            public Parsedresult[] ParsedResults { get; set; }
            public int OCRExitCode { get; set; }
            public bool IsErroredOnProcessing { get; set; }
            public string ErrorMessage { get; set; }
            public string ErrorDetails { get; set; }
        }

        private class Parsedresult
        {
            public object FileParseExitCode { get; set; }
            public string ParsedText { get; set; }
            public string ErrorMessage { get; set; }
            public string ErrorDetails { get; set; }
        }
    }
}