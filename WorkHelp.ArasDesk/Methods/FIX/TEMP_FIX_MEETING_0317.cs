﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.FIX
{
    public class TEMP_FIX_MEETING_0317 : Item
    {
        public TEMP_FIX_MEETING_0317(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();

            Item itmR = this;

            string sql = "";
            
            sql = "SELECT id FROM IN_MEETING WITH(NOLOCK)";
            Item itmMeetings = inn.applySQL(sql);
            int count = itmMeetings.getItemCount();
            for (int i = 0; i < count; i++)
            {
                string meeting_id = itmMeetings.getItemByIndex(i).getProperty("id", "");
                Item itmMeeting = inn.getItemById("In_Meeting", meeting_id);
                itmMeeting = itmMeeting.apply("In_Update_MeetingOnMUEdit");
            }

            sql = "SELECT id FROM IN_CLA_MEETING WITH(NOLOCK)";
            itmMeetings = inn.applySQL(sql);
            count = itmMeetings.getItemCount();
            for (int i = 0; i < count; i++)
            {
                string meeting_id = itmMeetings.getItemByIndex(i).getProperty("id", "");
                Item itmMeeting = inn.getItemById("In_Cla_Meeting", meeting_id);
                itmMeeting = itmMeeting.apply("In_Cla_Update_MeetingOnMUEdit");
            }

            return itmR;
        }


    }
}
