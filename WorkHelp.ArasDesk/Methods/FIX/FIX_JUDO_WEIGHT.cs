﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Common
{
    public class FIX_JUDO_WEIGHT : Item
    {
        public FIX_JUDO_WEIGHT(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 建立量級表
                日誌: 
                    - 2022-08-16: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "FIX_JUDO_WEIGHT";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cols = new List<string>
            {
                "in_name",
                "in_group",
                "in_no",
                "in_level",
                "in_weight",
                "in_min",
                "in_max",
                "in_range",
            };

            //清空資料
            inn.applySQL("DELETE FROM In_Group_Weight");

            Item items = inn.applySQL("SELECT * FROM AAA_Group_Weight_20220816 WITH(NOLOCK)");

            int count = items.getItemCount();
            
            for (int i = 0; i < count; i++)
            {
                Item itmSrc = items.getItemByIndex(i);

                Item itmNew = inn.newItem("In_Group_Weight", "add");
                foreach(var col in cols)
                {
                    itmNew.setProperty(col, itmSrc.getProperty(col, ""));
                }

                itmNew.apply();
            }
            return itmR;
        }
    }
}
