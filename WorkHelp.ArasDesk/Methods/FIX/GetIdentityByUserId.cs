﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class GetIdentityByUserId : Item
    {
        public GetIdentityByUserId(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState; Innovator inn = this.getInnovator();

            string UserID = this.getProperty("in_user", "");
            UserID = "0023FA2D0C9D49F2A5C302B6D8C9520B";

            string aml = "<AML>";
            aml += "<Item type='alias' action='get' select='related_id'>";
            aml += "<source_id>" + UserID + "</source_id>";
            aml += "</Item></AML>";
            Item itmAlias = inn.applyAML(aml);
            if (itmAlias.isError())
            {
                return inn.newError("查無對應的Alias");
            }

            Item itmIdentity = itmAlias.getPropertyItem("related_id");

            if (itmIdentity == null)
            {
                return inn.newError("查無對應的Identity");
            }

            var contents = itmIdentity.node.OuterXml;
            CCO.Utilities.WriteDebug("[leeway_debug]GetIdentityByUserId", "itmIdentity.node.OuterXml: " + contents);
            
            aml = "<AML>";
            aml += "<Item type='Identity' action='get'>";
            aml += "<id>CD9B76517C874919A99D5E5989D57CD3</id>";
            aml += "</Item></AML>";
            Item itmTemp = inn.applyAML(aml);
            contents = itmTemp.dom.InnerXml;
            CCO.Utilities.WriteDebug("[leeway_debug]GetIdentityByUserId", "itmIdentity.dom.InnerXml: " + contents);

            return itmIdentity;


        }
    }
}
