﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_Delete_Resume : Item
    {
        public In_Delete_Resume(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 徹底移除 Resume 資料
                輸入: 
                    - Resume 物件
                日誌:
                    - 2020.09.16 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Delete_Resume";

            string aml = "";
            string sql = "";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "resume dom: " + itmR.dom.InnerXml);

            Item itmSQL = null;

            //取得講師履歷
            string resume_id = itmR.getProperty("id", "");
            string resume_name = itmR.getProperty("in_name", "");
            string resume_sno = itmR.getProperty("in_sno", "");
            string resume_photo = itmR.getProperty("in_photo", "");
            CCO.Utilities.WriteDebug(strMethodName, "徹底移除 Resume 資料 _# 開始: " + resume_sno + " " + resume_name);
            
            string no_user = itmR.getProperty("no_user", "");

            #region IN_MEETING

            //刪除問卷結果
            sql = @"
                DELETE FROM [IN_MEETING_SURVEYS_RESULT] WHERE [IN_PARTICIPANT] IN (
                    SELECT [ID] FROM [IN_MEETING_USER] WHERE [IN_SNO] = '{#in_sno}' AND ISNULL([IN_SNO], '') <> ''
                )
            ".Replace("{#in_sno}", resume_sno);
            itmSQL = inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("刪除問卷結果: 發生錯誤 sql = " + sql);
            }

            //刪除學員履歷
            sql = @"
                DELETE FROM [IN_MEETING_RESUME] WHERE [IN_USER] IN (
                    SELECT [ID] FROM [IN_MEETING_USER] WHERE [IN_SNO] = '{#in_sno}' AND ISNULL([IN_SNO], '') <> ''
                )
            ".Replace("{#in_sno}", resume_sno);
            itmSQL = inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("刪除學員履歷: 發生錯誤 sql = " + sql);
            }

            //刪除與會者
            sql = "DELETE FROM [IN_MEETING_USER] WHERE [IN_SNO] = '{#in_sno}'"
                .Replace("{#in_sno}", resume_sno);
            itmSQL = inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("刪除與會者: 發生錯誤 sql = " + sql);
            }

            #endregion IN_MEETING

            #region IN_CLA_MEETING

            //刪除問卷結果
            sql = @"
                DELETE FROM [IN_CLA_MEETING_SURVEYS_RESULT] WHERE [IN_PARTICIPANT] IN (
                    SELECT [ID] FROM [IN_CLA_MEETING_USER] WHERE [IN_SNO] = '{#in_sno}' AND ISNULL([IN_SNO], '') <> ''
                )
            ".Replace("{#in_sno}", resume_sno);
            itmSQL = inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("刪除問卷結果: 發生錯誤 sql = " + sql);
            }

            //刪除學員履歷
            sql = @"
                DELETE FROM [IN_CLA_MEETING_RESUME] WHERE [IN_USER] IN (
                    SELECT [ID] FROM [IN_CLA_MEETING_USER] WHERE [IN_SNO] = '{#in_sno}' AND ISNULL([IN_SNO], '') <> ''
                )
            ".Replace("{#in_sno}", resume_sno);
            itmSQL = inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("刪除學員履歷: 發生錯誤 sql = " + sql);
            }

            //刪除與會者
            sql = @"
                DELETE FROM [IN_CLA_MEETING_USER] WHERE [IN_SNO] = '{#in_sno}'
            ".Replace("{#in_sno}", resume_sno);
            itmSQL = inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("刪除與會者: 發生錯誤 sql = " + sql);
            }

            #endregion IN_CLA_MEETING

            //刪除繳費單明細
            sql = @"
                DELETE FROM [IN_MEETING_NEWS] WHERE [source_id] IN (
                    SELECT [ID] FROM [IN_MEETING_PAY] WHERE [in_creator_sno] = '{#in_sno}' AND ISNULL(pay_bool, '') <> N'已繳費'
                )
            ".Replace("{#in_sno}", resume_sno);
            itmSQL = inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("刪除繳費單明細: 發生錯誤 sql = " + sql);
            }

            //刪除繳費單
            sql = "DELETE FROM [IN_MEETING_PAY] WHERE [in_creator_sno] = '{#in_sno}' AND ISNULL(pay_bool, '') <> N'已繳費'"
                .Replace("{#in_sno}", resume_sno);
            itmSQL = inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("刪除繳費單: 發生錯誤 sql = " + sql);
            }

            //刪除證照檔案
            DeleteResumeCertFiles(CCO, strMethodName, inn, resume_id);

            //刪除講師履歷 關係類型
            DeleteResumeRelations(CCO, strMethodName, inn, resume_id);

            //解除講師履歷關聯
            sql = "UPDATE [IN_RESUME] SET [OWNED_BY_ID] = '', [IN_USER_ID] = '', [IN_PHOTO] = '' WHERE [ID] = '{#resume_id}'"
                .Replace("{#resume_id}", resume_id);
            itmSQL = inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("解除講師履歷關聯: 發生錯誤 sql = " + sql);
            }

            if (no_user == "")
            {
                //刪除使用者和角色
                RemoveUserIdentity(CCO, strMethodName, inn, resume_sno);
            }

            //刪除講師履歷
            sql = "DELETE FROM [IN_RESUME] WHERE [ID] = '{#resume_id}'"
                .Replace("{#resume_id}", resume_id);
            itmSQL = inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("刪除講師履歷: 發生錯誤 sql = " + sql);
            }

            if (resume_photo != "")
            {
                //刪除大頭照所在
                sql = "DELETE FROM [LOCATED] WHERE [SOURCE_ID] = '{#resume_photo}'"
                    .Replace("{#resume_photo}", resume_photo);
                itmSQL = inn.applySQL(sql);
                if (itmSQL.isError())
                {
                    throw new Exception("刪除檔案所在: 發生錯誤 sql = " + sql);
                }

                //刪除大頭照
                sql = "DELETE FROM [FILE] WHERE [ID] = '{#resume_photo}'"
                    .Replace("{#resume_photo}", resume_photo);
                itmSQL = inn.applySQL(sql);
                if (itmSQL.isError())
                {
                    throw new Exception("刪除大頭照: 發生錯誤 sql = " + sql);
                }
            }

            CCO.Utilities.WriteDebug(strMethodName, "徹底移除 Resume 資料 _# 完成: " + resume_sno + " " + resume_name);

            return itmR;
        }

        private void RemoveUserIdentity(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string resume_sno)
        {
            string sql = "";
            Item itmSQL = null;

            //取得使用者資訊
            sql = "SELECT [id] FROM [USER] WITH(NOLOCK) WHERE [LOGIN_NAME] = '{#in_sno}'"
                .Replace("{#in_sno}", resume_sno);
            Item itmUser = inn.applySQL(sql);
            //CCO.Utilities.WriteDebug(strMethodName, "user dom: " + itmUser.dom.InnerXml);
            if (itmUser.isError() || itmUser.getItemCount() <= 0 || itmUser.getResult() == "")
            {
                throw new Exception("取得使用者資訊: 發生錯誤 sql = " + sql);
            }

            //取得角色資訊
            sql = "SELECT [id] FROM [IDENTITY] WITH(NOLOCK) WHERE [IN_NUMBER] = '{#in_sno}'"
                .Replace("{#in_sno}", resume_sno);
            Item itmIdentity = inn.applySQL(sql);
            //CCO.Utilities.WriteDebug(strMethodName, "identity dom: " + itmIdentity.dom.InnerXml);
            if (itmIdentity.isError() || itmIdentity.getItemCount() <= 0 || itmIdentity.getResult() == "")
            {
                throw new Exception("取得角色資訊: 發生錯誤 sql = " + sql);
            }

            string user_id = itmUser.getProperty("id", "");
            string identity_id = itmIdentity.getProperty("id", "");
            //解除使用者關聯
            sql = "UPDATE [USER] SET [IN_RESUME] = '' WHERE [ID] = '{#user_id}'"
                .Replace("{#user_id}", user_id);
            itmSQL = inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("解除使用者關聯: 發生錯誤 sql = " + sql);
            }

            //刪除別名
            sql = "DELETE FROM [ALIAS] WHERE [SOURCE_ID] = '{#user_id}'"
                .Replace("{#user_id}", user_id);
            itmSQL = inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("刪除別名: 發生錯誤 sql = " + sql);
            }

            //刪除別名
            sql = "DELETE FROM [ALIAS] WHERE [RELATED_ID] = '{#identity_id}'"
                .Replace("{#identity_id}", identity_id);
            itmSQL = inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("刪除別名: 發生錯誤 sql = " + sql);
            }

            //刪除 MEMBER
            sql = "DELETE FROM [MEMBER] WHERE [RELATED_ID] = '{#identity_id}'"
                .Replace("{#identity_id}", identity_id);
            itmSQL = inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("刪除 MEMBER: 發生錯誤 sql = " + sql);
            }

            //刪除角色
            sql = "DELETE FROM [IDENTITY] WHERE [ID] = '{#identity_id}'"
                .Replace("{#identity_id}", identity_id);
            itmSQL = inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("刪除角色: 發生錯誤 sql = " + sql);
            }

            //刪除使用者
            sql = "DELETE FROM [USER] WHERE [ID] = '{#user_id}'"
                .Replace("{#user_id}", user_id);
            itmSQL = inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("刪除使用者: 發生錯誤 sql = " + sql);
            }
        }

        //刪除證照列表
        private void DeleteResumeCertFiles(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string resume_id)
        {
            List<string> list = new List<string>();

            Item items = inn.applySQL("SELECT * FROM IN_RESUME_CERTIFICATE WITH(NOLOCK) WHERE source_id = '" + resume_id + "'");
            int count = items.getItemCount();
            if (count <= 0) return;

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_file1 = item.getProperty("in_file1", "");
                string in_file2 = item.getProperty("in_file2", "");
                string in_file3 = item.getProperty("in_file3", "");
                if (in_file1 != "") list.Add(in_file1);
                if (in_file2 != "") list.Add(in_file2);
                if (in_file3 != "") list.Add(in_file3);
            }
            
            string sql = "DELETE FROM IN_RESUME_CERTIFICATE WHERE source_id = '"+ resume_id + "'";
            Item itmSQL = inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("刪除證照列表: 發生錯誤 sql = " + sql);
            }

            if (list.Count <= 0) return;
            foreach(var fileid in list)
            {
                //刪除檔案所在
                sql = "DELETE FROM [LOCATED] WHERE [SOURCE_ID] = '"+ fileid + "'";
                itmSQL = inn.applySQL(sql);
                if (itmSQL.isError())
                {
                    throw new Exception("刪除檔案所在: 發生錯誤 sql = " + sql);
                }

                //刪除檔案
                sql = "DELETE FROM [FILE] WHERE [SOURCE_ID] = '" + fileid + "'";
                itmSQL = inn.applySQL(sql);
                if (itmSQL.isError())
                {
                    throw new Exception("刪除檔案: 發生錯誤 sql = " + sql);
                }
            }
        }

        //刪除講師履歷 關係類型
        private void DeleteResumeRelations(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string resume_id)
        {
            string sql = "";
            Item itmSQL = null;
            

            Item itmType = inn.applySQL("SELECT id FROM ITEMTYPE WHERE is_current = 1 AND [name] = 'In_Resume'");
            string source_id = itmType.getProperty("id", "");

            Item items = inn.applySQL("SELECT [name], label_zt AS 'label' FROM RELATIONSHIPTYPE WHERE is_current = 1 AND source_id = '"+ source_id + "' ORDER BY sort_order");
            int count = items.getItemCount();
            for (int i = 0; i <  count; i++)
            {
                Item item = items.getItemByIndex(i);
                string table_name = item.getProperty("name", "");
                string table_desc = item.getProperty("label", "");
                if (table_name.ToUpper() == "IN_RESUME_RESUME")
                {
                    continue;
                }

                sql = "DELETE FROM [" + table_name + "] WHERE [source_id] = '" + resume_id + "'";
                itmSQL = inn.applySQL(sql);
                if (itmSQL.isError())
                {
                    throw new Exception("刪除" + table_desc +  ": 發生錯誤 sql = " + sql);
                }
            }

            //刪除成員角色
            sql = "DELETE FROM [IN_RESUME_RESUME] WHERE [RELATED_ID] = '{#resume_id}'"
                .Replace("{#resume_id}", resume_id);
            itmSQL = inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("刪除成員角色: 發生錯誤 sql = " + sql);
            }

            //刪除成員角色
            sql = "DELETE FROM [IN_RESUME_RESUME] WHERE [SOURCE_ID] = '{#resume_id}'"
                .Replace("{#resume_id}", resume_id);
            itmSQL = inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("刪除成員角色: 發生錯誤 sql = " + sql);
            }
        }

    }
}
