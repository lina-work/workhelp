﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class FIX_CTA_MINORITY : Item
    {
        public FIX_CTA_MINORITY(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 啟用準會員帳號
                日誌: 
                    - 2022-04-01: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "FIX_CTA_MINORITY";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            string sql = @"
                SELECT TOP 1
                    id AS 'resume_id'
                    , in_name
	                , in_gender
	                , DATEADD(hour, 8, in_birth) AS 'in_birth'
	                , in_sno
	                , in_tel
	                , in_password_plain
	                , in_member_type
	                , in_is_teacher
                FROM 
	                IN_RESUME t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                (
	                SELECT t101.id, t102.in_number FROM [Member] t101 INNER JOIN [IDENTITY] t102 ON t102.id = t101.related_id 
	                WHERE t101.source_id='7AC8B33DA0F246DDA70BB244AB231E29'
                ) t2 ON t2.in_number = t1.in_sno
                WHERE
	                t2.id IS NULL
	                AND in_member_type = 'vip_minority'
                ORDER BY
	                t1.in_birth
            ";

            Item items = inn.applySQL(sql);

            int count = items.getItemCount();

            CCO.Utilities.WriteDebug(strMethodName, "[準會員]啟用帳號，筆數：" + count);

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string resume_id = item.getProperty("resume_id", "");
                string in_name = item.getProperty("in_name", "");
                string in_sno = item.getProperty("in_sno", "");

                string msg = (i + 1) + ". " + in_name + "(" + in_sno + ")";

                CCO.Utilities.WriteDebug(strMethodName, "    - " + msg);

                Item itmData = inn.newItem("In_Resume");
                itmData.setProperty("resume_id", resume_id);
                itmData.setProperty("scene", "enable_one_member");

                Item itmDataResult = itmData.apply("FIX_CTA_PROMOTIONS");
                if (itmDataResult.isError())
                {
                    throw new Exception(msg + "啟用失敗");
                }
            }

            return itmR;
        }

    }
}
