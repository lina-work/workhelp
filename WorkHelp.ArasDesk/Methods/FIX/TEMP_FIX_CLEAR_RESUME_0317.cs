﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.FIX
{
    public class TEMP_FIX_CLEAR_RESUME_0317 : Item
    {
        public TEMP_FIX_CLEAR_RESUME_0317(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();

            Item itmR = this;

            string sql = "";
            Item items = null;
            int count = 0;

            sql = "SELECT TOP 3 id, in_sno FROM IN_RESUME WITH(NOLOCK) WHERE ISNULL(in_is_admin, 0) <> 1 ORDER BY in_sno";
            items = inn.applySQL(sql);
            count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                string id = items.getItemByIndex(i).getProperty("id", "");
                Item itmTarget = inn.getItemById("In_Resume", id);
                itmTarget = itmTarget.apply("In_Delete_Resume");
            }
            
            return itmR;
        }


    }
}
