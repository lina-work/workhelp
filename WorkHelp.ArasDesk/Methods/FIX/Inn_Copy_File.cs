﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class Inn_Copy_File : Item
    {
        public Inn_Copy_File(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "Inn_Copy_File";

            Item itmR = this;
            CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            Item items = inn.applySQL("SELECT * FROM AAA_FILE WITH(NOLOCK) WHERE ISNULL(fileid, '') = ''");
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string name = item.getProperty("name", "");
                string value = item.getProperty("value", "");
                string fileid = item.getProperty("fileid", "");

                if (fileid != "") continue;
                if (value == "") continue;
                if (!System.IO.File.Exists(value)) continue;


                Item itmFile = inn.newItem("File", "add");
                itmFile.setProperty("filename", name);
                itmFile.attachPhysicalFile(value);
                itmFile = itmFile.apply();

                fileid = itmFile.getID();

                string sql_update = "UPDATE AAA_FILE SET fileid = '" + fileid + "', modified_on = DATEADD(hour, -8, GETDATE()) WHERE id = '" + id + "'";
                inn.applySQL(sql_update);
            }

            return itmR;
        }
    }
}