﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.SportOpen.Vendor
{
    public class Fix_Meeting_Data : Item
    {
        public Fix_Meeting_Data(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 活動資料修正
                日誌: 
                    - 2024-03-18: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "Fix_Meeting_Data";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "payment-amount":
                    FixPaymentAmount(cfg, itmR);
                    break;

                case "creator-info":
                    FixCreatorInfo(cfg, itmR);
                    break;

                case "note-state":
                    FixNoteState(cfg, itmR);
                    break;

                case "payment-item-number":
                    FixPayNumber(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void FixPayNumber(TConfig cfg, Item itmReturn)
        {
            var in_current_org = itmReturn.getProperty("in_current_org", ""); 
            var in_paynumber = itmReturn.getProperty("in_paynumber", "");
            var rows = GetPayRows(cfg, in_current_org, in_paynumber);

            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                if (row.pay_bool != "未繳費") continue;
                if (row.item_number == in_paynumber) continue;

                var sql1 = "UPDATE IN_MEETING_PAY SET"
                    + " pay_bool = N'已取消'"
                    + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                    + " AND item_number = '" + row.item_number + "'";

                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql1);

                //cfg.inn.applySQL(sql1);
            }

            var sql2 = "UPDATE IN_MEETING_USER SET"
                + " in_paynumber = '" + in_paynumber + "'"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_current_org = N'" + in_current_org + "'";

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql2);
            
            //cfg.inn.applySQL(sql2);
        }

        private List<TPayRow> GetPayRows(TConfig cfg, string in_current_org, string in_paynumber)
        {
            var sql = @"
                SELECT DISTINCT
	                t1.in_paynumber
	                , t2.pay_bool
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                IN_MEETING_PAY t2 WITH(NOLOCK)
	                ON t2.item_number = t1.in_paynumber
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_current_org = N'{#in_current_org}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_current_org}", in_current_org);

            var rows = new List<TPayRow>();
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();

            var message = new StringBuilder();
            message.AppendLine("[" + in_current_org + "]的繳費資料將合併為：" + in_paynumber);

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var row = new TPayRow
                {
                    item_number = item.getProperty("in_paynumber", ""),
                    pay_bool = item.getProperty("pay_bool", ""),
                };
                rows.Add(row);

                message.AppendLine(" - " + row.item_number + ": " + row.pay_bool);
            }

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, message.ToString());

            return rows;
        }

        private class TPayRow
        {
            public string item_number { get; set; }
            public string pay_bool { get; set; }
        }

        private void FixNoteState(TConfig cfg, Item itmReturn)
        {
            var in_current_org = itmReturn.getProperty("in_current_org", "");
            var in_note_state = itmReturn.getProperty("in_note_state", "");

            var sql = "UPDATE IN_MEETING_USER SET"
                + "  [in_note_state] = '" + in_note_state + "'"
                + " WHERE [source_id] = '" + cfg.meeting_id + "'"
                + " AND [in_current_org] = N'" + in_current_org + "'";

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        private void FixCreatorInfo(TConfig cfg, Item itmReturn)
        {
            var in_current_org = itmReturn.getProperty("in_current_org", "");
            var in_creator = itmReturn.getProperty("in_creator", "");
            var in_creator_sno = itmReturn.getProperty("in_creator_sno", "");
            var in_creator_tel = itmReturn.getProperty("in_creator_tel", "");
            var in_creator_email = itmReturn.getProperty("in_creator_email", "");
            var in_token = itmReturn.getProperty("in_token", "");

            var sql = "UPDATE IN_MEETING_USER SET"
                + "  [in_creator] = N'" + in_creator + "'"
                + ", [in_creator_tel] = '" + in_creator_tel + "'"
                + ", [in_creator_email] = '" + in_creator_email + "'"
                + " WHERE [source_id] = '" + cfg.meeting_id + "'"
                + " AND [in_team] = N'" + in_current_org + "'";

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        //修正全部訂單金額
        private void FixPaymentAmount(TConfig cfg, Item itmReturn)
        {
            var itmMeeting = cfg.inn.applySQL("SELECT id, in_course_fees FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (itmMeeting.isError() || itmMeeting.getResult() == "")
            {
                throw new Exception("請設定每隊報名費用");
            }

            var course_fees = GetIntVal(itmMeeting.getProperty("in_course_fees", "0"));
            if (course_fees <= 0)
            {
                throw new Exception("請設定每隊報名費用");
            }

            var sql_update = "UPDATE IN_MEETING_PAY SET IN_PAY_AMOUNT_EXP = " + course_fees + " WHERE in_meeting = '\" + cfg.meeting_id + \"'";
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql_update);
            cfg.inn.applySQL(sql_update);
        }

        private void CopyItemValue(Item itmA, Item itmB, string propA, string propB = "")
        {
            if (propB == "")
            {
                itmA.setProperty(propA, itmB.getProperty(propA, ""));
            }
            else
            {
                itmA.setProperty(propA, itmB.getProperty(propB, ""));
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string scene { get; set; }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}