﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.SportOpen.活動管理報表
{
    public class In_Meeting_Export_Eord_ACT : Item
    {
        public In_Meeting_Export_Eord_ACT(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的:跆拳道-秩序冊
                說明:
                    需區分 單打,混雙,雙打,團體 的個別 SHEET
                    與報名資訊不同的地方:
                    1.不分群組,不分單位
                    2.排序: 序號-in_l1-姓名
                    3.excel 模板變數名稱為 template_1_path (in_variable:meeting_excel)
                參數:
                    in_value: meeting_excel
                    --export
                    --template
                輸入: meeting_id
                輸出: excel的位置
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_Export_Eord_ACT";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            AppendWord(cfg, itmR);
            
            return itmR;
        }

        private void AppendWord(TConfig cfg, Item itmReturn)
        {
            AppendMeetingInfo(cfg);

            //建立 Students 的欄位
            Dictionary<string, Item> StudentFields = new Dictionary<string, Item>();
            int ColIndex = 1;
            Item tmp;

            //報名表問項
            string ExpenseProperty = ""; //紀錄本賽事的費用層次
            int Meeting_Surveys_Count = cfg.itmMeeting.getRelationships("In_Meeting_Surveys").getItemCount();
            for (int i = 0; i < Meeting_Surveys_Count; i++)
            {
                Item In_Meeting_Survey = cfg.itmMeeting.getRelationships("In_Meeting_Surveys").getItemByIndex(i);
                tmp = In_Meeting_Survey.getPropertyItem("related_id");
                tmp.setProperty("col_index", (ColIndex++).ToString());
                tmp.setProperty("inn_id", In_Meeting_Survey.getProperty("related_id", ""));
                StudentFields.Add(tmp.getProperty("inn_id"), tmp);

                if (tmp.getProperty("in_expense", "") == "1")
                {
                    //可能是 in_l1, in_l2, in_l3
                    ExpenseProperty = tmp.getProperty("in_property", ""); 
                }

            }
            Meeting_Surveys_Count++;
            tmp = cfg.inn.newItem("In_Meeting_Surveys");
            tmp.setProperty("inn_id", "expense");
            tmp.setProperty("in_questions", "金額");
            tmp.setProperty("in_request", "0");
            tmp.setProperty("col_index", (ColIndex++).ToString());
            tmp.setProperty("in_selectoption", "");
            tmp.setProperty("in_question_type", "number");
            StudentFields.Add(tmp.getProperty("inn_id"), tmp);

            AppendExportInfo(cfg);
            string Template_Path = cfg.template_path;
            string Export_Path = cfg.export_path;
            
            //Word
            Xceed.Words.NET.DocX doc;//word
            doc = Xceed.Words.NET.DocX.Load(Template_Path);//載入模板

            var docTables = doc.Tables[0];//取得模板第一個表格

            //先處理students的欄位
            int WSRow = 3;
            int WSCol = 0;

            string L1Name = "";
            string L2Name = "";
            string L3Name = "";
            foreach (KeyValuePair<string, Item> StudentField in StudentFields)
            {
                tmp = StudentField.Value;
                WSCol = int.Parse(tmp.getProperty("col_index", "0")) + 1;

                if (tmp.getProperty("in_property", "") == "in_l1")
                    L1Name = tmp.getProperty("in_questions", "");

                if (tmp.getProperty("in_property", "") == "in_l2")
                    L2Name = tmp.getProperty("in_questions", "");

                if (tmp.getProperty("in_property", "") == "in_l3")
                    L3Name = tmp.getProperty("in_questions", "");

            }

            //CCO.Utilities.WriteDebug("in_meeting_export_word_1_ACT", "1");
            //算出有幾間群組(in_group)
            var sql = "select distinct in_current_org as c1 from in_meeting_user where source_id='" + cfg.meeting_id + "' and [In_Meeting_User].in_note_state='official'";

            Item Orgs = cfg.inn.applySQL(sql);
            Dictionary<string, Item> DicOrgs = new Dictionary<string, Item>();

            //取得課程編號
            string item_number = cfg.itmMeeting.getProperty("item_number", "");
            string svy_l1_id = GetSurveyId(cfg, "in_l1");

            //取得題庫下的in_l1跟項次
            sql = "select in_value,sort_order from In_Survey_Option where source_id='" + svy_l1_id + "'order By sort_order";
            Item In_survey_options = cfg.inn.applySQL(sql);
            //隊別
            List<string> teams = new List<string>();
            //取出隊別
            sql = "select distinct in_team from In_Meeting_User where source_id='" + cfg.meeting_id + "'order By in_team";
            Item In_meeting_users = cfg.inn.applySQL(sql);

            for (int i = 0; i < In_meeting_users.getItemCount(); i++)
            {
                Item In_meeting_user = In_meeting_users.getItemByIndex(i);
                teams.Add(In_meeting_user.getProperty("in_team", ""));
            }
            //CCO.Utilities.WriteDebug("in_meeting_export_word_1_ACT", "3");	
            //將第一階加入Dictionary
            Dictionary<string, string> survey_in_l1s = new Dictionary<string, string>();
            int l1_index = 0;
            for (int i = 0; i < In_survey_options.getItemCount(); i++)
            {
                Item In_survey_option = In_survey_options.getItemByIndex(i);
                if (In_survey_option.getProperty("in_value", "") != "隊職員")
                {
                    l1_index++;
                    //CCO.Utilities.WriteDebug("[ACT]in_meeting_export_word_1_ACT", In_survey_option.getProperty("in_value","") + "," + l1_index);
                    survey_in_l1s.Add(In_survey_option.getProperty("in_value", ""), "in_l1_" + l1_index);
                }
            }
            //CCO.Utilities.WriteDebug("in_meeting_export_word_1_ACT", "4");
            //int SheetIndex = 4;
            for (int i = 0; i < Orgs.getItemCount(); i++)
            {
                Item Org = Orgs.getItemByIndex(i);
                string C1 = Org.getProperty("c1", "");

                string OrgName = C1;

                Org.setProperty("sheet_index", "0");
                Org.setProperty("last_row", "3");

                //預設(?)
                Org.setProperty("company_name", "");//單位名稱
                Org.setProperty("leader", "");//領隊
                Org.setProperty("coach", "");//教練
                Org.setProperty("contact_person", "");//管理
                Org.setProperty("management", "");//聯絡人
                Org.setProperty("mobile_phone", "");//手機
                Org.setProperty("phone", "");//電話
                                             //CCO.Utilities.WriteDebug("[ACT]in_meeting_export_word_1_ACT", OrgName + "," + Org);

                DicOrgs.Add(OrgName, Org);
            }


            //處理學員
            WSCol = 1;
            WSRow = 4;
            //計算總費用的方式:同一個費用層次且同一序號,只能算一個費用,例如雙打/2000, 代表 兩個人總共2000

            string CommanExpenseProperty = "";
            if (ExpenseProperty != "in_l1")
                CommanExpenseProperty = "," + ExpenseProperty;

            var aml = "<AML>" +
                "<Item type='In_Meeting_User' action='get' orderBy='in_current_org,in_l1,in_l2,in_team,in_index,in_name' where=\"[In_Meeting_User].in_note_state='official' and [In_Meeting_User].source_id='" + cfg.meeting_id + "' " + "" + "\"/>" +
                "</AML>";
            //CCO.Utilities.WriteDebug("in_meeting_export_word_1_ACT", "aml:" + aml);
            Item MeetingUsers = cfg.inn.applyAML(aml);
            string PreExpenseKey = "";
            string ThisExpenseKey = "";
            //CCO.Utilities.WriteDebug("in_meeting_export_word_1_ACT", "MeetingUsers.getItemCount():" + MeetingUsers.getItemCount());
            for (int i = 0; i < MeetingUsers.getItemCount(); i++)
            {
                WSRow = i + 4;
                Item In_Meeting_User = MeetingUsers.getItemByIndex(i);
                ThisExpenseKey = In_Meeting_User.getProperty(ExpenseProperty, "") + In_Meeting_User.getProperty("in_index", "");
                //string OrgName = In_Meeting_User.getProperty("in_current_org","").Split('/')[0];
                string OrgName = In_Meeting_User.getProperty("in_current_org", "");

                Item DicOrg = DicOrgs[OrgName];
                int docindex = int.Parse(DicOrg.getProperty("sheet_index"));
                docindex = docTables.Index;
                //CCO.Utilities.WriteDebug("in_meeting_export_word_1_ACT", DicOrg.dom.InnerXml);

                string ManPropertyName = "";
                string GroupTotal = "";
                string GroupQuantity = "";
                string in_l1 = In_Meeting_User.getProperty("in_l1", "");


                //第一階有包含key值塞進去
                foreach (var survey_in_l1 in survey_in_l1s)
                {
                    if (in_l1.Contains(survey_in_l1.Key))
                    {
                        ManPropertyName = survey_in_l1.Value + In_Meeting_User.getProperty("in_team", "");
                        GroupTotal = survey_in_l1.Value + "_g" + In_Meeting_User.getProperty("in_team", "");
                        //CCO.Utilities.WriteDebug("in_meeting_export_word_1_ACT", ManPropertyName+GroupTotal);
                    }
                }

                //CCO.Utilities.WriteDebug("in_meeting_export_word_1_ACT", "5");

                int OrgWSRow = int.Parse(DicOrg.getProperty("last_row"));
                OrgWSRow = OrgWSRow + 1;
                DicOrg.setProperty("last_row", OrgWSRow.ToString());
                //更新統計數字

                string company_name = DicOrg.getProperty("company_name", "");//單位名稱
                string leader = DicOrg.getProperty("leader", "");//領隊
                string coach = DicOrg.getProperty("coach", "");//教練
                string contact_person = DicOrg.getProperty("contact_person", "");//管理
                string management = DicOrg.getProperty("management", "");//聯絡人
                string mobile_phone = DicOrg.getProperty("mobile_phone", "");//手機
                string phone = DicOrg.getProperty("phone", "");//電話

                string in_gender = In_Meeting_User.getProperty("in_gender", "");
                string in_name = In_Meeting_User.getProperty("in_name", "");
                string in_l2 = In_Meeting_User.getProperty("in_l2", "");
                string in_team = In_Meeting_User.getProperty("in_team", "");
                string in_current_org = In_Meeting_User.getProperty("in_current_org", "");
                //CCO.Utilities.WriteDebug("in_meeting_export_word_1_ACT", "5.1");		
                if (in_l2.Contains("領隊"))
                {
                    ManPropertyName = "leader";
                    GroupTotal = "Leader";
                }
                else if (in_l2.Contains("教練"))
                {
                    ManPropertyName = "coach";
                    GroupTotal = "Coach";
                }
                else if (in_l2.Contains("管理"))
                {
                    ManPropertyName = "contact_person";
                    GroupTotal = "Contact_person";
                }
                else
                {
                    //continue;
                }

                string MySectionNames = DicOrg.getProperty(ManPropertyName, "");//依據組別取出值(姓名)
                string Group = DicOrg.getProperty(GroupTotal, "");////依據組別取出值(組)

                //判定是否有重複組別
                if (!Group.Contains(ManPropertyName))
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "5.2.1");
                    //沒有的話將現在組別塞進去
                    DicOrg.setProperty(GroupTotal, ManPropertyName);
                }

                //判定是否有重複名字
                if (!MySectionNames.Contains(in_name))
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "5.2.2");
                    //沒有的話依照組別取出再塞新的名字進去
                    MySectionNames = MySectionNames + in_name + " ";
                    DicOrg.setProperty(ManPropertyName, MySectionNames);
                }

                //CCO.Utilities.WriteDebug("in_meeting_export_word_1_ACT", "5.3");       
                DicOrg.setProperty("in_l1", in_l1);
                DicOrg.setProperty("in_current_org", in_current_org);
            }
            //CCO.Utilities.WriteDebug("in_meeting_export_word_1_ACT", "6");
            sql = "select distinct in_current_org as c1 from in_meeting_user where source_id='" + cfg.meeting_id + "' and [In_Meeting_User].in_note_state='official'";
            Item l1s = cfg.inn.applySQL(sql);
            //根據規格決定貼幾次
            for (int k = 0; k < 1; k++)
            {
                Item l1 = l1s.getItemByIndex(k);

                doc.InsertParagraph(cfg.mt_title + "隊職員名單").FontSize(24).Font("標楷體").Alignment = Xceed.Document.NET.Alignment.center;


                //將取出的各校資料貼出來
                Item entrylist = null;
                foreach (var entryitem in DicOrgs)
                {
                    entrylist = entryitem.Value;

                    if (entrylist.getProperty("in_current_org", "") != l1.getProperty("c1", ""))
                    {
                        //continue;
                    }
                    docTables = doc.InsertTable(doc.Tables[0]);
                    Xceed.Document.NET.Row row = docTables.Rows[2];//第幾列

                    doc.InsertParagraph();

                    docTables.Rows[0].Cells[0].Paragraphs.First().Append("單位名稱：" + entrylist.getProperty("in_current_org", "")).Font("標楷體").FontSize(11);


                    if (DicOrgs.ContainsKey(entrylist.getProperty("in_current_org", "")))
                    {
                        Item staff = DicOrgs[entrylist.getProperty("in_current_org", "")];

                        docTables.Rows[1].Cells[0].Paragraphs.First().Append("領    隊：" + staff.getProperty("leader", "")).Font("標楷體").FontSize(11);
                        docTables.Rows[1].Cells[1].Paragraphs.First().Append("教    練：" + staff.getProperty("coach", "")).Font("標楷體").FontSize(11);
                        docTables.Rows[1].Cells[2].Paragraphs.First().Append("管    理：" + staff.getProperty("contact_person", "")).Font("標楷體").FontSize(11);
                    }
                    else
                    {
                        docTables.Rows[1].Cells[0].Paragraphs.First().Append("領    隊：").Font("標楷體").FontSize(11);
                        docTables.Rows[1].Cells[1].Paragraphs.First().Append("教    練：").Font("標楷體").FontSize(11);
                        docTables.Rows[1].Cells[2].Paragraphs.First().Append("管    理：").Font("標楷體").FontSize(11);
                    }

                    //依照值列出成員*******(如果有隊別會抓不到)
                    int test = 0;
                    foreach (var survey_in_l1 in survey_in_l1s)
                    {
                        for (int i = 0; i < teams.Count; i++)
                        {
                            if (entrylist.getProperty(survey_in_l1.Value + teams[i], "") != "")
                            {
                                docTables.InsertRow(row, true);
                                //組別
                                string[] Keys = survey_in_l1.Key.Split('/');
                                string key = "";
                                if (Keys.Length > 1)
                                {
                                    key = Keys[0];
                                }
                                else
                                {
                                    key = survey_in_l1.Key;
                                }
                                docTables.Rows[3 + test].Cells[0].Paragraphs.First().Append(key + teams[i]).Font("標楷體").FontSize(11);
                                //該組成員
                                docTables.Rows[3 + test].Cells[1].Paragraphs.First().Append(entrylist.getProperty(survey_in_l1.Value + teams[i], "")).Font("標楷體").FontSize(11);
                                test++;
                            }
                        }
                    }
                    docTables.Rows[2].Remove();//移除用來複製的表格列
                }
            }

            doc.Tables[0].Remove();
            doc.RemoveParagraphAt(0);

            //匯出檔名
            var xlsName = cfg.mt_title
                + "_" + "秩序冊"
                + "_" + System.Guid.NewGuid().ToString().Substring(0, 4).ToUpper()
                + "_" + System.DateTime.Now.ToString("yyyyMMddHHmmss");

            xlsName = xlsName.Replace("+", "")
                            .Replace("-", "")
                            .Replace(".", "")
                            .Replace(" ", "");

            string extName = ".docx";
            string xls_file = cfg.export_path + xlsName + extName;
            string xls_name = xlsName + extName;

            if (!System.IO.Directory.Exists(cfg.export_path))
            {
                System.IO.Directory.CreateDirectory(cfg.export_path);
            }

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, cfg.export_path);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, xls_file);

            doc.SaveAs(xls_file);

            itmReturn.setProperty("xls_name", xls_name);
        }

        private void SetCellTextValue(ref ClosedXML.Excel.IXLCell Cell, string TextValue)
        {
            DateTime dtTmp;
            double dblTmp;


            if (DateTime.TryParse(TextValue, out dtTmp))
            {
                Cell.Value = "'" + TextValue;
            }
            else if (double.TryParse(TextValue, out dblTmp))
            {
                Cell.Value = "'" + TextValue;
            }
            else
            {
                Cell.Value = TextValue;
            }
            Cell.DataType = ClosedXML.Excel.XLDataType.Text;

        }
        private string GetSheetName(string TheString)
        {

            string[] Single_Names = { "單人", "單打" };
            string[] Mix_Names = { "混雙" };
            string[] Due_Names = { "雙人", "雙打" };
            string[] Group_Names = { "團體" };

            for (int i = 0; i < Single_Names.Length; i++)
            {
                if (TheString.Contains(Single_Names[i]))
                    return "單打";
            }

            for (int i = 0; i < Mix_Names.Length; i++)
            {
                if (TheString.Contains(Mix_Names[i]))
                    return "混雙";
            }

            for (int i = 0; i < Due_Names.Length; i++)
            {
                if (TheString.Contains(Due_Names[i]))
                    return "雙打";
            }

            for (int i = 0; i < Group_Names.Length; i++)
            {
                if (TheString.Contains(Group_Names[i]))
                    return "團體";
            }

            return TheString;

        }
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }

            public Item itmXlsx { get; set; }
            public string template_path { get; set; }
            public string export_path { get; set; }
            public string export_type { get; set; }
            public string font_name { get; set; }
        }

        //賽事資訊
        private void AppendMeetingInfo(TConfig cfg)
        {
            if (cfg.meeting_id == "") return;

            var aml = "<AML>" +
                "<Item type='In_Meeting' action='get' id='" + cfg.meeting_id + "'>" +
                "<Relationships>" +
                "<Item type='In_Meeting_Surveys' action='get' select='related_id(id,in_questions,in_request,in_selectoption,in_question_type,in_property,in_expense,in_is_pkey)'>" +
                "<in_surveytype>1</in_surveytype>" +
                "<in_property condition='in'>'in_l1','in_l2','in_l3'</in_property>" +
                "</Item>" +
                "</Relationships>" +
            "</Item></AML>";

            cfg.itmMeeting = cfg.inn.applyAML(aml);
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("查無 賽事資料");
            }
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
        }

        private void AppendExportInfo(TConfig cfg)
        {
            cfg.itmXlsx = cfg.inn.applyMethod("In_Variable_File", "<export_name>meeting_export_path</export_name><in_name>order_template_path_act</in_name>");
            cfg.template_path = cfg.itmXlsx.getProperty("template_path", "");
            cfg.export_path = cfg.itmXlsx.getProperty("export_path", "").TrimEnd('\\') + @"\" + cfg.meeting_id + @"\";
        }

        private string GetSurveyId(TConfig cfg, string in_property)
        {
            var sql = @"
                SELECT TOP 1
	                t2.id 
                FROM 
	                IN_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN
	                IN_SURVEY t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_surveytype = '1'
	                AND t2.in_property = '{#in_property}'
            ";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_property}", in_property);
            var item = cfg.inn.applySQL(sql);
            if (item.isError() || item.getResult() == "")
            {
                return "";
            }
            return item.getProperty("id", "");
        }
    }
}