﻿using Aras.IOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using Xceed.Words.NET;
using static System.Net.Mime.MediaTypeNames;

namespace WorkHelp.ArasDesk.Methods.SportOpen.活動管理報表
{
    public class In_Meeting_Export_PlayerId_ACT : Item
    {
        public In_Meeting_Export_PlayerId_ACT(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的:創建[跆拳道]選手證
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "in_playerid_act";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            var itmR = this;

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            AppendPlayerIdWord(cfg, itmR);
            
            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private void AppendPlayerIdWord(TConfig cfg, Item itmReturn)
        {
            AppendMeetingInfo(cfg);
            AppendExportInfo(cfg);

            //Word
            Xceed.Words.NET.DocX doc;//word
            doc = Xceed.Words.NET.DocX.Load(cfg.template_path);//載入模板
            var docTables = doc.Tables[0];//取得模板第一個表格

            var DicOrgs = GetOrgDictionary(cfg);
            foreach (KeyValuePair<string, Item> DicOrg in DicOrgs)
            {
                var Org = DicOrg.Value;
                var is_statff = Org.getProperty("in_l1", "").Contains("隊職員");

                if (!is_statff)
                {
                    doc.InsertParagraph();
                    docTables = doc.InsertTable(doc.Tables[0]);
                    //賽事名稱
                    docTables.Rows[0].Cells[0].Paragraphs.First().Append(cfg.mt_title).Bold().Font("標楷體").FontSize(20);
                    //選手證
                    docTables.Rows[1].Cells[0].Paragraphs.First().Append("選手證").Bold().Font("標楷體").FontSize(48);
                    //姓名
                    docTables.Rows[2].Cells[2].Paragraphs.First().Append(Org.getProperty("in_name", "")).Bold().Font("標楷體").FontSize(9);
                    //單位
                    docTables.Rows[3].Cells[2].Paragraphs.First().Append(Org.getProperty("in_current_org", "")).Bold().Font("標楷體").FontSize(9);
                    //組別
                    docTables.Rows[4].Cells[2].Paragraphs.First().Append(Org.getProperty("in_l1", "")).Bold().Font("標楷體").FontSize(9);
                    //量級
                    docTables.Rows[5].Cells[2].Paragraphs.First().Append(Org.getProperty("in_l3", "")).Bold().Font("標楷體").FontSize(9);
                    //地點
                    docTables.Rows[6].Cells[2].Paragraphs.First().Append(cfg.in_address).Bold().Font("標楷體").FontSize(9);
                    //日期
                    docTables.Rows[7].Cells[2].Paragraphs.First().Append(cfg.day_range).Bold().Font("標楷體").FontSize(9);
                    //大頭照
                    AppendPicture(cfg, doc, docTables, Org);
                }
            }

            foreach (KeyValuePair<string, Item> DicOrg in DicOrgs)
            {
                Item Org = DicOrg.Value;
                var is_statff = Org.getProperty("in_l1", "").Contains("隊職員");
                if (is_statff)
                {
                    //doc.InsertParagraph();
                    docTables = doc.InsertTable(doc.Tables[0]);
                    //賽事名稱
                    docTables.Rows[0].Cells[0].Paragraphs.First().Append(cfg.mt_title).Bold().Font("標楷體").FontSize(20);
                    //領隊&教練&管理證
                    docTables.Rows[1].Cells[0].Paragraphs.First().Append(Org.getProperty("in_l3", "") + "證").Bold().Font("標楷體").FontSize(48);
                    //姓名
                    docTables.Rows[2].Cells[2].Paragraphs.First().Append(Org.getProperty("in_name", "")).Bold().Font("標楷體").FontSize(9);
                    //單位
                    //docTables.Rows[3].Cells[2].Paragraphs.First().Append(Org.getProperty("in_current_org","")).Bold().Font("標楷體").FontSize(9);
                    //組別
                    docTables.Rows[4].Cells[2].Paragraphs.First().Append(Org.getProperty("in_l1", "")).Bold().Font("標楷體").FontSize(9);
                    //量級
                    docTables.Rows[5].Cells[2].Paragraphs.First().Append(Org.getProperty("in_l3", "")).Bold().Font("標楷體").FontSize(9);
                    //地點
                    docTables.Rows[6].Cells[2].Paragraphs.First().Append(cfg.in_address).Bold().Font("標楷體").FontSize(9);
                    //日期
                    docTables.Rows[7].Cells[2].Paragraphs.First().Append(cfg.day_range).Bold().Font("標楷體").FontSize(9);
                    //大頭照
                    AppendPicture(cfg, doc, docTables, Org);
                }
            }

            AppendExportResult(cfg, doc, itmReturn);
        }

        private void AppendPicture(TConfig cfg, Xceed.Words.NET.DocX doc, Xceed.Document.NET.Table docTables, Item item)
        {
            var in_photo = item.getProperty("in_photo", "").Trim();
            var filename = item.getProperty("filename", "").Trim();
            if (in_photo == "") return;

            var url = @"C:\Aras\Vault\SportOpen\";
            var id_1 = in_photo.Substring(0, 1);
            var id_2 = in_photo.Substring(1, 2);
            var id_3 = in_photo.Substring(3, 29);

            var file = url + id_1 + @"\" + id_2 + @"\" + id_3 + @"\" + filename;
            if (!System.IO.File.Exists(file)) return;

            try
            {
                Xceed.Document.NET.Image img = doc.AddImage(file);//取得路徑圖片
                Xceed.Document.NET.Picture p = img.CreatePicture(170, 120);
                docTables.Rows[2].Cells[0].Paragraphs.First().AppendPicture(p);
            }
            catch (Exception ex)
            {

            }
        }

        private void AppendExportResult(TConfig cfg, Xceed.Words.NET.DocX doc, Item itmReturn)
        {
            doc.Tables[0].Remove();
            doc.RemoveParagraphAt(0);
            doc.RemoveParagraphAt(0);

            //匯出檔名
            var xlsName = cfg.mt_title
                + "_" + "選手證"
                + "_" + System.Guid.NewGuid().ToString().Substring(0, 4).ToUpper()
                + "_" + System.DateTime.Now.ToString("yyyyMMddHHmmss");

            xlsName = xlsName.Replace("+", "")
                            .Replace("-", "")
                            .Replace(".", "")
                            .Replace(" ", "");

            string extName = ".docx";
            string xls_file = cfg.export_path + xlsName + extName;
            string xls_name = xlsName + extName;

            if (!System.IO.Directory.Exists(cfg.export_path))
            {
                System.IO.Directory.CreateDirectory(cfg.export_path);
            }

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, cfg.export_path);
            //儲存檔案          
            doc.SaveAs(xls_file);
            //下載路徑
            itmReturn.setProperty("xls_name", xls_name);
        }

        private Dictionary<string, Item> GetOrgDictionary(TConfig cfg)
        {
            var DicOrgs = new Dictionary<string, Item>();

            var sql = @"
                SELECT
	                t1.id
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_gender
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, t1.in_birth), 111) AS 'reg_birth'
	                , t1.in_current_org
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_index
	                , t1.in_expense
	                , t1.in_photo1
	                , t2.filename
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                [FILE] t2 WITH(NOLOCK)
	                ON t2.id = t1.in_photo1
                WHERE
	                t1.source_id = '5C756278DD674BF5A272B0770F8E18F4'
	                AND t1.in_note_state = 'official'
                ORDER BY
	                t1.in_current_org
	                , t1.in_l1
            ";

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                string in_current_org = item.getProperty("in_current_org", "");
                string in_name = item.getProperty("in_name", "");
                string in_l3 = item.getProperty("in_l3", "");
                string in_l1 = item.getProperty("in_l1", "");
                string in_sno = item.getProperty("in_sno", "");

                string OrgName = "";
                bool is_staff = in_l1 == "隊職員";
                if (is_staff)
                {
                    OrgName = in_name + "_" + in_l3;
                }
                else
                {
                    OrgName = in_current_org + "_" + in_sno + "_" + in_name;
                }


                Item orgnode = cfg.inn.newItem();
                orgnode.setProperty("sheet_index", "0");
                orgnode.setProperty("last_row", "3");
               
                //key不存在就存進去
                if (!DicOrgs.ContainsKey(OrgName))
                {
                    orgnode.setProperty("in_photo", item.getProperty("in_photo1", ""));
                    orgnode.setProperty("filename", item.getProperty("filename", ""));
                    orgnode.setProperty("in_name", item.getProperty("in_name", ""));
                    orgnode.setProperty("in_current_org", item.getProperty("in_current_org", ""));
                    orgnode.setProperty("in_l1", item.getProperty("in_l1", ""));
                    orgnode.setProperty("in_l3", item.getProperty("in_l3", ""));
                    DicOrgs.Add(OrgName, orgnode);
                }
                else
                {
                    //如果key存在 就將in_l1,in_l3拚一起
                    Item in_meeting_user_inc = DicOrgs[OrgName];

                    string inc_l1 = in_meeting_user_inc.getProperty("in_l1", "");
                    string inc_l3 = in_meeting_user_inc.getProperty("in_l3", "");

                    //只有選手 才將in_l1,in_l3拼起來
                    if (!is_staff)
                    {
                        inc_l1 += "," + in_l1;
                        inc_l3 += "," + in_l3;
                    }

                    orgnode.setProperty("in_l1", inc_l1);
                    orgnode.setProperty("in_l3", inc_l3);

                    orgnode.setProperty("in_photo", item.getProperty("in_photo1", ""));
                    orgnode.setProperty("filename", item.getProperty("filename", ""));
                    orgnode.setProperty("in_name", item.getProperty("in_name", ""));
                    orgnode.setProperty("in_current_org", item.getProperty("in_current_org", ""));

                    DicOrgs[OrgName] = orgnode;
                }
            }
            return DicOrgs;
        }

        //賽事資訊
        private void AppendMeetingInfo(TConfig cfg)
        {
            var aml = "<AML>" +
                "<Item type='In_Meeting' action='get' id='" + cfg.meeting_id + "'>" +
                "<Relationships>" +
                "<Item type='In_Meeting_Surveys' action='get' select='related_id(id,in_questions,in_request,in_selectoption,in_question_type,in_property,in_expense,in_is_pkey)'>" +
                "<in_surveytype>1</in_surveytype>" +
                "<in_property condition='in'>'in_l1','in_l2','in_l3'</in_property>" +
                "</Item>" +
                "</Relationships>" +
            "</Item></AML>"; 

            cfg.itmMeeting = cfg.inn.applyAML(aml);
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("查無 賽事資料");
            }
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.in_address = cfg.itmMeeting.getProperty("in_address", "");
            cfg.in_date_s = cfg.itmMeeting.getProperty("in_date_s", "");
            cfg.in_date_e = cfg.itmMeeting.getProperty("in_date_e", "");
            
            var in_date_s_dt = DateTime.Parse(cfg.in_date_s);
            var in_date_e_dt = DateTime.Parse(cfg.in_date_e);
            var time_date_s = string.Format("{0}年{1}月{2}", new System.Globalization.TaiwanCalendar().GetYear(in_date_s_dt), in_date_s_dt.Month, in_date_s_dt.Day);
            var time_date_e = string.Format("{2}日", new System.Globalization.TaiwanCalendar().GetYear(in_date_e_dt), in_date_e_dt.Month, in_date_e_dt.Day);

            cfg.day_range = time_date_s + "~" + time_date_e;
        }

        private void AppendExportInfo(TConfig cfg)
        {
            cfg.itmXlsx = cfg.inn.applyMethod("In_Variable_File", "<export_name>meeting_export_path</export_name><in_name>playerid_template_path_act</in_name>");
            cfg.template_path = cfg.itmXlsx.getProperty("template_path", "");
            cfg.export_path = cfg.itmXlsx.getProperty("export_path", "").TrimEnd('\\') + @"\" + cfg.meeting_id + @"\";
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }
            public string in_address { get; set; }
            public string in_date_s { get; set; }
            public string in_date_e { get; set; }
            public string day_range { get; set; }
            
            public Item itmXlsx { get; set; }
            public string template_path { get; set; }
            public string export_path { get; set; }
            public string export_type { get; set; }
            public string font_name { get; set; }
        }
    }
}