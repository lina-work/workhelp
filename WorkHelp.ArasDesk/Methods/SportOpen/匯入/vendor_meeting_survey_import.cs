﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using Xceed.Document.NET;
using System.CodeDom.Compiler;

namespace WorkHelp.ArasDesk.Methods.SportOpen.匯入
{
    internal class vendor_meeting_survey_import : Item
    {
        public vendor_meeting_survey_import(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 活動項目匯入
                輸入: meeting_id
                人員: lina 創建 (2024-10-15)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]vendor_meeting_survey_import";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
                FontName = "標楷體",
            };

            if (cfg.meeting_id == "")
            {
                throw new Exception("賽事 id 不得為空白");
            }

            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");

            switch (cfg.scene)
            {
                case "import":
                    ImportData(cfg, itmR);
                    break;

                case "download_sample":
                    DownloadSample(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void ImportData(TConfig cfg, Item itmReturn)
        {
            var json = itmReturn.getProperty("rows", "");
            var rows = GetRowData(cfg, json);
            if (rows == null || rows.Count == 0) throw new Exception("無匯入資料");

            SetSurveyId(cfg);

            RemoveSurveyOptions(cfg, cfg.l3_id);
            RemoveSurveyOptions(cfg, cfg.l2_id);
            RemoveSurveyOptions(cfg, cfg.l1_id);

            CreateSurveyOptions(cfg, rows);
            MatchL1SurveyOptions(cfg);
            MatchL2SurveyOptions(cfg);

            FixL2FilterId(cfg);
            FixL3FilterId(cfg);
        }

        private void FixL2FilterId(TConfig cfg)
        {
            var sql = @"
                UPDATE t1 SET
	                t1.in_filter_id = t12.opt_id
                FROM
	                IN_SURVEY_OPTION t1
                INNER JOIN
	                VU_MEETING_SVY_L2 t11
	                ON t11.opt_id = t1.id
                INNER JOIN
	                VU_MEETING_SVY_L1 t12
	                ON t12.source_id = t11.source_id
	                AND t12.in_value = t11.in_filter
                WHERE
	                t11.source_id = '{#source_id}'
            ";

            sql = sql.Replace("{#source_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        private void FixL3FilterId(TConfig cfg)
        {
            var sql = @"
                UPDATE t1 SET
	                t1.in_filter_id = t12.opt_id
                FROM
	                IN_SURVEY_OPTION t1
                INNER JOIN
	                VU_MEETING_SVY_L3 t11
	                ON t11.opt_id = t1.id
                INNER JOIN
	                VU_MEETING_SVY_L2 t12
	                ON t12.source_id = t11.source_id
	                AND t12.in_filter = t11.in_grand_filter
	                AND t12.in_value = t11.in_filter
                WHERE
	                t11.source_id = '{#source_id}'
            ";

            sql = sql.Replace("{#source_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        private void RemoveSurveyOptions(TConfig cfg, string source_id)
        {
            var sql = "DELETE FROM IN_SURVEY_OPTION WHERE source_id = '" + source_id + "'";
            cfg.inn.applySQL(sql);
        }

        private void MatchL1SurveyOptions(TConfig cfg)
        {
            var sql = "SELECT DISTINCT in_grand_filter FROM VU_MEETING_SVY_L3 WITH(NOLOCK) WHERE source_id = '" + cfg.meeting_id + "' AND ISNULL(in_grand_filter, '') <> ''";
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var x = new TRow
                {
                    in_l1 = item.getProperty("in_grand_filter", ""),
                    no_extend = true,
                };
                CreateLv1SurveyOption(cfg, x, (i + 1) * 1000);
            }
        }

        private void MatchL2SurveyOptions(TConfig cfg)
        {
            var sql = "SELECT DISTINCT in_grand_filter, in_filter FROM VU_MEETING_SVY_L3 WITH(NOLOCK) WHERE source_id = '" + cfg.meeting_id + "' AND ISNULL(in_grand_filter, '') <> '' AND ISNULL(in_filter, '') <> ''";
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var x = new TRow
                {
                    in_l1 = item.getProperty("in_grand_filter", ""),
                    in_l2 = item.getProperty("in_filter", ""),
                    no_extend = true,
                };
                CreateLv2SurveyOption(cfg, x, (i + 1) * 1000);
            }
        }

        private void CreateSurveyOptions(TConfig cfg, List<TRow> rows)
        {
            for (var i = 0; i < rows.Count; i++)
            {
                var x = rows[i];
                var is_lv3 = !string.IsNullOrWhiteSpace(x.in_l3);
                var is_lv2 = !string.IsNullOrWhiteSpace(x.in_l2);
                var is_lv1 = !string.IsNullOrWhiteSpace(x.in_l1);
                if (is_lv3)
                {
                    CreateLv3SurveyOption(cfg, x, (i + 1));
                }
                else if (is_lv2)
                {
                    CreateLv2SurveyOption(cfg, x, (i + 1));
                }
                else if (is_lv1)
                {
                    CreateLv1SurveyOption(cfg, x, (i + 1));
                }
            }
        }

        private void CreateLv1SurveyOption(TConfig cfg, TRow x, int sort_order)
        {
            ClearRowNull(x);

            var itmNew = cfg.inn.newItem("In_Survey_Option", "merge");

            itmNew.setAttribute("where", "source_id = '" + cfg.l1_id + "'"
                + " AND in_value = '" + x.in_l1 + "'"
                );

            itmNew.setProperty("source_id", cfg.l1_id);
            itmNew.setProperty("in_grand_filter", "");
            itmNew.setProperty("in_filter", "");
            itmNew.setProperty("in_value", x.in_l1);
            itmNew.setProperty("in_label", x.in_l1);

            SetCommonProperty(cfg, itmNew, x, sort_order);

            itmNew.apply();
        }

        private void CreateLv2SurveyOption(TConfig cfg, TRow x, int sort_order)
        {
            ClearRowNull(x);

            var itmNew = cfg.inn.newItem("In_Survey_Option", "merge");

            itmNew.setAttribute("where", "source_id = '" + cfg.l2_id + "'"
                + " AND in_filter = '" + x.in_l1 + "'"
                + " AND in_value = '" + x.in_l2 + "'"
                );

            itmNew.setProperty("source_id", cfg.l2_id);
            itmNew.setProperty("in_grand_filter", "");
            itmNew.setProperty("in_filter", x.in_l1);
            itmNew.setProperty("in_value", x.in_l2);
            itmNew.setProperty("in_label", x.in_l2);

            SetCommonProperty(cfg, itmNew, x, sort_order);
            
            itmNew.apply();
        }

        private void CreateLv3SurveyOption(TConfig cfg, TRow x, int sort_order)
        {
            ClearRowNull(x);

            var itmNew = cfg.inn.newItem("In_Survey_Option", "merge");

            itmNew.setAttribute("where", "source_id = '" + cfg.l3_id + "'"
                + " AND in_grand_filter = '" + x.in_l1 + "'"
                + " AND in_filter = '" + x.in_l2 + "'"
                + " AND in_value = '" + x.in_l3 + "'"
                );

            itmNew.setProperty("source_id", cfg.l3_id);
            itmNew.setProperty("in_grand_filter", x.in_l1);
            itmNew.setProperty("in_filter", x.in_l2);
            itmNew.setProperty("in_value", x.in_l3);
            itmNew.setProperty("in_label", x.in_l3);

            SetCommonProperty(cfg, itmNew, x, sort_order);

            itmNew.apply();
        }

        private void SetCommonProperty(TConfig cfg, Item itmNew, TRow x, int sort_order)
        {
            x.in_extend_value = GetExtend(cfg, x);

            var expense_early = GetInt(x.in_expense_early);
            x.in_early_yn = expense_early > 0 ? "1" : "0";
            if (x.in_early_yn == "1")
            {
                x.in_early_s = GetDtm(x.in_early_s, "yyyy/MM/dd", 0, "");
                x.in_early_e = GetDtm(x.in_early_e, "yyyy/MM/dd", 0, "");
            }

            itmNew.setProperty("in_expense_value", x.in_expense_value);
            itmNew.setProperty("in_expense_early", x.in_expense_early);
            itmNew.setProperty("in_expense_show", x.in_expense_show);
            itmNew.setProperty("in_extend_value", x.in_extend_value);

            itmNew.setProperty("in_early_yn", x.in_early_yn);
            itmNew.setProperty("in_early_s", x.in_early_s);
            itmNew.setProperty("in_early_e", x.in_early_e);

            itmNew.setProperty("in_unit", x.in_unit);
            itmNew.setProperty("in_link", x.in_link);
            itmNew.setProperty("in_subtitle", x.in_subtitle);
            if (x.in_quota > 0) itmNew.setProperty("in_quota", x.in_quota.ToString());

            itmNew.setProperty("sort_order", sort_order.ToString());
        }

        private void ClearRowNull(TRow x)
        {
            if (x.in_l1 == null) x.in_l1 = "";
            if (x.in_l2 == null) x.in_l2 = "";
            if (x.in_l3 == null) x.in_l3 = "";
            
            if (x.in_expense_value == null) x.in_expense_value = "";
            if (x.in_expense_early == null) x.in_expense_early = "";
            if (x.in_expense_show == null) x.in_expense_show = "";
            if (x.in_extend_value == null) x.in_extend_value = "";

            if (x.in_early_yn == null) x.in_early_yn = "";
            if (x.in_early_s == null) x.in_early_s = "";
            if (x.in_early_e == null) x.in_early_e = "";

            if (x.in_unit == null) x.in_unit = "";
            if (x.in_link == null) x.in_link = "";
            if (x.in_subtitle == null) x.in_subtitle = "";

            if (x.in_gender == null) x.in_gender = "";
            if (x.in_limit_s == null) x.in_limit_s = "";
            if (x.in_limit_e == null) x.in_limit_e = "";
            if (x.in_birth_s == null) x.in_birth_s = "";
            if (x.in_birth_e == null) x.in_birth_e = "";
        }

        private void SetSurveyId(TConfig cfg)
        {
            cfg.l1_id = "";
            cfg.l2_id = "";
            cfg.l3_id = "";

            var sql = @"
                SELECT 
	                t1.source_id
	                , t2.id AS 'svy_id'
	                , t2.in_property
                FROM
	                IN_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN
	                IN_SURVEY t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t2.in_property IN ('in_l1', 'in_l2', 'in_l3')
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var svy_id = item.getProperty("svy_id", "");
                var in_property = item.getProperty("in_property", "");
                switch (in_property)
                {
                    case "in_l1": cfg.l1_id = svy_id; break;
                    case "in_l2": cfg.l2_id = svy_id; break;
                    case "in_l3": cfg.l3_id = svy_id; break;
                }
            }
        }

        private string GetExtend(TConfig cfg, TRow x)
        {
            if (x.no_extend) return "";

            var limitS = GetInt(x.in_limit_s);
            var limitE = GetInt(x.in_limit_e);
            var dtS = GetDtm(x.in_birth_s, "yyyy/MM/dd", 0, "");
            var dtE = GetDtm(x.in_birth_e, "yyyy/MM/dd", 0, "");

            var list = new List<string>();
            if (x.in_gender != "")
            {
                list.Add("in_gender:" + x.in_gender);
            }
            if (x.in_birth_s != "" || x.in_birth_e != "")
            {
                list.Add("in_birth:" + dtS + "~" + dtE);
            }
            if (limitS > 0 || limitE > 0)
            {
                list.Add("in_limit:" + limitS + "~" + limitS);
            }
            if (list.Count == 0) return "";
            return string.Join("|", list);
        }

        private void DownloadSample(TConfig cfg, Item itmReturn)
        {
            var exp = ExportInfo(cfg, "survey_import_template");

            var book = new Spire.Xls.Workbook();
            book.LoadFromFile(exp.template_path);

            string ext_name = ".xlsx";
            string xls_folder = exp.export_path;
            string xls_name = cfg.mt_title + "_活動項目匯入_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");
            string xls_file = xls_folder + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            if (!System.IO.Directory.Exists(xls_folder))
            {
                System.IO.Directory.CreateDirectory(xls_folder);
            }

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, xls_file);

            book.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);

            itmReturn.setProperty("xls_name", xls_url);
        }

        private List<TRow> GetRowData(TConfig cfg, string json)
        {
            try
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject<List<TRow>>(json);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            Item itmXlsx = cfg.inn.applyMethod("In_Variable_File", "<export_name>meeting_export_path</export_name><in_name>" + in_name + "</in_name>");
            string export_path = itmXlsx.getProperty("export_path", "").TrimEnd('\\') + @"\" + cfg.meeting_id + @"\";
            string template_path = itmXlsx.getProperty("template_path", "");

            var result = new TExport
            {
                export_path = export_path,
                template_path = template_path,
            };

            return result;
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }
            public string l1_id { get; set; }
            public string l2_id { get; set; }
            public string l3_id { get; set; }
            public string FontName { get; set; }
        }

        private class TRow
        {
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_expense_value { get; set; }
            public string in_expense_early { get; set; }
            public string in_expense_show { get; set; }
            public string in_birth_s { get; set; }
            public string in_birth_e { get; set; }
            public string in_gender { get; set; }
            public string in_limit_s { get; set; }
            public string in_limit_e { get; set; }
            public string in_extend_value { get; set; }

            public string in_early_yn { get; set; }
            public string in_early_s { get; set; }
            public string in_early_e { get; set; }

            public int in_quota { get; set; }
            public string in_unit { get; set; }
            public string in_link { get; set; }
            public string in_subtitle { get; set; }

            public bool no_extend { get; set; }
        }

        private class TExport
        {
            public string template_path { get; set; }
            public string export_path { get; set; }
        }

        private int GetInt(string value, int def = 0)
        {
            if (value == "") return 0;
            int result = def;
            int.TryParse(value, out result);
            return result;
        }

        private string GetDtm(string value, string format, int hours, string defV = "")
        {
            if (value == "") return defV;
            DateTime result = DateTime.MinValue;
            if (DateTime.TryParse(value, out result))
            {
                return result.AddHours(hours).ToString(format);
            }
            return defV;
        }
    }
}