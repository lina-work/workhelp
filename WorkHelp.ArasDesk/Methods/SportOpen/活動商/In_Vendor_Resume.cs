﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using System.Net;

namespace WorkHelp.ArasDesk.Methods.SportOpen.活動商
{
    internal class In_Vendor_Resume : Item
    {
        public In_Vendor_Resume(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 活動商管理
    日誌: 
        - 2022-02-10: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Vendor_Resume";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                id = itmR.getProperty("id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "verify":
                    Verify(cfg, itmR);
                    break;
            }

            return itmR;
        }

        //活動商審核
        private void Verify(TConfig cfg, Item itmReturn)
        {
            var itmResume = cfg.inn.newItem("In_Resume", "get");
            itmResume.setProperty("id", cfg.id);
            itmResume = itmResume.apply();

            var in_vendor_verify = itmReturn.getProperty("in_vendor_verify", "");
            if (in_vendor_verify == "審核通過")
            {
                VerifyAgree(cfg, itmResume, itmReturn);
            }
            else if (in_vendor_verify == "審核不通過")
            {
                VerifyReject(cfg, itmResume, itmReturn);
            }
        }

        //活動商審核不通過
        private void VerifyReject(TConfig cfg, Item itmResume, Item itmReturn)
        {
            MergeUser(cfg, itmResume);

            var in_vendor_verify = itmReturn.getProperty("in_vendor_verify", "");
            var in_vendor_note = itmReturn.getProperty("in_vendor_note", "");

            var sql = "UPDATE IN_RESUME SET"
                + "  in_vendor_verify = N'" + in_vendor_verify + "'"
                + ", in_vendor_note = N'" + in_vendor_note + "'"
                + ", in_vendor_time = getutcdate()"
                + ", in_public_home = '0'"
                + ", logon_enabled = '0'"
                + " WHERE id = '" + cfg.id + "'";

            cfg.inn.applySQL(sql);
        }

        //活動商審核通過
        private void VerifyAgree(TConfig cfg, Item itmResume, Item itmReturn)
        {
            MergeUser(cfg, itmResume);

            var in_vendor_verify = itmReturn.getProperty("in_vendor_verify", "");
            var in_vendor_note = itmReturn.getProperty("in_vendor_note", "");

            var sql = "UPDATE IN_RESUME SET"
                + "  in_vendor_verify = N'" + in_vendor_verify + "'"
                + ", in_vendor_note = N'" + in_vendor_note + "'"
                + ", in_vendor_time = getutcdate()"
                + ", in_short_org = N'" + itmResume.getProperty("in_name", "") + "'"
                + ", in_org = '1'"
                + ", in_password_plain = '" + cfg.new_password + "'"
                + ", in_user_id = '" + cfg.new_user_id + "'"
                + ", owned_by_id = '" + cfg.new_identity_id + "'"
                + ", in_public_home = '1'"
                + ", in_public_sort = 9999"
                + ", in_public_name = N'" + itmResume.getProperty("in_name", "") + "'"
                + ", in_public_mvc = 'vendor'"
                + ", logon_enabled = '1'"
                + " WHERE id = '" + cfg.id + "'";

            cfg.inn.applySQL(sql);

            SendVerifyEMail(cfg);
        }

        private void MergeUser(TConfig cfg, Item itmResume)
        {
            string in_company = "leeway";
            string SequenceVal = itmResume.getProperty("in_sno", itmResume.getProperty("login_name", ""));

            Random rnd = new Random();
            string d1 = rnd.Next(0, 9).ToString();
            string d2 = rnd.Next(0, 9).ToString();
            string d3 = rnd.Next(0, 9).ToString();
            string d4 = rnd.Next(0, 9).ToString();

            string NewPwd = d1 + d2 + d3 + d4;
            string NewPwd_md5 = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(NewPwd, "md5");

            //建立使用者
            Item itmUser = cfg.inn.newItem("User", "merge");
            itmUser.setAttribute("where", "login_name='" + itmResume.getProperty("login_name", "") + "'");
            itmUser.setProperty("last_name", itmResume.getProperty("in_name", ""));//姓
            itmUser.setProperty("first_name", SequenceVal);//名
            itmUser.setProperty("user_no", SequenceVal);//員工編號
            itmUser.setProperty("login_name", itmResume.getProperty("in_sno"));//登入帳號
            itmUser.setProperty("cell", itmResume.getProperty("in_tel", ""));//行動電話
            itmUser.setProperty("email", itmResume.getProperty("in_email", ""));//電子郵件
            itmUser.setProperty("in_company", in_company);//公司別
            itmUser.setProperty("logon_enabled", "1");//可登入
            itmUser.setProperty("in_rank", cfg.inn.getItemByKeyedName("in_rank", "SYS-000000").getID());//職級
            itmUser.setProperty("in_app_page", "pages/c.aspx?page=In_MeetingSearch_light.html&method=In_Dashboard_MeetingList_light");
            itmUser.setProperty("password", NewPwd_md5);
            itmUser.setProperty("in_resume", cfg.id);
            itmUser = itmUser.apply();

            cfg.new_user_id = itmUser.getID();

            Item itmIdentity = cfg.inn.newItem("Identity", "get");
            itmIdentity.setProperty("in_user", cfg.new_user_id);
            itmIdentity = itmIdentity.apply();

            cfg.new_identity_id = itmIdentity.getID();
            cfg.new_password = NewPwd;

            //MeetingUser
            var itmMember = cfg.inn.newItem("Member", "merge");
            itmMember.setAttribute("where", "source_id='95277C725C954FEE959DCE2073DA1F9C' and related_id='" + cfg.new_identity_id + "'");
            itmMember.setProperty("source_id", "95277C725C954FEE959DCE2073DA1F9C");
            itmMember.setRelatedItem(itmIdentity);
            itmMember = itmMember.apply();

            //All Employees
            itmMember = cfg.inn.newItem("Member", "merge");
            itmMember.setAttribute("where", "source_id='B32BD81D1AD04207BF1E61E39A4E0E13' and related_id='" + cfg.new_identity_id + "'");
            itmMember.setProperty("source_id", "B32BD81D1AD04207BF1E61E39A4E0E13");
            itmMember.setRelatedItem(itmIdentity);
            itmMember = itmMember.apply();

            //ACT_GymOwner
            itmMember = cfg.inn.newItem("Member", "merge");
            itmMember.setAttribute("where", "source_id='7AC8B33DA0F246DDA70BB244AB231E29' and related_id='" + cfg.new_identity_id + "'");
            itmMember.setProperty("source_id", "7AC8B33DA0F246DDA70BB244AB231E29");
            itmMember.setRelatedItem(itmIdentity);
            itmMember = itmMember.apply();

            //成員角色
            Item itmResumeRole = cfg.inn.newItem("In_Resume_Resume", "merge");
            itmResumeRole.setAttribute("where", "source_id='" + cfg.id + "' and related_id='" + cfg.id + "'");
            itmResumeRole.setProperty("source_id", cfg.id);
            itmResumeRole.setProperty("in_resume_role", "999");
            itmResumeRole.setProperty("in_resume_remark", "主辦單位");
            itmResumeRole.setRelatedItem(itmResume);
            itmResumeRole = itmResumeRole.apply();
        }

        private void SendVerifyEMail(TConfig cfg)
        {
            try
            {
                var fullurl = "https://act.innosoft.com.tw/SportOpen/Vendor/SendVerifyResultMail?in_code_2=" + cfg.id;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(fullurl);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader tReader = new StreamReader(response.GetResponseStream());
                string mailResult = tReader.ReadToEnd();
                if (!string.IsNullOrWhiteSpace(mailResult))
                {
                    mailResult = mailResult.Replace("\"", "");
                }
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "新活動商審核結果通知");
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string id { get; set; }
            public string scene { get; set; }

            public string new_user_id { get; set; }
            public string new_identity_id { get; set; }
            public string new_password { get; set; }
        }
    }
}