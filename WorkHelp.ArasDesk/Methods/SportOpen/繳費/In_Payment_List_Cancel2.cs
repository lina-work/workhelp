﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.SportOpen.Payment
{
    public class In_Payment_List_Cancel2 : Item
    {
        public In_Payment_List_Cancel2(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 取消繳費單
                日誌: 
                    - 2024-05-30: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Payment_List_Cancel2";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                paynumbers = itmR.getProperty("paynumbers", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "cancel_payment":
                    CancelPayment(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void CancelPayment(TConfig cfg, Item itmReturn)
        {
            var itmPay = cfg.inn.applySQL("SELECT TOP 1 id FROM IN_MEETING_PAY WITH(NOLOCK) WHERE item_number = '" + cfg.paynumbers + "' ORDER BY created_on DESC");
            if (itmPay.isError() || itmPay.getResult() == "")
            {
                throw new Exception("查無繳費單");
            }

            var id = itmPay.getProperty("id", "");
            var sql = "UPDATE IN_MEETING_PAY SET pay_bool = N'已取消' WHERE id = '" + id + "'";
            cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string paynumbers { get; set; }
            public string scene { get; set; }
        }

    }
}
