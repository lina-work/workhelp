﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.SportOpen.Fight
{
    public class in_meeting_program_swimming : Item
    {
        public in_meeting_program_swimming(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 設定賽制
                日期: 
                    2023-12-21: 創建游泳版本 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_program_swimming";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                in_l1 = itmR.getProperty("in_l1", ""),
                in_l2 = itmR.getProperty("in_l2", ""),
                in_l3 = itmR.getProperty("in_l3", ""),
                in_fight_day = itmR.getProperty("in_fight_day", ""),
                scene = itmR.getProperty("scene", ""),
            };

            cfg.VariableMap = GetVariableMap(cfg);

            cfg.in_water_way = cfg.VariableMap.ContainsKey("fight_site_count")
                ? GetInt(cfg.VariableMap["fight_site_count"])
                : 8;

            // cfg.meeting_id = "452A3610AC744ED6A2F77BAEE98245A3";
            // cfg.scene = "create";

            switch (cfg.scene)
            {
                case "reset_tree_rank":
                    ResetTreeRank(cfg, itmR);
                    break;

                case "initial_variable":
                    InitialVariables(cfg, itmR);
                    break;

                case "clear_times_events":
                    ClearFightDayTimeEvents(cfg, itmR);
                    break;

                case "save_times_events":
                    SaveFightDayTimeEvents(cfg, itmR);
                    break;

                case "clear_days_events":
                    ClearFightDayEvents(cfg, itmR);
                    break;

                case "save_days_events":
                    SaveFightDayEvents(cfg, itmR);
                    break;

                case "clear_program_groups":
                    ClearProgramGroups(cfg, itmR);
                    break;

                case "save_program_groups":
                    SaveProgramGroups(cfg, itmR);
                    break;

                case "edit_foot_player":
                    EditFootPlayer(cfg, itmR);
                    break;

                case "assign_draw_no":
                    AssignDrawNo(cfg, itmR);
                    break;

                case "draw_program":
                    RunDraw(cfg, itmR);
                    break;

                case "edit_prop":
                    EditProperty(cfg, itmR);
                    break;

                case "time_event_edit":
                    EditTimeEvent(cfg, itmR);
                    break;

                case "time_edit":
                    EditTime(cfg, itmR);
                    break;

                //case "edit":
                //    ResetProgram(cfg, itmR);
                //    break;

                case "create":
                    //組別建檔
                    CreatePrograms(cfg, itmR);
                    //隊別建檔、場次
                    CreateProgramTeams(cfg, itmR);
                    break;

                case "clear":
                    ClearMeeting(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void InitialVariables(TConfig cfg, Item itmReturn)
        {
            var sql = "SELECT id FROM IN_MEETING_VARIABLE WITH(NOLOCK) WHERE source_id = '" + cfg.meeting_id + "' AND in_key = 'fight_site_address'";
            var itmOld = cfg.inn.applySQL(sql);
            if (!itmOld.isError() && itmOld.getResult() != "")
            {
                throw new Exception("參數已初始化");
            }

            var rows = new List<TEditRow>();
            rows.Add(new TEditRow { prop = "fight_site_address", value = "新竹縣游泳館", no = 100 });
            rows.Add(new TEditRow { prop = "fight_day_count", value = "5", no = 200 });
            rows.Add(new TEditRow { prop = "fight_site_distance", value = "50,100,200,400,800,1500", no = 300 });
            rows.Add(new TEditRow { prop = "fight_site_count", value = "8", no = 400 });
            rows.Add(new TEditRow { prop = "fight_battle_type", value = "WaterWay-8", no = 500 });
            rows.Add(new TEditRow { prop = "fight_robin_player", value = "8", no = 600 });
            rows.Add(new TEditRow { prop = "fight_type_array", value = "H,F", no = 700 });
            rows.Add(new TEditRow { prop = "fight_day_array", value = "2023-04-22,2023-04-23,2023-04-24,2023-04-25,2023-04-26", no = 2100 });
            rows.Add(new TEditRow { prop = "has_setted", value = "是", no = 90000 });

            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];

                var cond = "source_id = '" + cfg.meeting_id + "'"
                    + " AND in_key = N'" + row.prop + "'";

                var itmNew = cfg.inn.newItem("IN_MEETING_VARIABLE", "merge");
                itmNew.setAttribute("where", cond);
                itmNew.setProperty("source_id", cfg.meeting_id);
                itmNew.setProperty("in_key", row.prop);
                itmNew.setProperty("in_value", row.value);
                itmNew.setProperty("sort_order", row.no.ToString());
                itmNew.apply();
            }
        }


        private void ClearFightDayTimeEvents(TConfig cfg, Item itmReturn)
        {
            //清空當日賽程
            var sql1 = "DELETE FROM [IN_MEETING_PTIME]"
                + " WHERE [in_meeting] = '" + cfg.meeting_id + "'"
                + " AND [in_fight_day] = '" + cfg.in_fight_day + "'";

            cfg.inn.applySQL(sql1);
        }

        private void SaveFightDayTimeEvents(TConfig cfg, Item itmReturn)
        {
            var data = itmReturn.getProperty("data", "");
            var rows = GetEditRows(cfg, data);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, rows.Count.ToString());

            //清空當日賽程
            var sql1 = "DELETE FROM [IN_MEETING_PTIME]"
                + " WHERE [in_meeting] = '" + cfg.meeting_id + "'"
                + " AND [in_fight_day] = '" + cfg.in_fight_day + "'";

            cfg.inn.applySQL(sql1);

            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];

                var cond = "in_meeting = '" + cfg.meeting_id + "'"
                    + " AND in_fight_day = '" + cfg.in_fight_day + "'"
                    + " AND in_time_num = '" + row.no + "'";

                var itmNew = cfg.inn.newItem("IN_MEETING_PTIME", "merge");
                itmNew.setAttribute("where", cond);
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_fight_day", cfg.in_fight_day);
                itmNew.setProperty("in_time_num", row.no.ToString());

                if (!string.IsNullOrWhiteSpace(row.id))
                {
                    itmNew.setProperty("in_type", "event");
                    itmNew.setProperty("in_event", row.id);
                    itmNew.setProperty("in_title", row.time);
                    itmNew.setProperty("in_label", row.label);
                    itmNew = itmNew.apply();
                }
                else if (!string.IsNullOrWhiteSpace(row.label))
                {
                    itmNew.setProperty("in_type", GetTimeType(row.label));
                    itmNew.setProperty("in_event", null);
                    itmNew.setProperty("in_title", row.time);
                    itmNew.setProperty("in_label", row.label);
                    itmNew = itmNew.apply();
                }
            }
        }

        private string GetTimeType(string value)
        {
            if (string.IsNullOrWhiteSpace(value)) return "";
            switch (value)
            {
                case "計時賽": return "trial";
                case "預備場次": return "spare";
                case "休息": return "lunch";
                case "頒獎": return "awards";
                default: return "";
            }
        }

        private void ClearFightDayEvents(TConfig cfg, Item itmReturn)
        {
            //清空已上日期的場次
            var sql1 = "UPDATE [IN_MEETING_PEVENT] SET"
                + "  [in_fight_day] = NULL"
                + ", [in_show_serial] = [in_tree_no]"
                + " WHERE [in_meeting] = '" + cfg.meeting_id + "'";

            cfg.inn.applySQL(sql1);
        }

        private void SaveFightDayEvents(TConfig cfg, Item itmReturn)
        {
            var data = itmReturn.getProperty("data", "");
            var rows = GetEditRows(cfg, data);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, rows.Count.ToString());

            //清空已上日期的場次
            var sql1 = "UPDATE [IN_MEETING_PEVENT] SET"
                + "  [in_fight_day] = NULL"
                + ", [in_show_serial] = [in_tree_no]"
                + " WHERE [in_meeting] = '" + cfg.meeting_id + "'";

            cfg.inn.applySQL(sql1);

            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];

                var sql2 = "UPDATE [IN_MEETING_PEVENT] SET"
                    + "  [in_fight_day] = '" + row.value + "'"
                    + ", [in_show_serial] = " + row.no
                    + " WHERE [id] = '" + row.id + "'";

                cfg.inn.applySQL(sql2);
            }
        }

        private void ClearProgramGroups(TConfig cfg, Item itmReturn)
        {
            var sql = "UPDATE [IN_MEETING_PROGRAM] SET"
            + " [in_group_name] = NULL"
            + " WHERE [in_meeting] = '" + cfg.meeting_id + "'";

            cfg.inn.applySQL(sql);
        }

        private void SaveProgramGroups(TConfig cfg, Item itmReturn)
        {
            var data = itmReturn.getProperty("data", "");
            var rows = GetEditRows(cfg, data);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, rows.Count.ToString());
            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                var g = row.value;
                var n = row.no;

                var sql = "";
                if (g == null || g == "" || g == "w" || g == "W")
                {
                    sql = "UPDATE [IN_MEETING_PROGRAM] SET [in_group_name] = NULL, [in_show_serial] = [in_sort_order]"
                        + " WHERE [id] = '" + row.id + "'";
                }
                else
                {
                    g = g.Trim().ToUpper();
                    sql = "UPDATE [IN_MEETING_PROGRAM] SET [in_group_name] = '" + g + "', [in_show_serial] = " + n
                        + " WHERE [id] = '" + row.id + "'";
                }
                cfg.inn.applySQL(sql);
            }
        }

        private void EditFootPlayer(TConfig cfg, Item itmReturn)
        {
            var program_id = itmReturn.getProperty("program_id", "");
            var detail_id = itmReturn.getProperty("detail_id", "");
            var in_sign_no = itmReturn.getProperty("in_sign_no", "");
            var in_pteam = itmReturn.getProperty("in_pteam", "");

            var b1 = in_sign_no == "" || in_sign_no == "0";
            var b2 = in_pteam == "";

            if (b1 && b2)
            {
                //清除籤號與選手
                var sql3 = "UPDATE [In_Meeting_PEvent_Detail] SET [in_sign_no] = NULL, [in_pteam] = NULL WHERE [id] = '" + detail_id + "'";
                cfg.inn.applySQL(sql3);
                return;
            }

            var sql = "SELECT id, in_sign_no FROM IN_MEETING_PTEAM WITH(NOLOCK)"
            + " WHERE source_id = '" + program_id + "'"
            + " AND id = '" + in_pteam + "'";

            var itmOld = cfg.inn.applySQL(sql);
            if (itmOld.isError() || itmOld.getResult() == "")
            {
                throw new Exception("查無選手資料");
            }

            var old_team_id = itmOld.getProperty("id", "");
            var old_sign_no = itmOld.getProperty("in_sign_no", "");

            var sql2 = "";

            if (old_sign_no != "" && old_sign_no != "0")
            {
                sql2 = "UPDATE [In_Meeting_PEvent_Detail] SET [in_sign_no] = '" + old_sign_no + "', [in_pteam] = NULL WHERE [id] = '" + detail_id + "'";
            }
            else
            {
                sql2 = "UPDATE [In_Meeting_PEvent_Detail] SET [in_pteam] = '" + old_team_id + "', [in_sign_no] = NULL WHERE [id] = '" + detail_id + "'";
            }

            cfg.inn.applySQL(sql2);
        }

        private Dictionary<string, int> ProgramDrawNoMap(TConfig cfg
            , string in_battle_type
            , string in_tree_key)
        {
            var map = new Dictionary<string, int>();

            var sql = "SELECT id, in_key, in_sign_no FROM IN_MEETING_DRAWSIGNNO WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_battle_type = '" + in_battle_type + "'"
                + " AND in_tree_key = '" + in_tree_key + "'";

            var items = cfg.inn.applySQL(sql);

            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_key = item.getProperty("in_key", "");
                var in_sign_no = item.getProperty("in_sign_no", "");
                if (in_key == "") continue;
                if (map.ContainsKey(in_key)) continue;

                map.Add(in_key, GetInt(in_sign_no));
            }

            return map;
        }

        private void AssignDrawNo(TConfig cfg, Item itmReturn)
        {
            var in_battle_type = itmReturn.getProperty("in_battle_type", "");
            var in_tree_key = itmReturn.getProperty("in_tree_key", "");
            var in_key = itmReturn.getProperty("in_key", "");
            var in_sign_no = itmReturn.getProperty("in_sign_no", "");

            var cond = "in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_battle_type = '" + in_battle_type + "'"
                + " AND in_tree_key = '" + in_tree_key + "'"
                + " AND in_key = '" + in_key + "'";

            var itmNew = cfg.inn.newItem("In_Meeting_DrawSignNo", "merge");
            itmNew.setAttribute("where", cond);
            itmNew.setProperty("in_meeting", cfg.meeting_id);
            itmNew.setProperty("in_battle_type", in_battle_type);
            itmNew.setProperty("in_tree_key", in_tree_key);
            itmNew.setProperty("in_key", in_key);
            itmNew.setProperty("in_sign_no", in_sign_no);

            itmNew.apply();
        }

        private void RunDraw(TConfig cfg, Item itmReturn)
        {
            var sql = "SELECT id, in_name, in_l1, in_l2, in_l3, in_team_count FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            var itmProgram = cfg.inn.applySQL(sql);
            if (itmProgram.isError() || itmProgram.getResult() == "")
            {
                throw new Exception("查無組別資料");
            }

            //隊伍數
            var teamCount = GetInt(itmProgram.getProperty("in_team_count", "0"));
            var drawCount = teamCount > 12 ? 12 : teamCount;

            var items = GetHeatTeams(cfg, cfg.program_id);
            if (items.Count == 0)
            {
                throw new Exception("查無資料");
            }

            var rows = MapDrawRows(cfg, items);
            var sportType = InnSport.Core.Enums.SportEnum.Taekwondo;
            var sameTeamSeparate = "Y";

            //執行抽籤
            var entities = ExecuteDraw1(cfg
                , rows
                , sportType
                , needSeedLottery: "N"
                , sameTeamSeparate: sameTeamSeparate
                , seed14SameFace: "N"
                , robinPlayerCount: 2);

            //更新籤號
            UpdateDrawNo(cfg, itmProgram, entities);

            for (var i = 0; i < entities.Count; i++)
            {
                var entity = entities[i];
                var item = cfg.inn.newItem("inn_draw");
                item.setProperty("in_name", entity.in_name);
                item.setProperty("in_current_org", entity.in_current_org);
                item.setProperty("in_sign_no", entity.in_draw_no);
                item.setProperty("in_draw_no", entity.in_draw_no);
                itmReturn.addRelationship(item);
            }

            // var items2 = GetHeatTeams(cfg, teamCount);
            // var count = items2.getItemCount();
            // for (var i = 0; i < count; i++)
            // {
            //     var item = items2.getItemByIndex(i);
            //     item.setType("inn_draw");
            //     item.setProperty("in_draw_no", item.getProperty("in_sign_no", ""));
            //     itmReturn.addRelationship(item);
            // }
        }

        /// <summary>
        /// 執行抽籤
        /// </summary>
        private List<InnSport.Core.Models.Output.Item> ExecuteDraw1(
            TConfig cfg
            , List<InnSport.Core.Models.Output.Item> resource
            , InnSport.Core.Enums.SportEnum sportType
            , string needSeedLottery = "Y"
            , string sameTeamSeparate = "Y"
            , string seed14SameFace = "N"
            , int robinPlayerCount = 2)
        {
            var ns = InnSport.Core.Utilities.TUtility.RandomArray(resource.Count)
                .ToList();

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, Newtonsoft.Json.JsonConvert.SerializeObject(ns));

            for (var i = 0; i < resource.Count; i++)
            {
                var row = resource[i];
                row.in_draw_no = ns[i].ToString();
            }

            return resource;
        }

        /// <summary>
        /// 執行抽籤
        /// </summary>
        private List<InnSport.Core.Models.Output.Item> ExecuteDraw2(
            TConfig cfg
            , List<InnSport.Core.Models.Output.Item> resource
            , InnSport.Core.Enums.SportEnum sportType
            , string needSeedLottery = "Y"
            , string sameTeamSeparate = "Y"
            , string seed14SameFace = "N"
            , int robinPlayerCount = 2)
        {
            var draw = new InnSport.Core.Models.Business.TConfig();
            draw.SportType = sportType;
            draw.SourceType = InnSport.Core.Enums.SourceEnum.List;
            draw.Resource = resource;
            draw.DrawType = "2";
            draw.Integral = needSeedLottery; //種子籤: Y
            draw.Draw99 = "N";//99號籤: Y
            draw.SameTeamSeparate = sameTeamSeparate == "Y";
            draw.Seed14SameFace = seed14SameFace == "Y";
            draw.RobinPlayerCount = robinPlayerCount;
            return draw.ExecuteList();
        }

        private void UpdateDrawNo(TConfig cfg, Item itmProgram, List<InnSport.Core.Models.Output.Item> rows)
        {
            var in_l1 = itmProgram.getProperty("in_l1", "");
            var in_l2 = itmProgram.getProperty("in_l2", "");
            var in_l3 = itmProgram.getProperty("in_l3", "");

            //清空該組籤號
            var sql1 = "UPDATE [IN_MEETING_PTEAM] SET"
                + "   [in_sign_no] = NULL"
                + " WHERE source_id = '" + cfg.program_id + "'";

            cfg.inn.applySQL(sql1);

            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];

                var sql2 = "UPDATE [IN_MEETING_PTEAM] SET"
                    + "  [in_sign_no] = " + row.in_draw_no
                    + ", [in_sign_time] = GETUTCDATE()"
                    + " WHERE id = '" + row.key + "'";

                cfg.inn.applySQL(sql2);

                var sql3 = "UPDATE [IN_MEETING_USER] SET"
                    + "   [in_draw_no] = " + row.in_draw_no
                    + " WHERE source_id = '" + cfg.meeting_id + "'"
                    + " AND in_l1 = N'" + in_l1 + "'"
                    + " AND in_l2 = N'" + in_l2 + "'"
                    + " AND in_l3 = N'" + in_l3 + "'"
                    + " AND in_index = '" + row.in_index + "'";

                cfg.inn.applySQL(sql3);

            }

            var sql4 = "UPDATE [IN_MEETING_PROGRAM] SET"
                + " [in_sign_time] = GETUTCDATE()"
                + " WHERE id = '" + cfg.program_id + "'";

            cfg.inn.applySQL(sql4);

        }

        private List<InnSport.Core.Models.Output.Item> MapDrawRows(TConfig cfg, List<Item> items)
        {
            var rows = new List<InnSport.Core.Models.Output.Item>();
            var count = items.Count;
            for (var i = 0; i < count; i++)
            {
                var item = items[i];
                var row = new InnSport.Core.Models.Output.Item
                {
                    id = i + 1,
                    in_section_name = item.getProperty("source_id", ""),
                    key = item.getProperty("id", ""),
                    team_key = item.getProperty("in_index", ""),
                    in_index = item.getProperty("in_index", ""),
                    in_name = item.getProperty("in_name", ""),
                    in_current_org = item.getProperty("in_current_org", ""),
                    in_draw_no = item.getProperty("in_sign_no", ""),
                    in_seed_lottery = item.getProperty("in_seeds", ""),
                    in_points = 0,//GetInt(item.getProperty("in_seeds_points", "0")),
                };
                rows.Add(row);
            }

            return rows;
        }

        private List<Item> GetHeatTeams(TConfig cfg, string program_id)
        {
            var sql = @"
				SELECT 
					source_id
					, id
					, in_name
					, in_current_org
					, in_index
					, in_sign_no
					, in_seeds
					, ISNULL(in_seeds_points, 0)	AS 'in_seeds_points'
                    , in_entry_score1
                    , in_entry_score2
				FROM 
					[IN_MEETING_PTEAM] WITH(NOLOCK) 
				WHERE 
					source_id = '{#program_id}'
				ORDER BY 
					in_entry_score1
					, in_index
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            var list = new List<Item>();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                list.Add(item);
            }
            return list;
        }

        private void EditProperty(TConfig cfg, Item itmReturn)
        {
            var itemType = itmReturn.getProperty("itemType", "").ToUpper();
            var prop = itmReturn.getProperty("property", "").ToLower();
            var id = itmReturn.getProperty("id", "");
            var value = itmReturn.getProperty("value", "");

            var itmData = cfg.inn.applySQL("SELECT * FROM " + itemType + " WITH(NOLOCK) WHERE id = '" + id + "'");
            if (itmData.isError() || itmData.getResult() == "")
            {
                throw new Exception("查無資料");
            }

            var sql = "UPDATE [" + itemType + "] SET [" + prop + "] = N'" + value + "', modified_on = getutcdate() WHERE [id] = '" + id + "'";
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            cfg.inn.applySQL(sql);

            if (itemType == "IN_MEETING_PEVENT_DETAIL")
            {
                if (prop == "in_score" || prop == "in_rank")
                {
                    OverrideTeamProp(cfg, itmData, prop, value);
                }
            }
        }

        private void OverrideTeamProp(TConfig cfg, Item item, string keyword, string value)
        {
            var program_id = item.getProperty("in_program", "");
            var event_id = item.getProperty("source_id", "");
            var team_id = item.getProperty("in_pteam", "");
            var in_fight_id = item.getProperty("in_fight_id", "");
            var in_sign_no = item.getProperty("in_sign_no", "");

            var itmTeam = GetTeamItem(cfg, program_id, team_id, in_sign_no);
            if (itmTeam == null || itmTeam.isError() || itmTeam.getResult() == "") return;

            var id = itmTeam.getProperty("id", "");
            var suffix = keyword.Contains("score") ? "score" : "rank";
            var prop = GetTeamScoreProp(in_fight_id, suffix);

            var sql = "UPDATE [IN_MEETING_PTEAM] SET [" + prop + "] = '" + value + "' WHERE id = '" + id + "'";
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "OverrideTeamTimes" + Environment.NewLine + sql);
            cfg.inn.applySQL(sql);
        }

        private string GetTeamScoreProp(string in_fight_id, string suffix)
        {
            switch (in_fight_id)
            {
                case "TIME":
                    return "in_time_trial_" + suffix;

                case "H":
                case "H1":
                case "H2":
                case "H3":
                case "H4":
                case "H5":
                case "預賽":
                    return "in_heat_" + suffix;

                case "R":
                case "R1":
                case "R2":
                case "R3":
                    return "in_rpc_" + suffix;

                case "SA":
                case "SB":
                case "SC":
                    return "in_semifinal_" + suffix;

                case "計時決賽":
                case "計時決賽慢組":
                case "計時決賽快組":
                case "決賽":
                    return "in_final_" + suffix;

                default:
                    return "";
            }
        }

        private Item GetTeamItem(TConfig cfg, string program_id, string id, string in_sign_no)
        {
            var signNo = GetInt(in_sign_no);
            var sql = "";
            if (id != "")
            {
                sql = "SELECT * FROM IN_MEETING_PTEAM WITH(NOLOCK) WHERE id = '" + id + "'";
            }
            else if (signNo > 0)
            {
                sql = "SELECT * FROM IN_MEETING_PTEAM WITH(NOLOCK) WHERE source_id = '" + program_id + "' AND in_sign_no = " + signNo;
            }

            if (sql == "")
            {
                return null;
            }
            else
            {
                return cfg.inn.applySQL(sql);
            }

        }

        private void EditTimeEvent(TConfig cfg, Item itmReturn)
        {
            var in_type = "event";
            var in_time_num = itmReturn.getProperty("in_time_num", "");
            var in_event = itmReturn.getProperty("in_event", "");

            var itmNew = cfg.inn.newItem("IN_MEETING_PTIME", "merge");
            itmNew.setAttribute("where", "in_meeting = '" + cfg.meeting_id + "' AND in_fight_day = '" + cfg.in_fight_day + "' AND in_time_num = '" + in_time_num + "' AND in_type = '" + in_type + "'");
            itmNew.setProperty("in_meeting", cfg.meeting_id);
            itmNew.setProperty("in_fight_day", cfg.in_fight_day);
            itmNew.setProperty("in_time_num", in_time_num);
            itmNew.setProperty("in_event", in_event);
            itmNew.setProperty("in_type", in_type);
            // itmNew.setProperty("in_title", "設定");
            // itmNew.setProperty("in_label", "設定");
            itmNew = itmNew.apply();
        }

        private void EditTime(TConfig cfg, Item itmReturn)
        {
            var in_type = "setting";

            var itmNew = cfg.inn.newItem("IN_MEETING_PTIME", "merge");
            itmNew.setAttribute("where", "in_meeting = '" + cfg.meeting_id + "' AND in_fight_day = '" + cfg.in_fight_day + "' AND in_type = '" + in_type + "'");
            itmNew.setProperty("in_meeting", cfg.meeting_id);
            itmNew.setProperty("in_fight_day", cfg.in_fight_day);
            itmNew.setProperty("in_type", in_type);
            itmNew.setProperty("in_title", "設定");
            itmNew.setProperty("in_label", "設定");
            CopyItemValue(itmNew, itmReturn, "in_am_start");
            CopyItemValue(itmNew, itmReturn, "in_am_end");
            CopyItemValue(itmNew, itmReturn, "in_am_span");
            CopyItemValue(itmNew, itmReturn, "in_pm_start");
            CopyItemValue(itmNew, itmReturn, "in_pm_end");
            CopyItemValue(itmNew, itmReturn, "in_pm_span");
            CopyItemValue(itmNew, itmReturn, "in_group_name");
            itmNew = itmNew.apply();
        }

        private void ClearMeeting(TConfig cfg, Item itmReturn)
        {
            var sql = "";

            sql = "DELETE t1 FROM IN_MEETING_PEVENT_DETAIL t1"
                + " WHERE EXISTS(SELECT* FROM IN_MEETING_PEVENT t2 WITH(NOLOCK) WHERE t2.in_meeting = '{#meeting_id}' AND t2.id = t1.source_id)";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            cfg.inn.applySQL(sql);


            sql = "DELETE FROM IN_MEETING_PEVENT WHERE in_meeting = '{#meeting_id}'";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            cfg.inn.applySQL(sql);


            sql = "DELETE FROM IN_MEETING_PTIME WHERE in_meeting = '{#meeting_id}'";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            cfg.inn.applySQL(sql);


            sql = "DELETE FROM IN_MEETING_PTEAM WHERE in_meeting = '{#meeting_id}'";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            cfg.inn.applySQL(sql);


            sql = "DELETE FROM IN_MEETING_PROGRAM WHERE in_meeting = '{#meeting_id}'";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            cfg.inn.applySQL(sql);
        }

        private void EditProgram(TConfig cfg, string program_id, Item itmReturn)
        {
            var sql = "UPDATE IN_MEETING_PROGRAM SET"
                + "   in_group_name = '" + itmReturn.getProperty("in_group_name", "") + "'"
                + " WHERE id = '" + program_id + "'";

            cfg.inn.applySQL(sql);
        }

        //組別建檔
        private void CreatePrograms(TConfig cfg, Item itmReturn)
        {
            var itmPrograms = ProgramListFromMeetingUser(cfg);
            if (itmPrograms.isError() || itmPrograms.getResult() == "")
            {
                throw new Exception("查無組別資料");
            }

            var registerCountMap = MapProgramRegisterCount(cfg);
            var teamCountMap = MapProgramTeamCount(cfg);
            var programs = MapProgramList(cfg, itmPrograms, registerCountMap, teamCountMap);
            for (var i = 0; i < programs.Count; i++)
            {
                var program = programs[i];
                if (program.id == "")
                {
                    NewProgram(cfg, program);
                }
                else
                {
                    UpdProgram(cfg, program);
                }
            }
        }

        private void CreateProgramTeams(TConfig cfg, Item itmReturn)
        {
            var map = GetProgramMUsers(cfg);
            foreach (var kv in map)
            {
                var program = kv.Value;

                //隊別建檔
                RebuildTeams(cfg, program);

                //修正參賽成績名次
                ResetEntryRank(cfg, program.id, "in_entry_score1", "in_entry_rank");

                //場次清除
                RemoveOldEvents(cfg, program.id);

                //場次建檔
                RebuildEvents(cfg, program);
            }
        }

        //更新名次
        private void ResetTreeRank(TConfig cfg, Item itmReturn)
        {
            var in_tree_name = itmReturn.getProperty("in_tree_name", "");
            var score_property = itmReturn.getProperty("score_property", "");
            var rank_property = itmReturn.getProperty("rank_property", "");

            if (in_tree_name == "") throw new Exception("參數錯誤: in_tree_name");
            if (score_property == "") throw new Exception("參數錯誤: score_property");
            if (rank_property == "") throw new Exception("參數錯誤: rank_property");

            RunResetTreeRank(cfg, in_tree_name, score_property, rank_property);

            switch (in_tree_name)
            {
                case "預賽":
                    HeatPromotion(cfg); //晉級
                    break;

                case "決賽":
                    FinalScore(cfg);
                    break;

                case "計時決賽":
                    FinalScore(cfg);
                    break;

                case "計時決賽慢組":
                case "計時決賽快組":
                    HeatCloneToFinal(cfg);
                    break;
            }
        }

        //決賽
        private void FinalScore(TConfig cfg)
        {
            var sql2 = "UPDATE IN_MEETING_PTEAM SET "
                + "     in_final_rank = NULL"
                + " WHERE source_id = '" + cfg.program_id + "'"
                + " AND in_final_rank > 8";

            cfg.inn.applySQL(sql2);


            var sql3 = "UPDATE IN_MEETING_PTEAM SET "
                + "     in_final_rank = NULL"
                + " WHERE source_id = '" + cfg.program_id + "'"
                + " AND ISNULL(in_final_score, '') IN ('', '0', '-')";

            cfg.inn.applySQL(sql3);


            var sql4 = "UPDATE IN_MEETING_PTEAM SET "
                + "     in_rank = in_final_rank"
                + " WHERE source_id = '" + cfg.program_id + "'";

            cfg.inn.applySQL(sql4);
        }

        //預賽晉級
        private void HeatPromotion(TConfig cfg)
        {
            var sql1 = "UPDATE IN_MEETING_PTEAM SET "
                + "    in_final_yn = NULL"
                + " WHERE source_id = '" + cfg.program_id + "'";

            cfg.inn.applySQL(sql1);

            var sql2 = @"
                SELECT 
	                id
	                , in_name
	                , in_current_org
	                , in_heat_score
	                , in_heat_rank 
                FROM
	                IN_MEETING_PTEAM WITH(NOLOCK)
                WHERE
	                source_id = '{#program_id}'
	                AND ISNULL(in_heat_rank, 9999) <= 10
                ORDER BY
	                in_heat_rank
            ";

            sql2 = sql2.Replace("{#program_id}", cfg.program_id);
            var items = cfg.inn.applySQL(sql2);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var id = item.getProperty("id", "");
                var rank = GetInt(item.getProperty("in_heat_rank", "0"));
                if (rank <= 0) continue;

                var yn = rank <= 8 ? "q" : "候補";
                var lane = GetLaneNumber(rank);

                //更新選手晉級狀態
                var sql3 = "UPDATE IN_MEETING_PTEAM SET "
                    + "   in_final_yn = '" + yn + "'"
                    + " WHERE id = '" + id + "'";

                cfg.inn.applySQL(sql3);

                if (rank > 8) continue;

                var key = GetDetailKey("決賽", 1, lane);

                //晉級到決賽場次
                var sql4 = "UPDATE IN_MEETING_PEVENT_DETAIL SET "
                    + "   in_pteam = '" + id + "'"
                    + " WHERE in_program = '" + cfg.program_id + "'"
                    + " AND in_key = N'" + key + "'";

                cfg.inn.applySQL(sql4);
            }
        }

        //把預賽成績填入決賽成績
        private void HeatCloneToFinal(TConfig cfg)
        {
            var sql1 = "UPDATE IN_MEETING_PTEAM SET "
                + "  in_final_score = in_heat_score"
                + ", in_final_status = in_heat_status"
                + " WHERE source_id = '" + cfg.program_id + "'";

            cfg.inn.applySQL(sql1);


            ResetEntryRank(cfg, cfg.program_id, "in_final_score", "in_final_rank");


            var sql2 = "UPDATE IN_MEETING_PTEAM SET "
                + "     in_final_rank = NULL"
                + " WHERE source_id = '" + cfg.program_id + "'"
                + " AND in_final_rank > 8";

            cfg.inn.applySQL(sql2);


            var sql3 = "UPDATE IN_MEETING_PTEAM SET "
                + "     in_final_rank = NULL"
                + " WHERE source_id = '" + cfg.program_id + "'"
                + " AND ISNULL(in_final_score, '') IN ('', '0', '-')";

            cfg.inn.applySQL(sql3);


            var sql4 = "UPDATE IN_MEETING_PTEAM SET "
                + "     in_rank = in_final_rank"
                + " WHERE source_id = '" + cfg.program_id + "'";

            cfg.inn.applySQL(sql4);

        }

        //更新名次
        private void RunResetTreeRank(TConfig cfg, string in_tree_name, string score_property, string rank_property)
        {
            var sql = @"
                SELECT
	                t1.in_tree_name
	                , t1.in_fight_id
	                , t2.in_key
	                , t2.in_pteam AS 'team_id'
	                , t3.{#score_property}
                    , ROW_NUMBER() OVER (ORDER BY CASE WHEN t3.{#score_property} IS NULL THEN 1 ELSE 0 END, t3.{#score_property}) AS 'rn'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PTEAM t3 WITH(NOLOCK)
	                ON t3.id = t2.in_pteam
                WHERE
	                t1.source_id = '{#program_id}'
	                AND t1.in_tree_name = N'{#in_tree_name}'
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id)
                .Replace("{#in_tree_name}", in_tree_name)
                .Replace("{#score_property}", score_property);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var team_id = item.getProperty("team_id", "");
                var score = item.getProperty(score_property, "");
                var rn = item.getProperty("rn", "0");

                if (team_id == "") continue;

                var sql_upd = "";
                if (score == "" || score == "-" || rn == "" || rn == "0")
                {
                    sql_upd = "UPDATE IN_MEETING_PTEAM SET [" + rank_property + "] = NULL WHERE id = '" + team_id + "'";
                }
                else
                {
                    sql_upd = "UPDATE IN_MEETING_PTEAM SET [" + rank_property + "] = " + rn + " WHERE id = '" + team_id + "'";
                }
                cfg.inn.applySQL(sql_upd);
            }
        }

        //修正名次
        private void ResetEntryRank(TConfig cfg, string program_id, string score_property, string rank_property)
        {
            var sql1 = "UPDATE IN_MEETING_PTEAM SET [" + score_property + "] = NULL"
                 + " WHERE source_id = '" + program_id + "'"
                 + " AND ISNULL([" + score_property + "], '') = ''";

            cfg.inn.applySQL(sql1);

            var sql2 = @"
                UPDATE t1 SET 
                	t1.{#rank_property} = t2.rn
                FROM 
                	IN_MEETING_PTEAM t1
                INNER JOIN
                (
                	SELECT 
                		id
                		, ROW_NUMBER() OVER (ORDER BY CASE WHEN {#score_property} IS NULL THEN 1 ELSE 0 END, {#score_property}) AS 'rn'
                	FROM 
                		IN_MEETING_PTEAM WITH(NOLOCK)
                	WHERE 
                		source_id = '{#program_id}'
                ) t2 ON t2.id = t1.id
            ";

            sql2 = sql2.Replace("{#program_id}", program_id)
                .Replace("{#score_property}", score_property)
                .Replace("{#rank_property}", rank_property);

            cfg.inn.applySQL(sql2);
        }

        //場次清除
        private void RemoveOldEvents(TConfig cfg, string program_id)
        {
            var sql = "";

            sql = "DELETE t1 FROM IN_MEETING_PEVENT_DETAIL t1"
                + " WHERE EXISTS(SELECT* FROM IN_MEETING_PEVENT t2 WITH(NOLOCK) WHERE t2.source_id = '{#program_id}' AND t2.id = t1.source_id)";

            sql = sql.Replace("{#program_id}", program_id);

            cfg.inn.applySQL(sql);


            sql = "DELETE FROM IN_MEETING_PEVENT WHERE source_id = '{#program_id}'";

            sql = sql.Replace("{#program_id}", program_id);

            cfg.inn.applySQL(sql);
        }

        //場次建檔
        private void RebuildEvents(TConfig cfg, TProgram program)
        {
            //隊伍清單
            var itmTeams = GetHeatTeams(cfg, program.id);
            if (itmTeams.Count == 0) throw new Exception("查無隊伍資料");

            var arrSort = program.in_schedule_mode.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var arrName = program.in_schedule_value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            if (itmTeams.Count <= 8)
            {
                var itmEvent = NewEvent(cfg, program, arrSort[0], arrName[0], "1");
                CreateDetails(cfg, program, itmEvent, itmTeams);
            }
            else if (program.in_schedule_value.Contains("慢組"))
            {
                var itmTeamsS = new List<Item>();//Slow
                var itmTeamsF = new List<Item>();//Fast
                for (int i = 0; i < itmTeams.Count; i++)
                {
                    if (i < cfg.in_water_way)
                    {
                        itmTeamsF.Add(itmTeams[i]);
                    }
                    else
                    {
                        itmTeamsS.Add(itmTeams[i]);
                    }
                }

                var itmEventS = NewEvent(cfg, program, arrSort[0], arrName[0], "1");
                var itmEventF = NewEvent(cfg, program, arrSort[1], arrName[1], "2");
                //慢組
                CreateDetails(cfg, program, itmEventS, itmTeamsS);
                //快組
                CreateDetails(cfg, program, itmEventF, itmTeamsF);
            }
            else
            {
                var itmEventH = NewEvent(cfg, program, arrSort[0], arrName[0], "1");
                var itmEventF = NewEvent(cfg, program, arrSort[1], arrName[1], "2");
                //預賽
                CreateAllocateDetails(cfg, program, itmEventH, itmTeams);
                //決賽
                CreateFinalDetails(cfg, program, itmEventF);
            }
        }

        private void CreateAllocateDetails(TConfig cfg, TProgram program, Item itmEvent, List<Item> itmTeams)
        {
            var in_fight_id = itmEvent.getProperty("in_fight_id", "");
            var source_id = itmEvent.getProperty("id", "");

            var total = itmTeams.Count;
            var lanes = cfg.in_water_way;
            var topGroups = 3;
            var minLimit = 3;

            var groups = MapSwimmingGroups(total, lanes, topGroups, minLimit);
            var groupCount = groups.Count;

            CreateDetails(cfg, program, itmEvent, null, groupCount, total);

            for (var i = 0; i < groups.Count; i++)
            {
                var group = groups[i];
                var groupNo = groups.Count - i;

                for (var j = 0; j < group.Count; j++)
                {
                    var serial = j + 1;
                    var lane = GetLaneNumber(serial);

                    var n = group[j];
                    var idx = n - 1;

                    var itmTeam = itmTeams[idx];
                    var in_pteam = itmTeam.getProperty("id", "");
                    var in_note = itmTeam.getProperty("in_entry_score1", "");

                    var key = GetDetailKey(in_fight_id, groupNo, lane);

                    var sql = "UPDATE [IN_MEETING_PEVENT_DETAIL] SET"
                        + "  [in_sign_bypass] = '0'"
                        + ", [in_pteam] = '" + in_pteam + "'"
                        + ", [in_note] = '" + in_note + "'"
                        + " WHERE [source_id] = '" + source_id + "'"
                        + " AND [in_key] = N'" + key + "'";

                    cfg.inn.applySQL(sql);
                }
            }

        }

        private static List<List<int>> MapSwimmingGroups(int total, int lanes, int topGroups, int minLimit)
        {
            var rows = new List<int>();
            for (var i = 1; i <= total; i++) rows.Add(i);

            var result = new List<List<int>>();
            if (total <= lanes)
            {
                result.Add(rows);
                return result;
            }

            var groupA = new List<List<int>>();
            var groupB = new List<List<int>>();
            var groupE = new List<int>();

            //已排序: 快到慢
            var quotient = total / lanes;//商數
            var remainder = total % lanes;//餘數
            var divisible = remainder == 0;//整除
            var groupCount = divisible ? quotient : quotient + 1;

            var a1 = 0;
            var a2 = topGroups;
            if (a2 > groupCount) a2 = groupCount;

            for (var i = a1; i < a2; i++)
            {
                groupA.Add(new List<int>());
            }

            //1. 處理最後一組
            if (groupCount > topGroups)
            {
                var seek = remainder;
                if (divisible) seek = lanes;
                else if (seek < minLimit) seek = minLimit;
                for (var i = 0; i < seek; i++)
                {
                    //從最後抓
                    var idx = rows.Count - 1;
                    var item = rows[idx];
                    groupE.Insert(0, item);
                    rows.RemoveAt(idx);
                }
            }

            var topRows = lanes * topGroups;// 8 個水道 * 3 組

            if (rows.Count > topRows)
            {
                var rowsA = new List<int>();
                var rowsB = new List<int>();

                rowsA.AddRange(rows.Take(24));
                rowsB.AddRange(rows.Skip(24));

                var code = groupA.Count;
                for (var i = 0; i < rowsA.Count; i++)
                {
                    var row = rowsA[i];
                    var idx = i % code;
                    groupA[idx].Add(row);
                }

                for (var i = 0; i < rowsB.Count; i++)
                {
                    var row = rowsB[i];
                    var idx = i / lanes;
                    var cnt = idx + 1;
                    if (groupB.Count < cnt)
                    {
                        groupB.Add(new List<int>());
                    }
                    groupB[idx].Add(row);
                }
            }
            else
            {
                var code = groupA.Count;
                for (var i = 0; i < rows.Count; i++)
                {
                    var row = rows[i];
                    var idx = i % code;
                    groupA[idx].Add(row);
                }
            }

            foreach (var ns in groupA)
            {
                if (ns.Count > 0) result.Add(ns);
            }

            foreach (var ns in groupB)
            {
                if (ns.Count > 0) result.Add(ns);
            }

            if (groupE.Count > 0) result.Add(groupE);

            return result;
        }

        private void CreateFinalDetails(TConfig cfg, TProgram program, Item itmEvent)
        {
            var in_fight_id = itmEvent.getProperty("in_fight_id", "");
            var lanes = cfg.in_water_way;
            for (var i = 1; i <= lanes; i++)
            {
                var round = 1;
                var lane = i;
                var key = GetDetailKey(in_fight_id, 1, lane);

                var itmNew = cfg.inn.newItem("In_Meeting_PEvent_Detail", "add");
                itmNew.setProperty("source_id", itmEvent.getProperty("id", ""));
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_program", program.id);
                itmNew.setProperty("in_fight_id", in_fight_id);
                itmNew.setProperty("in_sign_foot", lane.ToString()); //水道
                itmNew.setProperty("in_sign_no", null);

                itmNew.setProperty("in_no", lane.ToString());
                itmNew.setProperty("in_round", round.ToString());
                itmNew.setProperty("in_lane", lane.ToString());
                itmNew.setProperty("in_key", key);
                itmNew.setProperty("in_serial", lane.ToString());
                itmNew.setProperty("in_sign_bypass", "");

                itmNew.apply();
            }
        }

        private void CreateDetails(TConfig cfg, TProgram program, Item itmEvent, List<Item> itmTeams)
        {
            var total = itmTeams.Count;
            var lanes = cfg.in_water_way;

            var quotient = total / lanes;//商數
            var remainder = total % lanes;//餘數
            var divisible = remainder == 0;//整除
            var groupCount = divisible ? quotient : quotient + 1;

            CreateDetails(cfg, program, itmEvent, itmTeams, groupCount, total);
        }

        private void CreateDetails(TConfig cfg, TProgram program, Item itmEvent, List<Item> itmTeams, int groupCount, int total)
        {
            var in_fight_id = itmEvent.getProperty("in_fight_id", "");
            var lanes = cfg.in_water_way;

            if (itmTeams != null)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, program.id + ": " + itmTeams.Count);
            }

            for (var i = 0; i < groupCount; i++)
            {
                var groupNo = groupCount - i;

                for (var j = 0; j < lanes; j++)
                {
                    var no = i * lanes + j + 1;

                    var serial = j + 1;
                    var lane = GetLaneNumber(serial);
                    var key = GetDetailKey(in_fight_id, groupNo, lane);

                    var itmNew = cfg.inn.newItem("In_Meeting_PEvent_Detail", "add");
                    itmNew.setProperty("source_id", itmEvent.getProperty("id", ""));
                    itmNew.setProperty("in_meeting", cfg.meeting_id);
                    itmNew.setProperty("in_program", program.id);
                    itmNew.setProperty("in_fight_id", in_fight_id);
                    itmNew.setProperty("in_sign_foot", lane.ToString()); //水道
                    itmNew.setProperty("in_sign_no", null);

                    itmNew.setProperty("in_no", no.ToString());
                    itmNew.setProperty("in_round", groupNo.ToString());
                    itmNew.setProperty("in_lane", lane.ToString());
                    itmNew.setProperty("in_key", key);
                    itmNew.setProperty("in_serial", serial.ToString());

                    if (itmTeams != null && no <= total)
                    {
                        var idx = no - 1;
                        var itmTeam = itmTeams[idx];
                        itmNew.setProperty("in_sign_bypass", "0");
                        itmNew.setProperty("in_pteam", itmTeam.getProperty("id", ""));
                        itmNew.setProperty("in_note", itmTeam.getProperty("in_entry_score1", ""));
                    }
                    else
                    {
                        itmNew.setProperty("in_sign_bypass", "1");
                    }

                    itmNew.apply();
                }
            }
        }

        private string GetDetailKey(string schedule, int groupNo, int laneNo)
        {
            return schedule + "-R" + groupNo + "-W" + laneNo;
        }

        private Item NewEvent(TConfig cfg, TProgram program, string in_tree_sort, string in_fight_id, string in_tree_no)
        {
            var itmNew = cfg.inn.newItem("In_Meeting_PEvent", "add");
            //itmNew.setAttribute("where", "source_id = '" + program.id + "' AND in_fight_id = '" + in_fight_id + "'");

            itmNew.setProperty("in_meeting", cfg.meeting_id);
            itmNew.setProperty("source_id", program.id);
            itmNew.setProperty("in_fight_id", in_fight_id);

            itmNew.setProperty("in_tree_name", in_fight_id);
            itmNew.setProperty("in_tree_sort", in_tree_sort);
            itmNew.setProperty("in_tree_no", in_tree_no);
            itmNew.setProperty("in_show_serial", in_tree_no);
            itmNew.setProperty("in_show_no", in_tree_no);

            return itmNew.apply();
        }

        private static int GetLaneNumber(int serial)
        {
            switch (serial)
            {
                case 1: return 4;
                case 2: return 5;
                case 3: return 3;
                case 4: return 6;
                case 5: return 2;
                case 6: return 7;
                case 7: return 1;
                case 8: return 8;
                default: return 0;
            }
        }

        private static List<int> SplitNumArray(int total, int lanes)
        {
            var list = new List<int>();
            if (total <= 0) return list;

            var quotient = total / lanes;//商數
            var remainder = total % lanes;//餘數

            if (quotient == 0)
            {
                list.Add(total);
                return list;
            }

            for (int i = 0; i < quotient; i++)
            {
                list.Add(lanes);
            }

            if (remainder == 0) return list;

            list.Add(lanes);

            //EX: 33/8 = 4 + 1 = 5

            var idxP = list.Count - 2;
            var idxE = list.Count - 1;//尾數

            var half = lanes / 2;
            if (remainder < half)
            {
                var sub = remainder + lanes;
                var q2 = sub / 2;
                var r2 = sub % 2;
                if (r2 == 0)
                {
                    list[idxP] = q2;
                    list[idxE] = q2;
                }
                else
                {
                    list[idxP] = q2 + 1;
                    list[idxE] = q2;
                }
            }
            else
            {
                list[idxE] = remainder;
            }
            return list;
        }

        private class TDetail
        {
            public int no { get; set; } // 序號
            public int round { get; set; } // 第幾排
            public int lane { get; set; } // 道次
            public string key { get; set; }
        }

        //隊別建檔
        private void RebuildTeams(TConfig cfg, TProgram program)
        {
            foreach (var team in program.teams)
            {
                var players = new List<TPlayer>();
                if (team.player1.Count > 0) players.AddRange(team.player1);
                if (team.player2.Count > 0) players.AddRange(team.player2);

                if (players.Count > 1)
                {
                    team.in_sno = "";
                    team.in_gender = "";
                    team.in_waiting_list = "";
                    //team.in_entry_score1 = "";

                    var names = string.Join(", ", players.Select(x => x.display));
                    team.in_name = names;
                    team.in_names = names;
                    team.in_team_players = players.Count.ToString();
                }
                else
                {
                    team.in_names = team.in_name;
                }

                MergeProgramTeam(cfg, program.id, team);
            }
        }

        private void MergeProgramTeam(TConfig cfg, string program_id, TTeam team)
        {
            var itmNew = cfg.inn.newItem("IN_MEETING_PTEAM", "merge");
            itmNew.setAttribute("where", "source_id = '" + program_id + "' AND in_team_key = '" + team.in_team_key + "'");
            itmNew.setProperty("in_meeting", cfg.meeting_id);
            itmNew.setProperty("source_id", program_id);
            itmNew.setProperty("in_team_key", team.in_team_key);

            itmNew.setProperty("in_current_org", team.in_current_org);
            itmNew.setProperty("in_short_org", team.in_short_org);
            itmNew.setProperty("in_team", team.in_team);
            itmNew.setProperty("in_index", team.in_index);
            itmNew.setProperty("in_creator_sno", team.in_creator_sno);

            itmNew.setProperty("in_name", team.in_name);
            itmNew.setProperty("in_sno", team.in_sno);
            itmNew.setProperty("in_gender", team.in_gender);
            //itmNew.setProperty("in_waiting_list", team.in_waiting_list);
            itmNew.setProperty("in_entry_score1", team.in_entry_score1);

            itmNew.setProperty("in_names", team.in_names);
            itmNew.setProperty("in_team_players", team.in_team_players);

            itmNew = itmNew.apply();
        }

        private Dictionary<string, TProgram> GetProgramMUsers(TConfig cfg)
        {
            var cond = cfg.program_id != ""
                ? "AND t2.id = '" + cfg.program_id + "'"
                : "";

            var sql = @"
                SELECT 
	               t1.*
				   , t2.id AS 'program_id'
				   , t2.in_battle_type
				   , t2.in_tree_key
				   , t2.in_team_count
				   , t2.in_time_trial
				   , t2.in_schedule_mode
				   , t2.in_schedule_value
                FROM 
	                VU_FIGHT_MUSERS t1
				INNER JOIN
					IN_MEETING_PROGRAM t2 WITH(NOLOCK)
					ON t2.in_meeting = t1.source_id
					AND t2.in_name = t1.program_key
                WHERE 
	                t1.source_id = '{#meeting_id}' 
	                {#cond}
                ORDER BY 
	                t1.in_l1_sort
	                , t1.in_l2_sort
	                , t1.in_l3_sort
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#cond}", cond);

            var map = new Dictionary<string, TProgram>();
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var program_id = item.getProperty("program_id", "");
                var team_key = item.getProperty("team_key", "");

                var program = default(TProgram);
                if (map.ContainsKey(program_id))
                {
                    program = map[program_id];
                }
                else
                {
                    program = new TProgram
                    {
                        id = program_id,
                        in_l1 = item.getProperty("in_l1", ""),
                        in_l2 = item.getProperty("in_l2", ""),
                        in_l3 = item.getProperty("in_l3", ""),
                        in_battle_type = item.getProperty("in_battle_type", ""),
                        in_tree_key = item.getProperty("in_tree_key", ""),
                        in_team_count = item.getProperty("in_team_count", ""),
                        in_time_trial = item.getProperty("in_time_trial", ""),
                        in_schedule_mode = item.getProperty("in_schedule_mode", ""),
                        in_schedule_value = item.getProperty("in_schedule_value", ""),
                        teams = new List<TTeam>(),
                    };

                    map.Add(program_id, program);
                }

                var player = NewPlayer(item);
                var team = program.teams.Find(x => x.in_team_key == team_key);
                if (team == null)
                {
                    team = new TTeam
                    {
                        in_team_key = team_key,
                        in_current_org = player.in_current_org,
                        in_short_org = player.in_short_org,
                        in_team = player.in_team,
                        in_index = player.in_index,
                        in_creator_sno = player.in_creator_sno,

                        in_name = player.in_name,
                        in_sno = player.in_sno,
                        in_gender = player.in_gender,
                        in_waiting_list = player.in_waiting_list,
                        in_entry_score1 = player.in_entry_score1,

                        player1 = new List<TPlayer>(),
                        player2 = new List<TPlayer>(),
                    };
                    program.teams.Add(team);
                }

                if (player.isBack)
                {
                    team.player2.Add(player);
                }
                else
                {
                    team.player1.Add(player);
                }
            }

            return map;
        }

        private string GetProgramName2(TProgram program)
        {
            var suffix = program.in_l3.Replace("高男", "")
                .Replace("高女", "")
                .Replace("國男", "")
                .Replace("國女", "");

            switch (program.in_l2)
            {
                case "公開男子組": return "公男-" + suffix;
                case "公開女子組": return "公女-" + suffix;
                case "高中男子組": return "高男-" + suffix;
                case "高中女子組": return "高女-" + suffix;
                case "國中男子組": return "國男-" + suffix;
                case "國中女子組": return "國女-" + suffix;

                case "高中一年級組":
                    if (program.in_l3.Contains("男")) return "高一男-" + suffix;
                    if (program.in_l3.Contains("女")) return "高一女-" + suffix;
                    return "高一-" + suffix;

                case "國中一年級組":
                    if (program.in_l3.Contains("男")) return "國一男-" + suffix;
                    if (program.in_l3.Contains("女")) return "國一女-" + suffix;
                    return "國一-" + suffix;

                default: return program.in_l2 + '-' + program.in_l3;
            }
        }

        private TPlayer NewPlayer(Item item)
        {
            var result = new TPlayer
            {
                in_current_org = item.getProperty("in_current_org", ""),
                in_short_org = item.getProperty("in_short_org", ""),
                in_team = item.getProperty("in_team", ""),
                in_index = item.getProperty("in_index", ""),
                in_creator_sno = item.getProperty("in_creator_sno", ""),

                in_name = item.getProperty("in_name", ""),
                in_sno = item.getProperty("in_sno", ""),
                in_gender = item.getProperty("in_gender", ""),
                in_waiting_list = item.getProperty("in_waiting_list", ""),

                in_paddle = item.getProperty("in_paddle", ""),
                in_entry_score1 = item.getProperty("in_entry_score1", ""),

                isBack = false,
            };

            if (result.in_short_org == "")
            {
                result.in_short_org = result.in_current_org;
            }

            result.display = result.in_name;

            // if (result.in_paddle != "")
            // {
            //     result.display += "-" + result.in_paddle;
            // }

            if (result.in_waiting_list == "備取")
            {
                result.isBack = true;
                result.display += "(備)";
            }

            return result;
        }

        private void UpdProgram(TConfig cfg, TProgram program)
        {
            var sql = "UPDATE IN_MEETING_PROGRAM SET"
                + "  in_name2 = '" + program.in_name2 + "'"
                + ", in_register_count = '" + program.in_register_count + "'"
                + ", in_team_count = '" + program.in_team_count + "'"
                + ", in_event_count = '" + program.in_event_count + "'"
                + ", in_time_trial = '" + program.in_time_trial + "'"
                + ", in_schedule_mode = '" + program.in_schedule_mode + "'"
                + ", in_schedule_value = '" + program.in_schedule_value + "'"
                + ", in_battle_type = '" + program.in_battle_type + "'"
                + ", in_tree_key = '" + program.in_tree_key + "'"
                + " WHERE id = '" + program.id + "'";

            cfg.inn.applySQL(sql);
        }

        private void NewProgram(TConfig cfg, TProgram program)
        {
            var itmData = program.value;

            var itmNew = cfg.inn.newItem("In_Meeting_Program", "add");
            itmNew.setProperty("in_meeting", cfg.meeting_id);
            itmNew.setProperty("in_name", program.key);
            itmNew.setProperty("in_name2", program.in_name2);
            //itmNew.setProperty("in_name3", program.in_l3);
            //itmNew.setProperty("in_display", program.key);
            itmNew.setProperty("in_short_name", GetShortName(program));

            itmNew.setProperty("in_l1", program.in_l1);
            itmNew.setProperty("in_l2", program.in_l2);
            itmNew.setProperty("in_l3", program.in_l3);
            itmNew.setProperty("in_register_count", program.in_register_count);
            itmNew.setProperty("in_team_count", program.in_team_count);
            itmNew.setProperty("in_event_count", program.in_event_count);

            itmNew.setProperty("in_time_trial", program.in_time_trial);
            itmNew.setProperty("in_schedule_mode", program.in_schedule_mode);
            itmNew.setProperty("in_schedule_value", program.in_schedule_value);

            itmNew.setProperty("in_battle_type", program.in_battle_type);
            itmNew.setProperty("in_tree_key", program.in_tree_key);

            //CopyItemValue(itmNew, itmData, "in_l1_sort");
            //CopyItemValue(itmNew, itmData, "in_l2_sort");
            //CopyItemValue(itmNew, itmData, "in_l3_sort");
            itmNew.setProperty("in_sort_order", GetShortOrder(program));

            var itmResult = itmNew.apply();
            if (!itmResult.isError() && itmResult.getResult() != "")
            {
                program.id = itmResult.getID();
            }
        }

        private Dictionary<string, string> MapProgramRegisterCount(TConfig cfg)
        {
            var result = new Dictionary<string, string>();

            var sql = @"
                SELECT 
	                in_l1
	                , in_l2
	                , in_l3
	                , COUNT(*) AS 'cnt' 
                FROM 
	                VU_FIGHT_MUSERS
                WHERE 
	                source_id = '{#meeting_id}' 
                GROUP BY 
	                in_l1
	                , in_l2
	                , in_l3
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var key = GetProgramKey(item);
                var cnt = item.getProperty("cnt", "0");
                if (result.ContainsKey(key)) continue;
                result.Add(key, cnt);
            }
            return result;

        }

        private Dictionary<string, string> MapProgramTeamCount(TConfig cfg)
        {
            var result = new Dictionary<string, string>();

            var sql = @"
                SELECT 
	                in_l1
	                , in_l2
	                , in_l3
	                , COUNT(*) AS 'cnt' 
                FROM 
	                VU_FIGHT_MTEAMS 
                WHERE 
	                source_id = '{#meeting_id}' 
                GROUP BY 
	                in_l1
	                , in_l2
	                , in_l3
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var key = GetProgramKey(item);
                var cnt = item.getProperty("cnt", "0");
                if (result.ContainsKey(key)) continue;
                result.Add(key, cnt);
            }
            return result;

        }

        private Item ProgramListFromMeetingUser(TConfig cfg)
        {
            var cond = cfg.program_id != ""
                ? "AND t2.id = '" + cfg.program_id + "'"
                : "";

            var sql = @"
                SELECT 
	                t1.*
	                , ROW_NUMBER() OVER (PARTITION BY t1.source_id ORDER BY t1.in_l1_sort, t1.in_l2_sort, t1.in_l3_sort) AS 'rn'
	                , t2.id AS 'program_id'
                FROM 
	                VU_FIGHT_PROGRAMS t1
                LEFT OUTER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.in_meeting = t1.source_id
	                AND ISNULL(t2.in_l1, '') = ISNULL(t1.in_l1, '')
	                AND ISNULL(t2.in_l2, '') = ISNULL(t1.in_l2, '')
	                AND ISNULL(t2.in_l3, '') = ISNULL(t1.in_l3, '')
                WHERE
	                t1.source_id = '{#meeting_id}'
                    {#cond}
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#cond}", cond);

            return cfg.inn.applySQL(sql);
        }

        private Item GetProgramItem(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                *
                FROM 
	                IN_MEETING_PROGRAM WITH(NOLOCK)
                WHERE
	                in_meeting = '{#meeting_id}'
	                AND in_l1 = N'{#in_l1}'
	                AND in_l2 = N'{#in_l2}'
	                AND in_l3 = N'{#in_l3}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", cfg.in_l1)
                .Replace("{#in_l2}", cfg.in_l2)
                .Replace("{#in_l3}", cfg.in_l3);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private void CopyItemValue(Item itmA, Item itmB, string propA, string propB = "")
        {
            if (propB == "")
            {
                itmA.setProperty(propA, itmB.getProperty(propA, ""));
            }
            else
            {
                itmA.setProperty(propA, itmB.getProperty(propB, ""));
            }
        }

        private string GetProgramKey(Item item)
        {
            var in_l1 = item.getProperty("in_l1", "");
            var in_l2 = item.getProperty("in_l2", "");
            var in_l3 = item.getProperty("in_l3", "");
            return string.Join("-", new List<string> { in_l1, in_l2, in_l3 });
        }

        private string GetShortOrder(TProgram program)
        {
            var rn = GetInt(program.value.getProperty("rn", "0")) * 100;
            return rn.ToString();
        }

        private string GetShortName(TProgram program)
        {
            try
            {
                if (program.in_l3 == "") return "";

                var value = program.in_l3
                    .Replace("（", "(")
                    .Replace("）", ")")
                    .Replace("\n", "")
                    .Replace("\t", "")
                    .Replace("\r", "");

                value = value.Trim();

                var arr = value.Split('(');
                if (arr == null || arr.Length < 2) return program.in_l3;

                var result = arr[1].Replace(")", "")
                    .Replace("/", "");

                return result;
            }
            catch
            {
                return program.in_l3;
            }
        }

        private List<TProgram> MapProgramList(TConfig cfg, Item items, Dictionary<string, string> regMap, Dictionary<string, string> teamMap)
        {
            var result = new List<TProgram>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var program = new TProgram
                {
                    id = item.getProperty("program_id", ""),
                    in_l1 = item.getProperty("in_l1", ""),
                    in_l2 = item.getProperty("in_l2", ""),
                    in_l3 = item.getProperty("in_l3", ""),
                    key = GetProgramKey(item),
                    value = item,
                };

                program.in_name2 = GetProgramName2(program);
                program.in_register_count = FindTeamCount(regMap, program.key);
                program.in_team_count = FindTeamCount(teamMap, program.key);

                ResetProgramBySchduleMode(program, GetInt(program.in_team_count));

                var team_count = GetInt(program.in_team_count);
                program.in_battle_type = GetBattleType(team_count);
                program.in_tree_key = GetTreeKey(team_count);

                result.Add(program);
            }

            return result;
        }

        private string GetBattleType(int team_count)
        {
            return "WaterWay-8";
        }

        private string GetTreeKey(int team_count)
        {
            if (team_count >= 9) return "schedule-09";
            return "schedule-08";
        }

        private Dictionary<string, string> GetVariableMap(TConfig cfg)
        {
            var map = new Dictionary<string, string>();

            var sql = @"
                SELECT 
	                in_key
	                , in_value 
                FROM 
	                IN_MEETING_VARIABLE WITH(NOLOCK) 
                WHERE 
	                source_id = '{#meeting_id}'
                ORDER BY
	                sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_key = item.getProperty("in_key", "");
                var in_value = item.getProperty("in_value", "");
                if (map.ContainsKey(in_key)) continue;
                map.Add(in_key, in_value);
            }

            return map;
        }

        private void ResetProgramBySchduleMode(TProgram program, int count)
        {
            var b1 = program.in_l3.Contains("800");
            var b2 = program.in_l3.Contains("1500");
            if (b1 || b2)
            {
                if (count <= 8)
                {
                    program.in_schedule_mode = "1000";
                    program.in_schedule_value = "計時決賽";
                }
                else
                {
                    program.in_schedule_mode = "1000,2500";
                    program.in_schedule_value = "計時決賽慢組,計時決賽快組";
                }
            }
            else
            {
                if (count <= 8)
                {
                    program.in_schedule_mode = "3000";
                    program.in_schedule_value = "決賽";
                }
                else
                {
                    program.in_schedule_mode = "2000,3000";
                    program.in_schedule_value = "預賽,決賽";
                }
            }

            var evts = program.in_schedule_value.Split(',');
            program.in_event_count = evts.Length.ToString();
        }

        private string FindTeamCount(Dictionary<string, string> map, string key)
        {
            if (map.ContainsKey(key)) return map[key];
            return "0";
        }

        private List<TEditRow> GetEditRows(TConfig cfg, string json)
        {
            try
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject<List<TEditRow>>(json);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private class TEditRow
        {
            public int no { get; set; }
            public string id { get; set; }
            public string value { get; set; }
            public string prop { get; set; }
            public string time { get; set; }
            public string label { get; set; }
            public string itemType { get; set; }
        }

        private class TProgram
        {
            public string id { get; set; }
            public string key { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_name2 { get; set; }
            public string in_register_count { get; set; }
            public string in_team_count { get; set; }
            public string in_event_count { get; set; }

            public string in_time_trial { get; set; }
            public string in_schedule_mode { get; set; }
            public string in_schedule_value { get; set; }

            public string in_battle_type { get; set; }
            public string in_tree_key { get; set; }

            public Item value { get; set; }

            public List<TTeam> teams { get; set; }
        }

        private class TTeam
        {
            public string in_team_key { get; set; }
            public string in_current_org { get; set; }
            public string in_short_org { get; set; }
            public string in_team { get; set; }
            public string in_index { get; set; }
            public string in_creator_sno { get; set; }

            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_gender { get; set; }
            public string in_waiting_list { get; set; }
            public string in_entry_score1 { get; set; }

            public string in_names { get; set; }
            public string in_team_players { get; set; }

            public List<TPlayer> player1 { get; set; }
            public List<TPlayer> player2 { get; set; }
        }

        private class TPlayer
        {
            public string in_current_org { get; set; }
            public string in_short_org { get; set; }
            public string in_team { get; set; }
            public string in_index { get; set; }
            public string in_creator_sno { get; set; }

            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_gender { get; set; }
            public string in_waiting_list { get; set; }

            public string in_paddle { get; set; }
            public string in_entry_score1 { get; set; }

            public string display { get; set; }

            public bool isBack { get; set; }
            public Item value { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }

            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_fight_day { get; set; }
            public string scene { get; set; }
            public int in_water_way { get; set; }

            public Dictionary<string, string> VariableMap { get; set; }

        }

        private class TTree
        {
            public string name { get; set; }
            public string desc { get; set; }
            public int sort { get; set; }
        }

        private int GetInt(string value, int defV = 0)
        {
            if (value == "" || value == "0") return 0;

            int result = defV;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return defV;
        }
    }
}