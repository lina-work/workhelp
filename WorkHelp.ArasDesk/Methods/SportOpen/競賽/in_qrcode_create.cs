﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using System.Net;
using System.Security.AccessControl;

namespace WorkHelp.ArasDesk.Methods.SportOpen.競賽
{
    internal class in_qrcode_create : Item
    {
        public in_qrcode_create(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 競賽連結中心-建立連結
                輸入: meeting_id
                日期: 
                    2021-12-09: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_qrcode_create";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
            };

            if (cfg.meeting_id == "")
            {
                throw new Exception("賽事 id 不可為空值");
            }

            //賽事
            Item itmMeeting = cfg.inn.applySQL("SELECT in_title, in_language FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (itmMeeting.isError() || itmMeeting.getResult() == "")
            {
                throw new Exception("查無 賽事資料");
            }
            cfg.mt_title = itmMeeting.getProperty("in_title", "");
            cfg.mt_language = itmMeeting.getProperty("in_language", "");
            cfg.is_english = cfg.mt_language == "en";

            //iplm 站台網址
            cfg.plm_url = GetVariable(cfg, "app_url");

            //Api 網址
            cfg.api_url = GetVariable(cfg, "api_url");

            //開放資料
            CreateFightPage(cfg);

            //修正英文標籤
            FixLanguageLabel(cfg);

            //刷新連結排序
            RefreshSortOrder(cfg, "function");

            return itmR;
        }

        private void FixLanguageLabel(TConfig cfg)
        {
            if (cfg.meeting_id == "") return;
            UpdLanguageLabel(cfg, "rtm-player", "Athlete Profile");
            UpdLanguageLabel(cfg, "checkin", "Contest Order");
            UpdLanguageLabel(cfg, "rtm-tree", "Draw Sheet");
            UpdLanguageLabel(cfg, "rtm-score-list", "Last Records");
            UpdLanguageLabel(cfg, "allocate-site", "Table Division");
            UpdLanguageLabel(cfg, "spotcheck-announce", "Random Weigh-In List");
            UpdLanguageLabel(cfg, "rtm-score3", "Real-Time Records");
            UpdLanguageLabel(cfg, "rank-list", "Results");
            UpdLanguageLabel(cfg, "rtm-site", "Running Order");
            UpdLanguageLabel(cfg, "tidbits-collection", "Gallery");
            UpdLanguageLabel(cfg, "meeting-information", "Information");
        }

        private void UpdLanguageLabel(TConfig cfg, string in_value, string in_label_en)
        {
            string sql = "";
            sql = "UPDATE IN_MEETING_SHORTURL SET in_label_en = '" + in_label_en + "' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_value = '" + in_value + "'";
            cfg.inn.applySQL(sql);
        }

        private void CreateFightPage(TConfig cfg)
        {
            //首頁
            var lnkIndex = new TLink { val = "index", lbl = "首頁", sort = "1", controller = "Fight", action = "Index" };
            Item itmIndex = GetMShortUrl(cfg, "", lnkIndex);
            itmIndex = IndexPage(cfg, lnkIndex);

            //功能頁
            var lnkFunc = new TLink { val = "function", lbl = cfg.mt_title, sort = "1", controller = "Fight", action = "Overview" };
            Item itmFunc = GetMShortUrl(cfg, cfg.meeting_id, lnkFunc);
            if (itmFunc.isError() || itmFunc.getResult() == "")
            {
                lnkFunc.sort = GetFuncSort(cfg, lnkFunc);
                itmFunc = MergeMeetingShortUrl(cfg, itmFunc, lnkFunc);
            }
            else
            {
                lnkFunc.sort = itmFunc.getProperty("in_sort_order", "");
                itmFunc = MergeMeetingShortUrl(cfg, itmFunc, lnkFunc);
            }

            MergeMeetingShortUrl(cfg, itmFunc, new TLink 
            { 
                val = "information", 
                lbl = "活動資訊", 
                sort = "100", 
                controller = "Meeting", 
                action = "Item" 
            });

            MergeMeetingShortUrl(cfg, itmFunc, new TLink 
            { 
                val = "checkin", 
                lbl = "比賽進度表", 
                sort = "200",
                controller = "Fight",
                action = "NextEvent" 
            });

            MergeMeetingShortUrl(cfg, itmFunc, new TLink 
            { 
                val = "rtm-score", lbl = "戰情中心",
                sort = "300",
                controller = "Fight",
                action = "Monitor" 
            });

            MergeMeetingShortUrl(cfg, itmFunc, new TLink 
            { 
                val = "rtm-tree", 
                lbl = "對戰表", 
                sort = "400", 
                controller = "Fight", 
                action = "DrawSheet" 
            });

            MergeMeetingShortUrl(cfg, itmFunc, new TLink
            {
                val = "rtm-player",
                lbl = "選手資訊",
                sort = "500",
                controller = "Fight",
                action = "PlayerInfo"
            });

            MergeMeetingShortUrl(cfg, itmFunc, new TLink
            {
                val = "rtm-site",
                lbl = "場次查詢",
                sort = "600",
                controller = "Fight",
                action = "PlayerSite"
            });

            MergeMeetingShortUrl(cfg, itmFunc, new TLink
            {
                val = "allocate-site",
                lbl = "場地賽程表",
                sort = "700",
                controller = "Fight",
                action = "SectSite"
            });

            MergeMeetingShortUrl(cfg, itmFunc, new TLink
            {
                val = "allocate-site",
                lbl = "場地賽程表",
                sort = "700",
                controller = "Fight",
                action = "SectSite"
            });

            MergeMeetingShortUrl(cfg, itmFunc, new TLink
            {
                val = "rank-list",
                lbl = "名次查詢",
                sort = "800",
                controller = "Fight",
                action = "RankList"
            });

            MergeMeetingShortUrl(cfg, itmFunc, new TLink
            {
                val = "tidbits-collection",
                lbl = "花絮集錦",
                sort = "900",
                controller = "Fight",
                action = "PhotoList"
            });
        }

        private string GetVariable(TConfig cfg, string in_name)
        {
            Item item = cfg.inn.applySQL("SELECT TOP 1 in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = '" + in_name + "'");
            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("查無 " + in_name + " 站台網址");
            }
            return item.getProperty("in_value", "").TrimEnd('/');
        }

        private Item GetMShortUrl(TConfig cfg, string meeting_id, TLink link)
        {
            string sql = "SELECT TOP 1 * FROM IN_MEETING_SHORTURL WITH(NOLOCK)"
                + " WHERE ISNULL(in_meeting, '') = '" + meeting_id + "'"
                + " AND in_value = '" + link.val + "'";

            return cfg.inn.applySQL(sql);
        }

        //首頁
        private Item IndexPage(TConfig cfg, TLink link)
        {
            string resource = ""
                + "/" + link.controller
                + "/" + link.action
                + "?meeting_id=" + cfg.meeting_id;

            //Aras
            var api_entity = GetUrlEntity(cfg, resource);

            Item item = cfg.inn.newItem("IN_MEETING_SHORTURL", "merge");
            item.setAttribute("where", "in_value = '" + link.val + "'");

            SetProperties(item, link.val, link.lbl, link.sort, api_entity);

            item = item.apply();

            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("競賽連結中心-首頁: 建立失敗");
            }

            return item;
        }

        private Item MergeMeetingShortUrl(TConfig cfg, Item itmParent, TLink link)
        {
            string parent_id = itmParent.getProperty("id", "");

            string resource = ""
                + "/" + link.controller
                + "/" + link.action
                + "?meeting_id=" + cfg.meeting_id;

            switch(link.action)
            {
                case "Monitor":
                    resource += "&fought=3&waiting=3";
                    break;
            }

            //Aras
            var api_entity = GetUrlEntity(cfg, resource);

            Item item = cfg.inn.newItem("IN_MEETING_SHORTURL", "merge");
            item.setAttribute("where", "in_meeting = '" + cfg.meeting_id + "' AND in_value = '" + link.val + "'");
            item.setProperty("in_meeting", cfg.meeting_id);
            item.setProperty("in_parent", parent_id);
            if (link.closed) item.setProperty("in_is_closed", "1");

            SetProperties(item, link.val, link.lbl, link.sort, api_entity);

            item = item.apply();

            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("功能-" + link.lbl + ": 建立失敗");
            }

            return item;
        }

        private void SetProperties(Item item, string in_value, string in_label, string in_sort, TUrl api_entity)
        {
            item.setProperty("in_value", in_value);
            item.setProperty("in_label", in_label);
            item.setProperty("in_sort_order", in_sort);

            item.setProperty("in_resource", api_entity.Resource);
            item.setProperty("in_api_srcurl", api_entity.FullUrl);
            item.setProperty("in_api_shorturl", api_entity.ShortUrl);
            item.setProperty("in_api_token", api_entity.Token);
            item.setProperty("in_api_qrcode", "");
        }

        private TUrl GetUrlEntity(TConfig cfg, string resource)
        {
            TUrl result = new TUrl
            {
                Resource = resource,
                FullUrl = cfg.plm_url.TrimEnd('/') + "/" + resource.TrimStart('/'),
                ShortUrl = "",
                Token = "",
            };

            result.ShortUrl = GetApiShortUrl(cfg, result.FullUrl);

            if (result.ShortUrl == "")
            {
                throw new Exception("建立短網址發生錯誤 _# " + result.FullUrl);
            }
            else
            {
                result.Token = result.ShortUrl.Split('/').Last();
            }

            return result;
        }

        private string GetPlmMtUrl(TConfig cfg, string page, string func_id)
        {
            return "/pages/b.aspx"
                + "?page=" + page
                + "&method=in_qrcode_query1"
                + "&meeting_id=" + cfg.meeting_id
                + "&func_id=" + func_id
                + "";
        }

        //生成短網址
        private string GetApiShortUrl(TConfig cfg, string srcUrl)
        {
            string result = "";

            try
            {
                string tgturl = System.Web.HttpUtility.UrlEncode(srcUrl);
                string fullurl = cfg.api_url + "/create?url=" + tgturl;
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, fullurl);

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(fullurl);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader tReader = new StreamReader(response.GetResponseStream());

                string rst = tReader.ReadToEnd();
                if (!string.IsNullOrWhiteSpace(rst))
                {
                    result = rst.Replace("\"", "");
                }
            }
            catch (Exception ex)
            {
                result = "";
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, ex.Message);
            }

            return result;
        }

        private string GetFuncSort(TConfig cfg, TLink link)
        {
            string sql = "SELECT count(*) AS 'cnt' FROM IN_MEETING_SHORTURL WITH(NOLOCK) WHERE in_value = '" + link.val + "'";
            Item itmData = cfg.inn.applySQL(sql);
            if (itmData.isError() || itmData.getResult() == "")
            {
                return "1";
            }

            string cnt = itmData.getProperty("cnt", "0");
            if (cnt == "" || cnt == "0")
            {
                return "1";
            }

            int old_sort = GetIntValue(cnt);
            if (old_sort == 0)
            {
                return "1";
            }
            else
            {
                return (old_sort + 1).ToString();
            }
        }

        private void RefreshSortOrder(TConfig cfg, string in_value)
        {
            //function
            //admin-func
            var sql = @"
                UPDATE t11 SET
	                t11.in_sort_order = t12.rn * 100
                FROM
	                IN_MEETING_SHORTURL t11 WITH(NOLOCK)
                INNER JOIN
                (
	                SELECT
		                t1.id
		                , ROW_NUMBER() OVER (ORDER BY t2.in_date_s DESC) AS rn
	                FROM 
		                IN_MEETING_SHORTURL t1 WITH(NOLOCK)
	                INNER JOIN
		                IN_MEETING t2 WITH(NOLOCK) 
		                ON t2.id = t1.in_meeting
	                WHERE
		                t1.in_value = 'function'
                ) t12 oN t12.id = t11.id
            ";

            sql = sql.Replace("{#in_value}", in_value);

            cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string mt_title { get; set; }
            public string mt_language { get; set; }
            public bool is_english { get; set; }

            public string plm_url { get; set; }
            public string api_url { get; set; }

            public int site_count { get; set; }
        }

        private class TLink
        {
            public string val { get; set; }
            public string lbl { get; set; }
            public string sort { get; set; }

            public string controller { get; set; }
            public string action { get; set; }
            public bool closed { get; set; }

            public string key { get; set; }
        }

        private class TUrl
        {
            public string Resource { get; set; }
            public string FullUrl { get; set; }
            public string ShortUrl { get; set; }
            public string Token { get; set; }
        }

        private int GetIntValue(string value)
        {
            int result = 0;
            if (int.TryParse(value, out result)) return result;
            return 0;
        }
    }
}