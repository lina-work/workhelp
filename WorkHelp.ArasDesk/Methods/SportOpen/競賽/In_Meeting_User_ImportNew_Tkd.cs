﻿using Aras.IOM;
using System;
using System.Collections.Generic;

namespace WorkHelp.ArasDesk.Methods.SportOpen.競賽
{
    internal class In_Meeting_User_ImportNew_Tkd : Item
    {
        public In_Meeting_User_ImportNew_Tkd(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 與會者匯入(TKD版本)
                日誌: 
                    - 2023-03-28: 巴西制 (lina)
                    - 2022-10-03: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_User_ImportNew_Tkd";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", ""),
                need_rebuild_meeting = itmR.getProperty("need_rebuild_meeting", ""),
                in_battle_type = itmR.getProperty("in_battle_type", ""),
                scene = itmR.getProperty("scene", ""),
            };


            switch (cfg.scene)
            {
                case "import":
                    CacheStep1(cfg, itmR);
                    SaveImportData(cfg, itmR);
                    break;

                case "force_clear":
                    ForceClear(cfg, itmR);
                    break;

                case "CacheStep3":
                    CacheStep2(cfg, itmR);
                    break;

                case "rebuild":
                    Rebuild(cfg, itmR);
                    break;

                case "fight":
                    Fight(cfg, itmR);
                    break;

                case "day_menu":
                    DayMenu(cfg, itmR);
                    break;

                case "fix_rank":
                    //修正準決賽名次
                    FixSemiFinalRank(cfg, itmR);
                    //修正舊資料 source_id
                    CacheStep2(cfg, itmR);
                    //修正組別簡稱
                    FixProgramShortName(cfg, itmR);
                    //修正單位排序編號
                    FixOrgStuffB1(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void ForceClear(TConfig cfg, Item itmReturn)
        {
            var sql1 = "";
            var sql2 = "";


            sql1 = "DELETE FROM IN_MEETING_ALLOCATION WHERE in_meeting = '" + cfg.meeting_id + "'";
            sql2 = "DELETE FROM IN_MEETING_ALLOCATION WHERE in_meeting IS NULL";
            cfg.inn.applySQL(sql1);
            cfg.inn.applySQL(sql2);

            sql1 = "DELETE FROM IN_MEETING_PEVENT_DETAIL WHERE SOURCE_ID IN (select id from IN_MEETING_PEVENT WHERE in_meeting = '" + cfg.meeting_id + "')";
            sql2 = "DELETE FROM IN_MEETING_PEVENT_DETAIL WHERE SOURCE_ID IN (select id from IN_MEETING_PEVENT WHERE in_meeting IS NULL)";
            cfg.inn.applySQL(sql1);
            cfg.inn.applySQL(sql2);

            sql1 = "DELETE FROM IN_MEETING_PEVENT WHERE in_meeting = '" + cfg.meeting_id + "'";
            sql2 = "DELETE FROM IN_MEETING_PEVENT WHERE in_meeting IS NULL";
            cfg.inn.applySQL(sql1);
            cfg.inn.applySQL(sql2);

            sql1 = "DELETE FROM IN_MEETING_PTEAM WHERE in_meeting = '" + cfg.meeting_id + "'";
            sql2 = "DELETE FROM IN_MEETING_PTEAM WHERE in_meeting IS NULL";
            cfg.inn.applySQL(sql1);
            cfg.inn.applySQL(sql2);

            sql1 = "DELETE FROM IN_MEETING_PROGRAM WHERE in_meeting = '" + cfg.meeting_id + "'";
            sql2 = "DELETE FROM IN_MEETING_PROGRAM WHERE in_meeting IS NULL";
            cfg.inn.applySQL(sql1);
            cfg.inn.applySQL(sql2);

            sql1 = "DELETE FROM IN_MEETING_SITE WHERE in_meeting = '" + cfg.meeting_id + "'";
            sql2 = "DELETE FROM IN_MEETING_SITE WHERE in_meeting IS NULL";
            cfg.inn.applySQL(sql1);
            cfg.inn.applySQL(sql2);

            sql1 = "DELETE FROM IN_MEETING_USER WHERE source_id = '" + cfg.meeting_id + "'";
            sql2 = "DELETE FROM IN_MEETING_USER WHERE source_id IS NULL";
            cfg.inn.applySQL(sql1);
            cfg.inn.applySQL(sql2);
        }

        private void CacheStep1(TConfig cfg, Item itmReturn)
        {
            if (cfg.need_rebuild_meeting == "1") return;

            var sql = "";

            sql = "UPDATE IN_MEETING_ALLOCATION SET in_meeting = NULL WHERE in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PROGRAM SET in_meeting = NULL WHERE in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PTEAM SET in_meeting = NULL WHERE in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PEVENT SET in_meeting = NULL WHERE in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_USER SET source_id = NULL WHERE source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
        }

        private void CacheStep2(TConfig cfg, Item itmReturn)
        {
            var sql = "";

            //檢查是否有緩存資料
            sql = "SELECT TOP 1 id FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_meeting IS NULL";
            var itmData = cfg.inn.applySQL(sql);
            if (itmData.isError() || itmData.getResult() == "") return;

            sql = "UPDATE IN_MEETING_ALLOCATION SET in_meeting = '" + cfg.meeting_id + "' WHERE in_meeting IS NULL";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PROGRAM SET in_meeting = '" + cfg.meeting_id + "' WHERE in_meeting IS NULL";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PTEAM SET in_meeting = '" + cfg.meeting_id + "' WHERE in_meeting IS NULL";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PEVENT SET in_meeting = '" + cfg.meeting_id + "' WHERE in_meeting IS NULL";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_USER SET source_id = '" + cfg.meeting_id + "' WHERE source_id IS NULL";
            cfg.inn.applySQL(sql);
        }

        private void DayMenu(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT DISTINCT
	                in_fight_day AS 'in_date'
                FROM
	                IN_MEETING_ALLOCATION WITH(NOLOCK)
                WHERE
	                in_meeting = '{#meeting_id}'
                ORDER BY
	                in_fight_day
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql + ": " + count.ToString());

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setType("inn_day");
                itmReturn.addRelationship(item);
            }
        }

        private void Fight(TConfig cfg, Item itmReturn)
        {
            //賽會參數設定
            RebuildMtVariable(cfg, itmReturn);
            //過磅狀態設定
            RebuildMtWeight(cfg, itmReturn);
            //清除賽程組別資料
            ClearPrograms(cfg, itmReturn);
            //建立賽程組別資料
            CreatePrograms(cfg, itmReturn);
            //更新隊伍資料
            UpdateTeamData(cfg, itmReturn);
            //建立場地分配
            CreateAllocation(cfg, itmReturn);
            //建立競賽連結
            CreateQrCode(cfg, itmReturn);
        }

        //修正組別簡稱
        private void FixProgramShortName(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT DISTINCT
	                t1.in_program_no
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t2.id
	                , t2.in_team_count
	                , t2.in_round_code
                FROM 
	                In_Meeting_User_ImportNew t1 WITH(NOLOCK)
				INNER JOIN
					IN_MEETING_PROGRAM t2 WITH(NOLOCK)
					ON t2.in_meeting = t1.in_meeting
					AND t2.in_l1 = t1.in_l1
					AND t2.in_l2 = t1.in_l2
					AND t2.in_l3 = t1.in_l3
                WHERE 
                    t1.in_meeting = '{#meeting_id}'
                ORDER BY
	                t1.in_program_no
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
            ";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_program_no = item.getProperty("in_program_no", "");
                var in_l1 = item.getProperty("in_l1", "");
                var in_l2 = item.getProperty("in_l2", "");
                var in_l3 = item.getProperty("in_l3", "");
                var in_round_code = item.getProperty("in_round_code", "").PadLeft(3, '0');
                var in_graph_main = "knockouts-" + in_round_code;

                var model = GetShortName(cfg, in_l1, in_l2, in_l3);
                SetWeight(cfg, model, in_l3);

                var in_short_name = model.age + model.gender + model.belt + model.weight;

                var sql_upd = "UPDATE IN_MEETING_PROGRAM SET"
                    + "  in_short_name = '" + in_short_name + "'"
                    + ", in_program_no = '" + in_program_no + "'"
                    + ", in_graph_main = '" + in_graph_main + "'"
                    + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                    + " AND in_l1 = '" + in_l1 + "'"
                    + " AND in_l2 = '" + in_l2 + "'"
                    + " AND in_l3 = '" + in_l3 + "'"
                    ;

                cfg.inn.applySQL(sql_upd);
            }

            sql = @"
                UPDATE t1 SET
	                t1.in_sort_order += (t2.rno * 10)
                FROM
	                IN_MEETING_PROGRAM t1
                INNER JOIN
                (
	                SELECT
		                id
		                , in_l1, in_l2, in_l3
		                , ROW_NUMBER() OVER (PARTITION BY in_l1, in_l2 ORDER BY in_sort_order, REPLACE(in_l3, '+' , 'X')) AS 'rno'
	                FROM
		                IN_MEETING_PROGRAM WITH(NOLOCK)
	                WHERE
		                in_meeting = '{#meeting_id}'
                ) t2 ON t2.id = t1.id
            ";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            cfg.inn.applySQL(sql);
        }

        private class TGBW
        {
            public string age { get; set; }
            public string gender { get; set; }
            public string belt { get; set; }
            public string weight { get; set; }
        }

        private void SetWeight(TConfig cfg, TGBW model, string in_l3)
        {
            if (in_l3.Contains("公分"))
            {
                model.weight = in_l3.Replace("公分級", "");
                return;
            }
            if (in_l3.Contains("以上級"))
            {
                model.weight = "+" + in_l3.Replace("以上級", "").Replace("公斤", "kg");
                model.weight = model.weight.Replace("++", "+");
                return;
            }
            if (in_l3.Contains("+"))
            {
                model.weight = in_l3.Replace("公斤級", "kg");
                return;
            }
            if (in_l3.Contains("-"))
            {
                model.weight = in_l3.Replace("公斤級", "kg");
                return;
            }
            if (in_l3.Contains("以上"))
            {
                model.weight = "+" + in_l3.Replace("公斤以上", "kg");
                model.weight = model.weight.Replace("++", "+");
                return;
            }
            if (in_l3.Contains("公斤級"))
            {
                model.weight = "-" + in_l3.Replace("公斤級", "kg");
                return;
            }

            model.weight = in_l3;
        }

        private TGBW GetShortName(TConfig cfg, string in_l1, string in_l2, string in_l3)
        {
            var gender = "";
            if (in_l2.Contains("混")) gender = "混";
            else if (in_l2.Contains("男")) gender = "男";
            else if (in_l2.Contains("女")) gender = "女";

            var belt = "";
            if (in_l2.Contains("黑帶")) belt = "黑";
            else if (in_l2.Contains("色帶")) belt = "色";
            else if (in_l3.Contains("黑帶")) belt = "黑";
            else if (in_l3.Contains("色帶")) belt = "色";

            in_l2 = in_l2.Replace("生", "子");
            var age = "";
            switch (in_l2)
            {
                case "女子組":
                case "男子組":
                    age = "";
                    break;

                case "一般女子組":
                case "一般男子組":
                    age = "一";
                    break;

                case "公開女子組":
                case "公開男子組":
                    age = "公";
                    break;

                case "社會女子組":
                case "社會男子組":
                    age = "社";
                    break;

                case "高中女子組":
                case "高中女子黑帶組":
                case "高中女生黑帶組":
                case "高中部女子組":
                case "高中男子組":
                case "高中男子黑帶組":
                case "高中男生黑帶組":
                case "高中部男子組":
                    age = "高";
                    break;

                case "國中女子一般":
                case "國中男子一般":
                    age = "國一般";
                    break;

                case "國中女子公開":
                case "國中男子公開":
                    age = "國公開";
                    break;

                case "國中部女子組":
                case "國中女子組":
                case "國中女子色帶組":
                case "國中女子黑帶組":
                case "國中部男子組":
                case "國中男子組":
                case "國中男子色帶組":
                case "國中男子黑帶組":
                    age = "國";
                    break;

                case "國小女子黑帶組":
                case "國小男子黑帶組":
                    age = "小";
                    break;

                case "國小女子色帶高級組":
                case "國小男子色帶高級組":
                case "國小高年級女子色帶組":
                case "國小高年級男子色帶組":
                case "國小高年級女子黑帶組":
                case "國小高年級男子黑帶組":
                    age = "小高";
                    break;

                case "國小女子色帶中級組":
                case "國小男子色帶中級組":
                case "國小中年級女子色帶組":
                case "國小中年級男子色帶組":
                case "國小中年級女子黑帶組":
                case "國小中年級男子黑帶組":
                    age = "小中";
                    break;

                case "國小女子色帶初級組":
                case "國小男子色帶初級組":
                case "國小女子黑帶初級組":
                case "國小男子黑帶初級組":
                case "國小初年級女子色帶組":
                case "國小初年級男子色帶組":
                case "國小初年級女子黑帶組":
                case "國小初年級男子黑帶組":
                    age = "小初";
                    break;

                case "國小低年級女子色帶組":
                case "國小低年級女子黑帶組":
                case "國小低年級男子色帶組":
                case "國小低年級男子黑帶組":
                    age = "小低";
                    break;

                case "青年女子組":
                case "青年女生組":
                case "青年男子組":
                case "青年男生組":
                    age = "青";
                    break;

                case "青少年女子組":
                case "青少年女生組":
                case "青少年男子組":
                case "青少年男生組":
                    age = "青少";
                    break;

                case "少年女子組":
                case "少年女生組":
                case "少年男子組":
                case "少年男生組":
                    age = "少";
                    break;

                case "幼稚園女子組":
                case "幼稚園女生組":
                case "幼稚園男子組":
                case "幼稚園男生組":
                    age = "幼";
                    break;

                case "男子大專社會以上黑帶組":
                case "女子大專社會以上黑帶組":
                case "男子大專社會以上色帶組":
                case "女子大專社會以上色帶組":
                    age = "大社";
                    break;

                case "男女混合團體組":
                case "混合團體組":
                    age = "";
                    break;

                case "高中大專社會女子":
                case "高中大專社會男子":
                    age = "高大社";
                    break;
            }
            return new TGBW { gender = gender, belt = belt, age = age };
        }

        //修正組別排序
        private void FixProgramSort(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                UPDATE t1 SET
                	t1.in_sort_order = t2.rno * 100
                FROM
                	IN_MEETING_PROGRAM t1
                INNER JOIN
                (
                	SELECT 
                		id
                		, in_l1
                		, in_l2
                		, new_name
                		, ROW_NUMBER() OVER(ORDER BY in_l1, in_l2, new_name) AS 'rno'
                	FROM
                		VU_Mt_Program_Sort
                	WHERE
                		in_meeting = '{#meeting_id}'
                ) t2 ON t2.id = t1.id
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        //修正單位排序編號
        private void FixOrgStuffB1(TConfig cfg, Item itmReturn)
        {
            var sql = "";

            sql = "UPDATE IN_MEETING_USER SEt in_short_org = in_current_org WHERE source_id = '" + cfg.meeting_id + "' AND ISNULL(in_short_org, '') = ''";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_USER SEt in_city_no = 'CT001' WHERE in_short_org LIKE '%基隆市%' AND source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
            sql = "UPDATE IN_MEETING_USER SEt in_city_no = 'CT002' WHERE in_short_org LIKE '%臺北市%' AND source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
            sql = "UPDATE IN_MEETING_USER SEt in_city_no = 'CT003' WHERE in_short_org LIKE '%新北市%' AND source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
            sql = "UPDATE IN_MEETING_USER SEt in_city_no = 'CT004' WHERE in_short_org LIKE '%桃園市%' AND source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
            sql = "UPDATE IN_MEETING_USER SEt in_city_no = 'CT005' WHERE in_short_org LIKE '%新竹縣%' AND source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
            sql = "UPDATE IN_MEETING_USER SEt in_city_no = 'CT006' WHERE in_short_org LIKE '%新竹市%' AND source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
            sql = "UPDATE IN_MEETING_USER SEt in_city_no = 'CT007' WHERE in_short_org LIKE '%苗栗縣%' AND source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
            sql = "UPDATE IN_MEETING_USER SEt in_city_no = 'CT008' WHERE in_short_org LIKE '%臺中市%' AND source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
            sql = "UPDATE IN_MEETING_USER SEt in_city_no = 'CT009' WHERE in_short_org LIKE '%彰化縣%' AND source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
            sql = "UPDATE IN_MEETING_USER SEt in_city_no = 'CT010' WHERE in_short_org LIKE '%南投縣%' AND source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
            sql = "UPDATE IN_MEETING_USER SEt in_city_no = 'CT011' WHERE in_short_org LIKE '%雲林縣%' AND source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
            sql = "UPDATE IN_MEETING_USER SEt in_city_no = 'CT012' WHERE in_short_org LIKE '%嘉義縣%' AND source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
            sql = "UPDATE IN_MEETING_USER SEt in_city_no = 'CT013' WHERE in_short_org LIKE '%嘉義市%' AND source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
            sql = "UPDATE IN_MEETING_USER SEt in_city_no = 'CT014' WHERE in_short_org LIKE '%臺南市%' AND source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
            sql = "UPDATE IN_MEETING_USER SEt in_city_no = 'CT015' WHERE in_short_org LIKE '%高雄市%' AND source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
            sql = "UPDATE IN_MEETING_USER SEt in_city_no = 'CT016' WHERE in_short_org LIKE '%屏東縣%' AND source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
            sql = "UPDATE IN_MEETING_USER SEt in_city_no = 'CT017' WHERE in_short_org LIKE '%宜蘭縣%' AND source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
            sql = "UPDATE IN_MEETING_USER SEt in_city_no = 'CT018' WHERE in_short_org LIKE '%花蓮縣%' AND source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
            sql = "UPDATE IN_MEETING_USER SEt in_city_no = 'CT019' WHERE in_short_org LIKE '%臺東縣%' AND source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
            sql = "UPDATE IN_MEETING_USER SEt in_city_no = 'CT020' WHERE in_short_org LIKE '%澎湖縣%' AND source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
            sql = "UPDATE IN_MEETING_USER SEt in_city_no = 'CT021' WHERE in_short_org LIKE '%金門縣%' AND source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
            sql = "UPDATE IN_MEETING_USER SEt in_city_no = 'CT022' WHERE in_short_org LIKE '%連江縣%' AND source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
        }

        //修正準決賽名次
        private void FixSemiFinalRank(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                UPDATE IN_MEETING_PEVENT SET
                    in_tree_rank = 'rank23'
                    , in_tree_rank_ns = '2,3'
                    , in_tree_rank_nss = '2,3'
                where in_meeting = '{#meeting_id}'
                AND in_tree_name = 'main'
                AND in_round_code = 4
                AND ISNULL(in_robin_key, '') = ''
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        //建立競賽連結
        private void CreateQrCode(TConfig cfg, Item itmReturn)
        {
            Item itmData = cfg.inn.newItem("In_Meeting");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.apply("in_qrcode_create");
        }

        //建立場地分配
        private void CreateAllocation(TConfig cfg, Item itmReturn)
        {
            //建立場地分配
            string sql = "SELECT * FROM IN_MEETING_SITE WITH(NOLOCK) WHERE in_meeting = '{#meeting_id}' AND in_code = 1";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            Item itmSite = cfg.inn.applySQL(sql);
            if (itmSite.isError() || itmSite.getResult() == "")
            {
                return;
            }

            //清除舊場地分配
            cfg.inn.applySQL("DELETE FROM IN_MEETING_ALLOCATION WHERE in_meeting = '" + cfg.meeting_id + "'");

            //建立主分類
            MergeAllocateCategory(cfg, itmSite);

            //建立場地分配
            MergeAllocateFight(cfg, itmSite);
        }

        private void MergeAllocateFight(TConfig cfg, Item itmSite)
        {
            List<string> pgs = new List<string>();

            string sql = @"
                SELECT DISTINCT
					in_program_no
					, in_l1
					, in_l2
					, in_l3
					, in_day
                FROM 
                    In_Meeting_User_ImportNew WITH(NOLOCK) 
                WHERE 
                    in_meeting = '{#meeting_id}'
                ORDER BY
					in_program_no
					, in_l1
					, in_l2
					, in_l3
					, in_day
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_program_no = item.getProperty("in_program_no", "");
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                string in_l3 = item.getProperty("in_l3", "");
                string in_day = item.getProperty("in_day", "");
                string in_fight_day = GetFightDay(cfg, in_day);

                if (in_fight_day == "")
                {
                    continue;
                }

                string sql_qry = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK)"
                    + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                    + " AND in_l1 = N'" + in_l1 + "'"
                    + " AND in_l2 = N'" + in_l2 + "'"
                    + " AND in_l3 = N'" + in_l3 + "'"
                    ;

                Item itmProgram = cfg.inn.applySQL(sql_qry);
                if (itmProgram.isError() || itmProgram.getResult() == "")
                {
                    continue;
                }

                string site_id = itmSite.getProperty("id", "");
                string program_id = itmProgram.getProperty("id", "");

                //更新組別代碼與比賽日期
                string sql_upd = "UPDATE IN_MEETING_PROGRAM SET"
                    + "  in_program_no = '" + in_program_no + "'"
                    + ", in_fight_day = '" + in_fight_day + "'"
                    + " WHERE id = '" + program_id + "'";

                cfg.inn.applySQL(sql_upd);

                Item itmNew = cfg.inn.newItem("In_Meeting_Allocation", "add");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_date", in_fight_day);
                itmNew.setProperty("in_fight_day", in_fight_day);
                itmNew.setProperty("in_site", site_id);
                itmNew.setProperty("in_program", program_id);
                itmNew.setProperty("in_type", "fight");
                itmNew.setProperty("in_l1", "");
                itmNew.setProperty("in_l2", "");
                itmNew = itmNew.apply();

                pgs.Add(program_id);
            }

            DateTime dt = DateTime.Now.AddDays(-1).Date;

            foreach (var pg in pgs)
            {
                string sql_upd = "UPDATE IN_MEETING_ALLOCATION SET created_on = '" + dt.ToString("yyyy-MM-dd HH:mm:00") + "' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_program = '" + pg + "'";
                cfg.inn.applySQL(sql_upd);
                dt = dt.AddMinutes(10);
            }
        }

        private void MergeAllocateCategory(TConfig cfg, Item itmSite)
        {
            string sql = @"
                SELECT DISTINCT
					in_program_no
					, in_l1
					, in_l2
					, in_day
                FROM 
                    In_Meeting_User_ImportNew WITH(NOLOCK) 
                WHERE 
                    in_meeting = '{#meeting_id}'
                ORDER BY
					in_program_no
					, in_l1
					, in_l2
					, in_day
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                string in_day = item.getProperty("in_day", "");
                string in_fight_day = GetFightDay(cfg, in_day);

                if (in_fight_day == "")
                {
                    continue;
                }

                Item itmNew = cfg.inn.newItem("In_Meeting_Allocation", "add");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_date", in_fight_day);
                itmNew.setProperty("in_fight_day", in_fight_day);
                itmNew.setProperty("in_type", "category");
                itmNew.setProperty("in_l1", in_l1);
                itmNew.setProperty("in_l2", in_l2);
                itmNew = itmNew.apply();
            }
        }

        private string GetFightDay(TConfig cfg, string value)
        {
            string code = value;
            if (code.Length <= 4)
            {
                code = DateTime.Now.Year + code.PadLeft(4, '0');
            }
            if (code.Length != 8)
            {
                return "";
            }

            string y = code.Substring(0, 4);
            string m = code.Substring(4, 2);
            string d = code.Substring(6, 2);
            return y + "-" + m + "-" + d;
        }

        private void UpdateTeamData(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                UPDATE t1 SET
                	t1.in_sign_no = t11.in_draw_no
                	, t1.in_section_no = t11.in_draw_no
                	, t1.in_team_index = t11.in_index
                FROM
                	IN_MEETING_PTEAM t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                	ON t2.id = t1.source_id
                INNER JOIN
                	IN_MEETING_USER t11 WITH(NOLOCK)
                	ON t11.source_id = t2.in_meeting
                	AND t11.in_l1 = t2.in_l1
                	AND t11.in_l2 = t2.in_l2
                	AND t11.in_l3 = t2.in_l3
                	AND t11.in_sno = t1.in_sno
                WHERE
                	t2.in_meeting = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        private void CreatePrograms(TConfig cfg, Item itmReturn)
        {
            Item itmData = cfg.inn.newItem("In_Meeting");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("battle_type", cfg.in_battle_type);
            itmData.setProperty("battle_repechage", "");
            itmData.setProperty("rank_type", "SameRank");
            itmData.setProperty("surface_code", "32");
            itmData.setProperty("robin_player", "0");
            itmData.setProperty("sub_event", "0");
            itmData.setProperty("mode", "save");
            itmData.apply("In_Meeting_Program_Tkd");
        }

        private void ClearPrograms(TConfig cfg, Item itmReturn)
        {
            Item itmData = cfg.inn.newItem("In_Meeting");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("mode", "clear");
            itmData.setProperty("exec", "all");
            itmData.apply("In_Meeting_Program_Tkd");
        }

        //賽會參數設定
        private void RebuildMtVariable(TConfig cfg, Item itmReturn)
        {
            string need_rank57 = itmReturn.getProperty("need_rank57", "1");

            cfg.inn.applySQL("DELETE FROM In_Meeting_Variable WHERE source_id = '" + cfg.meeting_id + "'");

            RebuildMtVariable(cfg, "fight_site", "臺北科大", "16");
            RebuildMtVariable(cfg, "allocate_mode", "cycle", "32");
            RebuildMtVariable(cfg, "line_color", "red", "64");
            RebuildMtVariable(cfg, "need_rank78", "0", "128");
            RebuildMtVariable(cfg, "need_rank56", "0", "256");
            RebuildMtVariable(cfg, "need_rank57", need_rank57, "512");
        }

        private void RebuildMtVariable(TConfig cfg, string in_key, string in_value, string sort_order)
        {
            Item itmNew = cfg.inn.newItem("In_Meeting_Variable", "add");
            itmNew.setProperty("source_id", cfg.meeting_id);
            itmNew.setProperty("in_key", in_key);
            itmNew.setProperty("in_value", in_value);
            itmNew.setProperty("sort_order", sort_order);
            itmNew = itmNew.apply();
        }

        //過磅狀態設定
        private void RebuildMtWeight(TConfig cfg, Item itmReturn)
        {
            cfg.inn.applySQL("DELETE FROM In_Meeting_PWeight WHERE in_meeting = '" + cfg.meeting_id + "'");
            RebuildMtWeight(cfg, "off", "0");
            RebuildMtWeight(cfg, "leave", "0");
            RebuildMtWeight(cfg, "dq", "0");
        }

        private void RebuildMtWeight(TConfig cfg, string in_status, string in_is_remove)
        {
            Item itmNew = cfg.inn.newItem("In_Meeting_PWeight", "add");
            itmNew.setProperty("in_meeting", cfg.meeting_id);
            itmNew.setProperty("in_status", in_status);
            itmNew.setProperty("in_is_remove", in_is_remove);
            itmNew = itmNew.apply();
        }

        private void Rebuild(TConfig cfg, Item itmReturn)
        {
            string aml = "<AML><Item type='In_Meeting' action='get' id='" + cfg.meeting_id + "' select='*'/></AML>";
            cfg.itmMeeting = cfg.inn.applyAML(aml);

            var svy_model = GetMtSvy(cfg);
            RebuildInL1(cfg, svy_model.l1_id);
            RebuildInL2(cfg, svy_model.l2_id);
            RebuildInL3(cfg, svy_model.l3_id);

            MergeMtMuser(cfg);
        }

        private void MergeMtMuser(TConfig cfg)
        {
            //刪除舊資料
            cfg.inn.applySQL("DELETE FROM IN_MEETING_USER WHERE source_id = '" + cfg.meeting_id + "'");

            string sql = "SELECT * FROM In_Meeting_User_ImportNew WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "' ORDER BY in_player_no";

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                //與會者資訊
                Item applicant = NewMUser(cfg, item);
                //建立與會者
                var itmMUser = applicant.apply("add");
            }

            //重算正取人數
            cfg.itmMeeting.apply("In_Update_MeetingOnMUEdit");
        }

        private Item NewMUser(TConfig cfg, Item item)
        {
            var applicant = cfg.inn.newItem("In_Meeting_User");

            applicant.setProperty("source_id", cfg.meeting_id);

            //所屬單位
            applicant.setProperty("in_current_org", item.getProperty("in_current_org", ""));

            //姓名
            applicant.setProperty("in_name", item.getProperty("in_name", ""));

            //身分證號
            applicant.setProperty("in_sno", item.getProperty("in_player_no", ""));
            applicant.setProperty("in_player_no", item.getProperty("in_player_no", ""));

            //性別
            applicant.setProperty("in_gender", GetGender(cfg, item));

            //西元生日
            applicant.getProperty("in_birth", "1990-01-01");

            //電子信箱
            applicant.setProperty("in_email", "na@na.n");

            //手機號碼
            applicant.setProperty("in_tel", "");

            //協助報名者姓名
            applicant.setProperty("in_creator", "lwu001");

            //協助報名者身分號
            applicant.setProperty("in_creator_sno", "lwu001");

            //所屬群組
            applicant.setProperty("in_group", "原創"); //F103277376測試者


            //預設身分均為選手
            applicant.setProperty("in_role", "player");

            //正取
            applicant.setProperty("in_note_state", "official");

            //組別
            applicant.setProperty("in_gameunit", item.getProperty("in_section_name", ""));

            //組名
            applicant.setProperty("in_section_name", item.getProperty("in_section_name", ""));

            //捐款金額
            applicant.setProperty("in_expense", "0");

            //序號
            Item itmIndex = MaxIndexItem(cfg, applicant);
            applicant.setProperty("in_index", item.getProperty("in_player_no", ""));

            //競賽項目
            applicant.setProperty("in_l1", item.getProperty("in_l1", ""));

            //競賽組別
            applicant.setProperty("in_l2", item.getProperty("in_l2", ""));

            //競賽分級
            applicant.setProperty("in_l3", item.getProperty("in_l3", ""));

            var in_mail = item.getProperty("in_player_no", "") + "-" + System.DateTime.Now.ToString("yyyyMMdd-HHmmss");
            //非實名制
            applicant.setProperty("in_mail", in_mail);

            //報名時間
            applicant.setProperty("in_regdate", System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));

            //繳費單號
            applicant.setProperty("in_paynumber", "");

            //籤號
            applicant.setProperty("in_draw_no", item.getProperty("in_draw_no", ""));

            return applicant;
        }

        private string GetGender(TConfig cfg, Item item)
        {
            string in_section_name = item.getProperty("in_section_name", "");
            return GetGender(cfg, in_section_name);
        }

        private string GetGender(TConfig cfg, string value)
        {
            if (value.Contains("男")) return "男";
            if (value.Contains("女")) return "女";
            return "";
        }

        //取得該組當前最大序號
        private Item MaxIndexItem(TConfig cfg, Item applicant)
        {
            string sql = "";

            string in_l1 = applicant.getProperty("in_l1", "");
            string in_l2 = applicant.getProperty("in_l2", "");
            string in_l3 = applicant.getProperty("in_l3", "");
            string in_l4 = applicant.getProperty("in_l4", "");

            sql = "SELECT MAX(in_index) AS 'max_idx' FROM In_Meeting_User WITH(NOLOCK) WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_l1 = N'" + in_l1 + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql_index: " + sql);

            Item itmResult = cfg.inn.applySQL(sql);

            if (itmResult.isError() || itmResult.getResult() == "")
            {
                itmResult = cfg.inn.newItem();
                itmResult.setProperty("new_idx", "00001");
            }
            else
            {
                var max_idx = itmResult.getProperty("max_idx", "1");
                var new_idx = GetIntVal(max_idx) + 1;
                applicant.setProperty("new_idx", new_idx.ToString("00000"));

            }

            return itmResult;
        }

        private void RebuildInL1(TConfig cfg, string id)
        {
            cfg.inn.applySQL("DELETE FROM IN_SURVEY_OPTION WHERE source_id = '" + id + "'");

            string sql = @"
                SELECT DISTINCT
                    in_l1
                FROM 
                    In_Meeting_User_ImportNew WITH(NOLOCK) 
                WHERE 
                    in_meeting = '{#meeting_id}'
                ORDER BY
                    in_l1
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            string in_selectoption = "";

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");
                int sort_order = (i + 1) * 100;

                Item itmNew = cfg.inn.newItem("In_Survey_Option", "add");
                itmNew.setProperty("source_id", id);
                itmNew.setProperty("in_label", in_l1);
                itmNew.setProperty("in_value", in_l1);
                itmNew.setProperty("sort_order", sort_order.ToString());
                itmNew = itmNew.apply();

                in_selectoption += "@" + in_l1;
            }

            cfg.inn.applySQL("UPDATE IN_SURVEY SET in_selectoption = N'" + in_selectoption + "' WHERE id = '" + id + "'");
        }

        private void RebuildInL2(TConfig cfg, string id)
        {
            cfg.inn.applySQL("DELETE FROM IN_SURVEY_OPTION WHERE source_id = '" + id + "'");

            string sql = @"
                SELECT DISTINCT
                    in_l1
                    , in_l2
                FROM 
                    In_Meeting_User_ImportNew WITH(NOLOCK) 
                WHERE 
                    in_meeting = '{#meeting_id}'
                ORDER BY
                    in_l1
                    , in_l2
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                int sort_order = (i + 1) * 100;

                Item itmNew = cfg.inn.newItem("In_Survey_Option", "add");
                itmNew.setProperty("source_id", id);
                itmNew.setProperty("in_filter", in_l1);
                itmNew.setProperty("in_label", in_l2);
                itmNew.setProperty("in_value", in_l2);
                itmNew.setProperty("sort_order", sort_order.ToString());
                itmNew = itmNew.apply();
            }
        }

        private void RebuildInL3(TConfig cfg, string id)
        {
            cfg.inn.applySQL("DELETE FROM IN_SURVEY_OPTION WHERE source_id = '" + id + "'");

            string sql = @"
                SELECT DISTINCT
                    in_program_no
                    , in_l1
                    , in_l2
                    , in_l3
                FROM 
                    In_Meeting_User_ImportNew WITH(NOLOCK) 
                WHERE 
                    in_meeting = '{#meeting_id}'
                ORDER BY
                    in_program_no
                    , in_l1
                    , in_l2
                    , in_l3
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            int weight_idx = 0;
            int last_code = 10000;
            string last_key = "";

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                string in_l3 = item.getProperty("in_l3", "");

                string key = in_l1 + "-" + in_l2;

                int sort_order = last_code + (i + 1) * 100;
                if (key != last_key)
                {
                    last_key = key;
                    last_code += 10000;
                }
                if (in_l3.Contains("以上"))
                {
                    sort_order += 3000;
                }

                var itmShortName = cfg.inn.applyMethod("In_Meeting_Program_ShortName"
                    , "<in_l1>" + in_l1 + "</in_l1><in_l2>" + in_l2 + "</in_l2><in_l3>" + in_l3 + "</in_l3>");

                var in_n1 = itmShortName.getProperty("result", "");
                var in_weight = itmShortName.getProperty("weight", "");

                Item itmNew = cfg.inn.newItem("In_Survey_Option", "add");
                itmNew.setProperty("source_id", id);
                itmNew.setProperty("in_grand_filter", in_l1);
                itmNew.setProperty("in_filter", in_l2);
                itmNew.setProperty("in_label", in_l3);
                itmNew.setProperty("in_value", in_l3);
                itmNew.setProperty("in_weight", in_weight);
                itmNew.setProperty("in_n1", in_n1);
                itmNew.setProperty("sort_order", sort_order.ToString());
                itmNew = itmNew.apply();

                if (in_l3.Contains("以上"))
                {
                    weight_idx = 0;
                }
                else
                {
                    weight_idx++;
                }
            }
        }

        private string GetWeight(TConfig cfg, Item item)
        {
            string in_l3 = item.getProperty("in_l3", "");

            string in_weight = "";


            if (in_l3.Contains("以上"))
            {
                in_weight = "+" + in_l3.Replace("公斤以上級", "");
            }
            else
            {
                in_weight = "-" + in_l3.Replace("公斤級", "");
            }

            return in_weight + "Kg";
        }



        private TMtSvy GetMtSvy(TConfig cfg)
        {
            TMtSvy result = new TMtSvy { l1_id = "", l2_id = "", l3_id = "" };

            string sql = @"
                SELECT
	                t2.id
	                , t2.in_property
	                , t2.in_questions
                FROM
	                IN_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN
	                IN_SURVEY t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t2.in_property IN ('in_l1', 'in_l2', 'in_l3')
                ORDER BY
	                t2.in_property 
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_property = item.getProperty("in_property", "");
                switch (in_property)
                {
                    case "in_l1": result.l1_id = id; break;
                    case "in_l2": result.l2_id = id; break;
                    case "in_l3": result.l3_id = id; break;
                }
            }
            return result;
        }

        private class TMtSvy
        {
            public string l1_id { get; set; }
            public string l2_id { get; set; }
            public string l3_id { get; set; }
        }

        //匯入與會者
        private void SaveImportData(TConfig cfg, Item itmReturn)
        {
            //重建場地資料
            RebuildMeetingSites(cfg, itmReturn.getProperty("site_count", "0"));

            var value = itmReturn.getProperty("value", "");
            if (value == "") throw new Exception("無匯入資料");

            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TRow>>(value);
            if (list == null || list.Count == 0) throw new Exception("無匯入資料");

            var itmMeeting = cfg.inn.applySQL("SELECT id, in_taking - in_real_taking AS 'allow_taking' FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            var allow_taking = GetIntVal(itmMeeting.getProperty("allow_taking", "0"));
            if (allow_taking < list.Count) throw new Exception("正取人數不足，剩餘: " + allow_taking + "，預計匯入: " + list.Count);

            cfg.inn.applySQL("DELETE FROM In_Meeting_User_ImportNew");

            var page_l1 = itmReturn.getProperty("in_l1", "");
            for (var i = 0; i < list.Count; i++)
            {
                var row = list[i];
                if (row.c07 == null || row.c07 == "") continue;

                var in_l1 = string.IsNullOrWhiteSpace(row.c11) ? page_l1 : row.c11;

                Item itmNew = cfg.inn.newItem("In_Meeting_User_ImportNew", "add");
                itmNew.setProperty("in_no", row.c00);
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_l1", in_l1);
                itmNew.setProperty("in_l2", row.c03);
                itmNew.setProperty("in_l3", row.c04);
                itmNew.setProperty("in_section_name", row.c02);
                itmNew.setProperty("in_program_no", row.c01);

                itmNew.setProperty("in_player_no", row.c05);
                itmNew.setProperty("in_current_org", row.c06);
                itmNew.setProperty("in_name", row.c07);
                itmNew.setProperty("in_draw_no", row.c08);
                itmNew.setProperty("in_city", row.c09);
                itmNew.setProperty("in_day", row.c10);
                itmNew = itmNew.apply();
            }
        }

        //重建場地資料
        private void RebuildMeetingSites(TConfig cfg, string value)
        {
            if (cfg.need_rebuild_meeting != "1") return;

            var max = GetIntVal(value);
            if (max <= 0) throw new Exception("場地數量錯誤");

            //清除所有場地資料
            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("mode", "remove");
            itmData.apply("In_Meeting_Site");

            for (var i = 1; i <= max; i++)
            {
                var code = i.ToString();
                var row = SiteCodeRow(i);

                var itmNew = cfg.inn.newItem("In_Meeting_Site", "add");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_code", code);
                //itmNew.setProperty("in_code_en", row.w1);
                itmNew.setProperty("in_name", "第" + row.w2 + "場地");
                itmNew.setProperty("in_rows", "1");
                itmNew.setProperty("in_cols", value);
                itmNew.setProperty("in_row_index", "0");
                itmNew.setProperty("in_col_index", (i - 1).ToString());
                itmNew.apply();
            }
        }

        private TSP SiteCodeRow(int code)
        {
            switch (code)
            {
                case 1: return new TSP { w1 = "A", w2 = "一" };
                case 2: return new TSP { w1 = "B", w2 = "二" };
                case 3: return new TSP { w1 = "C", w2 = "三" };
                case 4: return new TSP { w1 = "D", w2 = "四" };
                case 5: return new TSP { w1 = "E", w2 = "五" };
                case 6: return new TSP { w1 = "F", w2 = "六" };
                case 7: return new TSP { w1 = "G", w2 = "七" };
                case 8: return new TSP { w1 = "H", w2 = "八" };
                case 9: return new TSP { w1 = "I", w2 = "九" };
                case 10: return new TSP { w1 = "J", w2 = "十" };
                case 11: return new TSP { w1 = "K", w2 = "十一" };
                case 12: return new TSP { w1 = "L", w2 = "十二" };
                default: return new TSP { w1 = "ERR", w2 = "ERR" };
            }
        }

        private class TSP
        {
            public string w1 { get; set; }
            public string w2 { get; set; }
        }

        private class TRow
        {
            public string c00 { get; set; }
            public string c01 { get; set; }
            public string c02 { get; set; }
            public string c03 { get; set; }
            public string c04 { get; set; }
            public string c05 { get; set; }
            public string c06 { get; set; }
            public string c07 { get; set; }
            public string c08 { get; set; }
            public string c09 { get; set; }
            public string c10 { get; set; }
            public string c11 { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string need_rebuild_meeting { get; set; }
            public string in_battle_type { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
        }

        private DateTime GetDtmVal(string value)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            return result;
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}