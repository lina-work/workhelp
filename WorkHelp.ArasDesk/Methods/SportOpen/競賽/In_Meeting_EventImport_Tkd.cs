﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.SportOpen.競賽
{
    internal class In_Meeting_EventImport_Tkd : Item
    {
        public In_Meeting_EventImport_Tkd(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 場次編號匯入
                日誌: 
                    - 2022-10-03: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_EventImport_Tkd";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "import":
                    Import(cfg, itmR);
                    break;

                case "match_tree_no":
                    RunMatchTreeNo(cfg, itmR);
                    break;

                case "reset_local_schedule":
                    UpdateInLocalScheduleAgain(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void UpdateInLocalScheduleAgain(TConfig cfg, Item itmReturn)
        {
            var code = "";
            var sql = "SELECT id, item_number FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            var itmMeeting = cfg.inn.applySQL(sql);
            var item_number = itmMeeting.getProperty("item_number", "");

            code = "128";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);


            code = "64";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);


            code = "32";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);


            code = "16";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);


            code = "8";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);


            code = "4";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);


            code = "2";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);


            code = "34";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);


            code = "BC04";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);

            code = "BC03";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);


            code = "BC02";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);


            code = "BC01";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);

            code = "BC3";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);


            code = "1";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);


            code = "BC2";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);

            code = "BC1";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);

        }

        private void RunUpdateInLocalSchedule(TConfig cfg, string item_number, string esystem)
        {
            var sql = "UPDATE In_Local_Schedule SET syncflag = NULL WHERE mnumber = '" + item_number + "' AND esystem = '" + esystem + "'";
            cfg.inn.applySQL(sql);
        }

        private void RunLocalSync(TConfig cfg)
        {
            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting_PEvent");
            itmData.apply("In_Local_Sync");
        }

        private void RunMatchTreeNo(TConfig cfg, Item itmReturn)
        {
            //本次匯入所有組別資料
            var itmPrograms = GetImportProgramItems(cfg, itmReturn);

            //將所有場次編號變為 9999
            Update9999(cfg, itmPrograms);

            //匹配場次編號
            MatchTreeNo(cfg);

            //修正準決賽名次
            FixSemiFinalRank(cfg);

            //清空五七名場次 (人數不足)
            ClearRank57TreeNo(cfg, itmPrograms);

            //Fix籤號位置與規格不同
            FixBypassStatus(cfg, itmReturn);
        }

        //Fix籤號位置與規格不同
        private void FixBypassStatus(TConfig cfg, Item itmReturn)
        {
            Item itmData = cfg.inn.newItem("In_Meeting");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("scene", "fix");
            itmData.apply("in_meeting_program_score_fix");
        }

        //將所有場次編號變為 9999
        private void Update9999(TConfig cfg, Item itmPrograms)
        {
            var count = itmPrograms.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmProgram = itmPrograms.getItemByIndex(i);
                var program_id = itmProgram.getProperty("program_id", "");

                var sql_upd = "UPDATE IN_MEETING_PEVENT SET"
                    + "	in_tree_no = 9999"
                    + " WHERE source_id = '" + program_id + "'"
                    + " AND ISNULL(in_tree_no, 0) > 0";

                cfg.inn.applySQL(sql_upd);
            }
        }


        //清空五七名場次 (人數不足)
        private void ClearRank57TreeNo(TConfig cfg, Item itmPrograms)
        {
            var count = itmPrograms.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmProgram = itmPrograms.getItemByIndex(i);
                var program_id = itmProgram.getProperty("program_id", "");
                var in_team_count = itmProgram.getProperty("in_team_count", "0");
                var team_count = GetIntVal(in_team_count);

                var sql_upd = "";

                //不取第七名
                sql_upd = "UPDATE IN_MEETING_PEVENT SET"
                    + " in_tree_rank_ns = '5,0'"
                    + ", in_tree_rank_nss = '5,0'"
                    + " WHERE source_id = '" + program_id + "'"
                    + " AND in_tree_id IN ('rank57a', 'rank57b')";
                cfg.inn.applySQL(sql_upd);

                if (team_count <= 6)
                {
                    //不取五七名，清空場次編號
                    sql_upd = "UPDATE IN_MEETING_PEVENT SET"
                        + "	 in_tree_no = NULL"
                        + ", in_tree_rank_ns = ''"
                        + ", in_tree_rank_nss = ''"
                        + " WHERE source_id = '" + program_id + "'"
                        + " AND in_tree_id IN ('rank57a', 'rank57b')";

                    cfg.inn.applySQL(sql_upd);
                }

                if (team_count <= 4)
                {
                    //不取三四名，清空場次編號
                    sql_upd = "UPDATE IN_MEETING_PEVENT SET"
                        + "	 in_tree_no = NULL"
                        + ", in_tree_rank_ns = ''"
                        + ", in_tree_rank_nss = ''"
                        + " WHERE source_id = '" + program_id + "'"
                        + " AND in_tree_id IN ('rank34')";
                    cfg.inn.applySQL(sql_upd);

                    //不取三三名
                    sql_upd = "UPDATE IN_MEETING_PEVENT SET"
                        + "  in_tree_rank_ns = '2,0'"
                        + ", in_tree_rank_nss = '2,0'"
                        + " WHERE source_id = '" + program_id + "'"
                        + " AND in_tree_rank IN ('rank23')";
                    cfg.inn.applySQL(sql_upd);
                }
            }
        }

        //修正準決賽名次
        private void FixSemiFinalRank(TConfig cfg)
        {
            string sql = @"
                UPDATE IN_MEETING_PEVENT SET
                    in_tree_rank = 'rank23'
                    , in_tree_rank_ns = '2,3'
                    , in_tree_rank_nss = '2,3'
                where in_meeting = '{#meeting_id}'
                AND in_tree_name = 'main'
                AND in_round_code = 4
                AND ISNULL(in_robin_key, '') = ''
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        private void MatchTreeNo(TConfig cfg)
        {
            ////將場次分配狀態清空
            //ClearAllocateStatus(cfg);

            var itmImports = GetImportItems(cfg);
            var count = itmImports.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmImport = itmImports.getItemByIndex(i);
                var row = MapRow(cfg, itmImport);
                RunMapTreeNo(cfg, row);
            }
        }

        private void RunMapTreeNo(TConfig cfg, TRow row)
        {
            var mat = RunMatch(cfg, row);
            if (mat.q == QueryType.Player)
            {
                var itmEvent1 = QueryMatchEventByPlayer(cfg, row);
                if (itmEvent1 == null)
                {
                    LogErr(cfg, "EVT", row.c05, row.c16, "", "player");
                }
                else
                {
                    UpdateEvent(cfg, row, itmEvent1);
                    CheckNext(cfg, row, itmEvent1);
                }
            }
            else if (mat.q == QueryType.Round)
            {
                var itmEvent2 = QueryMatchEventByRound(cfg, row, mat);
                if (itmEvent2 == null)
                {
                    LogErr(cfg, "EVT", row.c05, row.c16, mat.in_round_code, "round_code");
                }
                else
                {
                    UpdateEvent(cfg, row, itmEvent2);
                }
            }
            else if (mat.q == QueryType.TreeId)
            {
                var itmEvent3 = QueryMatchEventByTreeId(cfg, row, mat);
                if (itmEvent3 == null)
                {
                    LogErr(cfg, "EVT", row.c05, row.c16, mat.in_tree_id, "tree_id");
                }
                else
                {
                    UpdateEvent(cfg, row, itmEvent3);
                }
            }
            else if (mat.q == QueryType.FightId)
            {
                var itmEvent3 = QueryMatchEventByFightId(cfg, row, mat);
                if (itmEvent3 == null)
                {
                    LogErr(cfg, "EVT", row.c05, row.c16, mat.in_tree_id, "tree_id");
                }
                else
                {
                    UpdateEvent(cfg, row, itmEvent3);
                }
            }
        }

        private void CheckNext(TConfig cfg, TRow row, Item itmEvent)
        {
            var next_evt_id_w = itmEvent.getProperty("in_next_win", "");
            var next_evt_id_l = itmEvent.getProperty("in_next_lose", "");

            if (row.c18 != "")
            {
                //勝者有下一場
                var itmNextSchW = GetImportItemFromERound(cfg, row.c05, row.c18);
                if (itmNextSchW.isError() || itmNextSchW.getResult() == "")
                {
                    LogErr(cfg, "SCH", row.c05, row.c18, "", "WIN");
                }
                else
                {
                    var itmNextEvtW = QueryMatchEventByEvtId(cfg, row, next_evt_id_w);
                    if (itmNextEvtW == null)
                    {
                        LogErr(cfg, "EVT", row.c05, row.c18, next_evt_id_w, "WIN");
                    }
                    else
                    {
                        var next_row_w = MapRow(cfg, itmNextSchW);
                        UpdateEvent(cfg, next_row_w, itmNextEvtW);
                        CheckNext(cfg, next_row_w, itmNextEvtW);
                    }
                }
            }

            if (row.c21 != "")
            {
                //敗者有下一場
                var itmNextSchL = GetImportItemFromERound(cfg, row.c05, row.c21);
                if (itmNextSchL.isError() || itmNextSchL.getResult() == "")
                {
                    LogErr(cfg, "SCH", row.c05, row.c21, "", "LOSE");
                }
                else
                {
                    var itmNextEvtL = QueryMatchEventByEvtId(cfg, row, next_evt_id_l);
                    if (itmNextEvtL == null)
                    {
                        LogErr(cfg, "EVT", row.c05, row.c21, next_evt_id_l, "LOSE");
                    }
                    else
                    {
                        var next_row_l = MapRow(cfg, itmNextSchL);
                        UpdateEvent(cfg, next_row_l, itmNextEvtL);
                        CheckNext(cfg, next_row_l, itmNextEvtL);
                    }
                }
            }
        }

        private void LogErr(TConfig cfg, string type, string in_program_no, string in_tree_no, string evt_id, string note)
        {
            var message = "查無[" + type + "]: " + in_program_no + " | " + in_tree_no + " | " + evt_id + " | " + note;
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName + "_Error", message);
        }

        private Item GetImportItemFromERound(TConfig cfg, string in_program_no, string next_tree_no)
        {
            string sql = @"
                SELECT 
	                t1.*
	                , t2.id AS 'site_id'
	                , t2.in_code AS 'site_code'
                FROM 
	                IN_MEETING_TKDSCHEDULE t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                IN_MEETING_SITE t2 WITH(NOLOCK)
	                ON t2.in_meeting = '{#meeting_id}'
	                AND t2.in_code = t1.c02
                WHERE
	                t1.c05 = '{#in_program_no}'
	                AND t1.c16 = '{#next_tree_no}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_program_no}", in_program_no)
                .Replace("{#next_tree_no}", next_tree_no);

            return cfg.inn.applySQL(sql);
        }

        private void UpdateEvent(TConfig cfg, TRow row, Item itmEvt)
        {
            var evt_id = itmEvt.getProperty("id", "");

            var in_tree_no = row.c16;
            var site_id = row.Value.getProperty("site_id", "");
            var site_code = row.Value.getProperty("site_code", "");

            string sql_upd = "UPDATE IN_MEETING_PEVENT SET"
                + "  in_site_no = '" + in_tree_no + "'"
                + ", in_site_code = '" + site_code + "'"
                + ", in_site_id = '" + in_tree_no + "'"
                + ", in_tree_no = '" + in_tree_no + "'"
                + ", in_site = '" + site_id + "'"
                + ", in_site_allocate = '1'"
                + " WHERE id = '" + evt_id + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql_upd);

            cfg.inn.applySQL(sql_upd);
        }

        private Item QueryMatchEventByEvtId(TConfig cfg, TRow row, string evt_id)
        {
            string sql = @"
                SELECT 
                    t2.*
                FROM 
					IN_MEETING_PROGRAM t1 WITH(NOLOCK)
				INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
					ON t2.source_id = t1.id
                WHERE 
	                t2.id = '{#id}'
            ";

            sql = sql.Replace("{#id}", evt_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError() || item.getResult() == "")
            {
                item = null;
            }

            return item;
        }

        private Item QueryMatchEventByTreeId(TConfig cfg, TRow row, TMatch match)
        {
            string sql = @"
                SELECT TOP 1 
                    t2.*
                FROM 
					IN_MEETING_PROGRAM t1 WITH(NOLOCK)
				INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
					ON t2.source_id = t1.id
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_program_no = '{#in_program_no}'
					AND t2.in_tree_id = '{#in_tree_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_program_no}", row.c05)
                .Replace("{#in_tree_id}", match.in_tree_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError() || item.getResult() == "")
            {
                item = null;
            }

            return item;
        }

        private Item QueryMatchEventByFightId(TConfig cfg, TRow row, TMatch match)
        {
            string sql = @"
                SELECT TOP 1 
                    t2.*
                FROM 
					IN_MEETING_PROGRAM t1 WITH(NOLOCK)
				INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
					ON t2.source_id = t1.id
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_program_no = '{#in_program_no}'
					AND t2.in_fight_id = '{#in_fight_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_program_no}", row.c05)
                .Replace("{#in_fight_id}", match.in_fight_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError() || item.getResult() == "")
            {
                item = null;
            }

            return item;
        }

        private Item QueryMatchEventByRound(TConfig cfg, TRow row, TMatch match)
        {
            string sql = @"
                SELECT TOP 1 
                    t2.*
                FROM 
					IN_MEETING_PROGRAM t1 WITH(NOLOCK)
				INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
					ON t2.source_id = t1.id
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_program_no = '{#in_program_no}'
					AND t2.in_tree_name = '{#in_tree_name}'
					AND t2.in_round_code = {#in_round_code}
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_program_no}", row.c05)
                .Replace("{#in_tree_name}", match.in_tree_name)
                .Replace("{#in_round_code}", match.in_round_code);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError() || item.getResult() == "")
            {
                item = null;
            }

            return item;
        }

        private Item QueryMatchEventByPlayer(TConfig cfg, TRow row)
        {
            string sql = @"
                SELECT TOP 1 
                    t2.*
                FROM 
					IN_MEETING_PROGRAM t1 WITH(NOLOCK)
				INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
					ON t2.source_id = t1.id
				INNER JOIN
					IN_MEETING_PEVENT_DETAIL t11 WITH(NOLOCK)
					ON t11.source_id = t2.id
					AND t11.in_sign_foot = 1
				INNER JOIN
					IN_MEETING_PEVENT_DETAIL t12 WITH(NOLOCK)
					ON t12.source_id = t2.id
					AND t12.in_sign_foot = 2
				INNER JOIN
					VU_MEETING_PTEAM t21 WITH(NOLOCK)
					ON t21.source_id = t1.id
					AND t21.in_sign_no = t11.in_sign_no
				INNER JOIN
					VU_MEETING_PTEAM t22 WITH(NOLOCK)
					ON t22.source_id = t1.id
					AND t22.in_sign_no = t12.in_sign_no
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_program_no = '{#in_program_no}'
					AND t21.in_team_index = '{#c10}'
					AND t22.in_team_index = '{#c13}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_program_no}", row.c05)
                .Replace("{#c10}", row.c10)
                .Replace("{#c13}", row.c13);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError() || item.getResult() == "")
            {
                item = null;
            }

            return item;
        }

        private Item GetImportItems(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t1.*
					, t2.id      AS 'site_id'
					, t2.in_code AS 'site_code'
                FROM 
	                IN_MEETING_TKDSCHEDULE t1 WITH(NOLOCK)
				LEFT OUTER JOIN
					IN_MEETING_SITE t2 WITH(NOLOCK)
					ON t2.in_meeting = '{#meeting_id}'
					AND t2.in_code = t1.c02
                ORDER BY 
	                c00
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetImportProgramItems(TConfig cfg, Item itmReturn)
        {
            var sql = @"
                SELECT DISTINCT
	                t1.in_program_no
	                , t2.id AS 'program_id'
	                , t2.in_team_count
                FROM
	                IN_MEETING_TKDSCHEDULE t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.in_meeting = t1.in_meeting
	                AND t2.in_program_no = t1.in_program_no
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                ORDER BY
	                t1.in_program_no
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private TRow MapRow(TConfig cfg, Item item)
        {
            var result = new TRow
            {
                c00 = item.getProperty("c00", ""),
                c01 = item.getProperty("c01", ""),
                c02 = item.getProperty("c02", ""),
                c03 = item.getProperty("c03", ""),
                c04 = item.getProperty("c04", ""),
                c05 = item.getProperty("c05", ""),
                c06 = item.getProperty("c06", ""),
                c07 = item.getProperty("c07", ""),
                c08 = item.getProperty("c08", ""),
                c09 = item.getProperty("c09", ""),
                c10 = item.getProperty("c10", ""),
                c11 = item.getProperty("c11", ""),
                c12 = item.getProperty("c12", ""),
                c13 = item.getProperty("c13", ""),
                c14 = item.getProperty("c14", ""),
                c15 = item.getProperty("c15", ""),
                c16 = item.getProperty("c16", ""),
                c17 = item.getProperty("c17", ""),
                c18 = item.getProperty("c18", ""),
                c19 = item.getProperty("c19", ""),
                c20 = item.getProperty("c20", ""),
                c21 = item.getProperty("c21", ""),
                c22 = item.getProperty("c22", ""),
                Value = item,
            };

            result.tno = item.getProperty("c16", "");
            result.Value = item;

            return result;
        }

        private void Import(TConfig cfg, Item itmReturn)
        {
            RunImport(cfg, itmReturn);
            var sql = "SELECT c05, c16 FROM IN_MEETING_TKDSCHEDULE WITH(NOLOCK) GROUP BY c05, c16 HAVING COUNT(*) > 1";
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            if (count > 0)
            {
                throw new Exception("場次編號重複");
            }
        }

        private void RunImport(TConfig cfg, Item itmReturn)
        {
            var value = itmReturn.getProperty("value", "");
            if (value == "") throw new Exception("無匯入資料");

            var list = MapList(cfg, value);
            if (list == null || list.Count == 0) throw new Exception("無匯入資料");

            cfg.inn.applySQL("DELETE FROM In_Meeting_TkdSchedule");

            for (var i = 0; i < list.Count; i++)
            {
                var row = list[i];
                Item itmNew = cfg.inn.newItem("In_Meeting_TkdSchedule", "add");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("c00", ClearVal(row.c00));
                itmNew.setProperty("c01", ClearVal(row.c01));
                itmNew.setProperty("c02", ClearVal(row.c02));
                itmNew.setProperty("c03", ClearVal(row.c03));
                itmNew.setProperty("c04", ClearVal(row.c04));
                itmNew.setProperty("c05", ClearVal(row.c05));
                itmNew.setProperty("c06", ClearVal(row.c06));
                itmNew.setProperty("c07", ClearVal(row.c07));
                itmNew.setProperty("c08", ClearVal(row.c08));
                itmNew.setProperty("c09", ClearVal(row.c09));
                itmNew.setProperty("c10", ClearVal(row.c10));
                itmNew.setProperty("c11", ClearVal(row.c11));
                itmNew.setProperty("c12", ClearVal(row.c12));
                itmNew.setProperty("c13", ClearVal(row.c13));
                itmNew.setProperty("c14", ClearVal(row.c14));
                itmNew.setProperty("c15", ClearVal(row.c15));
                itmNew.setProperty("c16", ClearVal(row.c16));
                itmNew.setProperty("c17", ClearVal(row.c17));
                itmNew.setProperty("c18", ClearVal(row.c18));
                itmNew.setProperty("c19", ClearVal(row.c19));
                itmNew.setProperty("c20", ClearVal(row.c20));
                itmNew.setProperty("c21", ClearVal(row.c21));
                itmNew.setProperty("c22", ClearVal(row.c22));

                itmNew.setProperty("in_l1", "");
                itmNew.setProperty("in_l2", ClearVal(row.c07));
                itmNew.setProperty("in_l3", ClearVal(row.c08));

                itmNew.setProperty("in_section_name", ClearVal(row.c06));
                itmNew.setProperty("in_program_no", ClearVal(row.c05));

                itmNew = itmNew.apply();
            }
        }

        private string ClearVal(string value)
        {
            if (value == null) return "";
            return value.Replace("\t", "").Trim();
        }

        private List<TRow> MapList(TConfig cfg, string value)
        {
            try
            {
                var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TRow>>(value);
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private TMatch RunMatch(TConfig cfg, TRow row)
        {
            //ESystem 代號 備註	
            //   1: 冠亞軍
            //   3: 輸的第3
            //   35: 贏的第3、輸的第5
            //   4R: 贏的第3
            //   57: 贏的第5、輸的第7
            //   7: 輸的第7
            //   7R: 贏的第7
            //   56: 56名
            //   78: 78名
            //   99A: A組循環賽    6、7人
            //   99B: B組循環賽    6、7人
            //   99: 循環賽   5人以下

            var result = new TMatch
            {
                e = TreeType.NONE,
                q = QueryType.NONE,
                in_tree_name = "",
                in_tree_id = "",
                in_round_code = "",
            };


            switch (row.c04)
            {
                case "64":
                    result.q = QueryType.Player;
                    result.e = TreeType.MAIN;
                    result.in_tree_name = "main";
                    result.in_round_code = "128";
                    break;
                case "32":
                    result.q = QueryType.Player;
                    result.e = TreeType.MAIN;
                    result.in_tree_name = "main";
                    result.in_round_code = "64";
                    break;
                case "16":
                    result.q = QueryType.Player;
                    result.e = TreeType.MAIN;
                    result.in_tree_name = "main";
                    result.in_round_code = "32";
                    break;
                case "8":
                    result.q = QueryType.Player;
                    result.e = TreeType.MAIN;
                    result.in_tree_name = "main";
                    result.in_round_code = "16";
                    break;
                case "4":
                    result.q = QueryType.Player;
                    result.e = TreeType.MAIN;
                    result.in_tree_name = "main";
                    result.in_round_code = "8";
                    break;
                case "2":
                    result.q = QueryType.Player;
                    result.e = TreeType.MAIN;
                    result.in_tree_name = "main";
                    result.in_round_code = "4";
                    break;
                case "1":
                    result.q = QueryType.Round;
                    result.e = TreeType.MAIN;
                    result.in_tree_name = "main";
                    result.in_round_code = "2";
                    break;
                case "34":
                    result.q = QueryType.TreeId;
                    result.e = TreeType.RANK34;
                    result.in_tree_id = "rank34";
                    break;

                //case "7"://輸的第7
                //    result.q = QueryType.FightId;
                //    result.e = TreeType.RPC;
                //    result.in_tree_name = "repechage";
                //    switch (sub)
                //    {
                //        case "93": result.in_fight_id = "R008-01"; break;
                //        case "94": result.in_fight_id = "R008-02"; break;
                //        default: break;
                //    }
                //    break;

                //case "35":
                //    result.q = QueryType.FightId;
                //    result.e = TreeType.RPC;
                //    result.in_tree_name = "repechage";
                //    switch (sub)
                //    {
                //        case "95": result.in_fight_id = "R004-01"; break;
                //        case "96": result.in_fight_id = "R004-02"; break;
                //        default: break;
                //    }
                //    break;

                case "57":
                    result.q = QueryType.TreeId;
                    result.e = TreeType.RANK57;
                    if (row.c15 == "1")
                    {
                        result.in_tree_id = "rank57a";
                    }
                    else if (row.c15 == "2")
                    {
                        result.in_tree_id = "rank57b";
                    }
                    break;

                case "BC04":
                    result.q = QueryType.FightId;
                    result.e = TreeType.RPC;
                    result.in_tree_name = "repechage";
                    result.in_fight_id = AnalysisSchedules(cfg, row, "R032-01", "R032-02");
                    if (result.in_fight_id == "")
                    {
                        result.q = QueryType.ERROR;
                    }
                    break;

                case "BC03":
                    result.q = QueryType.FightId;
                    result.e = TreeType.RPC;
                    result.in_tree_name = "repechage";
                    result.in_fight_id = AnalysisSchedules(cfg, row, "R016-01", "R016-02");
                    if (result.in_fight_id == "")
                    {
                        result.q = QueryType.ERROR;
                    }
                    break;

                case "BC02":
                    result.q = QueryType.FightId;
                    result.e = TreeType.RPC;
                    result.in_tree_name = "repechage";
                    result.in_fight_id = AnalysisSchedules(cfg, row, "R008-01", "R008-02");
                    if (result.in_fight_id == "")
                    {
                        result.q = QueryType.ERROR;
                    }
                    break;

                case "BC01":
                    result.q = QueryType.FightId;
                    result.e = TreeType.RPC;
                    result.in_tree_name = "repechage";
                    result.in_fight_id = AnalysisSchedules(cfg, row, "R004-01", "R004-02");
                    if (result.in_fight_id == "")
                    {
                        result.q = QueryType.ERROR;
                    }
                    break;

                case "BC3":
                    result.q = QueryType.FightId;
                    result.e = TreeType.RPC;
                    result.in_tree_name = "repechage";
                    result.in_fight_id = "R002-01";
                    break;

                case "BC2":
                    result.q = QueryType.FightId;
                    result.e = TreeType.RPC;
                    result.in_tree_name = "challenge-a";
                    result.in_fight_id = "CLG1-01";
                    break;

                case "BC1":
                    result.q = QueryType.FightId;
                    result.e = TreeType.RPC;
                    result.in_tree_name = "challenge-a";
                    result.in_fight_id = "CLG1-02";
                    break;

                //case "3":
                //    result.q = QueryType.FightId;
                //    result.e = TreeType.RANK23;
                //    if (row.c34 == "L")
                //    {
                //        result.in_fight_id = "M004-01";
                //    }
                //    else if (row.c34 == "R")
                //    {
                //        result.in_fight_id = "M004-02";
                //    }
                //    break;

                //case "99":
                //    result.q = QueryType.Player;
                //    result.e = TreeType.ROBIN;
                //    result.in_tree_name = "robin";
                //    break;

                //case "99A":
                //    result.q = QueryType.Player;
                //    result.e = TreeType.CROSS;
                //    result.in_tree_name = "cross";
                //    break;

                //case "99B":
                //    result.q = QueryType.Player;
                //    result.e = TreeType.CROSS;
                //    result.in_tree_name = "cross";
                //    break;

                default:
                    break;
            }

            return result;
        }


        private string AnalysisSchedules(TConfig cfg, TRow row, string fid_l, string fid_r)
        {
            var sql = @"
                SELECT 
	                c16
                FROM 
	                IN_MEETING_TKDSCHEDULE WITH(NOLOCK)
                WHERE
	                c05 = '{#c05}'
	                AND c04 = '{#c04}'
                ORDER BY
	                c15
            ";

            sql = sql.Replace("{#c04}", row.c04)
                .Replace("{#c05}", row.c05);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            if (count != 2) return "";

            var mn = items.getItemByIndex(0).getProperty("c16", "");
            var mx = items.getItemByIndex(1).getProperty("c16", "");
            if (row.c16 == mn) return fid_l;
            if (row.c16 == mx) return fid_r;

            return "";
        }

        private enum QueryType
        {
            NONE = 0,
            Round = 100,
            Player = 200,
            TreeId = 300,
            FightId = 400,
            ERROR = 9999,
        }

        private enum TreeType
        {
            NONE = 0,
            MAIN = 100,
            RPC = 200,
            ROBIN = 300,
            CROSS = 400,
            RANK23 = 2300,
            RANK34 = 3400,
            RANK35 = 3500,
            RANK57 = 5700,
            RANK78 = 7800,
        }

        private class TMatch
        {
            public string in_tree_name { get; set; }
            public string in_tree_id { get; set; }
            public string in_round_code { get; set; }
            public string in_fight_id { get; set; }
            public TreeType e { get; set; }
            public QueryType q { get; set; }
        }

        private class TRow
        {
            public string c00 { get; set; }
            public string c01 { get; set; }
            public string c02 { get; set; }
            public string c03 { get; set; }
            public string c04 { get; set; }
            public string c05 { get; set; }
            public string c06 { get; set; }
            public string c07 { get; set; }
            public string c08 { get; set; }
            public string c09 { get; set; }
            public string c10 { get; set; }
            public string c11 { get; set; }
            public string c12 { get; set; }
            public string c13 { get; set; }
            public string c14 { get; set; }
            public string c15 { get; set; }
            public string c16 { get; set; }
            public string c17 { get; set; }
            public string c18 { get; set; }
            public string c19 { get; set; }
            public string c20 { get; set; }
            public string c21 { get; set; }
            public string c22 { get; set; }

            public string tno { get; set; }
            public Item Value { get; set; }
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}