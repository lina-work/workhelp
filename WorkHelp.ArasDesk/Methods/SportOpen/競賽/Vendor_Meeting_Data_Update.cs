﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.SportOpen.競賽
{
    internal class Vendor_Meeting_Data_Update : Item
    {
        public Vendor_Meeting_Data_Update(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 競賽系統-資料修改通用函式
                日誌: 
                    - 2024-12-18: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "Vendor_Meeting_Data_Update";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                itemType = itmR.getProperty("itemType", "").ToUpper(),
                property = itmR.getProperty("property", "").ToLower(),
                id = itmR.getProperty("id", ""),
                value = itmR.getProperty("value", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "update-column"://單欄位修改
                    AnalysisUpdateColumn(cfg, itmR);
                    break;

                case "merge-entity"://整筆資料修改
                    AnalysisMergeOneEntity(cfg, itmR);
                    break;

                case "remove-entity":
                    RemoveOneEntity(cfg, itmR);
                    break;
            }

            return itmR;
        }

        //單欄位修改
        private void AnalysisUpdateColumn(TConfig cfg, Item itmReturn)
        {
            switch (cfg.itemType)
            {
                case "DISTINCT_MEETING_ORG":
                    UpdateMeetingUserOrg(cfg, itmReturn);
                    break;

                default:
                    UpdateOneColumn(cfg, itmReturn);
                    AfterOneColumn(cfg, itmReturn);
                    break;
            }
        }

        private void UpdateMeetingUserOrg(TConfig cfg, Item itmReturn)
        {
            var meeting_id = itmReturn.getProperty("meeting_id", "");
            var old_current_org = itmReturn.getProperty("id", "");
            var sql1 = "UPDATE IN_MEETING_USER SET [" + cfg.property + "] = N'" + cfg.value + "' WHERE [source_id] = '" + meeting_id + "' AND [in_current_org] = N'" + old_current_org + "'";
            var sql2 = "UPDATE IN_MEETING_PTEAM SET [" + cfg.property + "] = N'" + cfg.value + "' WHERE [in_meeting] = '" + meeting_id + "' AND [in_current_org] = N'" + old_current_org + "'";
            // cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql1);
            // cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql2);
            cfg.inn.applySQL(sql1);
            cfg.inn.applySQL(sql2);
        }

        private void AfterOneColumn(TConfig cfg, Item itmReturn)
        {
            switch (cfg.itemType)
            {
                case "IN_MEETING_PEVENT":
                    if (cfg.property == "in_site")
                    {
                        UpdateSiteCode(cfg, itmReturn);
                    }
                    break;
                case "IN_MEETING_PEVENT_DETAIL":
                    if (cfg.property == "in_sign_no")
                    {
                        UpdateOppSignNo(cfg, cfg.id);
                    }
                    break;
            }
        }

        private void UpdateOppSignNo(TConfig cfg, string id)
        {
            var sql_upd = @"
                UPDATE t2 SET
	                t2.in_target_foot = t1.in_sign_foot
	                , t2.in_target_no = t1.in_sign_no
                FROM
	                IN_MEETING_PEVENT_DETAIL t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.source_id
	                AND t2.id <> t1.id
                WHERE
	                t1.id = '{#id}'
            ";

            sql_upd = sql_upd.Replace("{#id}", id);

            cfg.inn.applySQL(sql_upd);
        }

        private void UpdateSiteCode(TConfig cfg, Item itmReturn)
        {
            var sql = @"
                UPDATE t1 SET
                	t1.in_site_code = t2.in_code
                FROM
                	IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_SITE t2 WITH(NOLOCK)
                	ON t2.id = t1.in_site
                WHERE
                	t1.id = '{#id}'
            ";

            sql = sql.Replace("{#id}", cfg.id);

            cfg.inn.applySQL(sql);
        }

        private void RemoveOneEntity(TConfig cfg, Item itmReturn)
        {
            if (cfg.id == "") throw new Exception("無 id");

            var itmOld = cfg.inn.applySQL("SELECT id FROM [" + cfg.itemType + "] WITH(NOLOCK) WHERE id = '" + cfg.id + "'");
            if (itmOld.isError() || itmOld.getResult() == "")
            {
                throw new Exception("查無資料");
            }

            var itmType = cfg.inn.applySQL("SELECT [name] FROM ITEMTYPE WITH(NOLOCK) WHERE [instance_data] = '" + cfg.itemType + "'");
            if (itmType.isError() || itmType.getResult() == "")
            {
                throw new Exception("查無物件類型");
            }

            var sql = "DELETE FROM [" + cfg.itemType + "] WHERE id = '" + cfg.id + "'";
            cfg.inn.applySQL(sql);
        }

        //整筆資料修改
        private void AnalysisMergeOneEntity(TConfig cfg, Item itmReturn)
        {
            MergeOneEntity(cfg, itmReturn);

            switch (cfg.itemType)
            {
                case "IN_MEETING_PEVENT":
                    UpdateSiteCode(cfg, itmReturn);
                    break;

                case "IN_MEETING_PEVENT_DETAIL":
                    if (cfg.newId != "")
                    {
                        var sql = "SELECT id, in_meeting, in_fight_day FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE id = '" + cfg.sourceId + "'";
                        cfg.itmEvent = cfg.inn.applySQL(sql);
                        CheckOpponent(cfg, itmReturn);
                        UpdateEventWinSignNo(cfg, itmReturn);
                        TriggerBroadcast(cfg, cfg.itmEvent);
                    }
                    break;
            }
        }

        private void TriggerBroadcast(TConfig cfg, Item itmEvent)
        {
            var itmData = cfg.inn.newItem("In_Meeting_PEvent");
            itmData.setProperty("id", itmEvent.getProperty("in_meeting", ""));
            itmData.setProperty("day", itmEvent.getProperty("in_fight_day", ""));
            itmData.apply("In_Meeting_OverviewBroadcast_Tkd");
        }

        private void CheckOpponent(TConfig cfg, Item itmReturn)
        {
            if (cfg.newId == "") return;
            if (cfg.values == null || cfg.values.Count == 0) return;

            var sql = "SELECT source_id, id, in_sign_foot, in_sign_no FROM IN_MEETING_PEVENT_DETAIL WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.sourceId + "' AND id <> '" + cfg.newId + "'";

            var itmOpp = cfg.inn.applySQL(sql);

            if (itmOpp.getResult() != "")
            {
                UpdateOppEventDetail(cfg, itmOpp);
            }
            else
            {
                CreateOppEventDetail(cfg);
            }
        }

        private void UpdateOppEventDetail(TConfig cfg, Item itmOpp)
        {
            var op_id = itmOpp.getProperty("id", "");
            var my_sign_foot = cfg.itmData.getProperty("in_sign_foot", "");
            var my_sign_no = cfg.itmData.getProperty("in_sign_no", "");
            var my_status = cfg.itmData.getProperty("in_status", "");

            //我方勝利，敵方則敗
            var in_status = my_status == "1"
                ? ", in_status = '0'"
                : "";

            var sql_upd = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + "  in_target_foot = '" + my_sign_foot + "'"
                + ", in_target_no = '" + my_sign_no + "'"
                + in_status
                + " WHERE id = '" + op_id + "'";

            cfg.inn.applySQL(sql_upd);
        }

        private void CreateOppEventDetail(TConfig cfg)
        {
            var my_sign_foot = cfg.itmData.getProperty("in_sign_foot", "");
            var my_sign_no = cfg.itmData.getProperty("in_sign_no", "");
            var op_sign_foot = my_sign_foot == "1" ? "2" : "1";

            var itmNew = cfg.inn.newItem(cfg.typeName, "add");
            for (var i = 0; i < cfg.values.Count; i++)
            {
                var x = cfg.values[i];
                if (x == null) continue;
                if (x.key == null || x.key == "") continue;
                if (x.value == null) continue;
                itmNew.setProperty(x.key, x.value);
            }

            itmNew.setProperty("source_id", cfg.sourceId);
            itmNew.setProperty("in_sign_foot", op_sign_foot);
            itmNew.setProperty("in_sign_no", "");
            itmNew.setProperty("in_target_foot", my_sign_foot);
            itmNew.setProperty("in_target_no", my_sign_no);

            itmNew.apply();
        }

        private void UpdateEventWinSignNo(TConfig cfg, Item itmReturn)
        {
            var sql = @"
                UPDATE t11 SET 
                    t11.in_win_status = 'fight'
                    , t11.in_win_time = GETUTCDATE()
                	, t11.in_win_sign_no = t12.in_sign_no
                FROM
                	IN_MEETING_PEVENT t11 WITH(NOLOCK)
                INNER JOIN
                (
                	SELECT TOP 1
                		t1.source_id
                		, t1.in_sign_no
                	FROM
                		IN_MEETING_PEVENT_DETAIL t1 WITH(NOLOCK)
                	WHERE
                		EXISTS
                		(
                			SELECT 
                				t2.source_id
                			FROM
                				IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                			WHERE
                				t2.id = '{#id}'
                				AND t2.source_id = t1.source_id
                		)
                		AND ISNULL(t1.in_status, '') = '1'
                ) t12 ON t12.source_id = t11.id
            ";

            sql = sql.Replace("{#id}", cfg.id);

            cfg.inn.applySQL(sql);


            sql = @"
                UPDATE t3 SET
                	t3.in_sign_no = t1.in_win_sign_no
                FROM
                	IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
                	ON t3.source_id = t1.in_next_win
                	AND t3.in_sign_foot = t1.in_next_foot_win
                WHERE
                	t1.id = '{#id}'
            ";
            sql = sql.Replace("{#id}", cfg.sourceId);

            cfg.inn.applySQL(sql);

            sql = @"
                UPDATE t3 SET
                	t3.in_target_no = t1.in_win_sign_no
                FROM
                	IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
                	ON t3.source_id = t1.in_next_win
                	AND t3.in_sign_foot <> t1.in_next_foot_win
                WHERE
                	t1.id = '{#id}'
            ";
            sql = sql.Replace("{#id}", cfg.sourceId);

            cfg.inn.applySQL(sql);
        }

        private void MergeOneEntity(TConfig cfg, Item itmReturn)
        {
            var json = itmReturn.getProperty("values", "");
            var values = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TCol>>(json);
            if (values.Count == 0) throw new Exception("無更新資料");

            if (cfg.id != "")
            {
                var itmOld = cfg.inn.applySQL("SELECT id FROM [" + cfg.itemType + "] WITH(NOLOCK) WHERE id = '" + cfg.id + "'");
                if (itmOld.isError() || itmOld.getResult() == "")
                {
                    throw new Exception("查無資料");
                }
            }

            var itmType = cfg.inn.applySQL("SELECT [name] FROM ITEMTYPE WITH(NOLOCK) WHERE [instance_data] = '" + cfg.itemType + "'");
            var name = itmType.getProperty("name", "");
            var isEdit = cfg.id != "";
            var action = isEdit ? "merge" : "add";

            var itmNew = cfg.inn.newItem(name, action);
            if (isEdit) itmNew.setAttribute("where", "id = '" + cfg.id + "'");

            for (var i = 0; i < values.Count; i++)
            {
                var x = values[i];
                if (x == null) continue;
                if (x.key == null || x.key == "") continue;
                if (x.value == null) continue;
                itmNew.setProperty(x.key, x.value);
            }

            cfg.itmData = itmNew.apply();

            cfg.typeName = name;
            cfg.values = values;
            cfg.newId = cfg.itmData.getProperty("id", "");
            cfg.sourceId = cfg.itmData.getProperty("source_id", "");

            itmReturn.setProperty("id", cfg.newId);
        }

        private void UpdateOneColumn(TConfig cfg, Item itmReturn)
        {
            var sql = "UPDATE [" + cfg.itemType + "] SET [" + cfg.property + "] = N'" + cfg.value + "' WHERE [id] = '" + cfg.id + "'";
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            cfg.inn.applySQL(sql);
        }

        private class TCol
        {
            public string key { get; set; }
            public string value { get; set; }
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string itemType { get; set; }
            public string property { get; set; }
            public string id { get; set; }
            public string value { get; set; }
            public string scene { get; set; }

            public string typeName { get; set; }
            public string newId { get; set; }
            public string sourceId { get; set; }
            public List<TCol> values { get; set; }
            public Item itmData { get; set; }
            public Item itmEvent { get; set; }
        }
    }
}