﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.SportOpen.Meeting
{
    public class Fix_Meeting_Data : Item
    {
        public Fix_Meeting_Data(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 資料修正
                日誌: 
                    - 2024-03-18: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "Fix_Meeting_Data";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "payment-amount":
                    FixPaymentAmount(cfg, itmR);
                    break;
            }

            return itmR;
        }

        //修正全部訂單金額
        private void FixPaymentAmount(TConfig cfg, Item itmReturn)
        {
            var itmMeeting = cfg.inn.applySQL("SELECT id, in_course_fees FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (itmMeeting.isError() || itmMeeting.getResult() == "")
            {
                throw new Exception("請設定每隊報名費用");
            }

            var course_fees = GetIntVal(itmMeeting.getProperty("in_course_fees", "0"));
            if (course_fees <= 0)
            {
                throw new Exception("請設定每隊報名費用");
            }

            var sql_update = "UPDATE IN_MEETING_PAY SET IN_PAY_AMOUNT_EXP = " + course_fees + " WHERE in_meeting = '\" + cfg.meeting_id + \"'";
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql_update);
            cfg.inn.applySQL(sql_update);
        }

        private void CopyItemValue(Item itmA, Item itmB, string propA, string propB = "")
        {
            if (propB == "")
            {
                itmA.setProperty(propA, itmB.getProperty(propA, ""));
            }
            else
            {
                itmA.setProperty(propA, itmB.getProperty(propB, ""));
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string scene { get; set; }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }

    }
}
