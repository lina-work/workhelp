﻿using Aras.IOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.WebSockets;
using Xceed.Document.NET;

namespace WorkHelp.ArasDesk.Methods.SportOpen.教練
{
    public class In_Meeting_Report_Register_ACT : Item
    {
        public In_Meeting_Report_Register_ACT(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 下載報名清冊(ACT)
    日誌: 
        - 2024-10-01: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_Report_Register_ACT";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                in_paynumber = itmR.getProperty("in_paynumber", ""),
                user_token = itmR.getProperty("user_token", ""),
                token_id = itmR.getProperty("token_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            cfg.head_color = System.Drawing.Color.FromArgb(232, 232, 234);
            cfg.blue_color = System.Drawing.Color.FromArgb(192, 230, 245);
            cfg.warning_color = System.Drawing.Color.FromArgb(31, 73, 125);
            switch (cfg.scene)
            {
                case "admin-report":
                    cfg.is_admin_report = true;
                    ExportAdminReport(cfg, itmR);
                    break;
                case "export_org_register":
                    cfg.is_admin_report = false;
                    ExportOrgRegister(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void ExportAdminReport(TConfig cfg, Item itmReturn)
        {
            AppendMeetingInfo(cfg);
            AppendExportInfo(cfg);
            cfg.export_type = "";

            var items = GetMeetingRegisterItems(cfg);
            var count = items.getItemCount();
            if (count <= 0) throw new Exception("查無報名資料");

            var pack = GetPack(cfg, items);
            pack.org_name = "原創道館";
            pack.in_creator_email = "lwu001";
            pack.in_creator_tel = "02-25585561#11";

            var book = new Spire.Xls.Workbook();

            if (!pack.has_more_items)
            {
                AppendAdminSummarySheetA(cfg, pack, book);
            }
            else
            {
                AppendAdminSummarySheetB(cfg, pack, book);
            }

            AppendAdminRegisterSheetA(cfg, pack, book);
            if (pack.has_more_items)
            {
                AppendAdminMoreItemSummarySheet(cfg, pack, book);
                AppendAdminMoreItemDetailSheet(cfg, pack, book);
            }

            AppendExportResult(cfg, book, "報名清冊", itmReturn);
        }

        private void ExportOrgRegister(TConfig cfg, Item itmReturn)
        {
            AppendMeetingInfo(cfg);
            AppendExportInfo(cfg);

            var items = GetMeetingRegisterItems(cfg);
            var count = items.getItemCount();
            if (count <= 0) throw new Exception("查無報名資料");

            var pack = GetPack(cfg, items);
            var book = new Spire.Xls.Workbook();

            if (!pack.has_more_items)
            {
                AppendOrgSummarySheetA(cfg, pack, book);
            }
            else
            {
                AppendOrgSummarySheetB(cfg, pack, book);
            }

            for (var i = 0; i < pack.orgs.Count; i++)
            {
                var org = pack.orgs[i];
                AppendOrgRegisterSheet(cfg, pack, org, book);
                if (pack.has_more_items && org.prods.Count > 0)
                {
                    AppendOrgProductSummarySheet(cfg, pack, org, book);
                    AppendOrgProductDetailSheet(cfg, pack, org, book);
                }
            }
            AppendExportResult(cfg, book, "報名清冊", itmReturn);
        }

        private void AppendAdminMoreItemSummarySheet(TConfig cfg, TPack pack, Spire.Xls.Workbook book)
        {
            var totalAmountP = pack.orgs.Sum(x => x.amountP);

            var sheet = book.CreateEmptySheet();
            sheet.Name = "加價購_統計";

            ////將所有欄放入單一頁面
            //sheet.PageSetup.FitToPagesWide = 1;
            //sheet.PageSetup.FitToPagesTall = 0;

            sheet.PageSetup.TopMargin = 0.3;
            sheet.PageSetup.LeftMargin = 0.3;
            sheet.PageSetup.RightMargin = 0.3;
            sheet.PageSetup.BottomMargin = 0.6;

            SetStrCell(cfg, sheet, GetCell("B2", "加購金額總計"), 12, 30, true, false, HA.L);
            SetNumCell(cfg, sheet, GetNumCell("C2:E2", totalAmountP), 12, 0, true, true, HA.R);

            var ri = 3;
            SetStrCell(cfg, sheet, GetCell("B" + ri, "品名"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("C" + ri, "數量"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("D" + ri, "單價"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("E" + ri, "小計"), 12, 20, true, false, HA.C, CT.Warning);
            ri++;

            var prods = pack.prods.Values.ToList();
            for (var i = 0; i < prods.Count; i++)
            {
                var prod = prods[i];
                var quantity = prod.nodes.Sum(x => x.quantity);
                var amount = prod.nodes.Sum(x => x.amount);
                SetStrCell(cfg, sheet, GetCell("B" + ri, prod.name), 12, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, GetCell("C" + ri, quantity.ToString()), 12, 0, false, false, HA.R);
                SetNumCell(cfg, sheet, GetNumCell("D" + ri, prod.expense), 12, 0, false, false, HA.R);
                SetNumCell(cfg, sheet, GetNumCell("E" + ri, amount), 12, 0, false, false, HA.R);
                ri++;
            }

            SetRangeBorder(sheet, "B3:E" + (ri - 1));

            sheet.Columns[1].ColumnWidth = 30;
            sheet.Columns[2].ColumnWidth = 12;
            sheet.Columns[3].ColumnWidth = 12;
            sheet.Columns[4].ColumnWidth = 12;
        }

        private void AppendAdminMoreItemDetailSheet(TConfig cfg, TPack pack, Spire.Xls.Workbook book)
        {
            var totalAmountP = pack.orgs.Sum(x => x.amountP);

            var sheet = book.CreateEmptySheet();
            sheet.Name = "加價購_明細";

            ////將所有欄放入單一頁面
            //sheet.PageSetup.FitToPagesWide = 1;
            //sheet.PageSetup.FitToPagesTall = 0;

            sheet.PageSetup.TopMargin = 0.3;
            sheet.PageSetup.LeftMargin = 0.3;
            sheet.PageSetup.RightMargin = 0.3;
            sheet.PageSetup.BottomMargin = 0.6;

            SetStrCell(cfg, sheet, GetCell("B2", "所屬單位"), 12, 0, true, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("C2", pack.org_name), 12, 0, true, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("D2", "加購金額總計"), 12, 30, true, false, HA.L);
            SetNumCell(cfg, sheet, GetNumCell("E2:F2", totalAmountP), 12, 30, true, true, HA.R);

            var ri = 3;
            SetStrCell(cfg, sheet, GetCell("B" + ri, "所屬單位"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("C" + ri, "加購人員"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("D" + ri, "品名"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("E" + ri, "數量"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("F" + ri, "單價"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("G" + ri, "小計"), 12, 20, true, false, HA.C, CT.Warning);
            ri++;

            for (var i = 0; i < pack.orgs.Count; i++)
            {
                var org = pack.orgs[i];
                for (var j = 0; j < org.details.Count; j++)
                {
                    var detail = org.details[j];
                    SetStrCell(cfg, sheet, GetCell("B" + ri, org.key), 12, 0, false, false, HA.L);
                    SetStrCell(cfg, sheet, GetCell("C" + ri, detail.names), 12, 0, false, false, HA.L);
                    SetStrCell(cfg, sheet, GetCell("D" + ri, detail.goods), 12, 0, false, false, HA.L);
                    SetStrCell(cfg, sheet, GetCell("E" + ri, detail.quantity.ToString()), 12, 0, false, false, HA.R);
                    SetNumCell(cfg, sheet, GetNumCell("F" + ri, detail.expense), 12, 0, false, false, HA.R);
                    SetNumCell(cfg, sheet, GetNumCell("G" + ri, detail.amount), 12, 0, false, false, HA.R);
                    ri++;
                }
            }

            SetRangeBorder(sheet, "B3:G" + (ri - 1));

            sheet.Columns[1].ColumnWidth = 18;
            sheet.Columns[2].ColumnWidth = 16;
            sheet.Columns[3].ColumnWidth = 30;
            sheet.Columns[4].ColumnWidth = 12;
            sheet.Columns[5].ColumnWidth = 12;
            sheet.Columns[6].ColumnWidth = 12;
        }

        private void AppendAdminRegisterSheetA(TConfig cfg, TPack pack, Spire.Xls.Workbook book)
        {
            var totalAmount = pack.orgs.Sum(x => x.amountR);

            var sheet = book.CreateEmptySheet();
            sheet.Name = "報名-明細";

            ////將所有欄放入單一頁面
            //sheet.PageSetup.FitToPagesWide = 1;
            //sheet.PageSetup.FitToPagesTall = 0;

            sheet.PageSetup.TopMargin = 0.3;
            sheet.PageSetup.LeftMargin = 0.3;
            sheet.PageSetup.RightMargin = 0.3;
            sheet.PageSetup.BottomMargin = 0.6;

            SetStrCell(cfg, sheet, GetCell("B2", "所屬單位"), 12, 0, true, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("C2", pack.org_name), 12, 0, true, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("F2", "報名金額總計"), 12, 0, true, false, HA.L);
            SetNumCell(cfg, sheet, GetNumCell("G2:H2", totalAmount), 12, 0, true, true, HA.R);

            var ri = 3;
            SetStrCell(cfg, sheet, GetCell("B" + ri, "身分證字號"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("C" + ri, "姓名"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("D" + ri, "性別"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("E" + ri, "西元生日"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("F" + ri, "所屬單位"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("G" + ri, "隊別"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("H" + ri, "競賽項目"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("I" + ri, "序號"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("J" + ri, "競賽組別"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("K" + ri, "競賽分級"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("L" + ri, "金額"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("M" + ri, "協助報名者"), 12, 20, true, false, HA.C, CT.Warning);
            SetPhoneCell(cfg, sheet, GetCell("N" + ri, "連絡電話"), 12, 20, true, false, HA.C, CT.Warning);
            ri++;

            for (var i = 0; i < pack.orgs.Count; i++)
            {
                var org = pack.orgs[i];
                org.ri = ri;
                AppendAdminSummarySheetA(cfg, pack, org, org.leaders.Values.ToList(), sheet);
                AppendAdminSummarySheetA(cfg, pack, org, org.managers.Values.ToList(), sheet);
                AppendAdminSummarySheetA(cfg, pack, org, org.coaches.Values.ToList(), sheet);
                AppendAdminSummarySheetA(cfg, pack, org, org.players.Values.ToList(), sheet);
                ri = org.ri;
            }

            SetRangeBorder(sheet, "B3:N" + (ri - 1));

            sheet.Columns[1].ColumnWidth = 14;
            sheet.Columns[2].ColumnWidth = 11;
            sheet.Columns[3].ColumnWidth = 7;
            sheet.Columns[4].ColumnWidth = 11;
            sheet.Columns[5].ColumnWidth = 14;
            sheet.Columns[6].ColumnWidth = 6;
            sheet.Columns[7].ColumnWidth = 20;
            sheet.Columns[8].ColumnWidth = 8;
            sheet.Columns[9].ColumnWidth = 17;
            sheet.Columns[10].ColumnWidth = 10;
            sheet.Columns[11].ColumnWidth = 9;
            sheet.Columns[12].ColumnWidth = 10;
            sheet.Columns[13].ColumnWidth = 12;
        }

        private void AppendAdminSummarySheetA(TConfig cfg, TPack pack, TOrg org, List<TRow> rows, Spire.Xls.Worksheet sheet)
        {
            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                var item = row.value;

                SetStrCell(cfg, sheet, GetCell("B" + org.ri, row.in_sno), 12, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, GetCell("C" + org.ri, row.in_name), 12, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, GetCell("D" + org.ri, row.in_gender), 12, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, GetCell("E" + org.ri, row.reg_birth.Replace("/", "-")), 12, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, GetCell("F" + org.ri, row.in_current_org), 12, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, GetCell("G" + org.ri, row.in_team), 12, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, GetCell("H" + org.ri, row.in_l1), 12, 0, false, false, HA.L);
                SetTxtCell(cfg, sheet, GetCell("I" + org.ri, row.in_index), 12, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, GetCell("J" + org.ri, row.in_l2), 12, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, GetCell("K" + org.ri, row.in_l3), 12, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, GetCell("L" + org.ri, row.in_expense), 12, 0, false, false, HA.R);
                SetStrCell(cfg, sheet, GetCell("M" + org.ri, row.resume_name), 12, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, GetCell("N" + org.ri, row.resume_tel), 12, 0, false, false, HA.L);
                org.ri++;
            }
        }

        private void AppendAdminSummarySheetA(TConfig cfg, TPack pack, Spire.Xls.Workbook book)
        {
            var totalAmount = pack.orgs.Sum(x => x.amountR);

            var sheet = book.CreateEmptySheet();
            sheet.Name = "總表";

            //將所有欄放入單一頁面
            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 0;

            sheet.PageSetup.TopMargin = 0.3;
            sheet.PageSetup.LeftMargin = 0.3;
            sheet.PageSetup.RightMargin = 0.3;
            sheet.PageSetup.BottomMargin = 0.6;

            var titleCell = GetCell("B2:I2", cfg.mt_title);
            SetStrCell(cfg, sheet, titleCell, 24, 30, true, true, HA.C);

            //var printCell = GetCell("G3:I3");
            //printCell.v = "列印日期: " + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
            //SetStrCell(cfg, sheet, printCell, 12, 24, true, true, HA.R);

            SetStrCell(cfg, sheet, GetCell("B4", "報名資訊"), 12, 0, true, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("B5", "報名單位"), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("B6", "報名人員"), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("B7", "連絡電話"), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("B8", "報名費用"), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("B9", "聯絡地址"), 12, 0, false, false, HA.L);

            SetStrCell(cfg, sheet, GetCell("C5", pack.org_name), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("C6", pack.in_creator_email), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("C7", pack.in_creator_tel), 12, 0, false, false, HA.L);
            SetNumCell(cfg, sheet, GetNumCell("C8", totalAmount), 12, 0, false, false, HA.R);
            SetStrCell(cfg, sheet, GetCell("C9", "臺北市大同區承德路一段48號9樓之2"), 12, 0, false, false, HA.L);

            SetStrCell(cfg, sheet, GetCell("G4", "繳費資訊"), 12, 0, true, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("G5", "銀行"), 12, 0, false, false, HA.L, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("G6", "分行"), 12, 0, false, false, HA.L, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("G7", "戶名"), 12, 0, false, false, HA.L, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("G8", "帳號"), 12, 0, false, false, HA.L, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("G9", "匯款人資訊"), 12, 0, false, false, HA.L, CT.Warning);

            SetStrCell(cfg, sheet, GetCell("H5", cfg.in_bank_1), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("H6", cfg.in_bank_2), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("H7", cfg.in_bank_name), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("H8", cfg.in_bank_account), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("H9", cfg.in_bank_note), 12, 0, false, false, HA.L);

            SetStrCell(cfg, sheet, GetCell("B11", "報名明細"), 12, 24, true, false, HA.L);

            SetStrCell(cfg, sheet, GetCell("B12", "所屬單位"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("C12", " "), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("D12", "隊職員數"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("E12", "選手人數"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("F12", "參賽項目總計"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("G12", "報名費用"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("H12", "協助報名者"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("I12", "連絡電話"), 12, 20, true, false, HA.C, CT.Warning);

            var ri = 13;
            for (var i = 0; i < pack.orgs.Count; i++)
            {
                var org = pack.orgs[i];
                var staffCount = org.staffs.Count.ToString();
                var playerCount = org.players.Count.ToString();
                var teamsCount = org.playerTeams.Count.ToString();

                SetStrCell(cfg, sheet, GetCell("B" + ri, org.key), 12, 20, false, true, HA.L);
                SetStrCell(cfg, sheet, GetCell("D" + ri, staffCount), 12, 20, false, true, HA.R);
                SetStrCell(cfg, sheet, GetCell("E" + ri, playerCount), 12, 20, false, true, HA.R);
                SetStrCell(cfg, sheet, GetCell("F" + ri, teamsCount), 12, 20, false, true, HA.R);
                SetNumCell(cfg, sheet, GetNumCell("G" + ri, org.amountR), 12, 20, false, true, HA.R);
                SetStrCell(cfg, sheet, GetCell("H" + ri, org.resume_name), 12, 20, false, true, HA.C);
                SetPhoneCell(cfg, sheet, GetCell("I" + ri, org.resume_tel), 12, 20, false, true, HA.C);
                ri++;
            }

            SetRangeBorder(sheet, "B12:I" + (ri - 1));

            sheet.Columns[1].ColumnWidth = 18;
            sheet.Columns[2].ColumnWidth = 8.5;
            sheet.Columns[3].ColumnWidth = 9;
            sheet.Columns[4].ColumnWidth = 9;
            sheet.Columns[5].ColumnWidth = 15;
            sheet.Columns[6].ColumnWidth = 11;
            sheet.Columns[7].ColumnWidth = 18;
            sheet.Columns[8].ColumnWidth = 16;
        }

        private void AppendAdminSummarySheetB(TConfig cfg, TPack pack, Spire.Xls.Workbook book)
        {
            var totalAmountR = pack.orgs.Sum(x => x.amountR);
            var totalAmountP = pack.orgs.Sum(x => x.amountP);

            var sheet = book.CreateEmptySheet();
            sheet.Name = "報名總表";

            //將所有欄放入單一頁面
            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 0;

            sheet.PageSetup.TopMargin = 0.3;
            sheet.PageSetup.LeftMargin = 0.3;
            sheet.PageSetup.RightMargin = 0.3;
            sheet.PageSetup.BottomMargin = 0.6;

            var titleCell = GetCell("B2:J2", cfg.mt_title);
            SetStrCell(cfg, sheet, titleCell, 24, 30, true, true, HA.C);

            //var printCell = GetCell("G3:J3");
            //printCell.v = "列印日期: " + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
            //SetStrCell(cfg, sheet, printCell, 12, 24, true, true, HA.R);

            SetStrCell(cfg, sheet, GetCell("B4", "報名資訊"), 12, 0, true, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("B5", "報名單位"), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("B6", "報名人員"), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("B7", "連絡電話"), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("B8", "報名費用"), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("B9", "加購費用"), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("B10", "聯絡地址"), 12, 0, false, false, HA.L);

            SetStrCell(cfg, sheet, GetCell("C5", pack.org_name), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("C6", pack.in_creator_email), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("C7", pack.in_creator_tel), 12, 0, false, false, HA.L);
            SetNumCell(cfg, sheet, GetNumCell("C8", totalAmountR), 12, 0, false, false, HA.R);
            SetNumCell(cfg, sheet, GetNumCell("C9", totalAmountP), 12, 0, false, false, HA.R);
            SetStrCell(cfg, sheet, GetCell("C10", "臺北市大同區承德路一段48號9樓之2"), 12, 0, false, false, HA.L);

            SetStrCell(cfg, sheet, GetCell("G4", "繳費資訊"), 12, 0, true, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("G5", "銀行"), 12, 0, false, false, HA.L, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("G6", "分行"), 12, 0, false, false, HA.L, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("G7", "戶名"), 12, 0, false, false, HA.L, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("G8", "帳號"), 12, 0, false, false, HA.L, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("G9", "匯款人資訊"), 12, 0, false, false, HA.L, CT.Warning);

            SetStrCell(cfg, sheet, GetCell("H5", cfg.in_bank_1), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("H6", cfg.in_bank_2), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("H7", cfg.in_bank_name), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("H8", cfg.in_bank_account), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("H9", cfg.in_bank_note), 12, 0, false, false, HA.L);

            SetStrCell(cfg, sheet, GetCell("B12", "報名明細"), 12, 24, true, false, HA.L);

            SetStrCell(cfg, sheet, GetCell("B13", "所屬單位"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("C13", " "), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("D13", "隊職員數"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("E13", "選手人數"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("F13", "參賽項目總計"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("G13", "報名費用"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("H13", "加購費用"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("I13", "協助報名者"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("J13", "連絡電話"), 12, 20, true, false, HA.C, CT.Warning);

            var ri = 14;
            for (var i = 0; i < pack.orgs.Count; i++)
            {
                var org = pack.orgs[i];
                var staffCount = org.staffs.Count.ToString();
                var playerCount = org.players.Count.ToString();
                var teamsCount = org.playerTeams.Count.ToString();

                SetStrCell(cfg, sheet, GetCell("B" + ri, org.key), 12, 20, false, true, HA.L);
                SetStrCell(cfg, sheet, GetCell("D" + ri, staffCount), 12, 20, false, true, HA.R);
                SetStrCell(cfg, sheet, GetCell("E" + ri, playerCount), 12, 20, false, true, HA.R);
                SetStrCell(cfg, sheet, GetCell("F" + ri, teamsCount), 12, 20, false, true, HA.R);
                SetNumCell(cfg, sheet, GetNumCell("G" + ri, org.amountR), 12, 20, false, true, HA.R);
                SetNumCell(cfg, sheet, GetNumCell("H" + ri, org.amountP), 12, 20, false, true, HA.R);
                SetStrCell(cfg, sheet, GetCell("I" + ri, org.resume_name), 12, 20, false, true, HA.C);
                SetPhoneCell(cfg, sheet, GetCell("J" + ri, org.resume_tel), 12, 20, false, true, HA.C);
                ri++;
            }

            SetRangeBorder(sheet, "B13:J" + (ri - 1));

            sheet.Columns[1].ColumnWidth = 18;
            sheet.Columns[2].ColumnWidth = 8.5;
            sheet.Columns[3].ColumnWidth = 9;
            sheet.Columns[4].ColumnWidth = 9;
            sheet.Columns[5].ColumnWidth = 15;
            sheet.Columns[6].ColumnWidth = 11;
            sheet.Columns[7].ColumnWidth = 11;
            sheet.Columns[8].ColumnWidth = 18;
            sheet.Columns[9].ColumnWidth = 16;
        }

        private void AppendOrgSummarySheetA(TConfig cfg, TPack pack, Spire.Xls.Workbook book)
        {
            var totalAmount = pack.orgs.Sum(x => x.amountR).ToString("$ ###,##0");

            var sheet = book.CreateEmptySheet();
            sheet.Name = "報名總表";

            //將所有欄放入單一頁面
            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 0;

            sheet.PageSetup.TopMargin = 0.3;
            sheet.PageSetup.LeftMargin = 0.3;
            sheet.PageSetup.RightMargin = 0.3;
            sheet.PageSetup.BottomMargin = 0.6;

            var titleCell = GetCell("A1:K1", cfg.mt_title);
            SetStrCell(cfg, sheet, titleCell, 24, 30, true, true, HA.C);

            var printCell = GetCell("H2:K2");
            printCell.v = "列印日期: " + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
            SetStrCell(cfg, sheet, printCell, 12, 30, true, true, HA.R);

            SetStrCell(cfg, sheet, GetCell("A3", "聯絡資訊"), 12, 0, true, false, HA.L);

            var lineValCell = GetCell("A4", "線上服務窗口Line id: @647yxbyg(請加上@)");
            SetStrCell(cfg, sheet, lineValCell, 12, 0, false, false, HA.L);

            SetStrCell(cfg, sheet, GetCell("A6", "報名總表"), 12, 0, true, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("A7", "報名單位: " + pack.org_name), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("A8", "報名人員: " + pack.in_creator_email), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("A9", "連絡電話: " + pack.in_creator_tel), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("A10", "報名費用: " + totalAmount), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("A11", "聯絡地址: "), 12, 0, false, false, HA.L);

            SetStrCell(cfg, sheet, GetCell("F6", "繳費資訊"), 12, 0, true, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("F7", "銀行: " + cfg.in_bank_1), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("F8", "分行: " + cfg.in_bank_2), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("F9", "戶名: " + cfg.in_bank_name), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("F10", "帳號: " + cfg.in_bank_account), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("F11", "匯款人資訊: " + cfg.in_bank_note), 12, 0, false, false, HA.L);

            SetStrCell(cfg, sheet, GetCell("A13", "報名清單"), 16, 30, true, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("A14:B14", "所屬單位"), 12, 20, true, true, HA.C, CT.Head);
            SetStrCell(cfg, sheet, GetCell("C14:D14", "隊職員數"), 12, 20, true, true, HA.C, CT.Head);
            SetStrCell(cfg, sheet, GetCell("E14:F14", "選手人數"), 12, 20, true, true, HA.C, CT.Head);
            SetStrCell(cfg, sheet, GetCell("G14:H14", "參賽項目總計"), 12, 20, true, true, HA.C, CT.Head);
            SetStrCell(cfg, sheet, GetCell("I14:K14", "報名費用"), 12, 20, true, true, HA.C, CT.Head);

            var ri = 15;
            for (var i = 0; i < pack.orgs.Count; i++)
            {
                var org = pack.orgs[i];
                var staffCount = org.staffs.Count.ToString();
                var playerCount = org.players.Count.ToString();
                var teamsCount = org.playerTeams.Count.ToString();

                SetStrCell(cfg, sheet, GetCell("A" + ri + ":B" + ri, org.key), 12, 20, false, true, HA.L);
                SetStrCell(cfg, sheet, GetCell("C" + ri + ":D" + ri, staffCount), 12, 20, false, true, HA.R);
                SetStrCell(cfg, sheet, GetCell("E" + ri + ":F" + ri, playerCount), 12, 20, false, true, HA.R);
                SetStrCell(cfg, sheet, GetCell("G" + ri + ":H" + ri, teamsCount), 12, 20, false, true, HA.R);
                SetNumCell(cfg, sheet, GetNumCell("I" + ri + ":K" + ri, org.amountR), 12, 20, false, true, HA.R);
                ri++;
            }

            SetRangeBorder(sheet, "A14:K" + (ri - 1));
        }

        private void AppendOrgSummarySheetB(TConfig cfg, TPack pack, Spire.Xls.Workbook book)
        {
            var totalAmountR = pack.orgs.Sum(x => x.amountR).ToString("$ ###,##0");
            var totalAmountP = pack.orgs.Sum(x => x.amountP).ToString("$ ###,##0");

            var sheet = book.CreateEmptySheet();
            sheet.Name = "報名總表";

            //將所有欄放入單一頁面
            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 0;

            sheet.PageSetup.TopMargin = 0.3;
            sheet.PageSetup.LeftMargin = 0.3;
            sheet.PageSetup.RightMargin = 0.3;
            sheet.PageSetup.BottomMargin = 0.6;

            var titleCell = GetCell("A1:L1", cfg.mt_title);
            SetStrCell(cfg, sheet, titleCell, 24, 30, true, true, HA.C);

            var printCell = GetCell("H2:L2");
            printCell.v = "列印日期: " + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
            SetStrCell(cfg, sheet, printCell, 12, 30, true, true, HA.R);

            SetStrCell(cfg, sheet, GetCell("A3", "聯絡資訊"), 12, 0, true, false, HA.L);

            var lineValCell = GetCell("A4", "線上服務窗口Line id: @647yxbyg(請加上@)");
            SetStrCell(cfg, sheet, lineValCell, 12, 0, false, false, HA.L);

            SetStrCell(cfg, sheet, GetCell("A7", "報名總表"), 12, 0, true, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("A8", "報名單位: " + pack.org_name), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("A9", "報名人員: " + pack.in_creator_email), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("A10", "連絡電話: " + pack.in_creator_tel), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("A11", "報名費用: " + totalAmountR), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("A12", "加購費用: " + totalAmountP), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("A13", "聯絡地址: "), 12, 0, false, false, HA.L);

            SetStrCell(cfg, sheet, GetCell("G7", "繳費資訊"), 12, 0, true, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("G8", "銀行: " + cfg.in_bank_1), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("G9", "分行: " + cfg.in_bank_2), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("G10", "戶名: " + cfg.in_bank_name), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("G11", "帳號: " + cfg.in_bank_account), 12, 0, false, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("G12", "匯款人資訊: " + cfg.in_bank_note), 12, 0, false, false, HA.L);

            SetStrCell(cfg, sheet, GetCell("A14", "報名清單"), 16, 30, true, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("A15:B15", "所屬單位"), 12, 20, true, true, HA.C, CT.Head);
            SetStrCell(cfg, sheet, GetCell("C15:D15", "隊職員數"), 12, 20, true, true, HA.C, CT.Head);
            SetStrCell(cfg, sheet, GetCell("E15:F15", "選手人數"), 12, 20, true, true, HA.C, CT.Head);
            SetStrCell(cfg, sheet, GetCell("G15:H15", "參賽項目總計"), 12, 20, true, true, HA.C, CT.Head);
            SetStrCell(cfg, sheet, GetCell("I15:J15", "報名費用"), 12, 20, true, true, HA.C, CT.Head);
            SetStrCell(cfg, sheet, GetCell("K15:L15", "加購費用"), 12, 20, true, true, HA.C, CT.Head);

            var ri = 16;
            for (var i = 0; i < pack.orgs.Count; i++)
            {
                var org = pack.orgs[i];
                var staffCount = org.staffs.Count.ToString();
                var playerCount = org.players.Count.ToString();
                var teamsCount = org.playerTeams.Count.ToString();

                SetStrCell(cfg, sheet, GetCell("A" + ri + ":B" + ri, org.key), 12, 20, false, true, HA.L);
                SetStrCell(cfg, sheet, GetCell("C" + ri + ":D" + ri, staffCount), 12, 20, false, true, HA.R);
                SetStrCell(cfg, sheet, GetCell("E" + ri + ":F" + ri, playerCount), 12, 20, false, true, HA.R);
                SetStrCell(cfg, sheet, GetCell("G" + ri + ":H" + ri, teamsCount), 12, 20, false, true, HA.R);
                SetNumCell(cfg, sheet, GetNumCell("I" + ri + ":J" + ri, org.amountR), 12, 20, false, true, HA.R);
                SetNumCell(cfg, sheet, GetNumCell("K" + ri + ":L" + ri, org.amountP), 12, 20, false, true, HA.R);
                ri++;
            }

            SetRangeBorder(sheet, "A15:L" + (ri - 1));

            sheet.Columns[0].ColumnWidth = 7.5;
            sheet.Columns[1].ColumnWidth = 7.5;
            sheet.Columns[2].ColumnWidth = 7.5;
            sheet.Columns[3].ColumnWidth = 7.5;
            sheet.Columns[4].ColumnWidth = 7.5;
            sheet.Columns[5].ColumnWidth = 7.5;
            sheet.Columns[6].ColumnWidth = 7.5;
            sheet.Columns[7].ColumnWidth = 7.5;
            sheet.Columns[8].ColumnWidth = 7.5;
            sheet.Columns[9].ColumnWidth = 7.5;
            sheet.Columns[10].ColumnWidth = 7.5;
            sheet.Columns[11].ColumnWidth = 7.5;
        }

        private void AppendOrgRegisterSheet(TConfig cfg, TPack pack, TOrg org, Spire.Xls.Workbook book)
        {
            var orgAmount = "報名費總計:" + org.amountR.ToString("$ ###,##0");

            var sheet = book.CreateEmptySheet();
            sheet.Name = org.key;

            //將所有欄放入單一頁面
            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 0;

            sheet.PageSetup.TopMargin = 0.3;
            sheet.PageSetup.LeftMargin = 0.3;
            sheet.PageSetup.RightMargin = 0.3;
            sheet.PageSetup.BottomMargin = 0.6;

            var titleCell = GetCell("A1:E1", cfg.mt_title);
            SetStrCell(cfg, sheet, titleCell, 24, 30, true, true, HA.C);

            SetStrCell(cfg, sheet, GetCell("A3", org.key), 14, 30, true, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("B3", orgAmount), 12, 30, false, false, HA.L);

            SetStrCell(cfg, sheet, GetCell("A4", "姓名"), 12, 24, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, GetCell("B4", "所屬單位"), 12, 24, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, GetCell("C4", "競賽項目"), 12, 24, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, GetCell("D4", "競賽組別"), 12, 24, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, GetCell("E4", "競賽分級"), 12, 24, true, false, HA.C, CT.Head);

            var ri = 5;
            org.ri = ri;

            AppendOrgPlayerRow(cfg, org, org.leaders.Values.ToList(), sheet);
            AppendOrgPlayerRow(cfg, org, org.managers.Values.ToList(), sheet);
            AppendOrgPlayerRow(cfg, org, org.coaches.Values.ToList(), sheet);
            AppendOrgPlayerRow(cfg, org, org.players.Values.ToList(), sheet);

            ri = org.ri;
            SetStrCell(cfg, sheet, GetCell("A" + (ri + 1), "總教練簽名: "), 12, 30, false, false, HA.L);

            sheet.Columns[0].ColumnWidth = 18;
            sheet.Columns[1].ColumnWidth = 20;
            sheet.Columns[2].ColumnWidth = 14;
            sheet.Columns[3].ColumnWidth = 27;
            sheet.Columns[4].ColumnWidth = 25;

            SetRangeBorder(sheet, "A4:E" + (ri - 1));
        }

        private void AppendOrgProductSummarySheet(TConfig cfg, TPack pack, TOrg org, Spire.Xls.Workbook book)
        {
            var orgAmount = "加價購總計:" + org.amountP.ToString("$ ###,##0");

            var sheet = book.CreateEmptySheet();
            sheet.Name = org.key + "_加購項目_統計";

            //將所有欄放入單一頁面
            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 0;

            sheet.PageSetup.TopMargin = 0.3;
            sheet.PageSetup.LeftMargin = 0.3;
            sheet.PageSetup.RightMargin = 0.3;
            sheet.PageSetup.BottomMargin = 0.6;

            var titleCell = GetCell("A1:D1", cfg.mt_title);
            SetStrCell(cfg, sheet, titleCell, 24, 30, true, true, HA.C);

            SetStrCell(cfg, sheet, GetCell("A3", org.key), 14, 30, true, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("D3", orgAmount), 12, 30, false, false, HA.R);

            SetStrCell(cfg, sheet, GetCell("A4", "加購項目"), 12, 24, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, GetCell("B4", "數量"), 12, 24, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, GetCell("C4", "單價"), 12, 24, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, GetCell("D4", "小計"), 12, 24, true, false, HA.C, CT.Head);

            var ri = 5;
            var prods = org.prods.Values.ToList();
            for (var i = 0; i < prods.Count; i++)
            {
                var prod = prods[i];
                var quantity = prod.nodes.Sum(x => x.quantity);
                var amount = prod.nodes.Sum(x => x.amount);

                SetStrCell(cfg, sheet, GetCell("A" + ri, prod.name), 12, 24, false, false, HA.C);
                SetStrCell(cfg, sheet, GetCell("B" + ri, quantity.ToString()), 12, 24, false, false, HA.C);
                SetStrCell(cfg, sheet, GetCell("C" + ri, prod.expense.ToString()), 12, 24, false, false, HA.C);
                SetNumCell(cfg, sheet, GetNumCell("D" + ri, amount), 12, 24, false, false, HA.C);
                ri++;
            }

            sheet.Columns[0].ColumnWidth = 40;
            sheet.Columns[1].ColumnWidth = 17;
            sheet.Columns[2].ColumnWidth = 17;
            sheet.Columns[3].ColumnWidth = 17;

            SetRangeBorder(sheet, "A4:D" + (ri - 1));
        }

        private void AppendOrgProductDetailSheet(TConfig cfg, TPack pack, TOrg org, Spire.Xls.Workbook book)
        {
            var orgAmount = "加價購總計:" + org.amountP.ToString("$ ###,##0");

            var sheet = book.CreateEmptySheet();
            sheet.Name = org.key + "_加購項目_明細";

            //將所有欄放入單一頁面
            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 0;

            sheet.PageSetup.TopMargin = 0.3;
            sheet.PageSetup.LeftMargin = 0.3;
            sheet.PageSetup.RightMargin = 0.3;
            sheet.PageSetup.BottomMargin = 0.6;

            var titleCell = GetCell("A1:E1", cfg.mt_title);
            SetStrCell(cfg, sheet, titleCell, 24, 30, true, true, HA.C);

            SetStrCell(cfg, sheet, GetCell("A3", org.key), 14, 30, true, false, HA.L);
            SetStrCell(cfg, sheet, GetCell("D3", orgAmount), 12, 30, false, false, HA.R);

            SetStrCell(cfg, sheet, GetCell("A4", "加購人員"), 12, 20, true, false, HA.C, CT.Warning);
            SetStrCell(cfg, sheet, GetCell("B4", "加購項目"), 12, 24, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, GetCell("C4", "數量"), 12, 24, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, GetCell("D4", "單價"), 12, 24, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, GetCell("E4", "小計"), 12, 24, true, false, HA.C, CT.Head);

            var ri = 5;
            for (var i = 0; i < org.details.Count; i++)
            {
                var detail = org.details[i];
                SetStrCell(cfg, sheet, GetCell("A" + ri, detail.names), 12, 24, false, false, HA.C);
                SetStrCell(cfg, sheet, GetCell("B" + ri, detail.goods), 12, 24, false, false, HA.C);
                SetStrCell(cfg, sheet, GetCell("C" + ri, detail.quantity.ToString()), 12, 24, false, false, HA.C);
                SetStrCell(cfg, sheet, GetCell("D" + ri, detail.expense.ToString()), 12, 24, false, false, HA.C);
                SetNumCell(cfg, sheet, GetNumCell("E" + ri, detail.amount), 12, 24, false, false, HA.C);
                ri++;
            }

            sheet.Columns[0].ColumnWidth = 15;
            sheet.Columns[1].ColumnWidth = 30;
            sheet.Columns[2].ColumnWidth = 15;
            sheet.Columns[3].ColumnWidth = 15;
            sheet.Columns[4].ColumnWidth = 15;

            SetRangeBorder(sheet, "A4:E" + (ri - 1));
        }

        private void AppendOrgPlayerRow(TConfig cfg, TOrg org, List<TRow> rows, Spire.Xls.Worksheet sheet)
        {
            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                SetStrCell(cfg, sheet, GetCell("A" + org.ri, row.in_name), 12, 20, false, false, HA.L);
                SetStrCell(cfg, sheet, GetCell("B" + org.ri, row.in_current_org), 12, 20, false, false, HA.L);
                SetStrCell(cfg, sheet, GetCell("C" + org.ri, row.in_l1), 12, 20, false, false, HA.L);
                SetStrCell(cfg, sheet, GetCell("D" + org.ri, row.in_l2), 12, 20, false, false, HA.L);
                SetStrCell(cfg, sheet, GetCell("E" + org.ri, row.in_l3), 12, 20, false, false, HA.L);
                org.ri++;
            }
        }

        private TKV GetCell(string k, string v = "")
        {
            return new TKV { k = k, v = v };
        }

        private TKV GetNumCell(string k, int d = 0)
        {
            return new TKV { k = k, d = d };
        }

        private void SetStrCell(TConfig cfg, Spire.Xls.Worksheet sheet
            , TKV o
            , int size
            , int height = 0
            , bool needBold = false
            , bool needMerge = false
            , HA ha = HA.C
            , CT color = CT.None)
        {
            SetStrCell(cfg, sheet, o.k, o.v, size, height, needBold, needMerge, ha, color);
        }

        private void SetNumCell(TConfig cfg, Spire.Xls.Worksheet sheet
            , TKV o
            , int size
            , int height = 0
            , bool needBold = false
            , bool needMerge = false
            , HA ha = HA.C
            , CT color = CT.None)
        {
            var range = sheet.Range[o.k];
            if (needMerge) range.Merge();

            ResetRangeStyle(cfg, range, size, height, needBold, ha, color);

            range.NumberValue = o.d;
            range.NumberFormat = "$ #,##0";
        }

        private void SetPhoneCell(TConfig cfg, Spire.Xls.Worksheet sheet
            , TKV o
            , int size
            , int height = 0
            , bool needBold = false
            , bool needMerge = false
            , HA ha = HA.C
            , CT color = CT.None)
        {
            var range = sheet.Range[o.k];
            if (needMerge) range.Merge();

            ResetRangeStyle(cfg, range, size, height, needBold, ha, color);

            var phone = MapPhone(o.v);
            if (phone.is_phone_number)
            {
                range.NumberValue = phone.code;
                range.Style.NumberFormat = "0000-000-000";
                range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            }
            else
            {
                range.Value = "'" + phone.source;
                range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            }
        }

        private void SetTxtCell(TConfig cfg, Spire.Xls.Worksheet sheet
            , TKV o
            , int size
            , int height = 0
            , bool needBold = false
            , bool needMerge = false
            , HA ha = HA.C
            , CT color = CT.None)
        {
            var range = sheet.Range[o.k];
            if (needMerge) range.Merge();

            ResetRangeStyle(cfg, range, size, height, needBold, ha, color);

            range.Text = "'" + o.v;
        }

        private void SetStrCell(TConfig cfg, Spire.Xls.Worksheet sheet
            , string pos
            , string text
            , int size
            , int height = 0
            , bool needBold = false
            , bool needMerge = false
            , HA ha = HA.C
            , CT color = CT.None)
        {
            var range = sheet.Range[pos];
            if (needMerge) range.Merge();

            ResetRangeStyle(cfg, range, size, height, needBold, ha, color);

            range.Text = text;
        }

        private void ResetRangeStyle(TConfig cfg
            , Spire.Xls.CellRange range
            , int size
            , int height
            , bool needBold
            , HA ha
            , CT color)
        {
            range.Style.Font.FontName = cfg.font_name;
            range.Style.Font.Size = size;

            if (needBold) range.Style.Font.IsBold = true;
            if (height > 0) range.RowHeight = height;

            switch (color)
            {
                case CT.Head:
                    range.Style.Color = cfg.head_color;
                    break;
                case CT.Blue:
                    range.Style.Color = cfg.blue_color;
                    break;
                case CT.Warning:
                    range.Style.Color = cfg.warning_color;
                    range.Style.Font.Color = System.Drawing.Color.White;
                    break;
            }

            range.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;

            switch (ha)
            {
                case HA.C:
                    range.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case HA.L:
                    range.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;
                    break;
                case HA.R:
                    range.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                    break;
            }
        }

        private TTEL MapPhone(string source)
        {
            var result = new TTEL { source = source, is_phone_number = false };

            if (source != "")
            {
                string value = source.Replace("+886", "0")
                    .Replace("-", "")
                    .Replace(" ", "")
                    .Replace(".", "");

                result.value = value;
                result.is_phone_number = value.Length == 10 && value.StartsWith("09");

                if (result.is_phone_number)
                {
                    result.code = GetInt(value);
                }
            }

            return result;
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        private enum HA
        {
            L = 100,
            C = 200,
            R = 300,
        }

        private enum CT
        {
            None = 1,
            Head = 100,
            Blue = 200,
            Warning = 300,
        }

        private class TKV
        {
            public string k { get; set; }
            public string v { get; set; }
            public double d { get; set; }
        }

        private class TTEL
        {
            public string source { get; set; }
            public string value { get; set; }
            public bool is_phone_number { get; set; }
            public int code { get; set; }
        }

        private void AppendExportResult(TConfig cfg, Spire.Xls.Workbook book, string book_title, Item itmReturn)
        {
            if (book.Worksheets.Count > 1) book.Worksheets[0].Remove();
            if (book.Worksheets.Count > 1) book.Worksheets[0].Remove();
            if (book.Worksheets.Count > 1) book.Worksheets[0].Remove();

            //匯出檔名
            var xlsName = cfg.mt_title
                + "_" + book_title
                + "_" + System.Guid.NewGuid().ToString().Substring(0, 4).ToUpper()
                + "_" + System.DateTime.Now.ToString("yyyyMMddHHmmss");

            xlsName = xlsName.Replace("+", "")
                            .Replace("-", "")
                            .Replace(".", "")
                            .Replace(" ", "");

            string extName = cfg.export_type != "" && cfg.export_type.ToLower() == "pdf" ? ".pdf" : ".xlsx";
            string xls_file = cfg.export_path + xlsName + extName;
            string xls_name = xlsName + extName;

            if (!System.IO.Directory.Exists(cfg.export_path))
            {
                System.IO.Directory.CreateDirectory(cfg.export_path);
            }
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, cfg.export_path);
            //儲存檔案          
            if (extName == ".pdf")
            {
                book.SaveToFile(xls_file, Spire.Xls.FileFormat.PDF);
            }
            else
            {
                book.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);
            }

            //下載路徑
            itmReturn.setProperty("xls_name", xls_name);
        }

        //賽事資訊
        private void AppendMeetingInfo(TConfig cfg)
        {
            if (cfg.meeting_id == "") return;

            cfg.itmMeeting = cfg.inn.applySQL("SELECT * FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("查無 賽事資料");
            }
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.in_bank_1 = cfg.itmMeeting.getProperty("in_bank_1", "");
            cfg.in_bank_2 = cfg.itmMeeting.getProperty("in_bank_2", "");
            cfg.in_bank_account = cfg.itmMeeting.getProperty("in_bank_account", "");
            cfg.in_bank_name = cfg.itmMeeting.getProperty("in_bank_name", "");
            cfg.in_bank_note = cfg.itmMeeting.getProperty("in_bank_note", "");
        }

        private void AppendExportInfo(TConfig cfg)
        {
            cfg.itmXlsx = cfg.inn.applyMethod("In_Variable_File", "<export_name>meeting_export_path</export_name><in_name></in_name>");
            cfg.template_path = cfg.itmXlsx.getProperty("template_path", "");
            cfg.export_path = cfg.itmXlsx.getProperty("export_path", "").TrimEnd('\\') + @"\" + cfg.meeting_id + @"\";
            cfg.export_type = "pdf";
            //cfg.export_type = "";
            cfg.font_name = "Source Sans Pro";
            cfg.font_name = "新細明體";
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string in_paynumber { get; set; }
            public string user_token { get; set; }
            public string token_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }
            public string in_bank_1 { get; set; }
            public string in_bank_2 { get; set; }
            public string in_bank_account { get; set; }
            public string in_bank_name { get; set; }
            public string in_bank_note { get; set; }
            public string in_address { get; set; }

            public Item itmXlsx { get; set; }
            public string template_path { get; set; }
            public string export_path { get; set; }
            public string export_type { get; set; }
            public string font_name { get; set; }

            public System.Drawing.Color head_color { get; set; }
            public System.Drawing.Color blue_color { get; set; }
            public System.Drawing.Color warning_color { get; set; }

            public bool is_admin_report { get; set; }
        }

        private class TRow
        {
            public string key { get; set; }

            public string in_paynumber { get; set; }
            public string in_creator { get; set; }
            public string in_creator_sno { get; set; }
            public string in_creator_email { get; set; }
            public string in_creator_tel { get; set; }

            public string id { get; set; }
            public string in_name { get; set; }
            public string in_gender { get; set; }
            public string in_sno { get; set; }
            public string reg_birth { get; set; }
            public string in_sno_display { get; set; }

            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_index { get; set; }
            public string in_current_org { get; set; }
            public string in_team { get; set; }
            public string resume_name { get; set; }
            public string resume_tel { get; set; }

            public string in_expense { get; set; }

            public bool is_staff { get; set; }
            public bool is_manager { get; set; }
            public bool is_leader { get; set; }
            public bool is_coach { get; set; }
            public bool is_player { get; set; }
            public bool is_male { get; set; }
            public bool is_female { get; set; }

            public string staff_key { get; set; }
            public string player_key { get; set; }
            public string sect_key { get; set; }
            public string team_key { get; set; }

            public int amount { get; set; }

            public Item value { get; set; }
        }

        private class TPack
        {
            public string org_name { get; set; }
            public string in_paynumber { get; set; }
            public string in_creator { get; set; }
            public string in_creator_sno { get; set; }
            public string in_creator_email { get; set; }
            public string in_creator_tel { get; set; }
            public bool has_more_items { get; set; }
            public List<TOrg> orgs { get; set; }
            public Dictionary<string, TProd> prods { get; set; }
        }

        private class TOrg
        {
            public string key { get; set; }
            public Dictionary<string, TRow> staffs { get; set; }
            public Dictionary<string, TRow> managers { get; set; }
            public Dictionary<string, TRow> leaders { get; set; }
            public Dictionary<string, TRow> coaches { get; set; }
            public Dictionary<string, TRow> players { get; set; }
            public List<TTeam> teams { get; set; }
            public List<TTeam> playerTeams { get; set; }
            public int ri { get; set; }

            public int amountR { get; set; }
            public int amountP { get; set; }

            public string resume_name { get; set; }
            public string resume_tel { get; set; }

            public Dictionary<string, TProd> prods { get; set; }

            public List<TTeamProd> details { get; set; }
        }

        private class TTeamProd
        {
            public string key { get; set; }
            public string names { get; set; }
            public string goods { get; set; }
            public int expense { get; set; }
            public int quantity { get; set; }
            public int amount { get; set; }
        }

        private class TTeam
        {
            public string key { get; set; }
            public string names { get; set; }
            public int amount { get; set; }
            public List<TRow> persons { get; set; }
            public bool is_player { get; set; }
        }

        private class TSome
        {
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_index { get; set; }
            public string in_current_org { get; set; }
            public int quantity { get; set; }
            public int amount { get; set; }
            public string in_note { get; set; }
            public string team_key { get; set; }
        }

        private class TProd
        {
            public string key { get; set; }
            public string name { get; set; }
            public int expense { get; set; }
            public List<TSome> nodes { get; set; }
        }

        private TPack GetPack(TConfig cfg, Item items)
        {
            var first = MapRow(cfg, items.getItemByIndex(0));
            var pack = MapPack(cfg, first);

            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var row = MapRow(cfg, item);
                var org = pack.orgs.Find(x => x.key == row.in_current_org);
                if (org == null)
                {
                    org = MapOrg(cfg, row);
                    pack.orgs.Add(org);
                }

                if (row.is_staff)
                {
                    AppendRow(cfg, org.staffs, row, row.staff_key);
                }

                if (row.is_coach)
                {
                    AppendRow(cfg, org.coaches, row, row.staff_key);
                }
                else if (row.is_manager)
                {
                    AppendRow(cfg, org.managers, row, row.staff_key);
                }
                else if (row.is_leader)
                {
                    AppendRow(cfg, org.leaders, row, row.staff_key);
                }
                else
                {
                    AppendRow(cfg, org.players, row, row.player_key);
                }

                var team = org.teams.Find(x => x.key == row.team_key);
                if (team == null)
                {
                    team = MapTeam(cfg, row);
                    org.teams.Add(team);
                    org.amountR += team.amount;
                }
                team.persons.Add(row);
            }

            //是否設定加價購項目
            pack.has_more_items = HasMoreItems(cfg);

            MergeProducts(cfg, pack);

            ResetOrgRows(cfg, pack);

            return pack;
        }

        private void ResetOrgRows(TConfig cfg, TPack pack)
        {
            for (var i = 0; i < pack.orgs.Count; i++)
            {
                var org = pack.orgs[i];
                for (var j = 0; j < org.teams.Count; j++)
                {
                    var team = org.teams[j];
                    if (team.is_player)
                    {
                        org.playerTeams.Add(team);
                    }

                    if (team.persons == null || team.persons.Count == 0)
                    {
                        team.names = "";
                    }
                    else
                    {
                        var names = team.persons.Select(x => x.in_name);
                        team.names = string.Join(",", names);
                    }
                }

                foreach (var kv in org.prods)
                {
                    var prod = kv.Value;
                    org.amountP += prod.nodes.Sum(x => x.amount);

                    foreach(var node in prod.nodes)
                    {
                        var team = org.teams.Find(x => x.key == node.team_key);
                        if (team == null) continue;
                        AppendTeamProd(cfg, org, team, prod, node);
                    }

                    org.details = org.details.OrderBy(x => x.names)
                        .ThenBy(x => x.goods)
                        .ToList();
                }
            }
        }

        private void AppendTeamProd(TConfig cfg, TOrg org, TTeam team, TProd prod, TSome node)
        {
            if (org.details == null) org.details = new List<TTeamProd>();
            var key = team.names + prod.name;
            var existed = org.details.Find(x => x.key == key);
            if (existed == null)
            {
                existed = new TTeamProd 
                {
                    key = key,
                    names = team.names,
                    goods = prod.name,
                    expense = prod.expense,
                    quantity = 0,
                    amount = 0,
                };
                org.details.Add(existed);
            }

            existed.quantity += node.quantity;
            existed.amount += node.amount;
        }

        private bool HasMoreItems(TConfig cfg)
        {
            var sql = "SELECT TOP 1 id FROM IN_MEETING_ITEM WITH(NOLOCK) WHERE source_id = '" + cfg.meeting_id + "'";
            var item = cfg.inn.applySQL(sql);
            if (item.isError() || item.getResult() == "") return false;
            var id = item.getProperty("id", "");
            return id != "";
        }

        private void MergeProducts(TConfig cfg, TPack pack)
        {
            pack.prods = new Dictionary<string, TProd>();

            var items = GetMeetingUserItems(cfg);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var some = NewSome(cfg, item);

                var prod_key = item.getProperty("item_id", "");
                var org_key = item.getProperty("in_current_org", "");

                MergeProdMap(cfg, pack.prods, prod_key, some, item);

                var org = pack.orgs.Find(x => x.key == org_key);
                if (org == null) continue;
                if (org.prods == null) org.prods = new Dictionary<string, TProd>();
                MergeProdMap(cfg, org.prods, prod_key, some, item);
            }
        }

        private void MergeProdMap(TConfig cfg, Dictionary<string, TProd> map, string key, TSome some, Item item)
        {
            if (!map.ContainsKey(key))
            {
                map.Add(key, NewProd(cfg, item));
            }

            var prod = map[key];
            prod.nodes.Add(some);
        }

        private TSome NewSome(TConfig cfg, Item item)
        {
            var x = new TSome
            {
                in_l1 = item.getProperty("in_l1", ""),
                in_l2 = item.getProperty("in_l2", ""),
                in_l3 = item.getProperty("in_l3", ""),
                in_index = item.getProperty("in_index", ""),
                in_current_org = item.getProperty("in_current_org", ""),
                quantity = GetInt(item.getProperty("in_quantity", "0")),
                amount = GetInt(item.getProperty("in_amount", "0")),
                in_note = item.getProperty("in_note", ""),
            };
            x.team_key = x.in_l1 + x.in_l2 + x.in_l3 + x.in_index;
            return x;
        }

        private TProd NewProd(TConfig cfg, Item item)
        {
            var x = new TProd
            {
                key = item.getProperty("related_id", ""),
                name = item.getProperty("item_name", ""),
                expense = GetInt(item.getProperty("item_expense", "0")),
                nodes = new List<TSome>(),
            };
            return x;
        }

        private void AppendRow(TConfig cfg, Dictionary<string, TRow> map, TRow row, string key)
        {
            if (map.ContainsKey(key)) return;
            map.Add(key, row);
        }

        private TPack MapPack(TConfig cfg, TRow row)
        {
            var x = new TPack
            {
                org_name = "",
                in_paynumber = row.in_paynumber,
                in_creator = row.in_creator,
                in_creator_sno = row.in_creator_sno,
                in_creator_email = row.in_creator_email,
                in_creator_tel = row.in_creator_tel,
                orgs = new List<TOrg>(),
            };
            return x;
        }

        private TOrg MapOrg(TConfig cfg, TRow row)
        {
            var x = new TOrg
            {
                key = row.in_current_org,
                staffs = new Dictionary<string, TRow>(),
                managers = new Dictionary<string, TRow>(),
                leaders = new Dictionary<string, TRow>(),
                coaches = new Dictionary<string, TRow>(),
                players = new Dictionary<string, TRow>(),
                teams = new List<TTeam>(),
                playerTeams = new List<TTeam>(),
                prods = new Dictionary<string, TProd>(),
                amountR = 0,
                amountP = 0,
                resume_name = row.resume_name,
                resume_tel = row.resume_tel,
            };
            return x;
        }

        private TTeam MapTeam(TConfig cfg, TRow row)
        {
            var x = new TTeam
            {
                key = row.team_key,
                persons = new List<TRow>(),
                amount = row.amount,
                is_player = row.is_player,
            };
            return x;
        }

        private TRow MapRow(TConfig cfg, Item item)
        {
            var x = new TRow
            {
                id = item.getProperty("id", ""),
                in_paynumber = item.getProperty("in_paynumber", ""),
                in_creator = item.getProperty("in_creator", ""),
                in_creator_sno = item.getProperty("in_creator_sno", ""),
                in_creator_email = item.getProperty("in_creator_email", ""),
                in_creator_tel = item.getProperty("in_creator_tel", ""),
                in_name = item.getProperty("in_name", ""),
                in_gender = item.getProperty("in_gender", ""),
                reg_birth = item.getProperty("reg_birth", ""),
                in_sno = item.getProperty("in_sno", ""),
                in_sno_display = item.getProperty("in_sno_display", ""),
                in_l1 = item.getProperty("in_l1", ""),
                in_l2 = item.getProperty("in_l2", ""),
                in_l3 = item.getProperty("in_l3", ""),
                in_index = item.getProperty("in_index", ""),
                in_current_org = item.getProperty("in_current_org", ""),
                in_team = item.getProperty("in_team", ""),
                in_expense = item.getProperty("in_expense", ""),
                resume_name = item.getProperty("resume_name", ""),
                resume_tel = item.getProperty("resume_tel", ""),
            };

            if (x.in_creator == "")
            {
                x.in_creator = x.in_creator_email;
            }

            x.player_key = x.in_sno.ToUpper();
            x.staff_key = x.in_name.ToUpper() + x.player_key;
            x.sect_key = x.in_l1 + x.in_l2 + x.in_l3;
            x.team_key = x.in_l1 + x.in_l2 + x.in_l3 + x.in_index;

            if (x.in_l1 == "隊職員")
            {
                x.is_staff = true;
                if (x.in_l2.Contains("教練"))
                {
                    x.is_coach = true;
                }
                else if (x.in_l2.Contains("管理"))
                {
                    x.is_manager = true;
                }
                else if (x.in_l2.Contains("領隊"))
                {
                    x.is_leader = true;
                }
            }
            else
            {
                x.is_player = true;
            }

            if (x.in_gender == "男") x.is_male = true;
            else if (x.in_gender == "女") x.is_female = true;

            x.amount = GetInt(x.in_expense);

            if (x.resume_name == "")
            {
                x.resume_name = x.in_creator;
                x.resume_tel = x.in_creator_tel;
            }
            else if (x.resume_name == "lwu001")
            {
                x.resume_name = x.in_creator;
                x.resume_tel = x.in_creator_tel;
            }

            x.value = item;

            return x;
        }

        private Item GetMeetingRegisterItems(TConfig cfg)
        {
            var condition = cfg.is_admin_report
                ? ""
                : "AND in_paynumber = '" + cfg.in_paynumber + "'";

            var sql = @"
                SELECT 
	                *
                FROM 
	                VU_MEETING_REGISTER_SECTIONS WITH(NOLOCK)
                WHERE 
	                source_id = '{#meeting_id}'
	                {#condition}
                ORDER BY
	                in_current_org
	                , l1_sort
	                , l2_sort
	                , l3_sort
	                , in_index
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#condition}", condition);

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingUserItems(TConfig cfg)
        {
            var condition = cfg.is_admin_report
                ? ""
                : "AND t1.in_paynumber = '" + cfg.in_paynumber + "'";

            var sql = @"
					SELECT
						t1.id
						, t1.in_l1
						, t1.in_l2
						, t1.in_l3
						, t1.in_index
						, t1.in_current_org
						, t1.in_paynumber
						, t1.in_quantity
						, t1.in_amount
						, t1.in_note
						, t2.id				AS 'item_id'
						, t2.in_name		AS 'item_name'
						, t2.in_expense		AS 'item_expense'
						, t2.in_type		AS 'item_type'
						, t2.in_url			AS 'item_url'
						, t2.in_file		AS 'item_file'
						, t2.sort_order		AS 'item_sort'
					FROM
						IN_MEETING_USER_ITEM t1 WITH(NOLOCK)
					INNER JOIN
						IN_MEETING_ITEM t2 WITH(NOLOCK)
						ON t2.id = t1.related_id
					WHERE
						t1.source_id = '{#meeting_id}'
                        {#condition}
					ORDER BY
						t1.in_current_org
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#condition}", condition);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private DateTime GetDtm(string value)
        {
            if (value == "") return DateTime.MinValue;
            DateTime result = DateTime.MinValue;
            if (DateTime.TryParse(value, out result)) return result;
            return DateTime.MinValue;
        }

        private int GetInt(string value, int defV = 0)
        {
            if (value == "") return 0;
            int result = defV;
            if (int.TryParse(value, out result)) return result;
            return defV;
        }
    }
}