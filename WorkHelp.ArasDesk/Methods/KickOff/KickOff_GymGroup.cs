﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.KickOff.Common
{
    public class KickOff_GymGroup : Item
    {
        public KickOff_GymGroup(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 創建團體會員資料
                日誌: 
                    - 2022-12-23: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "KickOff_GymGroup";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),
            };

            //創建團體會員系統帳號
            CreateVipGymAccount(cfg, itmR);

            return itmR;
        }

        private void CreateVipGymAccount(TConfig cfg, Item itmReturn)
        {
            //取得協會 Resume
            Item itmAResume = GetAdminResume(cfg);

            //取得團體會員資料
            Item itmNewGyms = GetNewGyms(cfg);

            //單位註冊帳號: 學校社團申請帳號
            string meeting_id = "38CAB90DF1274E048520A801948AC65C";

            int count = itmNewGyms.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmNewGym = itmNewGyms.getItemByIndex(i);
                NewRegister(cfg, itmAResume, itmNewGym, meeting_id);
            }
        }

        /// <summary>
        /// 新建與會者 (註冊模式)
        /// </summary>
        private void NewRegister(TConfig cfg, Item itmParent, Item itmEntity, string meeting_id)
        {
            string in_sno = itmEntity.getProperty("in_sno", "");

            string survey_type = "1";
            string method = "register_meeting";

            List<Item> lstSurveys = GetMeetingSurveyList(cfg, meeting_id);
            string parameters = GetParamValues(lstSurveys, itmEntity);
            string isUserId = itmParent.getProperty("in_user_id", "");
            string isIndId = itmParent.getProperty("owned_by_id", "");

            string aml = ""
                + "<surveytype>" + survey_type + "</surveytype>"
                + "<meeting_id>" + meeting_id + "</meeting_id>"
                + "<agent_id>" + "</agent_id>"
                + "<email>" + in_sno + "</email>"
                + "<isUserId>" + isUserId + "</isUserId>"
                + "<isIndId>" + isIndId + "</isIndId>"
                + "<method>" + method + "</method>"
                + "<parameter>" + parameters + "</parameter>"
                + "";

            //CCO.Utilities.WriteDebug(strMethodName, "aml: " + aml);

            Item itmMethod = cfg.inn.applyMethod("In_meeting_register", aml);

            if (itmMethod.isError())
            {
                throw new Exception("創建帳號發生錯誤");
            }
        }

        /// <summary>
        /// 組成 Url 參數
        /// </summary>
        /// <param name="list">問項清單</param>
        /// <param name="itmEntity">與會者資料</param>
        private string GetParamValues(List<Item> list, Item itmEntity)
        {
            string result = "";
            int count = list.Count;
            for (int i = 0; i < count; i++)
            {
                Item item = list[i];
                string id = item.getProperty("id", "");
                string property = item.getProperty("in_property", "");

                string value = itmEntity.getProperty(property, "");

                if (value == "")
                {
                    continue;
                }

                if (property == "in_birth")
                {
                    value = GetDateTimeValue(value, "yyyy-MM-dd");
                }

                if (result != "")
                {
                    result += "&amp;";
                }

                result += id + "=" + value;
            }
            return result;
        }

        /// <summary>
        /// 取得問項清單
        /// </summary>
        private List<Item> GetMeetingSurveyList(TConfig cfg, string meeting_id)
        {
            string sql = @"
                SELECT
                    t3.id,
                    t3.in_property
                FROM
                    IN_MEETING t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_SURVEYS t2 WITH(NOLOCK) ON t2.source_id = t1.id
                INNER JOIN
                    IN_SURVEY t3 WITH(NOLOCK) ON t3.id = t2.related_id
                WHERE
                    t1.id = '{#meeting_id}'
                    AND ISNULL(in_property, '') <> ''
                ORDER BY
                    t2.sort_order
            ";
            sql = sql.Replace("{#meeting_id}", meeting_id);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                throw new Exception("創建帳號發生錯誤(問項)");
            }

            List<Item> list = new List<Item>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                list.Add(items.getItemByIndex(i));
            }

            return list;
        }

        /// <summary>
        /// 取得協會 Resume
        /// </summary>
        private Item GetAdminResume(TConfig cfg)
        {
            string sql = "SELECT TOP 1 * FROM IN_RESUME WITH(NOLOCK) WHERE in_sno = N'M001'";
            return cfg.inn.applySQL(sql);
        }

        private Item GetNewGyms(TConfig cfg)
        {
            string sql = "SELECT * FROM AAA_Vip_Gym WITH(NOLOCK) ORDER BY no";
            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }
        }

        private string ClearName(string value)
        {
            return value.Replace("-", "_")
                .Replace(" ", "_");
        }

        private string ClearEmail(string value)
        {
            return value.Replace(":", "_")
                .Replace("/", "_");
        }

        private int GetIntValue(string value)
        {
            int result = 0;
            if (Int32.TryParse(value, out result))
            {
                return result;
            }
            else
            {
                return 0;
            }
        }

        private string GetDateTimeValue(string value, string format = "yyyy-MM-dd", int hours = 0)
        {
            if (value == "") return "";

            string day = value.Replace("/", "-");

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(day, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }
        }
}
