﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.KickOff.Common
{
    public class KickOff_GymMember : Item
    {
        public KickOff_GymMember(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 團體會員成員資料整合
                日誌: 
                    - 2022-05-31: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "KickOff_GymMember";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            string sql = "SELECT TOP 1 * FROM AAA_Gym_Member WITH(NOLOCK) WHERE ISNULL(in_sync, '') = '' ORDER BY in_no";

            Item items = inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_no = item.getProperty("in_no", "");
                string gym_resume_id = item.getProperty("gym_resume_id", "");
                string in_title = item.getProperty("in_title", "");
                
                item.setProperty("group_id", gym_resume_id);
                item.setProperty("in_birth", "1990/01/01");
                item.setProperty("in_resume_role", "gym_2100");
                item.setProperty("in_resume_remark", in_title);

                item.apply("In_ResumeRole_Add");

                string sql_upd = "UPDATE AAA_Gym_Member SET in_sync = 'finished' WHERE in_no = '" + in_no + "'";
                inn.applySQL(sql_upd);

            }


            return itmR;
        }


    }
}
