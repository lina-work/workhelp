﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class KickOff_ResetPwd : Item
    {
        public KickOff_ResetPwd(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 
                    站台初始化-重設密碼
                日誌: 
                    - 2022-04-26: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]KickOff_ResetPwd";

            Item itmR = this;
            CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            string sql = "SELECT * FROM IN_RESUME WITH(NOLOCK)"
                + " WHERE in_member_type IN ('area_cmt', 'vip_mbr')"
                + " ORDER BY in_member_type, in_sno";

            Item items = inn.applySQL(sql);
            int count = items.getItemCount();

            CCO.Utilities.WriteDebug(strMethodName, "[重設密碼]共 " + count + " 筆");

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string resume_id = item.getProperty("id", "");
                string in_name = item.getProperty("in_name", "");
                string in_sno = item.getProperty("in_sno", "");
                string in_member_type = item.getProperty("in_member_type", "");
                string in_user_id = item.getProperty("in_user_id", "");
                string newPwd = "";

                CCO.Utilities.WriteDebug(strMethodName, "[重設密碼]" + in_name + " (" + in_sno + ")");

                switch (in_member_type)
                {
                    case "area_cmt": //縣市委員會
                    case "vip_gym":  //道館社團
                        newPwd = "1234";
                        break;

                    case "vip_mbr":
                        if (in_sno.Length > 4)
                        {
                            newPwd = in_sno.Substring(in_sno.Length - 4, 4);
                        }
                        else
                        {
                            newPwd = "1234";
                        }
                        break;
                }

                string newPwd_md5 = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(newPwd, "md5");

                string sql_update1 = "UPDATE [User] SET password = N'" + newPwd_md5 + "' WHERE id = '" + in_user_id + "'";
                inn.applySQL(sql_update1);

                string sql_update2 = "UPDATE [In_Resume] SET password = N'" + newPwd_md5 + "', in_password_plain = N'" + newPwd + "' WHERE id = '" + resume_id + "'";
                inn.applySQL(sql_update2);
            }

            CCO.Utilities.WriteDebug(strMethodName, "[重設密碼]結束");

            return itmR;
        }
    }
}