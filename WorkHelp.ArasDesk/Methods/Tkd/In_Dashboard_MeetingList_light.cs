﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Tkd.Common
{
    public class In_Dashboard_MeetingList_light : Item
    {
        public In_Dashboard_MeetingList_light(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的:列出所有進行中的賽事
                說明:
                1.計算離報名截止日還有多少天(2019/9/4)
                --> 取得報名截止日(in_state_time)並計算還有幾天結束報名(現在時間-報名截止日)
            */
            
            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Dashboard_MeetingList_light";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string aml = "";
            string sql = "";
            string strError = "";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                search_type = itmR.getProperty("search_type", "").Trim(),
            };

            int Offset_start = -31;
            int Offset_end = 130;
            DateTime CurrentTime = System.DateTime.Today;
            itmR.setProperty("Inn_StartDate", CurrentTime.AddDays(Offset_start).ToString("yyyy/MM/dd"));
            itmR.setProperty("Inn_EndDate", CurrentTime.AddDays(Offset_end).ToString("yyyy/MM/dd"));
            string BeforeDay = CurrentTime.AddDays(Offset_start).ToString("yyyy-MM-ddT00:00:00");
            string AfterDay = DateTime.Now.ToString("yyyy-MM-ddT00:00:00");

            string StartDate = this.getProperty("startDate", BeforeDay);
            string EndDate = this.getProperty("endDate", AfterDay);
            string SystemMeeting = "5F73936711E04DC799CB02587F4FF7E0";

            //計算各賽事狀態數量
            int status_none = 0;//尚未開始
            int status_full = 0;//額滿
            int status_start = 0;//報名中
            int status_stop = 0;//截止

            //這是賽事列表頁 依據[賽事時間]秀出 [賽事結束]不秀出
            aml = @"<AML>
	            <Item type='In_Meeting' action='get' orderBy='in_state_time_start desc'>
		            <state>start</state>
		            <in_is_template>0</in_is_template>
		            <in_is_main>0</in_is_main>
		            <id condition='not in'>#SystemMeeting</id>
		            <in_date_e condition='gt'>#EndDate</in_date_e>
	            </Item>
            </AML>";

            aml = aml.Replace("#StartDate", StartDate);
            aml = aml.Replace("#EndDate", EndDate);
            aml = aml.Replace("#SystemMeeting", SystemMeeting);

            Item Meetings = inn.applyAML(aml);
            //取得賽事名稱(杰)
            string meeting_game_name = inn.getItemByKeyedName("in_variable", "meeting_game_name").getProperty("in_value", "");

            for (int i = 0; i < Meetings.getItemCount(); i++)
            {
                Item Meeting = Meetings.getItemByIndex(i);
                string in_meeting_type = Meeting.getProperty("in_meeting_type", "");
                if (cfg.search_type != "" && in_meeting_type != cfg.search_type)
                {
                    continue;
                }

                Meeting.setType("Inn_SearchResult");
                string Inn_in_date_s = DateTime.Parse(Meeting.getProperty("in_date_s", "")).ToString("yyyy-MM-dd HH:mm");
                Meeting.setProperty("Inn_in_date_s", Inn_in_date_s);

                string Send = Meeting.getProperty("in_state_time_end", CurrentTime.ToString("yyyy-MM-dd"));//取得結束時間
                DateTime Timer_end = Convert.ToDateTime(Send);//將結束時間轉型

                TimeSpan ts = Timer_end - System.DateTime.Now;//結束-現在
                double days = ts.TotalDays;//取出計算結果(天數)
                double hours = ts.Hours;//取出計算結果(小時)
                double minutes = ts.Minutes;//取出計算結果(分)


                string in_date_s = Meeting.getProperty("in_state_time_start", CurrentTime.ToString("yyyy-MM-dd"));//開始時間
                string in_state_time = Meeting.getProperty("in_state_time_end", CurrentTime.ToString("yyyy-MM-dd"));//結束時間
                string in_state_end = "";
                string in_address = Meeting.getProperty("in_address", "");//地址

                int end = Int32.Parse(Convert.ToInt32(days + 1).ToString());
                //依據離結束報名的日期傳值
                if (end <= 0)
                {
                    in_state_end = "報名時間結束";
                }
                else
                {
                    in_state_end = Convert.ToInt32(days + 1).ToString() + "天結束報名";
                }

                Meeting.setProperty("Inn_meeting_time_s", in_date_s.Split('T')[0]);//開始時間
                Meeting.setProperty("Inn_meeting_time_e", in_state_time.Split('T')[0]);//結束時間
                Meeting.setProperty("Inn_meeting_address", in_address);//地址

                DateTime Meeting_Time_s = Convert.ToDateTime(in_date_s);//將開始時間轉型
                DateTime Meeting_Time_e = Convert.ToDateTime(in_state_time);//將結束時間轉型

                //賽事狀態

                //尚未開始
                if (System.DateTime.Now < Meeting_Time_s)//今日<開始
                {
                    Meeting.setProperty("Inn_meeting_status", "status_none");
                    Meeting.setProperty("Inn_meeting_remaining_time", "報名尚未開始");//剩餘時間
                    status_none++;
                }
                //額滿
                else if (Meeting.getProperty("in_isfull", "") == "1")//已額滿 == 1
                {
                    Meeting.setProperty("Inn_meeting_status", "status_full");
                    Meeting.setProperty("Inn_meeting_remaining_time", in_state_end);//剩餘時間
                    status_full++;
                }
                //報名中
                else if (System.DateTime.Now < Meeting_Time_e && System.DateTime.Now > Meeting_Time_s)//今日<結束&&今日>開始
                {
                    Meeting.setProperty("Inn_meeting_status", "status_start");
                    Meeting.setProperty("Inn_meeting_remaining_time", in_state_end);//剩餘時間
                    status_start++;
                }
                //報名截止
                else if (System.DateTime.Now > Meeting_Time_e)//今日>結束
                {
                    Meeting.setProperty("Inn_meeting_status", "status_stop");
                    Meeting.setProperty("Inn_meeting_remaining_time", "報名截止");//剩餘時間
                    status_stop++;
                }
                //(杰)
                Meeting.setProperty("meeting_game_name", meeting_game_name);
                itmR.addRelationship(Meeting);
            }
            itmR = itmR.apply("In_AppendExtraProperties");
            //將賽事總數量以及各賽事狀態數量丟給前台
            itmR.setProperty("meeting_status", "共有" + Meetings.getItemCount() + "項賽事" + "<br/>" + status_none + "項賽事尚未開始、" + status_full + "項賽事已額滿、" + status_start + "項賽事報名中、" + status_stop + "項賽事已截止");


            itmR.setProperty("meeting_game_name", meeting_game_name);

            sql = "SELECT TOP 1 * FROM [IN_SITE] WITH(NOLOCK)";
            Item itmSite = inn.applySQL(sql);

            if (!itmSite.isError())
            {
                itmR.setProperty("meeting_name_1", itmSite.getProperty("in_site_name", ""));
                itmR.setProperty("meeting_banner_slider_1", itmSite.getProperty("in_banner_slider_1", ""));
            }
            else
            {
                itmR.setProperty("meeting_name_1", "運動賽事報名系統");
                itmR.setProperty("meeting_banner_slider_1", "");
            }

            AppendMeetingList(cfg, itmR);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;

        }

        private void AppendMeetingList(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT TOP 20 
	                t1.id
	                , t1.in_title
	                , t1.item_number
	                , t1.in_real_taking
	                , t1.in_meeting_type
	                , t1.in_address
	                , CONVERT(VARCHAR, t1.in_date_s, 111) AS 'in_date_s'
	                , CONVERT(VARCHAR, t1.in_date_s, 111) AS 'in_date_e'
	                , t2.in_api_token
	                , t2.in_api_shorturl
	                , t2.in_api_qrcode
                FROM 
	                IN_MEETING t1 WITH(NOLOCK) 
                INNER JOIN
	                IN_MEETING_SHORTURL t2 WITH(NOLOCK) 
	                ON t2.in_meeting = t1.id
	                AND t2.in_value = 'function'
                WHERE 
	                ISNULL(in_is_main, 1) = 0
	                AND ISNULL(in_is_template, 1) = 0
                ORDER BY 
	                in_date_e DESC
	                , in_date_s DESC
            ";



            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string dts = item.getProperty("in_date_s", "");
                string dte = item.getProperty("in_date_e", "");

                item.setType("inn_fight");
                item.setProperty("inn_day", dts+"~"+ dte);
                itmReturn.addRelationship(item);
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string search_type { get; set; }
        }
    }
}