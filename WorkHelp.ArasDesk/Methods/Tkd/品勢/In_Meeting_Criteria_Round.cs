﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Tkd.Common
{
    public class In_Meeting_Criteria_Round : Item
    {
        public In_Meeting_Criteria_Round(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 品勢格式場次
                日誌: 
                    - 2022-11-19: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_Criteria_Round";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            var dtS = DateTime.Now;
            switch (cfg.scene)
            {
                case "score_page":
                    ScorePage(cfg, itmR);
                    break;

                case "round_page":
                    RoundPage(cfg, itmR);
                    break;

                case "sync":
                    Sync(cfg, itmR);
                    break;

                case "sync_rank":
                    SyncRank(cfg, itmR);
                    break;

                case "import":
                    Import(cfg, itmR);
                    break;
            }

            var dtE = DateTime.Now;

            cfg.CCO.Utilities.WriteDebug(strMethodName,
                cfg.scene
                + "\r\n" + dtS.ToString("yyyy-MM-dd HH:mm:ss.fff")
                + "\r\n" + dtE.ToString("yyyy-MM-dd HH:mm:ss.fff")
            );

            return itmR;
        }

        //匯入品勢順序表
        private void Import(TConfig cfg, Item itmReturn)
        {
            var itmMeeting = cfg.inn.applySQL("SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (itmMeeting.isError() || itmMeeting.getResult() == "")
            {
                throw new Exception("查無活動資訊");
            }

            var enumber = itmReturn.getProperty("enumber", "");
            var value = itmReturn.getProperty("value", "");
            if (value == "") throw new Exception("無匯入資料");

            var list = MapList(cfg, value);
            if (list == null || list.Count == 0) throw new Exception("無匯入資料");

            cfg.inn.applySQL("DELETE FROM In_Meeting_Criteria_Round WHERE in_meeting = '" + cfg.meeting_id + "'");
            foreach (var row in list)
            {
                Item itmNew = cfg.inn.newItem("In_Meeting_Criteria_Round", "add");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_number", enumber);
                itmNew.setProperty("in_round", row.c01);
                itmNew.setProperty("in_name", row.c02);
                itmNew.setProperty("in_count", row.c03);
                itmNew.setProperty("in_status", "");
                itmNew.apply();
            }
        }

        //頁面
        private void ScorePage(TConfig cfg, Item itmReturn)
        {
            //string event_id = itmReturn.getProperty("event_id", "");

            //cfg.itmMeeting = cfg.inn.applySQL("SELECT id, in_title, in_score_url FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            //if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            //{
            //    throw new Exception("未設定參數 site path");
            //}

            //itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));

            //var itmRound = cfg.inn.applySQL("SELECT * FROM In_Meeting_Criteria_Round WITH(NOLOCK) WHERE id = '" + event_id + "'");
            //if (itmRound.isError() || itmRound.getResult() == "")
            //{
            //    throw new Exception("查無場次資料");
            //}

            ////同步場次資料
            //cfg.enumber = itmRound.getProperty("in_number", "");
            //cfg.rnumber = itmRound.getProperty("in_round", "");
            //SyncRank(cfg, itmReturn);


            //itmReturn.setProperty("round_name", itmRound.getProperty("in_name", ""));

            //var items = cfg.inn.applySQL("SELECT * FROM In_Meeting_Criteria_Score WITH(NOLOCK) WHERE source_id = '" + event_id + "' ORDER BY in_rank, in_index");
            //if (items.isError() || items.getResult() == "")
            //{
            //    return;
            //}

            //StringBuilder head = new StringBuilder();
            //StringBuilder body = new StringBuilder();


            //head.Append("<thead>");
            //head.Append("<tr>");
            //head.Append("  <th class='mailbox-subject text-center'>籤號</th>");
            //head.Append("  <th class='mailbox-subject text-center'>單位</th>");
            //head.Append("  <th class='mailbox-subject text-center'>姓名</th>");
            //head.Append("  <th class='mailbox-subject text-center'>成績</th>");
            //head.Append("  <th class='mailbox-subject text-center'>名次</th>");
            //head.Append("</tr>");
            //head.Append("</thead>");


            //var count = items.getItemCount();
            //body.Append("<tbody>");
            //for (int i = 0; i < count; i++)
            //{
            //    Item item = items.getItemByIndex(i);
            //    body.Append("<tr>");
            //    body.Append("  <td class='text-center'> " + item.getProperty("in_sign_no", "") + " </td>");
            //    body.Append("  <td class='text-center'> " + item.getProperty("in_org", "") + " </td>");
            //    body.Append("  <td class='text-center'> " + item.getProperty("in_name", "") + " </td>");
            //    body.Append("  <td class='text-center'> " + item.getProperty("in_score", "") + " </td>");
            //    body.Append("  <td class='text-center'> " + item.getProperty("in_rank", "") + " </td>");
            //    body.Append("</tr>");
            //}
            //body.Append("</tbody>");

            //string table_name = "data_table";

            //StringBuilder builder = new StringBuilder();
            //builder.Append("<div class='box-body tablenobg' style='position: relative'>");
            //builder.AppendLine(GetTableAttribute(table_name));
            //builder.Append(head);
            //builder.Append(body);
            //builder.AppendLine("</table>");
            //builder.Append("</div>");
            //builder.AppendLine("<script>");
            //builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            //builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            //builder.AppendLine("</script>");

            //itmReturn.setProperty("inn_table", builder.ToString());
        }

        //頁面
        private void RoundPage(TConfig cfg, Item itmReturn)
        {
            cfg.itmMeeting = cfg.inn.applySQL("SELECT id, in_title, in_banner_photo, in_banner_photo2, in_score_url FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("未設定參數 site path");
            }

            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("banner_file", cfg.itmMeeting.getProperty("in_banner_photo", ""));
            itmReturn.setProperty("banner_file2", cfg.itmMeeting.getProperty("in_banner_photo2", ""));


            var page = MapPageData(cfg, itmReturn);


            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();


            head.Append("<thead>");
            head.Append("<tr>");
            foreach (var site in page.sites)
            {
                head.Append("  <th class='mailbox-subject text-center'>場次</th>");
                head.Append("  <th class='mailbox-subject text-center'>狀態</th>");
            }
            head.Append("</tr>");
            head.Append("</thead>");


            body.Append("<tbody>");
            for (int i = page.min_serial; i < page.max_serial; i++)
            {
                body.Append("<tr>");
                foreach (var site in page.sites)
                {
                    var evt = site.rounds.Find(x => x.serial == i);
                    if (evt == null)
                    {
                        body.Append("  <td class='text-center'> &nbsp; </td>");
                        body.Append("  <td class='text-center'> &nbsp; </td>");
                    }
                    else
                    {
                        body.Append("  <td class='text-center'> " + EventLink(cfg, evt.item) + " </td>");
                        body.Append("  <td class='text-center'> " + evt.item.getProperty("in_status", "") + " </td>");
                    }
                }
                body.Append("</tr>");
            }
            body.Append("</tbody>");

            string table_name = "data_table";

            StringBuilder builder = new StringBuilder();
            builder.Append("<div class='box-body tablenobg' style='position: relative'>");
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.Append("</div>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            itmReturn.setProperty("inn_table", builder.ToString());
        }

        private string EventLink(TConfig cfg, Item item)
        {
            string id = item.getProperty("id", "");
            string in_round = item.getProperty("in_round", "");
            string in_status = item.getProperty("in_status", "");

            if (in_status == "") return in_round;

            return "<a target='_self' href='javascript:void()' onclick='Players_Click(this)' data-id='" + id + "'>"
                + in_round
                + "<a/>";
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "'"
                + " class='table table-hover table-striped'"
                + " data-toggle='table'"
                + " data-show-toggle='false'"
                + " data-show-columns='false'"
                + " data-search-align='left'"
                + " data-sort-stable='true'"
                + " data-search='false'"
                + ">";
        }

        private TPage MapPageData(TConfig cfg, Item itmReturn)
        {
            var result = new TPage
            {
                min_serial = int.MaxValue,
                max_serial = int.MinValue,
                sites = new List<TSite>(),
            };

            string sql = @"
                SELECT 
	                id
	                , in_number
	                , in_round
	                , in_name
	                , in_status
                FROM 
	                In_Meeting_Criteria_Round WITH(NOLOCK)
                WHERE
	                in_meeting = '{#meeting_id}'
	                AND ISNULL(in_round, '') <> ''
                ORDER BY 
	                in_round
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_round = item.getProperty("in_round", "");
                if (in_round == "")
                {
                    continue;
                }

                var code = in_round[0].ToString();
                var site = result.sites.Find(x => x.code == code);
                if (site == null)
                {
                    site = new TSite
                    {
                        code = code,
                        name = "第" + code + "場地",
                        rounds = new List<TEvt>(),
                    };
                    result.sites.Add(site);
                }

                var evt = new TEvt
                {
                    code = GetIntVal(in_round),
                    in_number = in_round,
                    item = item,
                };

                evt.serial = evt.code % 100;
                if (evt.serial < result.min_serial)
                {
                    result.min_serial = evt.serial;
                }
                if (evt.serial > result.max_serial)
                {
                    result.max_serial = evt.serial;
                }

                site.rounds.Add(evt);
            }

            return result;
        }

        //同步場次
        private void Sync(TConfig cfg, Item itmReturn)
        {
            cfg.itmMeeting = cfg.inn.applySQL("SELECT id, in_title, in_score_url FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("未設定參數 site path");
            }

            Item itmSitePate = cfg.inn.applySQL("SELECT id, in_name, in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = 'site_path'");
            if (itmSitePate.isError() || itmSitePate.getResult() == "")
            {
                throw new Exception("未設定參數 site path");
            }

            string enumber = itmReturn.getProperty("enumber", "");
            cfg.score_url = cfg.itmMeeting.getProperty("in_score_url", "").Trim();
            cfg.score_url = "http://www.tondar-cn.com/PoomSae/PoomSae_Score.php?Event_Num=" + enumber;
            if (cfg.score_url == "")
            {
                throw new Exception("未設定品勢成績網址");
            }

            cfg.site_path = itmSitePate.getProperty("in_value", "").Trim('\\').Trim();
            cfg.folder_path = cfg.site_path + "\\tempvault\\meeting_criteria\\" + cfg.meeting_id;
            if (!System.IO.Directory.Exists(cfg.folder_path))
            {
                System.IO.Directory.CreateDirectory(cfg.folder_path);
            }

            WriteEvent(cfg, itmReturn);
        }

        //同步名次
        private void SyncRank(TConfig cfg, Item itmReturn)
        {
            if (cfg.itmMeeting == null)
            {
                cfg.itmMeeting = cfg.inn.applySQL("SELECT id, in_title, in_score_url FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
                if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
                {
                    throw new Exception("未設定參數 site path");
                }
            }

            Item itmSitePate = cfg.inn.applySQL("SELECT id, in_name, in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = 'site_path'");
            if (itmSitePate.isError() || itmSitePate.getResult() == "")
            {
                throw new Exception("未設定參數 site path");
            }

            // cfg.score_url = cfg.itmMeeting.getProperty("in_score_url", "").Trim();
            // if (cfg.score_url == "")
            // {
            //     throw new Exception("未設定品勢成績網址");
            // }

            cfg.site_path = itmSitePate.getProperty("in_value", "").Trim('\\').Trim();
            cfg.folder_path = cfg.site_path + "\\tempvault\\meeting_criteria\\" + cfg.meeting_id;
            if (!System.IO.Directory.Exists(cfg.folder_path))
            {
                System.IO.Directory.CreateDirectory(cfg.folder_path);
            }

            string sql = @"
                SELECT
	                * 
                FROM 
	                In_Meeting_Criteria_Round WITH(NOLOCK) 
                WHERE 
	                in_meeting = '{#meeting_id}' 
	                AND ISNULL(in_sync, '') <> 'closed'
                ORDER BY
                    in_round
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);

            var count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var exe_result = WriteScore(cfg, item);
            }
        }

        //成績建檔
        private Item WriteScore(TConfig cfg, Item itmRound)
        {
            var itmResult = cfg.inn.newItem();

            string enumber = itmRound.getProperty("in_number", "");
            string rnumber = itmRound.getProperty("in_round", "");
            if (enumber == "" || rnumber == "")
            {
                return itmResult;
            }

            var fullurl = "http://www.tondar-cn.com/PoomSae/Search_Score_Dtl.php?Event_Num=1119&RRound=" + rnumber;
            var fullfile = cfg.folder_path + "\\score_" + rnumber + ".txt";
            var fulljson = cfg.folder_path + "\\score_" + rnumber + ".json";

            //下載網頁
            DownloadWebPage(cfg, fullurl, fullfile);

            //轉換
            var data = GetScoreData(cfg, fullfile);

            if (string.IsNullOrWhiteSpace(data.rno))
            {
                throw new Exception("查無場次");
            }

            //轉換為 json
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(data);

            //存檔
            System.IO.File.WriteAllText(fulljson, json, System.Text.Encoding.UTF8);

            var round_id = itmRound.getProperty("id", "");

            if (data.is_closed)
            {
                string sql_upd = "UPDATE In_Meeting_Criteria_Round SET"
                    + " in_sync = 'closed'"
                    + " WHERE id = '" + round_id + "'";

                cfg.inn.applySQL(sql_upd);
            }

            //寫入資料庫
            for (var i = 0; i < data.list.Count; i++)
            {
                var row = data.list[i];
                Item itmNew = cfg.inn.newItem("In_Meeting_Criteria_Score", "merge");
                itmNew.setAttribute("where", "source_id = '" + round_id + "' AND in_sign_no = '" + row.sign_no + "'");
                itmNew.setProperty("source_id", round_id);
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_number", enumber);
                itmNew.setProperty("in_round", rnumber);
                itmNew.setProperty("in_index", (i + 1).ToString());
                itmNew.setProperty("in_sign_no", row.sign_no);
                itmNew.setProperty("in_org", row.org);
                itmNew.setProperty("in_name", row.name);
                itmNew.setProperty("in_score", row.score);
                itmNew.setProperty("in_rank", row.rank);
                itmNew.apply();
            }

            return itmResult;
        }

        //場次建檔
        private void WriteEvent(TConfig cfg, Item itmReturn)
        {
            var enumber = itmReturn.getProperty("enumber", "");

            var fullurl = cfg.score_url;
            var fullfile = cfg.folder_path + "\\event.txt";
            var fulljson = cfg.folder_path + "\\event.json";

            //下載網頁
            DownloadWebPage(cfg, fullurl, fullfile);
            //轉換
            var rows = GetRoundList(cfg, fullfile);
            //排序
            var sorted = rows.OrderBy(x => x.rno);
            //轉換為 json
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(sorted);
            //存檔
            System.IO.File.WriteAllText(fulljson, json, System.Text.Encoding.UTF8);

            //寫入資料庫
            foreach (var row in sorted)
            {
                Item itmNew = cfg.inn.newItem("In_Meeting_Criteria_Round", "merge");
                itmNew.setAttribute("where", "in_meeting = '" + cfg.meeting_id + "' AND in_round = '" + row.rno + "'");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_number", enumber);
                itmNew.setProperty("in_round", row.rno);
                //itmNew.setProperty("in_name", "");
                itmNew.setProperty("in_status", row.status);
                itmNew.apply();
            }
        }

        private TCriteriaModel GetScoreData(TConfig cfg, string fullfile)
        {
            var entity = new TCriteriaModel { list = new List<TCriteriaPlayer>(), is_closed = false };
            try
            {
                var contents = System.IO.File.ReadAllText(fullfile, System.Text.Encoding.UTF8);

                var xml_h1 = ".//h1";
                var xml_page = ".//table";
                var xml_flag = "//input[@id='lbl_Flg']";

                var doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(contents);

                var title_node = doc.DocumentNode.SelectSingleNode(xml_h1);
                var table_node = doc.DocumentNode.SelectSingleNode(xml_page);
                var flag_node = doc.DocumentNode.SelectSingleNode(xml_flag);

                if (table_node == null)
                {
                    return entity;
                }
                if (flag_node != null)
                {
                    var x = flag_node.GetAttributeValue("value", "");
                    if (!string.IsNullOrWhiteSpace(x) && x == "V")
                    {
                        entity.is_closed = true;
                    }
                }

                entity.title = GetInnerText(title_node);
                if (!string.IsNullOrWhiteSpace(entity.title))
                {
                    entity.rno = entity.title.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries).First();
                }

                var ridx = 0;
                var rows = table_node.SelectNodes(".//tr");
                foreach (HtmlAgilityPack.HtmlNode row in rows)
                {
                    ridx++;
                    if (ridx == 1) continue;

                    var cidx = 0;
                    var player = new TCriteriaPlayer();
                    var cells = row.SelectNodes("th|td");
                    if (cells == null || cells.Count == 0)
                    {
                        continue;
                    }

                    foreach (HtmlAgilityPack.HtmlNode cell in cells)
                    {
                        cidx++;
                        var text = GetInnerText(cell);
                        //Console.WriteLine($"r{ridx}\tc{cidx}\t: {text}");

                        player.idx = ridx - 1;
                        switch (cidx)
                        {
                            case 1: player.sign_no = text; break;
                            case 2: player.org = text; break;
                            case 3: player.name = text; break;
                            case 4: player.score = text; break;
                            case 5: player.rank = text; break;
                        }
                    }
                    entity.list.Add(player);
                }
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, ex.Message);
            }
            return entity;
        }

        private List<TCriteriaModel> GetRoundList(TConfig cfg, string fullfile)
        {
            var list = new List<TCriteriaModel>();
            try
            {
                var contents = System.IO.File.ReadAllText(fullfile, System.Text.Encoding.UTF8);

                var xml_page = ".//table";

                var doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(contents);

                var src = doc.DocumentNode.SelectSingleNode(xml_page);
                if (src == null) return list;

                var bypass_arr = new string[] { "場次", "狀態" };
                var ridx = 0;
                var rows = src.SelectNodes(".//tr");

                foreach (HtmlAgilityPack.HtmlNode row in rows)
                {
                    ridx++;

                    var cidx = 0;
                    var entity = default(TCriteriaModel);
                    var cells = row.SelectNodes("th|td");
                    if (cells == null || cells.Count == 0)
                    {
                        continue;
                    }

                    foreach (HtmlAgilityPack.HtmlNode cell in cells)
                    {
                        cidx++;
                        var text = GetInnerText(cell);
                        //Console.WriteLine($"r{ridx}\tc{cidx}\t: {text}");

                        if (bypass_arr.Contains(text))
                        {
                            continue;
                        }

                        var is_no_cell = cidx % 2 == 1;
                        if (is_no_cell)
                        {
                            if (string.IsNullOrWhiteSpace(text))
                            {
                                continue;
                            }

                            entity = new TCriteriaModel
                            {
                                rno = text,
                                status = "",
                            };
                            list.Add(entity);
                        }
                        else
                        {
                            if (entity != null)
                            {
                                entity.status = text;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, ex.Message);
            }
            return list;
        }

        private string GetInnerText(HtmlAgilityPack.HtmlNode cell)
        {
            try
            {
                if (cell == null) return "";
                return cell.InnerText;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        private void DownloadWebPage(TConfig cfg, string fullurl, string fullfile)
        {
            try
            {
                var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(fullurl);
                var response = (System.Net.HttpWebResponse)request.GetResponse();
                StreamReader tReader = new StreamReader(response.GetResponseStream());

                string rst = tReader.ReadToEnd();
                if (!string.IsNullOrWhiteSpace(rst))
                {
                    System.IO.File.WriteAllText(fullfile, rst, System.Text.Encoding.UTF8);
                }
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, ex.Message);
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }

            public string score_url { get; set; }

            public string site_path { get; set; }
            public string folder_path { get; set; }

            public Item itmMeeting { get; set; }

            //public string enumber { get; set; }
            //public string rnumber { get; set; }
        }

        private class TCriteriaModel
        {
            public string rno { get; set; }
            public string status { get; set; }
            public string title { get; set; }
            public List<TCriteriaPlayer> list { get; set; }
            public bool is_closed { get; set; }
        }

        private class TCriteriaPlayer
        {
            public int idx { get; set; }
            public string sign_no { get; set; }
            public string org { get; set; }
            public string name { get; set; }
            public string score { get; set; }
            public string rank { get; set; }
        }

        private class TPage
        {
            public int min_serial { get; set; }
            public int max_serial { get; set; }
            public List<TSite> sites { get; set; }
        }

        private class TSite
        {
            public string code { get; set; }
            public string name { get; set; }
            public List<TEvt> rounds { get; set; }
        }

        private class TEvt
        {
            public int code { get; set; }
            public int serial { get; set; }
            public string in_number { get; set; }
            public Item item { get; set; }
        }

        private List<TRow> MapList(TConfig cfg, string value)
        {
            try
            {
                var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TRow>>(value);
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private class TRow
        {
            public string c00 { get; set; }
            public string c01 { get; set; }
            public string c02 { get; set; }
            public string c03 { get; set; }
        }

        private int GetIntVal(string value)
        {
            if (value == "" || value == "0") return 0;

            int result = 0;
            int.TryParse(value, out result);
            return result;
        }
    }
}