﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using System.Net;

namespace WorkHelp.ArasDesk.Methods.Tkd.Common
{
    public class In_InnSport_Service : Item
    {
        public In_InnSport_Service(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: InnSport.Sync.Service
                日誌: 
                    - 2022-11-24: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_InnSport_Service";
            
            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),
                scene = itmR.getProperty("scene", ""),
            };

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "in: " + cfg.scene + ": " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));

            switch (cfg.scene)
            {
                case "save_setting"://儲存設定
                    SaveSetting(cfg, itmR);
                    break;

                case "In_Meeting_Criteria_Round"://格式成績同步
                    RunSyncCriteriaRound(cfg, itmR);
                    break;

                case "In_Local_NewFoughtEvent"://最新勝出場次
                    RunNewFoughtEvent(cfg, itmR);
                    break;
            }

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "en: " + cfg.scene + ": " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));

            return itmR;
        }

        //儲存設定
        private void SaveSetting(TConfig cfg, Item itmReturn)
        {
            var in_table = itmReturn.getProperty("in_table", "");
            var in_time = itmReturn.getProperty("in_time", "");
            var in_count = itmReturn.getProperty("in_count", "0");
            var ckh_count = GetIntVal(in_count);
            if (ckh_count <= 0 || ckh_count > 10)
            {
                throw new Exception("推播筆數必須為正整數 3~10 之間");
            }

            var new_time = GetDtmStr(in_time, "yyyy-MM-dd HH:mm:ss", -8);

            var sql = @"
                UPDATE In_Meeting_LocalNotify SET
                  in_table = '{#in_table}'
                , in_time = '{#in_time}'
                , in_count = '{#in_count}'
            ";

            sql = sql.Replace("{#in_table}", in_table)
                .Replace("{#in_time}", new_time)
                .Replace("{#in_count}", in_count);

            var itmResult = cfg.inn.applySQL(sql);

            if (itmResult.isError() || itmResult.getResult() == "")
            {
                throw new Exception("儲存失敗");
            }
        }

        //最新勝出場次
        private void RunNewFoughtEvent(TConfig cfg, Item itmReturn)
        {
            //Api 網址
            Item itmWebApi = cfg.inn.applySQL("SELECT TOP 1 in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = 'api_url'");
            if (itmWebApi.isError() || itmWebApi.getResult() == "")
            {
                throw new Exception("查無 Api 網址");
            }

            string api_url = itmWebApi.getProperty("in_value", "").Trim().TrimEnd('/');
            if (api_url == "")
            {
                throw new Exception("查無 Api 網址");
            }

            string sql = @"
                SELECT TOP 1 
	                id
	                , in_table
	                , REPLACE(CONVERT(VARCHAR(30), DATEADD(HOUR, 8, in_time), 126), 'T', ' ') AS 'in_time'
	                , in_count
                FROM 
	                In_Meeting_LocalNotify WITH(NOLOCK)
            ";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            var itmSetting = cfg.inn.applySQL(sql);
            if (itmSetting.isError() || itmSetting.getResult() == "") return;

            string id = itmSetting.getProperty("id", "");
            string in_table = itmSetting.getProperty("in_table", "");
            string in_time = itmSetting.getProperty("in_time", "");
            string in_count = itmSetting.getProperty("in_count", "");

            string last_time = in_time;

            sql = "SELECT TOP " + in_count + " *, convert(varchar, DATEADD(HOUR, -8, SyncTime), 121) AS 'last_time' FROM " + in_table
                + " WHERE SyncTime > '" + in_time + "' ORDER BY SyncTime ";

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string site = item.getProperty("site", "");
                string eround = item.getProperty("eround", "");
                string eweight = item.getProperty("eweight", "");
                string win = item.getProperty("win", "");
                last_time = item.getProperty("last_time", "");

                string css = "";
                string org = "";
                string name = "";

                if (win == "B")
                {
                    css = "紅方勝";
                    org = item.getProperty("blued", "");
                    name = item.getProperty("bluen", "");
                }

                if (win == "W")
                {
                    css = "白方勝";
                    org = item.getProperty("whited", "");
                    name = item.getProperty("whiten", "");
                }

                string site_name = GetSiteName(site);

                string contents = site_name
                    + " " + eround
                    + " " + css
                    + " (" + org
                    + " " + name
                    + ")"
                    ;

                RunNotify(cfg, api_url, contents);
            }

            if (count > 0)
            {
                string sql_upd = "UPDATE In_Meeting_LocalNotify SET in_time = '" + last_time + "' WHERE id = '" + id + "'";
                //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql_upd);
                cfg.inn.applySQL(sql_upd);
            }
        }

        //格式成績同步
        private void RunSyncCriteriaRound(TConfig cfg, Item itmReturn)
        {
            try
            {
                string today = DateTime.Now.Date.AddHours(-8).ToString("yyyy-MM-dd 16:00:00");

                string sql = @"
                    SELECT 
    	                id             AS 'meeting_id'
    	                , in_score_eno AS 'enumber'
                    FROM
    	                IN_MEETING WITH(NOLOCK)
                    WHERE
    	                ISNULL(in_score_eno, '') <> ''
    	                AND in_score_day = '{#today}'
                    ORDER BY
    	                in_score_day
    	                , in_score_eno
                ";

                sql = sql.Replace("{#today}", today);

                Item items = cfg.inn.applySQL(sql);

                int count = items.getItemCount();

                for (int i = 0; i < count; i++)
                {
                    Item item = items.getItemByIndex(i);

                    item.setType("In_Meeting");
                    item.setProperty("scene", "sync");
                    item.apply("In_Meeting_Criteria_Round");

                    item.setProperty("scene", "sync_rank");
                    item.apply("In_Meeting_Criteria_Round");
                }
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, ex.Message);
            }
        }

        private string GetVariable(TConfig cfg, string in_name)
        {
            Item item = cfg.inn.applySQL("SELECT TOP 1 in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = '" + in_name + "'");
            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("查無 " + in_name + " 參數資料");
            }
            return item.getProperty("in_value", "").TrimEnd('/');
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string scene { get; set; }
        }

        //推播
        private void RunNotify(TConfig cfg, string api_url, string contents)
        {
            try
            {
                string name = "Aras";
                string message = contents; //System.Web.HttpUtility.UrlEncode(contents);
                string fullurl = api_url + "/NewFoughtEvent";
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, fullurl);

                var request = (HttpWebRequest)WebRequest.Create(fullurl);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";

                var postParams = System.Web.HttpUtility.ParseQueryString(string.Empty);
                postParams.Add("name", name);
                postParams.Add("message", message);

                var byteArray = Encoding.UTF8.GetBytes(postParams.ToString());
                using (var reqStream = request.GetRequestStream())
                {
                    reqStream.Write(byteArray, 0, byteArray.Length);
                }

                string rst = string.Empty;
                using (var response = request.GetResponse())
                {
                    using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        rst = sr.ReadToEnd();
                    }
                }

            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, ex.Message);
            }
        }

        private string GetSiteName(string value)
        {
            switch (value)
            {
                case "A": return "第一場地";
                case "B": return "第二場地";
                case "C": return "第三場地";
                case "D": return "第四場地";
                case "E": return "第五場地";
                case "F": return "第六場地";
                case "G": return "第七場地";
                case "H": return "第八場地";
                case "I": return "第九場地";
                case "J": return "第十場地";
                case "K": return "第十一場地";
                case "L": return "第十二場地";
                default: return "ERR";
            }
        }

        private string GetDtmStr(string value, string format, int hours = 0)
        {
            var dt = GetDtmVal(value, hours);
            if (dt == DateTime.MinValue) return "";
            return dt.ToString(format);
        }

        private DateTime GetDtmVal(string value, int hours = 0)
        {
            if (value == "") return DateTime.MinValue;

            DateTime dt = DateTime.MinValue;

            if (DateTime.TryParse(value, out dt))
            {
                return dt.AddHours(hours);
            }

            return DateTime.MinValue;
        }

        private int GetIntVal(string value)
        {
            if (value == "") return 0;

            int result = 0;

            if (int.TryParse(value, out result))
            {
                return result;
            }

            return 0;
        }
    }
}