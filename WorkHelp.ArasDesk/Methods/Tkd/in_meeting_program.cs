﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Tkd.Common
{
    public class in_meeting_program : Item
    {
        public in_meeting_program(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 設定賽制
    輸入: meeting_id
    日期: 
        2020-09-29: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_program";

            Item itmR = this;

            string meeting_id = itmR.getProperty("meeting_id", "");
            string mode = itmR.getProperty("mode", "");

            if (meeting_id == "")
            {
                itmR.setProperty("error_message", "賽事 id 不得為空白");
                return itmR;
            }

            //檢查頁面權限
            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isMeetingLeader = itmPermit.getProperty("isMeetingLeader", "") == "1";
            bool can_view = isMeetingAdmin || isMeetingLeader;
            if (!can_view)
            {
                itmR.setProperty("error_message", "您無權限瀏覽此頁面");
                return itmR;
            }

            switch (mode)
            {
                case "save"://提交
                    GeneratePrograms(CCO, strMethodName, inn, itmR);
                    break;

                case "event"://建立場次
                    GenerateEvents(CCO, strMethodName, inn, itmR);
                    break;

                case "display"://修改呈現
                    UpdateDisplay(CCO, strMethodName, inn, itmR);
                    break;

                case "fix_names"://修正名稱
                    FixNames(CCO, strMethodName, inn, itmR);
                    break;

                case "clear"://清除資料
                    ClearPrograms(CCO, strMethodName, inn, itmR);
                    break;

                case "modal"://上傳檔案跳窗
                    FileModal(CCO, strMethodName, inn, itmR);
                    break;

                case "upload"://上傳檔案
                    FileUpload(CCO, strMethodName, inn, itmR);
                    break;

                case "new_modal"://新增跳窗
                    NewModal(CCO, strMethodName, inn, itmR);
                    break;

                case "detail_modal"://明細
                    DetailModal(CCO, strMethodName, inn, itmR);
                    break;

                case "new"://新增組別
                    New(CCO, strMethodName, inn, itmR);
                    break;

                case "edit"://編輯組別
                    Edit(CCO, strMethodName, inn, itmR);
                    break;

                case "remove"://刪除組別
                    Remove(CCO, strMethodName, inn, itmR);
                    break;

                default:
                    Query(CCO, strMethodName, inn, itmR);
                    break;
            }

            return itmR;
        }

        //刪除組別
        private void Remove(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string program_id = itmReturn.getProperty("program_id", "");

            string sql = "";
            Item itmSQL = null;

            sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + program_id + "'";
            Item itmProgram = inn.applySQL(sql);

            itmProgram.setType("In_Meeting_Program");
            itmProgram.setProperty("program_id", itmProgram.getProperty("id", ""));
            //刪除隊伍資料
            itmProgram.setProperty("is_remove_team", "1");
            //刪除場地分配
            itmProgram.setProperty("is_remove_allocation", "1");
            itmProgram.apply("in_meeting_program_remove_all");

            sql = "DELETE FROM IN_MEETING_PROGRAM WHERE id = '" + program_id + "'";
            itmSQL = inn.applySQL(sql);
        }

        //新增組別
        private void New(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            TProgram program = new TProgram
            {
                in_meeting = itmReturn.getProperty("meeting_id", ""),
                in_l1 = itmReturn.getProperty("in_l1", ""),
                in_l2 = itmReturn.getProperty("in_l2", ""),
                in_l3 = itmReturn.getProperty("in_l3", ""),
                in_short_name = itmReturn.getProperty("in_short_name", ""),
                in_battle_type = itmReturn.getProperty("in_battle_type", ""),
                in_rank_type = itmReturn.getProperty("in_rank_type", ""),
                min_kg = itmReturn.getProperty("min_kg", ""),
                max_kg = itmReturn.getProperty("max_kg", ""),
            };

            program.in_name = string.Join("-", new string[]
            {
                program.in_l1,
                program.in_l2,
                program.in_l3,
            });

            program.in_display = program.in_name;

            string[] names = GetNames(program.in_l1, program.in_l2, program.in_l3);
            program.in_name2 = names[0];
            program.in_name3 = names[1];

            program.in_extend_value2 = program.min_kg + "-" + program.max_kg;

            var survey_id_map = GetSurveyMap(CCO, strMethodName, inn, program.in_meeting);
            var itmOption1 = AddSurveyOption(CCO, strMethodName, inn, survey_id_map["in_l1"], program.in_l1, "", "", "");
            var itmOption2 = AddSurveyOption(CCO, strMethodName, inn, survey_id_map["in_l2"], program.in_l2, program.in_l1, "", "");
            var itmOption3 = AddSurveyOption(CCO, strMethodName, inn, survey_id_map["in_l3"], program.in_l3, program.in_l2, program.in_l1, program.in_extend_value2);

            var itmSortOrder = inn.applySQL("SELECT (MAX(in_sort_order) + 100) AS 'new_sort_order' FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_meeting = '" + program.in_meeting + "'");
            var in_sort_order = "100";
            if (!itmSortOrder.isError() && itmSortOrder.getResult() != "")
            {
                in_sort_order = itmSortOrder.getProperty("new_sort_order", "100");
            }

            Item itmProgram = inn.newItem("In_Meeting_Program", "add");
            itmProgram.setProperty("in_meeting", program.in_meeting);
            itmProgram.setProperty("in_name", program.in_name);
            itmProgram.setProperty("in_display", program.in_display);
            itmProgram.setProperty("in_battle_type", program.in_battle_type);
            itmProgram.setProperty("in_rank_type", program.in_rank_type);
            itmProgram.setProperty("in_team_count", "0");
            itmProgram.setProperty("in_round_code", "0");
            itmProgram.setProperty("in_round_count", "0");

            itmProgram.setProperty("in_l1", program.in_l1);
            itmProgram.setProperty("in_l2", program.in_l2);
            itmProgram.setProperty("in_l3", program.in_l3);
            itmProgram.setProperty("in_l1_sort", itmOption1.getProperty("sort_order", ""));
            itmProgram.setProperty("in_l2_sort", itmOption2.getProperty("sort_order", ""));
            itmProgram.setProperty("in_l3_sort", itmOption3.getProperty("sort_order", ""));

            itmProgram.setProperty("in_name2", program.in_name2);
            itmProgram.setProperty("in_name3", program.in_name3);
            itmProgram.setProperty("in_short_name", program.in_short_name);

            itmProgram.setProperty("in_sort_order", in_sort_order);
            itmProgram.setProperty("in_tiebreaker", "");
            itmProgram.setProperty("in_challenge", "");

            itmProgram.setProperty("in_is_sync", "0");

            itmProgram = itmProgram.apply();

            if (itmProgram.isError())
            {
                throw new Exception("組別新增失敗");
            }
        }

        private Item AddSurveyOption(Aras.Server.Core.CallContext CCO
            , string strMethodName
            , Innovator inn
            , string survey_id
            , string in_value
            , string in_filter
            , string in_grand_filter
            , string in_extend_value2)
        {
            string sql = @"
                SELECT 
	                * 
                FROM 
	                IN_SURVEY_OPTION WITH(NOLOCK) 
                WHERE 
	                source_id = '{#survey_id}' 
	                AND in_filter = N'{#in_filter}' 
	                AND in_value = N'{#in_value}'
            ";

            sql = sql.Replace("{#survey_id}", survey_id);

            Item item = inn.applySQL(sql);

            if (item.isError()) throw new Exception("發生錯誤");
            if (item.getResult() != "") return item;

            Item itmOption = inn.newItem("IN_SURVEY_OPTION", "add");
            itmOption.setProperty("source_id", survey_id);
            itmOption.setProperty("in_label", in_value);
            itmOption.setProperty("in_value", in_value);
            itmOption.setProperty("in_filter", in_filter);
            itmOption.setProperty("in_grand_filter", in_grand_filter);
            itmOption.setProperty("in_expense_value", "0");
            itmOption.setProperty("in_extend_value2", in_extend_value2);
            itmOption = itmOption.apply();
            return itmOption;
        }

        private Dictionary<string, string> GetSurveyMap(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            string sql = @"
                SELECT
	                t2.*
                FROM
	                IN_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN
	                IN_SURVEY t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t2.in_property IN (N'in_l1', N'in_l2', N'in_l3')
                ORDER BY
	                t2.in_property
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            Item items = inn.applySQL(sql);

            if (items.isError()) throw new Exception("發生錯誤");

            if (items.getItemCount() != 3)
            {
                throw new Exception("問項資料錯誤");
            }

            Dictionary<string, string> map = new Dictionary<string, string>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string survey_id = item.getProperty("id", "");
                string in_property = item.getProperty("in_property", "");
                if (!map.ContainsKey(in_property))
                {
                    map.Add(in_property, survey_id);
                }
            }

            return map;
        }

        //編輯組別
        private void Edit(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string program_id = itmReturn.getProperty("program_id", "");

            string sql = "UPDATE IN_MEETING_PROGRAM SET "
                + "  in_name = N'" + itmReturn.getProperty("in_name", "") + "' "
                + ", in_name2 = N'" + itmReturn.getProperty("in_name2", "") + "' "
                + ", in_name3 = N'" + itmReturn.getProperty("in_name3", "") + "' "
                + ", in_short_name = N'" + itmReturn.getProperty("in_short_name", "") + "' "
                + ", in_sort_order = '" + itmReturn.getProperty("in_sort_order", "") + "' "
                + ", in_team_count = '" + itmReturn.getProperty("in_team_count", "") + "' "
                + ", in_round_code = '" + itmReturn.getProperty("in_round_code", "") + "' "
                + ", in_round_count = '" + itmReturn.getProperty("in_round_count", "") + "' "
                + ", in_sect_start = '" + itmReturn.getProperty("in_sect_start", "") + "' "
                + " WHERE id = '" + program_id + "'";

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmSQL = inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("修改失敗");
            }
        }

        //新增跳窗
        private void NewModal(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            Item itmMeeting = inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + meeting_id + "'");
            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));
        }

        //明細
        private void DetailModal(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string program_id = itmReturn.getProperty("program_id", "");
            Item itmProgram = inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + program_id + "'");

            itmReturn.setProperty("in_l1", itmProgram.getProperty("in_l1", ""));
            itmReturn.setProperty("in_l2", itmProgram.getProperty("in_l2", ""));
            itmReturn.setProperty("in_l3", itmProgram.getProperty("in_l3", ""));
            itmReturn.setProperty("in_name", itmProgram.getProperty("in_name", ""));
            itmReturn.setProperty("in_name2", itmProgram.getProperty("in_name2", ""));
            itmReturn.setProperty("in_name3", itmProgram.getProperty("in_name3", " "));
            itmReturn.setProperty("in_short_name", itmProgram.getProperty("in_short_name", " "));
            itmReturn.setProperty("in_sort_order", itmProgram.getProperty("in_sort_order", " "));
            itmReturn.setProperty("in_team_count", itmProgram.getProperty("in_team_count", " "));
            itmReturn.setProperty("in_round_code", itmProgram.getProperty("in_round_code", " "));
            itmReturn.setProperty("in_round_count", itmProgram.getProperty("in_round_count", " "));
            itmReturn.setProperty("in_sect_start", itmProgram.getProperty("in_sect_start", " "));
        }

        //上傳檔案 Modal
        private void FileModal(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string program_id = itmReturn.getProperty("program_id", "");

            string sql = @"
                SELECT	
                	t1.in_name3
                	, ISNULL(t1.in_file1, '')  AS 'old_file1_id'
                	, ISNULL(t11.mimetype, '') AS 'old_file1_mimetype'
                	, ISNULL(t1.in_file2, '')  AS 'old_file2_id'
                	, ISNULL(t12.mimetype, '') AS 'old_file2_mimetype'
                	, ISNULL(t1.in_file3, '')  AS 'old_file3_id'
                	, ISNULL(t13.mimetype, '') AS 'old_file3_mimetype'
                FROM 
                	IN_MEETING_PROGRAM t1 WITH(NOLOCK) 
                LEFT OUTER JOIN
                	[File] t11 WITH(NOLOCK)
                	ON t11.id = t1.in_file1
                LEFT OUTER JOIN
                	[File] t12 WITH(NOLOCK)
                	ON t12.id = t1.in_file2
                LEFT OUTER JOIN
                	[File] t13 WITH(NOLOCK)
                	ON t13.id = t1.in_file3
                WHERE 
                	t1.id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id);

            Item itmData = inn.applySQL(sql);

            itmReturn.setProperty("in_name3", itmData.getProperty("in_name3", ""));

            itmReturn.setProperty("old_file1", itmData.getProperty("old_file1_id", " "));
            itmReturn.setProperty("old_file1_ext", itmData.getProperty("old_file1_mimetype", " "));

            itmReturn.setProperty("old_file2", itmData.getProperty("old_file2_id", " "));
            itmReturn.setProperty("old_file2_ext", itmData.getProperty("old_file2_mimetype", " "));

            itmReturn.setProperty("old_file3", itmData.getProperty("old_file3_id", " "));
            itmReturn.setProperty("old_file3_ext", itmData.getProperty("old_file3_mimetype", " "));
        }

        //上傳檔案
        private void FileUpload(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string program_id = itmReturn.getProperty("program_id", "");
            string in_file1 = itmReturn.getProperty("in_file1", "");
            string in_file2 = itmReturn.getProperty("in_file2", "");
            string in_file3 = itmReturn.getProperty("in_file3", "");

            string sql = "UPDATE IN_MEETING_PROGRAM SET"
                + "  in_file1 = '" + in_file1 + "'"
                + ", in_file2 = '" + in_file2 + "'"
                + ", in_file3 = '" + in_file3 + "'"
                + " WHERE id = '" + program_id + "'";

            Item itmSQL = inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("上傳檔案失敗");
            }
        }

        //提交組別
        private void GeneratePrograms(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            TConfig cfg = new TConfig
            {
                meeting_id = itmReturn.getProperty("meeting_id", ""),
                battle_type = itmReturn.getProperty("battle_type", ""),
                rank_type = itmReturn.getProperty("rank_type", ""),
                surface_code = itmReturn.getProperty("surface_code", ""),
                robin_player = itmReturn.getProperty("robin_player", ""),
                sub_event = itmReturn.getProperty("sub_event", ""),
            };

            bool is_ok = UpdateMeeting(CCO, strMethodName, inn, cfg, itmReturn);
            if (!is_ok) return;

            //重建組別
            is_ok = RebuildProgram(CCO, strMethodName, inn, cfg, itmReturn);
            if (!is_ok) return;

            //修正場地資料
            is_ok = BindSites(CCO, strMethodName, inn, cfg, itmReturn);
            if (!is_ok) return;

            //賦予與會者量級序號 (排序規則 in_stuff_b1, in_team, in_sno)
            is_ok = SetMUserSectionNo(CCO, strMethodName, inn, cfg, itmReturn);
            if (!is_ok) return;

            //建立組別隊伍與抽籤用 XLS (如無隊伍存在)
            is_ok = GenerateMTeams(CCO, strMethodName, inn, cfg, itmReturn);
            if (!is_ok) return;

            //清除緩存
            ClearCache(CCO, strMethodName, inn, cfg, itmReturn);

            //附加賽事組別資訊
            Item itmPrograms = GetPrograms(CCO, strMethodName, inn, cfg.meeting_id);
            AppendPrograms(itmPrograms, itmReturn);
        }

        //清除暫存資訊(三階選單)
        private void ClearCache(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, TConfig cfg, Item itmReturn)
        {
            Item itmData = inn.newItem();
            itmData.setType("In_Meeting");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("scene", "clear");
            itmData.apply("in_meeting_program_options");

            Item itmData2 = inn.newItem();
            itmData2.setType("In_Meeting");
            itmData2.setProperty("meeting_id", cfg.meeting_id);
            itmData2.setProperty("scene", "clear");
            itmData2.apply("in_meeting_day_options");
        }

        //清除賽程資料資料
        private void ClearPrograms(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string exec = itmReturn.getProperty("exec", "").ToLower();
            switch (exec)
            {
                case "all":
                    ClearAllPrograms(CCO, strMethodName, inn, itmReturn);
                    break;

                case "events":
                    ClearAllEvents(CCO, strMethodName, inn, true, itmReturn);
                    ExeAllAllocation(CCO, strMethodName, inn, itmReturn);
                    break;

                case "scores"://清空成績(不含籤號)
                    ClearPlayers(CCO, strMethodName, inn, true, itmReturn);
                    ExeAllAllocation(CCO, strMethodName, inn, itmReturn);
                    break;
            }

            //清除介接本地端資料表
            ClearLocalTable(CCO, strMethodName, inn);
        }

        private void ExeAllAllocation(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");

            Item itmMeeting = inn.applySQL("SELECT in_site_mode FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + meeting_id + "'");
            if (itmMeeting.isError()) return;
            string in_site_mode = itmMeeting.getProperty("in_site_mode", "");

            Item itmDays = inn.applySQL("SELECT DISTINCT in_date_key AS 'in_date_key' FROM IN_MEETING_ALLOCATION WITH(NOLOCK)"
                + " WHERE in_meeting = '" + meeting_id + "' ORDER BY in_date_key");

            if (itmDays.isError()) return;

            int count = itmDays.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmDay = itmDays.getItemByIndex(i);
                string in_date_key = itmDay.getProperty("in_date_key", "");
                if (in_date_key == "") return;

                Item itmData = inn.newItem("In_Meeting_Program");
                itmData.setProperty("meeting_id", meeting_id);
                itmData.setProperty("in_date", in_date_key);
                itmData.setProperty("in_sort", in_site_mode);
                itmData.setProperty("mode", "fix_judo_site");
                itmData.setProperty("is_rebuild_events", "1");
                itmData.apply("in_meeting_allocation");
            }
        }

        //取得賽程日期資訊
        private Item GetAllocation(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string program_id)
        {
            string sql = @"SELECT * FROM IN_MEETING_ALLOCATION WITH(NOLOCK) WHERE in_program = '{#program_id}'";
            sql = sql.Replace("{#program_id}", program_id);
            return inn.applySQL(sql);
        }

        private void ClearLocalTable(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn)
        {
            string sql = "TRUNCATE TABLE In_Local_Schedule";
            Item itmSQL = inn.applySQL(sql);
        }

        //清除所有比賽資料
        private void ClearAllPrograms(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            string meeting_id = itmReturn.getProperty("meeting_id", "");

            sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_meeting = '" + meeting_id + "'";
            Item itmPrograms = inn.applySQL(sql);
            int count = itmPrograms.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                itmProgram.setType("In_Meeting_Program");
                itmProgram.setProperty("program_id", itmProgram.getProperty("id", ""));
                //刪除隊伍資料
                itmProgram.setProperty("is_remove_team", "1");
                //刪除場地分配
                itmProgram.setProperty("is_remove_allocation", "1");
                itmProgram.apply("in_meeting_program_remove_all");
            }

            //刪除無歸屬隊伍
            sql = "DELETE FROM IN_MEETING_PTEAM WHERE in_meeting = '" + meeting_id + "'";
            itmSQL = inn.applySQL(sql);

            //更新與會者資料
            sql = @"UPDATE
                	IN_MEETING_USER
                SET
                	in_sign_no = ''
                	, in_section_no = ''
                	, in_show_no = ''
                	, in_rollcall_time = NULL 
                	, in_weight_status = NULL
                	, in_weight = NULL
                	, in_weight_createid = NULL 
                	, in_weight_createname = NULL 
                	, in_weight_time = NULL 
                	, in_weight_result = NULL 
                WHERE
                	source_id = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);
            itmSQL = inn.applySQL(sql);

            // //刪除場地分配
            // sql = "DELETE FROM IN_MEETING_ALLOCATION WHERE in_meeting = '" + meeting_id + "'";
            // itmSQL = inn.applySQL(sql);

            //移除場地分配
            sql = "UPDATE t1 SET t1.in_program_name = t2.in_name"
                 + " FROM IN_MEETING_ALLOCATION t1 INNER JOIN IN_MEETING_PROGRAM t2 ON t2.id = t1.in_program"
                 + " WHERE t1.in_meeting = '" + meeting_id + "'";
            itmSQL = inn.applySQL(sql);

            //移除場地分配
            sql = "UPDATE t1 SET t1.in_program = NULL"
                 + " FROM IN_MEETING_ALLOCATION t1 INNER JOIN IN_MEETING_PROGRAM t2 ON t2.id = t1.in_program"
                 + " WHERE t1.in_meeting = '" + meeting_id + "'"
                 + " AND ISNULL(t1.in_program_name, '') <> ''";
            itmSQL = inn.applySQL(sql);

            //刪除組別
            sql = "DELETE FROM IN_MEETING_PROGRAM WHERE in_meeting = '" + meeting_id + "'";
            itmSQL = inn.applySQL(sql);

            // //刪除場地
            // sql = "DELETE FROM IN_MEETING_SITE WHERE in_meeting = '" + meeting_id + "'";
            // itmSQL = inn.applySQL(sql);

            sql = "UPDATE IN_MEETING SET in_draw_status = '', in_draw_file = '', in_site_mode = '' WHERE id = '" + meeting_id + "'";
            //sql = "UPDATE IN_MEETING SET in_draw_status = '', in_draw_file = '' WHERE id = '" + meeting_id + "'";
            itmSQL = inn.applySQL(sql);
        }

        private void ClearPlayers(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, bool clear_sign_no, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            string meeting_id = itmReturn.getProperty("meeting_id", "");

            //清空隊伍名次資料
            sql = @"
                UPDATE
                    IN_MEETING_PTEAM
                SET
                    in_final_rank = NULL
                    , in_show_rank = NULL
                WHERE
                    in_meeting = '{#meeting_id}'
            ";
            sql = sql.Replace("{#meeting_id}", meeting_id);
            itmSQL = inn.applySQL(sql);


            // //清空組別抽籤資料
            // sql = @"
            //     UPDATE
            //         IN_MEETING_PROGRAM
            //     SET
            //         in_sign_time = NULL
            //     WHERE
            //         in_meeting = '{#meeting_id}'
            // ";
            // sql = sql.Replace("{#meeting_id}", meeting_id);
            // itmSQL = inn.applySQL(sql);
        }

        //清除所有比賽資料
        private void ClearAllEvents(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, bool clear_sign_no, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            string meeting_id = itmReturn.getProperty("meeting_id", "");


            //更新與會者 serial no
            sql = @"
                UPDATE t1 SET
                    t1.in_show_no = t2.rno
                FROM 
                    IN_MEETING_USER t1
                INNER JOIN
                    (
                        SELECT
                            id
                            , ROW_NUMBER() OVER (PARTITION BY in_l1, in_l2, in_l3 ORDER BY in_stuff_b1, in_team, in_sno) AS 'rno'
                        FROM
                            IN_MEETING_USER WITH(NOLOCK)
                        WHERE
                            source_id = '{#meeting_id}'
                            AND in_l1 NOT IN (N'隊職員', N'常年會費')
                    ) t2
                    ON t2.id = t1.id
                WHERE
                    t1.source_id = '{#meeting_id}'
            ";
            sql = sql.Replace("{#meeting_id}", meeting_id);
            itmSQL = inn.applySQL(sql);

            //更新與會者資料
            sql = @"
                UPDATE
                    IN_MEETING_USER
                SET
                    in_sign_no = ''
                    , in_section_no = in_show_no
                    , in_rollcall_time = NULL 
                    , in_weight_status = NULL
                    , in_weight = NULL
                    , in_weight_createid = NULL 
                    , in_weight_createname = NULL 
                    , in_weight_time = NULL 
                    , in_weight_result = NULL
                    , in_spot_min = NULL
                    , in_spot_max = NULL
                    , in_spot_no = NULL
                    , in_spot_checked = NULL
                    , in_spot_weight = NULL
                    , in_spot_result = NULL
                    , in_spot_time = NULL
                    , in_spot_status = NULL
                    , in_weight_sign = NULL
                    , in_spot_sign = NULL
                WHERE
                    source_id = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);
            itmSQL = inn.applySQL(sql);

            //更新隊伍 serial no
            sql = @"
                UPDATE t1 SET
                    t1.in_show_no = t2.rno
                FROM 
                    IN_MEETING_PTEAM t1
                INNER JOIN
                    (
                        SELECT
                        id
                        , ROW_NUMBER() OVER (PARTITION BY source_id ORDER BY in_stuff_b1, in_team, in_sno) AS 'rno'
                        FROM
                        IN_MEETING_PTEAM WITH(NOLOCK)
                        WHERE
                        in_meeting = '{#meeting_id}'
                    ) t2
                    ON t2.id = t1.id
                WHERE
                    t1.in_meeting = '{#meeting_id}'
            ";
            sql = sql.Replace("{#meeting_id}", meeting_id);
            itmSQL = inn.applySQL(sql);

            //更新隊伍資料
            sql = @"
                UPDATE
                    IN_MEETING_PTEAM
                SET
                    in_sign_time = NULL
                    , in_final_rank = NULL
                    , in_show_rank = NULL
                    , in_check_result = NULL
                    , in_check_status = NULL 
                    , in_rollcall_result = NULL
                    , in_weight_result = NULL
                    , in_weight_value = NULL 
                    , in_sign_no = NULL 
                    , in_judo_no = NULL
                    , in_section_no = NULL
                    , in_weight_message = NULL
                where
                    in_meeting = '{#meeting_id}'
            ";
            sql = sql.Replace("{#meeting_id}", meeting_id);
            itmSQL = inn.applySQL(sql);


            //清空組別抽籤資料
            sql = @"
                UPDATE
                    IN_MEETING_PROGRAM
                SET
                    in_sign_time = NULL
                WHERE
                    in_meeting = '{#meeting_id}'
            ";
            sql = sql.Replace("{#meeting_id}", meeting_id);
            itmSQL = inn.applySQL(sql);
        }

        //清除所有比賽資料
        private void ClearAllEvents2(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, bool clear_sign_no, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            string meeting_id = itmReturn.getProperty("meeting_id", "");

            //清空場次明細
            sql = @"
                UPDATE t1 SET
                	in_sign_action = NULL
                	, in_sign_bypass = NULL
                	, in_sign_status = NULL
                	, in_target_bypass = NULL
                	, in_target_status = NULL
                	, in_correct_count = NULL
                	, in_points = NULL
                	, in_points_type = NULL
                	, in_points_text = NULL
                	, in_score = NULL
                	, in_status = NULL
                FROM
                	IN_MEETING_PEVENT_DETAIL t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PEVENT t2 WITH(NOLOCK)
                	ON t2.id = t1.source_id
                WHERE
                	t2.in_meeting = '{#meeting_id}'
                    AND t2.in_tree_name = 'main'
                    AND t2.in_round = 1
            ";
            sql = sql.Replace("{#meeting_id}", meeting_id);
            itmSQL = inn.applySQL(sql);


            //清空場次明細
            sql = @"
                UPDATE t1 SET
                	in_sign_no = NULL
                	, in_sign_action = NULL
                	, in_sign_bypass = NULL
                	, in_sign_status = NULL
                	, in_target_no = NULL
                	, in_target_bypass = NULL
                	, in_target_status = NULL
                	, in_correct_count = NULL
                	, in_points = NULL
                	, in_points_type = NULL
                	, in_points_text = NULL
                	, in_score = NULL
                	, in_status = NULL
                FROM
                	IN_MEETING_PEVENT_DETAIL t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PEVENT t2 WITH(NOLOCK)
                	ON t2.id = t1.source_id
                WHERE
                	t2.in_meeting = '{#meeting_id}'
                    AND t2.in_round > 1
                    AND t2.in_tree_name = 'main'
            ";
            sql = sql.Replace("{#meeting_id}", meeting_id);
            itmSQL = inn.applySQL(sql);


            //清空場次明細
            sql = @"
                UPDATE t1 SET
                	in_sign_no = NULL
                	, in_sign_action = NULL
                	, in_sign_bypass = NULL
                	, in_sign_status = NULL
                	, in_target_no = NULL
                	, in_target_bypass = NULL
                	, in_target_status = NULL
                	, in_correct_count = NULL
                	, in_points = NULL
                	, in_points_type = NULL
                	, in_points_text = NULL
                	, in_score = NULL
                	, in_status = NULL
                FROM
                	IN_MEETING_PEVENT_DETAIL t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PEVENT t2 WITH(NOLOCK)
                	ON t2.id = t1.source_id
                WHERE
                	t2.in_meeting = '{#meeting_id}'
                    AND t2.in_tree_name <> 'main'
            ";
            sql = sql.Replace("{#meeting_id}", meeting_id);
            itmSQL = inn.applySQL(sql);

            //清空場次狀態
            sql = @"
                UPDATE 
                	IN_MEETING_PEVENT
                SET
                	in_bypass_foot = NULL
                	, in_bypass_status = NULL
                	, in_win_status = NULL
                	, in_win_time = NULL
                	, in_win_sign_no = NULL
                	, in_win_creator = NULL
                	, in_win_creator_sno = NULL
                WHERE
                	in_meeting = '{#meeting_id}'
            ";
            sql = sql.Replace("{#meeting_id}", meeting_id);
            itmSQL = inn.applySQL(sql);


            //清除籤號
            if (clear_sign_no)
            {
                //更新與會者 serial no
                sql = @"
                    UPDATE t1 SET
                        t1.in_show_no = t2.rno
                    FROM 
                        IN_MEETING_USER t1
                    INNER JOIN
                        (
                            SELECT
                            id
                            , ROW_NUMBER() OVER (PARTITION BY in_l1, in_l2, in_l3 ORDER BY in_stuff_b1, in_team, in_sno) AS 'rno'
                            FROM
                                IN_MEETING_USER WITH(NOLOCK)
                            WHERE
                                source_id = '{#meeting_id}'
                                AND in_l1 NOT IN (N'隊職員', N'常年會費')
                        ) t2
                        ON t2.id = t1.id
                    WHERE
                        t1.source_id = '{#meeting_id}'
                ";
                sql = sql.Replace("{#meeting_id}", meeting_id);
                itmSQL = inn.applySQL(sql);

                //更新與會者資料
                sql = @"
                    UPDATE
                        IN_MEETING_USER
                    SET
                        in_sign_no = ''
                        , in_section_no = in_show_no
                        , in_rollcall_time = NULL 
                        , in_weight_status = NULL
                        , in_weight = NULL
                        , in_weight_createid = NULL 
                        , in_weight_createname = NULL 
                        , in_weight_time = NULL 
                        , in_weight_result = NULL
                        , in_spot_min = NULL
                        , in_spot_max = NULL
                        , in_spot_no = NULL
                        , in_spot_checked = NULL
                        , in_spot_weight = NULL
                        , in_spot_result = NULL
                        , in_spot_time = NULL
                        , in_spot_status = NULL
                        , in_weight_sign = NULL
                        , in_spot_sign = NULL
                    WHERE
                        source_id = '{#meeting_id}'
                ";

                sql = sql.Replace("{#meeting_id}", meeting_id);
                itmSQL = inn.applySQL(sql);

                //更新隊伍 serial no
                sql = @"
                    UPDATE t1 SET
                        t1.in_show_no = t2.rno
                    FROM 
                        IN_MEETING_PTEAM t1
                    INNER JOIN
                        (
                            SELECT
                            id
                            , ROW_NUMBER() OVER (PARTITION BY source_id ORDER BY in_stuff_b1, in_team, in_sno) AS 'rno'
                            FROM
                            IN_MEETING_PTEAM WITH(NOLOCK)
                            WHERE
                            in_meeting = '{#meeting_id}'
                        ) t2
                        ON t2.id = t1.id
                    WHERE
                        t1.in_meeting = '{#meeting_id}'
                ";
                sql = sql.Replace("{#meeting_id}", meeting_id);
                itmSQL = inn.applySQL(sql);

                //更新隊伍資料
                sql = @"
                    UPDATE
                        IN_MEETING_PTEAM
                    SET
                        in_final_rank = NULL
                        , in_show_rank = NULL
                        , in_check_result = NULL
                        , in_check_status = NULL 
                        , in_rollcall_result = NULL
                        , in_weight_result = NULL
                        , in_weight_value = NULL 
                        , in_sign_no = NULL 
                        , in_judo_no = NULL
                        , in_section_no = NULL
                        , in_weight_message = NULL
                    where
                        in_meeting = '{#meeting_id}'
                ";
                sql = sql.Replace("{#meeting_id}", meeting_id);
                itmSQL = inn.applySQL(sql);
            }
        }

        //修正組別名稱
        private void FixNames(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");

            string sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_meeting = '" + meeting_id + "'";

            Item itmPrograms = inn.applySQL(sql);

            string sql_update = "UPDATE IN_MEETING_PROGRAM SET in_name2 = N'{#in_name2}', in_name3 = N'{#in_name3}' WHERE id = '{#program_id}'";

            int count = itmPrograms.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                string id = itmProgram.getProperty("id", "");
                string in_l1 = itmProgram.getProperty("in_l1", "");
                string in_l2 = itmProgram.getProperty("in_l2", "");
                string in_l3 = itmProgram.getProperty("in_l3", "");

                string[] names = GetNames(in_l1, in_l2, in_l3);

                string sql_temp = sql_update.Replace("{#program_id}", id)
                    .Replace("{#in_name2}", names[0])
                    .Replace("{#in_name3}", names[1]);

                Item itmSQL = inn.applySQL(sql_temp);

                if (itmSQL.isError())
                {
                    throw new Exception("修正組別名稱發生錯誤");
                }
            }
        }

        //更新組別標籤
        private void UpdateDisplay(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string program_id = itmReturn.getProperty("program_id", "");
            //string program_display = itmReturn.getProperty("program_display", "");
            string program_short_name = itmReturn.getProperty("program_short_name", "");
            string program_sort = itmReturn.getProperty("program_sort", "");

            // string sql = "UPDATE IN_MEETING_PROGRAM SET "
            //     + " in_display = N'" + program_display + "' "
            //     + " , in_short_name = N'" + program_short_name + "' "
            //     + " , in_sort_order = '" + program_sort + "' "
            //     + " WHERE id = '" + program_id + "'";

            string sql = "UPDATE IN_MEETING_PROGRAM SET "
                + " in_short_name = N'" + program_short_name + "' "
                + " , in_sort_order = '" + program_sort + "' "
                + " WHERE id = '" + program_id + "'";

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmSQL = inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("更新組別資料失敗");
            }
        }

        //建立賽程圖
        private void GenerateEvents(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string sql = "";

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string program_id = itmReturn.getProperty("program_id", "");

            if (meeting_id == "")
            {
                return;
            }

            if (program_id == "")
            {
                sql = "SELECT * FROM In_Meeting_Program WITH(NOLOCK) WHERE in_meeting = '" + meeting_id + "'";
            }
            else
            {
                sql = "SELECT * FROM In_Meeting_Program WITH(NOLOCK) WHERE id = '" + program_id + "'";
            }

            Item itmPrograms = inn.applySQL(sql);

            int count = itmPrograms.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                string in_l1 = itmProgram.getProperty("in_l1", "");

                //建立主場
                GenerateEvents(CCO, strMethodName, inn, itmProgram, itmReturn);

            }
        }

        //建立賽程圖
        private void GenerateEvents(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmProgram, Item itmReturn)
        {
            string battle_type = itmProgram.getProperty("in_battle_type", "");

            string method = "";
            string mode = "";

            Item item = inn.newItem();
            item.setType("In_Meeting_Program");
            item.setProperty("meeting_id", itmProgram.getProperty("in_meeting", ""));
            item.setProperty("program_id", itmProgram.getProperty("id", ""));
            item.setProperty("battle_type", battle_type);

            switch (battle_type)
            {
                case "TopTwo"://單淘
                    method = "in_meeting_program_4column";
                    break;

                case "JudoTopFour"://四柱復活
                    method = "in_meeting_program_4column";
                    break;

                case "Challenge"://挑戰賽
                    method = "in_meeting_program_4column";
                    break;

                case "SingleRoundRobin"://單循環
                    method = "in_meeting_program_robin";
                    break;

                case "DoubleRoundRobin"://雙循環
                    method = "in_meeting_program_robin";
                    break;

                case "Brazil"://巴西制
                    method = "In_Meeting_DrawEvent";
                    item.setProperty("In_Meeting_DrawEvent", "create");
                    break;

                default:
                    break;
            }

            if (method != "")
            {
                item.apply(method);
            }
        }

        //查詢
        private void Query(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");

            string[] cols = new string[]
            {
                "in_title",
                "in_battle_type",
                "in_battle_repechage",
                "in_rank_type",
                "in_surface_code",
                "in_robin_player",
                "in_sub_event",
            };

            Item itmMeeting = GetMeeting(CCO, strMethodName, inn, meeting_id, cols);
            if (itmMeeting.isError())
            {
                itmReturn.setProperty("error_message", "取得賽事資料發生錯誤");
                return;
            }

            //附加賽事資訊
            AppendMeeting(itmMeeting, cols, itmReturn);

            string in_battle_type = itmMeeting.getProperty("in_battle_type", "");
            if (in_battle_type != "")
            {
                Item itmPrograms = GetPrograms(CCO, strMethodName, inn, meeting_id);
                //附加賽事組別資訊
                AppendPrograms(itmPrograms, itmReturn);
            }
            else
            {
                itmReturn.setProperty("hide_program", "item_show_0");
            }
        }

        //修改賽制
        private bool UpdateMeeting(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, TConfig cfg, Item itmReturn)
        {
            List<string> err = new List<string>();
            if (cfg.battle_type == "") err.Add("賽制 未選擇");
            if (cfg.rank_type == "") err.Add("名次類型 未選擇");
            if (cfg.surface_code == "") err.Add("每面人數 未選擇");
            if (cfg.robin_player == "") err.Add("循環賽人 未選擇");

            if (err.Count > 0)
            {
                itmReturn.setProperty("error_message", string.Join(" <br> ", err));
                return false;
            }

            string sql = @"
                UPDATE IN_MEETING SET
                    in_battle_type = N'{#in_battle_type}'
                    , in_rank_type = N'{#in_rank_type}'
                    , in_surface_code = N'{#in_surface_code}'
                    , in_robin_player = N'{#in_robin_player}'
                    , in_sub_event = {#in_sub_event}
                WHERE
                    id = '{#meeting_id}'
                ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_battle_type}", cfg.battle_type)
                .Replace("{#in_rank_type}", cfg.rank_type)
                .Replace("{#in_surface_code}", cfg.surface_code)
                .Replace("{#in_robin_player}", cfg.robin_player)
                .Replace("{#in_sub_event}", cfg.sub_event == "" ? "NULL" : cfg.sub_event);

            Item itmSQL = inn.applySQL(sql);

            if (itmSQL.isError())
            {
                itmReturn.setProperty("error_message", "提交失敗");
                return false;
            }
            else
            {
                return true;
            }
        }

        //賦予與會者量級序號 (排序規則 in_stuff_b1, in_team, in_sno)
        private bool SetMUserSectionNo(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, TConfig cfg, Item itmReturn)
        {
            string sql = @"
                UPDATE t1 SET
	                t1.in_section_no = t2.rn
	                , t1.in_show_no = t2.rn
                FROM 
	                IN_MEETING_USER t1
                INNER JOIN
                (
	                SELECT
		                ROW_NUMBER() OVER(PARTITION BY in_l1, in_l2, in_l3 ORDER BY in_stuff_b1, in_team, in_sno) AS 'rn',
		                id
	                FROM
		                IN_MEETING_USER WITH(NOLOCK)
	                WHERE
		                source_id = '{#meeting_id}'
		                AND in_l1 NOT IN (N'隊職員', N'常年會費')
                ) t2
	                ON t2.id = t1.id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_l1 NOT IN (N'隊職員', N'常年會費')
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item itmSQL = inn.applySQL(sql);

            if (itmSQL.isError())
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //建立組別隊伍與抽籤用 XLS (如無隊伍存在)
        private bool GenerateMTeams(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, TConfig cfg, Item itmReturn)
        {
            string sql = "SELECT count(*) AS 'cnt' FROM IN_MEETING_PTEAM　WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "'";
            Item itmSQL = inn.applySQL(sql);
            if (itmSQL.isError() || itmSQL.getResult() == "") return false;

            int cnt = GetIntVal(itmSQL.getProperty("cnt", "0"));
            if (cnt <= 0)
            {
                //建立組別隊伍
                Item itmData = inn.newItem();
                itmData.setType("In_Meeting");
                itmData.setProperty("meeting_id", cfg.meeting_id);
                itmData.apply("in_meeting_draw_team");

                // //建立抽籤用 XLS
                // Item itmData2 = inn.newItem();
                // itmData2.setType("In_Meeting");
                // itmData2.setProperty("meeting_id", cfg.meeting_id);
                // itmData2.setProperty("exe_type", "xls");
                // itmData2.apply("in_meeting_draw");
            }

            return true;
        }

        //重建賽事組別
        private bool RebuildProgram(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, TConfig cfg, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            //修正與會者過磅狀態
            //lina: Aras 手動修改與會者，會導致 in_weight_result 變為 0 = DQ
            sql = "UPDATE IN_MEETING_USER SET in_weight_result = NULL WHERE source_id = '" + cfg.meeting_id + "'";
            itmSQL = inn.applySQL(sql);

            //同步中
            sql = "UPDATE IN_MEETING_PROGRAM SET in_is_sync = '1' WHERE in_meeting = '" + cfg.meeting_id + "'";
            itmSQL = inn.applySQL(sql);

            //既有組別資料
            Item itmPrograms = GetPrograms(CCO, strMethodName, inn, cfg.meeting_id);

            Dictionary<string, Item> program_map = new Dictionary<string, Item>();

            for (int i = 0; i < itmPrograms.getItemCount(); i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                string in_l1 = itmProgram.getProperty("in_l1", "");
                string in_l2 = itmProgram.getProperty("in_l2", "");
                string in_l3 = itmProgram.getProperty("in_l3", "");
                string item_key = in_l1 + "-" + in_l2 + "-" + in_l3;
                if (!program_map.ContainsKey(item_key))
                {
                    program_map.Add(item_key, itmProgram);
                }
            }

            //更新組別資料
            MergePrograms(CCO, strMethodName, inn, cfg, program_map, itmReturn);

            //清除同步狀態未復歸的組別
            sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' AND ISNULL(in_is_sync, '') = '1'";
            Item itmRemoves = inn.applySQL(sql);
            for (int i = 0; i < itmRemoves.getItemCount(); i++)
            {
                Item itmRemove = itmRemoves.getItemByIndex(i);
                RemoveEvents(CCO, strMethodName, inn, itmRemove);
            }

            //刪除同步狀態未復歸的組別
            sql = "DELETE FROM IN_MEETING_PROGRAM WHERE in_meeting = '" + cfg.meeting_id + "' AND ISNULL(in_is_sync, '') = '1'";
            itmSQL = inn.applySQL(sql);

            if (itmSQL.isError())
            {
                itmReturn.setProperty("error_message", "提交失敗");
                return false;
            }
            else
            {
                return true;
            }
        }

        //連結原場地設定
        private bool BindSites(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, TConfig cfg, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            sql = "UPDATE t1 SET t1.in_program = t2.id"
                 + " FROM IN_MEETING_ALLOCATION t1 INNER JOIN IN_MEETING_PROGRAM t2"
                 + " ON t2.in_meeting = t1.in_meeting"
                 + " AND t2.in_name = t1.in_program_name"
                 + " WHERE t1.in_meeting = '" + cfg.meeting_id + "'";

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            itmSQL = inn.applySQL(sql);

            return true;
        }

        /// <summary>
        /// 清除場次資料
        /// </summary>
        private void RemoveEvents(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmProgram)
        {
            Item itmData = inn.newItem("In_Meeting_Program");
            itmData.setProperty("program_id", itmProgram.getProperty("id", ""));
            //刪除隊伍資料
            itmData.setProperty("is_remove_team", "1");
            itmData.apply("in_meeting_program_remove_all");
        }

        private Dictionary<string, TAllocate> AllocationMap(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            var map = new Dictionary<string, TAllocate>();

            string sql = @"
                SELECT 
	                t1.id
	                , t1.in_program_name
                    , t1.in_date_key
	                , t1.in_place
                    , t1.in_site
	                , t2.in_name
	                , t2.in_code
	                , ROW_NUMBER() OVER (PARTITION BY t1.in_date_key, t2.in_code ORDER BY t1.created_on) AS 'rn'
                FROM
                    IN_MEETING_ALLOCATION t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_SITE t2 WITH(NOLOCK)
                    ON t2.id = t1.in_site
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                ORDER BY
	                t1.in_date_key
	                , t1.created_on
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            Item items = inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_program_name = item.getProperty("in_program_name", "");
                if (!map.ContainsKey(in_program_name))
                {
                    var obj = new TAllocate
                    {
                        in_program_name = item.getProperty("in_program_name", ""),
                        in_fight_day = item.getProperty("in_date_key", ""),
                        in_fight_site = item.getProperty("in_place", ""),
                        site_name = item.getProperty("in_name", ""),
                        site_code = item.getProperty("in_code", ""),
                        in_site = item.getProperty("in_site", ""),
                        rn = item.getProperty("rn", ""),
                    };

                    obj.in_fight_time = GetFightTime(obj.in_program_name);

                    obj.in_site_mat = "MAT " + obj.site_code + "-" + obj.rn;
                    obj.in_site_mat2 = "MAT " + obj.site_code + "-" + obj.rn.PadLeft(2, '0');

                    map.Add(in_program_name, obj);
                }
            }
            return map;
        }

        //同步賽事組別
        private void MergePrograms(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, TConfig cfg, Dictionary<string, Item> program_map, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            //當該組人數 <= N，賽制更改為單循環賽(SingleRoundRobin)
            var srr_count = GetIntVal(cfg.robin_player);
            var srr_battle = "SingleRoundRobin";

            //問項
            List<TNode> nodes = GetOptionNodes(CCO, strMethodName, inn, cfg.meeting_id);
            //報名資訊
            Item items = GetSections(CCO, strMethodName, inn, cfg.meeting_id);

            //分配
            var allocate_map = AllocationMap(CCO, strMethodName, inn, cfg.meeting_id);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                //string in_section_name = item.getProperty("in_section_name", "");
                string item_name = item.getProperty("item_name", "");
                string item_count = item.getProperty("item_count", "");
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                string in_l3 = item.getProperty("in_l3", "");
                string item_key = in_l1 + "-" + in_l2 + "-" + in_l3;

                TNode l1 = GetNode(nodes, in_l1, "in_l1");
                TNode l2 = GetNode(l1.Nodes, in_l2, "in_l2");
                TNode l3 = GetNode(l2.Nodes, in_l3, "in_l3");

                string in_short_name = "";
                string in_weight = "";

                string rno = "";
                if (l3.not_find)
                {
                    rno = l2.not_find ? l1.RNo : l2.RNo;
                }
                else
                {
                    rno = l3.RNo;
                    in_short_name = l3.SName;
                    in_weight = l3.WName;
                }

                //人數
                int item_qty = GetIntVal(item_count);
                int[] rs = GetRoundAndSum(item_qty, 2, 2, 1);
                string round_count = rs[0].ToString();
                string round_code = rs[1].ToString();

                string program_battle_type = item_qty <= srr_count
                    ? srr_battle
                    : cfg.battle_type;

                Item itmProgram = inn.newItem("In_Meeting_Program");
                itmProgram.setProperty("in_meeting", cfg.meeting_id);
                itmProgram.setProperty("in_name", item_name);
                itmProgram.setProperty("in_display", item_name);
                itmProgram.setProperty("in_battle_type", program_battle_type);
                itmProgram.setProperty("in_rank_type", cfg.rank_type);
                itmProgram.setProperty("in_team_count", item_count);
                itmProgram.setProperty("in_round_code", round_code);
                itmProgram.setProperty("in_round_count", round_count);

                if (cfg.sub_event != "")
                {
                    itmProgram.setProperty("in_sub_event", cfg.sub_event);
                }

                itmProgram.setProperty("in_l1", in_l1);
                itmProgram.setProperty("in_l2", in_l2);
                itmProgram.setProperty("in_l3", in_l3);
                itmProgram.setProperty("in_l1_sort", l1.Sort);
                itmProgram.setProperty("in_l2_sort", l2.Sort);
                itmProgram.setProperty("in_l3_sort", l3.Sort);

                string[] names = GetNames(in_l1, in_l2, in_l3);
                itmProgram.setProperty("in_name2", names[0]);
                itmProgram.setProperty("in_name3", names[1]);
                itmProgram.setProperty("in_short_name", in_short_name);
                itmProgram.setProperty("in_weight", in_weight);

                itmProgram.setProperty("in_sort_order", rno);
                itmProgram.setProperty("in_tiebreaker", "");
                itmProgram.setProperty("in_challenge", "");

                if (allocate_map.ContainsKey(item_name))
                {
                    var obj = allocate_map[item_name];
                    itmProgram.setProperty("in_fight_day", obj.in_fight_day);
                    itmProgram.setProperty("in_fight_time", obj.in_fight_time);
                    itmProgram.setProperty("in_fight_site", obj.in_fight_site);
                    itmProgram.setProperty("in_site", obj.in_site);
                    itmProgram.setProperty("in_site_mat", obj.in_site_mat);
                    itmProgram.setProperty("in_site_mat2", obj.in_site_mat2);
                }
                else
                {
                    itmProgram.setProperty("in_fight_time", GetFightTime(item_name));
                }

                itmProgram.setProperty("in_is_sync", "0");

                string action = "add";

                if (program_map.ContainsKey(item_key))
                {
                    action = "merge";
                    Item itmOld = program_map[item_key];
                    itmProgram.setAttribute("where", "id='" + itmOld.getProperty("id", "") + "'");
                    itmProgram.setProperty("id", itmOld.getProperty("id", ""));

                    //打上存在標記
                    itmOld.setProperty("is_exist", "1");
                }

                itmProgram = itmProgram.apply(action);
            }
        }

        private string GetFightTime(string value)
        {
            if (value.Contains("國小"))
            {
                if (value.Contains("團"))
                {
                    return "3";
                }
                else if (value.Contains("高年級") || value.Contains("A"))
                {
                    return "3";
                }
                else
                {
                    return "2";
                }
            }
            else if (value.Contains("國中"))
            {
                return "3";
            }
            else if (value.Contains("高中"))
            {
                return "4";
            }
            else
            {
                return "4";
            }
        }

        private string[] GetNames(string in_l1, string in_l2, string in_l3)
        {
            bool is_multi = in_l1.Contains("團體");
            bool is_format = in_l1.Contains("格式");

            string in_name2 = "";//高中男子組第五級
            string in_name3 = "";//高中男子組第五級：73.1至81公斤

            if (is_multi)
            {
                in_name2 = in_l2;
                in_name3 = in_l2;
                // if (in_l3.Contains("特別組"))
                // {
                //     in_name2 = "特別組團體賽";
                //     in_name3 = in_name2;
                // }
                // else if (in_l3 != "")
                // {
                //     //in_name2 = in_l3.Replace("生", "子") + "團體賽";
                //     in_name2 = in_l3 + "團體賽";
                //     in_name3 = in_name2;
                // }
                // else
                // {
                //     //in_name2 = in_l2.Replace("生", "子").Replace("團-", "") + "團體賽";
                //     in_name2 = in_l2.Replace("團-", "") + "團體賽";
                //     in_name3 = in_name2;
                // }
            }
            else if (is_format)
            {
                string clear_l2 = in_l2.Replace("格-", "");
                if (in_l3 != "")
                {
                    in_name2 = clear_l2 + "-" + in_l3;
                    in_name3 = in_name2;
                }
                else
                {
                    in_name2 = clear_l2;
                    in_name3 = in_name2;
                }
            }
            else
            {
                //string clear_l2 = in_l2.Replace("生", "子").Replace("個-", "");
                string clear_l2 = in_l2.Replace("個-", "");

                if (in_l3 != "")
                {
                    char c = GetSplitChar(in_l3);
                    in_name2 = clear_l2 + in_l3.Split(new char[] { c }, StringSplitOptions.RemoveEmptyEntries).First();
                    in_name3 = clear_l2 + in_l3;
                }
                else
                {
                    string prefix = in_l1.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries).First();
                    string subfix = in_l2.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries).First();
                    string temp = subfix.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries).First();
                    in_name2 = prefix + temp;
                    in_name3 = prefix + subfix;
                }
            }

            return new string[] { in_name2, in_name3 };
        }

        private char GetSplitChar(string in_l3)
        {
            if (in_l3.Contains("：")) return '：';
            if (in_l3.Contains(":")) return ':';
            // if (in_l3.Contains("-")) return '-';
            // if (in_l3.Contains("+")) return '+';
            if (in_l3.Contains("(")) return '(';
            return ' ';
        }

        //附加賽事資訊
        private void AppendMeeting(Item itmMeeting, string[] cols, Item itmReturn)
        {
            foreach (var col in cols)
            {
                itmReturn.setProperty(col, itmMeeting.getProperty(col, ""));
            }
        }

        //附加賽事組別資訊
        private void AppendPrograms(Item items, Item itmReturn)
        {
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                string meeting_id = item.getProperty("in_meeting", "");
                string program_id = item.getProperty("id", "");
                string in_l1 = item.getProperty("in_l1", "");
                string team_link = " ";

                string in_battle_type = item.getProperty("in_battle_type", "");
                string battle_label = item.getProperty("battle_label", "");

                item.setType("inn_program");
                item.setProperty("no", (i + 1).ToString());
                item.setProperty("template", "In_Competition_Preview" + ".html");

                if (in_battle_type.StartsWith("Group"))
                {
                    item.setProperty("inn_battle_label", "<a href='c.aspx?page=In_MeetingProgram_Group.html"
                        + "&method=in_meeting_program_group"
                        + "&meeting_id=" + meeting_id
                        + "&program_id=" + program_id
                        + "' >" + battle_label + "</a>");
                }
                else
                {
                    item.setProperty("inn_battle_label", battle_label);
                }

                // if (in_l1.Contains("團體"))
                // {
                //     team_link = "<a target='_blank' href='c.aspx?page=in_team_battle_setting.html"
                //         + "&method=in_team_battle_setting"
                //         + "&meeting_id=" + meeting_id
                //         + "&program_id=" + program_id
                //         + "' >" + "<i class='fa fa-users'></i>" + "</a>";
                // }

                //item.setProperty("team_link", team_link);

                itmReturn.addRelationship(item);
            }
        }

        //取得賽事資訊
        private Item GetMeeting(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id, string[] cols)
        {
            string aml = "<AML><Item type='In_Meeting' action='get' id='{#meeting_id}' select='{#cols}'></Item></AML>";

            aml = aml.Replace("{#meeting_id}", meeting_id)
                .Replace("{#cols}", string.Join(",", cols));

            return inn.applyAML(aml);
        }

        //取得報名組別資訊
        private Item GetSections(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            string sql = @"
                SELECT
                    t1.source_id
                    , t1.in_l1
                    , t1.in_l2
                    , t1.in_l3
                    , t1.item_name  AS 'item_name'
                    , count(*)      AS 'item_count'
                FROM
                (
                    SELECT
                        source_id
                        , ISNULL(in_l1, '')    AS 'in_l1'
                        , ISNULL(in_l2, '')    AS 'in_l2'
                        , ISNULL(in_l3, '')    AS 'in_l3'
                        , ISNULL(in_l1, '') + '-' + ISNULL(in_l2, '') + '-' + ISNULL(in_l3, '') AS 'item_name'
                        , ISNULL(in_index, '') AS 'in_index'
                        , in_current_org
                        , in_creator_sno
                        , count(id)            AS 'in_count'
                    FROM
                        IN_MEETING_USER WITH(NOLOCK)
                    WHERE
                        source_id = '{#meeting_id}'
                        AND in_l1 NOT IN (N'隊職員', N'常年會費')
                    GROUP BY
                        source_id
                        , in_l1
                        , in_l2
                        , in_l3
                        , in_index
                        , in_current_org
                        , in_creator_sno
                ) t1
                WHERE 
                    t1.source_id = '{#meeting_id}'
                GROUP BY
                    t1.source_id
                    , t1.in_l1
                    , t1.in_l2
                    , t1.in_l3
                    , t1.item_name
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            return inn.applySQL(sql);
        }

        //取得賽事組別資訊
        private Item GetPrograms(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            string sql = @"
                SELECT 
                    t1.*
                    , t2.label AS 'battle_label' 
                FROM 
                    IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                (
                    SELECT
                        t12.value AS 'value'
                        , t12.label_zt AS 'label'
                    FROM
                        [LIST] t11 WITH(NOLOCK)
                    INNER JOIN
                        [VALUE] t12 WITH(NOLOCK)
                        ON t12.source_id = t11.id
                    WHERE
                        t11.name = N'In_Meeting_BattleType'
                ) t2
                    ON t1.in_battle_type = t2.value
                WHERE 
                    t1.in_meeting = '{#meeting_id}'
                    AND ISNULL(t1.in_program, '') = ''
                ORDER BY
                    t1.in_sort_order
                ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);

        }

        #region 問項排序

        //附加細項相關資訊
        private List<TNode> GetOptionNodes(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            Item itmData = GetLevelId(CCO, strMethodName, inn, meeting_id);
            Item items = GetOptions123(CCO, strMethodName, inn, itmData);

            List<TNode> nodes = new List<TNode>();

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                TNode l1 = AddAndGetNode(nodes, item, "in_l1");
                TNode l2 = AddAndGetNode(l1.Nodes, item, "in_l2");
                TNode l3 = AddAndGetNode(l2.Nodes, item, "in_l3");
            }

            return nodes;
        }


        private Item GetLevelId(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            string sql = @"
                SELECT 
                    t1.id,
                    t1.in_questions,
                    t1.in_property,
                    t2.sort_order
                FROM 
                    IN_SURVEY t1 WITH(NOLOCK)
                INNER JOIN 
                    IN_MEETING_SURVEYS t2 WITH(NOLOCK)
                    ON t2.related_id = t1.id 
                WHERE 
                    t2.source_id = '{#meeting_id}' 
                    AND t1.in_property IN (N'in_l1', N'in_l2', N'in_l3')
                ORDER BY 
                    t1.in_property
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                throw new Exception("取得問項資料發生錯誤");
            }

            Item itmResult = inn.newItem();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_property = item.getProperty("in_property", "");

                itmResult.setProperty(in_property + "_id", id);
            }

            return itmResult;
        }

        private Item GetOptions123(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmData)
        {
            string in_l1_id = itmData.getProperty("in_l1_id", "");
            string in_l2_id = itmData.getProperty("in_l2_id", "");
            string in_l3_id = itmData.getProperty("in_l3_id", "");

            string sql = @"
        SELECT
            ROW_NUMBER() OVER (ORDER BY t1.sort_order, t2.sort_order, t3.sort_order) * 100 AS rno
            , t1.in_value         AS 'in_l1_value'
            , t1.in_label         AS 'in_l1_label'
            , t1.sort_order       AS 'in_l1_sort_order'
            , t1.in_extend_value  AS 'in_l1_extend_value'
            , t2.in_value         AS 'in_l2_value'
            , t2.in_label         AS 'in_l2_label'
            , t2.sort_order       AS 'in_l2_sort_order'
            , t2.in_extend_value  AS 'in_l2_extend_value'
            , t3.in_value         AS 'in_l3_value'
            , t3.in_label         AS 'in_l3_label'
            , t3.sort_order       AS 'in_l3_sort_order'
            , t3.in_extend_value  AS 'in_l3_extend_value'
            , t3.in_n1            AS 'in_l3_sname'
            , t3.in_weight        AS 'in_l3_wname'
        FROM 
            IN_SURVEY_OPTION t1 WITH(NOLOCK)
        LEFT OUTER JOIN (
            SELECT IN_FILTER, IN_VALUE, IN_LABEL, SORT_ORDER, IN_EXPENSE_VALUE, IN_EXTEND_VALUE, IN_N1, IN_WEIGHT FROM IN_SURVEY_OPTION WITH(NOLOCK) WHERE SOURCE_ID = '{#in_l2_id}'
            ) t2 ON t2.IN_FILTER = t1.IN_VALUE
        LEFT OUTER JOIN (
            SELECT IN_FILTER, IN_VALUE, IN_LABEL, SORT_ORDER, IN_EXPENSE_VALUE, IN_EXTEND_VALUE, IN_N1, IN_WEIGHT FROM IN_SURVEY_OPTION WITH(NOLOCK) WHERE SOURCE_ID = '{#in_l3_id}'
            ) t3 ON t3.IN_FILTER = t2.IN_VALUE
        WHERE
            t1.SOURCE_ID = '{#in_l1_id}'
            AND t1.in_value NOT IN (N'隊職員', N'常年會費')
        ORDER BY
            t1.SORT_ORDER
            , t2.SORT_ORDER
            , t3.SORT_ORDER
        ";
            sql = sql.Replace("{#in_l1_id}", in_l1_id)
                .Replace("{#in_l2_id}", in_l2_id)
                .Replace("{#in_l3_id}", in_l3_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        private TNode AddAndGetNode(List<TNode> nodes, Item item, string lv)
        {
            string value = item.getProperty(lv + "_value", "");

            TNode search = nodes.Find(x => x.Val == value);

            if (search == null)
            {
                search = new TNode
                {
                    Lv = lv,
                    Val = value,
                    Lbl = item.getProperty(lv + "_label", ""),
                    //Ext = item.getProperty(lv + "_extend_value", ""),
                    Sort = item.getProperty(lv + "_sort_order", ""),
                    SName = item.getProperty(lv + "_sname", ""),// in_short_name
                    WName = item.getProperty(lv + "_wname", ""),// in_weight
                    RNo = item.getProperty("rno", ""),
                    Nodes = new List<TNode>()
                };
                nodes.Add(search);
            }

            return search;
        }

        private TNode GetNode(List<TNode> nodes, string value, string lv)
        {
            if (value == "" || lv == "")
            {
                return new TNode
                {
                    not_find = true,
                    Sort = "999999",
                    Nodes = new List<TNode>()
                };
            }

            TNode search = nodes.Find(x => x.Val == value);

            if (search == null)
            {
                search = new TNode
                {
                    not_find = true,
                    Sort = "999999",
                    Nodes = new List<TNode>()
                };
            }

            return search;
        }

        private class TNode
        {
            public bool not_find { get; set; }
            public string Lv { get; set; }
            public string Val { get; set; }
            public string Lbl { get; set; }
            public string Ext { get; set; }
            public string RNo { get; set; }
            public string Sort { get; set; }
            public string SName { get; set; }
            public string WName { get; set; }
            public List<TNode> Nodes { get; set; }
        }

        #endregion 問項排序

        private class TConfig
        {
            public string meeting_id { get; set; }
            public string battle_type { get; set; }
            public string rank_type { get; set; }
            public string surface_code { get; set; }
            public string robin_player { get; set; }
            public string sub_event { get; set; }
        }

        private class TProgram
        {
            public string in_meeting { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_name { get; set; }
            public string in_name2 { get; set; }
            public string in_name3 { get; set; }
            public string in_display { get; set; }
            public string in_short_name { get; set; }
            public string in_battle_type { get; set; }
            public string in_rank_type { get; set; }

            public string min_kg { get; set; }
            public string max_kg { get; set; }
            public string in_extend_value2 { get; set; }
        }

        private class TAllocate
        {
            public string in_program_name { get; set; }
            public string in_fight_day { get; set; }
            public string in_fight_time { get; set; }
            public string in_fight_site { get; set; }
            public string site_code { get; set; }
            public string site_name { get; set; }
            public string rn { get; set; }

            public string in_site { get; set; }
            public string in_site_mat { get; set; }
            public string in_site_mat2 { get; set; }
        }

        /// <summary>
        /// 取得最大輪次代碼
        /// </summary>
        private static int[] GetRoundAndSum(int value, int code, int sum, int round)
        {
            if (value == 0)
            {
                return new int[] { 0, 0 };
            }
            else if (value > sum)
            {
                return GetRoundAndSum(value, code, code * sum, round + 1);
            }
            else
            {
                return new int[] { round, sum };
            }
        }

        private int GetIntVal(string value, int def = 0)
        {
            if (value == "") return 0;

            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}