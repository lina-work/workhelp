﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Tkd
{
    public class In_Local_Sync : Item
    {
        public In_Local_Sync(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 現場系統整合(成績同步)
                輸入: meeting_id
                日期: 
                    - 2022-08-10: TKD 版本 (lina)
                    - 2021-09-28: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Local_Sync";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            CCO.Utilities.WriteDebug(strMethodName, "in");

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
            };

            //即時成績(R1+R2)與新同步資料
            Item itmSchedules = GetLocalSchedules(cfg);
            RefreshScore(cfg, itmSchedules);

            //成績修正
            Item itmChanges = GetChangeScores(cfg);
            RefreshScore(cfg, itmChanges);

            return itmR;
        }

        private void RefreshScore(TConfig cfg, Item itmSchedules)
        {
            if (itmSchedules.isError() || itmSchedules.getResult() == "" || itmSchedules.getItemCount() <= 0)
            {
                return ;
            }

            int count = itmSchedules.getItemCount();

            for (int i = 0; i < count; i++)
            {
                var itmSchedule = itmSchedules.getItemByIndex(i);
                var row = MapRow(cfg, itmSchedule);

                if (row.meeting_id == "")
                {
                    //活動無資料
                    UpdateSchedule(cfg, row, "2");
                    continue;
                }

                var itmDetails = GetEventDetail(cfg, row);
                if (itmDetails.isError() || itmDetails.getResult() == "" || itmDetails.getItemCount() <= 0)
                {
                    //明細無資料
                    UpdateSchedule(cfg, row, "3");
                    continue;
                }

                var evt = MapEvent(cfg, itmDetails);
                var is_f1_win = row.win == "B";
                var is_f2_win = row.win == "R";

                //說明
                //  籤腳 1 單位與選手(柔: 白 White、跆: 青 Blue)
                //  籤腳 2 單位與選手(柔: 藍 Blue、跆: 紅 Red)


                if (is_f1_win)
                {
                    //籤腳 1 勝出
                    row.item.setProperty("blue_win", "勝出");
                    row.item.setProperty("red_win", "");
                }

                if (is_f2_win)
                {
                    //籤腳 2 勝出
                    row.item.setProperty("blue_win", "");
                    row.item.setProperty("red_win", "勝出");
                }

                var f1psList = new List<TPS>();
                f1psList.Add(new TPS { col = "in_sign_action", src = "Blue_Win" });
                f1psList.Add(new TPS { col = "in_points", src = "Blue_Score" });
                f1psList.Add(new TPS { col = "in_aplseq", src = "Blue_Aplseq" });
                f1psList.Add(new TPS { col = "in_check", src = "Blue_Check" });
                f1psList.Add(new TPS { col = "in_score1", src = "Blue_Score1" });
                f1psList.Add(new TPS { col = "in_score2", src = "Blue_Score2" });
                f1psList.Add(new TPS { col = "in_score3", src = "Blue_Score3" });
                f1psList.Add(new TPS { col = "in_score", src = "Blue_Score" });
                f1psList.Add(new TPS { col = "in_win1", src = "Win1" });
                f1psList.Add(new TPS { col = "in_win2", src = "Win2" });
                f1psList.Add(new TPS { col = "in_win3", src = "Win3" });

                var f2psList = new List<TPS>();
                f2psList.Add(new TPS { col = "in_sign_action", src = "Red_Win" });
                f2psList.Add(new TPS { col = "in_points", src = "Red_Score" });
                f2psList.Add(new TPS { col = "in_aplseq", src = "Red_Aplseq" });
                f2psList.Add(new TPS { col = "in_check", src = "Red_Check" });
                f2psList.Add(new TPS { col = "in_score1", src = "Red_Score1" });
                f2psList.Add(new TPS { col = "in_score2", src = "Red_Score2" });
                f2psList.Add(new TPS { col = "in_score3", src = "Red_Score3" });
                f2psList.Add(new TPS { col = "in_score", src = "Red_Score" });
                f2psList.Add(new TPS { col = "in_win1", src = "Win1" });
                f2psList.Add(new TPS { col = "in_win2", src = "Win2" });
                f2psList.Add(new TPS { col = "in_win3", src = "Win3" });

                //更新明細: 籤腳1
                UpdateDetail(cfg, evt.Foot1, row, f1psList);
                //更新明細: 籤腳2
                UpdateDetail(cfg, evt.Foot2, row, f2psList);
                //更新場次
                UpdateEvent(cfg, evt, row);

                //遞迴勝出
                if (is_f1_win)
                {
                    ApplySync(cfg, evt.Value, evt.Foot1, evt.Foot2, row);

                    //更新現場 Schedule
                    UpdateSchedule(cfg, row, "1");
                }
                else if (is_f2_win)
                {
                    ApplySync(cfg, evt.Value, evt.Foot2, evt.Foot1, row);

                    //更新現場 Schedule
                    UpdateSchedule(cfg, row, "1");
                }
            }
        }

        /// <summary>
        /// 更新現場 Schedule
        /// </summary>
        private void UpdateSchedule(TConfig cfg, TRow row, string sync_flag)
        {
            var keys = new List<string>();
            keys.Add(row.item.getProperty("blue_score1", ""));
            keys.Add(row.item.getProperty("blue_score2", ""));
            keys.Add(row.item.getProperty("blue_score3", ""));
            keys.Add(row.item.getProperty("red_score1", ""));
            keys.Add(row.item.getProperty("red_score2", ""));
            keys.Add(row.item.getProperty("red_score3", ""));
            keys.Add(row.item.getProperty("win1", ""));
            keys.Add(row.item.getProperty("win2", ""));
            keys.Add(row.item.getProperty("win3", ""));
            keys.Add(row.item.getProperty("win", ""));

            string upd_result = string.Join("-", keys);

            string sql = "UPDATE In_Local_Schedule SET"
                + " SyncFlag = '" + sync_flag + "'"
                + ", UpdResult = '" + upd_result + "'"
                + " WHERE no = " + row.no;

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmSQL = cfg.inn.applySQL(sql);
        }

        private void ApplySync(TConfig cfg, Item itmEvent, Item itmWinner, Item itmLoser, TRow row)
        {
            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting_PEvent");
            itmData.setProperty("meeting_id", row.meeting_id);
            itmData.setProperty("program_id", itmEvent.getProperty("program_id", ""));
            itmData.setProperty("event_id", itmEvent.getProperty("event_id", ""));
            itmData.setProperty("detail_id", itmWinner.getProperty("detail_id", ""));
            itmData.setProperty("target_detail_id", itmLoser.getProperty("detail_id", ""));
            itmData.setProperty("mode", "sync");

            itmData.apply("in_meeting_program_score");
        }

        private void UpdateDetail(TConfig cfg, Item itmDetail, TRow row, List<TPS> pslist)
        {
            var detail_id = itmDetail.getProperty("detail_id", "");

            var item = row.item;
            var cmds = new List<string>();
            foreach (var ps in pslist)
            {
                cmds.Add("[" + ps.col + "] = N'" + item.getProperty(ps.src.ToLower()) + "'");
            }

            var sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET "
                + string.Join(", ", cmds)
                + " WHERE id = '" + detail_id + "'";

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "更新 IN_MEETING_PEVENT_DETAIL 失敗: " + sql);
            }
        }

        private void UpdateEvent(TConfig cfg, TEvent evt, TRow row)
        {
            string sql = "";

            sql = "UPDATE IN_MEETING_PEVENT SET"
            + "   in_win_local = N'" + row.win + "'"
            + " , in_win_local_time = '" + row.tme + "'"
            + " WHERE id = '" + evt.Id + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "更新 IN_MEETING_PEVENT 失敗: " + sql);
            }
        }

        private TEvent MapEvent(TConfig cfg, Item items)
        {
            var itmEvent = items.getItemByIndex(0);

            var evt = new TEvent
            {
                Id = itmEvent.getProperty("event_id", ""),
                Value = itmEvent,
            };

            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_sign_foot = item.getProperty("in_sign_foot", "");
                if (in_sign_foot == "1")
                {
                    evt.Foot1 = item;
                }
                else
                {
                    evt.Foot2 = item;
                }
            }

            if (evt.Foot1 == null) evt.Foot1 = cfg.inn.newItem();
            if (evt.Foot2 == null) evt.Foot2 = cfg.inn.newItem();

            return evt;
        }

        private Item GetLocalSchedules(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t1.*
	                , t2.id       AS 'mtid'
	                , t2.in_title AS 'mttitle'
                FROM 
	                In_Local_Schedule t1 WITH(NOLOCK) 
                INNER JOIN 
	                IN_MEETING t2 WITH(NOLOCK) 
	                ON t2.item_number = t1.MNumber
				WHERE
					ISNULL(t1.SyncFlag, '') = ''
            ";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetChangeScores(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t1.*
	                , t2.id       AS 'mtid'
	                , t2.in_title AS 'mttitle'
                FROM 
	                In_Local_Schedule t1 WITH(NOLOCK) 
                INNER JOIN 
	                IN_MEETING t2 WITH(NOLOCK) 
	                ON t2.item_number = t1.MNumber
				WHERE
                    ISNULL(SyncFlag, '') = 1
					AND (
                        ISNULL(t1.Blue_Score1, '') 
                        + '-' + ISNULL(t1.Blue_Score2, '') 
                        + '-' + ISNULL(t1.Blue_Score3, '') 
                        + '-' + ISNULL(t1.Red_Score1, '') 
                        + '-' + ISNULL(t1.Red_Score2, '') 
                        + '-' + ISNULL(t1.Red_Score3, '') 
                        + '-' + ISNULL(t1.Win1, '') 
                        + '-' + ISNULL(t1.Win2, '') 
                        + '-' + ISNULL(t1.Win3, '') 
                        + '-' + ISNULL(t1.Win, '')
                        <> 
                        ISNULL(UpdResult, '')
                    )
            ";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetEventDetail(TConfig cfg, TRow row)
        {
            string sql = @"
                SELECT 
	                t1.source_id    AS 'program_id'
	                , t1.id         AS 'event_id'
	                , t2.id         AS 'detail_id'
	                , t2.in_sign_foot
	                , t2.in_sign_no
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN 
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN 
	                IN_MEETING_PROGRAM t3 WITH(NOLOCK)
	                ON t3.id = t1.source_id
                WHERE
					t3.in_meeting = '{#meeting_id}'
	                AND t3.in_l1 = '{#in_l1}'
	                AND t3.in_l2 = '{#in_l2}'
	                AND t3.in_l3 = '{#in_l3}'
	                AND t3.in_fight_day = '{#in_fight_day}'
					AND t1.in_tree_no = {#in_tree_no}
                ORDER BY
	                t2.in_sign_foot
            ";

            sql = sql.Replace("{#meeting_id}", row.meeting_id)
                .Replace("{#in_l1}", row.in_l1)
                .Replace("{#in_l2}", row.in_l2)
                .Replace("{#in_l3}", row.in_l3)
                .Replace("{#in_fight_day}", row.in_fight_day)
                .Replace("{#in_tree_no}", row.in_tree_no);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private string GetSiteCodeNum(string value)
        {
            switch (value)
            {
                case "A": return "1";
                case "B": return "2";
                case "C": return "3";
                case "D": return "4";
                case "E": return "5";
                case "F": return "6";
                case "G": return "7";
                case "H": return "8";
                case "I": return "9";
                case "J": return "10";
                case "K": return "11";
                case "L": return "12";
                case "M": return "13";
                case "N": return "14";
                case "O": return "15";
                case "P": return "16";
                case "Q": return "17";
                case "R": return "18";
                case "S": return "19";
                case "T": return "20";
                case "U": return "21";
                case "V": return "22";
                case "W": return "23";
                case "X": return "24";
                case "Y": return "25";
                case "Z": return "26";
                default: return "";
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
        }

        private class TEvent
        {
            /// <summary>
            /// 編號
            /// </summary>
            public string Id { get; set; }
            public Item Value { get; set; }
            public Item Foot1 { get; set; }
            public Item Foot2 { get; set; }
        }

        private class TPS
        {
            public string col { get; set; }
            public string src { get; set; }
        }

        private class TRow
        {
            public string no { get; set; }
            public string meeting_id { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_fight_day { get; set; }
            public string in_tree_no { get; set; }
            public string win { get; set; }
            public string tme { get; set; }
            public Item item { get; set; }
        }

        private TRow MapRow(TConfig cfg, Item item)
        {
            return new TRow
            {
                no = item.getProperty("no", ""),
                meeting_id = item.getProperty("mtid", ""),
                in_l1 = item.getProperty("item", ""),
                in_l2 = item.getProperty("egrade", ""),
                in_l3 = item.getProperty("weight", ""),
                in_fight_day = item.getProperty("mday", ""),
                in_tree_no = item.getProperty("eround", ""),
                win = item.getProperty("win", ""),
                tme = "", //跆拳尚無時間
                item = item,
            };
        }
    }
}