﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Tkd.Common
{
    public class In_meeting_allocation_cycle : Item
    {
        public In_meeting_allocation_cycle(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 場次編制
                日誌: 
                    - 2022-09-13: 創建 (lina)
            */

            System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_meeting_allocation_cycle";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                in_date = itmR.getProperty("in_date", ""),
                in_sort = itmR.getProperty("in_sort", ""),
                mode = itmR.getProperty("mode", ""),
            };


            //更新組別-場地場次序
            FixProgramMatNo(cfg);

            //重設場次編號(每個組別 reset tree_no)
            Item itmData = cfg.inn.newItem("In_Meeting_Program");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("program_id", "");
            itmData.setProperty("in_date", cfg.in_date);
            itmData.apply("in_meeting_pevent_reset");

            //重算獎牌戰類型、賽別時程、出賽人數
            itmData.apply("in_meeting_program_medal");


            string sql_update = "";

            //清空特殊場次編號
            sql_update = "UPDATE IN_MEETING_PEVENT SET in_tree_no = NULL"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_date_key = '" + cfg.in_date + "'"
                + " AND in_tree_name IN ('rank34', 'rank56', 'rank78', 'sub')";
            cfg.inn.applySQL(sql_update);

            sql_update = "UPDATE IN_MEETING_PEVENT SET in_tree_no = NULL"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_date_key = '" + cfg.in_date + "'"
                + " AND in_tree_name = 'repechage' AND in_round_code = 2";
            cfg.inn.applySQL(sql_update);

            //紀錄初始 TreeNo
            sql_update = "UPDATE IN_MEETING_PEVENT SET in_tree_sno = in_tree_no"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_date_key = '" + cfg.in_date + "'"
                + " AND ISNULL(in_tree_sno, 0) = 0";
            cfg.inn.applySQL(sql_update);



            var sites = SiteItems(cfg);

            var list = new List<Item>();
            AddEventItems(cfg, 128, list);
            AddEventItems(cfg, 64, list);
            AddEventItems(cfg, 32, list);


            AddEventItems(cfg, 16, list);
            AddEventItems(cfg, 8, list);
            AddEventItems57(cfg, 8, list);
            AddEventItems(cfg, 4, list);
            AddEventItems(cfg, 2, list);

            AddEventRpcItems(cfg, 128, list);
            AddEventRpcItems(cfg, 64, list);
            AddEventRpcItems(cfg, 32, list);
            AddEventRpcItems(cfg, 16, list);
            AddEventRpcItems(cfg, 8, list);

            //勝部決賽
            AddEventItems(cfg, 2, list);

            //敗部銅牌戰(W、E)
            AddEventRpcItems(cfg, 4, list);

            //敗部34名
            AddEventRpcItems(cfg, 2, list);

            //挑戰賽 3 vs 2
            AddEventClgItems(cfg, "ca01", list);
            //挑戰賽 2 vs 1
            AddEventClgItems(cfg, "ca02", list);
            //挑戰賽 1 vs 2
            AddEventClgItems(cfg, "cb01", list);


            int p = 0;
            int n = 1;
            int m = sites.Count - 1;

            for (int i = 0; i < list.Count; i++)
            {
                if (p > m)
                {
                    p = 0;
                    n++;
                }

                Item itmEvent = list[i];
                Item itmSite = sites[p];

                string eid = itmEvent.getProperty("id", "");
                string site_id = itmSite.getProperty("id", "");
                string site_code = itmSite.getProperty("in_code", "");
                string site_no = n.ToString().PadLeft(3, '0');
                string in_tree_no = site_code + site_no;

                string sql_upd = @"
                    UPDATE IN_MEETING_PEVENT SET
                        in_site_allocate = 1
                        , in_site = '{#site_id}'
                        , in_site_code = '{#site_code}'
                        , in_site_id = '{#site_no}'
                        , in_site_no = '{#in_tree_no}'
                        , in_tree_no = '{#in_tree_no}'
                    WHERE
                        id = '{#eid}'
                ";

                sql_upd = sql_upd.Replace("{#eid}", eid)
                    .Replace("{#site_id}", site_id)
                    .Replace("{#site_code}", site_code)
                    .Replace("{#site_no}", site_no)
                    .Replace("{#in_tree_no}", in_tree_no);

                cfg.inn.applySQL(sql_upd);

                p++;
            }

            //清除暫存資訊(三階選單)
            ClearCache(cfg, itmR);

            //儲存排列模式
            UpdMeetingVariable(cfg);

            return itmR;
        }

        //儲存排列模式
        private void UpdMeetingVariable(TConfig cfg)
        {
            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting_Variable");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("scene", "update");
            itmData.setProperty("name", "allocate_mode");
            itmData.setProperty("value", cfg.in_sort);
            itmData.apply("In_Meeting_Variable");
        }

        //清除暫存資訊(三階選單)
        private void ClearCache(TConfig cfg, Item itmReturn)
        {
            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("scene", "clear");
            itmData.apply("in_meeting_program_options");

            Item itmData2 = cfg.inn.newItem();
            itmData2.setType("In_Meeting");
            itmData2.setProperty("meeting_id", cfg.meeting_id);
            itmData2.setProperty("scene", "clear");
            itmData2.apply("in_meeting_day_options");
        }

        //修正組別場地場次序
        private void FixProgramMatNo(TConfig cfg)
        {
            string sql = @"
                UPDATE t1 SET
                	t1.in_site_mat = 'MAT ' + CAST(in_code AS varchar) + '-' + CAST(rn AS VARCHAR)
                	, t1.in_site_mat2 = 'MAT ' + CAST(in_code AS varchar) + '-' + RIGHT(REPLICATE('0', 2) + CAST(rn as VARCHAR), 2)
                FROM
                	IN_MEETING_PROGRAM t1
                INNER JOIN
                (
                	SELECT
                		t11.in_program
                		, t12.in_code
                		, ROW_NUMBER() OVER (PARTITION BY t12.in_code ORDER BY t11.created_on) AS 'rn'
                	FROM
                		IN_MEETING_ALLOCATION t11 WITH(NOLOCK)
                	INNER JOIN
                		IN_MEETING_SITE t12 WITH(NOLOCK)
                		ON t12.id = t11.in_site
                	WHERE
                		t11.in_meeting = '{#meeting_id}'
                		AND t11.in_date_key = '{#in_date_key}'
                ) t2 ON t2.in_program = t1.id
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmSQL = cfg.inn.applySQL(sql);

            sql = @"UPDATE t1 SET 
            	t1.in_fight_day = t2.in_date_key
            	, t1.in_fight_site = t2.in_place
            	, t1.in_site = t2.in_site
            FROM
            	IN_MEETING_PROGRAM t1
            INNER JOIN
            	IN_MEETING_ALLOCATION t2
            	ON t2.in_program = t1.id
            WHERE
            	t2.in_meeting = '{#meeting_id}'
            	AND t2.in_date_key = '{#in_date_key}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            itmSQL = cfg.inn.applySQL(sql);

        }

        private void AddEventItems57(TConfig cfg, int in_round_code, List<Item> list)
        {
            string sql = @"
                SELECT
	                t1.*
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.id = t1.source_id
                INNER JOIN
	                IN_MEETING_ALLOCATION t3 WITH(NOLOCK)
	                ON t3.in_program = t2.id
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_date_key = '{#in_date}'
	                AND t1.in_tree_name IN ('rank57a', 'rank57b')
                ORDER BY
	                t3.created_on
	                , t1.in_tree_name
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date}", cfg.in_date)
                .Replace("{#in_round_code}", in_round_code.ToString());

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                list.Add(item);
            }
        }

        private void AddEventItems(TConfig cfg, int in_round_code, List<Item> list)
        {
            string sql = @"
                SELECT
	                t1.*
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.id = t1.source_id
                INNER JOIN
	                IN_MEETING_ALLOCATION t3 WITH(NOLOCK)
	                ON t3.in_program = t2.id
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_date_key = '{#in_date}'
	                AND t1.in_tree_name = 'main'
	                AND t1.in_round_code = {#in_round_code}
	                AND ISNULL(t1.in_tree_no, 0) > 0
                ORDER BY
	                t3.created_on
	                , t1.in_tree_no
	                , t1.in_tree_id
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date}", cfg.in_date)
                .Replace("{#in_round_code}", in_round_code.ToString());

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                list.Add(item);
            }
        }

        private void AddEventRpcItems(TConfig cfg, int in_round_code, List<Item> list)
        {
            string sql = @"
                SELECT
	                t1.*
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.id = t1.source_id
                INNER JOIN
	                IN_MEETING_ALLOCATION t3 WITH(NOLOCK)
	                ON t3.in_program = t2.id
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_date_key = '{#in_date}'
	                AND t1.in_tree_name = 'repechage'
	                AND t1.in_round_code = {#in_round_code}
	                AND ISNULL(t1.in_tree_no, 0) > 0
                ORDER BY
	                t3.created_on
	                , t1.in_tree_no
	                , t1.in_tree_id
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date}", cfg.in_date)
                .Replace("{#in_round_code}", in_round_code.ToString());

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                list.Add(item);
            }
        }

        private void AddEventClgItems(TConfig cfg, string in_tree_id, List<Item> list)
        {
            string sql = @"
                SELECT
	                t1.*
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.id = t1.source_id
                INNER JOIN
	                IN_MEETING_ALLOCATION t3 WITH(NOLOCK)
	                ON t3.in_program = t2.id
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_date_key = '{#in_date}'
	                AND t1.in_tree_id = '{#in_tree_id}'
                ORDER BY
	                t3.created_on
	                , t1.in_tree_no
	                , t1.in_tree_id
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date}", cfg.in_date)
                .Replace("{#in_tree_id}", in_tree_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                list.Add(item);
            }
        }

        private List<Item> SiteItems(TConfig cfg)
        {
            var result = new List<Item>();
            string sql = @"
                SELECT 
	                * 
                FROM 
	                IN_MEETING_SITE WITH(NOLOCK) 
                WHERE 
	                in_meeting = '{#meeting_id}'
                ORDER BY
	                in_code
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                result.Add(item);
            }
            return result;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }
            public string in_date { get; set; }
            public string in_sort { get; set; }
            public string mode { get; set; }

        }
    }
}