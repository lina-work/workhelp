﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Tkd.Common
{
    public class In_MeetingDsqImport : Item
    {
        public In_MeetingDsqImport(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 失格資料匯入
    日誌: 
        - 2022-02-10: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_MeetingDsqImport";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                value = itmR.getProperty("value", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "import":
                    ImportDsqList(cfg, itmR);
                    break;

                default:
                    Page(cfg, itmR);
                    break;
            }
            return itmR;
        }

        private void ImportDsqList(TConfig cfg, Item itmReturn)
        {
            var list = MapList(cfg.value);
            if (list == null || list.Count == 0)
            {
                throw new Exception("無失格資料，請重新匯入");
            }

            foreach(var row in list)
            {

            }
        }

        private void RunDsqPlayer(TConfig cfg, TRow row)
        {
            string sql = @"
                SELECT
	                t1.id
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.in_meeting = t1.source_id
	                AND t2.in_l1 = t1.in_l1
	                AND t2.in_l2 = t1.in_l2
	                AND t2.in_l3 = t1.in_l3
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_l2 = N'青少年女子組'
	                AND t1.in_l3 = N'-44公斤級'
	                AND t1.in_current_org = N'臺中市A'
	                AND t1.in_name = N'孔芷琪'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l2}", row.in_l2)
                .Replace("{#in_l3}", row.in_l3)
                .Replace("{#in_current_org}", row.in_current_org)
                .Replace("{#in_name}", row.in_name.Replace("(失格)", ""));

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item itmMUser = cfg.inn.applySQL(sql);
            if (itmMUser.isError() || itmMUser.getResult() == "")
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "  - 查無與會者");
                throw new Exception("查無與會者: " + sql);
            }

            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting_User");
            itmData.setProperty("muid", itmMUser.getProperty("id", ""));
            itmData.setProperty("in_weight_status", "dq");
            itmData.apply("In_Meeting_Weight_Status");

        }

        private List<TRow> MapList(string value)
        {
            try
            {
                var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TRow>>(value);
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            AppendMeeting(cfg, itmReturn);
        }

        private void AppendMeeting(TConfig cfg, Item itmReturn)
        {
            string sql = "SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);

            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string value { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
        }

        private class TRow
        {
            public int no { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_name { get; set; }
            public string in_team_index { get; set; }
            public string in_current_org { get; set; }
        }

    }
}