﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.TableTennis.Common
{
    public class in_meeting_program_preview_robin : Item
    {
        public in_meeting_program_preview_robin(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 循環賽預覽
                日期: 
                    2020-11-24: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            //Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_program_preview_robin";

            Item itmR = inn.newItem();
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = this.getProperty("meeting_id", ""),
                program_id = this.getProperty("program_id", ""),
                is_edit = this.getProperty("eno", "") == "edit",
                is_draw = this.getProperty("is_draw", "") == "1",
            };

            //賽事資訊
            cfg.itmMeeting = inn.applySQL("SELECT id, in_title, in_uniform_color FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");

            //道服顏色資訊
            cfg.itmColor = inn.newItem("In_Meeting");
            cfg.itmColor.setProperty("in_uniform_color", cfg.itmMeeting.getProperty("in_uniform_color", ""));
            cfg.itmColor = cfg.itmColor.apply("in_meeting_uniform_color");

            //組別資訊: this
            cfg.itmProgram = inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");

            //附加比賽選手
            var itmPlayers = GetPlayers(cfg);

            //附加場次
            var itmEvents = GetEventPlayers(cfg);

            var map = MapTeam(itmEvents);
            var evt_lst = MapCycleEvents(cfg, itmEvents);

            if (!itmPlayers.isError())
            {
                AppendCycleEvent(cfg, map, itmPlayers, itmEvents, evt_lst, itmR);
            }

            return itmR;
        }

        private List<TCEvt> MapCycleEvents(TConfig cfg, Item items)
        {
            var rows = new List<TCEvt>();
            var count = items.getItemCount();
            for (var i  =0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                string in_tree_no = item.getProperty("in_tree_no", "");
                string in_sign_foot = item.getProperty("in_sign_foot", "");
                string in_show_no = item.getProperty("in_show_no", "");
                var row = rows.Find(x => x.in_tree_no == in_tree_no);
                if (row == null)
                {
                    row = new TCEvt { in_tree_no = in_tree_no, foot1_show_no = "", foot2_show_no = "" };
                    rows.Add(row);
                }

                if (in_sign_foot == "1")
                {
                    row.foot1_show_no = in_show_no;
                }
                else if (in_sign_foot == "2")
                {
                    row.foot2_show_no = in_show_no;
                }
            }

            return rows.OrderBy(x => x.in_tree_no).ToList();
        }

        private class TCEvt
        {
            public string in_tree_no { get; set; }
            public string foot1_show_no { get; set; }
            public string foot2_show_no { get; set; }
        }

        //循環賽
        private void AppendCycleEvent(TConfig cfg
            , Dictionary<string, TStatistics> map
            , Item itmPlayers
            , Item itmEvents
            , List<TCEvt> evt_lst
            , Item itmReturn)
        {
            var model = new TCycleModel();
            model.program_id = cfg.itmProgram.getProperty("id", "");
            model.in_battle_type = cfg.itmProgram.getProperty("in_battle_type", "");
            model.team_count = GetInt(cfg.itmProgram.getProperty("in_team_count", ""));
            model.is_double = model.in_battle_type == "DoubleRoundRobin";
            model.player_map = ConvertMap(cfg, itmPlayers, "in_sign_no");
            model.event_map = ConvertMap2(cfg, itmEvents, "in_robin_key");
            model.in_group_display = cfg.itmProgram.getProperty("in_group_display", "");

            if (model.player_map.Count == 0 || model.event_map.Count == 0)
            {
                return;
            }

            model.row_title = model.is_double ? "循環二＼循環一" : "第一循環";
            model.itmPlayers = itmPlayers;
            model.itmEvents = itmEvents;

            if (model.in_group_display != "")
            {
                model.row_title = model.in_group_display + " " + "組";
            }

            var builder = new StringBuilder();

            //循環賽表格
            AppendCycelTable(cfg, map, model, builder);

            //場次表格
            AppendEventTable(cfg, model, evt_lst, builder);

            if (cfg.is_draw)
            {
                ////對戰表格
                //AppendFightTable(cfg, model, builder);
            }

            itmReturn.setProperty("bracket_table", builder.ToString());
        }

        private List<TEvent> MapEvents(TCycleModel model)
        {
            List<TEvent> list = new List<TEvent>();

            int count = model.itmEvents.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = model.itmEvents.getItemByIndex(i);
                string event_id = item.getProperty("event_id", "");
                string in_sign_foot = item.getProperty("in_sign_foot", "");
                string in_tree_no = item.getProperty("in_tree_no", "0");
                var evt = list.Find(x => x.Id == event_id);

                if (evt == null)
                {
                    evt = new TEvent
                    {
                        Id = event_id,
                        Value = item,
                        TreeNo = GetInt(in_tree_no),
                    };
                    list.Add(evt);
                }

                if (in_sign_foot == "1")
                {
                    evt.itmFoot1 = item;
                }
                else
                {
                    evt.itmFoot2 = item;
                }
            }

            return list.OrderBy(x => x.TreeNo).ToList();
        }

        private void AppendFightTable(TConfig cfg
            , TCycleModel model
            , StringBuilder builder)
        {

            string f1n = cfg.itmColor.getProperty("f1_name", "");
            string f1c = cfg.itmColor.getProperty("f1_css", "");
            string f2n = cfg.itmColor.getProperty("f2_name", "");
            string f2c = cfg.itmColor.getProperty("f2_css", "");

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            head.AppendLine("  <tr>");
            head.AppendLine("    <td class='text-center' rowspan='2'>場地</td>");
            head.AppendLine("    <td class='text-center' rowspan='2'>場次</td>");
            head.AppendLine("    <td class='text-center " + f1c + "' rowspan='2'>" + f1n + "<br>籤號</td>");
            head.AppendLine("    <td class='text-center " + f1c + "' rowspan='2'>" + f1n + "選手</td>");
            head.AppendLine("    <td class='text-center " + f1c + "' rowspan='1' colspan='5'>得分</td>");
            head.AppendLine("    <td class='text-center " + f1c + "' rowspan='2'>得分</td>");
            head.AppendLine("    <td class='text-center' rowspan='2'>比賽時間</td>");
            head.AppendLine("    <td class='text-center " + f2c + "' rowspan='2'>得分</td>");
            head.AppendLine("    <td class='text-center " + f2c + "' rowspan='1' colspan='3'>得分</td>");
            head.AppendLine("    <td class='text-center " + f2c + "' rowspan='2'>" + f2n + "選手</td>");
            head.AppendLine("    <td class='text-center " + f2c + "' rowspan='2'>" + f2n + "<br>籤號</td>");
            head.AppendLine("  </tr>");
            head.AppendLine("  <tr>");
            head.AppendLine("    <td class='text-center " + f1c + "'>1</td>");
            head.AppendLine("    <td class='text-center " + f1c + "'>2</td>");
            head.AppendLine("    <td class='text-center " + f1c + "'>3</td>");
            head.AppendLine("    <td class='text-center " + f2c + "'>I</td>");
            head.AppendLine("    <td class='text-center " + f2c + "'>W</td>");
            head.AppendLine("    <td class='text-center " + f2c + "'>S</td>");
            head.AppendLine("  </tr>");
            head.AppendLine("</thead>");

            var list = MapEvents(model);

            body.AppendLine("<tbody>");

            for (int i = 0; i < list.Count; i++)
            {
                var evt = list[i];
                if (evt.itmFoot1 == null) evt.itmFoot1 = cfg.inn.newItem();
                if (evt.itmFoot2 == null) evt.itmFoot2 = cfg.inn.newItem();

                evt.Foot1 = MapFoot(evt.itmFoot1);
                evt.Foot2 = MapFoot(evt.itmFoot2);

                string site_name = evt.Value.getProperty("site_name", "");
                string in_win_local_time = evt.Value.getProperty("in_win_local_time", "");

                site_name = site_name.Replace("第", "").Replace("場地", "");

                body.AppendLine("<tr>");
                body.AppendLine("    <td class='text-center'>" + site_name + "</td>");
                body.AppendLine("    <td class='text-center'>" + evt.TreeNo + "</td>");

                body.AppendLine("    <td class='text-center " + f1c + "'>" + evt.Foot1.display_no + "</td>");
                body.AppendLine("    <td class='text-center " + f1c + "'>" + evt.Foot1.display_name + "</td>");
                body.AppendLine("    <td class='text-center " + f1c + "'>" + evt.Foot1.score_i + "</td>");
                body.AppendLine("    <td class='text-center " + f1c + "'>" + evt.Foot1.score_w + "</td>");
                body.AppendLine("    <td class='text-center " + f1c + "'>" + evt.Foot1.score_s + "</td>");
                body.AppendLine("    <td class='text-center " + f1c + "'>" + evt.Foot1.score_points + "</td>");

                body.AppendLine("    <td class='text-center'>" + in_win_local_time + "</td>");

                body.AppendLine("    <td class='text-center " + f2c + "'>" + evt.Foot2.score_points + "</td>");
                body.AppendLine("    <td class='text-center " + f2c + "'>" + evt.Foot2.score_i + "</td>");
                body.AppendLine("    <td class='text-center " + f2c + "'>" + evt.Foot2.score_w + "</td>");
                body.AppendLine("    <td class='text-center " + f2c + "'>" + evt.Foot2.score_s + "</td>");
                body.AppendLine("    <td class='text-center " + f2c + "'>" + evt.Foot2.display_name + "</td>");
                body.AppendLine("    <td class='text-center " + f2c + "'>" + evt.Foot2.display_no + "</td>");

                body.AppendLine("</tr>");
            }

            body.AppendLine("</tbody>");

            var table_name = "table_fight_" + model.program_id;

            builder.AppendLine("</div>");

            builder.AppendLine("<hr style='margin-top: 30px;' />");
            builder.AppendLine("<div class='main-header' style='margin-bottom: 10px; z-index: 100;'>");
            builder.AppendLine("  <h4>循環賽紀錄表 &emsp; <small><button class='btn btn-sm btn-primary' onclick='ShowRecordMessage()'><i class='fa fa-info'></i></button></small> </h4>");
            builder.AppendLine("</div>");

            builder.AppendLine("<div class='container' style='margin-top: 30px;'>");
            builder.AppendLine("<table id='" + table_name + "' class='table table-hover table-bordered table-rwd rwd rwdtable match-table'>");
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
        }

        private void AppendCycelTable(TConfig cfg
            , Dictionary<string, TStatistics> map
            , TCycleModel model
            , StringBuilder builder)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();


            head.AppendLine("<thead>");
            head.AppendLine("<tr>");
            head.AppendLine("<th class='text-center'>" + model.row_title + "</th>");
            for (int x = 1; x <= model.team_count; x++)
            {
                var itmPlayer = FindItem(cfg, model.player_map, x.ToString());
                head.AppendLine("<th class='text-center' style='min-width: 60px'>" + GetCycleHead(itmPlayer) + "</th>");
            }
            head.AppendLine("<th class='text-center'>積分</th>");
            head.AppendLine("<th class='text-center'>勝率</th>");
            head.AppendLine("<th class='text-center'>名次</th>");
            head.AppendLine("</tr>");
            head.AppendLine("</thead>");

            int count = model.itmPlayers.getItemCount();

            body.AppendLine("<tbody>");

            for (int y = 1; y <= model.team_count; y++)
            {
                Item itmPlayer = FindItem(cfg, model.player_map, y.ToString());
                string in_sign_no = itmPlayer.getProperty("in_sign_no", "");
                string check_class = GetCheckClass(itmPlayer);
                TStatistics statistics = FindStatistics(map, in_sign_no);

                body.AppendLine("<tr>");
                body.AppendLine("<td class='text-center " + check_class + "'>" + GetCycleBody(itmPlayer) + "</td>");
                for (int x = 1; x <= model.team_count; x++)
                {
                    var event_key = model.is_double ? GetDoubleCycleKey(x, y) : GetSingleCycleKey(x, y);
                    var event_items = FindItems(cfg, model.event_map, event_key);
                    var event_first = event_items.First();

                    var in_tree_id = event_first.getProperty("in_tree_id", "");

                    if (in_tree_id != "")
                    {
                        var in_tree_no = event_first.getProperty("in_tree_no", "");
                        var in_site_allocate = event_first.getProperty("in_site_allocate", "");
                        var in_site_id = event_first.getProperty("in_site_id", "");
                        var in_win_sign_no = event_first.getProperty("in_win_sign_no", "");

                        var entity = default(TDetail);
                        if (in_win_sign_no != "")
                        {
                            entity = GetBattleResult(event_items);
                        }
                        else
                        {
                            entity = GetBattleInitial(event_items);
                        }

                        var event_link = GetRowIdLink(cfg, model.program_id, "", in_tree_id, in_site_id, entity.score_div, entity.btn_class);

                        body.AppendLine("<td class='text-center'>" + entity.event_div + "<br>" + event_link + "</td>");

                        model.event_map.Remove(event_key);
                    }
                    else
                    {
                        body.AppendLine("<td class='text-center disable-evet'>&nbsp;</td>");
                    }
                }
                body.AppendLine("<td class='text-center' data-title='積分'>" + statistics.total_wins + "</td>");
                body.AppendLine("<td class='text-center' data-title='勝率'>" + statistics.total_points + "</td>");
                body.AppendLine("<td class='text-center' data-title='名次'>" + itmPlayer.getProperty("in_final_rank", "") + "</td>");
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");

            var table_name = "table_" + model.program_id;

            builder.AppendLine("<table id='" + table_name + "' class='table table-hover table-bordered table-rwd rwd rwdtable match-table'>");
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
        }

        private void AppendEventTable(TConfig cfg
            , TCycleModel model
            , List<TCEvt> rows
            , StringBuilder builder)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<th class='text-center'>場次</th>");
            head.AppendLine("<th class='text-center'>球隊</th>");

            for (int i = 0; i < rows.Count;i++)
            {
                var row = rows[i];
                body.AppendLine("<tr>");
                body.AppendLine("<td class='text-center'>" + row.in_tree_no + "</td>");
                body.AppendLine("<td class='text-center'>" + row.foot1_show_no + " Vs " + row.foot2_show_no + "</td>");
                body.AppendLine("</tr>");
            }

            var table_name = "table_evt_" + model.program_id;

            builder.AppendLine("<table id='" + table_name + "' class='table table-hover table-bordered table-rwd rwd rwdtable match-table'>");
            builder.AppendLine("<thead>");
            builder.AppendLine("<tr>");
            builder.Append(head);
            builder.AppendLine("</tr>");
            builder.AppendLine("</thead>");
            builder.AppendLine("<tbody>");
            builder.Append(body);
            builder.AppendLine("</tbody>");
            builder.AppendLine("</table>");
        }

        /// <summary>
        /// 統計隊伍積分與勝場
        /// </summary>
        private Dictionary<string, TStatistics> MapTeam(Item itmEvents)
        {
            Dictionary<string, TStatistics> map = new Dictionary<string, TStatistics>();
            int count = itmEvents.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmEvent = itmEvents.getItemByIndex(i);
                string in_sign_no = itmEvent.getProperty("in_sign_no", "");
                string in_status = itmEvent.getProperty("in_status", "");
                string in_points = itmEvent.getProperty("in_points", "0");
                int points = GetInt(in_points);

                TStatistics entity = null;
                if (map.ContainsKey(in_sign_no))
                {
                    entity = map[in_sign_no];
                }
                else
                {
                    entity = new TStatistics
                    {
                        in_sign_no = in_sign_no,
                        total_points = 0,
                        total_wins = 0,
                    };
                    map.Add(in_sign_no, entity);
                }

                if (in_status == "1")
                {
                    entity.total_points += points;
                    entity.total_wins++;
                }

            }
            return map;
        }

        private TStatistics FindStatistics(Dictionary<string, TStatistics> map, string in_sign_no)
        {
            if (map.ContainsKey(in_sign_no))
            {
                return map[in_sign_no];
            }
            else
            {
                return new TStatistics
                {
                    in_sign_no = in_sign_no,
                    total_points = 0,
                    total_wins = 0
                };
            }
        }


        private string GetRowIdLink(TConfig cfg, string pid, string is_allocate, string rid, string sid, string tno, string btn_class = "")
        {
            if (cfg.is_edit)
            {
                return GetRowIdLinkEdit(pid, is_allocate, rid, sid, tno, btn_class);
            }
            else
            {
                return GetRowIdLinkView(pid, is_allocate, rid, sid, tno, btn_class);
            }
        }

        private string GetRowIdLinkView(string pid, string is_allocate, string rid, string sid, string tno, string btn_class = "")
        {
            string current_class = btn_class == "" ? "event-btn" : btn_class;

            if (is_allocate == "1")
            {
                return "<a class='" + current_class + "' href='javascript:void()' onclick='Event_Click(this)' data-pid='" + pid + "' data-rid='" + rid + "'>" + sid + "</a>";
            }
            else if (tno == "0")
            {
                return "<a class='no-event-btn' href='javascript:void()' onclick='Event_Click(this)' data-pid='" + pid + "' data-rid='" + rid + "'>　</a>";
                //return "&nbsp;";
            }
            else if (tno != "")
            {
                string val = "&nbsp;" + tno + "&nbsp;";

                return "<a class='" + current_class + "' href='javascript:void()' onclick='Event_Click(this)' data-pid='" + pid + "' data-rid='" + rid + "'>" + val + "</a>";
            }
            else
            {
                return "<a class='no-event-btn' href='javascript:void()' onclick='Event_Click(this)' data-pid='" + pid + "' data-rid='" + rid + "'>　</a>";
                //return "&nbsp;";
            }
        }

        private string GetRowIdLinkEdit(string pid, string is_allocate, string rid, string sid, string tno, string btn_class = "")
        {
            if (is_allocate == "1")
            {
                return "<input type='text' value='" + sid + "' class='event-input'"
                + " data-pid='" + pid + "' data-rid='" + rid + "' data-old='" + sid + "'"
                + " onkeyup='Event_KeyUp(this)'"
                + " ondblclick='Event_Click(this)'"
                + " >";
            }
            else if (tno != "")
            {
                return "<input type='text' value='" + tno + "' class='event-input'"
                + " data-pid='" + pid + "' data-rid='" + rid + "' data-old='" + tno + "'"
                + " onkeyup='Event_KeyUp(this)'"
                + " ondblclick='Event_Click(this)'"
                + " >";
            }
            else
            {
                return "<input type='text' value='NA' class='event-input'"
                + " data-pid='" + pid + "' data-rid='" + rid + "' data-old='" + tno + "'"
                + " onkeyup='Event_KeyUp(this)'"
                + " ondblclick='Event_Click(this)'"
                + " >";
            }
        }

        private string GetCheckClass(Item itmPlayer)
        {
            string in_check_result = itmPlayer.getProperty("in_check_result", "");

            switch (in_check_result)
            {
                case "1": return "bg-success";
                case "0": return "bg-danger";
                default: return "";
            }
        }

        private TDetail GetBattleResult(List<Item> items)
        {
            TDetail result = new TDetail
            {
                event_div = "",
                score_div = "",
                btn_class = "",
            };

            if (items.Count < 2)
            {
                return result;
            }

            Item item1 = items[0];
            Item item2 = items[1];

            string in_tree_no = item1.getProperty("in_tree_no", "");
            string in_win_sign_no = item1.getProperty("in_win_sign_no", "");
            string in_sign_foot = item1.getProperty("in_sign_foot", "");

            string f1_sign_no = item1.getProperty("in_sign_no", "");
            string f2_sign_no = item2.getProperty("in_sign_no", "");

            if (f1_sign_no == in_win_sign_no) f1_sign_no += "(win)";
            if (f2_sign_no == in_win_sign_no) f2_sign_no += "(win)";

            if (in_sign_foot == "1")
            {
                result.btn_class = "event-btn2";
                result.event_div = " <span class='event-btn'>&nbsp;" + in_tree_no + " </span>&nbsp;" + f1_sign_no + " vs " + f2_sign_no;
                result.score_div = item1.getProperty("in_points", "0") + ":" + item2.getProperty("in_points", "0");
            }
            else
            {
                result.btn_class = "event-btn2";
                result.event_div = " <span class='event-btn'>&nbsp;" + in_tree_no + " </span>&nbsp;" + f2_sign_no + " vs " + f1_sign_no;
                result.score_div = item2.getProperty("in_points", "0") + ":" + item1.getProperty("in_points", "0");
            }

            return result;
        }

        private TDetail GetBattleInitial(List<Item> items)
        {
            TDetail result = new TDetail
            {
                event_div = "",
                score_div = "",
                btn_class = "",
            };

            if (items.Count < 2)
            {
                return result;
            }

            Item item1 = items[0];
            Item item2 = items[1];

            string in_tree_no = item1.getProperty("in_tree_no", "");
            string in_win_sign_no = item1.getProperty("in_win_sign_no", "");
            string in_sign_foot = item1.getProperty("in_sign_foot", "");

            string f1_sign_no = item1.getProperty("in_sign_no", "");
            string f2_sign_no = item2.getProperty("in_sign_no", "");

            if (f1_sign_no == in_win_sign_no) f1_sign_no += "(win)";
            if (f2_sign_no == in_win_sign_no) f2_sign_no += "(win)";

            if (in_sign_foot == "1")
            {
                result.event_div = f1_sign_no + " vs " + f2_sign_no;
                result.score_div = in_tree_no;
            }
            else
            {
                result.event_div = f2_sign_no + " vs " + f1_sign_no;
                result.score_div = in_tree_no;
            }

            return result;
        }

        private string GetCycleSignNoInfo(Item item)
        {
            return item.getProperty("in_sign_no", "");
        }

        private string GetCycleHead(Item item)
        {
            return item.getProperty("in_show_no", "");
        }

        private string GetCycleBody(Item item)
        {
            string no = item.getProperty("in_show_no", "");
            string nm = item.getProperty("in_name", "");
            string og = item.getProperty("in_current_org", "");

            return ""
                + "<small class='label bg-yellow' style='margin-right: 3px; margin-left: 3px'> " + no + "</small>"
                + nm
                + "<BR>"
                + og
                ;
        }

        /// <summary>
        /// 轉換為字典
        /// </summary>
        private Dictionary<string, Item> ConvertMap(TConfig cfg, Item items, string property)
        {
            var map = new Dictionary<string, Item>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string key = item.getProperty(property, "");

                if (map.ContainsKey(key))
                {
                    //throw new Exception("鍵值重覆");
                }
                else
                {
                    map.Add(key, item);
                }
            }
            return map;
        }
        /// <summary>
        /// 轉換為字典
        /// </summary>
        private Dictionary<string, List<Item>> ConvertMap2(TConfig cfg, Item items, string property)
        {
            var map = new Dictionary<string, List<Item>>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string key = item.getProperty(property, "");

                List<Item> list = null;
                if (map.ContainsKey(key))
                {
                    list = map[key];
                }
                else
                {
                    list = new List<Item>();
                    map.Add(key, list);
                }
                list.Add(item);
            }

            return map;
        }

        /// <summary>
        /// 找出項目
        /// </summary>
        private Item FindItem(TConfig cfg, Dictionary<string, Item> map, string key)
        {
            if (map.ContainsKey(key))
            {
                return map[key];
            }
            else
            {
                return cfg.inn.newItem();
            }
        }

        /// <summary>
        /// 找出項目清單
        /// </summary>
        private List<Item> FindItems(TConfig cfg, Dictionary<string, List<Item>> map, string key)
        {
            if (map.ContainsKey(key))
            {
                return map[key];
            }
            else
            {
                List<Item> list = new List<Item>();
                list.Add(cfg.inn.newItem());
                list.Add(cfg.inn.newItem());
                return list;
            }
        }

        /// <summary>
        /// 取得單循環對戰鍵值
        /// </summary>
        private string GetSingleCycleKey(int y, int x)
        {
            if (y < x)
            {
                return y + "," + x;
            }
            else
            {
                return x + "," + y;
            }
        }

        /// <summary>
        /// 取得雙循環對戰鍵值
        /// </summary>
        private string GetDoubleCycleKey(int y, int x)
        {
            return y + "," + x;
        }

        //取得選手
        private Item GetPlayers(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.in_sign_no
                    , t1.in_section_no
                    , t1.in_check_result
                    , t1.in_current_org
                    , t1.map_short_org
                    , t1.in_name
                    , t1.in_sno
                    , t1.in_weight_value
                    , t1.in_final_rank
                    , t1.in_show_no
                    , t2.in_l1
                FROM
                    VU_MEETING_PTEAM t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                WHERE
                    t1.source_id = '{#program_id}'
                ORDER BY
                    CAST(ISNULL(t1.in_sign_no, '0') AS INT)
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //取得賽事組別場次
        private Item GetEventPlayers(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.id                   AS 'event_id'
                    , t1.in_tree_name
                    , t1.in_tree_id
                    , t1.in_tree_no
                    , t1.in_round
                    , t1.in_round_id
                    , t1.in_round_code
                    , t1.in_next_win
                    , t1.in_next_foot_win
                    , t1.in_next_lose
                    , t1.in_next_foot_lose
                    , t1.in_detail_ns
                    , t1.in_robin_key
                    , t1.in_win_status
                    , t1.in_win_time
                    , t1.in_win_sign_no
                    , t1.in_win_local_time
                    , t2.id                 AS 'detail_id'
                    , t2.in_sign_foot
                    , t2.in_sign_no
                    , t2.in_sign_bypass
                    , t2.in_target_no
                    , t2.in_status
                    , t2.in_points
                    , t2.in_points_type
                    , t2.in_correct_count
                    , t3.in_name            AS 'site_name'
                    , t11.id                AS 'mdid'
                    , t11.in_current_org
                    , t11.in_short_org
                    , t11.map_short_org
                    , t11.in_name
                    , t11.in_names
                    , t11.in_sno
                    , t11.in_weight_value
                    , t11.in_org_teams
                    , t11.in_section_no
                    , t11.in_show_no
                    , t11.in_seeds
                    , t11.in_check_result
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    IN_MEETING_SITE t3 WITH(NOLOCK)
                    ON t3.id = t1.in_site
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM t11 WITH(NOLOCK)
                    ON t11.source_id = t1.source_id
                    AND ISNULL(t11.in_sign_no, '') <> ''
                    AND t11.in_sign_no = t2.in_sign_no
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_name = 'main'
                ORDER BY
                    t1.in_tree_sort
                    , t1.in_round
                    , t1.in_round_id
                    , t2.in_sign_foot
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#program_id}", cfg.program_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private class TDetail
        {
            public string event_div { get; set; }
            public string score_div { get; set; }
            public string btn_class { get; set; }
        }

        private class TStatistics
        {
            /// <summary>
            /// 籤號
            /// </summary>
            public string in_sign_no { get; set; }

            /// <summary>
            /// 總積分
            /// </summary>
            public int total_points { get; set; }

            /// <summary>
            /// 總勝場
            /// </summary>
            public int total_wins { get; set; }
        }

        private class TCycleModel
        {
            public string program_id { get; set; }
            public string in_battle_type { get; set; }

            public bool is_double { get; set; }
            public Dictionary<string, Item> player_map { get; set; }
            public Dictionary<string, List<Item>> event_map { get; set; }
            public int team_count { get; set; }
            public string row_title { get; set; }
            public Item itmPlayers { get; set; }
            public Item itmEvents { get; set; }

            public string in_group_display { get; set; }
        }

        private class TEvent
        {
            public string Id { get; set; }
            public int TreeNo { get; set; }
            public Item Value { get; set; }
            public Item itmFoot1 { get; set; }
            public Item itmFoot2 { get; set; }
            public TFoot Foot1 { get; set; }
            public TFoot Foot2 { get; set; }
        }

        private TFoot MapFoot(Item item)
        {
            var result = new TFoot();

            string in_section_no = item.getProperty("in_section_no", "");
            string org = item.getProperty("map_short_org", "");
            string name = item.getProperty("in_name", "");
            string weight = item.getProperty("in_weight_value", "");
            string in_status = item.getProperty("in_status", "");
            string in_points = item.getProperty("in_points", "");
            string in_correct_count = item.getProperty("in_correct_count", "0");
            string in_win_time = item.getProperty("in_win_time", "");

            if (in_points.Length == 2)
            {
                result.score_i = in_points[0].ToString();
                result.score_w = in_points[1].ToString();
            }
            else if (in_points.Length == 1)
            {
                result.score_i = "";
                result.score_w = in_points;
            }
            else
            {
                result.score_i = "";
                result.score_w = "";
            }

            result.display_no = in_section_no;
            result.display_name = name + "<br>" + org;
            result.weight = weight;

            if (in_status == "1")
            {
                result.display_name = "<i class='fa fa-check winner_icon'></i>" + result.display_name;
            }
            if (weight != "")
            {
                result.display_name += "(" + weight + ")";
            }

            result.score_points = in_points;
            result.score_s = in_correct_count;

            if (in_win_time == "")
            {
                result.score_i = "";
                result.score_w = "";
                result.score_s = "";
                result.score_points = "";
            }

            return result;
        }

        private class TFoot
        {
            public string display_no { get; set; }
            public string display_name { get; set; }
            public string weight { get; set; }
            public string score_i { get; set; }
            public string score_w { get; set; }
            public string score_s { get; set; }
            public string score_points { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public bool is_draw { get; set; }
            public bool is_edit { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmColor { get; set; }
            public Item itmProgram { get; set; }

        }

        private int GetInt(string value, int def = 0)
        {
            if (value == "") return def;
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}