﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.TableTennis.Common
{
	public class FIX_CLEAR_RESUME : Item
	{
		public FIX_CLEAR_RESUME(IServerConnection arg) : base(arg) { }

		/// <summary>
		/// 編程啟動點 (Code 在此撰寫)
		/// </summary>
		public Item Run()
		{
			Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
			Aras.Server.Core.IContextState RequestState = CCO.RequestState;
			Innovator inn = this.getInnovator();

			string strDatabaseName = inn.getConnection().GetDatabaseName();
			string strMethodName = "[" + strDatabaseName + "]FIX_CLEAR_RESUME";

			Item itmR = this;

			string sql = "";

			sql = @"
				SELECT TOP 500
					t2.id
					, t2.in_name
					, t2.in_sno
					, t2.in_photo
				FROM 
					[IN_RESUME] t2  WITH(NOLOCK)
				WHERE 
					t2.login_name NOT IN 
					(
						  'A111111111'
						, 'A333333333'
						, 'A444444444'
						, 'A555555555'
						, 'A666666666'
						, 'esadmin'
						, 'ma000'
						, 'ma001'
						, 'ma002'
						, 'mu000'
						, 'mu001'
						, 'mu002'
						, 'mu003'
						, 'vadmin'
						, 'admin'
						, 'demo'
						, 'lwu001'
						, 'lwu002'
						, 'lwu003'
						, 'root'
					)
					AND LEN(in_sno) = 6
					AND ISNULL(in_is_teacher, 0) = 0
				ORDER BY
					t2.login_name
				";

			Item itmResumes = inn.applySQL(sql);

			int count = itmResumes.getItemCount();

			var log = new StringBuilder();
			log.AppendLine();
			log.AppendLine(System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " | FIX_CLEAR_RESUME.START: " + count);

			for (int i = 0; i < count; i++)
			{
				Item itmResume = itmResumes.getItemByIndex(i);
				string in_name = itmResume.getProperty("in_name", "");
				string in_sno = itmResume.getProperty("in_sno", "");
				string info = in_name + "(" + in_sno + ")";
				log.AppendLine(System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " |     - " + info);

				itmResume.setType("In_Resume");
				itmResume.apply("In_Delete_Resume");
			}

			log.AppendLine(System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " | FIX_CLEAR_RESUME.END.");

			CCO.Utilities.WriteDebug(strMethodName, log.ToString());

			return itmR;
		}
	}
}