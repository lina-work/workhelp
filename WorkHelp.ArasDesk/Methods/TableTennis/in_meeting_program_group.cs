﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.TableTennis.Common
{
    public class in_meeting_program_group : Item
    {
        public in_meeting_program_group(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 分組
                日期: 
                    - 2020-11-20: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            //Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_program_group";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                mode = itmR.getProperty("mode", ""),
            };

            if (cfg.meeting_id == "" || cfg.program_id == "")
            {
                throw new Exception("參數錯誤");
            }

            //檢查頁面權限
            Item itmPermission = cfg.inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermission.getProperty("isMeetingAdmin", "") == "1";
            if (!isMeetingAdmin)
            {
                itmR.setProperty("error_message", "您無權限瀏覽此頁面");
                return itmR;
            }

            cfg.itmMeeting = GetMeeting(cfg);
            if (cfg.itmMeeting.isError())
            {
                throw new Exception("賽事資料錯誤");
            }

            var itmProgram = GetProgram(cfg, cfg.program_id);
            if (itmProgram.isError())
            {
                throw new Exception("賽程組別資料錯誤");
            }

            switch (cfg.mode)
            {
                case "":
                    //附加賽事資訊
                    AppendMeeting(cfg, itmR);
                    //附加組別資訊
                    AppendProgram(cfg, itmProgram, itmR);
                    //附加分組資訊
                    AppendChildrenPrograms(cfg, itmProgram, itmR);
                    break;

                case "allocate": //自動分組
                    //清除資料
                    RemoveEvents(cfg, cfg.program_id, in_group_tag: "");
                    //進行分組
                    Allocate(cfg, itmR);
                    break;

                case "team": //附加隊伍
                    AppendProgramTeams(cfg, itmProgram, itmR);
                    break;

                case "assign": //指定分組
                    //清除資料
                    RemoveEvents(cfg, cfg.program_id, in_group_tag: "");
                    //進行分組
                    Assign(cfg, itmR);
                    break;

                case "straws"://抽籤
                    //產生 excel
                    RunXlsx(cfg, itmProgram, itmR);
                    //整組抽籤
                    RunDraw(cfg, itmProgram, itmR);
                    break;

                case "event": //建立場次
                    GenerateEvents(cfg, itmProgram, itmR);
                    break;

                case "tiebreaker": //加賽
                    RunAddTiebreaker(cfg, itmProgram, itmR);
                    break;

                default:
                    break;
            }


            return itmR;
        }

        private void RunAddTiebreaker(TConfig cfg, Item itmProgram, Item itmReturn)
        {
            //加賽類型
            string in_tiebreaker = itmProgram.getProperty("in_tiebreaker", "");

            //過濾
            string[] filter = new string[]
            {
                "",
                "請選擇",
                "None",
                "加賽類型",
            };

            if (filter.Contains(in_tiebreaker)) return;

            //1. 刪除舊資料
            RemoveEvents(cfg, itmProgram.getProperty("id", ""), in_group_tag: "tiebreaker");
            //2. 新增加賽
            AddTiebreaker(cfg, itmProgram, itmReturn);
            //3. 產生 excel
            RunXlsx(cfg, itmProgram, itmReturn);
            //4. 整組抽籤
            RunDraw(cfg, itmProgram, itmReturn);
            //5. 建立場次
            GenerateEvents(cfg, itmProgram, itmReturn);
            //6. 修正呈現選手序號
            FixPlayerShowNumber(cfg, itmProgram, itmReturn);
        }

        //修正呈現選手序號
        private void FixPlayerShowNumber(TConfig cfg, Item itmProgram, Item itmReturn)
        {
            string program_id = itmProgram.getProperty("id", "");

            string sql = @"
                UPDATE t2 SET
	                t2.in_show_no = t2.in_show_tag
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PTEAM t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t1.in_program = '{#program_id}'
	                AND in_group_tag = 'tiebreaker'
            ";

            sql = sql.Replace("{#program_id}", program_id);

            cfg.inn.applySQL(sql);

        }
        #region 指定分組

        //附加隊伍
        private void AppendProgramTeams(TConfig cfg, Item itmProgram, Item itmReturn)
        {
            var program_id = itmProgram.getProperty("id", "");
            var team_count = GetInt(itmProgram.getProperty("in_team_count", ""));

            //更新組別的分組設定
            UpdateProgram(cfg, program_id, itmReturn);

            var group_team = GetInt(itmReturn.getProperty("group_team", ""));
            var group_code = itmReturn.getProperty("group_code", "");

            //分組編碼
            var is_english = group_code.Contains("A");
            //分組模型
            var dv = RunDivide(team_count, group_team);
            //分成幾組 10/3 => 分成 4 組
            var group_count = dv.IsDivisible ? dv.Quotient : dv.Quotient + 1;
            //{ "A", "B", "C", "D" }
            var codes = GetCodeArray(group_count, group_code, is_english);
            //最後一組的索引
            var group_max_idx = group_count - 1;

            //取得組別 Team
            Item itmTeams = GetProgramTeams(cfg, program_id);
            int count = itmTeams.getItemCount();
            if (itmTeams.isError() || count <= 0)
            {
                return;
            }

            List<Item> rows = new List<Item>();
            List<Item> unassigneds = new List<Item>();

            for (int i = 0; i < count; i++)
            {
                Item itmTeam = itmTeams.getItemByIndex(i);

                var code = "";
                var index = i / group_team;
                var rmndr = (i + 1) % group_team;
                if (dv.IsDivisible)
                {
                    code = codes[index];
                }
                else
                {
                    if (rmndr > 0 && index == group_max_idx)
                    {
                        code = "";
                    }
                    else
                    {
                        code = codes[index];
                    }
                }

                itmTeam.setType("inn_team");
                itmTeam.setProperty("in_group_display", code);
                rows.Add(itmTeam);

                if (code == "")
                {
                    //無法整除的最後幾組
                    unassigneds.Add(itmTeam);
                }
            }

            var max_idx = codes.Length - 1 - 1;
            for (var i = unassigneds.Count - 1; i >= 0; i--)
            {
                var code = codes[max_idx];
                var itmTeam = unassigneds[i];
                itmTeam.setProperty("in_group_display", code);
                max_idx--;
            }

            foreach (var row in rows)
            {
                var in_group_display = row.getProperty("in_group_display", "");
                row.setProperty("in_group_code", GetGroupDisplayCode(in_group_display));
                itmReturn.addRelationship(row);
            }

            Item itmEmptyGroupCode = cfg.inn.newItem();
            itmEmptyGroupCode.setType("inn_group_code");
            itmEmptyGroupCode.setProperty("label", "指定分組");
            itmEmptyGroupCode.setProperty("value", "");
            itmEmptyGroupCode.setProperty("in_selected", "selected");
            itmReturn.addRelationship(itmEmptyGroupCode);

            for (int i = 0; i < codes.Length; i++)
            {
                string code = codes[i];
                string group_display = is_english ? code + " 組" : "第 " + code + " 組";

                Item itmGroup = cfg.inn.newItem();
                itmGroup.setType("inn_group_code");
                itmGroup.setProperty("label", group_display);
                itmGroup.setProperty("value", (i + 1).ToString());
                itmReturn.addRelationship(itmGroup);
            }
        }

        private string GetGroupDisplayCode(string value)
        {
            switch (value)
            {
                case "A": return "1";
                case "B": return "2";
                case "C": return "3";
                case "D": return "4";
                case "E": return "5";
                case "F": return "6";
                case "G": return "7";
                case "H": return "8";
                case "I": return "9";
                case "J": return "10";
                case "K": return "11";
                case "L": return "12";
                case "M": return "13";
                case "N": return "14";
                case "O": return "15";
                case "P": return "16";
                case "Q": return "17";
                case "R": return "18";
                case "S": return "19";
                case "T": return "20";
                case "U": return "21";
                case "V": return "22";
                case "W": return "23";
                case "X": return "24";
                case "Y": return "25";
                case "Z": return "26";
                default: return "0";
            }
        }
        #endregion 指定分組

        #region 加賽

        //新增加賽
        private void AddTiebreaker(TConfig cfg, Item itmProgram, Item itmReturn)
        {
            //轉換賽程組別
            TProgram entity = MapProgram(itmProgram);

            //取得加賽隊伍
            Item itmTiebreakerTeams = GetTiebreakerTeams(cfg, entity.id, "partition", entity.rank_ns);
            if (itmTiebreakerTeams.isError() || itmTiebreakerTeams.getItemCount() <= 0)
            {
                throw new Exception("匹配加賽隊伍發生錯誤");
            }

            //新增加賽組別
            AddTiebreakerProgram(cfg, entity, itmTiebreakerTeams);

            //取得組別 Children
            Item itmChildrenPrograms = GetChildrenPrograms(cfg, entity.id, in_group_tag: "tiebreaker");
            if (itmChildrenPrograms.isError() || itmChildrenPrograms.getItemCount() != 1)
            {
                throw new Exception("建立加賽組別發生錯誤");
            }

            Item itmChildrenProgram = itmChildrenPrograms.getItemByIndex(0);


            //新增加賽隊伍
            int team_count = itmTiebreakerTeams.getItemCount();
            for (int i = 0; i < team_count; i++)
            {
                var itmTeam = itmTiebreakerTeams.getItemByIndex(i);
                var team_idx = i + 1;
                var in_part_id = team_idx.ToString();
                var in_part_code = team_idx.ToString();

                AddProgramTeam(cfg, itmChildrenProgram, itmTeam, in_part_id, in_part_code);
            }
        }

        //新增加賽
        private void AddTiebreakerProgram(TConfig cfg, TProgram entity, Item itmTeams)
        {
            //計算加賽組別參賽人數
            int team_count = itmTeams.getItemCount(); //entity.group_count * entity.group_rank;

            int[] rs = GetRoundAndSum(team_count, 2, 2, 1);
            string round_count = rs[0].ToString();
            string round_code = rs[1].ToString();

            Item itmTiebreakerProgram = GetNewItem(cfg, entity, 1, true);
            itmTiebreakerProgram.setProperty("in_name", entity.in_name + "-" + "加賽");
            itmTiebreakerProgram.setProperty("in_display", entity.in_display + "-" + "加賽");

            if (entity.in_short_name != "")
            {
                itmTiebreakerProgram.setProperty("in_short_name", entity.in_short_name + "-" + "加賽");
            }

            itmTiebreakerProgram.setProperty("in_battle_type", entity.tiebreaker_battle_type);

            itmTiebreakerProgram.setProperty("in_group_type", "B"); //分類排序用
            itmTiebreakerProgram.setProperty("in_group_tag", "tiebreaker");
            itmTiebreakerProgram.setProperty("in_group_id", "1");
            itmTiebreakerProgram.setProperty("in_group_display", "加賽");

            itmTiebreakerProgram.setProperty("in_team_count", team_count.ToString());
            itmTiebreakerProgram.setProperty("in_round_code", round_code);
            itmTiebreakerProgram.setProperty("in_round_count", round_count);

            itmTiebreakerProgram = itmTiebreakerProgram.apply("add");

            if (itmTiebreakerProgram.isError())
            {
                throw new Exception("新增加賽發生錯誤");
            }
        }

        #endregion

        //建立場次(迴圈執行)
        private void GenerateEvents(TConfig cfg, Item itmProgram, Item itmReturn)
        {
            string mode = itmReturn.getProperty("mode", "");
            string in_group_tag = mode == "tiebreaker" ? "tiebreaker" : "partition";

            string meeting_id = itmProgram.getProperty("in_meeting", "");
            string program_id = itmProgram.getProperty("id", "");

            Item itmChildrenPrograms = GetChildrenPrograms(cfg, program_id, in_group_tag: in_group_tag);

            int count = itmChildrenPrograms.getItemCount();

            if (count <= 0)
            {
                return;
            }

            for (int i = 0; i < count; i++)
            {
                Item itmChildrenProgram = itmChildrenPrograms.getItemByIndex(i);
                string children_program = itmChildrenProgram.getProperty("id", "");

                itmChildrenProgram.setType("In_Meeting_Program");
                itmChildrenProgram.setProperty("meeting_id", meeting_id);
                itmChildrenProgram.setProperty("program_id", children_program);
                itmChildrenProgram.setProperty("mode", "event");

                Item itmMethodResult = itmChildrenProgram.apply("in_meeting_program");

                if (itmMethodResult.isError())
                {
                    throw new Exception("場次建立失敗");
                }
            }
        }

        #region 抽籤

        //產生 excel
        private void RunXlsx(TConfig cfg, Item itmProgram, Item itmReturn)
        {
            string mode = itmReturn.getProperty("mode", "");
            string in_group_tag = mode == "tiebreaker" ? "tiebreaker" : "partition";

            string program_id = itmProgram.getProperty("id", "");

            Item itmTeams = GetAllChildrenTeams(cfg, program_id, in_group_tag);

            //建立籤表 XLS
            GenerateExcel(cfg, itmTeams, itmReturn);

            //更新賽程狀態
            string in_draw_file = itmReturn.getProperty("in_draw_file", "");

            //更新到 in_meeting
            string sql = "UPDATE IN_MEETING SET in_draw_file = N'" + in_draw_file + "' WHERE id = '" + cfg.meeting_id + "'";

            cfg.inn.applySQL(sql);
        }

        //整組抽籤(迴圈執行)
        private void RunDraw(TConfig cfg, Item itmProgram, Item itmReturn)
        {
            string mode = itmReturn.getProperty("mode", "");
            string in_group_tag = mode == "tiebreaker" ? "tiebreaker" : "partition";
            string draw_sameteam = mode == "tiebreaker" ? "Y" : "N";

            string meeting_id = itmProgram.getProperty("in_meeting", "");
            string program_id = itmProgram.getProperty("id", "");

            Item itmChildrenPrograms = GetChildrenPrograms(cfg, program_id, in_group_tag: in_group_tag);

            int count = itmChildrenPrograms.getItemCount();

            if (count <= 0)
            {
                return;
            }

            for (int i = 0; i < count; i++)
            {
                Item itmChildrenProgram = itmChildrenPrograms.getItemByIndex(i);
                string children_program = itmChildrenProgram.getProperty("id", "");

                itmChildrenProgram.setType("In_Meeting_Program");
                itmChildrenProgram.setProperty("draw_type", "2");     //組別抽籤
                itmChildrenProgram.setProperty("draw_integral", "Y"); //種子籤
                itmChildrenProgram.setProperty("draw_99", "N");       //99號籤
                itmChildrenProgram.setProperty("draw_sameteam", draw_sameteam); //同隊分面

                itmChildrenProgram.setProperty("meeting_id", meeting_id);
                itmChildrenProgram.setProperty("program_id", children_program);
                itmChildrenProgram.setProperty("program_name", itmProgram.getProperty("in_name", ""));
                itmChildrenProgram.setProperty("program_name2", itmProgram.getProperty("in_name2", ""));
                itmChildrenProgram.setProperty("program_name3", itmProgram.getProperty("in_name3", ""));
                itmChildrenProgram.setProperty("team_count", itmProgram.getProperty("in_team_count", ""));

                Item itmMethodResult = itmChildrenProgram.apply("in_meeting_draw_run");

                if (itmMethodResult.isError())
                {
                    throw new Exception("抽籤失敗");
                }
            }
        }

        /// <summary>
        /// 建立籤表 XLS (xlsx)
        /// </summary>
        private void GenerateExcel(TConfig cfg, Item itmRows, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "").Trim();

            Item itmPath = GetExcelPath(cfg, "draw_path");

            string export_path = itmPath.getProperty("export_path", "");
            string folder_path = export_path.TrimEnd('\\') + "\\" + meeting_id + "\\";
            if (!System.IO.Directory.Exists(folder_path))
            {
                System.IO.Directory.CreateDirectory(folder_path);
            }

            string ext_name = ".xlsx";
            string xls_name = DateTime.Now.ToString("yyyyMMdd_HHmmss_fff");
            string xls_file = folder_path + xls_name + ext_name;
            string xls_url = meeting_id + "/" + xls_name + ext_name;
            string sheet_name = "sheet1";

            string in_draw_file = xls_name + ext_name;

            ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook();
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheets.Add(sheet_name);

            //lina 2020.08.04: 與 亂數抽籤 DLL 串接，標題列為欄位名稱，不得改簡體中文
            string[] titles = new string[]
            {
                "選手編號",
                "單位",
                "代號",
                "組別",
                "姓名",
                "籤號",
                "順序",
                "積分",
            };

            string[] fields = new string[]
            {
                "in_team_index",
                "in_current_org",
                "in_alias",
                "program_name3",
                "in_names",
                "in_sign_no",
                "in_sort_order",
                "in_points"
            };

            string[] formats = new string[]
            {
                "", //"選手編號",
                "", //"單位",
                "", //"代號",
                "", //"組別",
                "", //"姓名",
                "int", //"籤號",
                "int", //"順序",
                "int", //"積分",
            };

            int wsRow = 1;
            int wsCol = 1;
            int field_count = fields.Length;
            int raw_count = itmRows.getItemCount();
            int last_index = raw_count - 1;

            for (int i = 0; i < field_count; i++)
            {
                string title = titles[i];

                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol);
                cell.Value = title;
                cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                cell.Style.Font.Bold = true;
                cell.DataType = ClosedXML.Excel.XLDataType.Text;
                wsCol++;
            }

            for (int i = 0; i < raw_count; i++)
            {
                wsRow++;
                wsCol = 1;

                Item itmRow = itmRows.getItemByIndex(i);

                for (int j = 0; j < field_count; j++)
                {
                    string field_name = fields[j];
                    string field_format = formats[j];

                    string value = "";

                    value = itmRow.getProperty(field_name, "");

                    if (field_name == "in_sign_no")
                    {
                        value = "0";
                    }
                    else if (field_name == "in_points" && value == "")
                    {
                        value = "0";
                    }
                    else if (field_name == "in_sort_order" && value == "")
                    {
                        value = "0";
                    }

                    ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol);

                    if (value != "")
                    {
                        switch (field_format)
                        {
                            case "short_date":
                                cell.Value = value;
                                cell.Style.NumberFormat.Format = "yyyy/mm/dd";
                                cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                                break;
                            case "short_date_time":
                                cell.Value = value;
                                cell.Style.NumberFormat.Format = "yyyy/mm/dd HH:mm";
                                cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                                break;
                            case "int":
                                cell.Value = value;
                                break;
                            default:
                                SetCellTextValue(ref cell, value);
                                break;
                        }
                    }
                    else
                    {
                        cell.Value = value;
                    }

                    //自動伸縮欄寬
                    if (i == last_index)
                    {
                        sheet.Column(wsCol).AdjustToContents();
                        //sheet.Column(wsCol).Width += 1;
                    }

                    wsCol++;
                }
            }

            //寫入檔案
            workbook.SaveAs(xls_file);

            //輸出檔案路徑
            itmReturn.setProperty("xls_name", xls_url);
            itmReturn.setProperty("in_draw_file", in_draw_file);
        }

        private void SetCellTextValue(ref ClosedXML.Excel.IXLCell cell, string textValue)
        {
            DateTime dtTmp;
            double dblTmp;

            if (DateTime.TryParse(textValue, out dtTmp))
            {
                cell.Value = "'" + textValue;
            }
            else if (double.TryParse(textValue, out dblTmp))
            {
                cell.Value = "'" + textValue;
            }
            else
            {
                cell.Value = textValue;
            }
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
        }

        private Item GetExcelPath(TConfig cfg, string in_name)
        {
            Item itmResult = cfg.inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
                SELECT 
                    TOP 1 
                    t2.in_name AS 'variable'
                    , t1.in_name
                    , t1.in_value 
                FROM 
                    In_Variable_Detail t1 WITH(NOLOCK)
                INNER JOIN 
                    In_Variable t2 WITH(NOLOCK) ON t2.ID = t1.SOURCE_ID 
                WHERE 
                    t2.in_name = N'meeting_excel'  
                    AND t1.in_name = N'{#in_name}'
            ";

            sql = sql.Replace("{#in_name}", in_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError())
            {
                throw new Exception("未設定匯出路徑 _# " + in_name);
            }

            itmResult.setProperty("export_path", item.getProperty("in_value", ""));

            return itmResult;
        }
        #endregion

        //附加賽事資訊
        private void AppendMeeting(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));
        }

        //附加組別資訊
        private void AppendProgram(TConfig cfg, Item itmProgram, Item itmReturn)
        {
            itmReturn.setProperty("program_name", itmProgram.getProperty("in_name", ""));
            itmReturn.setProperty("program_name2", itmProgram.getProperty("in_name2", ""));
            itmReturn.setProperty("program_name3", itmProgram.getProperty("in_name3", ""));
            itmReturn.setProperty("program_display", itmProgram.getProperty("in_display", ""));
            itmReturn.setProperty("program_battle_type", itmProgram.getProperty("program_battle_type", ""));

            itmReturn.setProperty("in_team_count", itmProgram.getProperty("in_team_count", ""));

            itmReturn.setProperty("in_battle_type", itmProgram.getProperty("in_battle_type", ""));
            itmReturn.setProperty("in_rank_type", itmProgram.getProperty("in_rank_type", ""));
            itmReturn.setProperty("in_group_team", itmProgram.getProperty("in_group_team", ""));
            itmReturn.setProperty("in_group_code", itmProgram.getProperty("in_group_code", ""));
            itmReturn.setProperty("in_group_rank", itmProgram.getProperty("in_group_rank", ""));
            itmReturn.setProperty("in_group_mode", itmProgram.getProperty("in_group_mode", ""));
            itmReturn.setProperty("in_tiebreaker", itmProgram.getProperty("in_tiebreaker", ""));
        }

        //附加分組資訊
        private void AppendChildrenPrograms(TConfig cfg, Item itmProgram, Item itmReturn)
        {
            string program_id = itmProgram.getProperty("id", "");

            Item itmChildrenPrograms = GetChildrenPrograms(cfg, program_id);
            int count = itmChildrenPrograms.getItemCount();

            if (count <= 0)
            {
                return;
            }

            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < count; i++)
            {
                Item itmChildrenProgram = itmChildrenPrograms.getItemByIndex(i);
                string children_program = itmChildrenProgram.getProperty("id", "");

                Item itmChildrenTeams = GetProgramTeams(cfg, children_program, is_children: true);

                AppendChildrenTable(cfg, builder, itmChildrenProgram, itmChildrenTeams);
            }

            itmReturn.setProperty("inn_tables", builder.ToString());
        }

        //附加分組 Table
        private void AppendChildrenTable(TConfig cfg, StringBuilder builder, Item itmProgram, Item itmTeams)
        {
            string program_id = itmProgram.getProperty("id", "");
            string table_name = "tb_" + program_id;
            string in_group_display = itmProgram.getProperty("in_group_display", "");

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            head.AppendLine("<tr>");
            head.AppendLine("<th class='text-center'>選手編號</th>");
            head.AppendLine("<th class='text-center'>單位簡稱</th>");
            head.AppendLine("<th class='text-center'>姓名(隊別)</th>");
            head.AppendLine("<th class='text-center'>分組編號</th>");
            head.AppendLine("<th class='text-center'>籤號</th>");
            head.AppendLine("<th class='text-center'>名次</th>");
            head.AppendLine("</tr>");
            head.AppendLine("</thead>");

            int count = itmTeams.getItemCount();

            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item itmTeam = itmTeams.getItemByIndex(i);
                string team_id = itmTeam.getProperty("id", "");
                string in_show_no = itmTeam.getProperty("in_show_no", "");

                body.AppendLine("<tr>");
                body.AppendLine("<td class='text-center'>" + itmTeam.getProperty("in_team_index", "") + "</td>");
                body.AppendLine("<td class='text-left'>" + itmTeam.getProperty("map_org_name", "") + "</td>");
                body.AppendLine("<td class='text-left'>" + itmTeam.getProperty("in_name", "") + "</td>");
                body.AppendLine("<td class='text-center'>" + itmTeam.getProperty("in_part_code", "") + "</td>");
                body.AppendLine("<td class='text-center'>" + SignNoInput(program_id, team_id, in_show_no) + "</td>");
                body.AppendLine("<td class='text-center'>" + itmTeam.getProperty("in_final_rank", "") + "</td>");
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");


            builder.AppendLine("<div class='box'>");
            builder.AppendLine("  <div class='box-header with-border'>");
            builder.AppendLine("    <h3 class='box-title'>");
            builder.AppendLine(in_group_display);
            //builder.AppendLine(in_group_display + " " + program_id);
            builder.AppendLine("    </h3>");
            builder.AppendLine("  </div>");

            builder.AppendLine("  <div class='box-body'>");
            builder.AppendLine("    <table id='" + table_name + "' class='table table-hover table-bordered table-rwd rwd rwdtable match-table'>");
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("    </table>");
            builder.AppendLine("  </div>");
            builder.AppendLine("</div>");
        }

        private string SignNoInput(string program_id, string team_id, string no)
        {
            return "<label style='color: red'>" + no + "</label>";
            // return "<input type='text' class='text' style='text-align: right; width: 60px; color: red; '  "
            //     + " value='" + sign_no + "'"
            //     + " onkeyup=\"doUpdateNumber('" + program_id + "', '" + team_id + "', this)\"  />";
        }

        /// <summary>
        /// 清除場次資料
        /// </summary>
        private void RemoveEvents(TConfig cfg, string program_id, string in_group_tag = "")
        {
            Item itmData = cfg.inn.newItem("In_Meeting_Program");
            itmData.setProperty("program_id", program_id);
            itmData.setProperty("in_group_tag", in_group_tag);
            itmData.apply("in_meeting_program_remove_all");
        }

        /// <summary>
        /// 轉換指定分組資料
        /// </summary>
        private TAssign MapAssign(TConfig cfg, Item itmReturn)
        {
            var assign = new TAssign
            {
                Map = new Dictionary<string, TBox>(),
                IsError = true,
            };

            try
            {
                var teams = itmReturn.getProperty("teams", "");

                if (string.IsNullOrEmpty(teams))
                {
                    return assign;
                }

                //這行會避免json轉換為XML十日期被轉換格式。
                Newtonsoft.Json.JsonConvert.DefaultSettings = () => new Newtonsoft.Json.JsonSerializerSettings
                {
                    DateParseHandling = Newtonsoft.Json.DateParseHandling.None
                };

                var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Newtonsoft.Json.Linq.JObject>>(teams);
                var jobj_first = list.FirstOrDefault();

                if (jobj_first == null)
                {
                    return assign;
                }

                assign.MeetingId = GetJsonValue(jobj_first, "meeting_id");
                assign.ProgramId = GetJsonValue(jobj_first, "program_id");
                assign.itmParent = cfg.inn.newItem("In_Meeting_Program");

                assign.itmParent.setProperty("meeting_id", assign.MeetingId);
                assign.itmParent.setProperty("program_id", assign.ProgramId);
                assign.itmParent.setProperty("group_team", GetJsonValue(jobj_first, "group_team"));
                assign.itmParent.setProperty("group_code", GetJsonValue(jobj_first, "group_code"));
                assign.itmParent.setProperty("group_rank", GetJsonValue(jobj_first, "group_rank"));
                assign.itmParent.setProperty("group_mode", GetJsonValue(jobj_first, "group_mode"));
                assign.itmParent.setProperty("tiebreaker", GetJsonValue(jobj_first, "tiebreaker"));

                foreach (var jobj in list)
                {
                    string in_group_id = GetJsonValue(jobj, "group_id");
                    string group_display = GetJsonValue(jobj, "group_display");
                    string team_id = GetJsonValue(jobj, "team_id");

                    TBox box = null;
                    if (assign.Map.ContainsKey(in_group_id))
                    {
                        box = assign.Map[in_group_id];
                    }
                    else
                    {
                        box = new TBox
                        {
                            Id = in_group_id,
                            No = GetInt(in_group_id),
                            Display = group_display,
                            Teams = new List<Item>(),
                            TeamIdList = new List<string>(),
                        };
                        assign.Map.Add(in_group_id, box);
                    }
                    box.TeamIdList.Add(team_id);
                }

                assign.IsError = false;
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "error: " + ex.Message);
            }

            return assign;
        }

        //指定分組
        private void Assign(TConfig cfg, Item itmReturn)
        {
            TAssign assign = MapAssign(cfg, itmReturn);

            if (assign.IsError)
            {
                throw new Exception("指定分組發生錯誤");
            }

            //更新組別的分組設定
            UpdateProgram(cfg, assign.ProgramId, assign.itmParent);

            //取得賽程組別
            Item itmProgram = GetProgram(cfg, assign.ProgramId);

            if (itmProgram.isError())
            {
                throw new Exception("賽程組別資料錯誤");
            }

            //轉換賽程組別
            TProgram entity = MapProgram(itmProgram);

            //新增分組
            foreach (var kv in assign.Map)
            {
                var box = kv.Value;

                int team_count = box.TeamIdList.Count;
                int[] rs = GetRoundAndSum(team_count, 2, 2, 1);
                string round_count = rs[0].ToString();
                string round_code = rs[1].ToString();

                Item item = GetNewItem(cfg, entity, box.No, false);
                item.setProperty("in_team_count", team_count.ToString());
                item.setProperty("in_round_code", round_code);
                item.setProperty("in_round_count", round_count);
                item.setProperty("in_group_type", "A"); //分類排序用
                item.setProperty("in_group_tag", "partition");
                item.setProperty("in_group_id", box.Id);
                item.setProperty("in_group_display", box.Display);
                item = item.apply("add");

                if (item.isError())
                {
                    throw new Exception("分組發生錯誤");
                }

                box.Value = item;
            }

            //取得組別 Team
            Item itmTeams = GetProgramTeams(cfg, assign.ProgramId);

            if (itmTeams.isError())
            {
                throw new Exception("取得組別 Team 發生錯誤");
            }

            var teams = GetItemList(itmTeams);

            foreach (var kv in assign.Map)
            {
                var box = kv.Value;
                var itmChildrenProgram = box.Value;

                MapProgramTeam(box, teams);

                for (int j = 0; j < box.Teams.Count; j++)
                {
                    var team_idx = j + 1;
                    var in_part_id = team_idx.ToString();
                    var in_part_code = box.Id + "-" + team_idx;
                    var itmTeam = box.Teams[j];

                    AddProgramTeam(cfg, itmChildrenProgram, itmTeam, in_part_id, in_part_code);
                }
            }
        }

        private void MapProgramTeam(TBox box, List<Item> itmTeams)
        {
            List<int> remove_idx_list = new List<int>();

            for (int i = 0; i < itmTeams.Count; i++)
            {
                Item itmTeam = itmTeams[i];
                string team_id = itmTeam.getProperty("id", "");

                if (box.TeamIdList.Contains(team_id))
                {
                    box.Teams.Add(itmTeam);
                    remove_idx_list.Add(i);
                }
            }

            for (int i = remove_idx_list.Count - 1; i >= 0; i--)
            {
                int idx = remove_idx_list[i];
                itmTeams.RemoveAt(idx);
            }
        }

        private List<Item> GetItemList(Item items)
        {
            List<Item> list = new List<Item>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                list.Add(items.getItemByIndex(i));
            }

            return list;
        }

        //進行分組
        private void Allocate(TConfig cfg, Item itmReturn)
        {
            string program_id = itmReturn.getProperty("program_id", "");

            //更新組別的分組設定
            UpdateProgram(cfg, program_id, itmReturn);

            //取得賽程組別
            Item itmProgram = GetProgram(cfg, program_id);

            if (itmProgram.isError())
            {
                throw new Exception("賽程組別資料錯誤");
            }

            //轉換賽程組別
            TProgram entity = MapProgram(itmProgram);

            string[] codes = GetCodeArray(entity.group_count, entity.in_group_code, entity.is_english);

            int end_idx = entity.group_count - 1;

            //新增分組
            for (int i = 0; i < entity.group_count; i++)
            {
                int no = i + 1;
                bool is_end_idx = i == end_idx;

                string group_code = codes[i];
                string group_display = entity.is_english ? group_code + " 組" : "第 " + group_code + " 組";

                Item item = GetNewItem(cfg, entity, no, is_end_idx);
                item.setProperty("in_group_type", "A"); //分類排序用
                item.setProperty("in_group_tag", "partition");
                item.setProperty("in_group_id", no.ToString());
                item.setProperty("in_group_display", group_display);
                item = item.apply("add");

                if (item.isError())
                {
                    throw new Exception("分組發生錯誤");
                }
            }

            //取得組別 Team
            Item itmTeams = GetProgramTeams(cfg, program_id);

            if (itmTeams.isError())
            {
                throw new Exception("取得組別 Team 發生錯誤");
            }

            //取得組別 Children
            Item itmChildrenPrograms = GetChildrenPrograms(cfg, program_id, in_group_tag: "partition");

            if (itmChildrenPrograms.isError())
            {
                throw new Exception("取得組別 Children 發生錯誤");
            }

            TAllocate allocate = MapAllcolate(itmTeams, itmChildrenPrograms);

            ////分組前
            //var order_orgs = allocate.Orgs.OrderByDescending(x => x.Teams.Count);
            //foreach (var org in order_orgs)
            //{
            //    CCO.Utilities.WriteDebug(strMethodName, "org: " + org.Name + ", team count: " + org.Teams.Count);
            //    foreach(var team in org.Teams)
            //    {
            //        CCO.Utilities.WriteDebug(strMethodName, "in_team_index: " + team.getProperty("in_team_index", "") + ", in_name: " + team.getProperty("in_name", ""));
            //    }
            //}

            //分組(遞迴)
            AllocateTeamRecursive(cfg, allocate);

            ////分組後
            //foreach (var box in allocate.Boxes)
            //{
            //    CCO.Utilities.WriteDebug(strMethodName, "分組: " + box.Name+ ", team count: " + box.Teams.Count);
            //    foreach (var team in box.Teams)
            //    {
            //        CCO.Utilities.WriteDebug(strMethodName, "in_team_index: " + team.getProperty("in_team_index", "") + ", in_name: " + team.getProperty("in_name", ""));
            //    }
            //}

            for (int i = 0; i < allocate.Boxes.Count; i++)
            {
                var box = allocate.Boxes[i];
                var box_idx = i + 1;
                var itmChildrenProgram = box.Value;

                for (int j = 0; j < box.Teams.Count; j++)
                {
                    var team_idx = j + 1;
                    var in_part_id = team_idx.ToString();
                    var in_part_code = box_idx + "-" + team_idx;
                    var itmTeam = box.Teams[j];

                    AddProgramTeam(cfg, itmChildrenProgram, itmTeam, in_part_id, in_part_code);
                }
            }
        }

        private void AddProgramTeam(TConfig cfg, Item itmProgram, Item itmTeamSource, string in_part_id, string in_part_code)
        {
            string meeting_id = itmProgram.getProperty("in_meeting", "");
            string program_id = itmProgram.getProperty("id", "");
            string in_show_no = itmTeamSource.getProperty("in_show_no", "");

            Item itmPTeam = cfg.inn.newItem("In_Meeting_PTeam", "add");

            itmPTeam.setProperty("in_meeting", meeting_id);
            itmPTeam.setProperty("source_id", program_id);

            itmPTeam.setProperty("in_stuff_b1", itmTeamSource.getProperty("in_stuff_b1", ""));
            itmPTeam.setProperty("in_group", itmTeamSource.getProperty("in_group", ""));
            itmPTeam.setProperty("in_current_org", itmTeamSource.getProperty("in_current_org", ""));
            itmPTeam.setProperty("in_short_org", itmTeamSource.getProperty("in_short_org", ""));

            itmPTeam.setProperty("in_index", itmTeamSource.getProperty("in_index", ""));
            itmPTeam.setProperty("in_team", itmTeamSource.getProperty("in_team", ""));
            itmPTeam.setProperty("in_team_index", itmTeamSource.getProperty("in_team_index", ""));
            itmPTeam.setProperty("in_team_players", itmTeamSource.getProperty("in_team_players", ""));
            itmPTeam.setProperty("in_creator_sno", itmTeamSource.getProperty("in_creator_sno", ""));
            itmPTeam.setProperty("in_creator_sno", itmTeamSource.getProperty("in_creator_sno", ""));

            itmPTeam.setProperty("in_name", itmTeamSource.getProperty("in_name", ""));
            itmPTeam.setProperty("in_sno", itmTeamSource.getProperty("in_sno", ""));
            itmPTeam.setProperty("in_names", itmTeamSource.getProperty("in_names", ""));

            itmPTeam.setProperty("in_part_id", in_part_id);
            itmPTeam.setProperty("in_part_code", in_part_code);

            itmPTeam.setProperty("in_is_sync", "0");

            itmPTeam.setProperty("in_sign_no", in_part_id);
            itmPTeam.setProperty("in_show_no", in_show_no);
            itmPTeam.setProperty("in_judo_no", in_part_id);
            itmPTeam.setProperty("in_serial_no", in_part_id);
            itmPTeam.setProperty("in_show_tag", in_show_no);

            itmPTeam = itmPTeam.apply();
        }

        //分組(遞迴)
        private void AllocateTeamRecursive(TConfig cfg, TAllocate allocate)
        {
            //是否還有未分配的單位隊伍
            var has_non_assign = allocate.Orgs.Any(x => x.Teams.Count > 0);

            if (!has_non_assign)
            {
                return;
            }

            //先排隊伍數量最多的單位
            var order_orgs = allocate.Orgs.OrderByDescending(x => x.Teams.Count);

            foreach (var org in order_orgs)
            {
                foreach (var box in allocate.Boxes)
                {
                    if (org.Teams.Count == 0)
                    {
                        break;
                    }

                    if (box.Capacity == 0)
                    {
                        continue;
                    }

                    if (org.HasScaned)
                    {
                        List<int> remove_idxes = new List<int>();
                        for (int i = 0; i < org.Teams.Count; i++)
                        {
                            if (box.Capacity == 0)
                            {
                                break;
                            }
                            else
                            {
                                var team = org.Teams[i];

                                box.Teams.Add(team);
                                box.Capacity--;

                                remove_idxes.Add(i);
                            }
                        }
                        for (int i = remove_idxes.Count - 1; i >= 0; i--)
                        {
                            var idx = remove_idxes[i];
                            org.Teams.RemoveAt(idx);
                        }
                    }
                    else
                    {
                        org.HasScaned = true;
                        if (org.Teams.Count > box.Capacity)
                        {
                            continue;
                        }
                        else
                        {
                            foreach (var team in org.Teams)
                            {
                                box.Teams.Add(team);
                                box.Capacity--;
                            }
                            for (int i = org.Teams.Count - 1; i >= 0; i--)
                            {
                                var idx = i;
                                org.Teams.RemoveAt(idx);
                            }
                        }
                    }

                }
            }

            AllocateTeamRecursive(cfg, allocate);
        }

        private TAllocate MapAllcolate(Item itmTeams, Item itmPrograms)
        {
            TAllocate allocate = new TAllocate
            {
                Orgs = new List<TOrg>(),
                Boxes = new List<TBox>()
            };

            int team_count = itmTeams.getItemCount();

            for (int i = 0; i < team_count; i++)
            {
                Item itmTeam = itmTeams.getItemByIndex(i);
                string org_name = itmTeam.getProperty("in_current_org", "");

                TOrg org = allocate.Orgs.Find(x => x.Name == org_name);
                if (org == null)
                {
                    org = new TOrg
                    {
                        Name = org_name,
                        Teams = new List<Item>(),
                    };
                    allocate.Orgs.Add(org);
                }
                org.Teams.Add(itmTeam);
            }

            int program_count = itmPrograms.getItemCount();
            for (int i = 0; i < program_count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                string program_id = itmProgram.getProperty("id", "");
                string program_name = itmProgram.getProperty("in_name", "");
                int group_team = GetInt(itmProgram.getProperty("in_group_team", ""));

                TBox box = new TBox
                {
                    Id = program_id,
                    Name = program_name,
                    Limit = group_team,
                    Capacity = group_team,
                    Value = itmProgram,
                    Teams = new List<Item>(),
                };

                allocate.Boxes.Add(box);
            }

            int team_sum = allocate.Orgs.Sum(x => x.Teams.Count);
            int box_cell_sum = allocate.Boxes.Sum(x => x.Capacity);

            if (team_sum > box_cell_sum)
            {
                throw new Exception("人數大於分組總人數");
            }

            return allocate;
        }

        private Item GetNewItem(TConfig cfg, TProgram entity, int no, bool is_end_idx)
        {
            Item item = cfg.inn.newItem("In_Meeting_Program");
            item.setProperty("in_meeting", entity.in_meeting);

            item.setProperty("in_l1", entity.in_l1);
            item.setProperty("in_l2", entity.in_l2);
            item.setProperty("in_l3", entity.in_l3);
            item.setProperty("in_l1_sort", entity.in_l1_sort);
            item.setProperty("in_l2_sort", entity.in_l2_sort);
            item.setProperty("in_l3_sort", entity.in_l3_sort);

            item.setProperty("in_name", entity.in_name + "-" + no);
            item.setProperty("in_name2", entity.in_name2 + "-" + no);
            item.setProperty("in_name3", entity.in_name3 + "-" + no);
            item.setProperty("in_display", entity.in_display + "-" + no);

            if (entity.in_short_name != "")
            {
                item.setProperty("in_short_name", entity.in_short_name + "-" + no);
            }

            item.setProperty("in_sort_order", (no * 100).ToString());
            item.setProperty("in_battle_type", entity.children_battle_type);
            item.setProperty("in_rank_type", entity.in_rank_type);

            int team_count = entity.group_team;

            if (is_end_idx)
            {
                team_count = entity.end_group_team;
            }

            int[] rs = GetRoundAndSum(team_count, 2, 2, 1);
            string round_count = rs[0].ToString();
            string round_code = rs[1].ToString();

            item.setProperty("in_team_count", team_count.ToString());
            item.setProperty("in_round_code", round_code);
            item.setProperty("in_round_count", round_count);
            item.setProperty("in_group_team", entity.group_team.ToString());
            item.setProperty("in_group_code", entity.in_group_code);
            item.setProperty("in_group_rank", entity.in_group_rank);
            item.setProperty("in_group_mode", entity.in_group_mode);

            item.setProperty("in_program", entity.id);

            return item;
        }

        //更新組別的分組設定
        private void UpdateProgram(TConfig cfg, string program_id, Item itmReturn)
        {
            string group_team = itmReturn.getProperty("group_team", "");
            string group_code = itmReturn.getProperty("group_code", "");
            string group_rank = itmReturn.getProperty("group_rank", "");
            string group_mode = itmReturn.getProperty("group_mode", "");
            string tiebreaker = itmReturn.getProperty("tiebreaker", "");

            string sql = @"
                UPDATE 
                    IN_MEETING_PROGRAM 
                SET
                    in_group_team = '{#in_group_team}'
                    , in_group_code = '{#in_group_code}'
                    , in_group_rank = '{#in_group_rank}'
                    , in_group_mode = '{#in_group_mode}'
                    , in_tiebreaker = '{#in_tiebreaker}'
                WHERE
                    id = '{#program_id}'
                ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_group_team}", group_team)
                .Replace("{#in_group_code}", group_code)
                .Replace("{#in_group_rank}", group_rank)
                .Replace("{#in_group_mode}", group_mode)
                .Replace("{#in_tiebreaker}", tiebreaker);

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("分組設定提交失敗");
            }
        }

        //取得賽事資訊
        private Item GetMeeting(TConfig cfg)
        {
            string aml = @"
                <AML>
                    <Item type='In_Meeting' action='get' id='{#meeting_id}' select='in_title,in_battle_type,in_rank_type,in_surface_code'>
                    </Item>
                </AML>
                ".Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applyAML(aml);
        }

        //取得賽程組別資訊
        private Item GetProgram(TConfig cfg, string program_id)
        {
            string sql = @"
                SELECT
                    t1.*
                    , t2.label AS 'program_battle_type'
                FROM
                    IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                (
                    SELECT
                        t12.value AS 'value'
                        , t12.label_zt AS 'label'
                    FROM
                        [LIST] t11 WITH(NOLOCK)
                    INNER JOIN
                        [VALUE] t12 WITH(NOLOCK)
                        ON t12.source_id = t11.id
                    WHERE
                        t11.name = N'In_Meeting_BattleType'
                ) t2
                    ON t1.in_battle_type = t2.value
                WHERE
                    t1.id = '{#program_id}'
                ";

            sql = sql.Replace("{#program_id}", program_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //取得賽程組別資訊
        private Item GetProgramTeams(TConfig cfg, string program_id, bool is_children = false)
        {
            string order_by = is_children ? "t1.in_part_id" : "TRY_CAST(t1.in_sign_no AS INT)";

            string sql = @"
                SELECT
                    t1.*
                FROM
                    VU_MEETING_PTEAM t1 WITH(NOLOCK)
                WHERE
                    t1.source_id = '{#program_id}'
                ORDER BY
                    {#order_by}
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#order_by}", order_by);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //取得 Children 組別資訊
        private Item GetChildrenPrograms(TConfig cfg, string program_id, string in_group_tag = "")
        {
            string group_condition = in_group_tag == "" ? "" : "AND t1.in_group_tag = N'" + in_group_tag + "'";

            string sql = @"
                SELECT
                    t1.*
                    , t2.label AS 'program_battle_type'
                FROM
                    IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                (
                    SELECT
                        t12.value AS 'value'
                        , t12.label_zt AS 'label'
                    FROM
                        [LIST] t11 WITH(NOLOCK)
                    INNER JOIN
                        [VALUE] t12 WITH(NOLOCK)
                        ON t12.source_id = t11.id
                    WHERE
                        t11.name = N'In_Meeting_BattleType'
                ) t2
                    ON t1.in_battle_type = t2.value
                WHERE
                    t1.in_program = '{#program_id}'
                    {#group_condition}
                ORDER BY
                    t1.in_group_type
                    , t1.in_group_id
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#group_condition}", group_condition);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //取得賽程組別資訊(抽籤)
        private Item GetAllChildrenTeams(TConfig cfg, string program_id, string in_group_tag)
        {
            string sql = @"
                SELECT
                   t2.id           AS 'program_id'
                   , t2.in_name    AS 'program_name'
                   , t2.in_name2    AS 'program_name2'
                   , t2.in_name3    AS 'program_name3'
                   , t2.in_display AS 'program_display'
                   , t1.*
                FROM
                    VU_MEETING_PTEAM t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                WHERE
                    t2.in_meeting = '{#meeting_id}'
                    AND t2.in_program = '{#program_id}'
                    AND t2.in_group_tag = N'{#in_group_tag}'
                ORDER BY
                    t2.in_group_id
                    , t1.in_part_id
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#program_id}", program_id)
                .Replace("{#in_group_tag}", in_group_tag);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //取得加賽隊伍
        private Item GetTiebreakerTeams(TConfig cfg, string program_id, string in_group_tag, string rank_ns)
        {
            string sql = @"
                SELECT
                    t1.*
                FROM
                    VU_MEETING_PTEAM t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                WHERE
                    t2.in_program = '{#program_id}'
                    AND t2.in_group_tag = '{#in_group_tag}'
                    AND t1.in_final_rank IN ({#rank_ns})
                ORDER BY
                    t2.in_sort_order
                    , t1.in_final_rank
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_group_tag}", in_group_tag)
                .Replace("{#rank_ns}", rank_ns);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private TProgram MapProgram(Item itmProgram)
        {
            TProgram entity = new TProgram
            {
                in_meeting = itmProgram.getProperty("in_meeting", ""),
                id = itmProgram.getProperty("id", ""),

                in_l1 = itmProgram.getProperty("in_l1", ""),
                in_l2 = itmProgram.getProperty("in_l2", ""),
                in_l3 = itmProgram.getProperty("in_l3", ""),
                in_l1_sort = itmProgram.getProperty("in_l1_sort", ""),
                in_l2_sort = itmProgram.getProperty("in_l2_sort", ""),
                in_l3_sort = itmProgram.getProperty("in_l3_sort", ""),

                in_name = itmProgram.getProperty("in_name", ""),
                in_name2 = itmProgram.getProperty("in_name2", ""),
                in_name3 = itmProgram.getProperty("in_name3", ""),
                in_display = itmProgram.getProperty("in_display", ""),
                in_short_name = itmProgram.getProperty("in_short_name", ""),

                in_battle_type = itmProgram.getProperty("in_battle_type", ""),
                in_rank_type = itmProgram.getProperty("in_rank_type", ""),
                in_team_count = itmProgram.getProperty("in_team_count", ""),
                in_round_code = itmProgram.getProperty("in_round_code", ""),

                in_group_team = itmProgram.getProperty("in_group_team", ""),
                in_group_code = itmProgram.getProperty("in_group_code", ""),
                in_group_rank = itmProgram.getProperty("in_group_rank", ""),
                in_group_mode = itmProgram.getProperty("in_group_mode", ""),
                tiebreaker_battle_type = itmProgram.getProperty("in_tiebreaker", ""),
            };

            entity.team_count = GetInt(entity.in_team_count);
            entity.group_team = GetInt(entity.in_group_team);
            entity.group_rank = GetInt(entity.in_group_rank);

            List<string> rank_arr = new List<string>();
            for (int i = 1; i <= entity.group_rank; i++)
            {
                rank_arr.Add("'" + i + "'");
            }
            entity.rank_ns = string.Join(", ", rank_arr);

            entity.is_english = entity.in_group_code.Contains("A");

            var dv = RunDivide(entity.team_count, entity.group_team);
            entity.group_count = dv.IsDivisible ? dv.Quotient : dv.Quotient + 1;
            entity.end_group_team = dv.IsDivisible ? entity.group_team : dv.Residue;

            switch (entity.in_battle_type)
            {
                case "GroupSRoundRobin":
                    entity.children_battle_type = "SingleRoundRobin";
                    break;

                case "GroupDRoundRobin":
                    entity.children_battle_type = "DoubleRoundRobin";
                    break;

                case "GroupTopTwo":
                    entity.children_battle_type = "TopTwo";
                    break;

                case "GroupJudoTopFour":
                    entity.children_battle_type = "JudoTopFour";
                    break;

                default:
                    break;
            }

            return entity;
        }

        private TDivide RunDivide(int dividend, int divisor)
        {
            return new TDivide
            {
                Dividend = dividend,
                Divisor = divisor,
                Quotient = dividend / divisor,
                Residue = dividend % divisor,
                IsDivisible = dividend % divisor == 0,
            };
        }

        private class TDivide
        {
            /// <summary>
            /// 被除數
            /// </summary>
            public int Dividend { get; set; }

            /// <summary>
            /// 除數
            /// </summary>
            public int Divisor { get; set; }

            /// <summary>
            /// 商
            /// </summary>
            public int Quotient { get; set; }

            /// <summary>
            /// 餘數
            /// </summary>
            public int Residue { get; set; }

            /// <summary>
            /// 是否整除
            /// </summary>
            public bool IsDivisible { get; set; }
        }

        private class TAssign
        {
            /// <summary>
            /// 賽事 Id
            /// </summary>
            public string MeetingId { get; set; }

            /// <summary>
            /// 組別 Id
            /// </summary>
            public string ProgramId { get; set; }

            /// <summary>
            /// parent program
            /// </summary>
            public Item itmParent { get; set; }

            /// <summary>
            /// children programs
            /// </summary>
            public Dictionary<string, TBox> Map { get; set; }

            public bool IsError { get; set; }
        }

        private class TProgram
        {
            /// <summary>
            /// meeting id
            /// </summary>
            public string in_meeting { get; set; }

            /// <summary>
            /// 組別 id
            /// </summary>
            public string id { get; set; }

            /// <summary>
            /// 第一階
            /// </summary>
            public string in_l1 { get; set; }

            /// <summary>
            /// 第二階
            /// </summary>
            public string in_l2 { get; set; }

            /// <summary>
            /// 第三階
            /// </summary>
            public string in_l3 { get; set; }

            /// <summary>
            /// 第一階排序
            /// </summary>
            public string in_l1_sort { get; set; }

            /// <summary>
            /// 第二階排序
            /// </summary>
            public string in_l2_sort { get; set; }

            /// <summary>
            /// 第三階排序
            /// </summary>
            public string in_l3_sort { get; set; }

            /// <summary>
            /// 賽事組別
            /// </summary>
            public string in_name { get; set; }

            /// <summary>
            /// 賽事組別(無kg)
            /// </summary>
            public string in_name2 { get; set; }

            /// <summary>
            /// 賽事組別(含kg)
            /// </summary>
            public string in_name3 { get; set; }

            /// <summary>
            /// 呈現組別
            /// </summary>
            public string in_display { get; set; }

            /// <summary>
            /// 組別簡稱
            /// </summary>
            public string in_short_name { get; set; }

            /// <summary>
            /// 賽制類型
            /// </summary>
            public string in_battle_type { get; set; }

            /// <summary>
            /// 名次類型
            /// </summary>
            public string in_rank_type { get; set; }

            /// <summary>
            /// 參賽隊數 (ex: 20)
            /// </summary>
            public string in_team_count { get; set; }

            /// <summary>
            /// 賽制人數 (ex: 32)
            /// </summary>
            public string in_round_code { get; set; }

            /// <summary>
            /// 分組人數
            /// </summary>
            public string in_group_team { get; set; }

            /// <summary>
            /// 分組編碼
            /// </summary>
            public string in_group_code { get; set; }

            /// <summary>
            /// 取前幾名
            /// </summary>
            public string in_group_rank { get; set; }

            /// <summary>
            /// 分組配置模式
            /// </summary>
            public string in_group_mode { get; set; }

            /// <summary>
            /// 參賽隊數
            /// </summary>
            public int team_count { get; set; }

            /// <summary>
            /// 分組人數
            /// </summary>
            public int group_team { get; set; }

            /// <summary>
            /// 組別數量
            /// </summary>
            public int group_count { get; set; }

            /// <summary>
            /// 取前幾名
            /// </summary>
            public int group_rank { get; set; }

            /// <summary>
            /// 最後一組人數
            /// </summary>
            public int end_group_team { get; set; }

            /// <summary>
            /// children 賽制
            /// </summary>
            public string children_battle_type { get; set; }

            /// <summary>
            /// children 賽制
            /// </summary>
            public string tiebreaker_battle_type { get; set; }

            /// <summary>
            /// children 賽制
            /// </summary>
            public string rank_ns { get; set; }

            /// <summary>
            /// 是否為英文配置
            /// </summary>
            public bool is_english { get; set; }
        }

        /// <summary>
        /// 分配器
        /// </summary>
        private class TAllocate
        {
            public List<TOrg> Orgs { get; set; }

            public List<TBox> Boxes { get; set; }
        }

        /// <summary>
        /// 單位隊伍
        /// </summary>
        private class TOrg
        {
            public string Name { get; set; }

            public bool HasScaned { get; set; }
            public List<Item> Teams { get; set; }
        }

        /// <summary>
        /// 分組箱
        /// </summary>
        private class TBox
        {
            public string Id { get; set; }

            public int No { get; set; }

            public string Name { get; set; }

            public string Display { get; set; }

            public int Limit { get; set; }

            public int Capacity { get; set; }

            public Item Value { get; set; }

            public List<Item> Teams { get; set; }

            public List<string> TeamIdList { get; set; }
        }

        /// <summary>
        /// 取得最大輪次代碼
        /// </summary>
        private static int[] GetRoundAndSum(int value, int code, int sum, int round)
        {
            if (value == 0)
            {
                return new int[] { 0, 0 };
            }
            else if (value > sum)
            {
                return GetRoundAndSum(value, code, code * sum, round + 1);
            }
            else
            {
                return new int[] { round, sum };
            }
        }

        /// <summary>
        /// 取得英數字編碼陣列
        /// </summary>
        private static string[] GetCodeArray(int count, string code, bool is_english)
        {
            //bool is_english = code.Contains("A");

            string[] result = new string[count];

            for (int i = 1; i <= count; i++)
            {
                if (is_english)
                {
                    result[i - 1] = ((char)(i + 64)).ToString();
                }
                else
                {
                    result[i - 1] = i.ToString();
                }
            }
            return result;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string mode { get; set; }

            public Item itmMeeting { get; set; }
        }

        private string GetJsonValue(Newtonsoft.Json.Linq.JObject obj, string key)
        {
            Newtonsoft.Json.Linq.JToken value;
            if (obj.TryGetValue(key, out value))
            {
                return value.ToString();
            }
            return "";
        }

        private int GetInt(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}