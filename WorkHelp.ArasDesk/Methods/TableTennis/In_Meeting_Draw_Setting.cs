﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.TableTennis.Common
{
    public class In_Meeting_Draw_Setting : Item
    {
        public In_Meeting_Draw_Setting(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 提供正取的報名清單
    日誌: 
        - 2022-02-10: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_Draw_Setting";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "edit":
                    Save(cfg, itmR);
                    break;

                case "page":
                    Page(cfg, itmR);
                    break;
            }

            return itmR;
        }

        //儲存
        private void Save(TConfig cfg, Item itmReturn)
        {
            var in_mode = itmReturn.getProperty("in_mode", "");
            var in_team_count = itmReturn.getProperty("in_team_count", "0");

            Item itmOld = cfg.inn.newItem("In_Meeting_DrawTeam", "merge");
            itmOld.setAttribute("where", "in_mode = '" + in_mode + "' AND in_team_count = " + in_team_count);
            itmOld.setProperty("in_mode", in_mode);
            itmOld.setProperty("in_team_count", in_team_count);
            itmOld.setProperty("in_fight_yn", itmReturn.getProperty("in_fight_yn", "0"));
            itmOld.setProperty("in_battle_type", itmReturn.getProperty("in_battle_type", ""));
            itmOld.setProperty("in_partition_yn", itmReturn.getProperty("in_partition_yn", "0"));
            itmOld.setProperty("in_partition_count", itmReturn.getProperty("in_partition_count", "0"));
            itmOld.setProperty("in_partition_team", itmReturn.getProperty("in_partition_team", "0"));
            itmOld = itmOld.apply();
        }

        //頁面
        private void Page(TConfig cfg, Item itmReturn)
        {
            var in_mode = itmReturn.getProperty("in_mode", "");
            var in_team_count = itmReturn.getProperty("in_team_count", "0");

            var sql = "SELECT TOP 1 * FROM In_Meeting_DrawTeam WITH(NOLOCK)"
                + " WHERE in_mode = '" + in_mode + "'"
                + " AND in_team_count = " + in_team_count;

            var item = cfg.inn.applySQL(sql);

            if (!item.isError() && item.getResult() != "")
            {
                item.setType("inn_draw_team");
                itmReturn.addRelationship(item);

                itmReturn.setProperty("inn_tables", GetTableContents(cfg, item));
            }
        }

        private string GetTableContents(TConfig cfg, Item item)
        {
            var contents = new StringBuilder();
            var team_count = GetInt(item.getProperty("in_team_count", "0"));
            var tbl_count = GetInt(item.getProperty("in_partition_count", "0"));
            var plr_count = GetInt(item.getProperty("in_partition_team", "0"));
            var evt_count = GetTotalEventCount(tbl_count, plr_count);

            for (var i = 0; i < tbl_count; i++)
            {
                var no = i + 1;

                var bx = new TBox
                {
                    no = no,
                    tb_id = "data_table_" + no,
                    tb_title = GetGroupName(no),
                    plr_count = plr_count,
                    evt_count = evt_count,
                    team_count = team_count,
                    ns = new List<int>(),
                };

                var s = i * plr_count;
                var e = (i + 1) * plr_count;
                for (var n = s; n < e; n++)
                {
                    bx.ns.Add(n + 1);
                }

                AppendTableContents(cfg, contents, bx);
            }
            return contents.ToString();
        }

        private void AppendTableContents(TConfig cfg, StringBuilder contents, TBox bx)
        {
            var head = new StringBuilder();
            var body = new StringBuilder();
            var evt_menu = GetEventMenu(bx.evt_count);
            var no_menu = GetPlayerMenu(bx.team_count);

            head.Append("<th class='text-center'>籤號</th>");
            for (var i = 0; i < bx.ns.Count; i++)
            {
                var no = bx.ns[i];
                head.Append("<th class='text-center inn-head-" + no + "'> - </th>");
            }

            for (var i = 0; i < bx.ns.Count; i++)
            {
                var rn = bx.ns[i];
                body.Append("<tr data-rn='" + rn + "'>");
                body.Append("<td class='text-center'>" + no_menu.Replace("{#key}", rn.ToString()) + "</td>");

                for (var j = 0; j < bx.ns.Count; j++)
                {
                    var cn = bx.ns[j];
                    var tno = rn < cn ? evt_menu : "-";
                    var text = rn == cn ? "■" : tno;
                    body.Append("<td class='text-center' data-rn='" + rn + "' data-cn='" + cn + "'> " + text + " </td>");
                }

                body.Append("</tr>");
            }


            var table = new StringBuilder();
            table.Append("<div class='box-body'>");
            table.Append("  <h3>" + bx.tb_title + "</h3>");
            table.Append(GetTableAttribute(bx.tb_id));

            table.Append("  <thead>");
            table.Append("    <tr>");
            table.Append(head);
            table.Append("    </tr>");
            table.Append("  </thead>");

            table.Append("  <tbody>");
            table.Append(body);
            table.Append("  </tbody>");

            table.Append("</table>");
            table.Append("</div>");

            contents.Append(table);
        }

        private string GetEventMenu(int evt_count)
        {
            var result = new StringBuilder();
            result.Append("<select class='form-control'>");
            result.Append("<option value=''>無</option>");
            for (var i = 1; i <= evt_count; i++)
            {
                result.Append("<option value='" + i + "'>" + i + "</option>");
            }
            result.Append("</select>");
            return result.ToString();
        }

        private string GetPlayerMenu(int team_count)
        {
            var result = new StringBuilder();
            result.Append("<select id='inn-select-{#key}' class='form-control' onchange='ChangeSingNoMenu(this)'>");
            result.Append("<option value=''>無</option>");
            for (var i = 1; i <= team_count; i++)
            {
                result.Append("<option value='" + i + "'>" + i + "</option>");
            }
            result.Append("</select>");
            return result.ToString();
        }

        private string GetTableAttribute(string tb_id)
        {
            return "<table id='" + tb_id + "'"
                + " class='table table-hover table-striped'"
                + " data-toggle='table'"
                + " data-show-toggle='false'"
                + " data-show-columns='false'"
                + " data-search-align='left'"
                + " data-sort-stable='false'"
                + " data-search='false'"
                + ">";
        }

        private string GetGroupName(int no)
        {
            switch (no)
            {
                case 1: return "A組";
                case 2: return "B組";
                case 3: return "C組";
                case 4: return "D組";
                case 5: return "E組";
                case 6: return "F組";
                case 7: return "G組";
                case 8: return "H組";
                default: return "ERR";
            }
        }

        private int GetTotalEventCount(int pcnt, int pteam)
        {
            int total = 0;
            for (var i = 0; i < pcnt; i++)
            {
                total += GetEventCount(pteam);
            }
            return total;
        }

        private int GetEventCount(int pteam)
        {
            switch(pteam)
            {
                case 3: return 3;
                case 4: return 6;
                case 5: return 10;
                default: return 0;
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string scene { get; set; }
        }

        private class TBox
        {
            public int team_count { get; set; }
            public int no { get; set; }
            public string tb_id { get; set; }
            public string tb_title { get; set; }
            public int plr_count { get; set; }
            public int evt_count { get; set; }
            public List<int> ns { get; set; }
        }

        private int GetInt(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}