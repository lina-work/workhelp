﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.TableTennis.Common
{
    public class in_meeting_user_options : Item
    {
        public in_meeting_user_options(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 
                    取得三階選單 json
                輸入: 
                    meeting_id
                日期: 
                    - 2023-02-08: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_user_options";


            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = this.getProperty("meeting_id", ""),
                l1_id = this.getProperty("l1_id", ""),
                l2_id = this.getProperty("l2_id", ""),
                l3_id = this.getProperty("l3_id", ""),
                scene = this.getProperty("scene", ""),
            };

            if (cfg.meeting_id == "")
            {
                throw new Exception("賽事 id 不得為空白");
            }

            if (cfg.scene == "clear")
            {
                return ClearCache(cfg);
            }
            else
            {
                ClearCache(cfg);
                return JsonData(cfg);
            }
        }

        //清除暫存資訊(三階選單)
        private Item ClearCache(TConfig cfg)
        {
            var _caching = System.Runtime.Caching.MemoryCache.Default;

            var key1 = cfg.meeting_id + "_MT_LV_OPTIONS_TOTAL";
            var key2 = cfg.meeting_id + "_MT_LV_OPTIONS_KEY";

            _caching.Remove(key1);
            _caching.Remove(key2);

            return cfg.inn.newItem();
        }

        private Item JsonData(TConfig cfg)
        {
            string aml = "";
            string sql = "";

            var _caching = System.Runtime.Caching.MemoryCache.Default;
            var _policy = new System.Runtime.Caching.CacheItemPolicy
            {
                SlidingExpiration = TimeSpan.FromMinutes(10)
            };

            var key1 = cfg.meeting_id + "_MT_LV_OPTIONS_TOTAL";
            var key2 = cfg.meeting_id + "_MT_LV_OPTIONS_KEY";

            string str1 = _caching.Contains(key1) ? _caching.Get(key1) as string : "";
            string str2 = _caching.Contains(key2) ? _caching.Get(key2) as string : "";

            if (string.IsNullOrEmpty(str1) || string.IsNullOrEmpty(str2))
            {
                Item items = GetAllLeveItems(cfg);

                int count = items.getItemCount();

                List<TNode> nodes = new List<TNode>();

                bool need_id = false;

                int ptotal = 0;
                int etotal = 0;

                for (int i = 0; i < count; i++)
                {
                    Item item = items.getItemByIndex(i);
                    TNode l1 = AddAndGetNode(nodes, item, "in_l1_value", need_id);
                    TNode l2 = AddAndGetNode(l1.Nodes, item, "in_l2_value", need_id);
                    TNode l3 = AddAndGetNode(l2.Nodes, item, "in_l3_value", need_id);
                }

                str1 = ptotal.ToString();
                str2 = Newtonsoft.Json.JsonConvert.SerializeObject(nodes);

                _caching.Set(key1, str1, _policy);
                _caching.Set(key2, str2, _policy);
            }

            Item itmResult = cfg.inn.newItem();
            itmResult.setProperty("total", str1);
            itmResult.setProperty("json", str2);
            return itmResult;
        }

        private Item GetAllLeveItems(TConfig cfg)
        {
            string sql = @"
			SELECT
				*
			FROM
			(
                SELECT DISTINCT
					 t1.in_l1         AS 'in_l1_value'
					 , t1.in_l1       AS 'in_l1_label'
					 , t1.in_l2       AS 'in_l2_value'
					 , t1.in_l2       AS 'in_l2_label'
					 , t1.in_l3       AS 'in_l3_value'
					 , t1.in_l3       AS 'in_l3_label'
					 , t11.sort_order AS 'in_l1_sort'
					 , t12.sort_order AS 'in_l2_sort'
					 , t13.sort_order AS 'in_l3_sort'
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
				LEFT OUTER JOIN
					IN_SURVEY_OPTION t11 WITH(NOLOCK)
					ON t11.source_id = '{#l1_id}'
					AND t11.in_value = t1.in_l1
				LEFT OUTER JOIN
					IN_SURVEY_OPTION t12 WITH(NOLOCK)
					ON t12.source_id = '{#l2_id}'
					AND t12.in_filter = t1.in_l1
					AND t12.in_value = t1.in_l2
				LEFT OUTER JOIN
					IN_SURVEY_OPTION t13 WITH(NOLOCK)
					ON t13.source_id = '{#l3_id}'
					AND t13.in_grand_filter = t1.in_l1
					AND t13.in_filter = t1.in_l2
					AND t13.in_value = t1.in_l3
                WHERE 
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_l1 NOT IN ('隊職員')
			) t101
			ORDER BY
				t101.in_l1_sort
				, t101.in_l2_sort
				, t101.in_l3_sort
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#l1_id}", cfg.l1_id)
                .Replace("{#l2_id}", cfg.l2_id)
                .Replace("{#l3_id}", cfg.l3_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private TNode AddAndGetNode(List<TNode> nodes, Item item, string lv, bool need_id)
        {
            string value = item.getProperty(lv, "");

            TNode search = nodes.Find(x => x.Val == value);

            if (search == null)
            {
                search = new TNode
                {
                    Lv = lv,
                    Val = value,
                    Lbl = value,
                    Cnt = 0,
                    Nodes = new List<TNode>()
                };

                if (need_id)
                {
                    search.Id = item.getProperty("id", "");
                }

                nodes.Add(search);
            }

            return search;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string l1_id { get; set; }
            public string l2_id { get; set; }
            public string l3_id { get; set; }
            public string scene { get; set; }
        }

        private class TNode
        {
            public string Id { get; set; }

            public string Lv { get; set; }

            public string Val { get; set; }

            public string Lbl { get; set; }

            public int Cnt { get; set; }

            public int ECnt { get; set; }

            public List<TNode> Nodes { get; set; }
        }
    }
}