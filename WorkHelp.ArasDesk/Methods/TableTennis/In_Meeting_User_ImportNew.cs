﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.TableTennis.Common
{
    public class In_Meeting_User_ImportNew : Item
    {
        public In_Meeting_User_ImportNew(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
            目的: 新版與會者匯入
            日誌: 
                - 2022-10-03: 創建 (lina)
        */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_User_ImportNew";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "option":
                    RunOption(cfg, itmR);
                    break;

                case "import":
                    Import(cfg, itmR);
                    break;

                case "rebuild":
                    Rebuild(cfg, itmR);
                    break;

                case "fight":
                    Fight(cfg, itmR);
                    break;

                case "day_menu":
                    DayMenu(cfg, itmR);
                    break;

                case "fix_rank": //修正準決賽名次
                    FixOther(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void RunOption(TConfig cfg, Item itmReturn)
        {
            string in_func = itmReturn.getProperty("in_func", "");
            switch (in_func)
            {
                case "create_meeting_options":
                    RebuildSurveyOptions(cfg, itmReturn);
                    break;
            }
        }

        private void RebuildSurveyOptions(TConfig cfg, Item itmReturn)
        {
            string aml = "<AML><Item type='In_Meeting' action='get' id='" + cfg.meeting_id + "' select='*'/></AML>";
            cfg.itmMeeting = cfg.inn.applyAML(aml);

            //重建問項
            var svy_model = GetMtSvy(cfg);
            RebuildInL1(cfg, svy_model.l1_id);
            RebuildInL2(cfg, svy_model.l2_id);
            RebuildInL3(cfg, svy_model.l3_id);

            //重算正取人數
            cfg.itmMeeting.apply("In_Update_MeetingOnMUEdit");
        }

        private void DayMenu(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT DISTINCT
	                in_date_key AS 'in_date'
                FROM
	                IN_MEETING_ALLOCATION WITH(NOLOCK)
                WHERE
	                in_meeting = '{#meeting_id}'
                ORDER BY
	                in_date_key
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql + ": " + count.ToString());

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setType("inn_day");
                itmReturn.addRelationship(item);
            }
        }

        private void Fight(TConfig cfg, Item itmReturn)
        {
            //賽會參數設定
            RebuildMtVariable(cfg, itmReturn);
            //過磅狀態設定
            RebuildMtWeight(cfg, itmReturn);
            //清除賽程組別資料
            ClearPrograms(cfg, itmReturn);
            //建立賽程組別資料
            CreatePrograms(cfg, itmReturn);
            //更新隊伍資料
            UpdateTeamData(cfg, itmReturn);
            //建立場地分配
            CreateAllocation(cfg, itmReturn);
            //建立競賽連結
            CreateQrCode(cfg, itmReturn);
        }

        //修正其他
        private void FixOther(TConfig cfg, Item itmReturn)
        {
            FixSectCode(cfg, itmReturn);
            FixSemiFinalRank(cfg, itmReturn);
        }

        //修正組別代碼
        private void FixSectCode(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                UPDATE t1 SET
                	t1.in_section_no = t2.in_section_no
                FROM
                	IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                (
                	SELECT DISTINCT in_meeting, in_section_no, in_l1, in_l2, in_l3 FROM In_Meeting_User_ImportNew WITH(NOLOCK)
                ) t2 ON t2.in_meeting = t1.in_meeting
                    AND t2.in_l1 = t1.in_l1
                    AND t2.in_l2 = t1.in_l2
                    AND t2.in_l3 = t1.in_l3
                WHERE
                	t1.in_meeting = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        //修正準決賽名次
        private void FixSemiFinalRank(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                UPDATE IN_MEETING_PEVENT SET
                    in_tree_rank = 'rank23'
                    , in_tree_rank_ns = '2,3'
                    , in_tree_rank_nss = '2,3'
                where in_meeting = '{#meeting_id}'
                AND in_tree_name = 'main'
                AND in_round_code = 4
                AND ISNULL(in_robin_key, '') = ''
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        //建立競賽連結
        private void CreateQrCode(TConfig cfg, Item itmReturn)
        {
            Item itmData = cfg.inn.newItem("In_Meeting");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.apply("in_qrcode_create");
        }

        //建立場地分配
        private void CreateAllocation(TConfig cfg, Item itmReturn)
        {
            //建立場地分配
            string sql = "SELECT * FROM IN_MEETING_SITE WITH(NOLOCK) WHERE in_meeting = '{#meeting_id}' AND in_code = 1";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            Item itmSite = cfg.inn.applySQL(sql);
            if (itmSite.isError() || itmSite.getResult() == "")
            {
                return;
            }

            //清除舊場地分配
            cfg.inn.applySQL("DELETE FROM IN_MEETING_ALLOCATION WHERE in_meeting = '" + cfg.meeting_id + "'");

            //建立主分類
            MergeAllocateCategory(cfg, itmSite);

            //建立場地分配
            MergeAllocateFight(cfg, itmSite);
        }

        private void MergeAllocateFight(TConfig cfg, Item itmSite)
        {
            List<string> pgs = new List<string>();

            string sql = @"
                SELECT DISTINCT
					in_section_no
					, in_l1
					, in_l2
					, in_l3
					, in_day
                FROM 
                    In_Meeting_User_ImportNew WITH(NOLOCK) 
                WHERE 
                    in_meeting = '{#meeting_id}'
                ORDER BY
					in_section_no
					, in_l1
					, in_l2
					, in_l3
					, in_day
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();


            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                string in_l3 = item.getProperty("in_l3", "");
                string in_day = item.getProperty("in_day", "");
                string in_fight_day = GetFightDay(cfg, in_day);

                string sql_qry = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK)"
                    + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                    + " AND in_l1 = N'" + in_l1 + "'"
                    + " AND in_l2 = N'" + in_l2 + "'"
                    + " AND in_l3 = N'" + in_l3 + "'"
                    ;

                Item itmProgram = cfg.inn.applySQL(sql_qry);

                if (itmProgram.isError() || itmProgram.getResult() == "")
                {
                    continue;
                }

                if (in_fight_day == "")
                {
                    continue;
                }

                string site_id = itmSite.getProperty("id", "");
                string program_id = itmProgram.getProperty("id", "");


                Item itmNew = cfg.inn.newItem("In_Meeting_Allocation", "add");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_date", in_fight_day);
                itmNew.setProperty("in_date_key", in_fight_day);
                itmNew.setProperty("in_site", site_id);
                itmNew.setProperty("in_program", program_id);
                itmNew.setProperty("in_type", "fight");
                itmNew.setProperty("in_l1", "");
                itmNew.setProperty("in_l2", "");
                itmNew = itmNew.apply();

                pgs.Add(program_id);
            }

            DateTime dt = DateTime.Now.AddDays(-1).Date;

            foreach (var pg in pgs)
            {
                string sql_upd = "UPDATE IN_MEETING_ALLOCATION SET created_on = '" + dt.ToString("yyyy-MM-dd HH:mm:00") + "' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_program = '" + pg + "'";
                cfg.inn.applySQL(sql_upd);
                dt = dt.AddMinutes(10);
            }
        }

        private void MergeAllocateCategory(TConfig cfg, Item itmSite)
        {
            string sql = @"
                SELECT DISTINCT
					in_section_no
					, in_l1
					, in_l2
					, in_day
                FROM 
                    In_Meeting_User_ImportNew WITH(NOLOCK) 
                WHERE 
                    in_meeting = '{#meeting_id}'
                ORDER BY
					in_section_no
					, in_l1
					, in_l2
					, in_day
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                string in_day = item.getProperty("in_day", "");
                string in_fight_day = GetFightDay(cfg, in_day);

                if (in_fight_day == "")
                {
                    continue;
                }

                Item itmNew = cfg.inn.newItem("In_Meeting_Allocation", "add");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_date", in_fight_day);
                itmNew.setProperty("in_date_key", in_fight_day);
                itmNew.setProperty("in_type", "category");
                itmNew.setProperty("in_l1", in_l1);
                itmNew.setProperty("in_l2", in_l2);
                itmNew = itmNew.apply();
            }
        }

        private string GetFightDay(TConfig cfg, string value)
        {
            string code = value;
            if (code.Length <= 4)
            {
                code = DateTime.Now.Year + code.PadLeft(4, '0');
            }
            if (code.Length != 8)
            {
                return "";
            }

            string y = code.Substring(0, 4);
            string m = code.Substring(4, 2);
            string d = code.Substring(6, 2);
            return y + "-" + m + "-" + d;
        }

        private void UpdateTeamData(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                UPDATE t1 SET
                	t1.in_sign_no = t11.in_draw_no
                	, t1.in_section_no = t11.in_draw_no
                	, t1.in_team_index = t11.in_index
                FROM
                	IN_MEETING_PTEAM t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                	ON t2.id = t1.source_id
                INNER JOIN
                	IN_MEETING_USER t11 WITH(NOLOCK)
                	ON t11.source_id = t2.in_meeting
                	AND t11.in_l1 = t2.in_l1
                	AND t11.in_l2 = t2.in_l2
                	AND t11.in_l3 = t2.in_l3
                	AND t11.in_sno = t1.in_sno
                WHERE
                	t2.in_meeting = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        private void CreatePrograms(TConfig cfg, Item itmReturn)
        {
            Item itmData = cfg.inn.newItem("In_Meeting");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("battle_type", "TopTwo");
            itmData.setProperty("battle_repechage", "");
            itmData.setProperty("rank_type", "SameRank");
            itmData.setProperty("surface_code", "32");
            itmData.setProperty("robin_player", "0");
            itmData.setProperty("sub_event", "0");
            itmData.setProperty("mode", "save");
            itmData.apply("In_Meeting_Program");
        }

        private void ClearPrograms(TConfig cfg, Item itmReturn)
        {
            Item itmData = cfg.inn.newItem("In_Meeting");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("mode", "clear");
            itmData.setProperty("exec", "all");
            itmData.apply("In_Meeting_Program");
        }

        //賽會參數設定
        private void RebuildMtVariable(TConfig cfg, Item itmReturn)
        {
            string need_rank57 = itmReturn.getProperty("need_rank57", "1");

            cfg.inn.applySQL("DELETE FROM In_Meeting_Variable WHERE source_id = '" + cfg.meeting_id + "'");

            RebuildMtVariable(cfg, "fight_site", "臺北科大", "16");
            RebuildMtVariable(cfg, "allocate_mode", "cycle", "32");
            RebuildMtVariable(cfg, "line_color", "red", "64");
            RebuildMtVariable(cfg, "need_rank78", "0", "128");
            RebuildMtVariable(cfg, "need_rank56", "0", "256");
            RebuildMtVariable(cfg, "need_rank57", need_rank57, "512");
        }

        private void RebuildMtVariable(TConfig cfg, string in_key, string in_value, string sort_order)
        {
            Item itmNew = cfg.inn.newItem("In_Meeting_Variable", "add");
            itmNew.setProperty("source_id", cfg.meeting_id);
            itmNew.setProperty("in_key", in_key);
            itmNew.setProperty("in_value", in_value);
            itmNew.setProperty("sort_order", sort_order);
            itmNew = itmNew.apply();
        }

        //過磅狀態設定
        private void RebuildMtWeight(TConfig cfg, Item itmReturn)
        {
            cfg.inn.applySQL("DELETE FROM In_Meeting_PWeight WHERE in_meeting = '" + cfg.meeting_id + "'");
            RebuildMtWeight(cfg, "off", "0");
            RebuildMtWeight(cfg, "leave", "0");
            RebuildMtWeight(cfg, "dq", "0");
        }

        private void RebuildMtWeight(TConfig cfg, string in_status, string in_is_remove)
        {
            Item itmNew = cfg.inn.newItem("In_Meeting_PWeight", "add");
            itmNew.setProperty("in_meeting", cfg.meeting_id);
            itmNew.setProperty("in_status", in_status);
            itmNew.setProperty("in_is_remove", in_is_remove);
            itmNew = itmNew.apply();
        }

        private void Rebuild(TConfig cfg, Item itmReturn)
        {
            string aml = "<AML><Item type='In_Meeting' action='get' id='" + cfg.meeting_id + "' select='*'/></AML>";
            cfg.itmMeeting = cfg.inn.applyAML(aml);

            var svy_model = GetMtSvy(cfg);
            RebuildInL1(cfg, svy_model.l1_id);
            RebuildInL2(cfg, svy_model.l2_id);
            RebuildInL3(cfg, svy_model.l3_id);

            MergeMtMuser(cfg);
        }

        private void MergeMtMuser(TConfig cfg)
        {
            //刪除舊資料
            cfg.inn.applySQL("DELETE FROM IN_MEETING_USER WHERE source_id = '" + cfg.meeting_id + "'");

            string sql = "SELECT * FROM In_Meeting_User_ImportNew WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "' ORDER BY in_play_no";

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                //與會者資訊
                Item applicant = NewMUser(cfg, item);
                //建立與會者
                var itmMUser = applicant.apply("add");
            }

            //重算正取人數
            cfg.itmMeeting.apply("In_Update_MeetingOnMUEdit");
        }

        private Item NewMUser(TConfig cfg, Item item)
        {
            var applicant = cfg.inn.newItem("In_Meeting_User");

            applicant.setProperty("source_id", cfg.meeting_id);

            //所屬單位
            applicant.setProperty("in_current_org", item.getProperty("in_current_org", ""));

            //姓名
            applicant.setProperty("in_name", item.getProperty("in_name", ""));

            //身分證號
            applicant.setProperty("in_sno", item.getProperty("in_play_no", ""));

            //性別
            applicant.setProperty("in_gender", GetGender(cfg, item));

            //西元生日
            applicant.getProperty("in_birth", "1990-01-01");

            //電子信箱
            applicant.setProperty("in_email", "na@na.n");

            //手機號碼
            applicant.setProperty("in_tel", "");

            //協助報名者姓名
            applicant.setProperty("in_creator", "lwu001");

            //協助報名者身分號
            applicant.setProperty("in_creator_sno", "lwu001");

            //所屬群組
            applicant.setProperty("in_group", "原創"); //F103277376測試者


            //預設身分均為選手
            applicant.setProperty("in_role", "player");

            //正取
            applicant.setProperty("in_note_state", "official");

            //組別
            applicant.setProperty("in_gameunit", item.getProperty("in_section_name", ""));

            //組名
            applicant.setProperty("in_section_name", item.getProperty("in_section_name", ""));

            //捐款金額
            applicant.setProperty("in_expense", "0");

            //序號
            Item itmIndex = MaxIndexItem(cfg, applicant);
            applicant.setProperty("in_index", item.getProperty("in_play_no", ""));

            //競賽項目
            applicant.setProperty("in_l1", item.getProperty("in_l1", ""));

            //競賽組別
            applicant.setProperty("in_l2", item.getProperty("in_l2", ""));

            //競賽分級
            applicant.setProperty("in_l3", item.getProperty("in_l3", ""));

            //非實名制
            applicant.setProperty("in_mail", item.getProperty("in_play_no", ""));

            //報名時間
            applicant.setProperty("in_regdate", System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));

            //繳費單號
            applicant.setProperty("in_paynumber", "");

            //籤號
            applicant.setProperty("in_draw_no", item.getProperty("in_draw_no", ""));

            return applicant;
        }

        private string GetGender(TConfig cfg, Item item)
        {
            string in_section_name = item.getProperty("in_section_name", "");
            return GetGender(cfg, in_section_name);
        }

        private string GetGender(TConfig cfg, string value)
        {
            if (value.Contains("男")) return "男";
            if (value.Contains("女")) return "女";
            if (value.Contains("混")) return "混";
            return "";
        }


        //取得該組當前最大序號
        private Item MaxIndexItem(TConfig cfg, Item applicant)
        {
            string sql = "";

            string in_l1 = applicant.getProperty("in_l1", "");
            string in_l2 = applicant.getProperty("in_l2", "");
            string in_l3 = applicant.getProperty("in_l3", "");
            string in_l4 = applicant.getProperty("in_l4", "");

            sql = "SELECT MAX(in_index) AS 'max_idx' FROM In_Meeting_User WITH(NOLOCK) WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_l1 = N'" + in_l1 + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql_index: " + sql);

            Item itmResult = cfg.inn.applySQL(sql);

            if (itmResult.isError() || itmResult.getResult() == "")
            {
                itmResult = cfg.inn.newItem();
                itmResult.setProperty("new_idx", "00001");
            }
            else
            {
                var max_idx = itmResult.getProperty("max_idx", "1");
                var new_idx = GetInt(max_idx) + 1;
                applicant.setProperty("new_idx", new_idx.ToString("00000"));

            }

            return itmResult;
        }

        private void RebuildInL1(TConfig cfg, string id)
        {
            cfg.inn.applySQL("DELETE FROM IN_SURVEY_OPTION WHERE source_id = '" + id + "'");

            string sql = @"
                SELECT DISTINCT
                    in_l1
                FROM 
                    In_Meeting_User WITH(NOLOCK) 
                WHERE 
                    source_id = '{#meeting_id}'
                ORDER BY
                    in_l1
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            string in_selectoption = "";

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");
                int sort_order = (i + 1) * 100;

                Item itmNew = cfg.inn.newItem("In_Survey_Option", "add");
                itmNew.setProperty("source_id", id);
                itmNew.setProperty("in_label", in_l1);
                itmNew.setProperty("in_value", in_l1);
                itmNew.setProperty("sort_order", sort_order.ToString());
                itmNew = itmNew.apply();

                in_selectoption += "@" + in_l1;
            }

            cfg.inn.applySQL("UPDATE IN_SURVEY SET in_selectoption = N'" + in_selectoption + "' WHERE id = '" + id + "'");
        }

        private void RebuildInL2(TConfig cfg, string id)
        {
            cfg.inn.applySQL("DELETE FROM IN_SURVEY_OPTION WHERE source_id = '" + id + "'");

            string sql = @"
                SELECT DISTINCT
                    in_l1
                    , in_l2
                FROM 
                    In_Meeting_User WITH(NOLOCK) 
                WHERE 
                    source_id = '{#meeting_id}'
                ORDER BY
                    in_l1
                    , in_l2
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                int sort_order = (i + 1) * 100;

                Item itmNew = cfg.inn.newItem("In_Survey_Option", "add");
                itmNew.setProperty("source_id", id);
                itmNew.setProperty("in_filter", in_l1);
                itmNew.setProperty("in_label", in_l2);
                itmNew.setProperty("in_value", in_l2);
                itmNew.setProperty("sort_order", sort_order.ToString());
                itmNew = itmNew.apply();
            }
        }

        private void RebuildInL3(TConfig cfg, string id)
        {
            cfg.inn.applySQL("DELETE FROM IN_SURVEY_OPTION WHERE source_id = '" + id + "'");

            string sql = @"
                SELECT DISTINCT
                    ISNULL(in_program_no, '') AS 'in_program_no'
                    , in_l1
                    , in_l2
                    , in_l3
                FROM 
                    In_Meeting_User WITH(NOLOCK) 
                WHERE 
                    source_id = '{#meeting_id}'
                ORDER BY
                    in_program_no
                    , in_l1
                    , in_l2
                    , in_l3
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            int weight_idx = 0;
            int last_code = 10000;
            string last_key = "";

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                string in_l3 = item.getProperty("in_l3", "");

                string key = in_l1 + "-" + in_l2;

                int sort_order = last_code + (i + 1) * 100;
                if (key != last_key)
                {
                    last_key = key;
                    last_code += 10000;
                }
                if (in_l3.Contains("以上"))
                {
                    sort_order += 3000;
                }

                string in_weight = "";// GetWeight(cfg, item);
                string in_n1 = GetShortName(cfg, item, in_weight, weight_idx);

                Item itmNew = cfg.inn.newItem("In_Survey_Option", "add");
                itmNew.setProperty("source_id", id);
                itmNew.setProperty("in_grand_filter", in_l1);
                itmNew.setProperty("in_filter", in_l2);
                itmNew.setProperty("in_label", in_l3);
                itmNew.setProperty("in_value", in_l3);
                itmNew.setProperty("in_weight", in_weight);
                itmNew.setProperty("in_n1", in_n1);
                itmNew.setProperty("sort_order", sort_order.ToString());
                itmNew = itmNew.apply();

                if (in_l3.Contains("以上"))
                {
                    weight_idx = 0;
                }
                else
                {
                    weight_idx++;
                }
            }
        }

        private string GetShortName(TConfig cfg, Item item, string in_weight, int weight_index)
        {
            string result = "";
            string in_l2 = item.getProperty("in_l2", "");
            string in_l3 = item.getProperty("in_l3", "");
            string in_gender = GetGender(cfg, in_l2);
            string in_age = "";

            if (in_l2.Contains("女子站立組"))
            {
                result = "女" + in_l3;
            }
            if (in_l2.Contains("女子智障組"))
            {
                result = "女" + in_l3;
            }
            if (in_l2.Contains("女子輪椅組"))
            {
                result = "女" + in_l3;
            }
            if (in_l2.Contains("男子站立組"))
            {
                result = "男" + in_l3;
            }
            if (in_l2.Contains("男子智障組"))
            {
                result = "男" + in_l3;
            }
            if (in_l2.Contains("男子輪椅組"))
            {
                result = "男" + in_l3;
            }
            if (in_l2.Contains("女子雙打組"))
            {
                result = "女雙" + in_l3;
            }
            if (in_l2.Contains("男子雙打組"))
            {
                result = "男雙" + in_l3;
            }
            if (in_l2.Contains("混合雙打組"))
            {
                result = "混雙" + in_l3;
            }

            return result;
        }

        private string GetWeight(TConfig cfg, Item item)
        {
            string in_l3 = item.getProperty("in_l3", "");

            string in_weight = "";


            if (in_l3.Contains("以上"))
            {
                in_weight = "+" + in_l3.Replace("公斤以上級", "");
            }
            else
            {
                in_weight = "-" + in_l3.Replace("公斤級", "");
            }

            return in_weight + "Kg";
        }



        private TMtSvy GetMtSvy(TConfig cfg)
        {
            TMtSvy result = new TMtSvy { l1_id = "", l2_id = "", l3_id = "" };

            string sql = @"
                SELECT
	                t2.id
	                , t2.in_property
	                , t2.in_questions
                FROM
	                IN_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN
	                IN_SURVEY t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t2.in_property IN ('in_l1', 'in_l2', 'in_l3')
                ORDER BY
	                t2.in_property 
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_property = item.getProperty("in_property", "");
                switch (in_property)
                {
                    case "in_l1": result.l1_id = id; break;
                    case "in_l2": result.l2_id = id; break;
                    case "in_l3": result.l3_id = id; break;
                }
            }
            return result;
        }

        private class TMtSvy
        {
            public string l1_id { get; set; }
            public string l2_id { get; set; }
            public string l3_id { get; set; }
        }

        //匯入與會者
        private void Import(TConfig cfg, Item itmReturn)
        {
            Item itmSites = cfg.inn.applySQL("SELECT * FROM IN_MEETING_SITE WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "'");
            if (itmSites.isError() || itmSites.getResult() == "")
            {
                throw new Exception("請先設定場地");
            }

            var value = itmReturn.getProperty("value", "");
            if (value == "") throw new Exception("無匯入資料");

            var list = MapList(cfg, value);
            if (list == null || list.Count == 0) throw new Exception("無匯入資料");

            cfg.inn.applySQL("DELETE FROM In_Meeting_User_ImportNew");

            var page_l1 = itmReturn.getProperty("in_l1", "");
            for (var i = 0; i < list.Count; i++)
            {
                var row = list[i];
                var in_l1 = string.IsNullOrWhiteSpace(row.c11) ? page_l1 : row.c11;

                Item itmNew = cfg.inn.newItem("In_Meeting_User_ImportNew", "add");
                itmNew.setProperty("in_no", row.c00);
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_l1", in_l1);
                itmNew.setProperty("in_l2", row.c03);
                itmNew.setProperty("in_l3", row.c04);
                itmNew.setProperty("in_section_name", row.c02);
                itmNew.setProperty("in_section_no", row.c01);

                itmNew.setProperty("in_play_no", row.c05);
                itmNew.setProperty("in_current_org", row.c06);
                itmNew.setProperty("in_name", row.c07);
                itmNew.setProperty("in_draw_no", row.c08);
                itmNew.setProperty("in_city", row.c09);
                itmNew.setProperty("in_day", row.c10);
                itmNew = itmNew.apply();
            }
        }

        private List<TRow> MapList(TConfig cfg, string value)
        {
            try
            {
                var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TRow>>(value);
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private class TRow
        {
            public string c00 { get; set; }
            public string c01 { get; set; }
            public string c02 { get; set; }
            public string c03 { get; set; }
            public string c04 { get; set; }
            public string c05 { get; set; }
            public string c06 { get; set; }
            public string c07 { get; set; }
            public string c08 { get; set; }
            public string c09 { get; set; }
            public string c10 { get; set; }
            public string c11 { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
        }

        private DateTime GetDtm(string value)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            return result;
        }

        private int GetInt(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}