﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.TableTennis.Common
{
    public class In_Meeting_User_Merge : Item
    {
        public In_Meeting_User_Merge(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 併組、修改姓名
                日誌: 
                    - 2023-02-08: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_User_Merge";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "page":
                    Page(cfg, itmR);
                    break;

                case "players":
                    Players(cfg, itmR);
                    break;

                case "merge":
                    Merge(cfg, itmR);
                    break;

                case "query":
                    Query(cfg, itmR);
                    break;

                case "fix_name":
                    FixName(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void FixName(TConfig cfg, Item itmReturn)
        {
            var muid = itmReturn.getProperty("muid", "");
            var new_name = itmReturn.getProperty("new_name", "");
            if (muid == "") throw new Exception("查無與會者 id");
            if (new_name == "") throw new Exception("請輸入修正後姓名");

            var itmOldMUser = cfg.inn.applySQL("SELECT * FROM IN_MEETING_USER WITH(NOLOCK) WHERE id = '" + muid + "'");
            if (itmOldMUser.isError() || itmOldMUser.getResult() == "") throw new Exception("查無與會者資料");

            var itmProgram = GetProgramItem(cfg, itmOldMUser);
            if (itmProgram.isError() || itmProgram.getResult() == "") throw new Exception("查無組別資料");

            var row = new TFixRow
            {
                muid = muid,
                new_name = new_name,
                old_name = itmOldMUser.getProperty("in_name", ""),
                itmOldMUser = itmOldMUser,
                itmProgram = itmProgram,
            };

            MUserFixName(cfg, row);

            PTeamFIxName2(cfg, row);
        }

        private void MUserFixName(TConfig cfg, TFixRow row)
        {
            //日誌
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "與會者姓名修正"
                + ": meeting_id = " + cfg.meeting_id
                + ": muid = " + row.muid
                + ", old name = " + row.old_name
                + ", new_name = " + row.new_name);


            string sql = "UPDATE IN_MEETING_USER SET in_name = N'" + row.new_name + "' WHERE id = '" + row.muid + "'";

            cfg.inn.applySQL(sql);
        }

        private void PTeamFIxName1(TConfig cfg, TFixRow row)
        {
            var itmPTeam = GetProgramTeamItem(cfg, row.itmOldMUser, row.itmProgram);
            //if (itmPTeam.isError() || itmPTeam.getResult() == "") throw new Exception("查無比賽隊伍資料");

            if (itmPTeam.isError() || itmPTeam.getResult() == "") return;
            
            var team_id = itmPTeam.getProperty("id", "");

            var sql = "UPDATE IN_MEETING_PTEAM SET"
                + "  in_name = REPLACE(in_name, N'" + row.old_name + "', N'" + row.new_name + "')"
                + ", in_names = REPLACE(in_names, N'" + row.old_name + "', N'" + row.new_name + "')"
                + " WHERE id = '" + team_id + "'";

            cfg.inn.applySQL(sql);
        }

        private void PTeamFIxName2(TConfig cfg, TFixRow row)
        {
            var program_id = row.itmProgram.getProperty("id", "");

            var in_l1 = row.itmOldMUser.getProperty("in_l1", "");
            var in_l2 = row.itmOldMUser.getProperty("in_l2", "");
            var in_l3 = row.itmOldMUser.getProperty("in_l3", "");
            var in_index = row.itmOldMUser.getProperty("in_index", "");
            var in_team = row.itmOldMUser.getProperty("in_team", "").Trim();
            var in_creator_sno = row.itmOldMUser.getProperty("in_creator_sno", "");

            var is_team = in_l1.Contains("團體");

            var sql_qry = @"
                SELECT
                    in_name
                    , in_waiting_list
                FROM
                    IN_MEETING_USER t1 WITH(NOLOCK)
                WHERE
                    t1.source_id = '{#meeting_id}'
                    AND ISNULL(t1.in_l1, '') = N'{#in_l1}'
                    AND ISNULL(t1.in_l2, '') = N'{#in_l2}'
                    AND ISNULL(t1.in_l3, '') = N'{#in_l3}'
                    AND ISNULL(t1.in_index, '') = '{#in_index}'
                    AND ISNULL(t1.in_creator_sno, '') = '{#in_creator_sno}'
                ORDER BY
                    t1.in_stuff_b1
                    , t1.in_team
                    , t1.in_sno
            ";

            sql_qry = sql_qry.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3)
                .Replace("{#in_index}", in_index)
                .Replace("{#in_creator_sno}", in_creator_sno)
                ;

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql_qry);

            var itmMUsers = cfg.inn.applySQL(sql_qry);
            if (itmMUsers.isError() || itmMUsers.getResult() == "") return;

            var count = itmMUsers.getItemCount();
            var in_name = "";
            var in_names = "";

            if (!is_team)
            {
                if (count != 1)
                {
                    throw new Exception("與會者資料錯誤");
                }
                else
                {
                    in_name = itmMUsers.getProperty("in_name", "");
                    in_names = in_name;
                }
            }
            else
            {
                var names = NameList(cfg, itmMUsers);
                var split = ", ";
                in_names = string.Join(split, names);

                if (in_team != "")
                {
                    in_name = in_team;
                    in_names = in_names + " (" + in_team + "隊)";
                }
                else
                {
                    in_name = "";
                }
            }

            var sql = "UPDATE IN_MEETING_PTEAM SET"
                + "  in_name = N'" + in_name + "'"
                + ", in_names = N'" + in_names + "'"
                + " WHERE source_id = '" + program_id + "'"
                + " AND in_index = '" + in_index + "'"
                + " AND in_creator_sno = '" + in_creator_sno + "'"
                + " AND ISNULL(in_type, '') <> 's'"
                ;

            cfg.inn.applySQL(sql);
        }

        private List<string> NameList(TConfig cfg, Item itmMUsers)
        {
            var result = new List<string>();
            var count = itmMUsers.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmMUser = itmMUsers.getItemByIndex(i);
                var in_name = itmMUser.getProperty("in_name", "").Trim();
                var in_waiting_list = itmMUser.getProperty("in_waiting_list", "");
                if (in_name == "") continue;

                if (in_waiting_list.Contains("備"))
                {
                    in_name = in_name + "[備]";
                }

                result.Add(in_name);
            }
            return result;
        }

        private Item GetProgramTeamItem(TConfig cfg, Item itmOldMUser, Item itmProgram)
        {
            var program_id = itmProgram.getProperty("id", "");
            var in_index = itmOldMUser.getProperty("in_index", "");
            var in_creator_sno = itmOldMUser.getProperty("in_creator_sno", "");
            var old_name = itmOldMUser.getProperty("in_name", "");

            string sql = @"
                SELECT 
	                * 
                FROM 
	                IN_MEETING_PTEAM WITH(NOLOCK) 
                WHERE 
	                source_id = '{#program_id}' 
	                AND in_index = '{#in_index}'
	                AND in_creator_sno = N'{#in_creator_sno}'
					AND ISNULL(in_type, '') <> 's'
					AND in_names LIKE N'%{#old_name}%'
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_index}", in_index)
                .Replace("{#in_creator_sno}", in_creator_sno)
                .Replace("{#old_name}", old_name);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetProgramItem(TConfig cfg, Item itmOldMUser)
        {
            var in_l1 = itmOldMUser.getProperty("in_l1", "");
            var in_l2 = itmOldMUser.getProperty("in_l2", "");
            var in_l3 = itmOldMUser.getProperty("in_l3", "");

            string sql = @"
                SELECT 
	                * 
                FROM 
	                IN_MEETING_PROGRAM WITH(NOLOCK) 
                WHERE 
	                in_meeting = '{#meeting_id}' 
	                AND in_l1 = N'{#in_l1}'
	                AND in_l2 = N'{#in_l2}'
	                AND in_l3 = N'{#in_l3}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private void Merge(TConfig cfg, Item itmReturn)
        {
            var svy_model = GetMtSvy(cfg);

            string in_l1 = itmReturn.getProperty("in_l1", "");
            string in_l2 = itmReturn.getProperty("in_l2", "");
            string in_l3 = itmReturn.getProperty("in_l3", "");

            string new_in_l2 = itmReturn.getProperty("new_in_l2", "");
            string new_in_l3 = itmReturn.getProperty("new_in_l3", "");
            string ids = itmReturn.getProperty("ids", "");

            string sql = @"
                SELECT 
	                sort_order 
                FROM 
	                IN_SURVEY_OPTION WITH(NOLOCK) 
                WHERE 
	                source_id = '{#l3_id}' 
	                AND in_grand_filter = N'{#in_l1}'
	                AND in_filter = N'{#in_l2}'
	                AND in_value = N'{#in_l3}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3)
                .Replace("{#l3_id}", svy_model.l3_id);

            Item itmOldOpt = cfg.inn.applySQL(sql);
            if (itmOldOpt.isError() || itmOldOpt.getResult() == "")
            {
                itmOldOpt = cfg.inn.newItem();
                itmOldOpt.setProperty("sort_order", "");
            }


            sql = @"
                UPDATE IN_MEETING_USER SET 
	                  in_l2_old = in_l2
	                , in_l3_old = in_l3
                WHERE
	                source_id = '{#meeting_id}'
	                AND id IN ({#ids})
	                AND ISNULL(in_l1, '') = N'{#in_l1}'
	                AND ISNULL(in_l2, '') = N'{#in_l2}'
	                AND ISNULL(in_l3, '') = N'{#in_l3}'
	                AND ISNULL(in_l2_old, '') = ''
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#ids}", ids)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3);


            cfg.inn.applySQL(sql);

            sql = @"
                UPDATE IN_MEETING_USER SET 
	                  in_l2 = N'{#new_in_l2}'
	                , in_l3 = N'{#new_in_l3}'
                WHERE
	                source_id = '{#meeting_id}'
	                AND id IN ({#ids})
	                AND ISNULL(in_l1, '') = N'{#in_l1}'
	                AND ISNULL(in_l2, '') = N'{#in_l2}'
	                AND ISNULL(in_l3, '') = N'{#in_l3}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#ids}", ids)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3)
                .Replace("{#new_in_l2}", new_in_l2)
                .Replace("{#new_in_l3}", new_in_l3);

            cfg.inn.applySQL(sql);

            sql = "SELECT id FROM In_Survey_Option WITH(NOLOCK) WHERE source_id = '" + svy_model.l3_id + "' AND in_value = N'" + new_in_l3 + "'";
            var itmExists = cfg.inn.applySQL(sql);
            if (itmExists.getResult() == "")
            {
                var str_sort_order = itmOldOpt.getProperty("sort_order", "");
                var int_sort_order = GetInt(str_sort_order);

                //新增至 IN_SURVEY_OPTION
                Item itmNew = cfg.inn.newItem("In_Survey_Option", "merge");
                itmNew.setAttribute("where", "source_id = '" + svy_model.l3_id + "' AND in_value = '" + new_in_l3 + "'");
                itmNew.setProperty("source_id", svy_model.l3_id);
                itmNew.setProperty("in_label", new_in_l3);
                itmNew.setProperty("in_value", new_in_l3);
                itmNew.setProperty("in_filter", new_in_l2);
                itmNew.setProperty("in_grand_filter", in_l1);
                if (int_sort_order > 0)
                {
                    itmNew.setProperty("sort_order", (int_sort_order + 50).ToString());
                }
                itmNew.apply();
            }
        }

        private void Players(TConfig cfg, Item itmReturn)
        {
            string in_l1 = itmReturn.getProperty("in_l1", "");
            string in_l2 = itmReturn.getProperty("in_l2", "");
            string in_l3 = itmReturn.getProperty("in_l3", "");

            string sql = @"
                SELECT
	                id
	                , in_name
	                , in_gender
	                , in_current_org
	                , in_team
	                , in_l1
	                , in_l2
	                , in_l3
	                , in_index
	                , in_l2_old
	                , in_l3_old
                FROM
	                IN_MEETING_USER WITH(NOLOCK)
                WHERE
	                source_id = '{#meeting_id}'
	                AND ISNULL(in_l1, '') = N'{#in_l1}'
	                AND ISNULL(in_l2, '') = N'{#in_l2}'
	                AND ISNULL(in_l3, '') = N'{#in_l3}'
                ORDER BY
                    in_l1
                    , in_l2_old
                    , in_l3_old
	                , in_current_org
	                , in_team
	                , in_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType("inn_player");
                item.setProperty("no", (i + 1).ToString());
                itmReturn.addRelationship(item);
            }
        }

        private void Query(TConfig cfg, Item itmReturn)
        {
            string condition = itmReturn.getProperty("condition", "");
            string in_l1 = itmReturn.getProperty("in_l1", "");
            string in_l2 = itmReturn.getProperty("in_l2", "");
            string in_l3 = itmReturn.getProperty("in_l3", "");
            string query_org = itmReturn.getProperty("query_org", "");
            string query_name = itmReturn.getProperty("query_name", "");

            string order_by1 = "t1.in_current_org, t1.in_name, t1.in_l1, t1.in_l2, t1.in_l3";
            string order_by2 = "t1.in_l1, t1.in_l2, t1.in_l3, t1.in_current_org, t1.in_team, t1.in_sno";
            string order_by = order_by2;

            var list = new List<string>();
            if (condition == "lv")
            {
                list.Add("AND ISNULL(t1.in_l1, '') = N'" + in_l1 + "'");
                list.Add("AND ISNULL(t1.in_l2, '') = N'" + in_l2 + "'");
                list.Add("AND ISNULL(t1.in_l3, '') = N'" + in_l3 + "'");
            }
            else
            {
                if (query_org != "")
                {
                    list.Add("AND t1.in_current_org LIKE '%' + N'" + query_org + "' + '%' ");
                }
                if (query_name != "")
                {
                    list.Add("AND t1.in_name LIKE '%' + N'" + query_name + "' + '%' ");
                    order_by = order_by1;
                }
            }

            string sql = @"
                SELECT
	                t1.id
	                , t1.in_name
	                , t1.in_gender
	                , t1.in_current_org
	                , t1.in_team
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_index
	                , t1.in_creator
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                WHERE
	                t1.source_id = '{#meeting_id}'
	                {#cond_list}
                ORDER BY
	                {#order_by}
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#cond_list}", string.Join(" ", list))
                .Replace("{#order_by}", order_by);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var _in_l2 = item.getProperty("in_l2", "");
                _in_l2 = _in_l2.Replace("個-", "").Replace("團-", "");
                item.setProperty("in_l2", _in_l2);
                item.setType("inn_player");
                item.setProperty("no", (i + 1).ToString());
                itmReturn.addRelationship(item);
            }
        }

        //頁面
        private void Page(TConfig cfg, Item itmReturn)
        {
            cfg.itmMeeting = cfg.inn.applySQL("SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");

            //活動資訊
            AppendMeeting(cfg, itmReturn);

            var svy_model = GetMtSvy(cfg);

            // //附加選單資訊(清除)
            // Item itmData1 = cfg.inn.newItem();
            // itmData1.setType("In_Meeting");
            // itmData1.setProperty("meeting_id", cfg.meeting_id);
            // itmData1.setProperty("l1_id", svy_model.l1_id);
            // itmData1.setProperty("l2_id", svy_model.l2_id);
            // itmData1.setProperty("l3_id", svy_model.l3_id);
            // itmData1.setProperty("scene", "clear");
            // itmData1.apply("in_meeting_user_options");

            //附加選單資訊(查詢)
            Item itmJson1 = cfg.inn.newItem("In_Meeting");
            itmJson1.setProperty("meeting_id", cfg.meeting_id);
            itmJson1.setProperty("l1_id", svy_model.l1_id);
            itmJson1.setProperty("l2_id", svy_model.l2_id);
            itmJson1.setProperty("l3_id", svy_model.l3_id);
            itmJson1 = itmJson1.apply("in_meeting_user_options");
            itmReturn.setProperty("in_level_json", itmJson1.getProperty("json", ""));
        }

        //活動資訊
        private void AppendMeeting(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));
        }

        private TMtSvy GetMtSvy(TConfig cfg)
        {
            TMtSvy result = new TMtSvy { l1_id = "", l2_id = "", l3_id = "" };

            string sql = @"
                SELECT
	                t2.id
	                , t2.in_property
	                , t2.in_questions
                FROM
	                IN_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN
	                IN_SURVEY t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t2.in_property IN ('in_l1', 'in_l2', 'in_l3')
                ORDER BY
	                t2.in_property 
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_property = item.getProperty("in_property", "");
                switch (in_property)
                {
                    case "in_l1": result.l1_id = id; break;
                    case "in_l2": result.l2_id = id; break;
                    case "in_l3": result.l3_id = id; break;
                }
            }
            return result;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
        }

        private class TFixRow
        {
            public string muid { get; set; }
            public string new_name { get; set; }
            public string old_name { get; set; }
            public Item itmOldMUser { get; set; }
            public Item itmProgram { get; set; }
        }

        private class TMtSvy
        {
            public string l1_id { get; set; }
            public string l2_id { get; set; }
            public string l3_id { get; set; }
        }

        private int GetInt(string value)
        {
            if (value == "") return 0;
            var result = 0;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return 0;
        }
    }
}