﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Utility
{
    public class GIT_TEST : Item
    {
        public GIT_TEST(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 版控狀態
                日誌: 
                    - 2022-12-16: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Git_Status";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),
                wait_milliseconds = 600,
            };

            var items = GetSiteItems(cfg);
            var rows = MapRows(cfg, items);
            foreach (var row in rows)
            {
                GitStatusCheck(cfg, row);
                GitStatusOutput(cfg, row);
                RunMergeGitStatus(cfg, row);
            }
            return itmR;
        }

        private List<TRow> MapRows(TConfig cfg, Item items)
        {
            var result = new List<TRow>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                result.Add(new TRow
                {
                    id = item.getProperty("id", ""),
                    name = item.getProperty("in_name", ""),
                    path = item.getProperty("in_path", ""),
                    auto_commit = item.getProperty("in_auto_commit", "") == "1",
                    is_error = false,
                    message = string.Empty,
                    lines = new List<string>(),
                    need_commit = false,
                    commit_result = false,
                });
            }
            return result;
        }

        private Item GetSiteItems(TConfig cfg)
        {
            string sql = @"
                SELECT 
                	t1.*
                FROM
                	In_Git_Status t1 WITH(NOLOCK)
                ORDER BY
                	t1.in_sort_order
            ";

            return cfg.inn.applySQL(sql);
        }

        private void GitStatusOutput(TConfig cfg, TRow row)
        {
            if (row.lines.Count < 8)
            {
                row.in_note = "無權限或非 GIT";
                row.in_need_commit = "0";
                row.in_branch = "";
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, " - " + row.name + " " + row.in_note + " _# path: " + row.path);
                return;
            }

            var key_line = row.lines[7];
            if (key_line == "nothing to commit, working tree clean")
            {
                row.need_commit = false;
            }
            else
            {
                row.need_commit = true;
            }

            var contents = new StringBuilder(512);
            //contents.AppendLine(" - " + row.name + ".need_commit = " + row.need_commit);
            //contents.AppendLine("     - " + row.lines[3]);
            contents.AppendLine(" - " + row.lines[4]);
            contents.AppendLine(" - " + row.lines[5]);
            contents.AppendLine(" - " + row.lines[7]);

            row.in_note = contents.ToString();
            row.in_need_commit = row.need_commit ? "1" : "0";
            row.in_branch = row.lines[4].Replace("On branch ", "");
        }

        private void RunMergeGitStatus(TConfig cfg, TRow row)
        {
            row.in_commit_note = "";
            if (row.need_commit && row.auto_commit)
            {
                var result = AutoCommit(cfg, row);
                if (result)
                {
                    row.commit_result = true;
                    row.in_commit_note = DateTime.Now.ToString("HH:mm:ss") + " 自動簽入成功";
                    row.in_need_commit = "0";
                }
                else
                {
                    row.commit_result = false;
                    row.in_commit_note = DateTime.Now.ToString("HH:mm:ss") + " 自動簽入失敗";
                    MergeGitStatus(cfg, row);
                }
            }
            else
            {
                MergeGitStatus(cfg, row);
            }
        }

        private void MergeGitStatus(TConfig cfg, TRow row)
        {
            Item itmOld = cfg.inn.newItem("In_Git_Status", "merge");
            itmOld.setAttribute("where", "id='" + row.id + "'");
            itmOld.setProperty("id", row.id);
            itmOld.setProperty("in_branch", row.in_branch);
            itmOld.setProperty("in_note", row.in_note);
            itmOld.setProperty("in_need_commit", row.in_need_commit);
            itmOld.setProperty("in_commit_note", row.in_commit_note);
            itmOld.apply();
        }

        private bool AutoCommit(TConfig cfg, TRow row)
        {
            try
            {
               
                var cmd = CliWrap.Cli.Wrap("git")
                    .SetWorkingDirectory("C:/site/test_autocommit")
                    .SetArguments("GIT_AUTHOR_NAME", "Lina")
                    .SetEnvironmentVariable("GIT_AUTHOR_EMAIL", "lina.tian@innosoft.com.tw")
                    .SetArguments("status");

                var result = cmd.Execute();

                var output = result.StandardOutput;

                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, output);

                //string directory = ""; // directory of the git repository

                //using (var powershell = PowerShell.Create())
                //{
                //    // this changes from the user folder that PowerShell starts up with to your git repository
                //    powershell.AddScript($"cd {directory}");

                //    powershell.AddScript(@"git init");
                //    powershell.AddScript(@"git add *");
                //    powershell.AddScript(@"git commit -m 'git commit from PowerShell in C#'");
                //    powershell.AddScript(@"git push");

                //    Collection<PSObject> results = powershell.Invoke();
                //}

                //var p = new System.Diagnostics.Process();
                //p.StartInfo.FileName = "cmd.exe";
                //p.StartInfo.StandardOutputEncoding = Encoding.UTF8;
                //p.StartInfo.UseShellExecute = false;
                //p.StartInfo.RedirectStandardInput = true;// 接受來自呼叫程式的輸入資訊
                //p.StartInfo.RedirectStandardOutput = true;// 由呼叫程式獲取輸出資訊
                //p.StartInfo.RedirectStandardError = true;//重定向標準錯誤輸出
                //p.StartInfo.CreateNoWindow = true; //不跳出cmd視窗
                //p.StartInfo.WorkingDirectory = row.path;

                //p.Start();
                //p.StandardInput.WriteLine("git add .");
                //p.StandardInput.WriteLine("git commit -m 'auto'");
                //p.StandardInput.WriteLine("git push origin " + row.in_branch);

                //var line0 = GetOuputLine(p);//Microsoft Windows
                //var line1 = GetOuputLine(p);//(c) Microsoft Corporation.
                //var line2 = GetOuputLine(p);//空白行
                //var line3 = GetOuputLine(p);//path
                //var line4 = GetOuputLine(p);//path

                //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, line3);
                //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, line4);

                return true;
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, ex.Message);
                return false;
            }
        }

        private void GitStatusCheck(TConfig cfg, TRow row)
        {
            try
            {
                var fileName = @"C:\temp\Test\WorkHelp.AnalysisConsole.exe";
                System.Diagnostics.Process.Start(fileName, "");

                var p = new System.Diagnostics.Process();
                p.StartInfo.FileName = "cmd.exe";
                p.StartInfo.StandardOutputEncoding = Encoding.UTF8;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardInput = true;// 接受來自呼叫程式的輸入資訊
                p.StartInfo.RedirectStandardOutput = true;// 由呼叫程式獲取輸出資訊
                p.StartInfo.RedirectStandardError = true;//重定向標準錯誤輸出
                p.StartInfo.CreateNoWindow = true; //不跳出cmd視窗
                p.StartInfo.WorkingDirectory = row.path;

                p.Start();
                p.StandardInput.WriteLine(@"git status");

                var line0 = GetOuputLine(p);//Microsoft Windows
                var line1 = GetOuputLine(p);//(c) Microsoft Corporation.
                var line2 = GetOuputLine(p);//空白行
                var line3 = GetOuputLine(p);//path
                var line4 = GetOuputLine(p);//branch

                if (line4 == "")
                {
                    //表示非 git 或無權限，往下執行會死掉
                    p.WaitForExit(50);
                    return;
                }

                var line5 = GetOuputLine(p);//
                var line6 = GetOuputLine(p);
                var line7 = GetOuputLine(p);//

                row.lines.Add(line0);
                row.lines.Add(line1);
                row.lines.Add(line2);
                row.lines.Add(line3);
                row.lines.Add(line4);
                row.lines.Add(line5);
                row.lines.Add(line6);
                row.lines.Add(line7);

                p.WaitForExit(cfg.wait_milliseconds);
            }
            catch (Exception ex)
            {
                row.is_error = true;
                row.message = ex.Message;
            }
        }

        private string GetOuputLine(System.Diagnostics.Process p)
        {
            try
            {
                var line = p.StandardOutput.ReadLine();
                if (line == null)
                {
                    return "";
                }
                else
                {
                    return line;
                }
            }
            catch
            {
                return "";
            }
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }
            public int wait_milliseconds { get; set; }
        }

        private class TRow
        {
            public string id { get; set; }
            public string name { get; set; }
            public string path { get; set; }
            public List<string> lines { get; set; }
            public bool need_commit { get; set; }
            public bool is_error { get; set; }
            public string message { get; set; }

            public string in_branch { get; set; }
            public string in_note { get; set; }
            public string in_need_commit { get; set; }

            public bool auto_commit { get; set; }
            public bool commit_result { get; set; }
            public string in_commit_note { get; set; }
        }
    }
}