﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Rowing.Common
{
    public class In_Staff_Setting : Item
    {
        public In_Staff_Setting(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;            

            /*
                目的: 工作人員相關功能
                日誌: 
                    - 2022-08-01 查詢條件增加分區選單 (Lina)
                    - 2022-06-28 聘書字號、帶入活動起迄日期、發聘日為第一天 (Lina)
                    - 2022-04-22 增加費用表 (Lina)
                    - 2021-01-14 創建 (Alan)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Staff_Setting";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string aml = "";
            string sql = "";
            string strError = "";
            _InnH.AddLog(strMethodName, "MethodSteps");

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            string mode = itmR.getProperty("mode", "");
            if (mode == "cla")
            {
                mode = "cla_";
            }

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                mode = mode,
                qry_area = itmR.getProperty("qry_area", ""),

                staffId = itmR.getProperty("id", ""),
                type = itmR.getProperty("type", ""),
                in_sno = itmR.getProperty("in_sno", ""),
                in_name = itmR.getProperty("in_name", ""),
                in_gender = itmR.getProperty("in_gender", ""),
                in_birth = itmR.getProperty("in_birth", ""),
                in_resident_add = itmR.getProperty("in_resident_add", ""),
                in_tel = itmR.getProperty("in_tel", ""),
                in_cost = int.Parse(itmR.getProperty("in_cost", "0")),
                in_job = itmR.getProperty("in_job", ""),
                in_job_name = itmR.getProperty("in_job_name", ""),
                in_area = itmR.getProperty("in_area", ""),
                in_decree = itmR.getProperty("in_decree", ""),
            };

            List<string> cols = new List<string>();
            cols.Add("in_title");
            cols.Add("in_url");
            cols.Add("in_register_url");
            cols.Add("in_need_receipt");
            cols.Add("in_meeting_type");
            cols.Add("in_echelon");
            cols.Add("in_annual");
            cols.Add("in_decree");
            cols.Add("in_date_s");
            cols.Add("in_date_e");

            aml = "<AML>" +
                "<Item type='in_" + cfg.mode + "meeting' action='get' id='" + cfg.meeting_id + "'"
                + " select='" + string.Join(", ", cols) + "'>" +
                "</Item></AML>";
            cfg.itmMeeting = inn.applyAML(aml);

            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                itmR.setProperty("permission_type", "1");
                itmR.setProperty("permission_message", "查無活動");
                return itmR;
            }

            cfg.in_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.in_url = cfg.itmMeeting.getProperty("in_url", "");
            cfg.in_register_url = cfg.itmMeeting.getProperty("in_register_url", "");
            cfg.in_meeting_type = cfg.itmMeeting.getProperty("in_meeting_type", "");
            cfg.in_need_receipt = cfg.itmMeeting.getProperty("in_need_receipt", "");
            cfg.mt_echelon = cfg.itmMeeting.getProperty("in_echelon", "");
            cfg.mt_annual = cfg.itmMeeting.getProperty("in_annual", "");
            cfg.mt_decree = cfg.itmMeeting.getProperty("in_decree", "");
            cfg.mt_date_s = cfg.itmMeeting.getProperty("in_date_s", "");
            cfg.mt_date_e = cfg.itmMeeting.getProperty("in_date_e", "");

            //取得登入者資訊
            sql = "SELECT *";
            sql += " FROM In_Resume WITH(NOLOCK)";
            sql += " WHERE in_user_id = '" + cfg.strUserId + "'";
            cfg.loginResume = inn.applySQL(sql);
            cfg.login_resume_id = cfg.loginResume.getProperty("id", "");
            cfg.login_group = cfg.loginResume.getProperty("in_group", "");//所屬單位
            cfg.login_sno = cfg.loginResume.getProperty("in_sno", "");//協助報名者身分證號
            cfg.login_name = cfg.loginResume.getProperty("in_name", "");//
            cfg.login_member_type = cfg.loginResume.getProperty("in_member_type", "");
            cfg.login_member_role = cfg.loginResume.getProperty("in_member_role", "");

            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>ALL</code>");
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmPermit.dom.InnerXml);
            cfg.isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            cfg.isCommittee = itmPermit.getProperty("isCommittee", "") == "1";
            cfg.isGymOwner = itmPermit.getProperty("isGymOwner", "") == "1";
            cfg.isGymAssistant = itmPermit.getProperty("isGymAssistant", "") == "1";

            //是否為共同講師
            bool open = GetUserResumeListStatus(cfg.inn, cfg.meeting_id, cfg.strUserId, cfg.mode);
            if (open) cfg.isMeetingAdmin = true;

            if (!cfg.isMeetingAdmin && cfg.in_meeting_type == "degree")
            {
                Item itmRoleResult = inn.applyMethod("In_Association_Role"
                    , "<in_sno>" + cfg.login_sno + "</in_sno>"
                    + "<idt_names>ACT_ASC_Degree</idt_names>");

                cfg.isMeetingAdmin = itmRoleResult.getProperty("inn_result", "") == "1";
            }

            cfg.can_review = cfg.isMeetingAdmin;

            if (!cfg.can_review)
            {
                itmR.setProperty("permission_type", "1");
                itmR.setProperty("permission_message", "您無權限瀏覽此頁面");
                return itmR;
            }

            if (itmR.getProperty("in_term_s", "") != "")
            {
                cfg.in_term_s = Convert.ToDateTime(itmR.getProperty("in_term_s", "")).ToString("yyyy-MM-ddTHH:mm:ss");
            }
            else
            {
                cfg.in_term_s = "";
            }

            if (itmR.getProperty("in_term_e", "") != "")
            {
                cfg.in_term_e = Convert.ToDateTime(itmR.getProperty("in_term_e", "")).ToString("yyyy-MM-ddTHH:mm:ss");
            }
            else
            {
                cfg.in_term_e = "";
            }

            string strTitle = "";
            if (cfg.in_meeting_type == "degree")
            {
                itmR.setProperty("meeting_type", "晉段");
                strTitle = "porStaff";
            }
            else if (cfg.in_meeting_type == "seminar")
            {
                itmR.setProperty("meeting_type", "講習");
                strTitle = "semStaff";
                itmR.setProperty("pro_func", "item_show_0");
            }
            else
            {
                itmR.setProperty("meeting_type", "競賽");
                strTitle = "gameStaff";
                itmR.setProperty("pro_func", "item_show_0");
            }

            switch (cfg.type)
            {
                case "add"://新增工作人員
                    addProStaff(cfg, itmR);
                    break;

                case "delete"://刪除工作人員
                    delProStaff(cfg, itmR);
                    break;

                case "update"://修改工作人員
                    updateProStaff(cfg, itmR);
                    break;

                case "contract"://匯出聘書
                    exportContract(cfg, itmR);
                    break;

                case "assess"://匯出考核表
                    exportAssess(cfg, itmR);
                    break;

                case "fee_xls"://匯出費用表
                    exportFeeXLS(cfg, itmR);
                    break;

                default:
                    //tab2 委員會成員清單
                    Item itmStaffs = GetStaffs(cfg);
                    AppendStaffTable(cfg, itmStaffs, itmR);

                    //職別-下拉清單
                    Item itmJobTitle = GetJobTitles(CCO, strMethodName, inn, strTitle);
                    AppendMenu(CCO, strMethodName, inn, itmJobTitle, "inn_job_title", itmR);

                    var term_s = GetDateTime(cfg.mt_date_s).AddHours(8).ToString("yyyy/MM/dd");
                    var term_e = GetDateTime(cfg.mt_date_e).AddHours(8).ToString("yyyy/MM/dd");
                    itmR.setProperty("in_decree", "中跆爐字第" + cfg.mt_decree + "    號");
                    itmR.setProperty("in_term_s", term_s);
                    itmR.setProperty("in_term_e", term_e);
                    break;
            }

            itmR.setProperty("meeting_name", cfg.in_title);
            itmR.setProperty("id", cfg.meeting_id);
            itmR.setProperty("player_group", cfg.login_group);
            itmR.setProperty("method_type", cfg.method_type);
            itmR.setProperty("inn_meeting_admin", cfg.isMeetingAdmin ? "1" : "0");
            itmR.setProperty("inn_excel_method", cfg.method_type);

            itmR.setProperty("in_title", cfg.in_title);
            itmR.setProperty("in_url", cfg.in_url);
            itmR.setProperty("in_register_url", cfg.in_register_url);
            itmR.setProperty("in_group", cfg.login_group);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        //取得共同講師 (判斷現在的登入者是否為此課程的共同講師)
        private bool GetUserResumeListStatus(Innovator inn, string meeting_id, string strUserId, string mode)
        {
            string sql = @"
                SELECT
                    t2.id
                FROM 
                    In_{#mode}Meeting_Resumelist t1 WITH(NOLOCK) 
                INNER JOIN 
                    IN_RESUME t2 WITH(NOLOCK) 
                    ON t2.id = t1.related_id    
                WHERE 
                    t1.source_id = '{#meeting_id}' 
                    AND t2.in_user_id = '{#in_user_id}';    
            ";

            sql = sql.Replace("{#mode}", mode)
                .Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_user_id}", strUserId);

            Item item = inn.applySQL(sql);

            if (item.getResult() != "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsError(Item item, bool isSingle = true)
        {
            if (item.isError() || item.getResult() == "")
            {
                return true;
            }
            else if (isSingle && item.getItemCount() != 1)
            {
                return true;
            }
            else if (item.getItemCount() <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //新增工作人員
        private void addProStaff(TConfig cfg, Item itmReturn)
        {
            string aml = "<AML><Item type='in_" + cfg.mode + "meeting_staff' action='add'>" +
                "<source_id>" + cfg.meeting_id + "</source_id>" +
                "<in_job>" + cfg.in_job + "</in_job>" +
                "<in_job_name>" + cfg.in_job_name + "</in_job_name>" +
                "<in_term_s>" + GetDateTimeStr(cfg.in_term_s, "yyyy-MM-dd") + "</in_term_s>" +
                "<in_term_e>" + GetDateTimeStr(cfg.in_term_e, "yyyy-MM-dd") + "</in_term_e>" +
                "<in_name>" + cfg.in_name + "</in_name>" +
                "<in_gender>" + cfg.in_gender + "</in_gender>" +
                "<in_sno>" + cfg.in_sno + "</in_sno>" +
                "<in_birth>" + GetDateTimeStr(cfg.in_birth, "yyyy-MM-dd") + "</in_birth>" +
                "<in_tel>" + cfg.in_tel + "</in_tel>" +
                "<in_resident_add>" + cfg.in_resident_add + "</in_resident_add>" +
                "<in_cost>" + cfg.in_cost + "</in_cost>" +
                "<in_area>" + cfg.in_area + "</in_area>" +
                "<in_decree>" + cfg.in_decree + "</in_decree>" +
                "</Item></AML>";
            cfg.inn.applyAML(aml);
        }

        //刪除工作人員
        private void delProStaff(TConfig cfg, Item itmReturn)
        {
            Item delStaff = cfg.inn.newItem("in_" + cfg.mode + "meeting_staff", "delete");
            delStaff.setAttribute("where", "[in_" + cfg.mode + "meeting_staff].id = '" + cfg.staffId + "'");
            delStaff.apply();
        }

        //更新工作人員
        private void updateProStaff(TConfig cfg, Item itmReturn)
        {
            Item updateStaff = cfg.inn.newItem("in_" + cfg.mode + "meeting_staff", "edit");
            updateStaff.setAttribute("where", "[in_" + cfg.mode + "meeting_staff].id = '" + cfg.staffId + "'");
            updateStaff.setProperty("in_job", cfg.in_job);
            updateStaff.setProperty("in_job_name", cfg.in_job_name);
            updateStaff.setProperty("in_term_s", GetDateTimeStr(cfg.in_term_s, "yyyy-MM-dd"));
            updateStaff.setProperty("in_term_e", GetDateTimeStr(cfg.in_term_e, "yyyy-MM-dd"));
            updateStaff.setProperty("in_sno", cfg.in_sno);
            updateStaff.setProperty("in_name", cfg.in_name);
            updateStaff.setProperty("in_gender", cfg.in_gender);
            updateStaff.setProperty("in_area", cfg.in_area);
            updateStaff.setProperty("in_cost", cfg.in_cost.ToString());
            updateStaff.setProperty("in_birth", GetDateTimeStr(cfg.in_birth, "yyyy-MM-dd"));
            updateStaff.setProperty("in_resident_add", cfg.in_resident_add);
            updateStaff.setProperty("in_tel", cfg.in_tel);
            updateStaff.setProperty("in_decree", cfg.in_decree);
            updateStaff.apply();
        }

        //匯出聘書
        private void exportContract(TConfig cfg, Item itmReturn)
        {
            string pathXls = "";
            string pdfName = "";
            if (cfg.in_meeting_type == "degree")
            {
                pathXls = "contract_prostaff_path";
                pdfName = cfg.in_title + "_晉段測驗人員聘書";
            }
            else
            {
                pathXls = "contract_semstaff_path";
                if (cfg.in_meeting_type == "game")
                {
                    pdfName = cfg.in_title + "_競賽工作人員聘書";
                }
                else
                {
                    pdfName = cfg.in_title + "_講習工作人員聘書";
                }
            }

            //Word Template 所在路徑與匯出路徑 (from 原創.設定參數)
            Item itmXls = cfg.inn.applyMethod("In_Variable_File", "<in_name>" + pathXls + "</in_name>");
            string exportPath = itmXls.getProperty("export_path", "");
            string templatePath = itmXls.getProperty("template_path", "");

            string pdfFile = exportPath + pdfName + ".pdf";

            Spire.Xls.Workbook book = new Spire.Xls.Workbook();
            book.LoadFromFile(templatePath);
            Spire.Xls.Worksheet sheetTemplate = book.Worksheets[0];


            Item itmStaffs = GetStaffs(cfg);

            if (itmStaffs.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "匯出聘書資料發生錯誤");
                throw new Exception("匯出聘書資料發生錯誤");
            }


            if (cfg.in_meeting_type == "degree")
            {
                AppendContract(book, sheetTemplate, cfg, itmStaffs);
            }
            else
            {
                AppendSeminarContract(book, sheetTemplate, cfg, itmStaffs);
            }


            sheetTemplate.Remove();
            book.SaveToFile(pdfFile, Spire.Xls.FileFormat.PDF);


            itmReturn.setProperty("xls_name", pdfName + ".pdf");
        }

        private void AppendContract(Spire.Xls.Workbook book, Spire.Xls.Worksheet sheetTemplate, TConfig cfg, Item itmStaffs)
        {
            int staff_count = itmStaffs.getItemCount();
            if (staff_count <= 0) return;

            DateTime currentTime = System.DateTime.Now;
            DateTime eTime = Convert.ToDateTime(cfg.mt_date_e);

            for (int i = 0; i < staff_count; i++)
            {
                Item itmStaff = itmStaffs.getItemByIndex(i);

                string in_name = itmStaff.getProperty("in_name", "");
                string in_job = itmStaff.getProperty("in_job", "");
                string in_job_name = itmStaff.getProperty("in_job_name", "");
                string in_term_e = itmStaff.getProperty("in_term_e", "");
                string in_area = itmStaff.getProperty("in_area", "");

                string in_gender = "";
                if (in_job == "場地費")
                {
                    continue;
                }

                if (in_name == "")
                {
                    in_name = "　　　";
                }

                string chinese_day = "";

                if (itmStaff.getProperty("in_gender", "") == "男")
                {
                    in_gender = "先生";
                }
                else if (itmStaff.getProperty("in_gender", "") == "女")
                {
                    in_gender = "女士";
                }
                else
                {
                    in_gender = "君";
                }

                string contractText = "茲敦聘 {#in_name} {#in_gender}擔任本會{#annual}年第{#echelon}梯次晉段測驗{#areaStr}{#in_job_name}";

                contractText = contractText.Replace("{#in_name}", in_name)
                                            .Replace("{#in_gender}", in_gender)
                                            .Replace("{#in_job_name}", in_job_name)
                                            .Replace("{#echelon}", cfg.mt_echelon)
                                            .Replace("{#areaStr}", in_area)
                                            .Replace("{#annual}", cfg.mt_annual);

                Spire.Xls.Worksheet sheet = book.CreateEmptySheet();

                sheet.CopyFrom(sheetTemplate);
                sheet.Name = itmStaff.getProperty("id", "");
                //2022-08-22 Willy Excel內容改位置
                //sheet.Range["B17"].Text = contractText;   //聘書內文
                sheet.Range["C17"].Text = contractText;   //聘書內文

                if (in_term_e != "")
                {
                    chinese_day = TwDay(Convert.ToDateTime(in_term_e), DayType.Long);
                }
                else
                {
                    chinese_day = TwDay(eTime, DayType.Long);
                }

                //2022-08-22 Willy Excel內容改位置
                //sheet.Range["B43"].Text = chinese_day;   //日期
                sheet.Range["B42"].Text = chinese_day;   //日期

                sheet.PageSetup.FitToPagesWide = 1;
                sheet.PageSetup.FitToPagesTall = 1;
                sheet.PageSetup.TopMargin = 0.3;
                sheet.PageSetup.LeftMargin = 0.5;
                sheet.PageSetup.RightMargin = 0.5;
                sheet.PageSetup.BottomMargin = 0.5;

                //適合頁面
                book.ConverterSetting.SheetFitToPage = true;
            }
        }

        private void AppendSeminarContract(Spire.Xls.Workbook book, Spire.Xls.Worksheet sheetTemplate, TConfig cfg, Item itmStaffs)
        {
            int staff_count = itmStaffs.getItemCount();
            DateTime sTime = Convert.ToDateTime(cfg.mt_date_s);
            DateTime eTime = Convert.ToDateTime(cfg.mt_date_e);
            string chinese_day = TwDay(sTime, DayType.Long);

            if (staff_count != 0)
            {
                for (int i = 0; i < staff_count; i++)
                {
                    Item itmStaff = itmStaffs.getItemByIndex(i);

                    string in_name = itmStaff.getProperty("in_name", "");
                    string in_job = itmStaff.getProperty("in_job", "");
                    string in_job_name = itmStaff.getProperty("in_job_name", "");
                    string in_term_s = itmStaff.getProperty("in_term_s", "");
                    string in_term_e = itmStaff.getProperty("in_term_e", "");
                    string in_decree = itmStaff.getProperty("in_decree", "");
                    string in_gender = "";

                    if (in_name == "")
                    {
                        in_name = "　　　";
                    }


                    if (itmStaff.getProperty("in_gender", "") == "男")
                    {
                        in_gender = "先生";
                    }
                    else if (itmStaff.getProperty("in_gender", "") == "女")
                    {
                        in_gender = "女士";
                    }
                    else
                    {
                        in_gender = "君";
                    }

                    Spire.Xls.Worksheet sheet = book.CreateEmptySheet();

                    sheet.CopyFrom(sheetTemplate);
                    sheet.Name = itmStaff.getProperty("id", "");
                    //2022-08-22 Willy Excel內容改位置
                    //sheet.Range["B16"].Text = in_decree;//"中跆爐字第" + cfg.in_decree + "號";   //文號
                    //sheet.Range["D19"].Text = in_name + "　" + in_gender;   //姓名
                    //sheet.Range["D22"].Text = cfg.in_title.Replace("中華民國划船協會", "").Trim();  //項目
                    //sheet.Range["D26"].Text = in_job_name;                       //職稱
                    sheet.Range["C15"].Text = in_decree;//"中跆爐字第" + cfg.in_decree + "號";   //文號
                    sheet.Range["E17"].Text = in_name + "　" + in_gender;   //姓名
                    sheet.Range["E20"].Text = cfg.in_title.Replace("中華民國划船協會", "").Trim();  //項目
                    sheet.Range["E23"].Text = in_job_name;                       //職稱
                    if (in_term_s != "")
                    {
                        //sheet.Range["D31"].Text = "自民國" + TwDay(Convert.ToDateTime(in_term_s), DayType.Short) + "起";
                        sheet.Range["E25"].Text = "自民國" + TwDay(Convert.ToDateTime(in_term_s), DayType.Short) + "起";
                    }
                    else
                    {
                        //sheet.Range["D31"].Text = "自民國" + TwDay(sTime, DayType.Short) + "起";
                        sheet.Range["E25"].Text = "自民國" + TwDay(sTime, DayType.Short) + "起";
                    }
                    if (in_term_e != "")
                    {
                        //sheet.Range["D33"].Text = "至民國" + TwDay(Convert.ToDateTime(in_term_e), DayType.Short) + "止";
                        sheet.Range["E27"].Text = "至民國" + TwDay(Convert.ToDateTime(in_term_e), DayType.Short) + "止";
                    }
                    else
                    {
                        //sheet.Range["D33"].Text = "至民國" + TwDay(eTime, DayType.Short) + "止";
                        sheet.Range["E27"].Text = "至民國" + TwDay(eTime, DayType.Short) + "止";
                    }

                    //sheet.Range["B43"].Text = chinese_day;                  //日期
                    sheet.Range["B44"].Text = chinese_day;                  //日期

                    sheet.PageSetup.FitToPagesWide = 1;
                    sheet.PageSetup.FitToPagesTall = 1;
                    sheet.PageSetup.TopMargin = 0.3;
                    sheet.PageSetup.LeftMargin = 0.5;
                    sheet.PageSetup.RightMargin = 0.5;
                    sheet.PageSetup.BottomMargin = 0.5;

                    //適合頁面
                    book.ConverterSetting.SheetFitToPage = true;
                }
            }

        }

        //匯出費用表
        private void exportFeeXLS(TConfig cfg, Item itmReturn)
        {
            Item itmStaffs = GetStaffs(cfg);

            if (itmStaffs.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "匯出費用表資料發生錯誤");
                throw new Exception("匯出費用表資料發生錯誤");
            }

            if (itmStaffs.getResult() == "" || itmStaffs.getItemCount() <= 0)
            {
                throw new Exception("查無費用表資料");
            }

            //Word Template 所在路徑與匯出路徑 (from 原創.設定參數)
            Item itmXls = cfg.inn.applyMethod("In_Variable_File", "<in_name></in_name>");
            string exportPath = itmXls.getProperty("export_path", "");

            string xlsName = cfg.in_title + "_費用表";
            string xlsFile = exportPath + xlsName + ".xlsx";

            Spire.Xls.Workbook book = new Spire.Xls.Workbook();

            //費用表 Sheet
            AppendFeeSheet(book, cfg, itmStaffs);

            //移除空白 Sheet
            book.Worksheets[0].Remove();
            book.Worksheets[0].Remove();
            book.Worksheets[0].Remove();

            book.SaveToFile(xlsFile);
            // book.SaveToFile(pdfFile, Spire.Xls.FileFormat.PDF);

            itmReturn.setProperty("xls_name", xlsName + ".xlsx");
        }

        //費用表
        private void AppendFeeSheet(Spire.Xls.Workbook book, TConfig cfg, Item itmStaffs)
        {
            var dic = MapAreaItems(itmStaffs);

            var currentTime = System.DateTime.Now;
            var chinese_day = TwDay(currentTime, DayType.Short);

            foreach (var kv in dic)
            {
                var in_area = kv.Key;
                var list = kv.Value;

                Spire.Xls.Worksheet sheet = book.CreateEmptySheet();
                sheet.Name = in_area;
                //sheet.PageSetup.PaperSize = Spire.Xls.PaperSizeType.PaperA3;
                sheet.PageSetup.Orientation = Spire.Xls.PageOrientationType.Landscape;

                //頁面配置-邊界
                sheet.PageSetup.TopMargin = 0.5;
                sheet.PageSetup.BottomMargin = 0.5;
                sheet.PageSetup.LeftMargin = 0.5;
                sheet.PageSetup.RightMargin = 0.5;

                //列印標題
                sheet.PageSetup.PrintTitleRows = "$1:$3";

                string in_title = cfg.in_title + "費用表";

                int wsMin = 1;
                int wsRow = 1;

                //活動標題
                var meeting_pos = "A" + wsRow + ":" + "I" + wsRow;
                var meeting_mr = sheet.Range[meeting_pos];
                meeting_mr.Merge();
                meeting_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                meeting_mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
                meeting_mr.Style.Font.FontName = "標楷體";
                meeting_mr.Style.Font.IsBold = false;
                meeting_mr.Text = in_title;
                meeting_mr.Style.Font.Size = 18;
                sheet.SetRowHeight(wsRow, 30);
                wsRow++;

                //日期列
                var date_pos = "A" + wsRow + ":" + "I" + wsRow;
                var date_mr = sheet.Range[date_pos];
                date_mr.Merge();
                date_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                date_mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
                date_mr.Style.Font.FontName = "標楷體";
                date_mr.Style.Font.IsBold = false;
                date_mr.Text = "日期：" + chinese_day;
                date_mr.Style.Font.Size = 12;
                sheet.SetRowHeight(wsRow, 24);
                wsRow++;

                var fields = new List<TField>();
                fields.Add(new TField { cs = "A", title = "編號", property = "no", css = CssType.None, width = 4 });
                fields.Add(new TField { cs = "B", title = "職稱", property = "in_job_name", css = CssType.None, width = 14 });
                fields.Add(new TField { cs = "C", title = "姓名", property = "in_name", css = CssType.None, width = 10 });
                fields.Add(new TField { cs = "D", title = "出生" + Environment.NewLine + "年月日", property = "tw_birth", css = CssType.None, width = 12 });
                fields.Add(new TField { cs = "E", title = "身分證字號", property = "in_sno", css = CssType.None, width = 14 });
                fields.Add(new TField { cs = "F", title = "戶籍地址", property = "in_resident_add", css = CssType.Left, width = 38 });
                fields.Add(new TField { cs = "G", title = "電話", property = "in_tel", css = CssType.Phone, width = 15 });
                fields.Add(new TField { cs = "H", title = "費用", property = "in_cost", css = CssType.Money, width = 10 });
                fields.Add(new TField { cs = "I", title = "簽章", css = CssType.None, width = 16 });

                //標題列
                foreach (var field in fields)
                {
                    FeeHeadCell(cfg, sheet, field, wsRow);
                }
                wsRow++;

                //凍結窗格
                sheet.FreezePanes(3, fields.Count);

                for (int i = 0; i < list.Count; i++)
                {
                    Item itmStaff = list[i];

                    var tw_birth = "";
                    var dt_birth = GetDateTime(itmStaff.getProperty("in_birth", ""));
                    if (dt_birth != DateTime.MinValue)
                    {
                        tw_birth = TwDay(dt_birth, DayType.Easy);
                    }

                    itmStaff.setProperty("no", (i + 1).ToString());
                    itmStaff.setProperty("tw_birth", tw_birth);

                    foreach (var field in fields)
                    {
                        FeeBodyCell(cfg, sheet, field, wsRow, itmStaff);
                    }
                    sheet.SetRowHeight(wsRow, 32.4);

                    wsRow++;
                }

                //設定格線
                SetRangeBorder(sheet, fields, wsMin, wsRow);

                //設定欄寬
                SetColumnWidth(sheet, fields);

            }
        }

        private void SetColumnWidth(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                sheet.Columns[i].ColumnWidth = field.width;
            }
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, List<TField> fields, int mnRow, int wsRow)
        {
            var tbl_ps = fields.First().cs + mnRow;
            var tbl_pe = fields.Last().cs + (wsRow - 1);
            var pos = tbl_ps + ":" + tbl_pe;

            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        private void FeeBodyCell(TConfig cfg, Spire.Xls.Worksheet sheet, TField field, int wsRow, Item item)
        {
            var val = "";
            if (!string.IsNullOrWhiteSpace(field.property))
            {
                val = item.getProperty(field.property, "");
            }

            var pos = field.cs + wsRow;

            var range = sheet.Range[pos];
            range.Style.Font.FontName = "標楷體";
            range.Style.Font.IsBold = false;
            range.Text = field.title;
            range.Style.Font.Size = 12;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;

            switch (field.css)
            {
                case CssType.Text:
                    range.Text = "'" + val;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;

                case CssType.Left:
                    range.Value = val;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;
                    break;

                case CssType.Money:
                    if (val != "") range.Value = val.Replace("元", "") + "元";
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;

                case CssType.Phone:
                    var obj = PhoneObj(val);
                    if (obj.is_phone_number)
                    {
                        range.NumberValue = obj.code;
                        range.Style.NumberFormat = "0000-000-000";
                        range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    }
                    else
                    {
                        range.Value = "'" + val;
                        range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    }
                    break;

                default:
                    range.Value = val;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
            }
            range.IsWrapText = true;
        }

        private void FeeHeadCell(TConfig cfg, Spire.Xls.Worksheet sheet, TField field, int wsRow)
        {
            var pos = field.cs + wsRow;

            var range = sheet.Range[pos];
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Style.Font.FontName = "標楷體";
            range.Style.Font.IsBold = false;
            range.Text = field.title;
            range.Style.Font.Size = 14;
            //sheet.SetRowHeight(wsRow, 30);
            range.IsWrapText = true;
        }

        //匯出考核表
        private void exportAssess(TConfig cfg, Item itmReturn)
        {
            Item itmStaffs = GetStaffs(cfg);

            if (itmStaffs.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "匯出考核表資料發生錯誤");
                throw new Exception("匯出考核表資料發生錯誤");
            }

            if (itmStaffs.getResult() == "" || itmStaffs.getItemCount() <= 0)
            {
                throw new Exception("查無考核表資料");
            }

            //Word Template 所在路徑與匯出路徑 (from 原創.設定參數)
            Item itmXls = cfg.inn.applyMethod("In_Variable_File", "<in_name>assess_proStaff_path</in_name>");
            string exportPath = itmXls.getProperty("export_path", "");
            string templatePath = itmXls.getProperty("template_path", "");

            string xlsName = cfg.in_title + "_晉段測驗人員考核表";
            string xlsFile = exportPath + xlsName + ".xlsx";

            Spire.Xls.Workbook book = new Spire.Xls.Workbook();
            book.LoadFromFile(templatePath);
            Spire.Xls.Worksheet sheetTemplate = book.Worksheets[0];

            AppendAssess(book, sheetTemplate, cfg, itmStaffs);

            sheetTemplate.Remove();

            book.SaveToFile(xlsFile);
            // book.SaveToFile(pdfFile, Spire.Xls.FileFormat.PDF);

            itmReturn.setProperty("xls_name", xlsName + ".xlsx");
        }

        //考核表
        private void AppendAssess(Spire.Xls.Workbook book, Spire.Xls.Worksheet sheetTemplate, TConfig cfg, Item itmStaffs)
        {
            var dic = MapAreaItems(itmStaffs);

            var currentTime = System.DateTime.Now;
            var chinese_day = TwDay(currentTime, DayType.Short);

            foreach (var kv in dic)
            {
                var in_area = kv.Key;
                var list = kv.Value;

                Spire.Xls.Worksheet sheet = book.CreateEmptySheet();
                sheet.CopyFrom(sheetTemplate);
                sheet.Name = in_area + "考核表";

                string in_title = "中華民國划船協會　{#echelon}梯次{#areaStr}晉段測驗官考核表"
                    .Replace("{#echelon}", cfg.mt_echelon)
                    .Replace("{#areaStr}", in_area);

                sheet.Range["A1"].Text = in_title;
                sheet.Range["A23"].Text = chinese_day;

                int row = 5;

                for (int i = 0; i < list.Count; i++)
                {
                    Item itmStaff = list[i];

                    string in_name = itmStaff.getProperty("in_name", "");
                    string in_job = itmStaff.getProperty("in_job", "");
                    string in_job_name = itmStaff.getProperty("in_job_name", "");

                    // //民國年月日 (8碼): 開立日期: 108 年 12 月  27 日 
                    // string y = (currentTime.Year - 1911).ToString();
                    // string m = currentTime.Month.ToString();
                    // string d = currentTime.Day.ToString();
                    sheet.Range["A" + (row + i).ToString()].Text = (i + 1).ToString();
                    sheet.Range["B" + (row + i).ToString()].Text = in_job_name;
                    sheet.Range["C" + (row + i).ToString()].Text = in_name;
                }
            }
        }

        private Dictionary<string, List<Item>> MapAreaItems(Item items)
        {
            Dictionary<string, List<Item>> result = new Dictionary<string, List<Item>>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                string in_area = item.getProperty("in_area", "");
                if (in_area == "" || in_area == "請選擇")
                {
                    in_area = "未分區";
                }

                List<Item> list = null;
                if (!result.ContainsKey(in_area))
                {
                    list = new List<Item>();
                    result.Add(in_area, list);
                }
                else
                {
                    list = result[in_area];
                }

                list.Add(item);
            }

            return result;
        }

        //取得區域
        private string GetAreas(TConfig cfg)
        {
            string sql = @"
                    SELECT
                        t2.*
                    FROM
                        in_{#mode}meeting t1 WITH(NOLOCK)
                    INNER JOIN
                        In_{#mode}Meeting_Area t2 WITH(NOLOCK)
                        ON t2.source_id = t1.id
                    WHERE
                        t1.id = '{#meeting_id}'
                    ORDER BY in_area 
                ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                    .Replace("{#mode}", cfg.mode);
            Item itmAreas = cfg.inn.applySQL(sql);
            string areaStr = "";
            int area_count = itmAreas.getItemCount();
            if (!itmAreas.isError() && area_count > 0)
            {
                for (int i = 0; i < area_count; i++)
                {
                    Item itmArea = itmAreas.getItemByIndex(i);


                    string in_area = itmArea.getProperty("in_area", "");
                    string is_allow = itmArea.getProperty("is_allow", "");

                    if (is_allow == "1")
                    {
                        if (i > 0)
                        {
                            areaStr += "、";
                        }
                        areaStr += in_area;
                    }
                }

            }


            return areaStr;
        }

        //職別
        private Item GetJobTitles(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string strTitle)
        {
            string sql = @"
                    SELECT '---請選擇---' AS 'label', '' AS 'value' ,0 AS SORT_ORDER
                    UNION ALL
                    SELECT 
                        LABEL_ZT AS 'label'
                        , LABEL_ZT AS 'value'
                        , SORT_ORDER
                    FROM 
                        [list] AS T1 WITH (NOLOCK)
                    JOIN 
                        [value] AS T2 WITH (NOLOCK)
                    ON T1.id =T2.SOURCE_ID
                    WHERE 
                        name ='In_Meeting_JobTitle'
						AND VALUE like'{#strTitle}%'
                    ORDER BY 
                        SORT_ORDER
                ";
            sql = sql.Replace("{#strTitle}", strTitle);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);

        }

        //下拉選單
        private void AppendMenu(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string list_name, string type_name, Item itmReturn)
        {
            Item items = GetValues(CCO, strMethodName, inn, list_name: list_name);
            AppendMenu(CCO, strMethodName, inn, items, type_name, itmReturn);

        }
        //下拉選單
        private void AppendMenu(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item items, string type_name, Item itmReturn)
        {
            if (items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                item.setType(type_name);
                item.setProperty("inn_label", item.getProperty("label", ""));
                item.setProperty("inn_value", item.getProperty("value", ""));
                itmReturn.addRelationship(item);
            }

        }
        //取得下拉選單
        private Item GetValues(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string list_name)
        {
            string sql = @"
                    SELECT
                        t2.value
    			        , t2.label_zt AS 'label'
                    FROM
                        [LIST] t1 WITH(NOLOCK)
    		        INNER JOIN
    			        [VALUE] t2 WITH(NOLOCK)
    			        ON t2.source_id = t1.id
                    WHERE
    			        t1.name = N'{#list_name}'
    		        ORDER BY
    			        t2.sort_order
                ";

            sql = sql.Replace("{#list_name}", list_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //取得測驗人員清單
        private Item GetStaffs(TConfig cfg)
        {
            string area_cond = cfg.qry_area == ""
                ? ""
                : "AND t1.in_area = N'" + cfg.qry_area + "'";

            string sqlFilter = "";
            if (cfg.type == "contract" && cfg.staffId != "")
            {
                sqlFilter = " AND T1.id = '" + cfg.staffId + "'";
            }

            string sql = @"
                SELECT 
	                t1.id
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_gender
	                , DATEADD(HH, 8, t1.in_birth) AS 'in_birth'
	                , t1.in_resident_add
	                , t1.in_tel
	                , t1.in_job
	                , t1.in_job_name
	                , t1.in_cost
	                , DATEADD(HH, 8, t1.in_term_s) AS 'in_term_s'
	                , DATEADD(HH, 8, t1.in_term_e) AS 'in_term_e'
                    , t1.in_area
                    , t1.in_decree
                FROM 
	                IN_{#mode}MEETING_STAFF t1 WITH (NOLOCK)
                INNER JOIN 
	                [VU_Mt_JobTitle] t2
	                ON t2.label_zt = t1.in_job
	            LEFT OUTER JOIN
	                [VU_Mt_Area] t3
	                ON t3.value = t1.in_area
                WHERE 
	                t1.source_id = '{#meeting_id}' 
	                {#sqlFilter} {#area_cond}
                ORDER BY 
	                t3.sort_order
	                , t2.sort_order
	                , t1.created_on
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                        .Replace("{#sqlFilter}", sqlFilter)
                        .Replace("{#area_cond}", area_cond)
                        .Replace("{#mode}", cfg.mode.ToUpper());

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //建立測驗人員清單 Tab
        private void AppendStaffTable(TConfig cfg, Item items, Item itmReturn)
        {
            List<TField> fields = new List<TField>();
            // fields.Add(new TField { property = "in_job", title = "職別", css = "text-left" });
            fields.Add(new TField { property = "in_job_name", title = "職名", css = CssType.Left });
            fields.Add(new TField { property = "in_name", title = "姓名", css = CssType.Left });
            // fields.Add(new TField { property = "in_sno", title = "身分證字號", css = "text-left" });
            fields.Add(new TField { property = "in_gender", title = "性別", css = CssType.Left });
            fields.Add(new TField { property = "in_area", title = "分區", css = CssType.Left });
            fields.Add(new TField { property = "in_cost", title = "費用", css = CssType.Left });
            fields.Add(new TField { property = "in_func", title = "功能", css = CssType.Left });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            foreach (var field in fields)
            {
                head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true'>" + field.title + "</th>");
            }
            head.AppendLine("</thead>");

            int count = items.getItemCount(); ;

            body.AppendLine("<tbody>");
            string btnStr = "";
            for (int i = 0; i < count; i++)
            {
                btnStr = "";
                Item item = items.getItemByIndex(i);
                List<staffInfo> staff = new List<staffInfo>();

                var obj = MapObj(item);

                string termStr = "";

                if (obj.in_term_s != "" || obj.in_term_e != "")
                {
                    termStr = "<BR>(任期：" + obj.in_term_s + "~" + obj.in_term_e + ")";
                }

                staff.Add(obj);

                string msg = obj.in_job_name + item.getProperty("in_name", "");
                body.AppendLine("<tr>");
                if (obj.in_job != "場地費")
                {
                    btnStr += "<button class=\"btn btn-primary\" onclick=\"exportContract('" + obj.staffId + "')\"><i class=\"fa fa-print\"></i></button>&nbsp";
                }
                btnStr += "<button class=\"btn btn-success\" onclick=\"update_proStaff(" + Newtonsoft.Json.JsonConvert.SerializeObject(staff).Replace("\"", "'") + ")\"><i class=\"fa fa-pencil-square-o\"></i></button>&nbsp";
                btnStr += "<button class=\"btn btn-danger\" onclick=\"delete_proStaff('" + obj.staffId + "','" + msg + "')\"><i class=\"fa fa-trash-o\"></i></button>";

                item.setProperty("in_func", btnStr);

                foreach (var field in fields)
                {
                    if (field.property == "in_job_name")
                    {
                        body.AppendLine("<td class='" + field.css + "' data-field='" + field.property + "' >" + field.GetStr(item) + termStr + "</td>");
                    }
                    else
                    {
                        body.AppendLine("<td class='" + field.css + "' data-field='" + field.property + "' >" + field.GetStr(item) + "</td>");
                    }

                }

                body.AppendLine("</tr>");
            }

            body.AppendLine("</tbody>");


            string table_name = "resume_table";

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            itmReturn.setProperty("inn_StaffTable", builder.ToString());
        }

        private staffInfo MapObj(Item item)
        {
            string in_birth = item.getProperty("in_birth", "");
            in_birth = GetDateTimeStr(in_birth, "yyyy/MM/dd");

            string in_term_s = item.getProperty("in_term_s", "");
            in_term_s = GetDateTimeStr(in_term_s, "yyyy/MM/dd");

            string in_term_e = item.getProperty("in_term_e", "");
            in_term_e = GetDateTimeStr(in_term_e, "yyyy/MM/dd");

            staffInfo result = new staffInfo
            {
                staffId = item.getProperty("id", ""),
                in_job = item.getProperty("in_job", ""),
                in_job_name = item.getProperty("in_job_name", ""),
                in_name = item.getProperty("in_name", ""),
                in_sno = item.getProperty("in_sno", ""),
                in_gender = item.getProperty("in_gender", ""),
                in_cost = item.getProperty("in_cost", ""),
                in_term_s = in_term_s,
                in_term_e = in_term_e,
                in_birth = in_birth,
                in_area = item.getProperty("in_area", ""),
                in_resident_add = item.getProperty("in_resident_add", ""),
                in_tel = item.getProperty("in_tel", ""),
                in_decree = item.getProperty("in_decree", ""),
            };

            return result;
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "'"
                + " data-toggle='table' "
                + " data-click-to-select='true' "
                + " data-striped='false' "
                + " data-search='false' "
                + ">";
        }

        private string TwDay(DateTime CDay, DayType e)
        {
            string result = "";

            string y = (CDay.Year - 1911).ToString();
            string m = CDay.Month.ToString("00");
            string d = CDay.Day.ToString("00");

            switch (e)
            {
                case DayType.Long:
                    result = " 中華民國　" + y + "　年　" + m + "　月　" + d + "　日";
                    break;

                case DayType.Short:
                    result = y + "年" + m + "月" + d + "日";
                    break;

                case DayType.Easy:
                    result = y + "/" + m + "/" + d;
                    break;

                default:
                    break;
            }

            return result;
        }

        private enum DayType
        {
            Long = 1,
            Short = 2,
            Easy = 3,
        }

        private enum CssType
        {
            None = 0,
            Left = 1,
            Money = 2,
            Text = 3,
            Phone = 4,
        }

        private class TField
        {
            public string cs { get; set; }
            public string property { get; set; }
            public string title { get; set; }
            public CssType css { get; set; }

            public bool hide { get; set; }
            public double width { get; set; }

            public string GetStr(Item item)
            {
                return item.getProperty(this.property, "");
            }

        }

        private class staffInfo
        {
            public string staffId { get; set; }
            public string in_job { get; set; }
            public string in_job_name { get; set; }
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_gender { get; set; }
            public string in_birth { get; set; }
            public string in_cost { get; set; }
            public string in_term_s { get; set; }
            public string in_term_e { get; set; }
            public string in_area { get; set; }
            public string in_resident_add { get; set; }
            public string in_tel { get; set; }
            public string in_decree { get; set; }
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            //外部輸入區

            /// <summary>
            /// 活動 id
            /// </summary>
            public string meeting_id { get; set; }

            /// <summary>
            /// 模式 (cla 為課程)
            /// </summary>
            public string mode { get; set; }

            /// <summary>
            /// 分區
            /// </summary>
            public string qry_area { get; set; }

            /// <summary>
            /// 測試-顯示未繳費的單子
            /// </summary>
            public string s_type { get; set; }



            //權限區

            /// <summary>
            /// 最高管理者
            /// </summary>
            public bool isMeetingAdmin { get; set; }

            /// <summary>
            /// 地區管理者
            /// </summary>
            public bool isCommittee { get; set; }

            /// <summary>
            /// 道館主
            /// </summary>
            public bool isGymOwner { get; set; }

            /// <summary>
            /// 道館協助
            /// </summary>
            public bool isGymAssistant { get; set; }

            /// <summary>
            /// 可以審核
            /// </summary>
            public bool can_review { get; set; }



            //登入者資訊區

            /// <summary>
            /// 登入者 Resume 資料
            /// </summary>
            public Item loginResume { get; set; }

            /// <summary>
            /// 登入者 Resume id
            /// </summary>
            public string login_resume_id { get; set; }

            /// <summary>
            /// 登入者 會員類型
            /// </summary>
            public string login_member_type { get; set; }

            /// <summary>
            /// 登入者 會員角色
            /// </summary>
            public string login_member_role { get; set; }

            /// <summary>
            /// 登入者 所屬群組
            /// </summary>
            public string login_group { get; set; }

            /// <summary>
            /// 登入者 身分證號
            /// </summary>
            public string login_sno { get; set; }

            /// <summary>
            /// 登入者 姓名
            /// </summary>
            public string login_name { get; set; }


            // Meeting 區

            /// <summary>
            /// Meeting 資料
            /// </summary>
            public Item itmMeeting { get; set; }

            /// <summary>
            /// 活動名稱
            /// </summary>
            public string in_title { get; set; }

            /// <summary>
            /// 專屬展示網址
            /// </summary>
            public string in_url { get; set; }

            /// <summary>
            /// 專屬報名網址
            /// </summary>
            public string in_register_url { get; set; }

            /// <summary>
            /// 活動類型
            /// </summary>
            public string in_meeting_type { get; set; }

            /// <summary>
            /// 梯次
            /// </summary>
            public string mt_echelon { get; set; }

            /// <summary>
            /// 年度
            /// </summary>
            public string mt_annual { get; set; }

            /// <summary>
            /// 文號
            /// </summary>
            public string mt_decree { get; set; }

            /// <summary>
            /// 課程起
            /// </summary>
            public string mt_date_s { get; set; }

            /// <summary>
            /// 課程迄
            /// </summary>
            public string mt_date_e { get; set; }

            /// <summary>
            /// 需下載收據功能
            /// </summary>
            public string in_need_receipt { get; set; }

            public string SQL_OnlyMyGym { get; set; }
            public string group { get; set; }
            public string open_function { get; set; }
            public string method_type { get; set; }

            public string in_sno { get; set; }
            public string in_name { get; set; }
            public string in_gender { get; set; }
            public string in_birth { get; set; }
            public int in_cost { get; set; }
            public string in_job { get; set; }
            public string in_job_name { get; set; }
            public string in_term_s { get; set; }
            public string in_term_e { get; set; }
            public string in_area { get; set; }
            public string in_resident_add { get; set; }
            public string in_tel { get; set; }
            public string in_decree { get; set; }

            public string type { get; set; }
            public string staffId { get; set; }
        }

        private class TTEL
        {
            public string source { get; set; }
            public string value { get; set; }
            public bool is_phone_number { get; set; }
            public int code { get; set; }
        }

        private DateTime GetDateTime(string value)
        {
            if (value == "") return DateTime.MinValue;

            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value.Replace("/", "-"), out result);
            if (result == DateTime.MinValue) return DateTime.MinValue;

            return result;
        }

        private string GetDateTimeStr(string value, string format)
        {
            if (value == "") return "";

            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value.Replace("/", "-"), out result);
            if (result == DateTime.MinValue) return "";

            return result.ToString(format);
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        private TTEL PhoneObj(string source)
        {
            var result = new TTEL { source = source, is_phone_number = false };

            if (source != "")
            {
                string value = source.Replace("+886", "0")
                    .Replace("-", "")
                    .Replace(" ", "")
                    .Replace(".", "");

                result.value = value;
                result.is_phone_number = value.Length == 10 && value.StartsWith("09");

                if (result.is_phone_number)
                {
                    result.code = GetIntVal(value);
                }
            }

            return result;


        }
    }
}
