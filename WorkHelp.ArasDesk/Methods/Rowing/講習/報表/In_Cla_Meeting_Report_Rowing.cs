﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Rowing.Common
{
    public class In_Cla_Meeting_Report_Rowing : Item
    {
        public In_Cla_Meeting_Report_Rowing(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 划船報表
                日誌: 
                    - 2023-01-13: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Cla_Meeting_Report_Rowing";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
                mode = itmR.getProperty("mode", ""),
                qry_id = itmR.getProperty("id", ""),
                qry_area = itmR.getProperty("area", ""),
            };

            cfg.meeting_table = "IN_MEETING";
            cfg.muser_table = "IN_MEETING_USER";
            cfg.mresume_table = "IN_MEETING_RESUME";
            cfg.mstaff_table = "IN_MEETING_STAFF";
            if (cfg.mode == "cla")
            {
                cfg.meeting_table = "IN_CLA_MEETING";
                cfg.muser_table = "IN_CLA_MEETING_USER";
                cfg.mresume_table = "IN_CLA_MEETING_RESUME";
                cfg.mstaff_table = "IN_CLA_MEETING_STAFF";
            }


            switch (cfg.scene)
            {
                case "study_certificate"://研習證書-學員清冊下載
                    RunExport(cfg, itmR, new TCtrl
                    {
                        item_desc = "學員資料",
                        getItems = GetMUserItems,
                        appendSheet = AppendStudyCertificateSheet,
                    });
                    break;

                case "staff_contract"://工作人員-聘書下載
                    RunExport(cfg, itmR, new TCtrl
                    {
                        item_desc = "工作人資料",
                        getItems = GetMStaffItems,
                        appendSheet = AppendStaffContractSheet,
                    });
                    break;
            }

            return itmR;
        }

        private void RunExport(TConfig cfg, Item itmReturn, TCtrl ctrl)
        {
            cfg.itmMeeting = GetMeetingItem(cfg);
            AppendMeetingInfo(cfg);

            var items = ctrl.getItems(cfg);
            var count = items.getItemCount();

            if (count <= 0)
            {
                throw new Exception("查無" + ctrl.item_desc);
            }


            var xlsx = GetXlsx(cfg);
            var book = new Spire.Xls.Workbook();

            //附加工作頁
            ctrl.appendSheet(cfg, book, items);

            book.Worksheets[0].Remove();
            book.Worksheets[0].Remove();
            book.Worksheets[0].Remove();

            //儲存
            book.SaveToFile(xlsx.file, Spire.Xls.ExcelVersion.Version2013);

            //輸出檔案名稱
            itmReturn.setProperty("xls_name", xlsx.name);
        }

        //研習證書-學員清冊下載
        private void AppendStudyCertificateSheet(TConfig cfg, Spire.Xls.Workbook book, Item itmMUsers)
        {
            var sheet = book.CreateEmptySheet();
            sheet.Name = "學員清冊";

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "學員編號", property = "in_name_num", css = TCss.Center, width = 10 });
            fields.Add(new TField { title = "學員姓名", property = "in_name", css = TCss.None, width = 10 });
            fields.Add(new TField { title = "講習名稱", property = "mt_title", css = TCss.None, width = 45 });
            fields.Add(new TField { title = "講習類型", property = "mt_seminar_label", css = TCss.Center, width = 10 });
            fields.Add(new TField { title = "講習級別", property = "mt_level", css = TCss.Center, width = 10 });
            fields.Add(new TField { title = "研習日期", property = "inn_study_date", css = TCss.Center, width = 15 });
            fields.Add(new TField { title = "研習時數", property = "mt_hours", css = TCss.Number, width = 10 });
            fields.Add(new TField { title = "發證日期", property = "inn_cert_date", css = TCss.Center, width = 15 });
            fields.Add(new TField { title = "內文", property = "inn_contents", css = TCss.None, width = 24 });

            MapCharSetAndIndex(cfg, fields);


            int mnRow = 1;
            int wsRow = 1;

            //標題列
            SetHeadRow(sheet, wsRow, fields);
            wsRow++;

            var count = itmMUsers.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = itmMUsers.getItemByIndex(i);
                var in_l1 = item.getProperty("in_l1", "");
                var hours = cfg.mt_hours;
                if (in_l1.Contains("不含檢定")) hours = "6";

                item.setProperty("mt_title", cfg.mt_title);
                item.setProperty("mt_seminar_label", cfg.mt_seminar_label);
                item.setProperty("mt_level", cfg.mt_level);
                item.setProperty("inn_study_date", cfg.mt_date_s.tw_date3);
                item.setProperty("mt_hours", hours);
                item.setProperty("inn_cert_date", cfg.mt_date_s.tw_date1);
                item.setProperty("inn_contents", cfg.mt_study_contents);
                SetItemCell(cfg, sheet, wsRow, item, fields);

                wsRow++;
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        //工作人員-聘書下載
        private void AppendStaffContractSheet(TConfig cfg, Spire.Xls.Workbook book, Item itmMUsers)
        {
            var sheet = book.CreateEmptySheet();
            sheet.Name = "工作人員清冊";

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "姓名", property = "in_name", css = TCss.None, width = 10 });
            fields.Add(new TField { title = "聘書字號", property = "in_decree", css = TCss.None, width = 45 });
            fields.Add(new TField { title = "聘任職務", property = "inn_job", css = TCss.Center, width = 10 });
            fields.Add(new TField { title = "活動名稱", property = "mt_title", css = TCss.Center, width = 10 });
            fields.Add(new TField { title = "聘期", property = "inn_term", css = TCss.Center, width = 15 });
            fields.Add(new TField { title = "發證日期", property = "mt_cert_date", css = TCss.Center, width = 15 });

            MapCharSetAndIndex(cfg, fields);


            int mnRow = 1;
            int wsRow = 1;

            //標題列
            SetHeadRow(sheet, wsRow, fields);
            wsRow++;

            var count = itmMUsers.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = itmMUsers.getItemByIndex(i);
                var inn_job = GetStaffFullJob(cfg, item);
                var inn_term = GetStaffFullTerm(cfg, item);
                item.setProperty("mt_title", cfg.mt_title);
                item.setProperty("mt_cert_date", cfg.mt_date_s.tw_date1);

                item.setProperty("inn_job", inn_job);
                item.setProperty("inn_term", inn_term);
                
                SetItemCell(cfg, sheet, wsRow, item, fields);

                wsRow++;
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        //聘任職務
        private string GetStaffFullJob(TConfig cfg, Item item)
        {
            return item.getProperty("in_job_name", "");
        }

        //聘期
        private string GetStaffFullTerm(TConfig cfg, Item item)
        {
            var dt_s = GetMoment(item.getProperty("in_term_s", ""));
            var dt_e = GetMoment(item.getProperty("in_term_e", ""));
            
            if (dt_s.value == "") return "";
            if (dt_e.value == "") return dt_s.tw_date3;
            if (dt_s.value == dt_e.value) return dt_s.tw_date3;

            return dt_s.tw_date3 + "至" + dt_e.tw_date3;
        }

        //設定資料格
        private void SetCell(Spire.Xls.Worksheet sheet, string cr, string value, TCss css)
        {
            var range = sheet.Range[cr];

            switch (css)
            {
                case TCss.None:
                    range.Text = value;
                    break;
                case TCss.Center:
                    range.Text = value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Text:
                    range.Text = "'" + value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Number:
                    range.NumberValue = GetInt(value);
                    range.NumberFormat = "0";
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                    break;
                case TCss.Money:
                    range.NumberValue = GetInt(value);
                    range.NumberFormat = "$ #,##0";
                    break;
                case TCss.Day:
                    range.Text = GetDtmStr(value, "yyyy/MM/dd");
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
            }
        }

        private void SetMoney(Spire.Xls.Worksheet sheet, string cr, double value)
        {
            var range = sheet.Range[cr];
            range.NumberValue = value;
            range.NumberFormat = "$ #,##0";
        }

        private void SetHeadRow(Spire.Xls.Worksheet sheet, int wsRow, List<TField> fields)
        {
            foreach (var field in fields)
            {
                string cr = field.cs + wsRow;
                SetHead(sheet, cr, field.title);
            }
        }

        //設定標題列
        private void SetHead(Spire.Xls.Worksheet sheet, string cr, string value)
        {
            var range = sheet.Range[cr];
            range.Text = value;
            range.Style.Font.IsBold = true;
            range.Style.Font.Color = System.Drawing.Color.White;
            range.Style.Color = System.Drawing.Color.FromArgb(41, 92, 144);//#295C90
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
        }

        private void MapCharSetAndIndex(TConfig cfg, List<TField> fields)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                field.ci = i + 1;
                field.cs = cfg.CharSet[field.ci];
            }
        }


        private void SetColumnWidth(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].ColumnWidth = field.width;
            }
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, List<TField> fields, int mnRow, int wsRow)
        {
            var tbl_ps = fields.First().cs + mnRow;
            var tbl_pe = fields.Last().cs + (wsRow - 1);
            SetRangeBorder(sheet, tbl_ps + ":" + tbl_pe);
        }

        //設定資料列
        private void SetItemCell(TConfig cfg, Spire.Xls.Worksheet sheet, int wsRow, Item item, List<TField> fields)
        {
            foreach (var field in fields)
            {
                var cr = field.cs + wsRow;
                var va = "";

                if (field.getValue != null)
                {
                    va = field.getValue(cfg, field, item);
                }
                else if (field.property != "")
                {
                    va = item.getProperty(field.property, "");
                }

                SetCell(sheet, cr, va, field.css);
            }
        }

        private Item GetMeetingItem(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                in_title
	                , in_meeting_type
	                , in_seminar_type
	                , in_level
	                , in_coursh_hours
	                , DATEADD(HOUR, 8, in_date_s) AS 'in_date_s'
	                , DATEADD(HOUR, 8, in_date_e) AS 'in_date_e'
	                , DATEADD(HOUR, 8, in_sports_dept_date) AS 'in_sports_dept_date'
	                , in_sports_dept_word
                FROM 
	                {#table} WITH(NOLOCK) 
                WHERE 
	                id = '{#meeting_id}'
            ";

            sql = sql.Replace("{#table}", cfg.meeting_table)
                .Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }
        
        private Item GetMUserItems(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t1.id
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_gender
	                , t1.in_l1
	                , t1.in_name_num
	                , DATEADD(HOUR, 8, t1.in_birth) AS 'in_birth'
                FROM
	                {#table1} t1 WITH(NOLOCK) 
                INNER JOIN
	                {#table2} t2 WITH(NOLOCK)
	                ON t2.in_user = t1.id
                WHERE
	                t1.source_id = '{#meeting_id}'
                ORDER BY
	                t1.in_birth
            ";

            sql = sql.Replace("{#table1}", cfg.muser_table)
                .Replace("{#table2}", cfg.mresume_table)
                .Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private Item GetMStaffItems(TConfig cfg)
        {
            string id_cond = cfg.qry_id == ""
                ? ""
                : "AND t1.id = '" + cfg.qry_id + "'";

            string area_cond = cfg.qry_area == ""
                ? ""
                : "AND t1.in_area = N'" + cfg.qry_area + "'";

            string sql = @"
                SELECT 
	                t1.id
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_gender
	                , DATEADD(HH, 8, t1.in_birth) AS 'in_birth'
	                , t1.in_resident_add
	                , t1.in_tel
	                , t1.in_job
	                , t1.in_job_name
	                , t1.in_cost
	                , DATEADD(HH, 8, t1.in_term_s) AS 'in_term_s'
	                , DATEADD(HH, 8, t1.in_term_e) AS 'in_term_e'
                    , t1.in_area
                    , t1.in_decree
                FROM 
	                {#table1} t1 WITH(NOLOCK)
                INNER JOIN 
	                [VU_Mt_JobTitle] t2
	                ON t2.label_zt = t1.in_job
	            LEFT OUTER JOIN
	                [VU_Mt_Area] t3
	                ON t3.value = t1.in_area
                WHERE 
	                t1.source_id = '{#meeting_id}' 
	                {#id_cond} 
                    {#area_cond}
                ORDER BY 
	                t3.sort_order
	                , t2.sort_order
	                , t1.created_on
            ";

            sql = sql.Replace("{#table1}", cfg.mstaff_table)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#id_cond}", id_cond)
                .Replace("{#area_cond}", area_cond)
                ;

            return cfg.inn.applySQL(sql);
        }


        //設定活動資料
        private void AppendMeetingInfo(TConfig cfg)
        {
            cfg.CharSet = GetCharSet();

            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.mt_type = cfg.itmMeeting.getProperty("in_meeting_type", "");
            cfg.mt_seminar = cfg.itmMeeting.getProperty("in_seminar_type", "");
            cfg.mt_level = cfg.itmMeeting.getProperty("in_level", "");
            cfg.mt_hours = cfg.itmMeeting.getProperty("in_coursh_hours", "0");
            cfg.mt_date_s = GetMoment(cfg.itmMeeting.getProperty("in_date_s", ""));
            cfg.mt_date_e = GetMoment(cfg.itmMeeting.getProperty("in_date_e", ""));
            cfg.mt_sports_date = GetMoment(cfg.itmMeeting.getProperty("in_sports_dept_date", ""));
            cfg.mt_sports_word = cfg.itmMeeting.getProperty("in_sports_dept_word", "0");
            cfg.mt_study_contents = GetStudyCertContents(cfg);

            switch (cfg.mt_seminar)
            {
                case "referee"://國內裁判
                case "referee_enr"://國內裁判增能
                    cfg.mt_seminar_label = "裁判";
                    break;

                case "referee_gl"://國際裁判
                case "referee_enr_gl"://國際裁判增能
                    cfg.mt_seminar_label = "國際裁判";
                    break;

                case "coach"://國內教練
                case "coach_enr"://國內教練增能
                    cfg.mt_seminar_label = "教練";
                    break;

                case "coach_gl"://國際教練
                case "coach_enr_gl"://國際教練增能
                    cfg.mt_seminar_label = "國際教練";
                    break;

                default:
                    cfg.mt_seminar_label = "未設定講習類型";
                    break;
            }

        }

        private string GetStudyCertContents(TConfig cfg)
        {
            return "業經中華民國體育運動總會"
                + cfg.mt_sports_date.tw_date3
                + cfg.mt_sports_word
                + "函辦理。";
        }
        

        private class TCtrl
        { 
            public string item_desc { get; set; }
            public Func<TConfig, Item> getItems { get; set; }
            public Action<TConfig, Spire.Xls.Workbook, Item> appendSheet { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }
            public string mode { get; set; }
            public string qry_id { get; set; }
            public string qry_area { get; set; }

            public string meeting_table { get; set; }
            public string muser_table { get; set; }
            public string mresume_table { get; set; }
            public string mstaff_table { get; set; }
            
            public Item itmMeeting { get; set; }
            public string mt_type { get; set; }
            public string mt_title { get; set; }
            public string mt_seminar { get; set; }
            public string mt_level { get; set; }
            public string mt_hours { get; set; }
            public string mt_seminar_label { get; set; }
            public TMoment mt_date_s { get; set; }
            public TMoment mt_date_e { get; set; }
            public TMoment mt_sports_date { get; set; }
            public string mt_sports_word { get; set; }

            public string mt_study_contents { get; set; }

            public string[] CharSet { get; set; }
        }

        private TMoment GetMoment(string value)
        {
            var result = new TMoment { value = value };
            result.dt = GetDtm(result.value);
            if (result.dt == DateTime.MinValue)
            {
                result.tw_date1 = "";
                result.tw_date2 = "";
                result.tw_date3 = "";
            }
            else
            {
                var tw_year = result.dt.Year - 1911;
                var m = result.dt.Month.ToString().PadLeft(2, '0');
                var d = result.dt.Day.ToString().PadLeft(2, '0');
                result.tw_date1 = "中華民國" + tw_year + "年" + m + "月" + d + "日";
                result.tw_date2 = "民國" + tw_year + "年" + m + "月" + d + "日";
                result.tw_date3 = tw_year + "年" + m + "月" + d + "日";
            }
            return result;
        }

        private class TMoment
        {
            public string value { get; set; }
            public DateTime dt { get; set; }
            public string tw_date1 { get; set; }
            public string tw_date2 { get; set; }
            public string tw_date3 { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public TCss css { get; set; }
            public int ci { get; set; }
            public string cs { get; set; }
            public int width { get; set; }
            public Func<TConfig, TField, Item, string> getValue { get; set; }

            public bool is_merge { get; set; }
        }

        private enum TCss
        {
            None = 0,
            Center = 10,
            Text = 100,
            Number = 200,
            Money = 300,
            Day = 400,
        }

        private class TXlsx
        {
            public string export_path { get; set; }
            public string template_path { get; set; }

            public string name { get; set; }
            public string file { get; set; }
        }

        private string[] GetCharSet()
        {
            return new string[] { "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        }

        private TXlsx GetXlsx(TConfig cfg)
        {
            var result = new TXlsx();

            var itmXls = cfg.inn.applyMethod("In_Variable_File", "<in_name></in_name>");
            result.export_path = itmXls.getProperty("export_path", "").TrimEnd('\\');
            result.template_path = itmXls.getProperty("template_path", "");

            var guid = System.Guid.NewGuid().ToString().Substring(0, 6).ToUpper()
                + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff");

            if (!System.IO.Directory.Exists(result.export_path))
            {
                System.IO.Directory.CreateDirectory(result.export_path);
            }

            result.name = cfg.mt_title + "_研習證書_學員清冊_" + guid + ".xlsx";
            result.file = result.export_path + "\\" + result.name;

            return result;
        }

        private string GetDtmStr(string value, string format)
        {
            var result = GetDtm(value);
            if (result == DateTime.MinValue) return "";
            return result.ToString(format);
        }

        private DateTime GetDtm(string value)
        {
            if (value == "") return DateTime.MinValue;

            DateTime result = DateTime.MinValue;
            if (DateTime.TryParse(value, out result))
            {
                return result;
            }
            return DateTime.MinValue;
        }

        private int GetInt(string value, int defV = 0)
        {
            if (value == "") return 0;

            int result = defV;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return defV;
        }
    }
}
