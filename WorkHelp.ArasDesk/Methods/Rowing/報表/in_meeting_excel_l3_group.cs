﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Rowing.報表
{
	public class in_meeting_excel_l3_group : Item
	{
		public in_meeting_excel_l3_group(IServerConnection arg) : base(arg) { }

		/// <summary>
		/// 編程啟動點 (Code 在此撰寫)
		/// </summary>
		public Item Run()
		{
			Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
			Aras.Server.Core.IContextState RequestState = CCO.RequestState;
			/*
目的:跆拳道-學員清冊
說明:
若是模板類型的課程,則產生純空白的EXCEL,但包含所有的報名欄位
若是正常課程,則產生有值得EXCEL,包含表頭與報名表資料
參數:
in_value: meeting_excel
--export
--template

輸入:
meeting_id
export_type(excel,xml)  輸出格式, 若為 excel 則儲存 excel檔案,若為xml,則不儲存excel檔案,但回傳 Item

輸出:
excel的位置

位置:
1.meeting_import.html
做法:
1.取得課程主檔
2.取得報名表問項,以及總共需要幾次簽到
3.取得學員履歷,依據學員履歷取得歷次簽到記錄
4.填好class頁籤
5.填好student的題目表頭
6.填好student的學員資料

*/

			//System.Diagnostics.Debugger.Break();

			var inn = this.getInnovator();
			var strDatabaseName = inn.getConnection().GetDatabaseName();
			var strMethodName = "[" + strDatabaseName + "]" + "in_meeting_excel_l3_group";

			Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
			bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

			string aml = "";
			string sql = "";

			string strError = "";
			string strUserId = inn.getUserID();

			Item itmR = this;

			string meeting_id = this.getProperty("meeting_id", "1C4F7E67FD004515A996DB4F2D37D118");
			string export_type = this.getProperty("export_type", "excel");

			if (meeting_id == "")
			{
				throw new Exception("賽事 id 不得為空白");
			}

			Item Resume = this;
			Item tmp = null;
			bool open = false;

			//如果是屬於 ACT_GymOwner 則只能看到自己道館的資料
			string SQL_OnlyMyGym = "";

			//判斷現在的登入者是否為此課程的共同講師 
			aml = "<AML>" +
					"<Item type='In_Meeting_Resumelist' action='get'>" +
					"<source_id>" + meeting_id + "</source_id>" +
					"</Item></AML>";
			Item Meeting_Resumelists = inn.applyAML(aml);

			for (int i = 0; i < Meeting_Resumelists.getItemCount(); i++)
			{
				Item Meeting_Resumelist = Meeting_Resumelists.getItemByIndex(i);
				//判定登入者ID是否存在於該課程共同講師裡面
				if (strUserId == Meeting_Resumelist.getProperty("in_user_id", ""))//此次登入者是共同講師
				{
					open = true;
				}
				else
				{
					open = false;
				}
			}

			aml = "<AML>" +
			"<Item type='In_Resume' action='get'>" +
			"<in_user_id>" + strUserId + "</in_user_id>" +
			"</Item></AML>";
			Resume = inn.applyAML(aml);

			if (!open)//不是共同講師走這
			{
				var isGymOwner = CCO.Permissions.IdentityListHasId(Aras.Server.Security.Permissions.Current.IdentitiesList, "7AC8B33DA0F246DDA70BB244AB231E29");
				if (isGymOwner)
				{
					SQL_OnlyMyGym = " AND in_group = N'" + Resume.getProperty("in_group", "") + "'";
				}
			}

			aml = "<AML>" +
				"<Item type='In_Meeting' action='get' id='" + meeting_id + "'>" +
				"<Relationships>" +

				"<Item type='In_Meeting_Surveys' action='get' select='related_id(id,in_questions,in_request,in_selectoption,in_question_type,in_property,in_expense,in_is_pkey)'>" +
				"<in_surveytype>1</in_surveytype>" +
				"<in_property condition='in'>'in_l1','in_l2','in_l3'</in_property>" +
				"</Item>" +


				"</Relationships>" +
			"</Item></AML>";

			//1.取得課程主檔
			Item Meeting = inn.applyAML(aml);


			//建立 Students 的欄位
			Dictionary<string, Item> StudentFields = new Dictionary<string, Item>();
			int ColIndex = 1;

			//報名表問項
			string ExpenseProperty = ""; //紀錄本賽事的費用層次
			int Meeting_Surveys_Count = Meeting.getRelationships("In_Meeting_Surveys").getItemCount();
			for (int i = 0; i < Meeting_Surveys_Count; i++)
			{
				Item In_Meeting_Survey = Meeting.getRelationships("In_Meeting_Surveys").getItemByIndex(i);
				tmp = In_Meeting_Survey.getPropertyItem("related_id");
				tmp.setProperty("col_index", (ColIndex++).ToString());
				tmp.setProperty("inn_id", In_Meeting_Survey.getProperty("related_id", ""));
				StudentFields.Add(tmp.getProperty("inn_id"), tmp);

				if (tmp.getProperty("in_expense", "") == "1")
					ExpenseProperty = tmp.getProperty("in_property", ""); //可能是 in_l1, in_l2, in_l3

			}

			tmp = inn.newItem("In_Meeting_Surveys");
			tmp.setProperty("inn_id", "expense");
			tmp.setProperty("in_questions", "金額");
			tmp.setProperty("in_request", "0");
			tmp.setProperty("col_index", (ColIndex++).ToString());
			tmp.setProperty("in_selectoption", "");
			tmp.setProperty("in_question_type", "number");
			StudentFields.Add(tmp.getProperty("inn_id"), tmp);


			//開始整理EXCEL Template
			aml = "<AML>" +
				"<Item type='In_Variable' action='get'>" +
				"<in_name>meeting_excel</in_name>" +
				"<Relationships>" +
				"<Item type='In_Variable_Detail' action='get'/>" +
				"</Relationships>" +
				"</Item></AML>";
			Item Vairable = inn.applyAML(aml);

			string Template_Path = "";
			string Export_Path = "";
			for (int i = 0; i < Vairable.getRelationships("In_Variable_Detail").getItemCount(); i++)
			{
				Item In_Variable_Detail = Vairable.getRelationships("In_Variable_Detail").getItemByIndex(i);

				if (In_Variable_Detail.getProperty("in_name", "") == "template_2_path")
					Template_Path = In_Variable_Detail.getProperty("in_value", "");

				if (In_Variable_Detail.getProperty("in_name", "") == "export_path")
					Export_Path = In_Variable_Detail.getProperty("in_value", "");
				if (!Export_Path.EndsWith(@"\"))
					Export_Path = Export_Path + @"\";
			}

			//取得題庫下的 in_l1 跟項次
			Item itmOpts1 = GetMeetingSurveyOptions(inn, meeting_id, "in_l1");

			//取得題庫下的 in_l2 跟項次
			Item itmOpts2 = GetMeetingSurveyOptions(inn, meeting_id, "in_l2");

			//取得題庫下的 in_l3 跟項次
			Item itmOpts3 = GetMeetingSurveyOptions(inn, meeting_id, "in_l3");

			//用假欄位排序(L1)
			for (int i = 0; i < itmOpts1.getItemCount(); i++)
			{
				Item item = itmOpts1.getItemByIndex(i);

				sql = "UPDATE In_Meeting_User SET in_excel_order = '" + i.ToString() + "'"
					+ " WHERE source_id = '" + meeting_id + "'"
					+ " AND in_l1 = N'" + item.getProperty("in_value", "") + "'";

				inn.applySQL(sql);
			}

			//用假欄位排序(L2)
			for (int i = 0; i < itmOpts2.getItemCount(); i++)
			{
				Item item = itmOpts2.getItemByIndex(i);

				sql = "UPDATE In_Meeting_User SET in_excel_order_2 = '" + item.getProperty("sort_order", "") + "'"
					+ " WHERE source_id = '" + meeting_id + "'"
					+ " AND in_l1 = N'" + item.getProperty("in_filter", "") + "'"
					+ " AND in_l2 = N'" + item.getProperty("in_value", "") + "'";

				inn.applySQL(sql);
			}

			//用假欄位排序(L3)
			for (int i = 0; i < itmOpts3.getItemCount(); i++)
			{
				Item item = itmOpts3.getItemByIndex(i);

				sql = "UPDATE In_Meeting_User SET in_excel_order_3 = '" + item.getProperty("sort_order", "") + "'"
					+ " WHERE source_id = '" + meeting_id + "'"
					+ " AND in_l2 = N'" + item.getProperty("in_filter", "") + "'"
					+ " AND in_l3 = N'" + item.getProperty("in_value", "") + "'";

				inn.applySQL(sql);
			}

			var hasL3 = itmOpts3.getItemCount() > 0;

			//第三階分組(L1_order,L2_order,L3_order)
			sql = "SELECT id, in_name, in_gender, in_l1, in_l2, in_l3, in_index, in_current_org"
			+ ", CONVERT(VARCHAR, DATEADD(HOUR, 8, in_birth), 111) AS 'in_birth'"
			+ " FROM IN_MEETING_USER WHERE source_id = '" + meeting_id + "'"
			+ " AND in_note_state = 'official'"
			+ SQL_OnlyMyGym
			+ " ORDER BY in_excel_order, in_excel_order_2, in_excel_order_3, in_index, in_current_org";

			Item L3_groups = inn.applySQL(sql);


			ClosedXML.Excel.XLWorkbook wb = new ClosedXML.Excel.XLWorkbook(Template_Path);
			ClosedXML.Excel.IXLWorksheet testWS = wb.Worksheet(1);
			ClosedXML.Excel.IXLCell Test_Cell;


			int WSCol = 2;
			int WSRow = 4;
			int Count = 1;//流水號

			for (int i = 0; i < L3_groups.getItemCount(); i++)
			{
				Item L3_group = L3_groups.getItemByIndex(i);
				var in_l1 = L3_group.getProperty("in_l1", "");
				var in_l2 = L3_group.getProperty("in_l2", "");
				var in_l3 = L3_group.getProperty("in_l3", "");

				if (in_l1 == "隊職員") continue;
				
				Test_Cell = testWS.Cell(WSRow, WSCol);
				SetCell(Test_Cell);
				Test_Cell.Value = Count;

				Test_Cell = testWS.Cell(WSRow, WSCol + 1);
				SetCell(Test_Cell);
				Test_Cell.Value = L3_group.getProperty("in_current_org", "");
				Test_Cell.Style.Alignment.WrapText = true;//自動換行

				Test_Cell = testWS.Cell(WSRow, WSCol + 2);
				SetCell(Test_Cell);
				Test_Cell.Value = L3_group.getProperty("in_name", "");
				Test_Cell.Style.Alignment.WrapText = true;//自動換行

				Test_Cell = testWS.Cell(WSRow, WSCol + 3);
				SetCell(Test_Cell);
				Test_Cell.Value = L3_group.getProperty("in_birth", "").Split(' ')[0];
				Test_Cell.Style.Alignment.WrapText = true;//自動換行

				Test_Cell = testWS.Cell(WSRow, WSCol + 4);
				SetCell(Test_Cell);
				Test_Cell.Value = GetFullSectName(in_l1, in_l2, in_l3);
	
				//Test_Cell.Value = L3_group.getProperty("in_l1","");

				Test_Cell = testWS.Cell(WSRow, WSCol + 5);
				SetCell(Test_Cell);
				//Test_Cell.Value = L3_d.getProperty("test_count","0");

				WSRow++;
				Count++;


				if ((i + 1) >= L3_groups.getItemCount())
                {
					break;
                }

				Item itmNext = L3_groups.getItemByIndex(i + 1);
				var nextL1 = itmNext.getProperty("in_l1", "");
				var nextL2 = itmNext.getProperty("in_l2", "");
				var nextL3 = itmNext.getProperty("in_l3", "");
				var nextL12 = nextL1 + "-" + nextL2;
				var currentL12 = in_l1 + "-" + in_l2;

				var needEmptyRow = hasL3
					? nextL3 != in_l3
					: nextL12 != currentL12;

				if (needEmptyRow)
				{
					//testWS.Style.Fill.BackgroundColor = ClosedXML.Excel.XLColor.Blue;
					testWS.Row(WSRow).InsertRowsBelow(1);//插入下方行
					//testWS.Row(WSRow).InsertRowsAbove(1);//插入上方行
					//testWS.Row(WSRow).Style.Fill.BackgroundColor = ClosedXML.Excel.XLColor.Orange;
					Count = 1;
					WSRow++;
				}
			}

			string xlsName = Meeting.getProperty("in_title", "");

			xlsName = xlsName.Replace("+", "")
							   .Replace("-", "")
							   .Replace(".", "")
							   .Replace(" ", "");

			wb.SaveAs(Export_Path + xlsName + ".xlsx");

			itmR.setProperty("xls_name", xlsName + ".xlsx");

			return itmR;
		}

		private Item GetMeetingSurveyOptions(Innovator inn, string meeting_id, string in_property)
        {
			var sql = @"
				SELECT
					t3.in_value
					, t3.in_filter
					, t3.in_label
					, t3.sort_order
				FROM
					IN_MEETING_SURVEYS t1 WITH(NOLOCK)
				INNER JOIN
					IN_SURVEY t2 WITH(NOLOCK)
					ON t2.id = t1.related_id
				INNER JOIN
					IN_SURVEY_OPTION t3
					ON t3.source_id = t2.id
				WHERE
					t1.source_id = '{#meeting_id}'
					AND t2.in_property = '{#in_property}'
				ORDER BY
					t3.sort_order
			";

			sql = sql.Replace("{#meeting_id}", meeting_id)
				.Replace("{#in_property}", in_property);

			return inn.applySQL(sql);
		}

		private string GetFullSectName(string in_l1, string in_l2, string in_l3)
        {
			string[] c2s = in_l2.Split(new char[2] { '-', '/' });//取得-/中間的字

			var text = in_l1 + "-" + c2s[0];
			if (in_l3 != "") text += "-" + in_l3;

			return text;
		}

		private void SetCell(ClosedXML.Excel.IXLCell cell)
        {
			cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
			cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
			cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
			cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
			cell.Style.Alignment.Vertical = ClosedXML.Excel.XLAlignmentVerticalValues.Center;
			cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;//靠中
		}
	}
}
