﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Rowing.Common
{
    public class AAA_Method : Item
    {
        public AAA_Method(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的:划船-秩序冊
                說明:
                    需區分 單打,混雙,雙打,團體 的個別 SHEET
                    與報名資訊不同的地方:
                        1.不分群組,不分單位
                        2.排序: 序號-in_l1-姓名
                        3.excel 模板變數名稱為 template_1_path (in_variable:meeting_excel)

                    參數:
                        in_value: meeting_excel
                        --export
                        --template

                輸入:meeting_id
                輸出:
                    excel的位置

                位置:
                    1.meeting_import.html
                做法:
                    //2023.05.18 修改男女混合。個人賽計算分組錯誤
            */

            System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "in_meeting_export_word_1_rowing";

            var itmR = this;

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                meeting_id = itmR.getProperty("meeting_id", ""),
            };


            if (cfg.meeting_id == "")
            {
                throw new Exception("課程id不得為空白");
            }

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            if (CCO.Permissions.IdentityListHasId(Aras.Server.Security.Permissions.Current.IdentitiesList, "7AC8B33DA0F246DDA70BB244AB231E29"))
            {
                cfg.isGymOwner = true;
            }

            cfg.itmMeeting = GetMeetingItem(cfg);

            cfg.itmResume = cfg.inn.applyAML("<AML><Item type='In_Resume' action='get'><in_user_id>" + cfg.strUserId + "</in_user_id></Item></AML>");


            var in_group = cfg.itmResume.getProperty("in_group", "");

            cfg.onlyMyGym1 = cfg.isGymOwner
                ? " AND in_group = N'" + in_group + "'"
                : "";

            cfg.onlyMyGym2 = cfg.isGymOwner
                ? " and [In_Meeting_User].in_group = N'" + in_group + "'"
                : "";

            SetExportPath(cfg);

            Run(cfg, itmR);

            //把身分換回來
            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private void Run(TConfig cfg, Item itmReturn)
        {
            //建立 Students 的欄位
            var StudentFields = new Dictionary<string, Item>();

            AppendMeetingSurveys(cfg, StudentFields);

            //Word
            Xceed.Words.NET.DocX doc;//word
            doc = Xceed.Words.NET.DocX.Load(cfg.template_path);//載入模板
            var docTable = doc.Tables[0];//取得模板第一個表格

            //先處理 students 的欄位
            SetLevelName(cfg, StudentFields);

            //算出有幾間群組(in_group)
            Item Orgs = GetMeetingOrgItems(cfg);

            var DicOrgs = new Dictionary<string, Item>();
            AppendDicOrgs(DicOrgs, Orgs);


            //處理學員
            Item MeetingUsers = GetMeetingUserItems(cfg);

            AppendOrgTable(cfg, MeetingUsers, DicOrgs, docTable);

            Item l1s = GetMeetingL1Items(cfg);
            AppendL1Table(cfg, l1s, doc, DicOrgs);

            //分組名單
            Item In_Meeting_Users = GetMeetingUserGroupItems(cfg);

            var Groups = new Dictionary<string, Item>();
            AppendGroupTable(cfg, In_Meeting_Users, Groups);

            //第三階
            Item l3s = GetMeetingL3Items(cfg);
            AppendItemTable(cfg, l3s, doc, Groups);

            //移除兩個Sheet

            doc.Tables[0].Remove();
            doc.Tables[0].Remove();
            doc.Tables[0].Remove();
            doc.Tables[0].Remove();
            doc.RemoveParagraphAt(0);
            doc.RemoveParagraphAt(0);
            doc.RemoveParagraphAt(0);
            doc.RemoveParagraphAt(0);
            doc.RemoveParagraphAt(0);


            string xlsName = GetXlsxName(cfg);

            doc.SaveAs(cfg.export_path + xlsName + "_秩序冊.docx");

            itmReturn.setProperty("xls_name", xlsName + "_秩序冊.docx");
        }

        private void AppendItemTable(TConfig cfg, Item l3s, Xceed.Words.NET.DocX doc, Dictionary<string, Item> Groups)
        {
            int count = 0;
            var l3_count = "";

            //貼出分組名單
            for (int c = 0; c < l3s.getItemCount(); c++)
            {
                Item l3 = l3s.getItemByIndex(c);
                var g1A = l3.getProperty(cfg.group_1, "");
                var g2A = l3.getProperty(cfg.group_2, "");
                var l1A = l3.getProperty("in_l1", "");

                if (l1A == "隊職員") continue;

                doc.InsertSectionPageBreak();

                //(賽事名稱 + 分項參賽名單-(in_l2 or in_l3))
                doc.InsertParagraph(cfg.itmMeeting.getProperty("in_title", "") + "\n" + "分項參賽名單-"
                    + g1A).FontSize(16).Font("標楷體").Alignment = Xceed.Document.NET.Alignment.center;

                //取得數量
                count = 0;
                var tmpChkMemberName = new List<string>();
                foreach (var entryitem in Groups)
                {
                    var grouplist = entryitem.Value;
                    var g1B = grouplist.getProperty(cfg.group_1, "");
                    var g2B = grouplist.getProperty(cfg.group_2, "");

                    if (g2A != g2B || g1A != g1B)
                    {
                        continue;
                    }

                    if (g2A.Contains("單人") || l1A.Contains("個人"))
                    {
                        //紀錄個人賽的名稱
                        string in_name = grouplist.getProperty("in_name", "").Replace("  ", " ");
                        string[] tmpSplitNames = in_name.Split(' ');
                        foreach (var tmp_name in tmpSplitNames)
                        {
                            tmpChkMemberName.Add(tmp_name);
                        }
                    }
                    count++;
                }

                //移除重複
                tmpChkMemberName = tmpChkMemberName.Distinct().ToList();

                if (g2A.Contains("單人") || l1A.Contains("個人"))
                {
                    l3_count = tmpChkMemberName.Count + "人";
                }
                else
                {
                    l3_count = count.ToString() + "隊";
                }

                //將數量貼出來
                doc.InsertParagraph("〔" + g2A.Replace('^', ' ') + "〕 -- " + l3_count)
                    .Bold().FontSize(11).Font("標楷體");//(in_l1 or in_l2) -- 數量(隊伍or人)

                var TestTotal = doc.InsertTable(doc.Tables[3]);//第三個表格
                count = 0;
                //貼出各組資料
                foreach (var entryitem in Groups)
                {
                    var grouplist = entryitem.Value;
                    var g1B = grouplist.getProperty(cfg.group_1, "");
                    var g2B = grouplist.getProperty(cfg.group_2, "");

                    if (g2A != g2B || g1A != g1B)
                    {
                        continue;
                    }


                    Xceed.Document.NET.Row sum_row = TestTotal.Rows[1];//第三表格的第一行
                    TestTotal.InsertRow(sum_row, true);
                    //起始從下一行開始(第一行是複製用 需空白 從第二行開始塞資料)
                    TestTotal.Rows[2 + count].Cells[0].Paragraphs.First().Append(g1B.Split('組')[0]).Font("標楷體").FontSize(11);//組別
                    TestTotal.Rows[2 + count].Cells[2].Paragraphs.First().Append(grouplist.getProperty("in_current_org", "") + " " + grouplist.getProperty("in_team", "")).Font("標楷體").FontSize(11);//單位+隊別
                    TestTotal.Rows[2 + count].Cells[3].Paragraphs.First().Append(grouplist.getProperty("in_name", "")).Font("標楷體").FontSize(11);//姓名
                    TestTotal.Rows[2 + count].Cells[4].Paragraphs.First().Append(grouplist.getProperty("waiting_list_name", "")).Font("標楷體").FontSize(11);//替補名單
                    count++;
                }
                TestTotal.Rows[1].Remove();//移除掉複製的第一行
            }
        }

        private void AppendGroupTable(TConfig cfg, Item In_Meeting_Users, Dictionary<string, Item> Groups)
        {
            cfg.group_1 = "";
            cfg.group_2 = "";
            for (int i = 0; i < In_Meeting_Users.getItemCount(); i++)
            {
                Item In_Meeting_User = In_Meeting_Users.getItemByIndex(i);

                //有第三階就(三+二) 沒有第三階就(二+一)
                if (In_Meeting_User.getProperty("in_l3", "") == "")
                {
                    cfg.group_1 = "in_l1";
                    cfg.group_2 = "in_l2";
                }
                else
                {
                    cfg.group_1 = "in_l2";
                    cfg.group_2 = "in_l3";
                }

                var g1 = In_Meeting_User.getProperty(cfg.group_1, "");
                var g2 = In_Meeting_User.getProperty(cfg.group_2, "");

                //Dictionary的key(單位^隊別^(in_l3 or in_l2))
                string OrgName = In_Meeting_User.getProperty("in_current_org", "")
                    + "^" + In_Meeting_User.getProperty("in_team", "")
                    + "^" + g1
                    + g2;

                //為了不讓key值重複 單人的key+i
                if (g2.Contains("單人"))
                {
                    OrgName = OrgName + i;
                }

                Item orgnode = cfg.inn.newItem();
                orgnode.setProperty("in_l1", In_Meeting_User.getProperty("in_l1", ""));//第一階
                orgnode.setProperty("in_l2", In_Meeting_User.getProperty("in_l2", ""));//第二階
                orgnode.setProperty("in_l3", In_Meeting_User.getProperty("in_l3", ""));//第三階
                orgnode.setProperty("in_current_org", In_Meeting_User.getProperty("in_current_org", ""));//所屬單位
                orgnode.setProperty("in_team", In_Meeting_User.getProperty("in_team", ""));//隊別
                orgnode.setProperty("in_name", In_Meeting_User.getProperty("in_name", ""));//姓名
                orgnode.setProperty("in_waiting_list", "");//正備取
                orgnode.setProperty("waiting_list_name", "");//存備取的姓名

                if (!Groups.ContainsKey(OrgName))
                {
                    Groups.Add(OrgName, orgnode);
                }
                else
                {
                    if (In_Meeting_User.getProperty("in_waiting_list", "") == "備取")
                    {
                        //將[備取]的學員放到[waiting_list_name]
                        string waiting_list_names = Groups[OrgName].getProperty("waiting_list_name", "");
                        waiting_list_names += "  " + In_Meeting_User.getProperty("in_name", "");
                        Groups[OrgName].setProperty("waiting_list_name", waiting_list_names.Trim(' '));
                    }
                    else
                    {
                        string in_names = Groups[OrgName].getProperty("in_name", "");
                        in_names += "  " + In_Meeting_User.getProperty("in_name", "");
                        Groups[OrgName].setProperty("in_name", in_names.Trim(' '));
                    }
                }
            }
        }

        private void AppendMeetingSurveys(TConfig cfg, Dictionary<string, Item> StudentFields)
        {
            cfg.ColIndex = 1;
            cfg.ExpenseProperty = "";

            var itmMeetingSurveys = cfg.itmMeeting.getRelationships("In_Meeting_Surveys");
            var Meeting_Surveys_Count = itmMeetingSurveys.getItemCount();
            for (int i = 0; i < Meeting_Surveys_Count; i++)
            {
                Item In_Meeting_Survey = itmMeetingSurveys.getItemByIndex(i);
                var itmRelated = In_Meeting_Survey.getPropertyItem("related_id");
                itmRelated.setProperty("col_index", (cfg.ColIndex++).ToString());
                itmRelated.setProperty("inn_id", In_Meeting_Survey.getProperty("related_id", ""));
                StudentFields.Add(itmRelated.getProperty("inn_id"), itmRelated);

                if (itmRelated.getProperty("in_expense", "") == "1")
                {
                    cfg.ExpenseProperty = itmRelated.getProperty("in_property", "");
                }

            }

            var tmp = cfg.inn.newItem("In_Meeting_Surveys");
            tmp.setProperty("inn_id", "expense");
            tmp.setProperty("in_questions", "金額");
            tmp.setProperty("in_request", "0");
            tmp.setProperty("col_index", (cfg.ColIndex++).ToString());
            tmp.setProperty("in_selectoption", "");
            tmp.setProperty("in_question_type", "number");
            StudentFields.Add(tmp.getProperty("inn_id"), tmp);
        }

        private void AppendL1Table(TConfig cfg, Item l1s, Xceed.Words.NET.DocX doc, Dictionary<string, Item> DicOrgs)
        {
            var Meeting = cfg.itmMeeting;
            var time_s = Convert.ToDateTime(Meeting.getProperty("in_date_s", ""));
            var time_e = Convert.ToDateTime(Meeting.getProperty("in_date_e", ""));

            for (int k = 0; k < l1s.getItemCount(); k++)
            {
                Item l1 = l1s.getItemByIndex(k);
                var l1A = l1.getProperty("c1", "");

                if (l1A.Contains("隊職員"))
                {
                    continue;
                }

                var title = Meeting.getProperty("in_title", "") + "-" + l1.getProperty("c1", "").Split('/')[0] + "各校參賽人數統計表";

                doc.InsertSectionPageBreak();
                doc.InsertParagraph(title).FontSize(16).Font("標楷體").Alignment = Xceed.Document.NET.Alignment.center;
                var time_start = string.Format("民國 {0} 年 {1} 月 {2} 日", new System.Globalization.TaiwanCalendar().GetYear(time_s), time_s.Month, time_s.Day);
                var time_end = string.Format("{1} 月 {2} 日", new System.Globalization.TaiwanCalendar().GetYear(time_e), time_e.Month, time_e.Day);


                doc.InsertParagraph("比賽日期：" + time_start + "至" + time_end).FontSize(12).Font("標楷體").Alignment = Xceed.Document.NET.Alignment.center; ;
                doc.InsertParagraph("比賽地點：" + Meeting.getProperty("in_address", "")).FontSize(12).Font("標楷體").Alignment = Xceed.Document.NET.Alignment.center; ;

                var sum_docTable = doc.InsertTable(doc.Tables[1]);
                var FinalTotal = doc.InsertTable(doc.Tables[2]);

                doc.InsertSectionPageBreak();
                doc.InsertParagraph(Meeting.getProperty("in_title", "")).FontSize(16).Font("標楷體").Alignment = Xceed.Document.NET.Alignment.center;
                doc.InsertParagraph(l1A.Split('/')[0] + "參賽隊職員名單").FontSize(16).Font("標楷體").Alignment = Xceed.Document.NET.Alignment.center;

                int sum_row_inde = 0;
                int Sum_Man_qua = 0;//男生總計
                int Sum_Wonan_qua = 0;//女生總計
                int Sum_MW = 0;//男女總數
                int Sum_Staff_qua = 0;//職員
                int Sum_MWS = 0;//合計

                //將取出的各校資料貼出來
                Item entrylist = null;
                foreach (var entryitem in DicOrgs)
                {
                    entrylist = entryitem.Value;
                    var l1B = entrylist.getProperty("in_l1", "");
                    var org = entrylist.getProperty("in_current_org", "");

                    if (l1B == "隊職員") continue;
                    if (l1B != l1A) continue;


                    var docTable = doc.InsertTable(doc.Tables[0]);
                    Xceed.Document.NET.Row row = docTable.Rows[3];//第幾列

                    doc.InsertParagraph();

                    docTable.Rows[0].Cells[0].Paragraphs.First().Append("單位名稱：" + entrylist.getProperty("in_current_org", "")).Font("標楷體").FontSize(11);

                    var key = org + "隊職員";

                    if (DicOrgs.ContainsKey(key))
                    {
                        Item staff = DicOrgs[key];

                        docTable.Rows[1].Cells[0].Paragraphs.First().Append("領    隊：" + staff.getProperty("leader", "")).Font("標楷體").FontSize(11);
                        docTable.Rows[1].Cells[1].Paragraphs.First().Append("教    練：" + staff.getProperty("coach", "")).Font("標楷體").FontSize(11);
                        docTable.Rows[1].Cells[2].Paragraphs.First().Append("管    理：" + staff.getProperty("contact_person", "")).Font("標楷體").FontSize(11);
                        docTable.Rows[2].Cells[0].Paragraphs.First().Append("聯 絡 人：" + staff.getProperty("management", "")).Font("標楷體").FontSize(11);
                        docTable.Rows[2].Cells[1].Paragraphs.First().Append("手    機：" + staff.getProperty("mobile_phone", "")).Font("標楷體").FontSize(11);
                        docTable.Rows[2].Cells[2].Paragraphs.First().Append("電    話：" + staff.getProperty("phone", "")).Font("標楷體").FontSize(11);

                        entrylist.setProperty("staff_quantity", staff.getProperty("staff_quantity"));//隊職員數
                    }
                    else
                    {
                        docTable.Rows[1].Cells[0].Paragraphs.First().Append("領    隊：").Font("標楷體").FontSize(11);
                        docTable.Rows[1].Cells[1].Paragraphs.First().Append("教    練：").Font("標楷體").FontSize(11);
                        docTable.Rows[1].Cells[2].Paragraphs.First().Append("管    理：").Font("標楷體").FontSize(11);
                        docTable.Rows[2].Cells[0].Paragraphs.First().Append("聯 絡 人：" + entrylist.getProperty("management", "")).Font("標楷體").FontSize(11);
                        docTable.Rows[2].Cells[1].Paragraphs.First().Append("手    機：" + entrylist.getProperty("mobile_phone", "")).Font("標楷體").FontSize(11);
                        docTable.Rows[2].Cells[2].Paragraphs.First().Append("電    話：").Font("標楷體").FontSize(11);
                    }

                    //有幾組加起幾列進去(男生組數+女生組數)
                    int iman_team_quantity = int.Parse(entrylist.getProperty("man_team_quantity", ""));
                    int iwoman_team_quantity = int.Parse(entrylist.getProperty("woman_team_quantity", ""));
                    int imix_team_quantity = int.Parse(entrylist.getProperty("mix_team_quantity", "0"));
                    int g = iman_team_quantity + iwoman_team_quantity + imix_team_quantity;

                    for (int i = 0; i < g; i++)
                    {
                        docTable.InsertRow(row, true);
                    }

                    //男子組
                    SetTableRow(docTable, entrylist, "man_team_names", 3);
                    //女子組
                    SetTableRow(docTable, entrylist, "woman_team_names", 3 + iman_team_quantity);
                    //混合組
                    SetTableRow(docTable, entrylist, "mix_team_names", 3 + iman_team_quantity + iwoman_team_quantity);


                    sum_row_inde++;
                    Xceed.Document.NET.Row sum_row = sum_docTable.Rows[2];
                    sum_docTable.InsertRow(sum_row, true);
                    //編號
                    sum_docTable.Rows[2 + sum_row_inde].Cells[0].Paragraphs.First().Append(sum_row_inde.ToString()).Font("標楷體").FontSize(11);

                    //單位名稱
                    sum_docTable.Rows[2 + sum_row_inde].Cells[1].Paragraphs.First().Append(entrylist.getProperty("in_current_org", "")).Font("標楷體").FontSize(11);

                    //男生人數
                    var man_quantity = entrylist.getProperty("male_quantity", "");//man_quantity
                    sum_docTable.Rows[2 + sum_row_inde].Cells[2].Paragraphs.First().Append(man_quantity).Font("標楷體").FontSize(11);
                    int ManQuantity_i = int.Parse(man_quantity);
                    Sum_Man_qua += ManQuantity_i;

                    //女生人數
                    var woman_quantity = entrylist.getProperty("female_quantity", "");//woman_quantity
                    sum_docTable.Rows[2 + sum_row_inde].Cells[3].Paragraphs.First().Append(woman_quantity).Font("標楷體").FontSize(11);
                    int WomanQuantity_i = int.Parse(woman_quantity);
                    Sum_Wonan_qua += WomanQuantity_i;

                    //男女生合計人數(男+女)
                    int PeopleQuantity_i = ManQuantity_i + WomanQuantity_i;
                    sum_docTable.Rows[2 + sum_row_inde].Cells[4].Paragraphs.First().Append(PeopleQuantity_i.ToString()).Font("標楷體").FontSize(11);
                    Sum_MW += PeopleQuantity_i;

                    //職員人數
                    sum_docTable.Rows[2 + sum_row_inde].Cells[5].Paragraphs.First().Append(entrylist.getProperty("staff_quantity", "")).Font("標楷體").FontSize(11);
                    int StaffQuantity_i = int.Parse(entrylist.getProperty("staff_quantity", ""));
                    Sum_Staff_qua += StaffQuantity_i;

                    //合計
                    int PeopleSum = StaffQuantity_i + PeopleQuantity_i;
                    sum_docTable.Rows[2 + sum_row_inde].Cells[6].Paragraphs.First().Append(PeopleSum.ToString()).Font("標楷體").FontSize(11);
                    Sum_MWS += PeopleSum;

                }

                //doc.InsertTable(FinalTotal);
                FinalTotal.Rows[0].Cells[0].Paragraphs.First().Append(sum_row_inde.ToString() + "單位").Font("標楷體").FontSize(11);
                FinalTotal.Rows[0].Cells[1].Paragraphs.First().Append(Sum_Man_qua.ToString()).Font("標楷體").FontSize(11);
                FinalTotal.Rows[0].Cells[2].Paragraphs.First().Append(Sum_Wonan_qua.ToString()).Font("標楷體").FontSize(11);
                FinalTotal.Rows[0].Cells[3].Paragraphs.First().Append(Sum_MW.ToString()).Font("標楷體").FontSize(11);
                FinalTotal.Rows[0].Cells[4].Paragraphs.First().Append(Sum_Staff_qua.ToString()).Font("標楷體").FontSize(11);
                FinalTotal.Rows[0].Cells[5].Paragraphs.First().Append(Sum_MWS.ToString()).Font("標楷體").FontSize(11);

                sum_docTable.Rows[2].Remove();
            }
        }

        private void SetTableRow(Xceed.Document.NET.Table docTable, Item item, string property, int rIdx)
        {
            string values = item.getProperty(property, "").Trim(',');
            if (values == "") return;

            string[] names = values.Split(',');
            for (int i = 0; i < names.Length; i++)
            {
                var name = names[i];

                var sect = name
                    .Replace("Woman", "女子組")
                    .Replace("Man", "男子組")
                    .Replace("Mix", "混合組");

                var members = item.getProperty(name, "");

                //組別
                docTable.Rows[rIdx + i].Cells[0].Paragraphs.First().Append(sect).Font("標楷體").FontSize(11);
                //該組成員
                docTable.Rows[rIdx + i].Cells[1].Paragraphs.First().Append(members).Font("標楷體").FontSize(11);
            }
        }

        private void AppendOrgTable(TConfig cfg, Item MeetingUsers, Dictionary<string, Item> DicOrgs, Xceed.Document.NET.Table docTable)
        {
            int WSRow = 4;
            for (int i = 0; i < MeetingUsers.getItemCount(); i++)
            {
                WSRow = i + 4;
                var In_Meeting_User = MeetingUsers.getItemByIndex(i);
                var OrgName = In_Meeting_User.getProperty("in_current_org", "") + In_Meeting_User.getProperty("in_l1", "");

                Item DicOrg = DicOrgs[OrgName];

                var OrgWSRow = int.Parse(DicOrg.getProperty("last_row"));
                OrgWSRow = OrgWSRow + 1;

                DicOrg.setProperty("last_row", OrgWSRow.ToString());

                string company_name = DicOrg.getProperty("company_name", "");//單位名稱
                string leader = DicOrg.getProperty("leader", "");//領隊
                string coach = DicOrg.getProperty("coach", "");//教練
                string contact_person = DicOrg.getProperty("contact_person", "");//管理
                string management = DicOrg.getProperty("management", "");//聯絡人
                string mobile_phone = DicOrg.getProperty("mobile_phone", "");//手機
                string phone = DicOrg.getProperty("phone", "");//電話
                int man_quantity = int.Parse(DicOrg.getProperty("man_quantity", ""));//男生人數
                int woman_quantity = int.Parse(DicOrg.getProperty("woman_quantity", ""));//女生人數
                int staff_quantity = int.Parse(DicOrg.getProperty("staff_quantity", ""));//隊職員人數

                int man_team_quantity = int.Parse(DicOrg.getProperty("man_team_quantity", ""));//男生組數
                int woman_team_quantity = int.Parse(DicOrg.getProperty("woman_team_quantity", ""));//女生組數
                int mix_team_quantity = int.Parse(DicOrg.getProperty("mix_team_quantity", ""));//混合組數

                string man_team_names = DicOrg.getProperty("man_team_names", "");//男生組數
                string woman_team_names = DicOrg.getProperty("woman_team_names", "");//女生組數
                string mix_team_names = DicOrg.getProperty("mix_team_names", "");//混合組數


                string in_name = In_Meeting_User.getProperty("in_name", "");
                string in_gender = In_Meeting_User.getProperty("in_gender", "");
                string in_l1 = In_Meeting_User.getProperty("in_l1", "");
                string in_l2 = In_Meeting_User.getProperty("in_l2", "");
                string in_l3 = In_Meeting_User.getProperty("in_l3", "");
                string in_current_org = In_Meeting_User.getProperty("in_current_org", "");
                string in_team = In_Meeting_User.getProperty("in_team", "");

                bool isStaff = in_l1 == "隊職員";
                bool isMan = in_gender == "男";
                bool isWoman = in_gender == "女";

                string ManPropertyName = "";
                string GroupTotal = "";
                string GroupQuantity = "";

                //取得聯絡人

                if (management == "")
                {
                    Item Manager = GetResumeItem(cfg, In_Meeting_User);

                    if (Manager.getResult() != "")
                    {
                        //CCO.Utilities.WriteDebug(strMethodName, "sql:" + sql);
                        //CCO.Utilities.WriteDebug(strMethodName, "Manager:" + Manager.dom.InnerXml);
                        Manager = Manager.getItemByIndex(0);
                        DicOrg.setProperty("management", Manager.getProperty("in_name", ""));
                        DicOrg.setProperty("mobile_phone", Manager.getProperty("in_tel", ""));

                        phone = Manager.getProperty("in_tel", "");
                        management = Manager.getProperty("in_name", "");
                    }
                }

                if (in_l2.Contains("男子") || in_l3.Contains("男"))
                {
                    ManPropertyName = "Man" + In_Meeting_User.getProperty("in_team", "");
                    GroupTotal = "man" + In_Meeting_User.getProperty("in_team", "");
                }
                else if (in_l2.Contains("女子") || in_l3.Contains("女"))
                {
                    ManPropertyName = "Woman" + In_Meeting_User.getProperty("in_team", "");
                    GroupTotal = "woman" + In_Meeting_User.getProperty("in_team", "");
                }
                else if (in_l2.Contains("男女"))
                {
                    ManPropertyName = "Mix" + In_Meeting_User.getProperty("in_team", "");
                    GroupTotal = "mix" + In_Meeting_User.getProperty("in_team", "");
                }
                else if (in_l2.Contains("領隊"))
                {
                    ManPropertyName = "leader";
                    GroupTotal = "Leader";
                }
                else if (in_l2.Contains("教練"))
                {
                    ManPropertyName = "coach";
                    GroupTotal = "Coach";
                }
                else if (in_l2.Contains("管理"))
                {
                    ManPropertyName = "contact_person";
                    GroupTotal = "Contact_person";
                }
                else if (in_l2 == "高中組")
                {
                    ManPropertyName = "Mix" + In_Meeting_User.getProperty("in_team", "");
                    GroupTotal = "mix" + In_Meeting_User.getProperty("in_team", "");
                }
                else if (in_l2 == "國中組")
                {
                    ManPropertyName = "Mix" + In_Meeting_User.getProperty("in_team", "");
                    GroupTotal = "mix" + In_Meeting_User.getProperty("in_team", "");
                }
                else
                {
                    throw new Exception("in_l2 無法分析: " + in_l2);
                }

                string MySectionNames = DicOrg.getProperty(ManPropertyName, "");//依據組別取出值(姓名)
                string Group = DicOrg.getProperty(GroupTotal, "");////依據組別取出值(組)

                //判定是否有重複組別
                if (!Group.Contains(ManPropertyName))
                {
                    //沒有的話將現在組別塞進去
                    DicOrg.setProperty(GroupTotal, ManPropertyName);

                    //計算男女生組數
                    if (ManPropertyName.Contains("Woman"))
                    {
                        woman_team_quantity++;
                        DicOrg.setProperty("woman_team_quantity", woman_team_quantity.ToString());
                        woman_team_names = woman_team_names + ManPropertyName + ",";
                        DicOrg.setProperty("woman_team_names", woman_team_names);
                    }
                    else if (ManPropertyName.Contains("Man"))
                    {
                        man_team_quantity++;
                        DicOrg.setProperty("man_team_quantity", man_team_quantity.ToString());
                        man_team_names = man_team_names + ManPropertyName + ",";
                        DicOrg.setProperty("man_team_names", man_team_names);
                    }
                    else if (ManPropertyName.Contains("Mix"))
                    {
                        mix_team_quantity++;
                        DicOrg.setProperty("mix_team_quantity", mix_team_quantity.ToString());
                        mix_team_names = mix_team_names + ManPropertyName + ",";
                        DicOrg.setProperty("mix_team_names", mix_team_names);
                    }
                }

                //判定是否有重複名字
                if (!MySectionNames.Contains(in_name))
                {
                    //沒有的話依照組別取出再塞新的名字進去
                    MySectionNames = MySectionNames + in_name + " ";
                    DicOrg.setProperty(ManPropertyName, MySectionNames);

                    //計算隊職員 男生 女生人數
                    if (isStaff)
                    {
                        staff_quantity++;
                        DicOrg.setProperty("staff_quantity", staff_quantity.ToString());
                    }
                    else
                    {
                        if (isMan)
                        {
                            man_quantity++;
                            DicOrg.setProperty("man_quantity", man_quantity.ToString());
                        }
                        else if (isWoman)
                        {
                            woman_quantity++;
                            DicOrg.setProperty("woman_quantity", woman_quantity.ToString());
                        }
                    }
                }

                if (!isStaff)
                {
                    if (isMan) SetGenderQuantity(DicOrg, In_Meeting_User, "male_quantity", "male_keys");
                    else if (isWoman) SetGenderQuantity(DicOrg, In_Meeting_User, "female_quantity", "female_keys");
                }

                DicOrg.setProperty("in_l1", in_l1);
                DicOrg.setProperty("in_current_org", in_current_org);
            }
        }

        private void SetGenderQuantity(Item DicOrg, Item In_Meeting_User, string qProperty, string kProperty)
        {
            string key = In_Meeting_User.getProperty("in_sno", "").ToUpper();
            if (key == "") return;

            var oldKeys = DicOrg.getProperty(kProperty, "");
            var oldQuantity = int.Parse(DicOrg.getProperty(qProperty, "0"));

            if (oldKeys.Contains(key)) return;

            var newKeys = oldKeys + key + ",";
            var newQuantity = oldQuantity + 1;

            DicOrg.setProperty(kProperty, newKeys);
            DicOrg.setProperty(qProperty, newQuantity.ToString());
        }

        private string GetXlsxName(TConfig cfg)
        {
            string xlsName = cfg.itmMeeting.getProperty("in_title", "");

            xlsName = xlsName.Replace("+", "")
                            .Replace("-", "")
                            .Replace(".", "")
                            .Replace(" ", "");

            return xlsName;
        }

        private void SetCellTextValue(ref ClosedXML.Excel.IXLCell Cell, string TextValue)
        {
            DateTime dtTmp;
            double dblTmp;

            if (DateTime.TryParse(TextValue, out dtTmp))
            {
                Cell.Value = "'" + TextValue;
            }
            else if (double.TryParse(TextValue, out dblTmp))
            {
                Cell.Value = "'" + TextValue;
            }
            else
            {
                Cell.Value = TextValue;
            }
            Cell.DataType = ClosedXML.Excel.XLDataType.Text;

        }

        private void AppendDicOrgs(Dictionary<string, Item> DicOrgs, Item Orgs)
        {
            for (int i = 0; i < Orgs.getItemCount(); i++)
            {
                Item Org = Orgs.getItemByIndex(i);
                string C1 = Org.getProperty("c1", "");

                string OrgName = C1;

                Org.setProperty("sheet_index", "0");
                Org.setProperty("last_row", "3");

                //預設(?)
                Org.setProperty("company_name", "");//單位名稱
                Org.setProperty("leader", "");//領隊
                Org.setProperty("coach", "");//教練
                Org.setProperty("contact_person", "");//管理
                Org.setProperty("management", "");//聯絡人
                Org.setProperty("mobile_phone", "");//手機
                Org.setProperty("phone", "");//電話
                Org.setProperty("man_quantity", "0");//男生人數
                Org.setProperty("woman_quantity", "0");//女生人數
                Org.setProperty("staff_quantity", "0");//隊職員人數
                Org.setProperty("man_team_quantity", "0");//男生組數
                Org.setProperty("woman_team_quantity", "0");//女生組數
                Org.setProperty("mix_team_quantity", "0");//混合組數

                Org.setProperty("male_quantity", "0");//新的男生人數
                Org.setProperty("female_quantity", "0");//新的女生人數

                DicOrgs.Add(OrgName, Org);
            }
        }

        private void SetLevelName(TConfig cfg, Dictionary<string, Item> StudentFields)
        {
            string L1Name = "";
            string L2Name = "";
            string L3Name = "";

            foreach (KeyValuePair<string, Item> StudentField in StudentFields)
            {
                var item = StudentField.Value;
                var in_property = item.getProperty("in_property", "");

                switch (in_property)
                {
                    case "in_l1": L1Name = in_property; break;
                    case "in_l2": L2Name = in_property; break;
                    case "in_l3": L3Name = in_property; break;
                }
            }

            cfg.L1Name = L1Name;
            cfg.L2Name = L2Name;
            cfg.L3Name = L3Name;
        }

        private void SetExportPath(TConfig cfg)
        {
            //開始整理EXCEL Template

            var aml = "<AML>" +
                "<Item type='In_Variable' action='get'>" +
                "<in_name>meeting_excel</in_name>" +
                "<Relationships>" +
                "<Item type='In_Variable_Detail' action='get'/>" +
                "</Relationships>" +
                "</Item></AML>";

            var Vairable = cfg.inn.applyAML(aml);
            var items = Vairable.getRelationships("In_Variable_Detail");

            var Template_Path = "";
            var Export_Path = "";
            for (int i = 0; i < items.getItemCount(); i++)
            {
                Item item = items.getItemByIndex(i);
                var in_name = item.getProperty("in_name", "");
                var in_value = item.getProperty("in_value", "");

                if (in_name == "template_1_path")
                {
                    Template_Path = in_value;
                }

                if (in_name == "export_path")
                {
                    Export_Path = in_value;
                    if (!Export_Path.EndsWith(@"\"))
                    {
                        Export_Path = Export_Path + @"\";
                    }
                }
            }

            cfg.template_path = Template_Path;
            cfg.export_path = Export_Path;
        }

        private Item GetResumeItem(TConfig cfg, Item In_Meeting_User)
        {
            var in_group = In_Meeting_User.getProperty("in_group", "");
            var in_creator = In_Meeting_User.getProperty("in_creator", "");

            var sql = "SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_group = N'" + in_group + "' AND in_is_teacher = 1";

            if (in_creator != "")
            {
                sql += " AND in_name = N'" + in_creator + "'";
            }

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingUserGroupItems(TConfig cfg)
        {
            var sql = "SELECT in_l1, in_l2, in_l3, in_current_org, in_team, in_name, in_waiting_list FROM IN_MEETING_USER WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_l1 <> '隊職員'"
                + " ORDER BY in_l1 ,in_l2 desc,in_l3,in_current_org,in_team,in_waiting_list";

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingL3Items(TConfig cfg)
        {
            var sql = "SELECT DISTINCT in_l3, in_l1, in_l2, COUNT(CONCAT(in_current_org,in_team,in_l1,in_l2,in_l3)) AS 'c1' FROM IN_MEETING_USER WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_note_state = 'official'"
                + cfg.onlyMyGym1
                + " GROUP by in_l1, in_l2, in_l3"
                + " ORDER BY in_l1, in_l2 DESC, in_l3";

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingL1Items(TConfig cfg)
        {
            var sql = "SELECT DISTINCT in_l1 AS 'c1' FROM IN_MEETING_USER WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_note_state = 'official'"
                + cfg.onlyMyGym1;

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingUserItems(TConfig cfg)
        {
            var sql = "SELECT id, in_name, in_gender, in_sno, in_l1, in_l2, in_l3, in_index, in_team, in_current_org, in_group, in_waiting_list, in_creator, in_creator_sno FROM 	IN_MEETING_USER WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_note_state = 'official'"
                + cfg.onlyMyGym1
                + " ORDER BY in_current_org, in_team, in_l1, in_l2, in_l3, in_index";

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingOrgItems(TConfig cfg)
        {
            var sql = "SELECT DISTINCT CONCAT (in_current_org, in_l1) AS 'c1' FROM IN_MEETING_USER WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_note_state = 'official'"
                + cfg.onlyMyGym1;

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingItem(TConfig cfg)
        {
            var aml = "<AML>" +
               "<Item type='In_Meeting' action='get' id='" + cfg.meeting_id + "'>" +
               "<Relationships>" +

               "<Item type='In_Meeting_Surveys' action='get' select='related_id(id,in_questions,in_request,in_selectoption,in_question_type,in_property,in_expense,in_is_pkey)'>" +
               "<in_surveytype>1</in_surveytype>" +
               "<in_property condition='in'>'in_l1','in_l2','in_l3'</in_property>" +
               "</Item>" +

               "</Relationships>" +
                "</Item></AML>";

            return cfg.inn.applyAML(aml);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string meeting_id { get; set; }
            public bool isGymOwner { get; set; }
            public Item itmMeeting { get; set; }
            public Item itmResume { get; set; }

            public string template_path { get; set; }
            public string export_path { get; set; }

            public string L1Name { get; set; }
            public string L2Name { get; set; }
            public string L3Name { get; set; }

            public int ColIndex { get; set; }

            public string onlyMyGym1 { get; set; }
            public string onlyMyGym2 { get; set; }

            public string ExpenseProperty { get; set; }

            public string group_1 { get; set; }
            public string group_2 { get; set; }
        }
    }
}