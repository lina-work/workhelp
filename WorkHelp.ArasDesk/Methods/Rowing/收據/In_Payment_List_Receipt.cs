﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Rowing.Common
{
    public class In_Payment_List_Receipt : Item
    {
        public In_Payment_List_Receipt(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 下載收據(PDF)
        - Spire.Xls 需 license
        - 請於 method-config.xml 引用 DLL
    日期: 
        - 2023-01-13: 改為划船版 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Payment_List_Receipt";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                pay_numbers = itmR.getProperty("paynumbers", "").Trim(','),
                mode = itmR.getProperty("mode", ""),
                page = itmR.getProperty("page", ""),
                scene = itmR.getProperty("scene", ""),
            };

            //檢查參數
            if (cfg.meeting_id == "" || cfg.pay_numbers == "")
            {
                throw new Exception("參數錯誤");
            }

            //繳費單編號清單
            cfg.pay_number_arr = cfg.pay_numbers.Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (cfg.pay_number_arr == null || cfg.pay_number_arr.Length == 0)
            {
                throw new Exception("參數錯誤");
            }

            //取得登入者資訊
            cfg.itmLogin = inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = '" + cfg.strUserId + "'");
            if (cfg.itmLogin.isError() || cfg.itmLogin.getResult() == "")
            {
                throw new Exception("登入者履歷異常");
            }

            //登入者帳號
            cfg.login_sno = cfg.itmLogin.getProperty("in_sno", "");
            cfg.login_name = cfg.itmLogin.getProperty("login_name", "").ToUpper();
            cfg.login_is_admin = cfg.itmLogin.getProperty("in_is_admin", "");
            cfg.login_member_type = cfg.itmLogin.getProperty("in_member_type", "");
            cfg.login_member_role = cfg.itmLogin.getProperty("in_member_role", "");

            if (cfg.login_is_admin != "1")
            {
                CCO.Utilities.WriteDebug(strMethodName, "登入者: " + cfg.login_sno + ", pay_numbers: " + cfg.pay_numbers + " _# 無權限");
                throw new Exception("無下載權限");
            }

            //設定資料表及欄位
            cfg.meeting_table = "IN_MEETING";
            cfg.mt_user_table = "IN_MEETING_USER";
            cfg.mt_resume_table = "IN_MEETING_RESUMELIST";
            cfg.meeting_column = "in_meeting";
            if (cfg.mode == "cla")
            {
                cfg.meeting_table = "IN_CLA_MEETING";
                cfg.mt_user_table = "IN_CLA_MEETING_USER";
                cfg.mt_resume_table = "IN_CLA_MEETING_RESUMELIST";
                cfg.meeting_column = "in_cla_meeting";
            }

            //取得活動資料
            cfg.itmMeeting = cfg.inn.applySQL("SELECT * FROM " + cfg.meeting_table + " WHERE id = '" + cfg.meeting_id + "'");
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("活動資訊異常");
            }

            //設定活動資料
            AppendMeetingInfo(cfg);

            //設定權限
            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            cfg.isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            cfg.isCommittee = itmPermit.getProperty("isCommittee", "") == "1";

            //是否為共同講師
            bool open = GetUserResumeListStatus(cfg);
            if (open) cfg.isMeetingAdmin = true;

            //匯出收據
            ExportReceiptPDF(cfg, itmR);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }


        //設定活動資料
        private void AppendMeetingInfo(TConfig cfg)
        {
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.mt_type = cfg.itmMeeting.getProperty("in_meeting_type", "");
            cfg.mt_address = cfg.itmMeeting.getProperty("in_address", "");
            cfg.mt_bank_name = cfg.itmMeeting.getProperty("in_bank_name", "");
            cfg.mt_tel = cfg.itmMeeting.getProperty("in_tel", "");
            cfg.mt_receipt = cfg.itmMeeting.getProperty("in_receipt", "").Trim();
            cfg.staff_map = GetStaffMap(cfg, cfg.mt_receipt);
            SetReceiptTitleFunc(cfg);
        }

        private void ExportReceiptPDF(TConfig cfg, Item itmReturn)
        {
            var xlsx = GetXlsx(cfg);

            var book = new Spire.Xls.Workbook();
            book.LoadFromFile(xlsx.template_path);

            var sheetTemplate = book.Worksheets[0];

            for (int i = 0; i < cfg.pay_number_arr.Length; i++)
            {
                var pay_number = cfg.pay_number_arr[i];
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "登入者: " + cfg.login_sno + ", pay_number: " + pay_number + " _# start");

                //2020.09.29 為了取得關聯(In_Meeting_invoice) 改用此方式
                var aml = "<AML>" +
                    "<Item type='In_Meeting_Pay' action='get'>" +
                    "<item_number>" + pay_number + "</item_number>" +
                    "<Relationships>" +
                    "<Item type='In_Meeting_invoice' action='get'/>" +
                    "</Relationships>" +
                    "</Item></AML>";

                Item itmMeetingPay = cfg.inn.applyAML(aml);
                if (itmMeetingPay.isError())
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "登入者: " + cfg.login_sno + ", pay_number: " + pay_number + " _# error: 繳費單資料發生錯誤");
                    continue;
                }

                //各類申請-取得二階項目
                itmMeetingPay.setProperty("in_l2_title", getTitle(cfg, itmMeetingPay));

                var pay = GetMeetingPay(cfg, itmMeetingPay);

                //(發票/統編)關聯數量
                int invoice_count = itmMeetingPay.getRelationships("In_Meeting_invoice").getItemCount();
                if (invoice_count > 0)
                {
                    AppendSheetHasInvoice(cfg, book, sheetTemplate, pay, invoice_count);
                }
                else
                {
                    AppendSheetNonInvoice(cfg, book, sheetTemplate, pay);
                }
            }

            sheetTemplate.Remove();

            book.SaveToFile(xlsx.file, Spire.Xls.FileFormat.PDF);

            itmReturn.setProperty("pdf_name", xlsx.name);
        }

        //取得共同講師 (判斷現在的登入者是否為此課程的共同講師)
        private bool GetUserResumeListStatus(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t2.id
                FROM 
                    {#table} t1 WITH(NOLOCK) 
                INNER JOIN 
                    IN_RESUME t2 WITH(NOLOCK) 
                    ON t2.id = t1.related_id    
                WHERE 
                    t1.source_id = '{#meeting_id}' 
                    AND t2.in_user_id = '{#in_user_id}';    
                ";

            sql = sql.Replace("{#table}", cfg.mt_resume_table)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_user_id}", cfg.strUserId);

            Item item = cfg.inn.applySQL(sql);

            return item.getResult() != "";
        }

        private void AppendSheetHasInvoice(TConfig cfg, Spire.Xls.Workbook book, Spire.Xls.Worksheet sheetTemplate, TPay pay, int count)
        {
            int c = 1;
            for (int i = 0; i < count; i++)
            {
                var num = count == 1 ? "" : "-" + c;
                var itmMeetingInvoice = pay.item.getRelationships("In_Meeting_invoice").getItemByIndex(i);
                var receipt = GetReceipt(cfg, pay, itmMeetingInvoice, num);

                var list = new List<string>();
                list.Add(pay.item_number.Replace("-", "_"));
                list.Add("INV");
                list.Add((i + 1).ToString());

                Spire.Xls.Worksheet sheet = book.CreateEmptySheet();
                sheet.CopyFrom(sheetTemplate);
                sheet.Name = string.Join("_", list);

                RefreshSheet(cfg, sheet, pay, receipt);

                //適合頁面
                book.ConverterSetting.SheetFitToPage = true;
                c++;
            }
        }

        private void AppendSheetNonInvoice(TConfig cfg, Spire.Xls.Workbook book, Spire.Xls.Worksheet sheetTemplate, TPay pay)
        {
            var receipt = GetReceiptFromPay(cfg, pay);

            Spire.Xls.Worksheet sheet = book.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = pay.in_creator + "_" + pay.item_number;

            RefreshSheet(cfg, sheet, pay, receipt);

            //適合頁面
            book.ConverterSetting.SheetFitToPage = true;
        }

        private void RefreshSheet(TConfig cfg, Spire.Xls.Worksheet sheet, TPay pay, TReceipt receipt)
        {
            //（第一聯）
            sheet.Range["J1"].Text = receipt.no;
            sheet.Range["I4"].Text = receipt.day;
            sheet.Range["E7"].Text = receipt.title;
            sheet.Range["E9"].Text = receipt.expense;
            sheet.Range["E11"].Text = pay.account_for;
            //sheet.Range["F13"].Text = pay.undertake;
            //sheet.Range["H13"].Text = cfg.staff_map.account;
            //sheet.Range["I13"].Text = cfg.staff_map.chairman;

            //（第二聯）
            sheet.Range["J18"].Text = receipt.no;
            sheet.Range["I21"].Text = receipt.day;
            sheet.Range["E24"].Text = receipt.title;
            sheet.Range["E26"].Text = receipt.expense;
            sheet.Range["E28"].Text = pay.account_for;
            //sheet.Range["F28"].Text = pay.undertake;
            //sheet.Range["H28"].Text = cfg.staff_map.account;
            //sheet.Range["I28"].Text = cfg.staff_map.chairman;

            // //（第三聯 收執聯）
            sheet.Range["J35"].Text = receipt.no;
            sheet.Range["I38"].Text = receipt.day;
            sheet.Range["E41"].Text = receipt.title;
            sheet.Range["E43"].Text = receipt.expense;
            sheet.Range["E45"].Text = pay.account_for;
            //sheet.Range["F43"].Text = pay.undertake;
            //sheet.Range["H43"].Text = cfg.staff_map.account;
            //sheet.Range["I43"].Text = cfg.staff_map.chairman;

            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 1;
            //sheet.PageSetup.TopMargin = 0.3;
            sheet.PageSetup.LeftMargin = 0.5;
            sheet.PageSetup.RightMargin = 0.5;
            sheet.PageSetup.BottomMargin = 0.5;
        }

        private string getTitle(TConfig cfg, Item itmMeetingPay)
        {
            string in_paynumber = itmMeetingPay.getProperty("item_number", "");

            string sql = @"
                SELECT 
	                in_paynumber
	                , MAX(in_l2) AS 'in_l2' 
                FROM 
	                {#table} WITH (NOLOCK)
                WHERE 
	                in_paynumber = '{#in_paynumber}'
                GROUP BY 
	                in_paynumber
            ";

            sql = sql.Replace("{#table}", cfg.mt_user_table)
                .Replace("{#in_paynumber}", in_paynumber);

            Item itmSql = cfg.inn.applySQL(sql);

            if (!itmSql.isError() && itmSql.getItemCount() > 0)
            {
                return itmSql.getProperty("in_l2", "");
            }

            return "";
        }

        //數字轉中文大寫
        private string NumberToCHS(string Val)
        {
            string result = "";

            string CN_ZERO = "零";
            string CN_ONE = "壹";
            string CN_TWO = "貳";
            string CN_THREE = "參";
            string CN_FOUR = "肆";
            string CN_FIVE = "伍";
            string CN_SIX = "陸";
            string CN_SEVEN = "柒";
            string CN_EIGHT = "捌";
            string CN_NINE = "玖";
            string CN_TEN = "拾";
            string CN_HUNDRED = "佰";
            string CN_THOUSAND = "仟";
            string CN_TEN_THOUSAND = "萬";
            string CN_HUNDRED_MILLION = "億";
            //string CN_SYMBOL = "$";
            string CN_SYMBOL = "";
            string CN_DOLLAR = "元";
            string CN_TEN_CENT = "角";
            string CN_CENT = "分";
            string CN_INTEGER = "整";
            string[] aDigits = new string[] { CN_ZERO, CN_ONE, CN_TWO, CN_THREE, CN_FOUR, CN_FIVE, CN_SIX, CN_SEVEN, CN_EIGHT, CN_NINE };
            string[] aRadices = new string[] { "", CN_TEN, CN_HUNDRED, CN_THOUSAND };
            string[] aBigRadices = new string[] { "", CN_TEN_THOUSAND, CN_HUNDRED_MILLION };
            string[] aDecimals = new string[] { CN_TEN_CENT, CN_CENT };

            string[] aParts = null;
            string sInt = "";
            string sDcm = "";

            int iInt = 0;
            int iDcm = 0; // Represent decimal part of digit number.  

            int zeroCount = int.MinValue;
            int i = int.MinValue;
            int p = int.MinValue;
            int d = int.MinValue;

            int quotient = 0;
            int modulus = 0;

            aParts = Val.Split('.');
            if (aParts.Length > 1)
            {
                sInt = aParts[0];
                sDcm = aParts[1];
                if (sDcm.Length > 2) sDcm = sDcm.Substring(0, 2);
            }
            else
            {
                sInt = aParts[0];
                sDcm = "";
            }

            iInt = GetInt(sInt);

            if (iInt > 0)
            {
                zeroCount = 0;
                for (i = 0; i < sInt.Length; i++)
                {
                    p = sInt.Length - i - 1;
                    d = GetInt(sInt.Substring(i, 1));

                    quotient = p / 4;
                    modulus = p % 4;
                    if (d == 0)
                    {
                        zeroCount++;
                    }
                    else
                    {
                        if (zeroCount > 0)
                        {
                            result += aDigits[0];
                        }
                        zeroCount = 0;
                        result += aDigits[(int)(d)] + aRadices[modulus];
                    }
                    if (modulus == 0 && zeroCount < 4)
                    {
                        result += aBigRadices[quotient];
                    }
                }
                result += CN_DOLLAR;
            }
            if (sDcm.Length > 0)
            {
                for (i = 0; i < sDcm.Length; i++)
                {
                    d = GetInt(sDcm.Substring(i, 1));
                    if (d != 0)
                    {
                        result += aDigits[(int)(d)] + aDecimals[i];
                    }
                }
            }
            if (result.Length == 0)
            {
                result = CN_ZERO + CN_DOLLAR;
            }
            if (sDcm.Length == 0)
            {
                result += CN_INTEGER;
            }

            result = CN_SYMBOL + result;

            return result;
        }

        private void SetReceiptTitleFunc(TConfig cfg)
        {
            cfg.receipt_title = "";
            switch (cfg.mt_type)
            {
                case "game": //比賽
                    cfg.receipt_title = cfg.mt_title + "報名費";
                    break;

                case "degree": //晉段
                    cfg.receipt_title = cfg.mt_title + "晉段費";
                    break;

                case "seminar": //講習
                    cfg.receipt_title = cfg.mt_title + "報名費";
                    break;

                case "payment": //會員年費
                    if (cfg.mt_title.Contains("年費"))
                    {
                        cfg.receipt_title = cfg.mt_title;
                    }
                    else if (cfg.mt_title.Contains("會費"))
                    {
                        cfg.receipt_title = cfg.mt_title;
                    }
                    else
                    {
                        cfg.receipt_title = cfg.mt_title + "會費";
                    }
                    break;

                case "verify": //各類申請
                    cfg.getReceiptTitle = getReceiptTitle;
                    break;

                default:
                    break;
            }
        }

        private string getReceiptTitle(TConfig cfg, Item itmMeetingPay)
        {
            return itmMeetingPay.getProperty("in_l2_title", "") + "申請費用";
        }

        private TStaffMap GetStaffMap(TConfig cfg, string in_receipt)
        {
            var result = new TStaffMap { received = "", account = "", chairman = "", referee = "", coach = "", admin = "" };
            if (in_receipt == "") return result;

            //人員名冊
            var staffArr = cfg.mt_receipt.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (staffArr == null || staffArr.Length == 0)
            {
                return result;
            }

            foreach (var staff in staffArr)
            {
                var cols = staff.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                if (cols == null || cols.Length == 0) continue;

                string tag = cols[0].Trim();
                string names = cols.Length <= 1 ? "" : cols[1].Trim();

                switch (tag)
                {
                    case "經收人": result.received = names; break;
                    case "會計": result.account = names; break;
                    case "理事長": result.chairman = names; break;
                    case "會長": result.chairman = names; break;
                    case "裁判組": result.referee = names; break;
                    case "訓練組": result.coach = names; break;
                    case "行政組": result.admin = names; break;
                    default: break;
                }
            }

            return result;
        }

        private TPay GetMeetingPay(TConfig cfg, Item itmMeetingPay)
        {
            var result = new TPay();
            //付款方式
            result.pay_mode = "超商繳款";
            //繳費單號
            result.item_number = itmMeetingPay.getProperty("item_number", "");
            //所屬群組
            result.in_group = itmMeetingPay.getProperty("in_group", "");
            //協助報名者
            result.in_creator = itmMeetingPay.getProperty("in_creator", "");
            result.in_creator_sno = itmMeetingPay.getProperty("in_creator_sno", "");
            //收據編號
            result.rec_number = itmMeetingPay.getProperty("rec_number", "");
            //當前時間
            result.currentTime = System.DateTime.Now;
            result.in_fax = "缺傳真號碼";
            result.in_case_number = "缺立案證號";
            result.in_company_number = "缺統一編號";

            //付款性質
            result.account_for = cfg.receipt_title;
            if (result.account_for == "" && cfg.getReceiptTitle != null)
            {
                result.account_for = cfg.getReceiptTitle(cfg, itmMeetingPay);
            }

            result.amount_real = GetInt(itmMeetingPay.getProperty("in_pay_amount_real", "0"));
            result.refund_amount = GetInt(itmMeetingPay.getProperty("in_refund_amount", "0"));
            result.fee_amount = GetInt(itmMeetingPay.getProperty("in_fee_amount", "0"));

            if (result.amount_real == 0)
            {
                result.amount_real = GetInt(itmMeetingPay.getProperty("in_pay_amount_exp", "0"));
            }

            //金額已全退
            result.in_amount = result.amount_real - result.refund_amount - result.fee_amount;
            if (result.in_amount == 0)
            {
                result.account_for = cfg.mt_title + "退款手續費";
            }

            if (result.in_amount == result.fee_amount)
            {
                throw new Exception("所選項目已完全退款，無法下載收據");
            }

            //民國年月日 (8碼): 開立日期: 108 年 12 月  27 日 
            result.y = result.currentTime.Year.ToString();
            result.m = result.currentTime.Month.ToString();
            result.d = result.currentTime.Day.ToString();
            result.tw_year = (result.currentTime.Year - 1911).ToString();
            result.tw_date = result.tw_year + " 年 " + result.m + " 月 " + result.d + " 日";

            //動態判斷
            result.undertake = cfg.staff_map.received;
            string in_undertake = itmMeetingPay.getProperty("in_undertake", "");
            switch (in_undertake)
            {
                case "裁判組":
                    result.undertake = "經收人:" + cfg.staff_map.referee;
                    break;
                case "訓練組":
                    result.undertake = "經收人:" + cfg.staff_map.coach;
                    break;
                case "行政組":
                    result.undertake = "經收人:" + cfg.staff_map.admin;
                    break;
            }

            result.item = itmMeetingPay;

            return result;
        }

        private TReceipt GetReceipt(TConfig cfg, TPay pay, Item itmMeetingInvoice, string num)
        {
            var result = new TReceipt();
            //發票抬頭
            result.invoice_up = itmMeetingInvoice.getProperty("invoice_up", "").Trim();
            //開立之統一編號
            result.uniform_numbers = itmMeetingInvoice.getProperty("uniform_numbers", "");
            //實際收款金額
            result.invoice_amount = itmMeetingInvoice.getProperty("invoice_amount", "").TrimStart('0').Replace(".00", "");

            result.show_title = pay.in_creator;
            if (result.invoice_up != "")
            {
                result.show_title = result.invoice_up;
            }

            result.amount = GetInt(result.invoice_amount);
            result.tw_amount = NumberToCHS(result.invoice_amount).Replace("元整", "");

            result.no = pay.tw_year + pay.item_number + num;
            result.day = pay.tw_date;
            result.title = result.show_title + "  (統一編號：" + result.uniform_numbers + ")";   //茲收到
            result.expense = result.tw_amount + " NT$" + result.amount.ToString("#,##0");   //共計新台幣

            return result;
        }

        private TReceipt GetReceiptFromPay(TConfig cfg, TPay pay)
        {
            var result = new TReceipt();
            result.amount = pay.amount_real;
            result.invoice_amount = pay.amount_real.ToString();
            result.tw_amount = NumberToCHS(result.invoice_amount).Replace("元整", "");

            result.no = pay.tw_year + "-" + pay.item_number;
            result.day = pay.tw_date;
            result.title = pay.in_creator + "  (統一編號：" + pay.in_creator_sno + ")";
            result.expense = result.tw_amount + " NT$" + result.amount.ToString("#,##0");   //共計新台幣

            return result;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string pay_numbers { get; set; }
            public string mode { get; set; }
            public string page { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmLogin { get; set; }

            public string mt_type { get; set; }
            public string mt_title { get; set; }
            public string mt_address { get; set; }
            public string mt_bank_name { get; set; }
            public string mt_tel { get; set; }
            public string mt_receipt { get; set; }

            public string login_sno { get; set; }
            public string login_name { get; set; }
            public string login_is_admin { get; set; }
            public string login_member_type { get; set; }
            public string login_member_role { get; set; }

            public string meeting_table { get; set; }
            public string meeting_column { get; set; }
            public string mt_user_table { get; set; }
            public string mt_resume_table { get; set; }

            public bool isMeetingAdmin { get; set; }
            public bool isCommittee { get; set; }

            public string[] pay_number_arr { get; set; }

            public TStaffMap staff_map { get; set; }

            public string receipt_title { get; set; }
            public Func<TConfig, Item, string> getReceiptTitle { get; set; }
        }

        private class TStaffMap
        {
            //經收人
            public string received { get; set; }
            //會計
            public string account { get; set; }
            //理事長 or 會長
            public string chairman { get; set; }
            //裁判組
            public string referee { get; set; }
            //訓練組
            public string coach { get; set; }
            //行政組
            public string admin { get; set; }
        }

        private class TPay
        {
            //付款方式
            public string pay_mode { get; set; }
            //繳費單號
            public string item_number { get; set; }
            //所屬群組
            public string in_group { get; set; }
            //協助報名者
            public string in_creator { get; set; }
            //協助報名者身分證號
            public string in_creator_sno { get; set; }
            //收據編號
            public string rec_number { get; set; }

            //當前時間
            public DateTime currentTime { get; set; }
            //傳真號碼
            public string in_fax { get; set; }
            //立案證號
            public string in_case_number { get; set; }
            //統一編號
            public string in_company_number { get; set; }
            //付款性質
            public string account_for { get; set; }
            //費用
            public int amount_real { get; set; }
            //退款額
            public int refund_amount { get; set; }
            //手續費
            public int fee_amount { get; set; }
            //實收費用
            public int in_amount { get; set; }

            public string y { get; set; }
            public string m { get; set; }
            public string d { get; set; }
            public string tw_year { get; set; }
            public string tw_date { get; set; }

            public string undertake { get; set; }

            public Item item { get; set; }
        }

        private class TReceipt
        {
            public string no { get; set; }
            public string day { get; set; }
            public string title { get; set; }
            public string expense { get; set; }

            public string invoice_up { get; set; }
            public string uniform_numbers { get; set; }
            public string invoice_amount { get; set; }
            public string show_title { get; set; }

            public int amount { get; set; }
            public string tw_amount { get; set; }
        }

        private class TXlsx
        {
            public string export_path { get; set; }
            public string template_path { get; set; }

            public string name { get; set; }
            public string file { get; set; }
        }


        private TXlsx GetXlsx(TConfig cfg)
        {
            var result = new TXlsx();

            //一般使用者走這(第一聯)
            var variable_name = cfg.isMeetingAdmin
                ? "receipt_template_path_1"  //委員會以上權限(第一聯,第三聯)
                : "receipt_template_path_2"; //一般使用者走這(第一聯)

            var itmXls = cfg.inn.applyMethod("In_Variable_File", "<in_name>" + variable_name + "</in_name>");
            result.export_path = itmXls.getProperty("export_path", "").TrimEnd('\\');
            result.template_path = itmXls.getProperty("template_path", "");

            var guid = System.Guid.NewGuid().ToString().Substring(0, 6).ToUpper()
                + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff");

            if (!System.IO.Directory.Exists(result.export_path))
            {
                System.IO.Directory.CreateDirectory(result.export_path);
            }

            result.name = cfg.mt_title + "_報名費收據_" + guid + ".pdf";
            result.file = result.export_path + "\\" + result.name;

            return result;
        }

        /// <summary>
        /// 轉換為整數
        /// </summary>
        private int GetInt(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}