﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Rowing.Common
{
    public class In_Payment_List_Receipt_Old : Item
    {
        public In_Payment_List_Receipt_Old(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 下載收據(PDF)
                    - Spire.Xls 需 license
                    - 請於 method-config.xml 引用 DLL
                人員: lina
                日期: 2020-05-27
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Payment_List_Receipt";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;

            //取得前端輸入資料
            string meeting_id = itmR.getProperty("meeting_id", "");
            string pay_numbers = itmR.getProperty("paynumbers", "").Trim(',');
            string mode = itmR.getProperty("mode", "");
            string page = itmR.getProperty("page", "");

            string aml = "";
            string sql = "";
            string strError = "";
            string strUserId = inn.getUserID();
            string strIdentityId = inn.getUserAliases();

            //取得登入者資訊
            sql = "SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = '" + strUserId + "'";
            Item itmLoginResume = inn.applySQL(sql);
            if (itmLoginResume.isError() || itmLoginResume.getResult() == "")
            {
                throw new Exception("登入者履歷異常");
            }

            //登入者帳號
            string in_sno = itmLoginResume.getProperty("in_sno", "");
            string login_name = itmLoginResume.getProperty("login_name", "").ToUpper();
            string in_is_admin = itmLoginResume.getProperty("in_is_admin", "");
            string in_member_type = itmLoginResume.getProperty("in_member_type", "");
            string in_member_role = itmLoginResume.getProperty("in_member_role", "");

            if (login_name == "M001") in_is_admin = "1";

            if (in_is_admin != "1")
            {
                CCO.Utilities.WriteDebug(strMethodName, "登入者: " + in_sno + ", pay_numbers: " + pay_numbers + " _# 無權限");
                throw new Exception("無下載權限");
            }

            string meeeting_type = "";

            if (meeting_id == "" || pay_numbers == "")
            {
                throw new Exception("參數錯誤");
            }

            if (mode == "cla")
            {
                meeeting_type = "in_cla_meeting";
            }
            else
            {
                meeeting_type = "in_meeting";
            }

            //繳費單編號清單
            string[] strArray = pay_numbers.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            if (strArray == null || strArray.Length == 0)
            {
                return itmR;
            }


            //取得賽事資訊
            sql = "SELECT * FROM {#meeeting_type} WHERE [id] = '" + meeting_id + "'";
            sql = sql.Replace("{#meeeting_type}", meeeting_type);
            Item itmMeeting = inn.applySQL(sql);

            //活動標題
            string in_title = itmMeeting.getProperty("in_title", "");

            //Word Template 所在路徑與匯出路徑 (from 原創.設定參數)
            string amlVariable = "<AML>" +
                "<Item type='In_Variable' action='get'>" +
                "<in_name>meeting_excel</in_name>" +
                "<Relationships>" +
                "<Item type='In_Variable_Detail' action='get'/>" +
                "</Relationships>" +
                "</Item></AML>";
            Item itmVairable = inn.applyAML(amlVariable);
            Item itmVairableDetail = itmVairable.getRelationships("In_Variable_Detail");
            int count = itmVairableDetail.getItemCount();

            string templatePath = "";
            string exportPath = "";

            string LoginPermission = "";

            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isCommittee = itmPermit.getProperty("isCommittee", "") == "1";

            //是否為共同講師
            bool open = GetUserResumeListStatus(inn, meeting_id, strUserId);
            if (open) isMeetingAdmin = true;

            bool can_review = false;
            // bool can_review = isMeetingAdmin || isCommittee ;
            //取得登入者的權限 決定下載收據的依據
            if (isMeetingAdmin)
            {
                can_review = true;
            }

            if (can_review)
            {
                LoginPermission = "receipt_template_path_1";//(委員會以上權限)(第一聯,第三聯)
            }
            else
            {
                LoginPermission = "receipt_template_path_2";//一般使用者走這(第一聯)
            }


            for (int i = 0; i < count; i++)
            {
                Item itmDetail = itmVairableDetail.getItemByIndex(i);

                if (itmDetail.getProperty("in_name", "") == LoginPermission)
                {
                    templatePath = itmDetail.getProperty("in_value", "");
                }

                if (itmDetail.getProperty("in_name", "") == "export_path")
                {
                    exportPath = itmDetail.getProperty("in_value", "");
                }

                if (!exportPath.EndsWith(@"\"))
                {
                    exportPath = exportPath + @"\";
                }
            }

            string guid = System.Guid.NewGuid().ToString().Substring(0, 6).ToUpper()
                + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff");

            string pdfName = in_title + "_報名費收據_" + guid;
            string pdfFile = exportPath + pdfName + ".pdf";
            // CCO.Utilities.WriteDebug(strMethodName, "Xls File Source: " + templatePath);
            // CCO.Utilities.WriteDebug(strMethodName, "pdf File Target: " + pdfFile);

            Spire.Xls.Workbook book = new Spire.Xls.Workbook();
            book.LoadFromFile(templatePath);
            Spire.Xls.Worksheet sheetTemplate = book.Worksheets[0];

            for (int i = 0; i < strArray.Length; i++)
            {
                string pay_number = strArray[i];
                CCO.Utilities.WriteDebug(strMethodName, "登入者: " + in_sno + ", pay_number: " + pay_number + " _# start");

                //Item itmMeetingPay = inn.newItem("In_Meeting_Pay", "get");
                //itmMeetingPay.setProperty("item_number", pay_number);
                //itmMeetingPay = itmMeetingPay.apply();

                //2020.09.29 為了取得關聯(In_Meeting_invoice) 改用此方式
                aml = "<AML>" +
                    "<Item type='In_Meeting_Pay' action='get'>" +
                    "<item_number>" + pay_number + "</item_number>" +
                    "<Relationships>" +
                    "<Item type='In_Meeting_invoice' action='get'/>" +
                    "</Relationships>" +
                    "</Item></AML>";
                Item itmMeetingPay = inn.applyAML(aml);

                if (itmMeetingPay.isError())
                {
                    CCO.Utilities.WriteDebug(strMethodName, "登入者: " + in_sno + ", pay_number: " + pay_number + " _# error: 繳費單資料發生錯誤");
                    continue;
                }

                //各類申請-取得二階項目
                itmMeetingPay.setProperty("in_l2_title", getTitle(CCO, inn, itmMeetingPay));

                AppendSheetCTA(book, sheetTemplate, itmMeeting, itmMeetingPay);
            }

            sheetTemplate.Remove();

            book.SaveToFile(pdfFile, Spire.Xls.FileFormat.PDF);

            itmR.setProperty("pdf_name", pdfName + ".pdf");

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private string getTitle(Aras.Server.Core.CallContext CCO, Innovator inn, Item itmMeetingPay)
        {
            string sql = @"SELECT in_paynumber,  MAX(in_l2) as in_l2 FROM in_meeting_user WITH (NOLOCK)
                WHERE in_paynumber ='{#in_paynumber}'
                GROUP BY in_paynumber";
            sql = sql.Replace("{#in_paynumber}", itmMeetingPay.getProperty("item_number", ""));
            Item itmSql = inn.applySQL(sql);

            if (!itmSql.isError() && itmSql.getItemCount() > 0)
            {
                return itmSql.getProperty("in_l2", "");
            }
            return "";
        }
        //取得共同講師 (判斷現在的登入者是否為此課程的共同講師)
        private bool GetUserResumeListStatus(Innovator inn, string meeting_id, string strUserId)
        {
            string sql = @"
SELECT
    t2.id
FROM 
    In_Cla_Meeting_Resumelist t1 WITH(NOLOCK) 
INNER JOIN 
    IN_RESUME t2 WITH(NOLOCK) 
    ON t2.id = t1.related_id    
WHERE 
    t1.source_id = '{#meeting_id}' 
    AND t2.in_user_id = '{#in_user_id}';    
";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_user_id}", strUserId);

            Item item = inn.applySQL(sql);

            if (item.getResult() != "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static void AppendSheetCTA(Spire.Xls.Workbook book, Spire.Xls.Worksheet sheetTemplate, Item itmMeeting, Item itmMeetingPay)
        {
            //當前時間
            DateTime currentTime = System.DateTime.Now;

            //(發票/統編)關聯數量
            int invoice_count = itmMeetingPay.getRelationships("In_Meeting_invoice").getItemCount();

            string in_title = itmMeeting.getProperty("in_title", "");
            string in_meeting_type = itmMeeting.getProperty("in_meeting_type", "");
            string in_address = itmMeeting.getProperty("in_address", "");
            string in_bank_name = itmMeeting.getProperty("in_bank_name", "");
            string in_tel = itmMeeting.getProperty("in_tel", "");

            string in_fax = "缺傳真號碼";//itmMeeting.getProperty("in_fax", "");
            string in_case_number = "缺立案證號";//itmMeeting.getProperty("in_case_number", "");
            string in_company_number = "缺統一編號";//itmMeeting.getProperty("in_case_number", "");
            string received = "";
            string account = "";
            string chairman = "";
            //承辦單位(如有值則提換原經收人)
            string in_undertake = itmMeetingPay.getProperty("in_undertake", "");
            string in_undertake_value = "";
            try
            {
                //人員名冊
                string in_receipt = itmMeeting.getProperty("in_receipt", "");
                string[] staffArr = in_receipt.Split(',');
                foreach (var staff in staffArr)
                {
                    switch (staff.Split(':')[0])
                    {
                        case "經收人":
                            received = staff;
                            break;
                        case "會計":
                            account = staff;
                            break;
                        case "理事長":
                            chairman = staff;
                            break;
                        case "裁判組":
                            if (in_undertake == "裁判組")
                            {
                                in_undertake_value = "經收人:" + staff.Split(':')[1];
                            }
                            break;
                        case "訓練組":
                            if (in_undertake == "訓練組")
                            {
                                in_undertake_value = "經收人:" + staff.Split(':')[1];
                            }
                            break;
                        case "行政組":
                            if (in_undertake == "行政組")
                            {
                                in_undertake_value = "經收人:" + staff.Split(':')[1];
                            }
                            break;
                    }
                }
                if (in_undertake_value != "")
                {
                    received = in_undertake_value;
                }

            }
            catch
            {

            }

            string account_for = "";

            switch (in_meeting_type)
            {
                case "game": //比賽
                    account_for = in_title + "報名費";
                    break;

                case "degree": //晉段
                    account_for = in_title + "晉段費";
                    break;

                case "seminar": //講習
                    account_for = in_title + "報名費";
                    break;

                case "payment": //會員年費
                    if (in_title.Contains("年費"))
                    {
                        account_for = in_title;
                    }
                    else if (in_title.Contains("會費"))
                    {
                        account_for = in_title;
                    }
                    else
                    {
                        account_for = in_title + "會費";
                    }
                    break;
                case "verify": //各類申請
                    account_for = itmMeetingPay.getProperty("in_l2_title", "") + "申請費用";
                    break;
                default:
                    break;
            }

            int amount_real = Convert.ToInt32(itmMeetingPay.getProperty("in_pay_amount_real", "0"));
            if (amount_real == 0)
            {
                amount_real = Convert.ToInt32(itmMeetingPay.getProperty("in_pay_amount_exp", "0"));
            }
            int refund_amount = Convert.ToInt32(itmMeetingPay.getProperty("in_refund_amount", "0"));
            int fee_amount = Convert.ToInt32(itmMeetingPay.getProperty("in_fee_amount", "0"));

            int in_amount = amount_real - refund_amount - fee_amount;

            if (in_amount == 0)
            {
                account_for = in_title + "退款手續費";
            }

            if (in_amount == fee_amount)
            {
                throw new Exception("所選項目已完全退款，無法下載收據");
            }
            //count不是0就抓 In_Meeting_invoice的(發票抬頭,統一編號,費用) 如果是0則抓In_Meeting_pay的(發票抬頭,統一編號,費用)
            if (invoice_count != 0)
            {
                int c = 1;
                for (int i = 0; i < invoice_count; i++)
                {
                    string num = "";
                    if (invoice_count > 1)
                    {
                        num = "-" + c.ToString();
                    }
                    Item In_Meeting_invoice = itmMeetingPay.getRelationships("In_Meeting_invoice").getItemByIndex(i);

                    //付款方式
                    string pay_mode = "超商繳款";

                    //繳費單號
                    string item_number = itmMeetingPay.getProperty("item_number", "");
                    //所屬群組
                    string in_group = itmMeetingPay.getProperty("in_group", "");
                    //協助報名者
                    string in_creator = itmMeetingPay.getProperty("in_creator", "");
                    //發票抬頭
                    string invoice_up = In_Meeting_invoice.getProperty("invoice_up", "");
                    //開立之統一編號
                    string uniform_numbers = In_Meeting_invoice.getProperty("uniform_numbers", "");
                    //實際收款金額
                    string in_pay_amount_real = In_Meeting_invoice.getProperty("invoice_amount", "").TrimStart('0').Replace(".00", "");


                    //收據編號
                    string rec_number = itmMeetingPay.getProperty("rec_number", "");

                    string as_title = in_creator;
                    if (invoice_up != null && invoice_up != "")
                    {
                        as_title = invoice_up;
                    }

                    //民國年月日 (8碼): 開立日期: 108 年 12 月  27 日 
                    string y = (currentTime.Year - 1911).ToString();
                    string m = currentTime.Month.ToString();
                    string d = currentTime.Day.ToString();
                    string chinese_day = y + " 年 " + m + " 月 " + d + " 日";

                    //正體中文金額
                    int int_expense = Convert.ToInt32(in_pay_amount_real);
                    //正體中文金額
                    string chinese_expense = NumberToCHS(in_pay_amount_real).Replace("元整", "");

                    Spire.Xls.Worksheet sheet = book.CreateEmptySheet();
                    sheet.CopyFrom(sheetTemplate);
                    //2021-12-06 分多家單位印時sheet.name會重複導致錯誤，故新增變數部分(sheet.name最大長度為31 characters)
                    //sheet.Name = in_creator + "_" + In_Meeting_invoice.getProperty("invoice_up","") + "_" + In_Meeting_invoice.getProperty("invoice_up","uniform_numbers") + "_" + item_number;
                    sheet.Name = "P" + (i + 1).ToString() + "_" + in_creator + "_" + In_Meeting_invoice.getProperty("invoice_up", "") + "_" + In_Meeting_invoice.getProperty("invoice_up", "uniform_numbers") + "_" + item_number;

                    //（第一聯）
                    sheet.Range["J1"].Text = y + "-" + item_number + num; //No
                    sheet.Range["I4"].Text = chinese_day;   //日期
                    sheet.Range["E7"].Text = as_title + "  (統一編號：" + uniform_numbers + ")";   //茲收到
                    sheet.Range["E9"].Text = chinese_expense + " NT$" + int_expense.ToString("#,##0");   //共計新台幣
                    sheet.Range["E11"].Text = account_for;   //付款性質
                    sheet.Range["F13"].Text = received;   //經收人
                    sheet.Range["H13"].Text = account;   //會計
                    sheet.Range["I13"].Text = chairman;   //理事長
                                                          // sheet.Range["B14"].Text = "統一編號：" + uniform_numbers;   //統一編號

                    //（第二聯）
                    sheet.Range["J16"].Text = y + "-" + item_number + num; //No
                    sheet.Range["I19"].Text = chinese_day;   //日期
                    sheet.Range["E22"].Text = as_title + "  (統一編號：" + uniform_numbers + ")";   //茲收到
                    sheet.Range["E24"].Text = chinese_expense + " NT$" + int_expense.ToString("#,##0");   //共計新台幣
                    sheet.Range["E26"].Text = account_for;   //付款性質
                    sheet.Range["F28"].Text = received;   //經收人
                    sheet.Range["H28"].Text = account;   //會計
                    sheet.Range["I28"].Text = chairman;   //理事長
                                                          // sheet.Range["B29"].Text = "統一編號：" + uniform_numbers;  //統一編號

                    //（第三聯 收執聯）
                    sheet.Range["J31"].Text = y + "-" + item_number + num; //No
                    sheet.Range["I34"].Text = chinese_day;   //日期
                    sheet.Range["E37"].Text = as_title + "  (統一編號：" + uniform_numbers + ")";   //茲收到
                    sheet.Range["E39"].Text = chinese_expense + " NT$" + int_expense.ToString("#,##0");   //共計新台幣
                    sheet.Range["E41"].Text = account_for;   //付款性質
                    sheet.Range["F43"].Text = received;   //經收人
                    sheet.Range["H43"].Text = account;   //會計
                    sheet.Range["I43"].Text = chairman;   //理事長
                                                          // sheet.Range["B29"].Text = "統一編號：" + uniform_numbers;  //統一編號

                    // for(int i = 0; i < array.Length; i++)
                    // {
                    //     string cell = array[i];
                    //     sheet.Range[cell].Style.Font.FontName = fontName;
                    // }


                    sheet.PageSetup.FitToPagesWide = 1;
                    sheet.PageSetup.FitToPagesTall = 1;
                    sheet.PageSetup.TopMargin = 0.3;
                    sheet.PageSetup.LeftMargin = 0.5;
                    sheet.PageSetup.RightMargin = 0.5;
                    sheet.PageSetup.BottomMargin = 0.5;

                    //適合頁面
                    book.ConverterSetting.SheetFitToPage = true;
                    c++;
                }
            }
            else
            {
                //付款方式
                string pay_mode = "超商繳款";

                //繳費單號
                string item_number = itmMeetingPay.getProperty("item_number", "");
                //所屬群組
                string in_group = itmMeetingPay.getProperty("in_group", "");
                //協助報名者
                string in_creator = itmMeetingPay.getProperty("in_creator", "");
                //發票抬頭
                string invoice_up = itmMeetingPay.getProperty("invoice_up", "");
                //統一編號
                string uniform_numbers = itmMeetingPay.getProperty("uniform_numbers", "");
                //實際收款金額
                string in_pay_amount_real = itmMeetingPay.getProperty("in_pay_amount_real", "").TrimStart('0').Replace(".00", "");

                if (in_pay_amount_real == "")
                {
                    in_pay_amount_real = itmMeetingPay.getProperty("in_pay_amount_exp", "").TrimStart('0').Replace(".00", "");
                }

                if (uniform_numbers == "")
                {
                    uniform_numbers = itmMeetingPay.getProperty("in_creator_sno", "");
                }

                //收據編號
                string rec_number = itmMeetingPay.getProperty("rec_number", "");

                string as_title = in_creator;
                if (invoice_up != null && invoice_up != "")
                {
                    as_title = invoice_up;
                }

                //民國年月日 (8碼): 開立日期: 108 年 12 月  27 日 
                string y = (currentTime.Year - 1911).ToString();
                string m = currentTime.Month.ToString();
                string d = currentTime.Day.ToString();
                string chinese_day = y + " 年 " + m + " 月 " + d + " 日";

                //正體中文金額
                int int_expense = Convert.ToInt32(in_pay_amount_real);
                //正體中文金額
                string chinese_expense = NumberToCHS(in_pay_amount_real);

                Spire.Xls.Worksheet sheet = book.CreateEmptySheet();
                sheet.CopyFrom(sheetTemplate);
                sheet.Name = in_creator + "_" + item_number;

                //（第一聯）
                sheet.Range["J1"].Text = y + "-" + item_number; //No
                sheet.Range["I4"].Text = chinese_day;   //日期
                sheet.Range["E7"].Text = as_title + "  (統一編號：" + uniform_numbers + ")";   //茲收到
                sheet.Range["E9"].Text = chinese_expense + " NT$" + int_expense.ToString("#,##0");   //共計新台幣
                sheet.Range["E11"].Text = account_for;   //付款性質
                sheet.Range["F13"].Text = received;   //經收人
                sheet.Range["H13"].Text = account;   //會計
                sheet.Range["I13"].Text = chairman;   //理事長
                                                      // sheet.Range["B14"].Text = "統一編號：" + uniform_numbers;   //統一編號

                //（第二聯）
                sheet.Range["J16"].Text = y + "-" + item_number; //No
                sheet.Range["I19"].Text = chinese_day;   //日期
                sheet.Range["E22"].Text = as_title + "  (統一編號：" + uniform_numbers + ")";   //茲收到
                sheet.Range["E24"].Text = chinese_expense + " NT$" + int_expense.ToString("#,##0");   //共計新台幣
                sheet.Range["E26"].Text = account_for;   //付款性質
                sheet.Range["F28"].Text = received;   //經收人
                sheet.Range["H28"].Text = account;   //會計
                sheet.Range["I28"].Text = chairman;   //理事長
                                                      // sheet.Range["B29"].Text = "統一編號：" + uniform_numbers;  //統一編號

                // //（第三聯 收執聯）
                sheet.Range["J31"].Text = y + "-" + item_number; //No
                sheet.Range["I34"].Text = chinese_day;   //日期
                sheet.Range["E37"].Text = as_title + "  (統一編號：" + uniform_numbers + ")";   //茲收到
                sheet.Range["E39"].Text = chinese_expense + " NT$" + int_expense.ToString("#,##0");   //共計新台幣
                sheet.Range["E41"].Text = account_for;   //付款性質
                sheet.Range["F43"].Text = received;   //經收人
                sheet.Range["H43"].Text = account;   //會計
                sheet.Range["I43"].Text = chairman;   //理事長
                                                      // sheet.Range["B29"].Text = "統一編號：" + uniform_numbers;  //統一編號

                // for(int i = 0; i < array.Length; i++)
                // {
                //     string cell = array[i];
                //     sheet.Range[cell].Style.Font.FontName = fontName;
                // }


                sheet.PageSetup.FitToPagesWide = 1;
                sheet.PageSetup.FitToPagesTall = 1;
                sheet.PageSetup.TopMargin = 0.3;
                sheet.PageSetup.LeftMargin = 0.5;
                sheet.PageSetup.RightMargin = 0.5;
                sheet.PageSetup.BottomMargin = 0.5;

                //適合頁面
                book.ConverterSetting.SheetFitToPage = true;
            }
        }

        private static void AppendSheetRowing(Spire.Xls.Workbook book, Spire.Xls.Worksheet sheetTemplate, Item itmMeeting, Item itmMeetingPay)
        {
            //(發票/統編)關聯數量
            int invoice_count = itmMeetingPay.getRelationships("In_Meeting_invoice").getItemCount();

            //count不是0就抓 In_Meeting_invoice的(發票抬頭,統一編號,費用) 如果是0則抓In_Meeting_pay的(發票抬頭,統一編號,費用)
            if (invoice_count != 0)
            {
                for (int c = 0; c < invoice_count; c++)
                {
                    Item In_Meeting_invoice = itmMeetingPay.getRelationships("In_Meeting_invoice").getItemByIndex(c);

                    //賽事 (缺會址、傳真號碼、立案證號、統一編號)
                    string in_title = itmMeeting.getProperty("in_title", "");
                    string in_address = itmMeeting.getProperty("in_address", "");
                    string in_bank_name = itmMeeting.getProperty("in_bank_name", "");
                    string in_tel = itmMeeting.getProperty("in_tel", "");
                    string in_fax = "缺傳真號碼";//itmMeeting.getProperty("in_fax", "");
                    string in_case_number = "缺立案證號";//itmMeeting.getProperty("in_case_number", "");
                    string in_company_number = "缺統一編號";//itmMeeting.getProperty("in_case_number", "");
                                                       //當前時間
                    DateTime currentTime = System.DateTime.Now;
                    //所屬單位
                    string in_current_org = itmMeetingPay.getProperty("in_current_org", "");
                    //繳費單號
                    string item_number = itmMeetingPay.getProperty("item_number", "");
                    //實際收款金額
                    string in_pay_amount_real = In_Meeting_invoice.getProperty("invoice_amount", "").TrimStart('0').Replace(".00", "");

                    //民國年月日 (8碼)
                    string chinese_day = (currentTime.Year - 1911).ToString() + "年" + currentTime.Month + "月" + currentTime.Day + "日";
                    //正體中文金額
                    string chinese_expense = NumberToCHS(in_pay_amount_real);


                    Spire.Xls.Worksheet sheet = book.CreateEmptySheet();
                    sheet.CopyFrom(sheetTemplate);
                    sheet.Name = in_current_org + "_" + In_Meeting_invoice.getProperty("invoice_up", "") + "_" + In_Meeting_invoice.getProperty("invoice_up", "uniform_numbers");

                    string fontName = "標楷體";
                    string[] array = new string[] {
                "B2",  "I3",  "C4",  "G4",  "B6",  "I6",  "B7",  "C8",  "C10", "C11", "H11", "J11",
                "B18", "I19", "C20", "G20", "B22", "I22", "B23", "C24", "C26", "C27", "H27", "J27",
            };

                    sheet.Range["B2"].Text = in_bank_name;
                    sheet.Range["I3"].Text = item_number;
                    sheet.Range["C4"].Text = in_current_org;
                    sheet.Range["G4"].Text = chinese_day;
                    sheet.Range["B6"].Text = in_title;
                    sheet.Range["I6"].Text = "NT$" + in_pay_amount_real;
                    sheet.Range["B7"].Text = "合計新台幣" + chinese_expense;
                    sheet.Range["C8"].Text = "一、立案證書：台內社字第" + in_case_number + "號，符合所得稅法第11條第四項規定之團體。";
                    sheet.Range["C10"].Text = "三、統一編號：" + in_company_number;
                    sheet.Range["C11"].Text = "四、會址：" + in_address;
                    sheet.Range["H11"].Text = in_tel;
                    sheet.Range["J11"].Text = in_fax;

                    sheet.Range["B18"].Text = in_bank_name;
                    sheet.Range["I19"].Text = item_number;
                    sheet.Range["C20"].Text = in_current_org;
                    sheet.Range["G20"].Text = chinese_day;
                    sheet.Range["B22"].Text = in_title;
                    sheet.Range["I22"].Text = "NT$" + in_pay_amount_real;
                    sheet.Range["B23"].Text = "合計新台幣" + chinese_expense;
                    sheet.Range["C24"].Text = "一、立案證書：台內社字第" + in_case_number + "號，符合所得稅法第11條第四項規定之團體。";
                    sheet.Range["C26"].Text = "三、統一編號：" + in_company_number;
                    sheet.Range["C27"].Text = "四、會址：" + in_address;
                    sheet.Range["H27"].Text = in_tel;
                    sheet.Range["J27"].Text = in_fax;

                    for (int i = 0; i < array.Length; i++)
                    {
                        string cell = array[i];
                        sheet.Range[cell].Style.Font.FontName = fontName;
                    }
                }

            }
            else
            {
                //賽事 (缺會址、傳真號碼、立案證號、統一編號)
                string in_title = itmMeeting.getProperty("in_title", "");
                string in_address = itmMeeting.getProperty("in_address", "");
                string in_bank_name = itmMeeting.getProperty("in_bank_name", "");
                string in_tel = itmMeeting.getProperty("in_tel", "");
                string in_fax = "缺傳真號碼";//itmMeeting.getProperty("in_fax", "");
                string in_case_number = "缺立案證號";//itmMeeting.getProperty("in_case_number", "");
                string in_company_number = "缺統一編號";//itmMeeting.getProperty("in_case_number", "");

                //當前時間
                DateTime currentTime = System.DateTime.Now;
                //所屬單位
                string in_current_org = itmMeetingPay.getProperty("in_current_org", "");
                //繳費單號
                string item_number = itmMeetingPay.getProperty("item_number", "");
                //實際收款金額
                string in_pay_amount_real = itmMeetingPay.getProperty("in_pay_amount_real", "").TrimStart('0').Replace(".00", "");

                //民國年月日 (8碼)
                string chinese_day = (currentTime.Year - 1911).ToString() + "年" + currentTime.Month + "月" + currentTime.Day + "日";
                //正體中文金額
                string chinese_expense = NumberToCHS(in_pay_amount_real);


                Spire.Xls.Worksheet sheet = book.CreateEmptySheet();
                sheet.CopyFrom(sheetTemplate);
                sheet.Name = in_current_org;

                string fontName = "標楷體";
                string[] array = new string[] {
            "B2",  "I3",  "C4",  "G4",  "B6",  "I6",  "B7",  "C8",  "C10", "C11", "H11", "J11",
            "B18", "I19", "C20", "G20", "B22", "I22", "B23", "C24", "C26", "C27", "H27", "J27",
        };

                sheet.Range["B2"].Text = in_bank_name;
                sheet.Range["I3"].Text = item_number;
                sheet.Range["C4"].Text = in_current_org;
                sheet.Range["G4"].Text = chinese_day;
                sheet.Range["B6"].Text = in_title;
                sheet.Range["I6"].Text = "NT$" + in_pay_amount_real;
                sheet.Range["B7"].Text = "合計新台幣" + chinese_expense;
                sheet.Range["C8"].Text = "一、立案證書：台內社字第" + in_case_number + "號，符合所得稅法第11條第四項規定之團體。";
                sheet.Range["C10"].Text = "三、統一編號：" + in_company_number;
                sheet.Range["C11"].Text = "四、會址：" + in_address;
                sheet.Range["H11"].Text = in_tel;
                sheet.Range["J11"].Text = in_fax;

                sheet.Range["B18"].Text = in_bank_name;
                sheet.Range["I19"].Text = item_number;
                sheet.Range["C20"].Text = in_current_org;
                sheet.Range["G20"].Text = chinese_day;
                sheet.Range["B22"].Text = in_title;
                sheet.Range["I22"].Text = "NT$" + in_pay_amount_real;
                sheet.Range["B23"].Text = "合計新台幣" + chinese_expense;
                sheet.Range["C24"].Text = "一、立案證書：台內社字第" + in_case_number + "號，符合所得稅法第11條第四項規定之團體。";
                sheet.Range["C26"].Text = "三、統一編號：" + in_company_number;
                sheet.Range["C27"].Text = "四、會址：" + in_address;
                sheet.Range["H27"].Text = in_tel;
                sheet.Range["J27"].Text = in_fax;

                for (int i = 0; i < array.Length; i++)
                {
                    string cell = array[i];
                    sheet.Range[cell].Style.Font.FontName = fontName;
                }
            }
        }

        //數字轉中文大寫
        public static string NumberToCHS(string Val)
        {
            string result = "";

            string CN_ZERO = "零";
            string CN_ONE = "壹";
            string CN_TWO = "貳";
            string CN_THREE = "參";
            string CN_FOUR = "肆";
            string CN_FIVE = "伍";
            string CN_SIX = "陸";
            string CN_SEVEN = "柒";
            string CN_EIGHT = "捌";
            string CN_NINE = "玖";
            string CN_TEN = "拾";
            string CN_HUNDRED = "佰";
            string CN_THOUSAND = "仟";
            string CN_TEN_THOUSAND = "萬";
            string CN_HUNDRED_MILLION = "億";
            //string CN_SYMBOL = "$";
            string CN_SYMBOL = "";
            string CN_DOLLAR = "元";
            string CN_TEN_CENT = "角";
            string CN_CENT = "分";
            string CN_INTEGER = "整";
            string[] aDigits = new string[] { CN_ZERO, CN_ONE, CN_TWO, CN_THREE, CN_FOUR, CN_FIVE, CN_SIX, CN_SEVEN, CN_EIGHT, CN_NINE };
            string[] aRadices = new string[] { "", CN_TEN, CN_HUNDRED, CN_THOUSAND };
            string[] aBigRadices = new string[] { "", CN_TEN_THOUSAND, CN_HUNDRED_MILLION };
            string[] aDecimals = new string[] { CN_TEN_CENT, CN_CENT };

            string[] aParts = null;
            string sInt = "";
            string sDcm = "";

            int iInt = 0;
            int iDcm = 0; // Represent decimal part of digit number.  

            int zeroCount = int.MinValue;
            int i = int.MinValue;
            int p = int.MinValue;
            int d = int.MinValue;

            int quotient = 0;
            int modulus = 0;

            aParts = Val.Split('.');
            if (aParts.Length > 1)
            {
                sInt = aParts[0];
                sDcm = aParts[1];
                if (sDcm.Length > 2) sDcm = sDcm.Substring(0, 2);
            }
            else
            {
                sInt = aParts[0];
                sDcm = "";
            }

            iInt = Convert.ToInt32(sInt);

            if (iInt > 0)
            {
                zeroCount = 0;
                for (i = 0; i < sInt.Length; i++)
                {
                    p = sInt.Length - i - 1;
                    d = Convert.ToInt32(sInt.Substring(i, 1));

                    quotient = p / 4;
                    modulus = p % 4;
                    if (d == 0)
                    {
                        zeroCount++;
                    }
                    else
                    {
                        if (zeroCount > 0)
                        {
                            result += aDigits[0];
                        }
                        zeroCount = 0;
                        result += aDigits[(int)(d)] + aRadices[modulus];
                    }
                    if (modulus == 0 && zeroCount < 4)
                    {
                        result += aBigRadices[quotient];
                    }
                }
                result += CN_DOLLAR;
            }
            if (sDcm.Length > 0)
            {
                for (i = 0; i < sDcm.Length; i++)
                {
                    d = Convert.ToInt32(sDcm.Substring(i, 1));
                    if (d != 0)
                    {
                        result += aDigits[(int)(d)] + aDecimals[i];
                    }
                }
            }
            if (result.Length == 0)
            {
                result = CN_ZERO + CN_DOLLAR;
            }
            if (sDcm.Length == 0)
            {
                result += CN_INTEGER;
            }

            result = CN_SYMBOL + result;

            return result;
        }
    }
}