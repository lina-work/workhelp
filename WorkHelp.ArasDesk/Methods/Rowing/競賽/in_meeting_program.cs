﻿using System;
using System.Collections.Generic;
using System.Linq;
using Aras.IOM;
using WorkHelp.ArasDesk.Samples.SqlClient;

namespace WorkHelp.ArasDesk.Methods.Rowing.Fight
{
    public class in_meeting_program : Item
    {
        public in_meeting_program(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 設定賽制
                日期: 
                    2024-01-22: 加入 0 水道、計時賽可以兩場 (lina)
                    2023-12-11: 創建划船版本 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_program";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                in_l1 = itmR.getProperty("in_l1", ""),
                in_l2 = itmR.getProperty("in_l2", ""),
                in_l3 = itmR.getProperty("in_l3", ""),
                in_fight_day = itmR.getProperty("in_fight_day", ""),
                scene = itmR.getProperty("scene", ""),
            };

            cfg.meeting_id = "932D9E22CD2147CEA5AD31C69461C031";
            cfg.program_id = "10AF78289D7E45E8AFBC9507C9C9469E";
            cfg.scene = "fix_event";

            switch (cfg.scene)
            {
                case "fix_event":
                    FixEvent(cfg, itmR);
                    break;

                case "remove_time_schedule":
                    RemoveTimeSchedule(cfg, itmR);
                    break;

                case "add_time_schedule":
                    AddTimeSchedule(cfg, itmR);
                    break;

                case "close_event":
                    CloseEvent(cfg, itmR);
                    break;

                case "remove_foot_player":
                    RemoveFootPlayer(cfg, itmR);
                    break;

                case "edit_team_score":
                    EditTeamScore(cfg, itmR);
                    break;

                case "import_schedule":
                    ImportSchedules(cfg, itmR);
                    break;

                case "save_time_trial_teams":
                    SaveTimeTrialTeams(cfg, itmR);
                    break;

                case "remove_time_trial_teams":
                    RemoveTimeTrialTeams(cfg, itmR);
                    break;

                case "initial_variable":
                    InitialVariables(cfg, itmR);
                    break;

                case "clear_times_events":
                    ClearFightDayTimeEvents(cfg, itmR);
                    break;

                case "save_times_events":
                    SaveFightDayTimeEvents(cfg, itmR);
                    break;

                case "clear_days_events":
                    ClearFightDayEvents(cfg, itmR);
                    break;

                case "save_days_events":
                    SaveFightDayEvents(cfg, itmR);
                    break;

                case "clear_program_groups":
                    ClearProgramGroups(cfg, itmR);
                    break;

                case "save_program_groups":
                    SaveProgramGroups(cfg, itmR);
                    break;

                case "edit_foot_player":
                    EditFootPlayer(cfg, itmR);
                    break;

                case "assign_draw_no":
                    AssignDrawNo(cfg, itmR);
                    break;

                case "draw_program":
                    RunDraw(cfg, itmR);
                    break;

                case "edit_prop":
                    EditProperty(cfg, itmR);
                    break;

                case "time_event_edit":
                    EditTimeEvent(cfg, itmR);
                    break;

                case "time_edit":
                    EditTime(cfg, itmR);
                    break;

                case "edit":
                    EditProgram(cfg, itmR);
                    break;

                case "rebuild_program":
                    RebuildProgram(cfg, itmR);
                    break;

                case "create":
                    //組別建檔
                    CreatePrograms(cfg, itmR);
                    //隊別建檔、場次
                    CreateProgramTeams(cfg, itmR);
                    break;

                case "clear":
                    ClearMeeting(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void FixEvent(TConfig cfg, Item itmReturn)
        {
            cfg.variableMap = VariableMap(cfg);
            cfg.waterWayCount = GeMapIntValue(cfg.variableMap, "fight_site_count", 4);
            cfg.timeTrialTeamCount = GeMapIntValue(cfg.variableMap, "fight_robin_player", 12);
            cfg.hasTwoTimeTrialEvents = HasTwoTimeTrialEvents(cfg.variableMap, "two_time_trial");

            var itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");
            var program = MapProgramModel(cfg, itmProgram, cfg.program_id);

            program.in_tree_key = "schedule-12";
            program.in_schedule_mode = "H1,H2,H3,H4,R1,R2,R3,R4,SB,SA,FB,FA";
            program.in_schedule_value = "H1,H2,H3,H4,R1,R2,R3,R4,SB,SA,FB,FA";
            program.in_event_count = "12";

            var sql = "UPDATE IN_MEETING_PROGRAM SET"
                + "  in_tree_key = '" + program.in_tree_key + "'"
                + ", in_schedule_mode = '" + program.in_schedule_mode + "'"
                + ", in_schedule_value = '" + program.in_schedule_value + "'"
                + ", in_schedule_show = '" + program.in_schedule_value + "'"
                + ", in_event_count = '" + program.in_event_count + "'"
                + " WHERE id = '" + program.id + "'";

            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PEVENT SET in_tree_no = '9' WHERE source_id = '" + cfg.program_id + "' AND in_fight_id = 'SB'";
            cfg.inn.applySQL(sql);
            sql = "UPDATE IN_MEETING_PEVENT SET in_tree_no = '10' WHERE source_id = '" + cfg.program_id + "' AND in_fight_id = 'SA'";
            cfg.inn.applySQL(sql);
            sql = "UPDATE IN_MEETING_PEVENT SET in_tree_no = '11' WHERE source_id = '" + cfg.program_id + "' AND in_fight_id = 'FB'";
            cfg.inn.applySQL(sql);
            sql = "UPDATE IN_MEETING_PEVENT SET in_tree_no = '12' WHERE source_id = '" + cfg.program_id + "' AND in_fight_id = 'FA'";
            cfg.inn.applySQL(sql);

            var in_fight_id = "R3";
            var in_tree_no = 7;
            var in_tree_name = in_fight_id;
            var in_tree_sort = GetEventSort(in_fight_id);
            var in_show_serial = 37;
            var in_show_no = "37-1";

            var in_fight_day = "2024-01-27";
            var in_fight_time = "15:40";
            var in_sect = "國男組";
            var in_item = "YJ/M1X";
            var in_teams = "3";
            var in_note = "1000M";

            var itmEvent = cfg.inn.newItem("In_Meeting_PEvent", "add");
            itmEvent.setProperty("in_meeting", cfg.meeting_id);
            itmEvent.setProperty("source_id", cfg.program_id);
            itmEvent.setProperty("in_fight_id", in_fight_id);
            itmEvent.setProperty("in_tree_name", in_tree_name);
            itmEvent.setProperty("in_tree_sort", in_tree_sort.ToString());
            itmEvent.setProperty("in_tree_no", in_tree_no.ToString());
            itmEvent.setProperty("in_show_serial", in_show_serial.ToString());
            itmEvent.setProperty("in_show_no", in_show_no);

            itmEvent.setProperty("in_fight_day", in_fight_day);
            itmEvent.setProperty("in_fight_time", in_fight_time);
            itmEvent.setProperty("in_sect", in_sect);
            itmEvent.setProperty("in_item", in_item);
            itmEvent.setProperty("in_teams", in_teams);
            itmEvent.setProperty("in_note", in_note);

            itmEvent = itmEvent.apply();

            var event_id = itmEvent.getID();

            for (var j = 0; j <= cfg.waterWayCount; j++)
            {
                var in_water_num = j.ToString();

                //建立明細
                NewDetail1(cfg
                    , program.id
                    , event_id
                    , in_fight_id
                    , in_water_num
                    , 9999
                    , new Dictionary<string, int>());
            }
        }

        private void RemoveTimeSchedule(TConfig cfg, Item itmReturn)
        {
            var time_id = itmReturn.getProperty("time_id", "");
            var sql = "DELETE FROM IN_MEETING_PTIME WHERE id = '" + time_id + "'";
            cfg.inn.applySQL(sql);
        }

        private void AddTimeSchedule(TConfig cfg, Item itmReturn)
        {
            var event_id = itmReturn.getProperty("event_id", "");
            var time_id = itmReturn.getProperty("time_id", "");
            var in_type = itmReturn.getProperty("in_type", "");
            var in_title = itmReturn.getProperty("in_title", "");//時間
            var in_label = itmReturn.getProperty("in_label", "");
            var in_fight_day = itmReturn.getProperty("in_fight_day", "");
            var in_time_num = itmReturn.getProperty("in_time_num", "");
            var in_up = itmReturn.getProperty("in_up", "");

            // if (event_id == "") throw new Exception("場次編號 為必填");
            // if (in_type == "") throw new Exception("類型 為必填");
            //if (in_label) throw new Exception("標籤 為必填");

            if (event_id != "")
            {
                var sql1 = "SELECT id, in_fight_day, in_time_num FROM IN_MEETING_PTIME WITH(NOLOCK) WHERE in_event = '" + event_id + "' AND in_time_num > 0";
                var itmEvent = cfg.inn.applySQL(sql1);
                if (itmEvent.isError() || itmEvent.getResult() == "") throw new Exception("查無場次");
                in_fight_day = itmEvent.getProperty("in_fight_day", "");
                in_time_num = itmEvent.getProperty("in_time_num", "");
            }
            else if (time_id != "")
            {
                var sql1 = "SELECT id, in_fight_day, in_time_num FROM IN_MEETING_PTIME WITH(NOLOCK) WHERE id = '" + time_id + "' AND in_time_num > 0";
                var itmTime = cfg.inn.applySQL(sql1);
                if (itmTime.isError() || itmTime.getResult() == "") throw new Exception("查無時序");
                in_fight_day = itmTime.getProperty("in_fight_day", "");
                in_time_num = itmTime.getProperty("in_time_num", "");
            }

            var cond = " AND in_time_num > " + in_time_num;
            var new_time_num = GetInt(in_time_num) + 1;
            if (in_up == "1")
            {
                cond = " AND in_time_num >= " + in_time_num;
                new_time_num = GetInt(in_time_num);
            }

            var sql2 = "UPDATE IN_MEETING_PTIME SET"
                + " in_time_num = in_time_num + 1"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_fight_day = '" + in_fight_day + "'"
                + cond;

            cfg.inn.applySQL(sql2);

            var itmNew = cfg.inn.newItem("IN_MEETING_PTIME", "add");
            itmNew.setProperty("in_meeting", cfg.meeting_id);
            itmNew.setProperty("in_fight_day", in_fight_day);
            itmNew.setProperty("in_time_num", new_time_num.ToString());
            itmNew.setProperty("in_type", in_type);
            itmNew.setProperty("in_title", in_title);
            itmNew.setProperty("in_label", in_label);
            itmNew = itmNew.apply();
        }

        private void CloseEvent(TConfig cfg, Item itmReturn)
        {
            var event_id = itmReturn.getProperty("event_id", "");
            var is_close = itmReturn.getProperty("is_close", "");

            var sql = "";
            if (is_close == "1")
            {
                //完賽
                sql = "UPDATE IN_MEETING_PEVENT SET in_win_time = getutcdate() WHERE id = '" + event_id + "'";
            }
            else
            {
                //未完賽
                sql = "UPDATE IN_MEETING_PEVENT SET in_win_time = NULL WHERE id = '" + event_id + "'";
            }

            cfg.inn.applySQL(sql);
        }

        private void RemoveFootPlayer(TConfig cfg, Item itmReturn)
        {
            var detail_id = itmReturn.getProperty("detail_id", "");
            var sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_sign_no = NULL, in_pteam = NULL WHERE id = '" + detail_id + "'";
            cfg.inn.applySQL(sql);
        }

        private void EditTeamScore(TConfig cfg, Item itmReturn)
        {
            var team_id = itmReturn.getProperty("team_id", "");
            var itmOld = cfg.inn.newItem("IN_MEETING_PTEAM", "merge");
            itmOld.setAttribute("where", "id = '" + team_id + "'");
            CopyItemValue(itmOld, itmReturn, "in_status");
            CopyItemValue(itmOld, itmReturn, "in_time_trial_score");
            CopyItemValue(itmOld, itmReturn, "in_time_trial_rank");
            CopyItemValue(itmOld, itmReturn, "in_time_trial_score2");
            CopyItemValue(itmOld, itmReturn, "in_time_trial_rank2");
            CopyItemValue(itmOld, itmReturn, "in_heat_score");
            CopyItemValue(itmOld, itmReturn, "in_heat_rank");
            CopyItemValue(itmOld, itmReturn, "in_rpc_score");
            CopyItemValue(itmOld, itmReturn, "in_rpc_rank");
            CopyItemValue(itmOld, itmReturn, "in_semifinal_score");
            CopyItemValue(itmOld, itmReturn, "in_semifinal_rank");
            CopyItemValue(itmOld, itmReturn, "in_final_score");
            CopyItemValue(itmOld, itmReturn, "in_final_rank");
            CopyItemValue(itmOld, itmReturn, "in_rank");
            itmOld = itmOld.apply();
        }

        private void ImportSchedules(TConfig cfg, Item itmReturn)
        {
            var data = itmReturn.getProperty("data", "");
            var rows = GetImportRows(cfg, data);
            if (rows == null || rows.Count == 0) throw new Exception("無資料");

            var programMap = GetProgramMap(cfg);
            if (programMap == null || programMap.Count == 0) throw new Exception("無組別");

            MergeProgramMap(cfg, programMap, rows);

            cfg.variableMap = VariableMap(cfg);
            cfg.waterWayCount = GeMapIntValue(cfg.variableMap, "fight_site_count", 4);
            cfg.timeTrialTeamCount = GeMapIntValue(cfg.variableMap, "fight_robin_player", 12);
            cfg.hasTwoTimeTrialEvents = HasTwoTimeTrialEvents(cfg.variableMap, "two_time_trial");

            foreach (var kv in programMap)
            {
                var program = kv.Value;
                if (program.importsA == null) program.importsA = new List<TImport>();
                if (program.importsB == null || program.importsB.Count == 0) continue;

                RebuildEventsFromImport(cfg, program);
            }

            //建立日程檔
            SaveFightDayTimeEvents2(cfg);
        }

        private void SaveFightDayTimeEvents2(TConfig cfg)
        {
            var sql = "SELECT DISTINCT in_fight_day FROM IN_MEETING_PEVENT WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND ISNULL(in_fight_day, '') <> ''"
                + " ORDER BY in_fight_day";

            var itmDays = cfg.inn.applySQL(sql);
            if (itmDays.isError() || itmDays.getResult() == "")
            {
                throw new Exception("查無比賽日期資料");
            }

            var count = itmDays.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var itmDay = itmDays.getItemByIndex(i);
                var in_fight_day = itmDay.getProperty("in_fight_day", "");
                var itmEvents = GetDayEvents(cfg, in_fight_day);
                SaveFightDayTimeEvents2(cfg, in_fight_day, itmEvents);
            }
        }

        private void SaveFightDayTimeEvents2(TConfig cfg, string in_fight_day, Item itmEvents)
        {
            //清空當日賽程
            var sql1 = "DELETE FROM [IN_MEETING_PTIME]"
                + " WHERE [in_meeting] = '" + cfg.meeting_id + "'"
                + " AND [in_fight_day] = '" + in_fight_day + "'";

            cfg.inn.applySQL(sql1);

            var count = itmEvents.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var no = i + 1;
                var itmEvent = itmEvents.getItemByIndex(i);
                var event_id = itmEvent.getProperty("id", "");
                var in_fight_time = itmEvent.getProperty("in_fight_time", "");
                var in_fight_id = itmEvent.getProperty("in_fight_id", "");

                var itmNew = cfg.inn.newItem("IN_MEETING_PTIME", "add");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_fight_day", in_fight_day);
                itmNew.setProperty("in_time_num", no.ToString());

                itmNew.setProperty("in_type", "event");
                itmNew.setProperty("in_event", event_id);
                itmNew.setProperty("in_title", in_fight_time);
                itmNew.setProperty("in_label", in_fight_id);

                itmNew = itmNew.apply();
            }
        }

        private Item GetDayEvents(TConfig cfg, string in_fight_day)
        {
            var sql = "SELECT id, in_fight_time, in_fight_id FROM IN_MEETING_PEVENT WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND ISNULL(in_fight_day, '') = '" + in_fight_day + "'"
                + " AND ISNULL(in_fight_time, '') <> ''"
                + " ORDER BY in_fight_day, in_tree_no";

            return cfg.inn.applySQL(sql);
        }

        private void RebuildEventsFromImport(TConfig cfg, TProgram program)
        {
            program.importsA = program.importsA.OrderBy(x => x.sort).ToList();
            program.importsB = program.importsB.OrderBy(x => x.sort).ToList();

            var codes = program.importsB.Select(x => x.code);
            var in_schedule_value = string.Join(",", codes);
            if (in_schedule_value == "")
            {
                throw new Exception("查無賽程表場次");
            }

            program.in_schedule_mode = in_schedule_value;
            program.in_schedule_value = in_schedule_value;
            program.in_event_count = program.importsB.Count.ToString();
            program.in_tree_key = GetScheduleName(in_schedule_value);
            if (program.in_tree_key == "")
            {
                throw new Exception("查無對映賽程表: " + in_schedule_value);
            }

            var sql = "UPDATE IN_MEETING_PROGRAM SET"
                + "  in_tree_key = '" + program.in_tree_key + "'"
                + ", in_schedule_mode = '" + program.in_schedule_mode + "'"
                + ", in_schedule_value = '" + program.in_schedule_value + "'"
                + ", in_schedule_show = '" + program.in_schedule_value + "'"
                + ", in_event_count = '" + program.in_event_count + "'"
                + " WHERE id = '" + program.id + "'";

            cfg.inn.applySQL(sql);

            //樹圖對照表
            var arr = in_schedule_value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var treeMap = CreateTreeMap(cfg, arr);

            //籤號對照表
            var drawNoMap = ProgramDrawNoMap(cfg
                , program.in_battle_type
                , program.in_tree_key);

            //場次清除
            RemoveOldEvents(cfg, program.id);

            //場次建立 (計時賽)
            for (int i = 0; i < program.importsA.Count; i++)
            {
                var x = program.importsA[i];
                RebuildEvents2(cfg, program, treeMap, drawNoMap, x, i);
            }

            //場次建立
            for (int i = 0; i < program.importsB.Count; i++)
            {
                var x = program.importsB[i];
                RebuildEvents2(cfg, program, treeMap, drawNoMap, x, i);
            }
        }

        //private void RemoveOldEvents2(TConfig cfg, string program_id)
        //{
        //    var sql = "";

        //    var sub = "SELECT * FROM IN_MEETING_PEVENT t2 WITH(NOLOCK)"
        //        + " WHERE t2.source_id = '{#program_id}'"
        //        + " AND t2.id = t1.source_id"
        //        + " AND t2.in_fight_id NOT IN ('TIME', 'TT1', 'TT2')";

        //    sql = "DELETE t1 FROM IN_MEETING_PEVENT_DETAIL t1"
        //        + " WHERE EXISTS(" + sub + ")";

        //    sql = sql.Replace("{#program_id}", program_id);

        //    cfg.inn.applySQL(sql);


        //    sql = "DELETE FROM IN_MEETING_PEVENT WHERE source_id = '{#program_id}' AND in_fight_id NOT IN ('TIME', 'TT1', 'TT2')";

        //    sql = sql.Replace("{#program_id}", program_id);

        //    cfg.inn.applySQL(sql);
        //}

        private void RebuildEvents2(TConfig cfg
            , TProgram program
            , Dictionary<string, TTree> treeMap
            , Dictionary<string, int> drawNoMap
            , TImport x
            , int index)
        {
            var in_fight_id = x.code;
            var in_tree_no = index + 1;
            if (!treeMap.ContainsKey(in_fight_id))
            {
                throw new Exception("查無對應樹圖: " + in_fight_id);
            }

            var tree = treeMap[in_fight_id];

            var itmEvent = cfg.inn.newItem("In_Meeting_PEvent", "add");
            itmEvent.setProperty("in_meeting", cfg.meeting_id);
            itmEvent.setProperty("source_id", program.id);
            itmEvent.setProperty("in_fight_id", in_fight_id);
            itmEvent.setProperty("in_tree_name", tree.name);
            itmEvent.setProperty("in_tree_sort", tree.sort.ToString());
            itmEvent.setProperty("in_tree_no", x.serial.ToString());
            itmEvent.setProperty("in_show_serial", x.serial);
            itmEvent.setProperty("in_show_no", x.serial);

            itmEvent.setProperty("in_fight_day", x.day);
            itmEvent.setProperty("in_fight_time", x.time);
            itmEvent.setProperty("in_sect", x.sect);
            itmEvent.setProperty("in_item", x.item);
            itmEvent.setProperty("in_teams", x.teams);
            itmEvent.setProperty("in_note", x.note);

            itmEvent = itmEvent.apply();

            var event_id = itmEvent.getID();

            if (in_fight_id == "TT1")
            {
                //計時賽第1場
                CreateTimeTrialDetail(cfg, program, event_id, in_fight_id);
                return;
            }

            if (in_fight_id == "TT2")
            {
                //計時賽第2場 (不用建立明細)
                return;
            }

            if (in_fight_id == "TIME")
            {
                //計時賽1場
                CreateTimeTrialDetail(cfg, program, event_id, in_fight_id);
                return;
            }

            for (var j = 0; j <= cfg.waterWayCount; j++)
            {
                var in_water_num = j.ToString();

                //建立明細
                NewDetail1(cfg
                    , program.id
                    , event_id
                    , in_fight_id
                    , in_water_num
                    , 9999
                    , drawNoMap);
            }
        }

        //建立計時賽明細
        private void CreateTimeTrialDetail(TConfig cfg, TProgram program, string event_id, string in_fight_id)
        {
            var itmTeams = GetHeatTeams(cfg, program.id, 0);
            var teamCount = itmTeams.getItemCount();

            for (var j = 0; j < teamCount; j++)
            {
                var itmTeam = itmTeams.getItemByIndex(j);
                var team_id = itmTeam.getProperty("id", "");
                var in_water_num = (j + 1).ToString();

                NewDetail2(cfg, program.id, event_id, in_fight_id, in_water_num, team_id);
            }
        }

        private void MergeProgramMap(TConfig cfg, Dictionary<string, TProgram> programMap, List<TImport> rows)
        {
            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                var in_l2 = MapL2(row.sect);
                var in_short_name = MapShortName(row.item);
                var key = in_short_name;
                if (!programMap.ContainsKey(key))
                {
                    throw new Exception("查無組別: " + key);
                }

                var program = programMap[key];

                row.sort = GetEventSort(row.code);

                switch (row.code)
                {
                    case "T":
                    case "TT":
                    case "TIME":
                    case "TIME 1":
                    case "TIME 2":
                    case "TT1":
                    case "TT2":
                        var existed1 = program.importsA.Find(x => x.code == row.code);
                        if (existed1 != null)
                        {
                            throw new Exception("場次重複: " + key + " " + row.code);
                        }
                        program.importsA.Add(row);
                        break;

                    default:
                        var existed2 = program.importsB.Find(x => x.code == row.code);
                        if (existed2 != null)
                        {
                            throw new Exception("場次重複: " + key + " " + row.code);
                        }
                        program.importsB.Add(row);
                        break;
                }
            }

        }

        private Dictionary<string, TProgram> GetProgramMap(TConfig cfg)
        {
            var result = new Dictionary<string, TProgram>();
            var sql = @"SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' ORDER BY in_sort_order";
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var program = new TProgram
                {
                    id = item.getProperty("id", ""),
                    in_l2 = item.getProperty("in_l2", ""),
                    in_short_name = item.getProperty("in_short_name", ""),
                    in_schedule_value = item.getProperty("in_schedule_value", ""),
                };
                program.key = program.in_short_name;
                program.importsA = new List<TImport>();
                program.importsB = new List<TImport>();
                result.Add(program.key, program);
            }
            return result;
        }

        private string MapShortName(string value)
        {
            return value.Replace("/", "");
        }

        private string MapL2(string value)
        {
            switch (value)
            {
                case "公開男": return "公開男子組";
                case "公開女": return "公開女子組";

                case "高男組": return "高中男子組";
                case "高女組": return "高中女子組";
                case "高中組": return "高中組";

                case "國男組": return "國中男子組";
                case "國女組": return "國中女子組";
                case "國中組": return "國中組";

                default: return "";
            }
        }

        private void RemoveTimeTrialTeams(TConfig cfg, Item itmReturn)
        {
            var sql = "SELECT TOP 1 id FROM IN_MEETING_PEVENT WHERE source_id = '" + cfg.program_id + "' AND in_fight_id = 'TT2'";
            var itmEvent = cfg.inn.applySQL(sql);
            if (itmEvent.isError() || itmEvent.getResult() == "")
            {
                throw new Exception("查無場次資料");
            }

            var event_id = itmEvent.getProperty("id", "");
            sql = "DELETE FROM IN_MEETING_PEVENT_DETAIL WHERE source_id = '" + event_id + "'";
            cfg.inn.applySQL(sql);
        }

        private void SaveTimeTrialTeams(TConfig cfg, Item itmReturn)
        {
            var program_id = itmReturn.getProperty("program_id", "");
            var event_id = itmReturn.getProperty("event_id", "");
            var in_fight_id = itmReturn.getProperty("in_fight_id", "");
            var data = itmReturn.getProperty("data", "");
            var rows = GetEditRows(cfg, data);

            var sql = "DELETE FROM IN_MEETING_PEVENT_DETAIL WHERE source_id = '" + event_id + "'";
            cfg.inn.applySQL(sql);

            for (var j = 0; j < rows.Count; j++)
            {
                var row = rows[j];
                var team_id = row.id;
                var in_water_num = (j + 1).ToString();

                NewDetail2(cfg, program_id, event_id, in_fight_id, in_water_num, team_id);
            }
        }

        private void InitialVariables(TConfig cfg, Item itmReturn)
        {
            var sql = "SELECT id FROM IN_MEETING_VARIABLE WITH(NOLOCK) WHERE source_id = '" + cfg.meeting_id + "' AND in_key = 'fight_site_address'";
            var itmOld = cfg.inn.applySQL(sql);
            if (!itmOld.isError() && itmOld.getResult() != "")
            {
                throw new Exception("參數已初始化");
            }

            var rows = new List<TEditRow>();
            rows.Add(new TEditRow { prop = "fight_site_address", value = "臺南運河水上訓練中心", no = 100 });
            rows.Add(new TEditRow { prop = "fight_day_count", value = "4", no = 200 });
            rows.Add(new TEditRow { prop = "fight_site_distance", value = "400", no = 300 });
            rows.Add(new TEditRow { prop = "fight_site_count", value = "4", no = 400 });
            rows.Add(new TEditRow { prop = "fight_battle_type", value = "雙敗淘汰制", no = 500 });
            rows.Add(new TEditRow { prop = "fight_robin_player", value = "16", no = 600 });
            rows.Add(new TEditRow { prop = "two_time_trial", value = "", no = 700 });
            rows.Add(new TEditRow { prop = "fight_type_array", value = "H,R,SF,F", no = 800 });
            rows.Add(new TEditRow { prop = "fight_day_array", value = "2023-04-30,2023-05-01", no = 2100 });
            rows.Add(new TEditRow { prop = "has_setted", value = "是", no = 90000 });

            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];

                var cond = "source_id = '" + cfg.meeting_id + "'"
                    + " AND in_key = '" + row.prop + "'";

                var itmNew = cfg.inn.newItem("IN_MEETING_VARIABLE", "merge");
                itmNew.setAttribute("where", cond);
                itmNew.setProperty("source_id", cfg.meeting_id);
                itmNew.setProperty("in_key", row.prop);
                itmNew.setProperty("in_value", row.value);
                itmNew.setProperty("sort_order", row.no.ToString());
                itmNew.apply();
            }
        }


        private void ClearFightDayTimeEvents(TConfig cfg, Item itmReturn)
        {
            //清空當日賽程
            var sql1 = "DELETE FROM [IN_MEETING_PTIME]"
                + " WHERE [in_meeting] = '" + cfg.meeting_id + "'"
                + " AND [in_fight_day] = '" + cfg.in_fight_day + "'";

            cfg.inn.applySQL(sql1);
        }

        private void SaveFightDayTimeEvents(TConfig cfg, Item itmReturn)
        {
            var data = itmReturn.getProperty("data", "");
            var rows = GetEditRows(cfg, data);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, rows.Count.ToString());

            //清空當日賽程
            var sql1 = "DELETE FROM [IN_MEETING_PTIME]"
                + " WHERE [in_meeting] = '" + cfg.meeting_id + "'"
                + " AND [in_fight_day] = '" + cfg.in_fight_day + "'";

            cfg.inn.applySQL(sql1);

            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];

                var cond = "in_meeting = '" + cfg.meeting_id + "'"
                    + " AND in_fight_day = '" + cfg.in_fight_day + "'"
                    + " AND in_time_num = '" + row.no + "'";

                var itmNew = cfg.inn.newItem("IN_MEETING_PTIME", "merge");
                itmNew.setAttribute("where", cond);
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_fight_day", cfg.in_fight_day);
                itmNew.setProperty("in_time_num", row.no.ToString());

                if (!string.IsNullOrWhiteSpace(row.id))
                {
                    itmNew.setProperty("in_type", "event");
                    itmNew.setProperty("in_event", row.id);
                    itmNew.setProperty("in_title", row.time);
                    itmNew.setProperty("in_label", row.label);
                    itmNew = itmNew.apply();
                }
                else if (!string.IsNullOrWhiteSpace(row.label))
                {
                    itmNew.setProperty("in_type", GetTimeType(row.label));
                    itmNew.setProperty("in_event", null);
                    itmNew.setProperty("in_title", row.time);
                    itmNew.setProperty("in_label", row.label);
                    itmNew = itmNew.apply();
                }
            }
        }

        private string GetTimeType(string value)
        {
            if (string.IsNullOrWhiteSpace(value)) return "";
            switch (value)
            {
                case "計時賽": return "trial";
                case "預備場次": return "spare";
                case "休息": return "lunch";
                case "頒獎": return "awards";
                case "空格": return "empty";
                case "上午": return "am";
                case "下午": return "pm";
                default: return "";
            }
        }

        private void ClearFightDayEvents(TConfig cfg, Item itmReturn)
        {
            //清空已上日期的場次
            var sql1 = "UPDATE [IN_MEETING_PEVENT] SET"
                + "  [in_fight_day] = NULL"
                + ", [in_show_serial] = [in_tree_no]"
                + " WHERE [in_meeting] = '" + cfg.meeting_id + "'";

            cfg.inn.applySQL(sql1);
        }

        private void SaveFightDayEvents(TConfig cfg, Item itmReturn)
        {
            var data = itmReturn.getProperty("data", "");
            var rows = GetEditRows(cfg, data);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, rows.Count.ToString());

            //清空已上日期的場次
            var sql1 = "UPDATE [IN_MEETING_PEVENT] SET"
                + "  [in_fight_day] = NULL"
                + ", [in_show_serial] = [in_tree_no]"
                + " WHERE [in_meeting] = '" + cfg.meeting_id + "'";

            cfg.inn.applySQL(sql1);

            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];

                var sql2 = "UPDATE [IN_MEETING_PEVENT] SET"
                    + "  [in_fight_day] = '" + row.value + "'"
                    + ", [in_show_serial] = " + row.no
                    + " WHERE [id] = '" + row.id + "'";

                cfg.inn.applySQL(sql2);
            }
        }

        private void ClearProgramGroups(TConfig cfg, Item itmReturn)
        {
            var sql = "UPDATE [IN_MEETING_PROGRAM] SET"
            + " [in_group_name] = NULL"
            + " WHERE [in_meeting] = '" + cfg.meeting_id + "'";

            cfg.inn.applySQL(sql);
        }

        private void SaveProgramGroups(TConfig cfg, Item itmReturn)
        {
            var data = itmReturn.getProperty("data", "");
            var rows = GetEditRows(cfg, data);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, rows.Count.ToString());

            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                var g = row.value;

                var sql = "";
                if (g == null || g == "" || g == "w" || g == "W")
                {
                    sql = "UPDATE [IN_MEETING_PROGRAM] SET [in_group_name] = NULL WHERE [id] = '" + row.id + "'";
                }
                else
                {
                    g = g.Trim().ToUpper();
                    sql = "UPDATE [IN_MEETING_PROGRAM] SET [in_group_name] = '" + g + "' WHERE [id] = '" + row.id + "'";
                }
                cfg.inn.applySQL(sql);
            }
        }

        private void EditFootPlayer(TConfig cfg, Item itmReturn)
        {
            var program_id = itmReturn.getProperty("program_id", "");
            var detail_id = itmReturn.getProperty("detail_id", "");
            var in_sign_no = itmReturn.getProperty("in_sign_no", "");
            var in_pteam = itmReturn.getProperty("in_pteam", "");

            var sql = "SELECT id, in_sign_no FROM IN_MEETING_PTEAM WITH(NOLOCK)"
            + " WHERE source_id = '" + program_id + "'"
            + " AND id = '" + in_pteam + "'";

            var itmOld = cfg.inn.applySQL(sql);
            if (itmOld.isError() || itmOld.getResult() == "")
            {
                throw new Exception("發生錯誤");
            }

            var old_team_id = itmOld.getProperty("id", "");
            var old_sign_no = itmOld.getProperty("in_sign_no", "");

            var sql2 = "";

            if (old_sign_no != "" && old_sign_no != "0")
            {
                sql2 = "UPDATE [In_Meeting_PEvent_Detail] SET [in_sign_no] = '" + old_sign_no + "', [in_pteam] = NULL WHERE [id] = '" + detail_id + "'";
            }
            else
            {
                sql2 = "UPDATE [In_Meeting_PEvent_Detail] SET [in_pteam] = '" + old_team_id + "', [in_sign_no] = NULL WHERE [id] = '" + detail_id + "'";
            }

            cfg.inn.applySQL(sql2);
        }

        private Dictionary<string, int> ProgramDrawNoMap(TConfig cfg
            , string in_battle_type
            , string in_tree_key)
        {
            var map = new Dictionary<string, int>();

            var sql = "SELECT id, in_key, in_sign_no FROM IN_MEETING_DRAWSIGNNO WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_battle_type = '" + in_battle_type + "'"
                + " AND in_tree_key = '" + in_tree_key + "'";

            var items = cfg.inn.applySQL(sql);

            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_key = item.getProperty("in_key", "");
                var in_sign_no = item.getProperty("in_sign_no", "");
                if (in_key == "") continue;
                if (map.ContainsKey(in_key)) continue;

                map.Add(in_key, GetInt(in_sign_no));
            }

            return map;
        }

        private void AssignDrawNo(TConfig cfg, Item itmReturn)
        {
            var in_battle_type = itmReturn.getProperty("in_battle_type", "");
            var in_tree_key = itmReturn.getProperty("in_tree_key", "");
            var in_key = itmReturn.getProperty("in_key", "");
            var in_sign_no = itmReturn.getProperty("in_sign_no", "");

            var cond = "in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_battle_type = '" + in_battle_type + "'"
                + " AND in_tree_key = '" + in_tree_key + "'"
                + " AND in_key = '" + in_key + "'";

            var itmNew = cfg.inn.newItem("In_Meeting_DrawSignNo", "merge");
            itmNew.setAttribute("where", cond);
            itmNew.setProperty("in_meeting", cfg.meeting_id);
            itmNew.setProperty("in_battle_type", in_battle_type);
            itmNew.setProperty("in_tree_key", in_tree_key);
            itmNew.setProperty("in_key", in_key);
            itmNew.setProperty("in_sign_no", in_sign_no);

            itmNew.apply();
        }

        private void RunDraw(TConfig cfg, Item itmReturn)
        {
            cfg.variableMap = VariableMap(cfg);
            cfg.waterWayCount = GeMapIntValue(cfg.variableMap, "fight_site_count", 4);
            cfg.timeTrialTeamCount = GeMapIntValue(cfg.variableMap, "fight_robin_player", 12);
            cfg.hasTwoTimeTrialEvents = HasTwoTimeTrialEvents(cfg.variableMap, "two_time_trial");

            var sql = "SELECT id, in_name, in_l1, in_l2, in_l3, in_team_count FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            var itmProgram = cfg.inn.applySQL(sql);
            if (itmProgram.isError() || itmProgram.getResult() == "")
            {
                throw new Exception("查無組別資料");
            }

            //隊伍數
            var teamCount = GetInt(itmProgram.getProperty("in_team_count", "0"));
            var drawCount = teamCount > cfg.timeTrialTeamCount ? cfg.timeTrialTeamCount : teamCount;

            //通過預賽
            var items = GetHeatTeams(cfg, cfg.program_id, teamCount);
            if (items.isError() || items.getResult() == "")
            {
                throw new Exception("查無資料");
            }

            var rows = MapDrawRows(cfg, items);
            var sportType = InnSport.Core.Enums.SportEnum.Taekwondo;
            var sameTeamSeparate = "Y";

            //執行抽籤
            var entities = ExecuteDraw1(cfg
                , rows
                , sportType
                , needSeedLottery: "N"
                , sameTeamSeparate: sameTeamSeparate
                , seed14SameFace: "N"
                , robinPlayerCount: 2);

            //更新籤號
            UpdateDrawNo(cfg, itmProgram, entities);

            for (var i = 0; i < entities.Count; i++)
            {
                var entity = entities[i];
                var item = cfg.inn.newItem("inn_draw");
                item.setProperty("in_name", entity.in_name);
                item.setProperty("in_current_org", entity.in_current_org);
                item.setProperty("in_sign_no", entity.in_draw_no);
                item.setProperty("in_draw_no", entity.in_draw_no);
                itmReturn.addRelationship(item);
            }

            // var items2 = GetHeatTeams(cfg, teamCount);
            // var count = items2.getItemCount();
            // for (var i = 0; i < count; i++)
            // {
            //     var item = items2.getItemByIndex(i);
            //     item.setType("inn_draw");
            //     item.setProperty("in_draw_no", item.getProperty("in_sign_no", ""));
            //     itmReturn.addRelationship(item);
            // }
        }

        /// <summary>
        /// 執行抽籤
        /// </summary>
        private List<InnSport.Core.Models.Output.Item> ExecuteDraw1(
            TConfig cfg
            , List<InnSport.Core.Models.Output.Item> resource
            , InnSport.Core.Enums.SportEnum sportType
            , string needSeedLottery = "Y"
            , string sameTeamSeparate = "Y"
            , string seed14SameFace = "N"
            , int robinPlayerCount = 2)
        {
            var ns = InnSport.Core.Utilities.TUtility.RandomArray(resource.Count)
                .ToList();

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, Newtonsoft.Json.JsonConvert.SerializeObject(ns));

            for (var i = 0; i < resource.Count; i++)
            {
                var row = resource[i];
                row.in_draw_no = ns[i].ToString();
            }

            return resource;
        }

        /// <summary>
        /// 執行抽籤
        /// </summary>
        private List<InnSport.Core.Models.Output.Item> ExecuteDraw2(
            TConfig cfg
            , List<InnSport.Core.Models.Output.Item> resource
            , InnSport.Core.Enums.SportEnum sportType
            , string needSeedLottery = "Y"
            , string sameTeamSeparate = "Y"
            , string seed14SameFace = "N"
            , int robinPlayerCount = 2)
        {
            var draw = new InnSport.Core.Models.Business.TConfig();
            draw.SportType = sportType;
            draw.SourceType = InnSport.Core.Enums.SourceEnum.List;
            draw.Resource = resource;
            draw.DrawType = "2";
            draw.Integral = needSeedLottery; //種子籤: Y
            draw.Draw99 = "N";//99號籤: Y
            draw.SameTeamSeparate = sameTeamSeparate == "Y";
            draw.Seed14SameFace = seed14SameFace == "Y";
            draw.RobinPlayerCount = robinPlayerCount;
            return draw.ExecuteList();
        }

        private void UpdateDrawNo(TConfig cfg, Item itmProgram, List<InnSport.Core.Models.Output.Item> rows)
        {
            var in_l1 = itmProgram.getProperty("in_l1", "");
            var in_l2 = itmProgram.getProperty("in_l2", "");
            var in_l3 = itmProgram.getProperty("in_l3", "");

            //清空該組籤號
            var sql1 = "UPDATE [IN_MEETING_PTEAM] SET"
                + "   [in_sign_no] = NULL"
                + " WHERE source_id = '" + cfg.program_id + "'";

            cfg.inn.applySQL(sql1);

            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];

                var sql2 = "UPDATE [IN_MEETING_PTEAM] SET"
                    + "  [in_sign_no] = " + row.in_draw_no
                    + ", [in_sign_time] = GETUTCDATE()"
                    + " WHERE id = '" + row.key + "'";

                cfg.inn.applySQL(sql2);

                var sql3 = "UPDATE [IN_MEETING_USER] SET"
                    + "   [in_draw_no] = " + row.in_draw_no
                    + " WHERE source_id = '" + cfg.meeting_id + "'"
                    + " AND in_l1 = N'" + in_l1 + "'"
                    + " AND in_l2 = N'" + in_l2 + "'"
                    + " AND in_l3 = N'" + in_l3 + "'"
                    + " AND in_index = '" + row.in_index + "'";

                cfg.inn.applySQL(sql3);

            }

            var sql4 = "UPDATE [IN_MEETING_PROGRAM] SET"
                + " [in_sign_time] = GETUTCDATE()"
                + " WHERE id = '" + cfg.program_id + "'";

            cfg.inn.applySQL(sql4);

        }

        private List<InnSport.Core.Models.Output.Item> MapDrawRows(TConfig cfg, Item items)
        {
            var rows = new List<InnSport.Core.Models.Output.Item>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var row = new InnSport.Core.Models.Output.Item
                {
                    id = i + 1,
                    in_section_name = item.getProperty("source_id", ""),
                    key = item.getProperty("id", ""),
                    team_key = item.getProperty("in_index", ""),
                    in_index = item.getProperty("in_index", ""),
                    in_name = item.getProperty("in_name", ""),
                    in_current_org = item.getProperty("in_current_org", ""),
                    in_draw_no = item.getProperty("in_sign_no", ""),
                    in_seed_lottery = item.getProperty("in_seeds", ""),
                    in_points = 0,//GetInt(item.getProperty("in_seeds_points", "0")),
                };
                rows.Add(row);
            }

            return rows;
        }

        //teamCount > 12: 增加[需通過預賽]條件
        private Item GetHeatTeams(TConfig cfg, string program_id, int teamCount)
        {
            var heatCond = teamCount > cfg.timeTrialTeamCount ? "AND ISNULL(in_heat_yn, '') IN ('1', 'V')" : "";

            var sql = @"
				SELECT 
					source_id
					, id
					, in_name
					, in_current_org
					, in_index
					, in_sign_no
					, in_seeds
					, ISNULL(in_seeds_points, 0)	AS 'in_seeds_points'
				FROM 
					[IN_MEETING_PTEAM] WITH(NOLOCK) 
				WHERE 
					source_id = '{#program_id}'
					{#heatCond}
				ORDER BY 
					in_current_org
					, in_index
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#heatCond}", heatCond);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private void EditProperty(TConfig cfg, Item itmReturn)
        {
            var itemType = itmReturn.getProperty("itemType", "").ToUpper();
            var prop = itmReturn.getProperty("property", "").ToLower();
            var id = itmReturn.getProperty("id", "");
            var value = itmReturn.getProperty("value", "");

            var itmData = cfg.inn.applySQL("SELECT * FROM " + itemType + " WITH(NOLOCK) WHERE id = '" + id + "'");
            if (itmData.isError() || itmData.getResult() == "")
            {
                throw new Exception("查無資料");
            }

            var sql = "UPDATE [" + itemType + "] SET [" + prop + "] = N'" + value + "', modified_on = getutcdate() WHERE [id] = '" + id + "'";
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            cfg.inn.applySQL(sql);

            if (itemType == "IN_MEETING_PEVENT_DETAIL")
            {
                if (prop == "in_score" || prop == "in_rank")
                {
                    OverrideTeamProp(cfg, itmData, prop, value);
                }
            }
        }

        private void OverrideTeamProp(TConfig cfg, Item item, string keyword, string value)
        {
            var program_id = item.getProperty("in_program", "");
            var event_id = item.getProperty("source_id", "");
            var team_id = item.getProperty("in_pteam", "");
            //var in_fight_id = item.getProperty("in_fight_id", "");
            var in_sign_no = item.getProperty("in_sign_no", "");

            var itmTeam = GetTeamItem(cfg, program_id, team_id, in_sign_no);
            if (itmTeam == null || itmTeam.isError() || itmTeam.getResult() == "") return;

            var itmEvent = cfg.inn.applySQL("SELECT id, in_fight_id FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE id = '" + event_id + "'");
            if (itmEvent == null || itmEvent.isError() || itmEvent.getResult() == "") return;

            var id = itmTeam.getProperty("id", "");
            var in_fight_id = itmEvent.getProperty("in_fight_id", "");
            var suffix = keyword.Contains("score") ? "score" : "rank";
            var prop = GetTeamScoreProp(in_fight_id, suffix);

            var sql = "UPDATE [IN_MEETING_PTEAM] SET [" + prop + "] = '" + value + "' WHERE id = '" + id + "'";
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "OverrideTeamTimes" + Environment.NewLine + sql);
            cfg.inn.applySQL(sql);
        }

        private string GetTeamScoreProp(string in_fight_id, string suffix)
        {
            switch (in_fight_id)
            {
                case "TT2":
                    return "in_time_trial_" + suffix + "2";

                case "TT1":
                    return "in_time_trial_" + suffix;

                case "TT":
                    return "in_time_trial_" + suffix;

                case "TIME":
                    return "in_time_trial_" + suffix;

                case "H":
                case "H1":
                case "H2":
                case "H3":
                case "H4":
                case "H5":
                    return "in_heat_" + suffix;

                case "R":
                case "R1":
                case "R2":
                case "R3":
                case "R4":
                    return "in_rpc_" + suffix;

                case "SA":
                case "SB":
                case "SC":
                case "SD":
                    return "in_semifinal_" + suffix;

                case "FA":
                case "FB":
                case "FC":
                case "FD":
                    return "in_final_" + suffix;

                default:
                    return "";
            }
        }

        private Item GetTeamItem(TConfig cfg, string program_id, string id, string in_sign_no)
        {
            var signNo = GetInt(in_sign_no);
            var sql = "";
            if (id != "")
            {
                sql = "SELECT * FROM IN_MEETING_PTEAM WITH(NOLOCK) WHERE id = '" + id + "'";
            }
            else if (signNo > 0)
            {
                sql = "SELECT * FROM IN_MEETING_PTEAM WITH(NOLOCK) WHERE source_id = '" + program_id + "' AND in_sign_no = " + signNo;
            }

            if (sql == "")
            {
                return null;
            }
            else
            {
                return cfg.inn.applySQL(sql);
            }

        }

        private void EditTimeEvent(TConfig cfg, Item itmReturn)
        {
            var in_type = "event";
            var in_time_num = itmReturn.getProperty("in_time_num", "");
            var in_event = itmReturn.getProperty("in_event", "");

            var itmNew = cfg.inn.newItem("IN_MEETING_PTIME", "merge");
            itmNew.setAttribute("where", "in_meeting = '" + cfg.meeting_id + "' AND in_fight_day = '" + cfg.in_fight_day + "' AND in_time_num = '" + in_time_num + "' AND in_type = '" + in_type + "'");
            itmNew.setProperty("in_meeting", cfg.meeting_id);
            itmNew.setProperty("in_fight_day", cfg.in_fight_day);
            itmNew.setProperty("in_time_num", in_time_num);
            itmNew.setProperty("in_event", in_event);
            itmNew.setProperty("in_type", in_type);
            // itmNew.setProperty("in_title", "設定");
            // itmNew.setProperty("in_label", "設定");
            itmNew = itmNew.apply();
        }

        private void EditTime(TConfig cfg, Item itmReturn)
        {
            var in_type = "setting";

            var itmNew = cfg.inn.newItem("IN_MEETING_PTIME", "merge");
            itmNew.setAttribute("where", "in_meeting = '" + cfg.meeting_id + "' AND in_fight_day = '" + cfg.in_fight_day + "' AND in_type = '" + in_type + "'");
            itmNew.setProperty("in_meeting", cfg.meeting_id);
            itmNew.setProperty("in_fight_day", cfg.in_fight_day);
            itmNew.setProperty("in_type", in_type);
            itmNew.setProperty("in_title", "設定");
            itmNew.setProperty("in_label", "設定");
            CopyItemValue(itmNew, itmReturn, "in_am_start");
            CopyItemValue(itmNew, itmReturn, "in_am_end");
            CopyItemValue(itmNew, itmReturn, "in_am_span");
            CopyItemValue(itmNew, itmReturn, "in_pm_start");
            CopyItemValue(itmNew, itmReturn, "in_pm_end");
            CopyItemValue(itmNew, itmReturn, "in_pm_span");
            CopyItemValue(itmNew, itmReturn, "in_group_name");
            itmNew = itmNew.apply();
        }

        private void ClearMeeting(TConfig cfg, Item itmReturn)
        {
            var sql = "";

            sql = "DELETE t1 FROM IN_MEETING_PEVENT_DETAIL t1"
                + " WHERE EXISTS(SELECT* FROM IN_MEETING_PEVENT t2 WITH(NOLOCK) WHERE t2.in_meeting = '{#meeting_id}' AND t2.id = t1.source_id)";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            cfg.inn.applySQL(sql);


            sql = "DELETE FROM IN_MEETING_PEVENT WHERE in_meeting = '{#meeting_id}'";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            cfg.inn.applySQL(sql);


            sql = "DELETE FROM IN_MEETING_PTIME WHERE in_meeting = '{#meeting_id}'";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            cfg.inn.applySQL(sql);


            sql = "DELETE FROM IN_MEETING_PTEAM WHERE in_meeting = '{#meeting_id}'";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            cfg.inn.applySQL(sql);


            sql = "DELETE FROM IN_MEETING_PROGRAM WHERE in_meeting = '{#meeting_id}'";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            cfg.inn.applySQL(sql);
        }

        private void EditProgram(TConfig cfg, Item itmReturn)
        {
            var itmProgram = GetProgramItem(cfg);
            if (itmProgram.isError() || itmProgram.getResult() == "")
            {
                throw new Exception("查無組別資料");
            }

            var program_id = itmProgram.getProperty("id", "");
            var in_battle_type = itmProgram.getProperty("in_battle_type", "");
            var in_tree_key = itmProgram.getProperty("in_tree_key", "");

            var sql = "UPDATE IN_MEETING_PROGRAM SET"
                + "  in_group_name = '" + itmReturn.getProperty("in_group_name", "") + "'"
                + ", in_schedule_show = '" + itmReturn.getProperty("in_schedule_show", "") + "'"
                + ", in_schedule_zero = '" + itmReturn.getProperty("in_schedule_zero", "") + "'"
                + " WHERE id = '" + program_id + "'";

            cfg.inn.applySQL(sql);
        }

        private void RebuildEvents1(TConfig cfg, TProgram program, Dictionary<string, int> map)
        {
            RebuildEvents(cfg
                , program.id
                , program.in_team_count
                , program.in_water_way
                , program.in_time_trial
                , program.in_schedule_value
                , map);
        }

        private void RebuildEvents(TConfig cfg
            , string program_id
            , string in_team_count
            , string in_water_way
            , string in_time_trial
            , string in_schedule_value
            , Dictionary<string, int> drawNoMap)
        {
            if (in_water_way == "" || in_water_way == "0") return;
            if (in_schedule_value == "") return;

            var water_way = GetInt(in_water_way);

            var arr = in_schedule_value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            var treeMap = CreateTreeMap(cfg, arr);

            //建立計時賽
            if (in_time_trial == "1")
            {
                if (cfg.hasTwoTimeTrialEvents)
                {
                    CreateTimingEvents(cfg, treeMap, program_id);
                }
                else
                {
                    CreateTimingEvent(cfg, treeMap, program_id);
                }
            }

            if (arr == null || arr.Length == 0) return;

            var team_count = GetInt(in_team_count);

            //建立預賽、複賽、準決賽、決賽
            for (var i = 0; i < arr.Length; i++)
            {
                var in_fight_id = arr[i].ToUpper();
                var in_tree_no = i + 1;

                if (in_fight_id == "") continue;
                if (!treeMap.ContainsKey(in_fight_id))
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, in_fight_id);
                    continue;
                }

                var eTree = treeMap[in_fight_id];

                //建立場次
                var itmEvent = NewEvent(cfg
                    , program_id
                    , in_fight_id
                    , in_tree_no.ToString()
                    , eTree);

                if (itmEvent.isError()) continue;
                if (itmEvent.getResult() == "") continue;

                var event_id = itmEvent.getID();
                for (var j = 0; j <= water_way; j++)
                {
                    var in_water_num = j.ToString();

                    //建立明細
                    NewDetail1(cfg
                        , program_id
                        , event_id
                        , in_fight_id
                        , in_water_num
                        , team_count
                        , drawNoMap);
                }
            }
        }

        private Dictionary<string, TTree> CreateTreeMap(TConfig cfg, string[] arr)
        {
            var map = new Dictionary<string, TTree>();
            if (cfg.hasTwoTimeTrialEvents)
            {
                map.Add("TT1", new TTree { name = "TT1", desc = "TT1", sort = 600 });
                map.Add("TT2", new TTree { name = "TT2", desc = "TT2", sort = 700 });
            }
            else
            {
                map.Add("SYS_TIME", new TTree { name = "TT", desc = "計時賽", sort = 500 });
            }

            if (arr != null)
            {
                for (var i = 0; i < arr.Length; i++)
                {
                    var word = arr[i];
                    if (word == "") continue;

                    //var c = word[0].ToString();
                    if (map.ContainsKey(word)) continue;

                    map.Add(word, new TTree { name = word, desc = "", sort = GetTreeSort(word.ToUpper()) });
                }
            }

            return map;
        }

        private int GetEventSort(string value)
        {
            switch (value)
            {
                case "TT": return 500;
                case "TIME": return 500;
                case "TT1": return 600;
                case "TT2": return 700;

                case "LR": return 1500;
                case "H": return 1500;

                case "H1": return 1110;
                case "H2": return 1120;
                case "H3": return 1130;
                case "H4": return 1140;
                case "H5": return 1150;
                case "H6": return 1160;
                case "H7": return 1170;
                case "H8": return 1180;

                case "R": return 3500;
                case "R1": return 3110;
                case "R2": return 3120;
                case "R3": return 3130;
                case "R4": return 3140;
                case "R5": return 3150;
                case "R6": return 3160;

                case "S": return 5100;
                case "SD": return 5110;
                case "SC": return 5320;
                case "SB": return 5530;
                case "SA": return 5740;

                case "F": return 7100;
                case "FD": return 7110;
                case "FC": return 7310;
                case "FB": return 7510;
                case "FA": return 7710;

                default: return 100;//異常
            }
        }

        private int GetTreeSort(string value)
        {
            switch (value)
            {
                case "TT": return 500;
                case "TIME": return 500;
                case "TT1": return 600;
                case "TT2": return 700;

                case "LR": return 1500;
                case "H": return 1500;

                case "H1": return 1100;
                case "H2": return 1100;
                case "H3": return 1100;
                case "H4": return 1100;
                case "H5": return 1100;
                case "H6": return 1100;
                case "H7": return 1100;
                case "H8": return 1100;

                case "R": return 3500;
                case "R1": return 3100;
                case "R2": return 3100;
                case "R3": return 3100;
                case "R4": return 3100;
                case "R5": return 3100;
                case "R6": return 3100;

                case "S": return 5100;
                case "SD": return 5100;
                case "SC": return 5300;
                case "SB": return 5500;
                case "SA": return 5700;

                case "F": return 7100;
                case "FD": return 7100;
                case "FC": return 7300;
                case "FB": return 7500;
                case "FA": return 7700;

                default: return 100;//異常
            }
        }

        //建立兩場計時賽
        private void CreateTimingEvents(TConfig cfg, Dictionary<string, TTree> treeMap, string program_id)
        {
            var tree = treeMap["TT1"];
            var in_fight_id = "TT1";
            var in_tree_no = 9910;

            var itmEvent = NewEvent(cfg, program_id, in_fight_id, in_tree_no.ToString(), tree);

            var itmTeams = GetHeatTeams(cfg, program_id, 0);
            var teamCount = itmTeams.getItemCount();

            var event_id = itmEvent.getID();
            for (var j = 0; j < teamCount; j++)
            {
                var itmTeam = itmTeams.getItemByIndex(j);
                var team_id = itmTeam.getProperty("id", "");
                var in_water_num = (j + 1).ToString();

                NewDetail2(cfg, program_id, event_id, in_fight_id, in_water_num, team_id);
            }

            var tree2 = treeMap["TT2"];
            var in_fight_id2 = "TT2";
            var in_tree_no2 = 9920;
            var itmEvent2 = NewEvent(cfg, program_id, in_fight_id2, in_tree_no2.ToString(), tree2);
        }

        //建立計時賽
        private void CreateTimingEvent(TConfig cfg, Dictionary<string, TTree> treeMap, string program_id)
        {
            var tree = treeMap["SYS_TIME"];
            var in_fight_id = "TIME";
            var in_tree_no = 9999;

            var itmEvent = NewEvent(cfg, program_id, in_fight_id, in_tree_no.ToString(), tree);

            var itmTeams = GetHeatTeams(cfg, program_id, 0);
            var teamCount = itmTeams.getItemCount();

            var event_id = itmEvent.getID();
            for (var j = 0; j < teamCount; j++)
            {
                var itmTeam = itmTeams.getItemByIndex(j);
                var team_id = itmTeam.getProperty("id", "");
                var in_water_num = (j + 1).ToString();

                NewDetail2(cfg, program_id, event_id, in_fight_id, in_water_num, team_id);
            }
        }

        private Item NewDetail2(TConfig cfg
            , string program_id
            , string event_id
            , string in_fight_id
            , string in_water_num
            , string team_id)
        {
            var in_key = in_fight_id + "-" + "W" + in_water_num;

            var itmNew = cfg.inn.newItem("In_Meeting_PEvent_Detail", "add");
            itmNew.setProperty("source_id", event_id);
            itmNew.setProperty("in_meeting", cfg.meeting_id);
            itmNew.setProperty("in_program", program_id);
            itmNew.setProperty("in_fight_id", in_fight_id);
            itmNew.setProperty("in_sign_foot", in_water_num);
            itmNew.setProperty("in_key", in_key);
            itmNew.setProperty("in_pteam", team_id);

            return itmNew.apply();
        }

        private Item NewDetail1(TConfig cfg
            , string program_id
            , string event_id
            , string in_fight_id
            , string in_water_num
            , int team_count
            , Dictionary<string, int> map)
        {
            var in_key = in_fight_id + "-" + "W" + in_water_num;
            var in_sign_no = FindSignNo(map, in_key);

            var itmNew = cfg.inn.newItem("In_Meeting_PEvent_Detail", "add");
            itmNew.setProperty("source_id", event_id);
            itmNew.setProperty("in_meeting", cfg.meeting_id);
            itmNew.setProperty("in_program", program_id);
            itmNew.setProperty("in_fight_id", in_fight_id);
            itmNew.setProperty("in_sign_foot", in_water_num);
            itmNew.setProperty("in_key", in_key);

            if (in_sign_no > team_count)
            {
                //itmNew.setProperty("in_sign_bypass", "1");
                itmNew.setProperty("in_sign_no", in_sign_no.ToString());
            }
            else if (in_sign_no > 0)
            {
                //itmNew.setProperty("in_sign_bypass", "0");
                itmNew.setProperty("in_sign_no", in_sign_no.ToString());
            }
            else
            {
                itmNew.setProperty("in_sign_bypass", "");
            }

            return itmNew.apply();
        }

        private int FindSignNo(Dictionary<string, int> map, string in_key)
        {
            if (map == null || map.Count == 0) return 0;
            if (in_key == "") return 0;
            if (!map.ContainsKey(in_key)) return 0;
            return map[in_key];
        }

        private Item NewEvent(TConfig cfg, string program_id, string in_fight_id, string in_tree_no, TTree tree)
        {
            var itmNew = cfg.inn.newItem("In_Meeting_PEvent", "add");
            itmNew.setProperty("in_meeting", cfg.meeting_id);
            itmNew.setProperty("source_id", program_id);
            itmNew.setProperty("in_fight_id", in_fight_id);

            itmNew.setProperty("in_tree_name", tree.name);
            itmNew.setProperty("in_tree_sort", tree.sort.ToString());
            itmNew.setProperty("in_tree_no", in_tree_no);
            itmNew.setProperty("in_show_serial", in_tree_no);
            itmNew.setProperty("in_show_no", in_tree_no);

            return itmNew.apply();
        }

        private void RemoveOldEvents(TConfig cfg, string program_id)
        {
            var sql = "";

            sql = "DELETE t1 FROM IN_MEETING_PEVENT_DETAIL t1"
                + " WHERE EXISTS(SELECT * FROM IN_MEETING_PEVENT t2 WITH(NOLOCK) WHERE t2.source_id = '{#program_id}' AND t2.id = t1.source_id)";

            sql = sql.Replace("{#program_id}", program_id);

            cfg.inn.applySQL(sql);


            sql = "DELETE FROM IN_MEETING_PEVENT WHERE source_id = '{#program_id}'";

            sql = sql.Replace("{#program_id}", program_id);

            cfg.inn.applySQL(sql);
        }

        //組別建檔
        private void CreatePrograms(TConfig cfg, Item itmReturn)
        {
            cfg.variableMap = VariableMap(cfg);
            cfg.waterWayCount = GeMapIntValue(cfg.variableMap, "fight_site_count", 4);
            cfg.timeTrialTeamCount = GeMapIntValue(cfg.variableMap, "fight_robin_player", 12);
            cfg.hasTwoTimeTrialEvents = HasTwoTimeTrialEvents(cfg.variableMap, "two_time_trial");

            var itmPrograms = ProgramListFromMeetingUser(cfg);
            if (itmPrograms.isError() || itmPrograms.getResult() == "")
            {
                throw new Exception("查無組別資料");
            }

            var registerCountMap = MapProgramRegisterCount(cfg);
            var teamCountMap = MapProgramTeamCount(cfg);
            var programs = MapProgramList(cfg, itmPrograms, registerCountMap, teamCountMap);
            for (var i = 0; i < programs.Count; i++)
            {
                var program = programs[i];
                if (program.in_team_count == "" || program.in_team_count == "0")
                {
                    continue;
                }

                if (program.id == "")
                {
                    NewProgram(cfg, program);
                }
                else
                {
                    UpdProgram(cfg, program);
                }
            }
        }

        private void RebuildProgram(TConfig cfg, Item itmReturn)
        {
            cfg.variableMap = VariableMap(cfg);
            cfg.waterWayCount = GeMapIntValue(cfg.variableMap, "fight_site_count", 4);
            cfg.timeTrialTeamCount = GeMapIntValue(cfg.variableMap, "fight_robin_player", 12);
            cfg.hasTwoTimeTrialEvents = HasTwoTimeTrialEvents(cfg.variableMap, "two_time_trial");

            var map = GetProgramMUsers(cfg);
            var kv = map.First();
            var program = kv.Value;

            var teamCount = GetInt(program.in_team_count);
            var in_tree_key = itmReturn.getProperty("in_tree_key", "");
            var in_schedule_value = GetEventNamesFromTreeKey(in_tree_key);
            if (in_schedule_value == "")
            {
                throw new Exception("查無賽程表場次");
            }

            var trial = "0";
            if (teamCount > cfg.timeTrialTeamCount)
            {
                trial = "1";
            }

            program.in_time_trial = trial;
            program.in_schedule_mode = in_schedule_value;
            program.in_schedule_value = in_schedule_value;

            var evts = program.in_schedule_value.Split(',');
            var evt_cnt = evts.Length;
            program.in_event_count = evt_cnt.ToString();

            var sql = "UPDATE IN_MEETING_PROGRAM SET"
                + "  in_tree_key = '" + in_tree_key + "'"
                + ", in_schedule_mode = '" + program.in_schedule_mode + "'"
                + ", in_schedule_value = '" + program.in_schedule_value + "'"
                + ", in_schedule_show = '" + program.in_schedule_value + "'"
                + ", in_event_count = '" + program.in_event_count + "'"
                + " WHERE id = '" + program.id + "'";

            cfg.inn.applySQL(sql);

            //籤號對照表
            var drawNoMap = ProgramDrawNoMap(cfg
                , program.in_battle_type
                , in_tree_key);

            //場次清除
            RemoveOldEvents(cfg, program.id);

            RebuildEvents(cfg
                , program.id
                , program.in_team_count
                , cfg.waterWayCount.ToString()
                , program.in_time_trial
                , in_schedule_value
                , drawNoMap);
        }

        private void CreateProgramTeams(TConfig cfg, Item itmReturn)
        {
            var map = GetProgramMUsers(cfg);
            foreach (var kv in map)
            {
                var program = kv.Value;

                //籤號對照表
                var drawNoMap = ProgramDrawNoMap(cfg
                    , program.in_battle_type
                    , program.in_tree_key);

                //隊別建檔
                RebuildTeams(cfg, program);

                //場次清除
                RemoveOldEvents(cfg, program.id);

                //場次建檔
                RebuildEvents1(cfg, program, drawNoMap);
            }
        }

        //隊別建檔
        private void RebuildTeams(TConfig cfg, TProgram program)
        {
            //隊別建檔
            foreach (var team in program.teams)
            {
                var players = new List<TPlayer>();
                if (team.player1.Count > 0) players.AddRange(team.player1);
                if (team.player2.Count > 0) players.AddRange(team.player2);

                if (players.Count > 1)
                {
                    team.in_sno = "";
                    team.in_gender = "";
                    team.in_paddle = "";
                    team.in_waiting_list = "";

                    var names = string.Join(", ", players.Select(x => x.display));
                    team.in_name = names;
                    team.in_names = names;
                    team.in_team_players = players.Count.ToString();
                }
                else
                {
                    team.in_names = team.in_name;
                }

                MergeProgramTeam(cfg, program.id, team);
            }
        }

        private void MergeProgramTeam(TConfig cfg, string program_id, TTeam team)
        {
            var itmNew = cfg.inn.newItem("IN_MEETING_PTEAM", "merge");
            itmNew.setAttribute("where", "source_id = '" + program_id + "' AND in_team_key = '" + team.in_team_key + "'");
            itmNew.setProperty("in_meeting", cfg.meeting_id);
            itmNew.setProperty("source_id", program_id);
            itmNew.setProperty("in_team_key", team.in_team_key);

            itmNew.setProperty("in_current_org", team.in_current_org);
            itmNew.setProperty("in_short_org", team.in_short_org);
            itmNew.setProperty("in_team", team.in_team);
            itmNew.setProperty("in_index", team.in_index);
            itmNew.setProperty("in_creator_sno", team.in_creator_sno);

            itmNew.setProperty("in_name", team.in_name);
            itmNew.setProperty("in_sno", team.in_sno);
            itmNew.setProperty("in_gender", team.in_gender);
            itmNew.setProperty("in_paddle", team.in_paddle);
            //itmNew.setProperty("in_waiting_list", team.in_waiting_list);

            itmNew.setProperty("in_names", team.in_names);
            itmNew.setProperty("in_team_players", team.in_team_players);

            itmNew = itmNew.apply();
        }

        private Dictionary<string, TProgram> GetProgramMUsers(TConfig cfg)
        {
            var cond = cfg.program_id != ""
                ? "AND t2.id = '" + cfg.program_id + "'"
                : "";

            var sql = @"
                SELECT 
	               t1.*
				   , t2.id AS 'program_id'
				   , t2.in_battle_type
				   , t2.in_tree_key
				   , t2.in_team_count
				   , t2.in_water_way
				   , t2.in_time_trial
				   , t2.in_schedule_value
                FROM 
	                VU_FIGHT_MUSERS t1
				INNER JOIN
					IN_MEETING_PROGRAM t2 WITH(NOLOCK)
					ON t2.in_meeting = t1.source_id
					AND t2.in_name = t1.program_key
                WHERE 
	                t1.source_id = '{#meeting_id}' 
	                {#cond}
                ORDER BY 
	                t1.in_l1_sort
	                , t1.in_l2_sort
	                , t1.in_l3_sort
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#cond}", cond);

            var map = new Dictionary<string, TProgram>();

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var program_id = item.getProperty("program_id", "");
                var team_key = item.getProperty("team_key", "");

                var program = default(TProgram);
                if (map.ContainsKey(program_id))
                {
                    program = map[program_id];
                }
                else
                {
                    program = MapProgramModel(cfg, item, program_id);
                    map.Add(program_id, program);
                }

                var player = NewPlayer(item);
                var team = program.teams.Find(x => x.in_team_key == team_key);
                if (team == null)
                {
                    team = new TTeam
                    {
                        in_team_key = team_key,
                        in_current_org = player.in_current_org,
                        in_short_org = player.in_short_org,
                        in_team = player.in_team,
                        in_index = player.in_index,
                        in_creator_sno = player.in_creator_sno,

                        in_name = player.in_name,
                        in_sno = player.in_sno,
                        in_gender = player.in_gender,
                        in_paddle = player.in_paddle,
                        in_waiting_list = player.in_waiting_list,

                        player1 = new List<TPlayer>(),
                        player2 = new List<TPlayer>(),
                    };
                    program.teams.Add(team);
                }

                //備
                if (player.isBack)
                {
                    team.player2.Add(player);
                }
                else
                {
                    team.player1.Add(player);
                }
            }

            return map;
        }

        private string GetProgramName2(TProgram program)
        {
            var suffix = program.in_l3.Replace("高男", "")
                .Replace("高女", "")
                .Replace("國男", "")
                .Replace("國女", "");

            switch (program.in_l2)
            {
                case "公開男子組": return "公男-" + suffix;
                case "公開女子組": return "公女-" + suffix;
                case "高中男子組": return "高男-" + suffix;
                case "高中女子組": return "高女-" + suffix;
                case "國中男子組": return "國男-" + suffix;
                case "國中女子組": return "國女-" + suffix;

                case "高中一年級組":
                    if (program.in_l3.Contains("男")) return "高一男-" + suffix;
                    if (program.in_l3.Contains("女")) return "高一女-" + suffix;
                    return "高一-" + suffix;

                case "國中一年級組":
                    if (program.in_l3.Contains("男")) return "國一男-" + suffix;
                    if (program.in_l3.Contains("女")) return "國一女-" + suffix;
                    return "國一-" + suffix;

                default: return program.in_l2 + '-' + program.in_l3;
            }
        }


        private TPlayer NewPlayer(Item item)
        {
            var result = new TPlayer
            {
                in_current_org = item.getProperty("in_current_org", ""),
                in_short_org = item.getProperty("in_short_org", ""),
                in_team = item.getProperty("in_team", ""),
                in_index = item.getProperty("in_index", ""),
                in_creator_sno = item.getProperty("in_creator_sno", ""),

                in_name = item.getProperty("in_name", ""),
                in_sno = item.getProperty("in_sno", ""),
                in_gender = item.getProperty("in_gender", ""),
                in_paddle = item.getProperty("in_paddle", ""),
                in_waiting_list = item.getProperty("in_waiting_list", ""),

                isBack = false,
            };

            if (result.in_short_org == "")
            {
                result.in_short_org = result.in_current_org;
            }

            result.display = result.in_name;
            // if (result.in_paddle != "")
            // {
            //     result.display += "-" + result.in_paddle;
            // }
            if (result.in_waiting_list == "備取")
            {
                result.isBack = true;
                result.display += "(備)";
            }

            return result;
        }

        private void UpdProgram(TConfig cfg, TProgram program)
        {
            var sql = "UPDATE IN_MEETING_PROGRAM SET"
                + "  in_name2 = '" + program.in_name2 + "'"
                + ", in_register_count = '" + program.in_register_count + "'"
                + ", in_team_count = '" + program.in_team_count + "'"
                + ", in_event_count = '" + program.in_event_count + "'"
                + ", in_time_trial = '" + program.in_time_trial + "'"
                + ", in_schedule_mode = '" + program.in_schedule_mode + "'"
                + ", in_schedule_value = '" + program.in_schedule_value + "'"
                + ", in_battle_type = '" + program.in_battle_type + "'"
                + ", in_tree_key = '" + program.in_tree_key + "'"
                + " WHERE id = '" + program.id + "'";

            cfg.inn.applySQL(sql);
        }

        private TProgram MapProgramModel(TConfig cfg, Item item, string program_id)
        {
            var program = new TProgram
            {
                id = program_id,
                in_l1 = item.getProperty("in_l1", ""),
                in_l2 = item.getProperty("in_l2", ""),
                in_l3 = item.getProperty("in_l3", ""),
                in_battle_type = item.getProperty("in_battle_type", ""),
                in_tree_key = item.getProperty("in_tree_key", ""),
                in_team_count = item.getProperty("in_team_count", ""),
                in_water_way = item.getProperty("in_water_way", ""),
                in_time_trial = item.getProperty("in_time_trial", ""),
                in_schedule_value = item.getProperty("in_schedule_value", ""),
                teams = new List<TTeam>(),
            };

            if (program.in_water_way == "" || program.in_water_way == "0")
            {
                program.in_water_way = "4";
            }

            return program;
        }

        private void NewProgram(TConfig cfg, TProgram program)
        {
            var itmData = program.value;

            var itmNew = cfg.inn.newItem("In_Meeting_Program", "add");
            itmNew.setProperty("in_meeting", cfg.meeting_id);
            itmNew.setProperty("in_name", program.key);
            itmNew.setProperty("in_name2", program.in_name2);
            //itmNew.setProperty("in_name3", program.in_l3);
            //itmNew.setProperty("in_display", program.key);
            itmNew.setProperty("in_short_name", getShortName(program));

            itmNew.setProperty("in_l1", program.in_l1);
            itmNew.setProperty("in_l2", program.in_l2);
            itmNew.setProperty("in_l3", program.in_l3);
            itmNew.setProperty("in_register_count", program.in_register_count);
            itmNew.setProperty("in_team_count", program.in_team_count);
            itmNew.setProperty("in_event_count", program.in_event_count);

            itmNew.setProperty("in_time_trial", program.in_time_trial);
            itmNew.setProperty("in_schedule_mode", program.in_schedule_mode);
            itmNew.setProperty("in_schedule_value", program.in_schedule_value);
            itmNew.setProperty("in_schedule_show", program.in_schedule_value);

            itmNew.setProperty("in_battle_type", program.in_battle_type);
            itmNew.setProperty("in_tree_key", program.in_tree_key);

            //CopyItemValue(itmNew, itmData, "in_l1_sort");
            //CopyItemValue(itmNew, itmData, "in_l2_sort");
            //CopyItemValue(itmNew, itmData, "in_l3_sort");
            itmNew.setProperty("in_sort_order", GetShortOrder(program));

            var itmResult = itmNew.apply();
            if (!itmResult.isError() && itmResult.getResult() != "")
            {
                program.id = itmResult.getID();
            }
        }

        private Dictionary<string, string> MapProgramRegisterCount(TConfig cfg)
        {
            var result = new Dictionary<string, string>();

            var sql = @"
                SELECT 
	                in_l1
	                , in_l2
	                , in_l3
	                , COUNT(*) AS 'cnt' 
                FROM 
	                VU_FIGHT_MUSERS
                WHERE 
	                source_id = '{#meeting_id}' 
                GROUP BY 
	                in_l1
	                , in_l2
	                , in_l3
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var key = GetProgramKey(item);
                var cnt = item.getProperty("cnt", "0");
                if (result.ContainsKey(key)) continue;
                result.Add(key, cnt);
            }
            return result;

        }

        private Dictionary<string, string> MapProgramTeamCount(TConfig cfg)
        {
            var result = new Dictionary<string, string>();

            var sql = @"
                SELECT 
	                in_l1
	                , in_l2
	                , in_l3
	                , COUNT(*) AS 'cnt' 
                FROM 
	                VU_FIGHT_MTEAMS 
                WHERE 
	                source_id = '{#meeting_id}' 
                GROUP BY 
	                in_l1
	                , in_l2
	                , in_l3
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var key = GetProgramKey(item);
                var cnt = item.getProperty("cnt", "0");
                if (result.ContainsKey(key)) continue;
                result.Add(key, cnt);
            }
            return result;

        }

        private Item ProgramListFromMeetingUser(TConfig cfg)
        {
            var cond = cfg.program_id != ""
                ? "AND t2.id = '" + cfg.program_id + "'"
                : "";

            var sql = @"
                SELECT 
	                t1.*
	                , ROW_NUMBER() OVER (PARTITION BY t1.source_id ORDER BY t1.in_l1_sort, t1.in_l2_sort, t1.in_l3_sort) AS 'rn'
	                , t2.id AS 'program_id'
                FROM 
	                VU_FIGHT_PROGRAMS t1
                LEFT OUTER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.in_meeting = t1.source_id
	                AND ISNULL(t2.in_l1, '') = ISNULL(t1.in_l1, '')
	                AND ISNULL(t2.in_l2, '') = ISNULL(t1.in_l2, '')
	                AND ISNULL(t2.in_l3, '') = ISNULL(t1.in_l3, '')
                WHERE
	                t1.source_id = '{#meeting_id}'
                    {#cond}
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#cond}", cond);

            return cfg.inn.applySQL(sql);
        }

        private Item GetProgramItem(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                *
                FROM 
	                IN_MEETING_PROGRAM WITH(NOLOCK)
                WHERE
	                in_meeting = '{#meeting_id}'
	                AND in_l1 = N'{#in_l1}'
	                AND in_l2 = N'{#in_l2}'
	                AND in_l3 = N'{#in_l3}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", cfg.in_l1)
                .Replace("{#in_l2}", cfg.in_l2)
                .Replace("{#in_l3}", cfg.in_l3);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private void CopyItemValue(Item itmA, Item itmB, string propA, string propB = "")
        {
            if (propB == "")
            {
                itmA.setProperty(propA, itmB.getProperty(propA, ""));
            }
            else
            {
                itmA.setProperty(propA, itmB.getProperty(propB, ""));
            }
        }

        private string GetProgramKey(Item item)
        {
            var in_l1 = item.getProperty("in_l1", "");
            var in_l2 = item.getProperty("in_l2", "");
            var in_l3 = item.getProperty("in_l3", "");
            return string.Join("-", new List<string> { in_l1, in_l2, in_l3 });
        }

        private string GetShortOrder(TProgram program)
        {
            var rn = GetInt(program.value.getProperty("rn", "0")) * 100;
            return rn.ToString();
        }

        private string getShortName(TProgram program)
        {
            try
            {
                if (program.in_l3 == "") return "";

                var value = program.in_l3
                    .Replace("（", "(")
                    .Replace("）", ")")
                    .Replace("\n", "")
                    .Replace("\t", "")
                    .Replace("\r", "");

                value = value.Trim();

                var arr = value.Split('(');
                if (arr == null || arr.Length < 2) return program.in_l3;

                var result = arr[1].Replace(")", "")
                    .Replace("/", "");

                if (program.in_l2.Contains("一年級"))
                {
                    result += "_FG"; //first grade
                }

                return result;
            }
            catch
            {
                return program.in_l3;
            }
        }

        private List<TProgram> MapProgramList(TConfig cfg, Item items, Dictionary<string, string> regMap, Dictionary<string, string> teamMap)
        {
            var result = new List<TProgram>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var program = new TProgram
                {
                    id = item.getProperty("program_id", ""),
                    in_l1 = item.getProperty("in_l1", ""),
                    in_l2 = item.getProperty("in_l2", ""),
                    in_l3 = item.getProperty("in_l3", ""),
                    key = GetProgramKey(item),
                    value = item,
                };

                program.in_name2 = GetProgramName2(program);
                program.in_register_count = FindTeamCount(regMap, program.key);
                program.in_team_count = FindTeamCount(teamMap, program.key);

                ResetProgramBySchduleMode(cfg, program, GetInt(program.in_team_count));

                var team_count = GetInt(program.in_team_count);
                program.in_battle_type = GetBattleType(team_count);
                program.in_tree_key = GetTreeKey(team_count);

                result.Add(program);
            }

            return result;
        }

        private string GetBattleType(int team_count)
        {
            return "WaterWay-4";
        }

        private string GetTreeKey(int team_count)
        {
            if (team_count >= 13) return "schedule-13";
            //if (team_count >= 12) return "schedule-12";
            if (team_count >= 10) return "schedule-10";
            if (team_count >= 9) return "schedule-09";
            if (team_count >= 7) return "schedule-07";
            if (team_count >= 5) return "schedule-05";
            return "schedule-03";
        }

        private string GetEventNamesFromTreeKey(string treeKey)
        {
            switch (treeKey)
            {
                case "schedule-03": return "LR,FA";
                case "schedule-05": return "H1,H2,R,FB,FA";
                case "schedule-07": return "H1,H2,R,SB,SA,FB,FA";
                case "schedule-09": return "H1,H2,H3,R,SB,SA,FB,FA";
                case "schedule-10": return "H1,H2,H3,H4,R1,R2,SB,SA,FC,FB,FA";
                case "schedule-12": return "H1,H2,H3,H4,R1,R2,R3,R4,SB,SA,FB,FA";
                case "schedule-13": return "H1,H2,H3,H4,R1,R2,R3,R4,SD,SC,SB,SA,FD,FC,FB,FA";
                default: return "";
            }
        }

        private string GetScheduleName(string treeKey)
        {
            switch (treeKey)
            {
                case "F": return "schedule-03";
                case "FA": return "schedule-03";
                case "LR,FA": return "schedule-03";

                case "H1,H2,R,FB,FA": return "schedule-05";

                case "H1,H2,R,SB,SA,FB,FA": return "schedule-07";

                case "H1,H2,R1,R2,SB,SA,FB,FA": return "schedule-08";

                case "H1,H2,H3,R,SB,SA,FB,FA": return "schedule-09";

                case "H1,H2,H3,H4,R1,R2,SB,SA,FC,FB,FA": return "schedule-10";

                case "H1,H2,H3,H4,R1,R2,R3,R4,SB,SA,FB,FA": return "schedule-12";

                case "H1,H2,H3,H4,R1,R2,R3,R4,SD,SC,SB,SA,FD,FC,FB,FA": return "schedule-13";

                default: return "schedule-NA";
            }
        }

        private void ResetProgramBySchduleMode(TConfig cfg, TProgram program, int count)
        {
            var trial = "0";
            var value = "";

            if (count > cfg.timeTrialTeamCount)
            {
                trial = "1";
            }

            if (count >= 13)
            {
                value = "H1,H2,H3,H4,R1,R2,R3,R4,SD,SC,SB,SA,FD,FC,FB,FA";
            }
            else if (count >= 10)
            {
                value = "H1,H2,H3,H4,R1,R2,SB,SA,FC,FB,FA";
            }
            else if (count >= 9)
            {
                value = "H1,H2,H3,R,SB,SA,FB,FA";
            }
            else if (count >= 7)
            {
                value = "H1,H2,R,SB,SA,FB,FA";
            }
            else if (count >= 5)
            {
                value = "H1,H2,R,FB,FA";
            }
            else
            {
                value = "LR,FA";
            }

            program.in_time_trial = trial;
            program.in_schedule_mode = value;
            program.in_schedule_value = value;

            var evts = value.Split(',');
            var evt_cnt = evts.Length;
            //if (trial == "1") evt_cnt = evt_cnt + 1;

            program.in_event_count = evt_cnt.ToString();
        }

        private string FindTeamCount(Dictionary<string, string> map, string key)
        {
            if (map.ContainsKey(key)) return map[key];
            return "0";
        }

        private List<TImport> GetImportRows(TConfig cfg, string json)
        {
            try
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject<List<TImport>>(json);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private List<TEditRow> GetEditRows(TConfig cfg, string json)
        {
            try
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject<List<TEditRow>>(json);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private bool HasTwoTimeTrialEvents(Dictionary<string, string> map, string key)
        {
            if (!map.ContainsKey(key)) return false;

            var value = map[key];
            if (!string.IsNullOrWhiteSpace(value) && value == "Y") return true;
            return false;
        }

        private int GeMapIntValue(Dictionary<string, string> map, string key, int defVaule)
        {
            if (!map.ContainsKey(key)) return defVaule;

            var value = map[key];
            var result = GetInt(value);
            if (result <= 0) return defVaule;
            return result;
        }

        private Dictionary<string, string> VariableMap(TConfig cfg)
        {
            var result = new Dictionary<string, string>();

            var sql = @"
                SELECT
	                in_key
	                , in_value
                FROM 
	                IN_MEETING_VARIABLE WITH(NOLOCK) 
                WHERE 
	                source_id = '{#meeting_id}'
                ORDER BY 
	                sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_key = item.getProperty("in_key", "");
                var in_value = item.getProperty("in_value", "");
                if (result.ContainsKey(in_key)) continue;
                result.Add(in_key, in_value);
            }

            return result;
        }
        private class TEditRow
        {
            public int no { get; set; }
            public string id { get; set; }
            public string value { get; set; }
            public string prop { get; set; }
            public string time { get; set; }
            public string label { get; set; }
            public string itemType { get; set; }
        }

        private class TProgram
        {
            public string id { get; set; }
            public string key { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_name2 { get; set; }
            public string in_short_name { get; set; }

            public string in_register_count { get; set; }
            public string in_team_count { get; set; }
            public string in_event_count { get; set; }

            public string in_water_way { get; set; }
            public string in_time_trial { get; set; }
            public string in_schedule_mode { get; set; }
            public string in_schedule_value { get; set; }

            public string in_battle_type { get; set; }
            public string in_tree_key { get; set; }

            public Item value { get; set; }

            public List<TTeam> teams { get; set; }

            public List<TImport> importsA { get; set; }
            public List<TImport> importsB { get; set; }
        }

        private class TTeam
        {
            public string in_team_key { get; set; }
            public string in_current_org { get; set; }
            public string in_short_org { get; set; }
            public string in_team { get; set; }
            public string in_index { get; set; }
            public string in_creator_sno { get; set; }

            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_gender { get; set; }
            public string in_paddle { get; set; }
            public string in_waiting_list { get; set; }

            public string in_names { get; set; }
            public string in_team_players { get; set; }

            public List<TPlayer> player1 { get; set; }
            public List<TPlayer> player2 { get; set; }
        }

        private class TPlayer
        {
            public string in_current_org { get; set; }
            public string in_short_org { get; set; }
            public string in_team { get; set; }
            public string in_index { get; set; }
            public string in_creator_sno { get; set; }

            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_gender { get; set; }
            public string in_paddle { get; set; }
            public string in_waiting_list { get; set; }

            public string display { get; set; }

            public bool isBack { get; set; }
            public Item value { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }

            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_fight_day { get; set; }
            public string scene { get; set; }

            public Dictionary<string, string> variableMap { get; set; }
            public int waterWayCount { get; set; }
            public int timeTrialTeamCount { get; set; }
            public bool hasTwoTimeTrialEvents { get; set; }
        }

        private class TTree
        {
            public string name { get; set; }
            public string desc { get; set; }
            public int sort { get; set; }
        }

        private class TImport
        {
            public string serial { get; set; }
            public string day { get; set; }
            public string time { get; set; }
            public string sect { get; set; }
            public string item { get; set; }
            public string code { get; set; }
            public string teams { get; set; }
            public string note { get; set; }
            public int sort { get; set; }
        }

        private int GetInt(string value, int defV = 0)
        {
            if (value == "" || value == "0") return 0;
            int result = defV;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return defV;
        }
    }
}