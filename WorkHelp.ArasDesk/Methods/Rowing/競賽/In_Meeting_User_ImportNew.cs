﻿using Aras.IOM;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace WorkHelp.ArasDesk.Methods.Rowing.Common
{
    public class In_Meeting_User_ImportNew : Item
    {
        public In_Meeting_User_ImportNew(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 大會與會者匯入
*/

            System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_User_ImportNew";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", ""),
                need_rebuild_meeting = itmR.getProperty("need_rebuild_meeting", ""),
                draw_sheet_mode = itmR.getProperty("draw_sheet_mode", ""),
                scene = itmR.getProperty("scene", ""),
                import_all_muser = true,
            };

            switch (cfg.scene)
            {
                case "import":
                    Import(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Import(TConfig cfg, Item itmReturn)
        {
            var value = itmReturn.getProperty("value", "");
            if (value == "") throw new Exception("無匯入資料");

            var rows = MapList(cfg, value);
            if (rows == null || rows.Count == 0) throw new Exception("無匯入資料");

            var aml = "<AML><Item type='In_Meeting' action='get' id='" + cfg.meeting_id + "' select='*'/></AML>";
            cfg.itmMeeting = cfg.inn.applyAML(aml);

            MergeMeetingUsers(cfg, rows);

            var svy_model = GetMtSvy(cfg);
            RebuildInL1(cfg, svy_model.l1_id);
            RebuildInL2(cfg, svy_model.l2_id);
            RebuildInL3(cfg, svy_model.l3_id);
        }
        private void RebuildInL1(TConfig cfg, string id)
        {
            cfg.inn.applySQL("DELETE FROM IN_SURVEY_OPTION WHERE source_id = '" + id + "'");

            string sql = @"
                SELECT DISTINCT
                    in_l1
                FROM 
                    IN_MEETING_USER WITH(NOLOCK) 
                WHERE 
                    source_id = '{#meeting_id}'
                ORDER BY
                    in_l1
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            string in_selectoption = "";

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");
                int sort_order = (i + 1) * 100;

                Item itmNew = cfg.inn.newItem("In_Survey_Option", "merge");
                itmNew.setAttribute("where", "source_id='" + id + "' AND in_value=N'" + in_l1 + "'");
                itmNew.setProperty("source_id", id);
                itmNew.setProperty("in_label", in_l1);
                itmNew.setProperty("in_value", in_l1);
                itmNew.setProperty("sort_order", sort_order.ToString());
                itmNew = itmNew.apply();

                in_selectoption += "@" + in_l1;
            }

            cfg.inn.applySQL("UPDATE IN_SURVEY SET in_selectoption = N'" + in_selectoption + "' WHERE id = '" + id + "'");
        }

        private void RebuildInL2(TConfig cfg, string id)
        {
            cfg.inn.applySQL("DELETE FROM IN_SURVEY_OPTION WHERE source_id = '" + id + "'");

            string sql = @"
                SELECT DISTINCT
					LEFT(in_program_no,2) AS 'code'
                    , in_l1
                    , in_l2
                FROM 
                    IN_MEETING_USER WITH(NOLOCK) 
                WHERE 
                    source_id = '{#meeting_id}'
                ORDER BY
                    LEFT(in_program_no,2)
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                int sort_order = (i + 1) * 100;

                Item itmNew = cfg.inn.newItem("In_Survey_Option", "merge");
                itmNew.setAttribute("where", "source_id='" + id + "' AND in_filter = N'" + in_l1 + "' AND in_value = N'" + in_l2 + "'");
                itmNew.setProperty("source_id", id);
                itmNew.setProperty("in_filter", in_l1);
                itmNew.setProperty("in_label", in_l2);
                itmNew.setProperty("in_value", in_l2);
                itmNew.setProperty("sort_order", sort_order.ToString());
                itmNew = itmNew.apply();
            }
        }

        private void RebuildInL3(TConfig cfg, string id)
        {
            cfg.inn.applySQL("DELETE FROM IN_SURVEY_OPTION WHERE source_id = '" + id + "'");

            string sql = @"
                SELECT DISTINCT
                    in_program_no
                    , in_l1
                    , in_l2
                    , in_l3
                FROM 
                    IN_MEETING_USER WITH(NOLOCK) 
                WHERE 
                    source_id = '{#meeting_id}'
                ORDER BY
                    in_program_no
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                string in_l3 = item.getProperty("in_l3", "");
                int sort_order = (i + 1) * 100;

                Item itmNew = cfg.inn.newItem("In_Survey_Option", "merge");
                itmNew.setAttribute("where", "source_id='" + id + "' AND in_grand_filter = N'" + in_l1 + "' AND in_filter = N'" + in_l2 + "' AND in_value = N'" + in_l3 + "'");
                itmNew.setProperty("source_id", id);
                itmNew.setProperty("in_grand_filter", in_l1);
                itmNew.setProperty("in_filter", in_l2);
                itmNew.setProperty("in_label", in_l3);
                itmNew.setProperty("in_value", in_l3);
                itmNew.setProperty("sort_order", sort_order.ToString());
                itmNew.apply();
            }
        }

        private TMtSvy GetMtSvy(TConfig cfg)
        {
            TMtSvy result = new TMtSvy { l1_id = "", l2_id = "", l3_id = "" };

            string sql = @"
                SELECT
	                t2.id
	                , t2.in_property
	                , t2.in_questions
                FROM
	                IN_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN
	                IN_SURVEY t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t2.in_property IN ('in_l1', 'in_l2', 'in_l3')
                ORDER BY
	                t2.in_property 
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_property = item.getProperty("in_property", "");
                switch (in_property)
                {
                    case "in_l1": result.l1_id = id; break;
                    case "in_l2": result.l2_id = id; break;
                    case "in_l3": result.l3_id = id; break;
                }
            }
            return result;
        }

        private void MergeMeetingUsers(TConfig cfg, List<TRow> rows)
        {
            //刪除舊與會者資料
            var sql_del = "DELETE FROM IN_MEETING_USER WHERE source_id = '{#meeting_id}'";
            sql_del = sql_del.Replace("{#meeting_id}", cfg.meeting_id);
            cfg.inn.applySQL(sql_del);

            for (int i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                //與會者資訊
                Item applicant = NewMUser(cfg, row);
                //建立與會者
                applicant.apply("add");
            }

            //重算正取人數
            cfg.itmMeeting.apply("In_Update_MeetingOnMUEdit");
        }

        private Item NewMUser(TConfig cfg, TRow row)
        {
            var applicant = cfg.inn.newItem("In_Meeting_User");

            applicant.setProperty("source_id", cfg.meeting_id);

            //單位排序
            applicant.setProperty("in_city_no", row.in_city_no);

            //所屬單位
            applicant.setProperty("in_current_org", row.in_current_org);

            //單位簡稱
            applicant.setProperty("in_short_org", row.in_current_org);

            //姓名
            applicant.setProperty("in_name", row.in_name);

            //身分證號
            applicant.setProperty("in_sno", row.in_player_no);

            //性別
            applicant.setProperty("in_gender", GetGender(cfg, row));

            //西元生日
            applicant.getProperty("in_birth", "1990-01-01");

            //電子信箱
            applicant.setProperty("in_email", "na@na.n");

            //手機號碼
            applicant.setProperty("in_tel", "");

            //協助報名者姓名
            applicant.setProperty("in_creator", "lwu001");

            //協助報名者身分號
            applicant.setProperty("in_creator_sno", "lwu001");

            //所屬群組
            applicant.setProperty("in_group", "原創"); //F103277376測試者


            //預設身分均為選手
            applicant.setProperty("in_role", "player");

            //正取
            applicant.setProperty("in_note_state", "official");

            //組別
            applicant.setProperty("in_gameunit", row.in_l2 + "-" + row.in_l3);

            //組名
            applicant.setProperty("in_section_name", row.in_l1 + "-" + row.in_l2 + "-" + row.in_l3);

            //組別編號
            applicant.setProperty("in_program_no", row.in_program_no);

            //捐款金額
            applicant.setProperty("in_expense", "0");

            //隊伍編號
            applicant.setProperty("in_index", row.in_index);

            //競賽項目
            applicant.setProperty("in_l1", row.in_l1);

            //競賽組別
            applicant.setProperty("in_l2", row.in_l2);

            //競賽分級
            applicant.setProperty("in_l3", row.in_l3);
            //選手編號
            applicant.setProperty("in_player_no", row.in_player_no);
            //選手編號
            applicant.setProperty("in_player_key", row.in_player_key);

            //非實名制
            var in_mail = row.in_player_no + System.DateTime.Now.ToString("yyyyMMdd_HHmmss_fff");
            applicant.setProperty("in_mail", in_mail);

            //報名時間
            applicant.setProperty("in_regdate", System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));

            //繳費單號
            applicant.setProperty("in_paynumber", "");

            return applicant;
        }

        private string GetGender(TConfig cfg, TRow row)
        {
            string sect = row.in_l1 + row.in_l2;
            return GetGender(cfg, sect);
        }

        private string GetGender(TConfig cfg, string value)
        {
            if (value.Contains("男")) return "男";
            if (value.Contains("女")) return "女";
            return "";
        }

        private List<TRow> MapList(TConfig cfg, string value)
        {
            try
            {
                var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TRow>>(value);
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private class TRow
        {
            public string no { get; set; }
            public string in_program_no { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_city_no { get; set; }
            public string in_current_org { get; set; }
            public string in_player_no { get; set; }
            public string in_name { get; set; }
            public string in_index { get; set; }
            public string in_player_key { get; set; }
        }

        private class TMtSvy
        {
            public string l1_id { get; set; }
            public string l2_id { get; set; }
            public string l3_id { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string need_rebuild_meeting { get; set; }
            public string draw_sheet_mode { get; set; }
            public string scene { get; set; }
            public bool import_all_muser { get; set; }

            public Item itmMeeting { get; set; }
        }

        private string GetFightDay(TConfig cfg, string value)
        {
            string code = value;
            if (code.Length <= 4)
            {
                code = DateTime.Now.Year + code.PadLeft(4, '0');
            }
            if (code.Length != 8)
            {
                return "";
            }

            string y = code.Substring(0, 4);
            string m = code.Substring(4, 2);
            string d = code.Substring(6, 2);
            return y + "-" + m + "-" + d;
        }

        private int GetInt(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}