﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using Newtonsoft.Json.Linq;
using WorkHelp.ArasDesk.Methods.PLMCTA.Common;

namespace WorkHelp.ArasDesk.Methods.Rowing.Fight
{
    public class in_meeting_program_bulletin_print : Item
    {
        public in_meeting_program_bulletin_print(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 
                    名次列印
                輸出: 
                    docx
                重點: 
                    - 需 Xceed.Document.NET.dll
                    - 需 Xceed.Words.NET.dll
                    - 請於 method-config.xml 引用 DLL
                日期: 
                    2020-11-18: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_program_bulletin_print";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            string meeting_id = itmR.getProperty("meeting_id", "");
            string in_date = itmR.getProperty("in_date", "");
            string mode = itmR.getProperty("mode", "");

            if (meeting_id == "")
            {
                itmR.setProperty("error_message", "賽事 id 不得為空白");
                return itmR;
            }

            //取得賽事資料
            Item itmMeeting = GetMeeting(CCO, strMethodName, inn, itmR);
            itmR.setProperty("in_title", itmMeeting.getProperty("in_title", ""));

            //取得名次資料
            Item itmRanks = GetRanks(CCO, strMethodName, inn, itmMeeting);

            //轉換名次資料
            Dictionary<string, TTable> map = MapRankList(CCO, strMethodName, inn, itmMeeting, itmRanks);
            //ClearRanks(map);

            if (map.Count > 0)
            {
                map.Last().Value.IsEndTable = true;

                if (mode == "")
                {
                    AppendDays(CCO, strMethodName, inn, meeting_id, itmR);
                    AppendProgramMenu(CCO, strMethodName, inn, meeting_id, in_date, itmR);
                    AppendTables(CCO, strMethodName, inn, map, itmR);
                }
                else if (mode == "word")
                {
                    ExportWord(CCO, strMethodName, inn, itmMeeting, map, itmR);
                    AppendSiteUrl(CCO, strMethodName, inn, itmR);
                }
                else if (mode == "pdf")
                {
                    ExportPDF(CCO, strMethodName, inn, itmMeeting, map, itmR);
                    AppendSiteUrl(CCO, strMethodName, inn, itmR);
                }
            }

            return itmR;
        }

        private void AppendSiteUrl(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            var url = " ";
            var sql = "SELECT id, in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = 'app_url'";
            var itmV = inn.applySQL(sql);
            if (!itmV.isError() && itmV.getResult() != "")
            {
                url = itmV.getProperty("in_value", "");
            }
            itmReturn.setProperty("inn_app_url", url);
        }

        //組別選單
        private void AppendDays(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id, Item itmReturn)
        {
            string qry_date = itmReturn.getProperty("in_date", "");

            Item itmEmpty = inn.newItem();
            itmEmpty.setType("inn_date");
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("label", "請選擇");
            itmReturn.addRelationship(itmEmpty);

            // string sql = "SELECT DISTINCT in_date_key FROM IN_MEETING_ALLOCATION WITH(NOLOCK) WHERE in_meeting = '" + meeting_id + "' ORDER BY in_date_key";
            // Item itmDays = inn.applySQL(sql);
            // int count = itmDays.getItemCount();

            // for (int i = 0; i < count; i++)
            // {
            //     var itmDay = itmDays.getItemByIndex(i);
            //     string in_date_key = itmDay.getProperty("in_date_key", "");
            //     string selected = in_date_key == qry_date ? "selected" : "";

            //     itmDay.setType("inn_date");
            //     itmDay.setProperty("selected", selected);
            //     itmDay.setProperty("value", in_date_key);
            //     itmDay.setProperty("label", in_date_key);
            //     itmReturn.addRelationship(itmDay);
            // }
        }

        //組別選單
        private void AppendProgramMenu(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id, string in_date, Item itmReturn)
        {
            // if (in_date == "")
            // {
            //     return;
            // }

            // string sql = @"
            //     SELECT 
            //         t2.id
            //         , t2.in_name3 + ' (' + CAST(in_rank_count AS VARCHAR) + ')' AS 'program_display'
            //     FROM
            //         IN_MEETING_ALLOCATION t1 WITH(NOLOCK)
            //     INNER JOIN
            //         IN_MEETING_PROGRAM t2 WITH(NOLOCK)
            //         ON t2.id = t1.in_program
            //     WHERE
            //         t1.in_meeting = '{#meeting_id}'
            //         AND t1.in_date_key = '{#in_date}'
            //     ORDER BY
            //         t2.in_medal_sort
            // ";

            // sql = sql.Replace("{#meeting_id}", meeting_id)
            //     .Replace("{#in_date}", in_date);

            // //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            // Item items = inn.applySQL(sql);

            Item itmEmpty = inn.newItem();
            itmEmpty.setType("inn_program");
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("label", "請選擇");
            itmReturn.addRelationship(itmEmpty);

            // int count = items.getItemCount();

            // for (int i = 0; i < count; i++)
            // {
            //     var item = items.getItemByIndex(i);
            //     item.setType("inn_program");
            //     item.setProperty("value", item.getProperty("id", ""));
            //     item.setProperty("label", item.getProperty("program_display", ""));
            //     itmReturn.addRelationship(item);
            // }
        }

        private void ClearRanks(Dictionary<string, TTable> map)
        {
            foreach (var kv in map)
            {
                var table = kv.Value;
                foreach (var kv2 in table.Rows)
                {
                    var row = kv2.Value;
                    switch (row.max_rank)
                    {
                        case 1:
                            row.Rank2.Text = "";
                            row.Rank3.Text = "";
                            row.Rank4.Text = "";
                            row.Rank5.Text = "";
                            row.Rank6.Text = "";
                            row.Rank7.Text = "";
                            row.Rank8.Text = "";
                            break;

                        case 2:
                            row.Rank3.Text = "";
                            row.Rank4.Text = "";
                            row.Rank5.Text = "";
                            row.Rank6.Text = "";
                            row.Rank7.Text = "";
                            row.Rank8.Text = "";
                            break;

                        case 3:
                            row.Rank4.Text = "";
                            row.Rank5.Text = "";
                            row.Rank6.Text = "";
                            row.Rank7.Text = "";
                            row.Rank8.Text = "";
                            break;

                        case 4:
                            row.Rank5.Text = "";
                            row.Rank6.Text = "";
                            row.Rank7.Text = "";
                            row.Rank8.Text = "";
                            break;

                        case 5:
                            row.Rank6.Text = "";
                            row.Rank7.Text = "";
                            row.Rank8.Text = "";
                            break;

                        case 6:
                            row.Rank7.Text = "";
                            row.Rank8.Text = "";
                            break;

                        default:
                            row.Rank7.Text = "";
                            row.Rank8.Text = "";
                            break;
                    }
                }
            }
        }

        #region 匯出 Word

        /// <summary>
        /// 匯出 Word
        /// </summary>
        private void ExportWord(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMeeting, Dictionary<string, TTable> map, Item itmReturn)
        {
            TExport export = GetExportInfo(CCO, strMethodName, inn, itmMeeting);

            TConfig cfg = new TConfig
            {
                FontName = "標楷體",
                FontSize = 11,
                RowCount = 1 + 20,
                BodyRowCount = 20,
                RowStart = 1,
                ColStart = 0,
                CurrentRow = 1,
                Fields = new List<TField>(),
                MeetingTitle = itmMeeting.getProperty("in_title", ""),
            };

            cfg.Fields.Add(new TField { Title = "第二階", Property = "left_title", Format = "" });
            cfg.Fields.Add(new TField { Title = "第三階", Property = "row_title", Format = "" });
            cfg.Fields.Add(new TField { Title = "第一名", Property = "rank1", Format = "" });
            cfg.Fields.Add(new TField { Title = "第二名", Property = "rank2", Format = "" });
            cfg.Fields.Add(new TField { Title = "第三名", Property = "rank3", Format = "" });
            cfg.Fields.Add(new TField { Title = "第四名", Property = "rank4", Format = "" });
            cfg.Fields.Add(new TField { Title = "第五名", Property = "rank5", Format = "" });
            cfg.Fields.Add(new TField { Title = "第六名", Property = "rank6", Format = "" });
            cfg.Fields.Add(new TField { Title = "第七名", Property = "rank7", Format = "" });
            cfg.Fields.Add(new TField { Title = "第八名", Property = "rank8", Format = "" });

            //載入模板
            using (var doc = Xceed.Words.NET.DocX.Load(export.Source))
            {
                foreach (var kv in map)
                {
                    var table = kv.Value;

                    if (table.Rows.Count == 0)
                    {
                        continue;
                    }

                    if (table.Rows.Count > cfg.BodyRowCount)
                    {
                        throw new Exception("級數超過預設值，請調整 Word 樣板，並修改參數");
                    }

                    AppendWordTable(inn, doc, cfg, table);
                }

                doc.Tables[0].Remove();

                //doc.RemoveParagraphAt(0);
                doc.RemoveParagraphAt(0);

                doc.SaveAs(export.File);

            }

            itmReturn.setProperty("xls_name", export.Url);
        }

        private void AppendWordTable(Innovator inn, Xceed.Words.NET.DocX doc, TConfig cfg, TTable table)
        {
            cfg.CurrentRow = cfg.RowStart;

            //插入標題段落
            var p = doc.InsertParagraph();

            p.Append(cfg.MeetingTitle)
               .Font(new Xceed.Document.NET.Font(cfg.FontName))
               .FontSize(25)
               .Bold()
               .Spacing(10);
            //.Color(System.Drawing.Color.Red)
            p.Alignment = Xceed.Document.NET.Alignment.center;

            p = doc.InsertParagraph();
            p.Append(table.Title)
               .Font(new Xceed.Document.NET.Font(cfg.FontName))
               .FontSize(12);
            p.Alignment = Xceed.Document.NET.Alignment.left;

            //取得模板表格
            var template_table = doc.Tables[0];

            var docTable = doc.InsertTable(template_table);

            //資料容器
            Item itmContainer = inn.newItem();

            foreach (var rkv in table.Rows)
            {
                var row = rkv.Value;
                itmContainer.setProperty("left_title", table.LeftTitle_Word);
                itmContainer.setProperty("row_title", row.Title_Word);
                itmContainer.setProperty("rank1", ShowName2(row.Rank1.Text));
                itmContainer.setProperty("rank2", ShowName2(row.Rank2.Text));
                itmContainer.setProperty("rank3", ShowName2(row.Rank3.Text));
                itmContainer.setProperty("rank4", ShowName2(row.Rank4.Text));
                itmContainer.setProperty("rank5", ShowName2(row.Rank5.Text));
                itmContainer.setProperty("rank6", ShowName2(row.Rank6.Text));

                SetCellValue(docTable, cfg, itmContainer);

                cfg.CurrentRow++;
            }

            for (int i = cfg.RowCount - 1; i >= cfg.CurrentRow; i--)
            {
                docTable.RemoveRow(i);
            }

            if (!table.IsEndTable)
            {
                var pend = doc.InsertParagraph();
                pend.InsertPageBreakAfterSelf();
            }
        }

        //設定資料表格
        private void SetCellValue(Xceed.Document.NET.Table table, TConfig cfg, Item item)
        {
            int wsRow = cfg.CurrentRow;
            int wsCol = cfg.ColStart;
            int fdCount = cfg.Fields.Count;

            for (int i = 0; i < fdCount; i++)
            {
                var f = cfg.Fields[i];
                var x = wsCol + i;
                var v = GetCellValue(f, item);

                table.Rows[wsRow].Cells[x].Paragraphs.First().Append(v).Font(cfg.FontName).FontSize(cfg.FontSize);
            }
        }

        //設定資料列
        private string GetCellValue(TField field, Item item)
        {
            if (field.Property == "")
            {
                return "";
            }

            return item.getProperty(field.Property);
        }

        /// <summary>
        /// 取得匯出設定資訊
        /// </summary>
        private TExport GetExportInfo(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMeeting)
        {
            Item itmPath = GetXlsPaths(CCO, strMethodName, inn, "rank_overall_path");

            //比賽名稱
            string in_title = itmMeeting.getProperty("in_title", "");

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".docx";
            string doc_name = in_title + "_成績總表_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");

            string doc_file = export_path + "\\" + doc_name + ext_name;
            string doc_url = doc_name + ext_name;

            return new TExport
            {
                Source = itmPath.getProperty("template_path", ""),
                File = doc_file,
                Url = doc_url,
            };
        }

        #endregion 匯出 Word

        #region 匯出 PDF

        /// <summary>
        /// 匯出 PDF
        /// </summary>
        private void ExportPDF(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMeeting, Dictionary<string, TTable> map, Item itmReturn)
        {
            TConfig cfg = new TConfig
            {
                WsRow = 4,
                WsCol = 1,
                Heads = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                SheetCount = 1,
                Sheets = new List<string>(),
                FontName = "標楷體",
                FontSize = 11,
                Fields = new List<TField>(),
                MeetingTitle = itmMeeting.getProperty("in_title", ""),
            };

            TExport export = GetPDFExportInfo(CCO, strMethodName, inn, itmMeeting);

            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();
            workbook.LoadFromFile(export.Source);
            Spire.Xls.Worksheet sheetTemplate = workbook.Worksheets[0];

            foreach (var kv in map)
            {
                var table = kv.Value;

                if (table.Rows.Count == 0)
                {
                    continue;
                }

                AppendSheets(cfg, CCO, strMethodName, inn, workbook, sheetTemplate, table);
            }

            //移除樣板 sheet
            sheetTemplate.Remove();
            //輸出 pdf
            workbook.SaveToFile(export.File.Replace("xlsx", "pdf"), Spire.Xls.FileFormat.PDF);

            itmReturn.setProperty("xls_name", export.Url.Replace("xlsx", "pdf"));
        }

        private void AppendSheets(TConfig cfg
            , Aras.Server.Core.CallContext CCO
            , string strMethodName
            , Innovator inn
            , Spire.Xls.Workbook workbook
            , Spire.Xls.Worksheet sheetTemplate
            , TTable table)
        {

            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = table.Title;

            sheet.Range["A1"].Text = table.Head;
            sheet.Range["A1"].Style.Font.FontName = cfg.FontName;

            sheet.Range["A2"].Text = table.Title;
            sheet.Range["A2"].Style.Font.FontName = cfg.FontName;

            int wsRow = cfg.WsRow;
            int wsCol = cfg.WsCol;

            List<TField> fields = new List<TField>();
            //fields.Add(new TField { Title = "第二階", Property = "left_title", Format = "" });
            fields.Add(new TField { Title = "第三階", Property = "row_title", Format = "" });
            fields.Add(new TField { Title = "冠軍", Property = "rank1", Format = "" });
            fields.Add(new TField { Title = "亞軍", Property = "rank2", Format = "" });
            fields.Add(new TField { Title = "季軍", Property = "rank3", Format = "" });
            fields.Add(new TField { Title = "第四名", Property = "rank4", Format = "" });
            fields.Add(new TField { Title = "第五名", Property = "rank5", Format = "" });
            fields.Add(new TField { Title = "第六名", Property = "rank6", Format = "" });

            int row_start = wsRow;
            int row_end = wsRow + table.Rows.Count;

            //資料容器
            Item itmContainer = inn.newItem();

            foreach (var rkv in table.Rows)
            {
                var row = rkv.Value;
                //itmContainer.setProperty("left_title", table.LeftTitle_Word);
                itmContainer.setProperty("row_title", row.Title_Word);
                itmContainer.setProperty("rank1", row.Rank1.Text);
                itmContainer.setProperty("rank2", row.Rank2.Text);
                itmContainer.setProperty("rank3", row.Rank3.Text);
                itmContainer.setProperty("rank4", row.Rank4.Text);
                itmContainer.setProperty("rank5", row.Rank5.Text);
                itmContainer.setProperty("rank6", row.Rank6.Text);

                SetItemCell(sheet, cfg, wsRow, wsCol, itmContainer, fields);

                wsRow++;
            }

            string pos_1 = "A" + row_start;
            string pos_2 = "A" + (row_end - 1);

            Spire.Xls.CellRange range = sheet.Range[pos_1 + ":" + pos_2];
            range.Merge();
            range.Text = table.LeftTitle_Word;
            range.Style.Font.FontName = cfg.FontName;

            range.Style.Borders[Spire.Xls.BordersLineType.EdgeTop].LineStyle = Spire.Xls.LineStyleType.Thin;
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeRight].LineStyle = Spire.Xls.LineStyleType.Thin;
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeLeft].LineStyle = Spire.Xls.LineStyleType.Thin;

        }

        //設定物件與資料列
        private void SetItemCell(Spire.Xls.Worksheet sheet, TConfig cfg, int wsRow, int wsCol, Item item, List<TField> fields)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                var value = "";
                var position = cfg.Heads[wsCol + i] + wsRow;

                if (field.getValue != null)
                {
                    value = field.getValue(cfg, item, field);
                }
                else if (field.Property != "")
                {
                    value = item.getProperty(field.Property, "");
                }

                Spire.Xls.CellRange range = sheet.Range[position];
                SetBodyCell(range, cfg, value, field.Property);
            }
        }

        //設定資料列
        private void SetBodyCell(Spire.Xls.CellRange range, TConfig cfg, string value = "", string format = "")
        {
            switch (format)
            {
                case "center":
                    range.Text = value;
                    range.Style.Font.FontName = cfg.FontName;
                    break;

                case "yyyy/MM/dd":
                case "yyyy/MM/dd HH:mm":
                    range.Text = GetDateTimeVal(value, format);
                    range.Style.Font.FontName = cfg.FontName;
                    break;

                case "$ #,##0":
                    range.Text = value;
                    range.Style.Font.FontName = cfg.FontName;
                    break;

                case "text":
                    range.Text = value;
                    range.Style.Font.FontName = cfg.FontName;
                    break;

                default:
                    range.Text = value;
                    range.Style.Font.FontName = cfg.FontName;
                    break;
            }

            range.Style.Borders[Spire.Xls.BordersLineType.EdgeTop].LineStyle = Spire.Xls.LineStyleType.Thin;
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeRight].LineStyle = Spire.Xls.LineStyleType.Thin;
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeLeft].LineStyle = Spire.Xls.LineStyleType.Thin;
        }

        /// <summary>
        /// 取得匯出設定資訊
        /// </summary>
        private TExport GetPDFExportInfo(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMeeting)
        {
            Item itmPath = GetXlsPaths(CCO, strMethodName, inn, "rank_pdf_path");

            //比賽名稱
            string in_title = itmMeeting.getProperty("in_title", "");

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".xlsx";
            string doc_name = in_title + "_成績總表_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");

            string doc_file = export_path + "\\" + doc_name + ext_name;
            string doc_url = doc_name + ext_name;

            return new TExport
            {
                Source = itmPath.getProperty("template_path", ""),
                File = doc_file,
                Url = doc_url,
            };
        }

        #endregion 匯出 PDF

        //附加表格
        private void AppendTables(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Dictionary<string, TTable> map, Item itmReturn)
        {
            StringBuilder builder = new StringBuilder();

            foreach (var kv in map)
            {
                var table = kv.Value;
                if (table.Rows.Count > 0)
                {
                    AppendTable(builder, kv.Value);
                }
            }

            itmReturn.setProperty("inn_tables", builder.ToString());
        }

        //附加分組名單
        private void AppendTable(StringBuilder builder, TTable table)
        {
            builder.Append("<div class=\"showsheet_" + table.Name + " page-block\">");
            builder.Append("  <h2 class=\"text-center\">" + table.Head + "</h2>");
            builder.Append("  <h4 class=\"sheet-title\">" + table.Title + "<span> </span> </h4>");

            builder.Append("  <table id=\"" + table.Name + "\" data-toggle='table'  class=\"table table-hover table-bordered table-rwd rwd\">");

            AppendRankTable(builder, table);

            builder.Append("  </table>");

            builder.Append("</div>");
        }

        //附加名次
        private void AppendRankTable(StringBuilder builder, TTable table)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("  <tr>");
            head.Append("    <th class='text-center mailbox-subject search-field' colspan='2'>組別／級別／名次</th>");
            head.Append("    <th class='text-center mailbox-subject search-field'>第一名</th>");
            head.Append("    <th class='text-center mailbox-subject search-field'>第二名</th>");
            head.Append("    <th class='text-center mailbox-subject search-field'>第三名</th>");
            head.Append("    <th class='text-center mailbox-subject search-field'>第四名</th>");
            head.Append("    <th class='text-center mailbox-subject search-field'>第五名</th>");
            head.Append("    <th class='text-center mailbox-subject search-field'>第六名</th>");
            head.Append("    <th class='text-center mailbox-subject search-field'>第七名</th>");
            head.Append("    <th class='text-center mailbox-subject search-field'>第八名</th>");
            head.Append("  </tr>");
            head.Append("</thead>");

            body.Append("<tbody>");
            foreach (var kv in table.Rows)
            {
                var row = kv.Value;

                body.Append("  <tr>");
                if (row.No == 1)
                {
                    body.Append("    <td rowspan='" + table.Rows.Count + "' class='text-center' style='width: 10px'>" + table.LeftTitle_Html + "</td>");
                }
                body.Append("    <td class='text-center' style='width: 65px'>" + row.Title_Html + "</td>");
                body.Append("    <td class='text-center' style='width: 120px'>" + ShowName(row.Rank1) + "</td>");
                body.Append("    <td class='text-center' style='width: 120px'>" + ShowName(row.Rank2) + "</td>");
                body.Append("    <td class='text-center' style='width: 120px'>" + ShowName(row.Rank3) + "</td>");
                body.Append("    <td class='text-center' style='width: 120px'>" + ShowName(row.Rank4) + "</td>");
                body.Append("    <td class='text-center' style='width: 120px'>" + ShowName(row.Rank5) + "</td>");
                body.Append("    <td class='text-center' style='width: 120px'>" + ShowName(row.Rank6) + "</td>");
                body.Append("    <td class='text-center' style='width: 120px'>" + ShowName(row.Rank7) + "</td>");
                body.Append("    <td class='text-center' style='width: 120px'>" + ShowName(row.Rank8) + "</td>");
                body.Append("  </tr>");

            }
            body.Append("</tbody>");

            builder.Append(head);
            builder.Append(body);
        }

        private string ShowName(TRank rank)
        {
            if (rank == null) return "";
            if (rank.Text == null) return "";
            if (rank.Org == null) return "";

            return rank.Org + "<br>"
                + rank.Text.Replace("(備)", "")
                .Replace("-S", "").Replace("-B", "")
                .Replace("-1", "").Replace("-2", "")
                .Replace("-3", "").Replace("-4", "")
                .Replace("-5", "").Replace("-6", "")
                .Replace("-7", "").Replace("-8", "")
                .Replace("-舵手", "");
        }

        private string ShowName(string text)
        {
            if (text == null) return "";
            return text.Replace("(備)", "")
                .Replace("-S", "").Replace("-B", "")
                .Replace("-1", "").Replace("-2", "")
                .Replace("-3", "").Replace("-4", "")
                .Replace("-5", "").Replace("-6", "")
                .Replace("-7", "").Replace("-8", "")
                .Replace("-舵手", "")
                .Replace("(", "<br>(");
        }

        private string ShowName2(string text)
        {
            if (text == null) return "";

            return text.Replace("(備)", "")
                .Replace("-S", "").Replace("-B", "")
                .Replace("-1", "").Replace("-2", "")
                .Replace("-3", "").Replace("-4", "")
                .Replace("-5", "").Replace("-6", "")
                .Replace("-7", "").Replace("-8", "")
                .Replace("-舵手", "")
                .Replace("(", Environment.NewLine + "(");
        }

        /// <summary>
        /// 取得賽事資訊
        /// </summary>
        private Item GetMeeting(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string sql = "";

            string meeting_id = itmReturn.getProperty("meeting_id", "");

            sql = @"
                SELECT
                    t1.id
                    , t1.in_title
                FROM
                    IN_MEETING t1 WITH(NOLOCK)
                WHERE
                    t1.id = '{#meeting_id}'
                ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        /// <summary>
        /// 取得樣板與匯出路徑
        /// </summary>
        private Item GetXlsPaths(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string in_name)
        {
            Item itmResult = inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
                SELECT 
                    t2.in_name AS 'variable'
                    , t1.in_name
                    , t1.in_value 
                FROM 
                    In_Variable_Detail t1 WITH(NOLOCK)
                INNER JOIN 
                    In_Variable t2 WITH(NOLOCK) 
                    ON t2.ID = t1.SOURCE_ID 
                WHERE 
                    t2.in_name = N'meeting_excel'  
                    AND t1.in_name IN (N'export_path', N'{#in_name}')
            ";

            sql = sql.Replace("{#in_name}", in_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = inn.applySQL(sql);

            if (items.isError() || items.getItemCount() < 1)
            {
                throw new Exception("未設定樣板與匯出路徑 _# " + in_name);
            }

            for (int i = 0; i < items.getItemCount(); i++)
            {
                Item item = items.getItemByIndex(i);
                string xls_name = item.getProperty("in_name", "");
                string xls_value = item.getProperty("in_value", "");

                if (xls_name == "export_path")
                {
                    itmResult.setProperty("export_path", xls_value);
                }
                else
                {
                    itmResult.setProperty("template_path", xls_value);
                }
            }

            return itmResult;
        }

        /// <summary>
        /// 取得名次資訊
        /// </summary>
        private Item GetRanks(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMeeting)
        {
            string meeting_id = itmMeeting.getProperty("id", "");

            string sql = @"
                SELECT
                    t3.id AS 'program_id'     
                    , t3.in_short_name AS 'in_short_name'
                    , t3.in_l1
                    , t3.in_l2
                    , t3.in_l3
                    , t3.in_team_count
                    , t3.in_register_count AS 'in_rank_count'
                    , t3.in_name           AS 'program_name2'
                    , t1.in_current_org
                    , t1.in_current_org   AS 'in_short_org'
                    , t1.in_current_org   AS 'map_short_org'
                    , t1.in_current_org   AS 'map_org_name'
                    , t1.in_team
                    , t1.in_name
                    , t1.in_sign_no
                    , t1.in_sno
                    , t1.in_rank AS 'in_final_rank'
                    , t1.in_rank AS 'in_show_rank'
                FROM
                    IN_MEETING_PTEAM t1
                INNER JOIN
                    IN_MEETING_PROGRAM t3 WITH(NOLOCK)
                    ON t3.id = t1.source_id
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                    AND ISNULL(t1.in_rank, 0) > 0
                ORDER BY
                    t3.in_sort_order
                    , t1.in_stuff_b1
                    , t1.in_rank
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            return inn.applySQL(sql);
        }

        /// <summary>
        /// 取得名次資訊
        /// </summary>
        private Dictionary<string, TTable> MapRankList(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMeeting, Item items)
        {
            string meeting_title = itmMeeting.getProperty("in_title", "");

            int count = items.getItemCount();

            Dictionary<string, TTable> map = new Dictionary<string, TTable>();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                TTable table = AddAndGetTable(map, meeting_title, item);

                AddRow(inn, table, item);
            }

            return map;
        }

        /// <summary>
        /// 附加並回傳 Table
        /// </summary>
        private TTable AddAndGetTable(Dictionary<string, TTable> map, string meeting_title, Item item)
        {
            string in_l1 = item.getProperty("in_l1", "");
            string in_l2 = item.getProperty("in_l2", "");
            string title = ClearLv2(in_l2);

            var key = "";
            var type = TableEnum.None;

            switch (in_l1)
            {
                case "團體組":
                    key = in_l1;
                    type = TableEnum.Multiple;
                    break;

                case "個人組":
                    key = in_l2;
                    type = TableEnum.Single;
                    break;

                case "格式組":
                    key = in_l2;
                    type = TableEnum.Format;
                    break;

                default:
                    key = in_l2;
                    type = TableEnum.Single;
                    break;
            }

            TTable table = null;

            if (map.ContainsKey(key))
            {
                table = map[key];
            }
            else
            {
                table = new TTable
                {
                    Key = key,
                    Id = (map.Count + 1).ToString(),
                    Name = "rank_" + (map.Count + 1).ToString(),
                    Head = meeting_title,
                    Rows = new Dictionary<string, TRow>(),
                    RowSpan = 1,
                    TableType = type
                };

                switch (table.TableType)
                {
                    case TableEnum.Multiple:
                        table.Title = in_l1;
                        table.Title2 = in_l1;
                        table.LeftTitle = in_l1;
                        table.LeftTitle_Html = string.Join("<br>", in_l1.ToCharArray());
                        table.LeftTitle_Word = string.Join("\n", in_l1.ToCharArray());
                        break;

                    case TableEnum.Single:
                    case TableEnum.Format:
                        table.Title = ClearTitle(in_l1, title);
                        table.Title2 = in_l1;
                        table.LeftTitle = title;
                        table.LeftTitle_Html = string.Join("<br>", title.ToCharArray());
                        table.LeftTitle_Word = string.Join("\n", title.ToCharArray());
                        break;

                    default:
                        break;
                }

                map.Add(key, table);
            }
            return table;
        }

        private string ClearLv2(string in_l2)
        {
            if (in_l2 == "") return "";

            return in_l2.Replace("少~", "")
                .Replace("青~", "")
                .Replace("成~", "");

            //in_l2 == "" ? "" : in_l2.Split('-').Last();
        }

        private string ClearTitle(string in_l1, string clear_l2)
        {
            return clear_l2;
            if (in_l1.EndsWith("組") && clear_l2.EndsWith("組"))
            {
                return in_l1.Trim('組') + "-" + clear_l2;
            }
            else
            {
                return in_l1 + "-" + clear_l2;
            }
        }

        /// <summary>
        /// 附加 Row
        /// </summary>
        private void AddRow(Innovator inn, TTable table, Item item)
        {
            string in_l1 = item.getProperty("in_l1", "");
            string in_l2 = item.getProperty("in_l2", "");
            string in_l3 = item.getProperty("in_l3", "");
            string in_short_name = item.getProperty("in_short_name", "");
            string in_team_count = item.getProperty("in_team_count", "0");
            string in_rank_count = item.getProperty("in_rank_count", "0");
            string in_final_rank = item.getProperty("in_final_rank", "");

            //string program_name2 = item.getProperty("program_name2", "").Replace(in_l2, "");

            string program_name2 = item.getProperty("program_name2", "").Replace(in_l2, "");
            //+ "(" + in_rank_count + ")";

            var key = "";

            switch (table.TableType)
            {
                case TableEnum.Multiple:
                    key = in_l2;
                    break;

                case TableEnum.Single:
                case TableEnum.Format:
                    key = in_l3;
                    break;

                default:
                    break;
            }

            TRow row = null;
            if (table.Rows.ContainsKey(key))
            {
                row = table.Rows[key];
            }
            else
            {
                row = new TRow
                {
                    Key = key,
                    Title = in_l3,
                    No = table.Rows.Count + 1,
                    Rank1 = new TRank(),
                    Rank2 = new TRank(),
                    Rank3 = new TRank(),
                    Rank4 = new TRank(),
                    Rank5 = new TRank(),
                    Rank6 = new TRank(),
                    Rank7 = new TRank(),
                    Rank8 = new TRank(),
                    in_team_count = in_team_count,
                    in_rank_count = in_rank_count,
                };

                row.max_rank = 8;
                // Item itmMaxRank = inn.applyMethod("in_meeting_program_rank", "<in_team_count>" + in_rank_count + "</in_team_count>");
                // row.max_rank = GetIntVal(itmMaxRank.getProperty("max_rank", "0"));

                switch (table.TableType)
                {
                    case TableEnum.Multiple:
                        var c = in_l3.Contains("男") ? '男' : '女';
                        var arr = in_l3.Split(c);

                        //row.Title = in_l3 + "(" + row.in_team_count + ")";
                        row.Title = in_l3;
                        row.Title_Html = string.Join("<br>" + c, arr);
                        row.Title_Word = string.Join("\n" + c, arr);
                        break;

                    case TableEnum.Single:
                    case TableEnum.Format:
                        row.Title = in_l3.Split('：').First();
                        // row.Title_Html = row.Title + "(" + row.in_team_count + ")";
                        // row.Title_Word = row.Title + "(" + row.in_team_count + ")";
                        row.Title_Html = row.Title;
                        row.Title_Word = row.Title;
                        break;

                    default:
                        break;
                }

                table.Rows.Add(key, row);
            }

            switch (in_final_rank)
            {
                case "1":
                    SetRank(table, row.Rank1, item);
                    break;

                case "2":
                    SetRank(table, row.Rank2, item);
                    break;

                case "3":
                    SetRank(table, row.Rank3, item);
                    break;

                case "4":
                    SetRank(table, row.Rank4, item);
                    break;

                case "5":
                    SetRank(table, row.Rank5, item);
                    break;

                case "6":
                    SetRank(table, row.Rank6, item);
                    break;

                case "7":
                    SetRank(table, row.Rank7, item);
                    break;

                case "8":
                    SetRank(table, row.Rank8, item);
                    break;


                default:
                    break;
            }
        }

        //設定名次資料
        private void SetRank(TTable table, TRank rank, Item item)
        {
            rank.HasSetted = true;

            rank.Value = item;

            switch (table.TableType)
            {
                case TableEnum.Multiple:
                    rank.Org = GetOrgInfo(item);
                    rank.Text = GetNameInfo2(item);
                    break;

                case TableEnum.Single:
                case TableEnum.Format:
                    rank.Org = GetOrgInfo(item);
                    rank.Text = GetNameInfo1(item);
                    break;

                default:
                    break;
            }
        }

        //個人組、格式組
        private string GetOrgInfo(Item item)
        {
            string in_current_org = item.getProperty("in_current_org", "");
            string org = GetShortOrgName(in_current_org);
            string in_team = item.getProperty("in_team", "");
            if (in_team != "") org += " " + in_team;
            return org;
        }

        //個人組、格式組
        private string GetNameInfo1(Item item)
        {
            if (item == null)
            {
                return "&nbsp;";
            }
            
            return item.getProperty("in_name", "");

            //string in_name = item.getProperty("in_name", "");
            //string in_current_org = item.getProperty("in_current_org", "");
            //string map_short_org = item.getProperty("in_current_org", "");
            //string org = GetShortOrgName(map_short_org);
            //string in_team = item.getProperty("in_team", "");
            //if (in_team != "") org += " " + in_team;

            //return in_name + "(" + org + ")";
        }

        //團體組
        private string GetNameInfo2(Item item)
        {
            return "&nbsp;";

            //if (item == null)
            //{
            //    return "&nbsp;";
            //}

            //string in_current_org = item.getProperty("in_current_org", "");
            //string map_short_org = item.getProperty("in_current_org", "");
            //string in_team = item.getProperty("in_team", "");
            //string in_name = item.getProperty("in_name", "");
            //string org = GetShortOrgName(map_short_org);

            //if (in_team == "")
            //{
            //    return org;
            //}
            //else
            //{
            //    return org + "(" + in_team + ")";
            //}
        }

        private string GetShortOrgName(string value)
        {
            switch (value)
            {
                case "三重文化聯隊": return "三重文化";
                case "鷺江國中": return "鷺江國中";
                case "國立水里高級商工職業學校": return "水里商工";
                case "國立羅東高級商業職業學校": return "羅東高商";
                case "國立羅東高級中學": return "羅東高中";
                case "三重高中": return "三重高中";
                case "水里國中": return "水里國中";
                case "宜蘭縣冬山國民中學": return "冬山國中";
                case "桃園市立石門國民中學": return "石門國中";
                case "桃園市立武漢國民中學": return "武漢國中";
                case "臺北市立北安國民中學": return "北安國中";
                case "臺南市立大灣高中": return "大灣高中";
                case "桃園市立羅浮高級中等學校": return "羅浮高中";
                case "桃園市立龍潭高級中等學校": return "龍潭高中";
                case "新北市立三民高中": return "三民高中";
                case "新北市私立莊敬高級工業家事職業學校": return "莊敬工家";
                case "臺北市立育成高級中學": return "育成高中";
                case "台中市立東峰國民中學": return "東峰國中";
                case "宜蘭縣立五結國民中學": return "五結國中";
                case "宜蘭縣利澤國民中學": return "利澤國中";
                case "新北市立五股國民中學": return "五股國中";
                case "臺北市體育總會划船協會": return "臺北市體育總會划船協會";
                case "國立臺北商業大學": return "臺北商大";
                case "內胡高工": return "內湖高工";
                case "臺中市立大里高級中學": return "大里高中";
                case "臺南市立土城高中": return "土城高中";
                case "國立暨南國際大學": return "暨南大學";
                case "國家訓練中心": return "國訓中心";
                case "國立體育大學": return "國立體大";
                case "輔仁大學學校財團法人輔仁大學": return "輔仁大學";
                case "臺南市立大成國中": return "大成國中";

                default: return value;
            }
        }
        /// <summary>
        /// 表格類型
        /// </summary>
        private enum TableEnum
        {
            /// <summary>
            /// 未設定
            /// </summary>
            None = 0,
            /// <summary>
            /// 團體組
            /// </summary>
            Multiple = 1,
            /// <summary>
            /// 個人組
            /// </summary>
            Single = 2,
            /// <summary>
            /// 格式組
            /// </summary>
            Format = 3,
        }

        /// <summary>
        /// 表格資料模型
        /// </summary>
        private class TTable
        {
            /// <summary>
            /// 鍵
            /// </summary>
            public string Key { get; set; }

            /// <summary>
            /// Table 流水號
            /// </summary>
            public string Id { get; set; }

            /// <summary>
            /// Table 名稱
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// Table 標題
            /// </summary>
            public string Title { get; set; }

            /// <summary>
            /// Table 標題
            /// </summary>
            public string Title2 { get; set; }

            /// <summary>
            /// Table 左側標題
            /// </summary>
            public string LeftTitle { get; set; }

            /// <summary>
            /// Table 左側標題
            /// </summary>
            public string LeftTitle_Html { get; set; }

            /// <summary>
            /// Table 左側標題
            /// </summary>
            public string LeftTitle_Word { get; set; }


            /// <summary>
            /// 賽事名稱
            /// </summary>
            public string Head { get; set; }

            /// <summary>
            /// 參賽人數
            /// </summary>
            public string TeamCount { get; set; }

            /// <summary>
            /// 列資料集
            /// </summary>
            public Dictionary<string, TRow> Rows { get; set; }

            /// <summary>
            /// 跨列
            /// </summary>
            public int RowSpan { get; set; }

            /// <summary>
            /// 表格類型
            /// </summary>
            public TableEnum TableType { get; set; }

            /// <summary>
            /// 是否為結束表格
            /// </summary>
            public bool IsEndTable { get; set; }
        }

        /// <summary>
        /// 列模式
        /// </summary>
        private class TRow
        {
            /// <summary>
            /// 鍵
            /// </summary>
            public string Key { get; set; }

            /// <summary>
            /// 列序
            /// </summary>
            public int No { get; set; }

            /// <summary>
            /// 項目名稱
            /// </summary>
            public string Title { get; set; }

            /// <summary>
            /// 項目名稱
            /// </summary>
            public string Title_Html { get; set; }

            /// <summary>
            /// 項目名稱
            /// </summary>
            public string Title_Word { get; set; }

            /// <summary>
            /// 冠軍
            /// </summary>
            public TRank Rank1 { get; set; }

            /// <summary>
            /// 亞軍
            /// </summary>
            public TRank Rank2 { get; set; }

            /// <summary>
            /// 季軍
            /// </summary>
            public TRank Rank3 { get; set; }

            /// <summary>
            /// 第 4 名 or 季軍
            /// </summary>
            public TRank Rank4 { get; set; }

            /// <summary>
            /// 第 5 名
            /// </summary>
            public TRank Rank5 { get; set; }

            /// <summary>
            /// 第 6 名
            /// </summary>
            public TRank Rank6 { get; set; }

            /// <summary>
            /// 第 7 名
            /// </summary>
            public TRank Rank7 { get; set; }

            /// <summary>
            /// 第 8 名
            /// </summary>
            public TRank Rank8 { get; set; }

            public string in_team_count { get; set; }
            public string in_rank_count { get; set; }
            public int max_rank { get; set; }
        }

        /// <summary>
        /// 名次資料模型
        /// </summary>
        private class TRank
        {
            public string Org { get; set; }
            public string Text { get; set; }

            public Item Value { get; set; }

            public bool HasSetted { get; set; }
        }

        /// <summary>
        /// 匯出資料模型
        /// </summary>
        private class TExport
        {
            /// <summary>
            /// 樣板來源完整路徑
            /// </summary>
            public string Source { get; set; }

            /// <summary>
            /// 檔案名稱
            /// </summary>
            public string File { get; set; }

            /// <summary>
            /// 檔案網址
            /// </summary>
            public string Url { get; set; }
        }

        /// <summary>
        /// 組態
        /// </summary>
        private class TConfig
        {
            /// <summary>
            /// 字型名稱
            /// </summary>
            public string FontName { get; set; }

            /// <summary>
            /// 字型大小
            /// </summary>
            public int FontSize { get; set; }

            /// <summary>
            /// 樣板列數( Head 加 Body)
            /// </summary>
            public int RowCount { get; set; }

            /// <summary>
            /// 樣板列數( Body)
            /// </summary>
            public int BodyRowCount { get; set; }

            /// <summary>
            /// 列起始位置
            /// </summary>
            public int RowStart { get; set; }

            /// <summary>
            /// 欄起始位置
            /// </summary>
            public int ColStart { get; set; }

            /// <summary>
            /// 當前列位置
            /// </summary>
            public int CurrentRow { get; set; }

            /// <summary>
            /// 欄位清單
            /// </summary>
            public List<TField> Fields { get; set; }

            /// <summary>
            /// 賽事名稱
            /// </summary>
            public string MeetingTitle { get; set; }

            public int SheetCount { get; set; }

            public List<string> Sheets { get; set; }

            public int WsRow { get; set; }

            public int WsCol { get; set; }

            public string[] Heads { get; set; }

        }

        /// <summary>
        /// 欄位
        /// </summary>
        private class TField
        {
            /// <summary>
            /// 標題
            /// </summary>
            public string Title { get; set; }
            /// <summary>
            /// 屬性
            /// </summary>
            public string Property { get; set; }
            /// <summary>
            /// 格式
            /// </summary>
            public string Format { get; set; }

            /// <summary>
            /// 欄寬
            /// </summary>
            public int Width { get; set; }

            public Func<TConfig, Item, TField, string> getValue { get; set; }
        }

        /// <summary>
        /// 轉換為整數
        /// </summary>
        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        /// <summary>
        /// 轉換為日期時間
        /// </summary>
        private string GetDateTimeVal(string value, string format)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            if (result != DateTime.MinValue)
            {
                return result.ToString(format);
            }
            else
            {
                return "";
            }
        }
    }
}