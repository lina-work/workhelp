﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.ArasDesk.Samples.SqlClient
{
    class Program
    {
        static void Execute()
        {
            //資料庫名稱
            string dbName = "CTA_test";

            //當設定 Integrated Security 為 True 的時候，連線語句前面的 UserID, PW 是不起作用的，即採用 Windows 身份驗證模式。
            //使用 Windows 身分驗證 OR SA
            string connStr = "Data Source=.;Initial Catalog=master;Persist Security Info=True;User ID=sa;Password=xxxx";


            Console.WriteLine("- 還原資料庫 START: " + dbName);


            //刪除資料庫的備份和還原紀錄資訊
            string sql_del_his = $@"EXEC msdb.dbo.sp_delete_database_backuphistory @database_name = N'{dbName}'";
            RestoreDatabase(sql_del_his, connStr);
            Console.WriteLine("    - 完成: 刪除資料庫的備份和還原紀錄資訊");


            //關閉現有連接
            string sql_single_user = $@"ALTER DATABASE [{dbName}] SET SINGLE_USER WITH ROLLBACK IMMEDIATE";
            RestoreDatabase(sql_single_user, connStr);
            Console.WriteLine("    - 完成: 關閉現有連接");


            //刪除資料庫
            string sql_drop = $@"DROP DATABASE [{dbName}]";
            RestoreDatabase(sql_drop, connStr);
            Console.WriteLine("    - 完成: 刪除資料庫");


            //還原資料庫
            Console.WriteLine("    - 執行: 還原資料庫");
            string sql_restore = @"
                RESTORE DATABASE [CTA_test]
                FROM DISK = 'C:\DBBackup\cta_test_20220216_0914.bak'
                WITH FILE = 1,
                MOVE 'InnovatorSolutions' TO 'C:\DBSpace\CTA_TEST.mdf',
                MOVE 'InnovatorSolutions_log' TO 'C:\DBSpace\CTA_TEST_log.ldf',
                REPLACE
            ";
            RestoreDatabase(sql_restore, connStr);
            Console.WriteLine("    - 完成: 還原資料庫");


            //如果符合的登入不存在，sp_change_users_login 會建立新的登入， SQL Server 並將密碼指派為新登入的密碼。
            string sql_user = $@"
                USE [{dbName}]
                exec sp_change_users_login 'Update_One', 'innovator', 'innovator'
            ";
            RestoreDatabase(sql_user, connStr);
            Console.WriteLine("    - 完成: sp_change_users_login");


            Console.WriteLine("- 還原資料庫 END.");


            Console.Read();
        }

        /// <summary>
        /// 測試連線
        /// </summary>
        private static void RestoreDatabase(string sql, string connStr)
        {
            using (var conn = new System.Data.SqlClient.SqlConnection(connStr))
            {
                try
                {
                    conn.Open();
                    var cmd = new System.Data.SqlClient.SqlCommand(sql, conn);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
                catch (System.Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

    }
}
